TYMPHANY_SDK_PATH = MarshallBluetooth®/InternalFrameworks/MarshallBluetoothLibrary/SDK/TymphanySDK
DEVELOPMENT_CHECKSUM = $(shell md5 $(TYMPHANY_SDK_PATH)/Controller\ SDK/Development/TAPlatform.framework/TAPlatform | cut -d '=' -f 2)
PRODUCTION_CHECKSUM = $(shell md5 $(TYMPHANY_SDK_PATH)/Controller\ SDK/Production/TAPlatform.framework/TAPlatform | cut -d '=' -f 2)

submodules:
	git submodule sync --recursive || true
	git submodule update --init --recursive || true

strings:
	swiftc bin/main.swift MarshallBluetooth®/InternalFrameworks/MarshallBluetoothLibrary/Locales/GenerateStrings.swift -o bin/prepareTranslations
	bin/prepareTranslations
	rm bin/prepareTranslations

bootstrap:
	sudo gem install jazzy

sure:
	@echo Tymphany SDK Development MD5 checksum =$(DEVELOPMENT_CHECKSUM)
	@echo Tymphany SDK Production MD5 checksum =$(PRODUCTION_CHECKSUM)
doc:
	jazzy --clean \
		  --author Tieto \
		  --xcodebuild-arguments -scheme,MarshallBluetooth®,-project,MarshallBluetooth®.xcodeproj,-configuration,Debug \
		  --module MarshallBluetoothLibrary \
		  --exclude=/*/*ViewController*



