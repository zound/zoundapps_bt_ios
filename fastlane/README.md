fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## iOS
### ios change_version
```
fastlane ios change_version
```
Change version and build number. Options: xcodeproj, build_number, version_number
### ios change_version_and_build
```
fastlane ios change_version_and_build
```
Change version and build application. Options: app_name, build_number, version_number, configuration, export_method, build_as
### ios upload_build
```
fastlane ios upload_build
```
Upload build to iTunes Connect for TestFlight beta testing. Options: package_path, changelog, groups
### ios execute_tests
```
fastlane ios execute_tests
```
Execute unit tests. Options: scheme

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
