//
//  AppDelegate.swift
//  MarshallBluetooth®
//


import UIKit
import MarshallBluetoothLibrary
import GUMA
import JoplinPlatform
import RxSwift
import Reachability

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appFlow: AppFlow!
    var applicationMode = BehaviorRelay<AppMode>.init(value: .foreground)
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
        application.isStatusBarHidden = true

        let supportedAdapters = [TymphanyDeviceAdapter.shared]
        let deviceAdapterService: DeviceAdapterServiceType = DeviceAdapterService.shared
        
        supportedAdapters.forEach { deviceAdapterService.inputs.append(adapter: $0, mocked: false) }
        
        let changesObservable = DeviceListChanges(new: deviceAdapterService.outputs.addedDevice,
                                                  updated: deviceAdapterService.outputs.updatedDevice,
                                                  removed: deviceAdapterService.outputs.removedDevice)
        
        let applicationWorkingMode = PublishRelay<ApplicationProcessMode>()
        applicationMode
            .distinctUntilChanged()
            .subscribe(onNext: { appMode in
                switch appMode {
                case .foreground:
                    applicationWorkingMode.accept(.foreground)
                case .background:
                    applicationWorkingMode.accept(.background)
                }
            })
            .disposed(by: rx_disposeBag)
        
        let deviceService = DeviceService(deviceInfoSource: changesObservable)
        let otaService = OTAService(deviceAdapterService: deviceAdapterService,
                                    interfaceError: deviceAdapterService.outputs.error,
                                    processMode: applicationWorkingMode)
        
        let appDependencies = AppDependency(
            deviceService: deviceService,
            otaService: otaService,
            deviceAdapterService: deviceAdapterService,
            appMode: applicationMode,
            analyticsService: FirebaseAnalyticsService(),
            newsletterService: NewsletterService())

        NotificationCenter.default.rx.notification(.UIApplicationWillResignActive)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.applicationMode.accept(.background)
            }).disposed(by: rx_disposeBag)

        NotificationCenter.default.rx.notification(.UIApplicationDidBecomeActive)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.applicationMode.accept(.foreground)
            }).disposed(by: rx_disposeBag)

        
        let window = UIWindow(frame: UIScreen.main.bounds)
        let rootViewController = UIViewController.instantiate(RootViewController.self)
        
        window.rootViewController = rootViewController
        appFlow = AppFlow(with: AppFlowModel(dependency: appDependencies), rootViewController: rootViewController)
        window.makeKeyAndVisible()
        self.window = window

        return true
    }
}

