//
//  ButtonAreaAnimating.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 14/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Config {
    static let buttonAreaExpandedHeight = CGFloat(110)
    struct ButtonAreaAnimation {
        static let duration: TimeInterval = .seconds(0.3)
        static let curve: UIViewAnimationCurve = .easeInOut
    }
}

enum ButtonAreaState {
    case enabled, enabling
    case disabled, disabling
}

protocol ButtonAreaAnimating: class {
    var buttonAreaState: ButtonAreaState { set get }
    var buttonAreaHeightAnimator: UIViewPropertyAnimator { get }
    var buttonAreaBaseView: UIView { get }
    var buttonAreaHeight: NSLayoutConstraint { get }
    func setButtonArea(enabled: Bool, animated: Bool)
    func getDefautButtonAreaHeightAnimator() -> UIViewPropertyAnimator
    var additionalSpaceForButton: CGFloat { get }
}
extension ButtonAreaAnimating {

    /// Additional space related to safe area insets
    var additionalSpaceForButton: CGFloat {
        if #available(iOS 11.0, *) {
            return self.buttonAreaBaseView.window?.safeAreaInsets.bottom ?? 0
        } else {
            return 0
        }
    }

    /// Expected height for buttons area. This is height from config extended with safe area inset.
    ///
    /// - Returns: height
    private func buttonAreaExpandedHeight() -> CGFloat {
        return Config.buttonAreaExpandedHeight + additionalSpaceForButton
    }

    func setButtonArea(enabled: Bool, animated: Bool = false) {
        if !animated {
            buttonAreaHeight.constant = enabled ? buttonAreaExpandedHeight() : CGFloat()
            return
        }
        switch (buttonAreaState, enabled) {
        case (.enabled, false):
            enableButtonAreaAnimated()
        case (.disabled, true):
            disableButtonAreaAnimated()
        case (.enabling, false) where buttonAreaHeightAnimator.isRunning, (.disabling, true) where buttonAreaHeightAnimator.isRunning:
            buttonAreaHeightAnimator.isReversed = true
        default:
            break
        }
    }

    func getDefautButtonAreaHeightAnimator() -> UIViewPropertyAnimator {
        return UIViewPropertyAnimator(duration: Config.ButtonAreaAnimation.duration, curve: Config.ButtonAreaAnimation.curve)
    }

    func enableButtonAreaAnimated() {
        buttonAreaHeightAnimator.addAnimations { [weak self] in
            self?.buttonAreaHeight.constant = CGFloat()
            self?.buttonAreaBaseView.layoutIfNeeded()
        }
        buttonAreaHeightAnimator.addCompletion { [weak self] _ in
            self?.buttonAreaState = .disabled
        }
        buttonAreaState = .disabling
        buttonAreaHeightAnimator.startAnimation()
    }

    func disableButtonAreaAnimated() {
        buttonAreaHeightAnimator.addAnimations { [weak self] in
            if let strongSelf = self {
                strongSelf.buttonAreaHeight.constant = strongSelf.buttonAreaExpandedHeight()
                strongSelf.buttonAreaBaseView.layoutIfNeeded()
            }
        }
        buttonAreaHeightAnimator.addCompletion { [weak self] _ in
            self?.buttonAreaState = .enabled
        }
        buttonAreaState = .enabling
        buttonAreaHeightAnimator.startAnimation()
    }

}
