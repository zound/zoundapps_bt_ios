//
//  ButtonSegmentsControl.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 08/03/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Const {
    static let leftImageEdgeInset: CGFloat = -20
    static let alphaMax: CGFloat = 1
}

struct ButtonSegment {
    let image: UIImage
    let attributedString: NSAttributedString
}

class ButtonSegmentsControl: UIControl {
    var lastTouchedUpInsideTag: Int? {
        return internalLastTouchedUpInsideTag
    }
    private var internalLastTouchedUpInsideTag: Int?
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.spacing = .zero
        return stackView
    }()
    required convenience init(buttonSegments: [ButtonSegment]) {
        self.init()
        setupConstraints()
        add(buttonSegments: buttonSegments)
    }
    private func setupConstraints() {
        [stackView].forEach {
            addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            stackView.leftAnchor.constraint(equalTo: leftAnchor),
            stackView.rightAnchor.constraint(equalTo: rightAnchor)
        ])
    }
    func set(enabled: Bool) {
        stackView.arrangedSubviews.forEach {
            guard let button = $0 as? UIButton else { return }
            button.isUserInteractionEnabled = enabled
            button.alpha = enabled ? Const.alphaMax : .zero
        }
    }
}

private extension ButtonSegmentsControl {
    func add(buttonSegments: [ButtonSegment]) {
        buttonSegments.enumerated().forEach {
            let button = UIButton()
            button.backgroundColor = .clear
            button.addTarget(self, action: #selector(buttonTouchedUpInside(_:)), for: .touchUpInside)
            button.tag = $0.offset
            button.setImage($0.element.image, for: .normal)
            button.imageEdgeInsets = UIEdgeInsets(top: .zero, left: Const.leftImageEdgeInset, bottom: .zero, right: .zero)
            button.setAttributedTitle($0.element.attributedString, for: .normal)
            stackView.addArrangedSubview(button)
        }
    }
    @objc func buttonTouchedUpInside(_ sender: UIButton) {
        internalLastTouchedUpInsideTag = sender.tag
        sendActions(for: .touchUpInside)
    }
}
