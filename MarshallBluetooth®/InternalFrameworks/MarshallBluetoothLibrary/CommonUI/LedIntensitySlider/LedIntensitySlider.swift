//
//  LedIntensitySlider.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 25.05.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

class LedIntensitySlider: UISlider {
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        let spacing = CGFloat(8)
        var result = super.trackRect(forBounds: bounds)
        
        // to calculate the proper size for track we need to substract
        // widths of both min and max image (if provided)
        // also we need to substract spacing between images and track
        if let minImageWidth = self.minimumValueImage?.size.width {
            result.origin.x = minImageWidth + spacing
            result.size.width = bounds.size.width - (minImageWidth + spacing)
        } else {
            result.origin.x = CGFloat(0)
            result.size.width = bounds.size.width
        }
        
        if let maxImageWidth = self.maximumValueImage?.size.width {
            result.size.width -= maxImageWidth + spacing
        }

        return result
    }
    
    override func thumbRect(forBounds bounds: CGRect, trackRect rect: CGRect, value: Float) -> CGRect {
        return super.thumbRect(forBounds: bounds, trackRect: rect, value: value).offsetBy(dx: 0, dy: 0)
    }
}
