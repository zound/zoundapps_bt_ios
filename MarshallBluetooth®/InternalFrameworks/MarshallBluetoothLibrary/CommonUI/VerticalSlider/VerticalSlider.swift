//
//  VerticalSlider.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 10/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

/// Vertical slider conrol wrapping UISlider with changed orientation.
@IBDesignable class VerticalSlider: UIControl {
    private let slider = UISlider()
    @IBInspectable var value: Float {
        set {
            slider.setValue(newValue, animated: true)
        }
        get {
            return slider.value
        }
    }
    @IBInspectable var minimumValue: Float {
        set {
            slider.minimumValue = newValue
        }
        get {
            return slider.minimumValue
        }
    }
    @IBInspectable var maximumValue: Float {
        set {
            slider.maximumValue = newValue
        }
        get {
            return slider.maximumValue
        }
    }
    @IBInspectable var thumbImage: UIImage? {
        set {
            slider.setThumb(image: newValue)
        }
        get {
            return slider.thumbImage(for: .normal)
        }
    }
    @IBInspectable var minimumTrackTintColor: UIColor? {
        set {
            slider.minimumTrackTintColor = newValue
        }
        get {
            return slider.minimumTrackTintColor
        }
    }
    @IBInspectable var maximumTrackTintColor: UIColor? {
        set {
            slider.maximumTrackTintColor = newValue
        }
        get {
            return slider.maximumTrackTintColor
        }
    }
    @IBInspectable var isContinuous: Bool {
        set {
            slider.isContinuous = newValue
        }
        get {
            return slider.isContinuous
        }
    }
    required override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    func set(alpha: CGFloat) {
        slider.alpha = alpha
    }
    private func configure() {
        slider.transform = CGAffineTransform(rotationAngle: -CGFloat.pi.half)
        addSubview(slider)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        slider.bounds.size.width = bounds.height
        slider.center.x = bounds.midX
        slider.center.y = bounds.midY
    }
    override func addTarget(_ target: Any?, action: Selector, for controlEvents: UIControlEvents) {
        slider.addTarget(target, action: action, for: controlEvents)
    }
    override func removeTarget(_ target: Any?, action: Selector?, for controlEvents: UIControlEvents) {
        slider.removeTarget(target, action: action, for: controlEvents)
    }
}

