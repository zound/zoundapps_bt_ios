//
//  Checkbox.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 19/09/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Config {
    static let backgroundColor = UIColor.darkBeige
    static let checkedImage = UIImage.checkmarkMarshall
    static let uncheckedImage = UIImage.checkmarkMarshall.with(alpha: 0.2)
    static let animationDuration = TimeInterval.seconds(0.1)
}

class Checkbox: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    var checked: Bool = false {
        didSet {
            let animations: () -> Void = { [unowned self] in
                self.setImage(self.checked == true ? Config.checkedImage : Config.uncheckedImage, for: .normal)
            }
            UIView.transition(with: self, duration: Config.animationDuration, options: .transitionCrossDissolve, animations: animations)
        }
    }

    override func awakeFromNib() {
        addTarget(self, action:#selector(buttonClicked(sender:)), for: UIControlEvents.touchUpInside)
        checked = false
    }

    @objc func buttonClicked(sender: UIButton) {
        guard sender == self  else { return }
        checked = !checked
    }

}

private extension Checkbox {
    func setup() {
        backgroundColor = Config.backgroundColor
    }
}
