//
//  BlinkingLabel.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 15/12/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Const {
    static let blinkPeriod: TimeInterval = 0.5
}

class BlinkingLabel: UILabel {
    private enum Alpha: CGFloat {
        case min = 0.0
        case max = 1.0
        mutating func toggle() {
            self = self == .min ? .max : .min
        }
    }
    private var isBlinking: Bool = false
    private var currentAlpha: Alpha = .min {
        didSet {
            alpha = currentAlpha.rawValue
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        alpha = Alpha.min.rawValue
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        alpha = Alpha.min.rawValue
    }
    private func blink() {
        DispatchQueue.main.asyncAfter(deadline: .now() + Const.blinkPeriod) { [weak self] in
            if self?.isBlinking == true {
                self?.currentAlpha.toggle()
                self?.blink()
            }
        }
    }
    func startBlink() {
        guard !isBlinking else {
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + Const.blinkPeriod) { [weak self] in
            self?.isBlinking = true
            self?.blink()
        }
    }
    func stopBlink() {
        isBlinking = false
        currentAlpha = .min
    }
}
