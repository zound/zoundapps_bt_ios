//
//  UnderlineSegmentedControl.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 15/11/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Const {
    static let selectorHeight = CGFloat(2)
    static let selectorDistance = CGFloat(10)
    static let selectorAnimationDuration = TimeInterval(0.25)
    static let selectedIndexColor = Color.segmentedControlSelected
    static let notSelectedIndexColor = Color.segmentedControlNotSelected
    static let underlineBackgroundColor = Color.selectedItemBackground
}

class UnderlineSegmentedControl: UIControl {
    var selectedSegmentIndex = Int.zero {
        didSet {
            guard selectedSegmentIndex < segments.count, selectedSegmentIndex >= .zero else {
                fatalError()
            }
            update(with: selectedSegmentIndex)
        }
    }
    var selectedIndexColor = Const.selectedIndexColor
    var notSelectedIndexColor = Const.notSelectedIndexColor
    private var segments: [Any] = []
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.spacing = .zero
        return stackView
    }()
    private lazy var selectorView: UIView = {
        let selectorView = UIView()
        selectorView.backgroundColor = selectedIndexColor
        return selectorView
    }()
    private lazy var backgroundSelectorView: UIView = {
        let backgroundSelectorView = UIView()
        backgroundSelectorView.backgroundColor = Const.underlineBackgroundColor
        return backgroundSelectorView
    }()
    private var selectorViewLeadingConstraint: NSLayoutConstraint?
    required convenience init(segments: [NSAttributedString], selectedSegmentIndex: Int = .zero) {
        self.init()
        self.segments = segments
        self.selectedSegmentIndex = min(segments.count - 1, selectedSegmentIndex)
        setupConstraints()
        addSegments()
    }
    required convenience init(segments: [UIImage], selectedSegmentIndex: Int = .zero) {
        self.init()
        self.segments = segments
        self.selectedSegmentIndex = min(segments.count - 1, selectedSegmentIndex)
        setupConstraints()
        addSegments()
    }
    private func setupConstraints() {
        [stackView, backgroundSelectorView, selectorView].forEach {
            addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        selectorViewLeadingConstraint = selectorView.leadingAnchor.constraint(equalTo: leadingAnchor)
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            stackView.leftAnchor.constraint(equalTo: leftAnchor),
            stackView.rightAnchor.constraint(equalTo: rightAnchor),
            backgroundSelectorView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: Const.selectorDistance),
            backgroundSelectorView.leadingAnchor.constraint(equalTo: leadingAnchor),
            backgroundSelectorView.heightAnchor.constraint(equalToConstant: Const.selectorHeight),
            backgroundSelectorView.widthAnchor.constraint(equalTo: widthAnchor),
            selectorView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: Const.selectorDistance),
            selectorViewLeadingConstraint!,
            selectorView.heightAnchor.constraint(equalToConstant: Const.selectorHeight),
            selectorView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: segments.count == .zero ? .zero : CGFloat(1)/CGFloat(segments.count))
        ])
    }
}

private extension UnderlineSegmentedControl {
    func addSegments() {
        segments.enumerated().forEach {
            let button = UIButton()
            button.backgroundColor = .clear
            button.addTarget(self, action: #selector(buttonTouchedUpInside(_:)), for: .touchUpInside)
            button.tag = $0.offset
            button.setFace(for: $0.element)
            button.set(color: $0.offset == selectedSegmentIndex ? selectedIndexColor : notSelectedIndexColor, for: $0.element)
            stackView.addArrangedSubview(button)
        }
    }
    @objc func buttonTouchedUpInside(_ sender: UIButton) {
        selectedSegmentIndex = sender.tag
        sendActions(for: .valueChanged)
    }
    func update(with selectedSegemntIndex: Int) {
        let zippedViewsWithSegments = zip(stackView.arrangedSubviews, segments)
        zippedViewsWithSegments.filter { $0.0.tag != selectedSegemntIndex }.forEach {
            ($0.0 as! UIButton).set(color: notSelectedIndexColor, for: $0.1)
        }
        zippedViewsWithSegments.filter { $0.0.tag == selectedSegemntIndex }.forEach {
            ($0.0 as! UIButton).set(color: selectedIndexColor, for: $0.1)
        }
        updateSelectorViewLeadingConstraint(with: selectedSegmentIndex)
    }
    func updateSelectorViewLeadingConstraint(with selectedSegemntIndex: Int) {
        selectorViewLeadingConstraint?.constant = selectorView.bounds.width * CGFloat(selectedSegemntIndex)

        UIView.animate(withDuration: Const.selectorAnimationDuration, delay: .zero, options: .curveEaseInOut, animations: {
            self.layoutIfNeeded()
        }, completion: nil)
    }
}

private extension UIButton {
    func setFace(for segment: Any) {
        switch segment {
        case is NSAttributedString:
            setAttributedTitle(segment as? NSAttributedString, for: .normal)
        case is UIImage:
            setImage((segment as? UIImage)?.withRenderingMode(.alwaysTemplate), for: .normal)
        default:
            break
        }
    }
    func set(color: UIColor, for segment: Any) {
        switch segment {
        case is NSAttributedString:
            guard let attributedTtile = attributedTitle(for: .normal) else {
                return
            }
            var attributes = attributedTtile.attributes(at: .zero, effectiveRange: nil)
            attributes[NSAttributedString.Key.foregroundColor] = color
            setAttributedTitle(NSAttributedString(string: attributedTtile.string, attributes: attributes), for: .normal)
        case is UIImage:
            tintColor = color
        default:
            break
        }
    }
}
