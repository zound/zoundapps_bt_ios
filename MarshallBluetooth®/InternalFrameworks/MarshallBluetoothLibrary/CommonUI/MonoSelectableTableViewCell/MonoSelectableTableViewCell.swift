//
//  MonoSelectableTableViewCell.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 14/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

private struct Config {
    static let selectedBackgroundViewColor = Color.selectedItemBackground
    static let checkmarkRightInset = CGFloat(10)
}

class MonoSelectableTableViewCell: UITableViewCell {
    private static let checkmarkInsets = UIEdgeInsets(top: .zero, left: .zero, bottom: .zero, right: Config.checkmarkRightInset)
    var permanentlySelected = false {
        didSet {
            (accessoryView as? UIImageView)?.isHidden = !permanentlySelected
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        accessoryView = UIImageView(image: UIImage.checkmarkMarshall.with(insets: MonoSelectableTableViewCell.checkmarkInsets))
        selectedBackgroundView = UIView().with(backgroundColor: Config.selectedBackgroundViewColor)
        backgroundColor = .clear
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(permanentlySelected ? true : selected, animated: animated)
        if !permanentlySelected {
            (accessoryView as? UIImageView)?.isHidden = !selected
        }
    }
}
