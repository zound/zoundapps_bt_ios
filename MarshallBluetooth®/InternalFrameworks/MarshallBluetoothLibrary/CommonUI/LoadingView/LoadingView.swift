//
//  LoadingView.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 24/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Config {
    static let animationDuration = TimeInterval.seconds(0.3)
}

class LoadingView: UIView {

    var imageView: UIImageView?

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
        addRotatingImageView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
        addRotatingImageView()
    }

    private func configure() {
        backgroundColor = Color.background
        alpha = CGFloat()
    }

    private func addRotatingImageView() {
        imageView = UIImageView(image: UIImage.commonSpinner)
        addSubview(imageView!)
        imageView?.center = center
        imageView?.rotate()
    }

    func fadeOutRotatingImageView(completion: @escaping (Bool) -> Void) {
        UIView.animate(withDuration: Config.animationDuration, delay: TimeInterval(), options: .curveEaseInOut, animations: { [weak self] in
            self?.imageView?.alpha = CGFloat()
        }, completion: completion)
    }

}
