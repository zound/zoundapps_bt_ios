//
//  DropDownList.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 23/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

private class Config {
    static let backgroundColor = Color.background
    static let borderColor = UIColor.darkBeige
    static let disabledColor = UIColor.darkBeige.withAlphaComponent(0.4)
    static let borderWidth = CGFloat(2.0)
    static let arrowDownImage = UIImage.arrowDownGold
    static let tintColor = UIColor.darkBeige
    static let animationDuration = TimeInterval.seconds(0.4)
    static let tableViewAlpha = CGFloat(1.0)
}

/// Delegate providing drop down list events.
protocol DropDownListDelegate: class {

    /// Called when drop list option is selected.
    ///
    /// - Parameter index: Index of currently selected option.
    func didSelect(index: Int)

}

class DropDownList: UIButton {

    private var options: [NSAttributedString] = [] {
        didSet {
            dropDownListView.options = options
        }
    }
    private enum DropDownListState {
        case expanded, collapsed, animating
    }
    private var dropDownListState: DropDownListState = .collapsed
    private var dropDownListView = DropDownListView()
    private var height = NSLayoutConstraint()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }

    override func didMoveToSuperview() {
        if let superview = superview {
            superview.addSubview(dropDownListView)
            superview.bringSubview(toFront: dropDownListView)
            dropDownListView.topAnchor.constraint(equalTo: bottomAnchor).isActive = true
            dropDownListView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
            dropDownListView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
            height = dropDownListView.heightAnchor.constraint(equalToConstant: CGFloat())
            if let imageView = imageView {
                let rightInset = -imageView.bounds.width
                imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: rightInset).isActive = true
                imageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: CGFloat()).isActive = true
            }
        }
    }

    /// Delegate providing drop down list events.
    weak var delegate: DropDownListDelegate?

    /// Currently selected index. Once set drop down list will be updated together with button label.
    var selectedIndex: Int = Int() {
        didSet {
            guard let option = options[optional: selectedIndex] else {
                selectedIndex = oldValue
                return
            }
            let attributedTitle = isEnabled ? UIFont.attributedDropdown(option.string) : UIFont.attributedDropdown(option.string, color: Config.disabledColor)
            setAttributedTitleAnimated(attributedTitle, for: .normal)
            dropDownListView.selectedIndex = selectedIndex
        }
    }

    /// Feeds drop down list with data.
    ///
    /// - Parameters:
    ///   - options: Array of drop list options.
    ///   - selectedIndex: Currently selected index.
    func setup(options: [NSAttributedString], selectedIndex: Int = Int()) {
        self.options = options
        self.selectedIndex = selectedIndex
    }

    func set(rowHeight: CGFloat) {
        self.dropDownListView.rowHeight = rowHeight
    }

    func setCollapsed(completion: @escaping (Bool) -> Void) {
        collapse(completion: completion)
    }

    func set(enabled: Bool, withSelected index: Int) {
        if dropDownListState == .expanded {
            collapse { [weak self] _ in
                self?.dropDownListState = .collapsed
                self?.isEnabled = enabled
                self?.selectedIndex = index
                self?.layer.borderColor = enabled ? Config.borderColor.cgColor : Config.disabledColor.cgColor
            }
        } else {
            isEnabled = enabled
            selectedIndex = index
            layer.borderColor = enabled ? Config.borderColor.cgColor : Config.disabledColor.cgColor
        }

    }
}

private extension DropDownList {

    func configure() {
        backgroundColor = Config.backgroundColor
        layer.borderWidth = Config.borderWidth
        layer.borderColor = Config.borderColor.cgColor
        setImage(Config.arrowDownImage, for: .normal)
        tintColor = Config.tintColor
        addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        dropDownListView = DropDownListView(frame: CGRect.zero)
        dropDownListView.delegate = self
        dropDownListView.translatesAutoresizingMaskIntoConstraints = false
        translatesAutoresizingMaskIntoConstraints = false
        imageView?.translatesAutoresizingMaskIntoConstraints = false
    }

    @objc func buttonAction(sender: UIButton!) {
        switch dropDownListState {
        case .collapsed:
            dropDownListState = .animating
            expand { [weak self] _ in
                self?.dropDownListState = .expanded
            }
        case .expanded:
            dropDownListState = .animating
            collapse { [weak self] _ in
                self?.dropDownListState = .collapsed
            }
        case .animating:
            break
        }
    }

    func expand(completion: @escaping (Bool) -> Void) {
        NSLayoutConstraint.deactivate([height])
        height.constant = dropDownListView.tableView.contentSize.height
        NSLayoutConstraint.activate([height])
        UIView.animate(withDuration: Config.animationDuration, delay: TimeInterval(), options: .curveEaseInOut, animations: { [weak self] in
            self?.dropDownListView.layoutIfNeeded()
            guard let dropDownListViewHeight = self?.dropDownListView.bounds.height else { return }
            self?.dropDownListView.center.y += dropDownListViewHeight.half
            self?.dropDownListView.tableView.alpha = Config.tableViewAlpha
        }, completion: completion)
    }

    func collapse(completion: @escaping (Bool) -> Void) {
        NSLayoutConstraint.deactivate([height])
        height.constant = CGFloat()
        NSLayoutConstraint.activate([height])
        dropDownListView.tableView.alpha = 1
        UIView.animate(withDuration: Config.animationDuration, delay: TimeInterval(), options: .curveEaseInOut, animations: { [weak self] in
            guard let dropDownListViewHeight = self?.dropDownListView.bounds.height else { return }
            self?.dropDownListView.center.y -= dropDownListViewHeight.half
            self?.dropDownListView.tableView.alpha = CGFloat()
            self?.dropDownListView.layoutIfNeeded()
        }, completion: completion)
    }

}

extension DropDownList: DropDownListDelegate {

    func didSelect(index: Int) {
        dropDownListState = .animating
        selectedIndex = index
        collapse { [weak self] _ in
            self?.dropDownListState = .collapsed
            self?.delegate?.didSelect(index: index)
        }
    }

}
