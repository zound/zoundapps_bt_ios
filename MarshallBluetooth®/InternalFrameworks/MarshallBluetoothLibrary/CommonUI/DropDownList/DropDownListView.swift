//
//  DropDownListView.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 23/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

private class Config {
    static let rowHeight = CGFloat(65.0)
    static let cellBackgroundColor = UIColor.stylebar
    static let cellSelectedBackgroundColor = UIColor.highlight
    static let cellTextColor = UIColor.white
}

class DropDownListView: UIView {

    weak var delegate: DropDownListDelegate?
    var options: [NSAttributedString] = []
    var selectedIndex: Int = Int() {
        didSet {
            tableView.reloadData()
        }
    }
    var tableView = UITableView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }

    var rowHeight: CGFloat = Config.rowHeight {
        didSet {
            tableView.rowHeight = rowHeight
            tableView.estimatedRowHeight = rowHeight
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }

}

private extension DropDownListView {

    func configure() {
        backgroundColor = Config.cellBackgroundColor
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        tableView.alpha = CGFloat()
        tableView.rowHeight = Config.rowHeight
        tableView.estimatedRowHeight = Config.rowHeight
        tableView.separatorInset = UIEdgeInsets(top: CGFloat(), left: CGFloat(), bottom: CGFloat(), right: .greatestFiniteMagnitude)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(DropDownListCell.self, forCellReuseIdentifier: String(describing: DropDownListCell.self))
        tableView.backgroundView = UIView().with(backgroundColor: Config.cellBackgroundColor)
        addSubview(tableView)
        tableView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }

}

extension DropDownListView: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DropDownListCell.self), for: indexPath)
        cell.textLabel?.attributedText = options[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.item == selectedIndex {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelect(index: indexPath.item)
    }

}

private class DropDownListCell: UITableViewCell {

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectedBackgroundView = UIView().with(backgroundColor: Config.cellSelectedBackgroundColor)
        backgroundColor = .none
        textLabel?.textAlignment = .center
        textLabel?.textColor = Config.cellTextColor
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
