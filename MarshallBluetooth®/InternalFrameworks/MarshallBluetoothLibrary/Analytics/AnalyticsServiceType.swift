//
//  AnalyticsServiceType.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 03/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

public protocol AnalyticsServiceInputs {
    func log(_ event: AnalyticsEvent)
    func log(_ event: AnalyticsEvent, associatedWith deviceModel: String)
    func log(_ event: AnalyticsEvent, associatedWith deviceModel: DeviceModel?)
    func setAnalyticsCollection(enabled: Bool)
}

public protocol AnalyticsServiceType {
    var inputs: AnalyticsServiceInputs { get }
}

public extension AnalyticsServiceInputs {
    func log(_ event: AnalyticsEvent) {
        log(event, associatedWith: nil)
    }
    func log(_ event: AnalyticsEvent, associatedWith deviceModel: String) {
        log(event, associatedWith: DeviceModel(name: deviceModel))
    }
}
