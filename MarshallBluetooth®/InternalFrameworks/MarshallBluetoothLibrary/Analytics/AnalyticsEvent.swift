//
//  AnalyticsEvent.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 03/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

public enum AnalyticsEvent {
    case appGraphicalEqualizerPresetChanged(GraphicalEqualizerPreset)
    case appSpeakerNameChanged
    case appShareDataSwitched(SwitchState)
    case appOtaStarted
    case appOtaCompleted
    case appOtaFailed(OtaError)
    case appForcedOtaStarted
    case appOnboardingStayUpdatedSkipped
    case appOnboardingShareDataSkipped
    case appNewsletterSignedUp
    case appVolumeChanged(Int)
    case appSourceBluetoothActivated
    case appSourceAuxActivated
    case appSourceRcaActivated
    case appMediaButtonTouchedPlay
    case appMediaButtonTouchedPause
    case appMediaButtonTouchedNext
    case appMediaButtonTouchedPrev
    case appCouplingSwitchedAmbient
    case appCouplingSwitchedStereo
    case appLightSettingChanged(Int)
    case appErrorOccurredBluetooth
    case appErrorOccurredNetwork
    case appErrorOccurredOtaFlashing
    case appErrorOccurredOtaDownload
    case appErrorOccurredOtaUndefined
}


public extension AnalyticsEvent {
    enum GraphicalEqualizerPreset: String {
        case flat
        case rock
        case metal
        case pop
        case hipHop
        case electronic
        case jazz
        case custom
    }
    enum SwitchState: String {
        case on
        case off
    }
    enum OtaError: String {
        case unknownDeviceType
        case bluetoothOff
        case internetNotAvailable
        case deviceDisconnected
        case errorParsingRemoteURL
        case unknownRemoteFwVersion
        case unknownRemoteDownloadURL
        case emptyLocalOrRemoteFirmwareVersion
        case deviceInOTANotFoundInManagedDevicesList
        case unableToCreateDownloadURL
        case failedToCreateUnzippedFolder
        case failedToUnzipFirmwareFiles
        case failedToCalculateMD5Checksum
        case md5checksumMismatch
        case urlSessionError
        case unknownRemoteVersion
        case otherError
    }
}

