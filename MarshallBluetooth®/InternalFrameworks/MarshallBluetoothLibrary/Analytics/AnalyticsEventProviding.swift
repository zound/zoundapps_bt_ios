//
//  AnalyticsEventProviding.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 03/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

protocol AnalyticsEventProviding {
    var analyticsEvent: AnalyticsEvent { get }
}

