//
//  MockedAnalyticsService.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 07/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

@testable import MarshallBluetoothLibrary

struct MockedAnalyticsService: AnalyticsServiceType, AnalyticsServiceInputs {
    var inputs: AnalyticsServiceInputs { return self }
    func log(_ event: AnalyticsEvent, associatedWith deviceModel: DeviceModel?) {}
    func setAnalyticsCollection(enabled: Bool) {}
}

struct MockedAnalyticsServiceDependency: AnalyticsServiceDependency {
    var analyticsService: AnalyticsServiceType = MockedAnalyticsService()
}

struct MockedAppModeDependency: AppModeDependency {
    var appMode = BehaviorRelay<AppMode>(value: .foreground)
}
