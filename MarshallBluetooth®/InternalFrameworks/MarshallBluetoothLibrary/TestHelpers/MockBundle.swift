//
//  MockBundle.swift
//  MarshallBluetoothLibraryTests
//

// It's heavily inspired by the Kickstarter iOS app
// https://github.com/kickstarter/ios-oss

import Foundation
import MarshallBluetoothLibrary

private let storage = [
    "Base": [
        "help.online_manual.button.go_to_website" : "GO TO WEBSITE",
        "player.session_lost.timeout" : "Connection to %{speakerName} has timed out. Press \"Reconnect\" to try again."
    ],
    "de" : [
        "help.online_manual.button.go_to_website" : "ZUR WEBSITE",
        "player.session_lost.timeout" : "Verbindung zu %{speakerName} ist abgelaufen. Drücke auf „Erneut verbinden“, um es noch einmal zu versuchen."
    ]
]

internal struct MockBundle: BundleType {
    
    let bundleIdentifier: String?
    fileprivate let store: [String: String]
    
    init(bundleIdentifier: String? = "lang.bundle.mock", lang: String = "Base") {
        self.bundleIdentifier = bundleIdentifier
        self.store = storage[lang] ?? [:]
    }
    
    func localizedString(forKey key: String, value: String?, table tableName: String?) -> String {
        // A real `Bundle` will return the key if the key is missing and value is `nil`.
        return store[key] ?? value ?? key
    }
    
    func path(forResource name: String?, ofType ext: String?) -> String? {
        return name
    }
    
    static func create(path: String) -> BundleType? {
        return MockBundle(lang: path)
    }
}




