//
//  Double+Extension.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 30.05.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

extension Double {
    /// Trunc the double to decimal places value
    func trunc(toPlaces places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return Double(Int(self * divisor)) / divisor
    }
}
