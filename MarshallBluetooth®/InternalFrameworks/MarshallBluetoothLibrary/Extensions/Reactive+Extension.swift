//
//  Reactive+Extension.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 05.07.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift

// MARK: - Useful extensions to existing RxCocoa implementation and UIView
extension Reactive where Base: UIView {
    
    /// Bindable sink reverse to 'hidden' property of UIView
    public var visible: Binder<Bool> {
        return Binder(self.base) { view, visible in
            view.isHidden = !visible
        }
    }
}

// MARK: - Useful extensions to existing RxCocoa implementation and UILabel
extension Reactive where Base: UILabel {
    
    /// Bindable sink to 'isEnabled' property of UILabel
    public var enabled: Binder<Bool> {
        return Binder(self.base) { view, enabled in
            view.isEnabled = enabled
        }
    }
}
