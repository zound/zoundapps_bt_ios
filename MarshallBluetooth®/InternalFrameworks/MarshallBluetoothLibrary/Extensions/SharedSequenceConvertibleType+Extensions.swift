//
//  SharedSequenceConvertibleType+Extensions.swift
//  MarshallBluetooth®
//
//  Created by Paprota Przemyslaw on 19/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

extension SharedSequenceConvertibleType {

    public func skipOne() -> SharedSequence<Self.SharingStrategy, Self.E> {
        return skip(1)
    }

}
