//
//  CGFloat+Extensions.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 10/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import CoreGraphics

extension CGFloat {

    /// Provides value divided by 2
    var half: CGFloat {
        return self / 2
    }

}
