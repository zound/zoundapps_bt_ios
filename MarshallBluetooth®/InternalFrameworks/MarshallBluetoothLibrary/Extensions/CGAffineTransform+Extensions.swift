//
//  CGAffineTransform+Extensions.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 09/12/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

extension CGAffineTransform {
    static let headerScale = CGAffineTransform(scaleX: 0.9, y: 0.9)
}
