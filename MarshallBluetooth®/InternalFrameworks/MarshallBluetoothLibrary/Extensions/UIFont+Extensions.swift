//
//  UIFont+Extensions.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 13/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

// taken from https://stackoverflow.com/questions/30507905/xcode-using-custom-fonts-inside-dynamic-framework
public extension UIFont {

    static func registerFont(name: String, bundle: Bundle = .framework) {
        guard let path = bundle.path(forResource: name, ofType: "ttf") else {
            fatalError("Path for font: \(name) not found")
        }
        guard let fontData = NSData(contentsOfFile: path) else {
            fatalError("Could not create NSData containing font using path: \(path)")
        }
        guard let dataProvider = CGDataProvider(data: fontData) else {
            fatalError("Could not create CGDataProvider for font \(name)")
        }
        guard let fontRef = CGFont(dataProvider) else {
            fatalError("Could not create fontRef for font \(name)")
        }
        var errorRef: Unmanaged<CFError>? = nil
        if(CTFontManagerRegisterGraphicsFont(fontRef, &errorRef) == false) {
            fatalError("Failed to register font: \(name)")
        }
    }

}
