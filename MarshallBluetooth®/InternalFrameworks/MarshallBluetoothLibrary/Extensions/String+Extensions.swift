//
//  String+Extensions.swift
//  MarshallBluetoothLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 23.03.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

// MARK: - Utility to format MAC address into human-friendly way
extension String {
    func formatToMAC() -> String {
        let groupSize = 2
        let separator = ":"
        
        if self.count <= groupSize {
            return self
        }
        
        let splitIndex = index(startIndex, offsetBy: groupSize)
        return String(self[..<splitIndex]) + separator + String(self[splitIndex...]).formatToMAC()
    }
}

// MARK: - Remove all white characters in string (leading, trailing or in the middle, etc)
extension String {
    static let invisible = String(" ")
    func removeWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
}
