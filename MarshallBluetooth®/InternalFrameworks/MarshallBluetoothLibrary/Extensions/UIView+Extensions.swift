//
//  UIView+Extensions.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 16/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

public extension UIView {

    static var defaultNib: String {
        return self.description().components(separatedBy: ".").dropFirst().joined(separator: ".")
    }
    
    static func instantiate<V: UIView>(_ view:V.Type) -> V {
        return Nib(rawValue: V.defaultNib)!.instantiate(V.self)
    }

    func with(backgroundColor: UIColor) -> UIView {
        self.backgroundColor = backgroundColor
        return self
    }

    func setConstrainsOnBordersToBeEqual(with view: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: view.topAnchor),
            bottomAnchor.constraint(equalTo: view.bottomAnchor),
            leftAnchor.constraint(equalTo: view.leftAnchor),
            rightAnchor.constraint(equalTo: view.rightAnchor)
        ])
    }

}
