//
//  BatteryStatus+Extensions.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 30/10/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import GUMA

extension BatteryStatus {
    func icon() -> UIImage {
        switch self {
        case .l10:
            return UIImage.BatteryLevel.l10
        case .l20:
            return UIImage.BatteryLevel.l20
        case .l30:
            return UIImage.BatteryLevel.l30
        case .l40:
            return UIImage.BatteryLevel.l40
        case .l50:
            return UIImage.BatteryLevel.l50
        case .l60:
            return UIImage.BatteryLevel.l60
        case .l70:
            return UIImage.BatteryLevel.l70
        case .l80:
            return UIImage.BatteryLevel.l80
        case .l90:
            return UIImage.BatteryLevel.l90
        case .full:
            return UIImage.BatteryLevel.full
        case .unknown, .charging:
            return UIImage()
        }
    }
    func description() -> String {
        switch self {
        case .l10(let value): return String(format: "%d%%", value)
        case .l20(let value): return String(format: "%d%%", value)
        case .l30(let value): return String(format: "%d%%", value)
        case .l40(let value): return String(format: "%d%%", value)
        case .l50(let value): return String(format: "%d%%", value)
        case .l60(let value): return String(format: "%d%%", value)
        case .l70(let value): return String(format: "%d%%", value)
        case .l80(let value): return String(format: "%d%%", value)
        case .l90(let value): return String(format: "%d%%", value)
        case .full: return "100%"
        default:
            return String()
        }
    }

}
