//
//  UIEdgeInsets+Extensions.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 15/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

extension UIEdgeInsets {

    static let backBarButtonItemImageEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
    static let leftBarButtonItemImageEdgeInsets = UIEdgeInsets(top: 11 + 2, left: 0, bottom: 11, right: 50)
}
