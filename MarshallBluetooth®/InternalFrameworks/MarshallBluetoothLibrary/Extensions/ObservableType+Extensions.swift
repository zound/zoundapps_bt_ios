//
//  ObservableType+Extensions.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 08/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift

extension ObservableType {

    public func skipOne() -> RxSwift.Observable<Self.E> {
        return skip(1)
    }

    public func takeOne() -> RxSwift.Observable<Self.E> {
        return take(1)
    }


}
