//
//  UISlider+Extensions.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 10/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

extension UISlider {

    private static let controlStates: [UIControlState] = [.normal, .selected, .disabled, .highlighted, .application, .reserved]

    /// Assigns a thumb image to all control states.
    ///
    /// - Parameter image: The thumb image.
    func setThumb(image: UIImage?) {
        UISlider.controlStates.forEach {
            setThumbImage(image, for: $0)
        }
    }

}
