//
//  UITableView+Extensions.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 15/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

public extension UITableView {

    func register(nib: Nib, inBundle bundle: Bundle = .framework) {
        self.register(UINib(nibName: nib.rawValue, bundle: bundle), forCellReuseIdentifier: nib.rawValue)
    }
}
