//
//  UIViewController+Extensions.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 13/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

public extension UIViewController {

    static var defaultNib: String {
        return self.description().components(separatedBy: ".").dropFirst().joined(separator: ".")
    }
    
    static func instantiate<VC: UIViewController>(_ viewController: VC.Type) -> VC {
        return Nib(rawValue: VC.defaultNib)!.instantiate(VC.self)
    }

}

extension UIViewController {

    private final class NavigationItemTitleView: UILabel {
        static func create(with attributedText: NSAttributedString) -> NavigationItemTitleView {
            let navigationItemTitleView = NavigationItemTitleView()
            navigationItemTitleView.attributedText = attributedText
            navigationItemTitleView.sizeToFit()
            return navigationItemTitleView
        }
    }

    struct NavigationBarConfiguration {
        var isBackButtonHidden: Bool = false
        var isTranslucent: Bool = false
        var navigationBarTheme: NavigationBarTheme = .dark
    }

    /// Setups UINavigationBar with custom arrow and title.
    ///
    /// - Parameters:
    ///   - title: Title attributed string to be presented within navigation bar.
    ///   - configuration: Navigation bar configuration.
    func setupNavigationBar(title: NSAttributedString, configuration: NavigationBarConfiguration = NavigationBarConfiguration()) {
        navigationItem.titleView = NavigationItemTitleView.create(with: title)
        navigationItem.hidesBackButton = configuration.isBackButtonHidden
        navigationItem.backBarButtonItem = UIBarButtonItem(title: String(), style: .plain, target: nil, action: nil)
        let backArrowImage = UIImage.backArrow.with(insets: .backBarButtonItemImageEdgeInsets)
        navigationController?.navigationBar.backIndicatorImage = backArrowImage
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = backArrowImage
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = configuration.navigationBarTheme.tintColor
        navigationController?.navigationBar.barTintColor = configuration.navigationBarTheme.barTintColor
        navigationController?.navigationBar.isTranslucent = configuration.isTranslucent
    }

}
