//
//  Dictionary+Extensions.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 27/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

extension Dictionary where Value: Equatable {

    /// Returns all keys for given value.
    ///
    /// - Parameter value: Dictionary value.
    /// - Returns: Array of keys associated with value.
    func keys(for value: Value) -> [Key] {
        return filter { $1 == value }.map { $0.0 }
    }

}
