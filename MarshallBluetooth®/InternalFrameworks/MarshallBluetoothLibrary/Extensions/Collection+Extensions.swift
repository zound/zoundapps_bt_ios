//
//  Collection+Extensions.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 23/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

public extension Collection {

    subscript(optional index: Index) -> Iterator.Element? {
        return self.indices.contains(index) ? self[index] : nil
    }

}
