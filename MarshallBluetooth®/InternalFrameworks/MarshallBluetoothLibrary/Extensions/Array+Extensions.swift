//
//  Array+Extensions.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 18/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

extension Array where Element == FlowModel {

    mutating func remove(itemsWith flowId: FlowId) {
        self = Array(drop { type(of: $0).id == flowId })
    }

}
