//
//  UICollectionView+Extension.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 25.06.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

public extension UICollectionView {
    
    func register(nib: Nib, inBundle bundle: Bundle = .framework) {
        self.register(UINib(nibName: nib.rawValue, bundle: bundle), forCellWithReuseIdentifier: nib.rawValue)
    }
}
