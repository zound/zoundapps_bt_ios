//
//  UIButton+Extensions.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 06/11/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit

extension UIButton {
    public func setAttributedTitleAnimated(_ title: NSAttributedString?, for state: UIControl.State) {
        if attributedTitle(for: state)?.string != title?.string {
            UIView.performWithoutAnimation {
                setAttributedTitle(NSAttributedString(string: String.invisible), for: state)
                layoutIfNeeded()
            }
        }
        setAttributedTitle(title, for: state)
    }
}
