//
//  UIImage+Extensions.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 15/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

extension UIImage {

    /// Extends UIImage with transparent insets.
    ///
    /// - Parameter insets: Insets to be applied on UIImage.
    /// - Returns: Extended UIImage.
    func with(insets: UIEdgeInsets) -> UIImage? {
        let newSize = CGSize(width: size.width + insets.left + insets.right, height: size.height + insets.top + insets.bottom)
        UIGraphicsBeginImageContextWithOptions(newSize, false, scale)
        let _ = UIGraphicsGetCurrentContext()
        draw(at: CGPoint(x: insets.left, y: insets.top))
        let imageWithInsets = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return imageWithInsets
    }

    /// Applys alpha to UIImage
    ///
    /// - Parameter alpha: Range (0, 1).
    /// - Returns: UIImage with alpha.
    func with(alpha: CGFloat) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: .zero, blendMode: .normal, alpha: alpha)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
}

extension UIImage {
    func mergeWith(image: UIImage, x: CGFloat, y: CGFloat) -> UIImage {
        let newWidth = size.width < x + image.size.width ? x + image.size.width : size.width
        let newHeight = size.height < y + image.size.height ? y + image.size.height : size.height
        let newSize = CGSize(width: newWidth, height: newHeight)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        draw(in: CGRect(origin: CGPoint.zero, size: size))
        image.draw(in: CGRect(origin: CGPoint(x: x, y: y), size: image.size))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }
}

extension UIImage {
    func scaled(to size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, CGFloat())
        draw(in: CGRect(x: CGFloat(), y: CGFloat(), width: size.width, height: size.height))
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

extension UIImage {



    static let quick_guide_1 = UIImage(named: "quick_guide_1_image", in: .framework, compatibleWith: nil)!
    static let quick_guide_2 = UIImage(named: "quick_guide_2_image", in: .framework, compatibleWith: nil)!
    static let quick_guide_4 = UIImage(named: "quick_guide_4_image", in: .framework, compatibleWith: nil)!
    static let quick_guide_5 = UIImage(named: "quick_guide_5_image", in: .framework, compatibleWith: nil)!


    static let forwardArrow = UIImage(named: "forward_arrow_button", in: .framework, compatibleWith: nil)!
    static let backArrow = UIImage(named: "back_arrow_button", in: .framework, compatibleWith: nil)!
    static let arrowDownGold = UIImage(named: "arrowDownGold", in: .framework, compatibleWith: nil)!
    static let commonSpinner = UIImage(named: "common_spinner", in: .framework, compatibleWith: nil)!
    static let checkmarkMarshall = UIImage(named: "checkMark", in: .framework, compatibleWith: nil)!
    static let failIcon = UIImage(named: "hamburger_menu.close", in: .framework, compatibleWith: nil)!
    static let warningIcon = UIImage(named: "hamburger_menu.info", in: .framework, compatibleWith: nil)!
    static let sliderThumb = UIImage(named: "slider_handle_round", in: .framework, compatibleWith: nil)!
    static let scrubberThumb = UIImage(named: "scrubberHandle", in: .framework, compatibleWith: nil)!
    static let bluetoothImage = UIImage(named: "bluetooth", in: .framework, compatibleWith: nil)!
    static let auxImage = UIImage(named: "auxiliary", in: .framework, compatibleWith: nil)!
    static let rcaImage = UIImage(named: "rca", in: .framework, compatibleWith: nil)!
    static let bluetoothLargeImage = UIImage(named: "bluetoothLarge", in: .framework, compatibleWith: nil)!
    static let auxLargeImage = UIImage(named: "auxLarge", in: .framework, compatibleWith: nil)!
    static let rcaLargeImage = UIImage(named: "rcaLarge", in: .framework, compatibleWith: nil)!
    static let guidePlay = UIImage(named: "guide_play.image", in: .framework, compatibleWith: nil)!
    static let guideBluetooth = UIImage(named: "guide_bluetooth.image", in: .framework, compatibleWith: nil)!
    static let coupleSpeakers = UIImage(named: "couple_speakers.image", in: .framework, compatibleWith: nil)!
    static let volumeThumb = UIImage(named: "slider_handle_round", in: .framework, compatibleWith: nil)!
    static let volumeIcon = UIImage(named: "volume_icon", in: .framework, compatibleWith: nil)!
    static let deviceSettingsIcon = UIImage(named: "device_settings_icon", in: .framework, compatibleWith: nil)!
    static let deviceSettingsIconUpdate = UIImage(named: "device_settings_icon_ota_indicator", in: .framework, compatibleWith: nil)!
    static let playPauseBackground = UIImage(named: "playPauseBackground", in: .framework, compatibleWith: nil)!
    static let playButton = UIImage(named: "playButton", in: .framework, compatibleWith: nil)!
    static let pauseButton = UIImage(named: "pauseButton", in: .framework, compatibleWith: nil)!
    static let welcomeSpeakers = UIImage(named: "welcome_speakers", in: .framework, compatibleWith: nil)!
    struct BatteryLevel {
        static let l10 = UIImage(named: "battery10", in: .framework, compatibleWith: nil)!
        static let l20 = UIImage(named: "battery20", in: .framework, compatibleWith: nil)!
        static let l30 = UIImage(named: "battery30", in: .framework, compatibleWith: nil)!
        static let l40 = UIImage(named: "battery40", in: .framework, compatibleWith: nil)!
        static let l50 = UIImage(named: "battery50", in: .framework, compatibleWith: nil)!
        static let l60 = UIImage(named: "battery60", in: .framework, compatibleWith: nil)!
        static let l70 = UIImage(named: "battery70", in: .framework, compatibleWith: nil)!
        static let l80 = UIImage(named: "battery80", in: .framework, compatibleWith: nil)!
        static let l90 = UIImage(named: "battery90", in: .framework, compatibleWith: nil)!
        static let full = UIImage(named: "battery100", in: .framework, compatibleWith: nil)!
    }
    struct EqualizerButton {
        static let m1 = UIImage(named: "m1", in: .framework, compatibleWith: nil)!.scaled(to: CGSize(width: 34, height: 16))
        static let m2 = UIImage(named: "m2", in: .framework, compatibleWith: nil)!.scaled(to: CGSize(width: 34, height: 16))
        static let m3 = UIImage(named: "m3", in: .framework, compatibleWith: nil)!.scaled(to: CGSize(width: 34, height: 16))
    }
    struct ButtonSegment {
        static let eq = UIImage(named: "buttonSegmentEq", in: .framework, compatibleWith: nil)!.scaled(to: CGSize(width: 13, height: 12))
        static let anc = UIImage(named: "buttonSegmentAnc", in: .framework, compatibleWith: nil)!.scaled(to: CGSize(width: 13, height: 14))
        static let couple = UIImage(named: "buttonSegmentCouple", in: .framework, compatibleWith: nil)!.scaled(to: CGSize(width: 14, height: 9))
    }
}
