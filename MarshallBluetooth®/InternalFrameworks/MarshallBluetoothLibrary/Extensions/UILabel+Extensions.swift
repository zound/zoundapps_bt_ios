//
//  UILabel+Extensions.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 08.09.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

private struct Const {
    static let blinkingTime: TimeInterval = 0.35
    static let alphaMin: CGFloat = 0.2
    static let alphaMax: CGFloat = 1
}

extension UILabel {
    func resizableAttributedFont(text: NSAttributedString, numberOfLines: Int = 1) {
        self.adjustsFontSizeToFitWidth = true
        self.attributedText = text
        self.numberOfLines = numberOfLines
    }
}

extension UILabel {
    func startBlinking() {
        UIView.animate(withDuration: Const.blinkingTime,
                       delay: .zero,
                       options: [.allowUserInteraction, .curveEaseOut, .autoreverse, .repeat],
                       animations: { self.alpha = Const.alphaMin })
    }
    func stopBlinking() {
        layer.removeAllAnimations()
        alpha = Const.alphaMax
    }
    func indicateAnimated(completion: ((Bool) -> Void)? = nil) {
        let fadeInAmination: () -> Void = {
            self.transform = .identity
        }
        transform = .headerScale
        UIView.animate(withDuration: .t025, delay: .zero, options: [.curveEaseInOut, .transitionCrossDissolve],
                       animations: fadeInAmination,
                       completion: completion)
    }
    struct TextTransitionParameters {
        let attributedText: NSAttributedString
        let textAligment: NSTextAlignment
        let lineBreakMode: NSLineBreakMode
        let animationOptions: UIView.AnimationOptions
    }
    func animateTextTransition(with textTransitionPatameters: TextTransitionParameters, completion: ((Bool) -> Void)? = nil) {
        let fadeOutAmination: () -> Void = { [weak self] in
            self?.attributedText = NSAttributedString()
        }
        let fadeInAmination = { [weak self] in
            self?.attributedText = textTransitionPatameters.attributedText
            self?.textAlignment = textTransitionPatameters.textAligment
            self?.lineBreakMode = textTransitionPatameters.lineBreakMode
        }
        let options: UIView.AnimationOptions = textTransitionPatameters.animationOptions
        UIView.transition(with: self, duration: .t010, options: options, animations: fadeOutAmination) { _ in
            UIView.transition(with: self, duration: .t025, options: options, animations: fadeInAmination, completion: completion)
        }
    }
}
