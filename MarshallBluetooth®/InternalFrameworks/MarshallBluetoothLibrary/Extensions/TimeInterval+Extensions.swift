//
//  TimeInterval+Extensions.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 09/12/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

extension TimeInterval {
    static let t010: TimeInterval = 0.1
    static let t025: TimeInterval = 0.25
    static let t050: TimeInterval = 0.5
    static let t075: TimeInterval = 0.75
}
