//
//  Fonts.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 13/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

extension UIFont {

    public static func attributedH0(_ text: String, color: UIColor = Color.text) -> NSAttributedString {
        return UIFont.h0.attributedText(text, color: color, letterSpacing: 0.0, lineHeight: 120.0)
    }
    
    /// Prepare formatted string for "H1" Zeplin Styleguide
    ///
    /// - Parameter text: title to be formatted
    /// - Returns: title with fonts and styles applied
    public static func attributedH1(_ text: String) -> NSAttributedString {
        return UIFont.h1.attributedText(text, color: Color.text, letterSpacing: 2.4, lineHeight: 86.0)
    }
    
    /// Prepare formatted string for "H2" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedH2(_ text: String) -> NSAttributedString {
        return UIFont.h2.attributedText(text, color: Color.text, letterSpacing: 2.4, lineHeight: 55.0)
    }
    
    /// Prepare formatted string for "H3" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedH3(_ text: String, color: UIColor = Color.text) -> NSAttributedString {
        return UIFont.h3.attributedText(text, color: color, letterSpacing: 0.5, lineHeight: 23.0)
    }
    
    /// Prepare formatted string for "H4" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedH4(_ text: String) -> NSAttributedString {
        return UIFont.h4.attributedText(text, color: Color.text, letterSpacing: 0.5, lineHeight: 25.0)
    }
    
    /// Prepare formatted string for "Button" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedButton(_ text: String, color: UIColor = Color.text) -> NSAttributedString {
        return UIFont.button.attributedText(text, color: color, letterSpacing: 1.0, lineHeight: 36.0)
    }
    
    /// Prepare formatted string for "Dropdown" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedDropdown(_ text: String, color: UIColor = Color.dropdown) -> NSAttributedString {
        return UIFont.dropdown.attributedText(text, color: color, letterSpacing: 1.5, lineHeight: 18.0)
    }
    
    /// Prepare formatted string for "DropdownList" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedDropdownList(_ text: String) -> NSAttributedString {
        return UIFont.dropdownList.attributedText(text, color: Color.dropdownList, letterSpacing: 0.0, lineHeight: 18.0)
    }
    
    /// Prepare formatted string for "Body text center" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedBodyTextCenter(_ text: String) -> NSAttributedString {
        return UIFont.bodyTextCenter.attributedText(text, color: Color.text, letterSpacing: 0.5, lineHeight: 41.0)
    }
    
    /// Prepare formatted string for "Body text" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedBodyText(_ text: String) -> NSAttributedString {
        return UIFont.bodyText.attributedText(text, color: Color.text, letterSpacing: 0, lineHeight: 20.5)
    }
    
    /// Prepare formatted string for "Text style" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedTextStyle(_ text: String) -> NSAttributedString {
        return UIFont.textStyle.attributedText(text, color: Color.text, letterSpacing: 0, lineHeight: 41)
    }
    
    /// Prepare formatted string for "Text" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedText(_ text: String, color: UIColor) -> NSAttributedString {
        return UIFont.text.attributedText(text, color: color, letterSpacing: 0.0, lineHeight: 20.5)
    }
    
    /// Prepare formatted string for "Text" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedEmailAddressPlaceholder(_ text: String, color: UIColor) -> NSAttributedString {
        return UIFont.label.attributedText(text, color: color, letterSpacing: 0.0, lineHeight: 20.5)
    }
    public static func attributedEmailAddress(color: UIColor) -> [String : Any] {
        return UIFont.label.attributedEmailTyping(color: color, letterSpacing: 0.0, lineHeight: 20.5)
    }
    public static func attributedLabelText(_ text: String) -> NSAttributedString {
        return UIFont.label.attributedText(text, color: Color.text, letterSpacing: 0.0, lineHeight: 20)
    }
    public static func attributedLabelSubText(_ text: String) -> NSAttributedString {
        return UIFont.artistText.attributedText(text, color: Color.text, letterSpacing: 0.0, lineHeight: 18)
    }
    
    /// Prepare formatted string for "Medium Text" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedMediumText(_ text: String, color: UIColor) -> NSAttributedString {
        return UIFont.mediumText.attributedText(text, color: color, letterSpacing: 0.0, lineHeight: 20.5)
    }
    
    /// Prepare formatted string for "Privacy Policy" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedPrivacyPolicy(_ text: String, color: UIColor) -> NSAttributedString {
        let base = UIFont.smallText.attributedText(text, color: color, letterSpacing: 0.0, lineHeight: 12.5)
        let result = NSMutableAttributedString(attributedString: base)
        let paragraphStyle = NSMutableParagraphStyle.init()
        paragraphStyle.alignment = .center
        result.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRangeFromString(text))
        return result
    }
    
    
    /// Prepare formatted string for "Title" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedTitle(_ text: String, color: UIColor = Color.text) -> NSAttributedString {
        return UIFont.title.attributedText(text, color: color, letterSpacing: 0.5, lineHeight: 21.0)
    }
    
    public static func attributedTextForShareDataConfirm(_ text: String, color: UIColor = Color.text) -> NSAttributedString {
        return UIFont.menuText.attributedText(text, color: color, letterSpacing: 0.0, lineHeight: 17.0)
    }
    
    /// Prepare formatted string for "Medium title" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedMediumTitle(_ text: String) -> NSAttributedString {
        return UIFont.mediumTitle.attributedText(text, color: Color.text, letterSpacing: 1.2, lineHeight: 27.5)
    }
    
    /// Prepare formatted string for "Large title" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedLargeTitle(_ text: String) -> NSAttributedString {
        return UIFont.largeTitle.attributedText(text, color: Color.text, letterSpacing: 1.2, lineHeight: 33.0)
    }
    
    /// Prepare formatted string for "SKIP" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedSkip(_ text: String, color: UIColor = Color.text) -> NSAttributedString {
        return UIFont.skip.attributedText(text, color: color, letterSpacing: 1.5, lineHeight: 15.0)
    }
    
    /// Prepare formatted string for "Menu text" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedMainMenuText(_ text: String) -> NSAttributedString {
        return UIFont.menuText.attributedText(text, color: Color.text, letterSpacing: 1.0, lineHeight: 17.0)
    }
    
    /// Prepare formatted string for "Menu text" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedSubMenuText(_ text: String) -> NSAttributedString {
        return UIFont.menuText.attributedText(text, color: Color.text, letterSpacing: 0.0, lineHeight: 17.0)
    }
    
    /// Prepare formatted string for "Menu text" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributeDisabledSubMenuText(_ text: String) -> NSAttributedString {
        return UIFont.menuText.attributedText(text, color: UIColor.red, letterSpacing: 0.0, lineHeight: 17.0)
    }
    
    /// Prepare formatted string for "Small text" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedSmallText(_ text: String) -> NSAttributedString {
        return UIFont.smallText.attributedText(text, color: Color.text, letterSpacing: 0.3, lineHeight: 23.0)
    }

    public static func attributedSmallerText(_ text: String, color: UIColor = Color.text) -> NSAttributedString {
        return UIFont.smallerText.attributedText(text, color: color, letterSpacing: 0.0, lineHeight: 15.0)
    }
    
    /// Prepare formatted string for "Small text" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedRenameValidationText(_ text: String) -> NSAttributedString {
        return UIFont.smallText.attributedText(text, color: Color.renameValidationText, letterSpacing: 0.0, lineHeight: 23.0)
    }
    
    /// Prepare formatted string for "Small button" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedSmallButton(_ text: String) -> NSAttributedString {
        return UIFont.smallButton.attributedText(text, color: Color.text, letterSpacing: 0.3, lineHeight: 23.0)
    }
    
    /// Prepare formatted string for "Menu label" Zeplin Styleguide
    ///
    /// - Parameter text: subtitle to be formatted
    /// - Returns: subtitle with fonts and styles applied
    public static func attributedMenuLabel(_ text: String) -> NSAttributedString {
        return UIFont.menuLabel.attributedText(text, color: Color.text, letterSpacing: 0.0, lineHeight: 24.5)
    }
    
    public static func attributedTrackLabel(_ text: String) -> NSAttributedString {
        return UIFont.trackText.attributedText(text, color: Color.text, letterSpacing: 0.6, lineHeight:  21.0)
    }
    
    public static func attributedAlbumLabel(_ text: String) -> NSAttributedString {
        return UIFont.albumText.attributedText(text, color: Color.text, letterSpacing: 0.0, lineHeight:  14.0)
    }
    
    public static func attributedArtistLabel(_ text: String) -> NSAttributedString {
        return UIFont.artistText.attributedText(text, color: Color.text, letterSpacing: 0.0, lineHeight:  14.0)
    }
    
    public static func attributedCircularProgressLabel(_ text: String, size: CGFloat? = nil) -> NSAttributedString {
        return preferredFont(style: .headline, size: size).attributedText(text, color: Color.text)
    }
    
    public static func otaProgressLabel(_ text: String, size: CGFloat = 14) -> NSAttributedString {
        return preferredFont(style: .callout, size: size).attributedText(text, color: Color.text)
    }
    
    public static func attributedLabel(_ text: String, color: UIColor = Color.text) -> NSAttributedString {
        return UIFont.label.attributedText(text, color: color, letterSpacing: 0.0)
    }
    
    public static func attributedInfoText(_ text: String) -> NSAttributedString {
        return UIFont.infoText.attributedText(text, color: Color.text, letterSpacing: 1.0, lineHeight: 25.5)
    }
    
    public static func attributedNumber(_ text: String) -> NSAttributedString {
        return UIFont.number.attributedText(text, color: Color.text, letterSpacing: 0.0)
    }
    
    /// Create proper font with attributes for Terms And Conditions agreement sentence
    ///
    /// - Parameter text: Translated user agreement sentence
    /// - Returns: Formatted string with proper fonts and attributes
    public static func attributeTermsAndConditionsAgreement(_ text: String) -> NSAttributedString {
        let attributedText = preferredFont(style: .callout, size: 10.0).attributedText(text, color: Color.text)
        let underlinedText = NSMutableAttributedString(attributedString: attributedText)
        underlinedText.addAttribute(NSAttributedStringKey.underlineStyle, value: 1, range: NSMakeRange(15, 20))
        
        return underlinedText
    }
    
    public static func attributedTermsAndConditionsContent(_ text: String) -> NSAttributedString {
        return UIFont.bodyText.attributedText(text, color: Color.text)
    }
    
    public static func listDeviceNameFont(connected : Bool) -> UIFont {
        let small = UIScreen.main.bounds.size.width < 375
        
        switch (connected, small) {
        case (true, true):
            return preferredFont(style: .headline, size: 15.0)
        case (true, false):
            return preferredFont(style: .title1, size: 17.0)
        case (false, true):
            return preferredFont(style: .headline, size: 15.0)
        case (false, false):
            return preferredFont(style: .title1, size: 17.0)
        }
    }
    
    public static func attributedListDeviceNameLabel(_ text: String, connected: Bool) -> NSAttributedString {
        return connected ?
            listDeviceNameFont(connected: connected).attributedText(text, color: Color.text) :
            listDeviceNameFont(connected: connected).attributedText(text, color: Color.text.withAlphaComponent(0.5))
    }
    
    public static func attributedListDeviceConnectionStatus(_ text: String, connected: Bool) -> NSAttributedString {
        return connected ?
            preferredFont(style: .callout, size: 12.0).attributedText(text, color: Color.text) :
            preferredFont(style: .callout, size: 12.0).attributedText(text, color: Color.text.withAlphaComponent(0.5))
    }
    public static func attributedBatteryLabelStatus(_ text: String) -> NSAttributedString {
        return preferredFont(style: .callout, size: 12.0).attributedText(text, color: Color.text)
    }
    public static func deviceNameLabelConnectedLargeScreen(_ text: String, size: CGFloat? = nil) -> NSAttributedString {
        return preferredFont(style: .title1, size: size).attributedText(text, color: Color.text)
    }
    
    public static func deviceNameLabelConnectedSmallScreen(_ text: String, size: CGFloat? = nil) -> NSAttributedString {
        return preferredFont(style: .title1, size: size).attributedText(text, color: Color.text)
    }
    
    public static func deviceNameLabelDisconnectedLargeScreen(_ text: String, size: CGFloat? = nil) -> NSAttributedString {
        return preferredFont(style: .title1, size: size).attributedText(text, color: Color.text)
    }
    
    public static func deviceNameLabelDisconnectedSmallScreen(_ text: String, size: CGFloat? = nil) -> NSAttributedString {
        return preferredFont(style: .title1, size: size).attributedText(text, color: Color.text)
    }
    
    public static func deviceNameLabelActiveSmallScreen(_ text: String, size: CGFloat? = nil) -> NSAttributedString {
        return preferredFont(style: .title1, size: size).attributedText(text, color: Color.text)
    }
    
    public static func preferredFont(style: UIFontTextStyle, size: CGFloat? = nil) -> UIFont {
        var defaultSize: CGFloat
        var fontName: String = ""
        
        switch(style) {
        case UIFontTextStyle.title1:
            defaultSize = 45
            fontName = "RobotoCondensed-Bold"
        case UIFontTextStyle.headline:
            defaultSize = 17
            fontName = "RobotoCondensed-Regular"
        case UIFontTextStyle.callout:
            defaultSize = 12
            fontName = "RobotoCondensed-Light"
        default:
            defaultSize = 17
        }
        
        return UIFont(name: fontName, size: size ?? defaultSize) ?? UIFont.systemFont(ofSize: size ?? defaultSize)
    }
    
    public func attributedText(_ text: String,
                               color: UIColor = UIColor.black,
                               letterSpacing: CGFloat = 0,
                               lineHeight: CGFloat = 0,
                               lineSpacing: CGFloat = 0.0,
                               shadow: Bool = false) -> NSAttributedString {
        
        var attributes:[NSAttributedStringKey : Any] = [NSAttributedStringKey.font:self,
                                                        NSAttributedStringKey.foregroundColor: color]
        if letterSpacing != 0 {
            attributes[NSAttributedStringKey.kern] = letterSpacing
        }
        
        if lineSpacing != 0 {
            let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
            paragraphStyle.lineSpacing = lineSpacing
            if lineHeight != 0.0 {
                paragraphStyle.maximumLineHeight = lineHeight
                paragraphStyle.minimumLineHeight = lineHeight
            }
            attributes[NSAttributedStringKey.paragraphStyle] = paragraphStyle
        }
        
        if shadow {
            let shadowObject = NSShadow()
            shadowObject.shadowOffset = CGSize(width: 0.5, height: 1.0)
            shadowObject.shadowBlurRadius = 1.0
            shadowObject.shadowColor = UIColor(white: 0.0, alpha: 0.5)
            attributes[NSAttributedStringKey.shadow] = shadowObject
        }
        
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    public func attributedEmailTyping(color: UIColor = UIColor.black,
                                      letterSpacing: CGFloat = 0,
                                      lineHeight: CGFloat = 0,
                                      lineSpacing: CGFloat = 0.0) -> [String : Any] {
        
        var attributes: [String : Any] = [NSAttributedStringKey.font.rawValue: self,
                                          NSAttributedStringKey.foregroundColor.rawValue: color]
        
        if letterSpacing != 0 {
            attributes[NSAttributedStringKey.kern.rawValue] = letterSpacing
        }
        
        let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.alignment = .center
        
        if lineSpacing != 0 {
            paragraphStyle.lineSpacing = lineSpacing
        }
        
        if lineHeight != 0.0 {
            paragraphStyle.maximumLineHeight = lineHeight
            paragraphStyle.minimumLineHeight = lineHeight
        }
        
        attributes[NSAttributedStringKey.paragraphStyle.rawValue] = paragraphStyle
        return attributes
    }
}


