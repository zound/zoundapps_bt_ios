//
//  Forms.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 21/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

/// Create particular input style for input-like view's
///
/// - Parameter input: UITextField that user can use to input some text (for example: e-mail address)
public func applyFormInputStyle(_ input: UITextField) {
    input.font = UIFont.text
    input.textColor = Color.text
    input.backgroundColor = .clear
    input.borderStyle = .none
    input.autocapitalizationType = .none
    input.autocorrectionType = .no
    input.spellCheckingType = .no
}

/// Create display style for Terms And Conditions agreement label
///
/// - Parameter input: UILabel that will display Terms and conditions agreement sentence
public func applyFormTermsAndConditionAgreement(_ input: UILabel) {
    //TODO font
    input.backgroundColor = .clear
    input.layer.borderWidth = 1.0
    input.layer.borderColor = Color.text.cgColor
}
