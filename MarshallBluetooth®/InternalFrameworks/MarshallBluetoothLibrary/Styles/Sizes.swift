//
//  Sizes.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 13/04/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

extension CGSize {
    static func squaredPageIndicator() -> CGSize {
        return CGSize(width: 7.0, height: 7.0)
    }
}

