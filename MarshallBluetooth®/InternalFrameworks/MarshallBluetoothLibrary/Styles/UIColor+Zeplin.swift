//
//  UIColor+Zeplin.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 13.04.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

// Color palette - this extension is copied from Zeplin (generated Swift code)
extension UIColor {
    @nonobjc class var blackOne: UIColor {
        return UIColor(white: 31.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var transparentBlack: UIColor {
        return UIColor(white: 31.0 / 255.0, alpha: 0.0)
    }
    @nonobjc class var semiTransparentBlack: UIColor {
        return UIColor(white: 32.0 / 255.0, alpha: 0.6)
    }
    @nonobjc class var highlight: UIColor {
        return UIColor(white: 143.0 / 255.0, alpha: 0.24)
    }
    @nonobjc class var menuBackground: UIColor {
        return UIColor(red: 20.0 / 255.0, green: 20.0 / 255.0, blue: 20.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var recordbg: UIColor {
        return UIColor(white: 0.0, alpha: 0.46)
    }
    @nonobjc class var white: UIColor {
        return UIColor(white: 1.0, alpha: 1.0)
    }
    @nonobjc class var blackTwo: UIColor {
        return UIColor(white: 27.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var stylebar: UIColor {
        return UIColor(white: 47.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var darkBeige: UIColor {
        return UIColor(red: 173.0 / 255.0, green: 145.0 / 255.0, blue: 92.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var pinkishGrey: UIColor {
        return UIColor(white: 205.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var mediumGrey: UIColor {
        return UIColor(white: 155.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var mediumGrey2: UIColor {
           return UIColor(white: 165.0 / 255.0, alpha: 1.0)
       }
    @nonobjc class var gradientGrey: UIColor {
        return UIColor(white: 108.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var warmGrey: UIColor {
        return UIColor(white: 119.0 / 255.0, alpha: 1.0)
    }

}
