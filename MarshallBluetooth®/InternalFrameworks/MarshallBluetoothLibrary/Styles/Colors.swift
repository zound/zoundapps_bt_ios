//
//  Colors.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 13/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import Zound

public enum Color {
    static let background = UIColor.blackOne
    static let backgroundForWelcome = UIColor.blackTwo
    static let backgroundForMainMenu = UIColor.menuBackground
    static let pageIndicatorActive = UIColor.darkBeige
    static let pageIndicatorInactive = UIColor.white
    static let progressBarTint = UIColor.darkBeige
    static let primaryButton = UIColor.darkBeige
    static let primaryButtonText = UIColor.white
    static let negativeButtonText = UIColor("#B4955C")
    static let switcher = UIColor.darkBeige
    static let checkBox = UIColor.darkBeige
    static let segmentedControl = UIColor.darkBeige
    static let volumeSliderMin = UIColor.darkBeige
    static let volumeSliderMax = UIColor("#646464")
    public static let text = UIColor.white
    public static let textInversed = UIColor.blackOne
    static let textGrayedOut = UIColor.white.withAlphaComponent(0.5)
    static let selectedItemBackground = UIColor.white.withAlphaComponent(0.1)
    static let otaProgressUnfilled = UIColor.white.withAlphaComponent(0.27)
    public static let dropdown = UIColor.darkBeige
    static let dropdownList = UIColor.white
    static let segmentedControlSelected = UIColor.darkBeige
    static let segmentedControlNotSelected = UIColor.mediumGrey
    static let renameValidationText = UIColor.pinkishGrey
    static let safariBarTintColor = UIColor.white
    static let safariControlTintColor = UIColor.black
}
