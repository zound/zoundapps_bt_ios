//
//  Buttons.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 13/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import Zound

extension UIFont {
    public static func attributedPrimaryButtonLabel(_ text: String, size: CGFloat? = nil) -> NSAttributedString {
        return preferredFont(style: .title1, size: 20.0).attributedText(text,
                                                                        color: Color.primaryButtonText,
                                                                        letterSpacing: 0.5,
                                                                        lineSpacing: 0)
    }
    
    public static func attributedNegativeButtonLabel(_ text: String, size: CGFloat? = nil) -> NSAttributedString {
        return preferredFont(style: .title1, size: 20.0).attributedText(text,
                                                                        color: Color.negativeButtonText,
                                                                        letterSpacing: 0.5,
                                                                        lineSpacing: 0)
    }
    
    public static func attributedSecondaryButtonLabel(_ text: String, size: CGFloat? = nil) -> NSAttributedString {
        return preferredFont(style: .title1, size: 12.0).attributedText(text,
                                                                        color: Color.primaryButtonText,
                                                                        letterSpacing: 0.2,
                                                                        lineHeight: 0,
                                                                        lineSpacing: 0.5)
    }

    public static func attributedSkipButtonLabel(_ text: String, size: CGFloat? = nil) -> NSAttributedString {
        return preferredFont(style: .title1, size: 18.0).attributedText(text,
                                                                        color: UIColor.darkBeige,
                                                                        letterSpacing: 0.5,
                                                                        lineHeight: 18,
                                                                        lineSpacing: 0.5)
    }
}
