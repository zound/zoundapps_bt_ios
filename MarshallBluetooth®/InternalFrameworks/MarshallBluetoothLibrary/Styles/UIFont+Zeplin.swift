//
//  UIFont+Zeplin.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 16.04.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

// Font styles - this extension is copied from Zeplin (generated Swift code)
extension UIFont {
    class var h0: UIFont {
        return UIFont(name: "RobotoCondensed-Bold", size: 96.0)!
    }
    class var h1: UIFont {
        return UIFont(name: "RobotoCondensed-Bold", size: 45.0)!
    }
    class var bodyTextCenter: UIFont {
        return UIFont(name: "RobotoCondensed-Light", size: 34.0)!
    }
    class var h3: UIFont {
        return UIFont(name: "RobotoCondensed-Bold", size: 17.0)!
    }
    class var h2: UIFont {
        return UIFont(name: "RobotoCondensed-Bold", size: 25.0)!
    }
    class var h4: UIFont {
        return UIFont(name: "RobotoCondensed-Bold", size: 25.0)!
    }
    class var button: UIFont {
        return UIFont(name: "RobotoCondensed-Bold", size: 36.0)!
    }
    class var dropdown: UIFont {
        return UIFont(name: "RobotoCondensed-Bold", size: 18.0)!
    }
    class var dropdownList: UIFont {
        return UIFont(name: "RobotoCondensed-Bold", size: 17.0)!
    }
    class var bodyText: UIFont {
        return UIFont(name: "RobotoCondensed-Light", size: 17.0)!
    }
    class var textStyle: UIFont {
        return UIFont(name: "RobotoCondensed-Light", size: 34.0)!
    }
    class var menuLabel: UIFont {
        return UIFont(name: "RobotoCondensed-Regular", size: 17.0)!
    }
    class var smallText: UIFont {
        return UIFont(name: "RobotoCondensed-Light", size: 12.0)!
    }
    class var smallButton: UIFont {
        return UIFont(name: "RobotoCondensed-Bold", size: 12.0)!
    }
    class var menuText: UIFont {
        return UIFont(name: "RobotoCondensed-Regular", size: 17.0)!
    }
    class var title: UIFont {
        return UIFont(name: "RobotoCondensed-Bold", size: 17.0)!
    }
    class var largeTitle: UIFont {
        return UIFont(name: "RobotoCondensed-Bold", size: 35.0)!
    }
    class var mediumTitle: UIFont {
        return UIFont(name: "RobotoCondensed-Bold", size: 25.0)!
    }
    class var skip: UIFont {
        return UIFont(name: "RobotoCondensed-Regular", size: 13.0)!
    }
    class var text: UIFont {
        return UIFont(name: "RobotoCondensed-Light", size: 17.0)!
    }
    class var mediumText: UIFont {
        return UIFont(name: "RobotoCondensed-Light", size: 15.0)!
    }
    class var label: UIFont {
        return UIFont(name: "RobotoCondensed-Regular", size: 17.0)!
    }
    class var smallerText: UIFont {
        return UIFont(name: "RobotoCondensed-Regular", size: 13.0)!
    }
    class var textWhite: UIFont {
        return UIFont(name: "RobotoCondensed-Regular", size: 34.0)!
    }
    class var infoText: UIFont {
        return UIFont(name: "RobotoCondensed-Light", size: 17.0)!
    }
    class var number: UIFont {
        return UIFont(name: "RobotoCondensed-Bold", size: 35.0)!
    }
    class var trackText: UIFont {
        return UIFont(name: "RobotoCondensed-Bold", size: 20.0)!
    }
    class var albumText: UIFont {
        return UIFont(name: "RobotoCondensed-Regular", size: 14.0)!
    }
    class var artistText: UIFont {
        return UIFont(name: "RobotoCondensed-Light", size: 14.0)!
    }
}
