//
//  LocalizedStringTest.swift
//  MarshallBluetoothLibraryTests
//

import XCTest
@testable import MarshallBluetoothLibrary

class LocalizedStringTest: XCTestCase {
    fileprivate let mockBundle = MockBundle()
    
    func testLocalizingInGerman() {
        AppEnvironment.pushEnvironment(Environment(language: .de))
    
        XCTAssertEqual("ZUR WEBSITE", localizedString(key: "help.online_manual.button.go_to_website", bundle: mockBundle))
        XCTAssertEqual("Hello", localizedString(key: "_fakeKey_", defaultValue: "Hello", bundle: mockBundle))
        XCTAssertEqual("", localizedString(key: "_fakeKey_", bundle: mockBundle))
        XCTAssertEqual("Verbindung zu BeggenBlack ist abgelaufen. Drücke auf „Erneut verbinden“, um es noch einmal zu versuchen.", localizedString(key: "player.session_lost.timeout", substitutions: ["speakerName" : "BeggenBlack"],  bundle: mockBundle))
    }
}
