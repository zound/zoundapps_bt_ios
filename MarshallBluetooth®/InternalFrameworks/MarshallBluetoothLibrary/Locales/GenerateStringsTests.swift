//
//  MarshallBluetoothLibraryTests.swift
//  MarshallBluetoothLibraryTests
//

import Foundation
import XCTest
@testable import MarshallBluetoothLibrary

var threeLabelJSON =
"""
{
	"all": [
		{ "label": "welcome.buttons.accept",
		  "translations": [
			 { "lang": "en", "translation": "START" },
			 { "lang": "da", "translation": "START" },
			 { "lang": "nl", "translation": "START" },
			 { "lang": "de", "translation": "START" },
			 { "lang": "fr", "translation": "DÉMARRER" },
			 { "lang": "it", "translation": "INIZIO" },
			 { "lang": "ja", "translation": "開始" },
			 { "lang": "nb", "translation": "START" },
			 { "lang": "pt", "translation": "COMEÇAR" },
			 { "lang": "ru", "translation": "НАЧАТЬ" },
			 { "lang": "es", "translation": "INICIO" },
			 { "lang": "sv", "translation": "START" }
		  ]
		},
		{ "label": "welcome.subtitle",
		  "translations": [
			 { "lang": "en", "translation": "Your Marshall Wireless system taps \\ninto over 50 years of experience to bring home that live feeling." },
			 { "lang": "da", "translation": "Dit Marshall Wireless-system har mere end 50 års erfaring at trække på, så du får den bedste live-oplevelse." },
			 { "lang": "nl", "translation": "Je Marshall Wireless-systeem heeft meer dan 50 jaar ervaring in het afspelen van muziek in eigen huis." },
			 { "lang": "de", "translation": "Dein Marshall Wireless System basiert auf über 50 Jahren Erfahrung und bringt dir das Live-Gefühl direkt nach Hause." },
			 { "lang": "fr", "translation": "Votre système Marshall Wireless puise dans plus de 50 ans d'expérience pour vous amener le son d'un concert live à la maison." },
			 { "lang": "it", "translation": "Grazie a competenze sviluppate in oltre mezzo secolo, il sistema senza fili Marshall ti fa vivere direttamente a casa tua un’esperienza live." },
			 { "lang": "ja", "translation": "Marshallワイヤレスシステムは、50年を超える豊富な経験を活かして、ライブでしか味わえなかった臨場感をご家庭にもたらします。" },
			 { "lang": "nb", "translation": "Marshall Wireless-systemet bygger på over 50 års erfaring når det gjelder å få deg til å føle at du er der." },
			 { "lang": "pt", "translation": "O seu sistema Marshall Wireless conta com mais de 50 anos de experiência para conseguir esse sentimento de transmissão em direto." },
			 { "lang": "ru", "translation": "Ваша беспроводная система Marshall Wireless использует весь наш 50-летний опыт, чтобы создать ощущение живого концерта у вас дома." },
			 { "lang": "es", "translation": "Tu sistema Marshall Wireless se beneficia de 50 años de experiencia para traerte esta sensación de la música en directo." },
			 { "lang": "sv", "translation": "Ditt trådlösa Marshall-system drar nytta av 50 års erfarenhet att skapa live-känsla i hemmet." }
		  ]
		},
		{ "label": "welcome.title",
		  "translations": [
			 { "lang": "en", "translation": "WELCOME" },
			 { "lang": "da", "translation": "VELKOMMEN" },
			 { "lang": "nl", "translation": "WELKOM" },
			 { "lang": "de", "translation": "WILLKOMMEN" },
			 { "lang": "fr", "translation": "BIENVENUE" },
			 { "lang": "it", "translation": "BENVENUTO" },
			 { "lang": "ja", "translation": "ようこそ" },
			 { "lang": "nb", "translation": "VELKOMMEN" },
			 { "lang": "pt", "translation": "BEM-VINDO" },
			 { "lang": "ru", "translation": "ДОБРО ПОЖАЛОВАТЬ!" },
			 { "lang": "es", "translation": "BIENVENIDO/A" },
			 { "lang": "sv", "translation": "VÄLKOMMEN" }
		  ]
		}
]}
"""

var smallJSONWithMissingTranslations =
"""
{
	"all": [
		{ "label": "welcome.buttons.accept",
		  "translations": [
			 { "lang": "en", "translation": "START" },
			 { "lang": "de", "translation": "START" },
			 { "lang": "fr", "translation": "DÉMARRER" }
		  ]
		},
		{ "label": "welcome.subtitle",
		  "translations": [
			 { "lang": "en", "translation": "Your Marshall Wireless system taps \\ninto over 50 years of experience to bring home that live feeling." },
			 { "lang": "de", "translation": "Dein Marshall Wireless System basiert auf über 50 Jahren Erfahrung und bringt dir das Live-Gefühl direkt nach Hause." }
		  ]
		},
		{ "label": "welcome.title",
		  "translations": [
			 { "lang": "en", "translation": "WELCOME" },
			 { "lang": "de", "translation": "WILLKOMMEN" },
			 { "lang": "fr", "translation": "BIENVENUE" }
          ]
		}
]}
"""

class GenerateStringsTest: XCTestCase {
    
    let jsonDecoder = JSONDecoder()
    var translations: Translations!
    
    override func setUp() {
        super.setUp()
        let data = threeLabelJSON.data(using: .utf8)
        do {
            translations = try jsonDecoder.decode(Translations.self, from: data!)
        } catch {
            dump(error)
            fatalError()
        }
    }
    
    func testParsedItemCount() {
        XCTAssertTrue(translations.all.count == 3)
    }
    
    func testLanguageFileContents() {
        let data = smallJSONWithMissingTranslations.data(using: .utf8)
        do {
            translations = try jsonDecoder.decode(Translations.self, from: data!)
        } catch {
            dump(error)
            fatalError()
        }

        XCTAssertThrowsError(try checkMissing(translations: translations, for: [.en, .de, .fr]),
                             "") { XCTAssertEqual(($0 as! String), "Translation for label: welcome.subtitle, for language: fr not found") }

        XCTAssertEqual("\"welcome.buttons.accept\" = \"START\";\n\"welcome.subtitle\" = \"Your Marshall Wireless system taps \\ninto over 50 years of experience to bring home that live feeling.\";\n\"welcome.title\" = \"WELCOME\";", translationFileContents(translations, forLanguage: .en))
    }
    
    func testNoFuncArgument() {
        let argumentNames = funcArgumentNames("Connection timed out. Press \"Reconnect\" to try again.")
        XCTAssertTrue(argumentNames == [])
        XCTAssertTrue(funcArguments(argumentNames) == "")
    }
    
    func testSingleFuncArgument() {
        let argumentNames = funcArgumentNames("Connection to %{speakerName} has timed out. Press \"Reconnect\" to try again.")
        XCTAssertTrue(argumentNames == ["speakerName"])
        XCTAssertTrue(funcArguments(argumentNames) == "speakerName: String")
    }
    
    func testMultipleFuncArguments() {
        let argumentNames = funcArgumentNames("Connection to %{speakerName} using %{wifiName} has timed out. Press \"Reconnect\" to try again.")
        XCTAssertTrue(argumentNames == ["speakerName", "wifiName"])
        XCTAssertTrue(funcArguments(argumentNames) == "speakerName: String, wifiName: String")
    }
    
    func testSimpleSubstitutions() {
        let value = "Connection to %{speakerName} using %{wifiName} has timed out. Press \"Reconnect\" to try again."
        XCTAssertTrue("[\"speakerName\": speakerName, \"wifiName\": wifiName]" == funcSubstitutions(value))
    }
    
    override func tearDown() {
        super.tearDown()
    }
}
