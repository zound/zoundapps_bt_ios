//=======================================================================
//
// This file is generated. Do not edit.
//
//=======================================================================

// swiftlint:disable valid_docs
// swiftlint:disable line_length
public enum Strings {
 /**
"Model:"

     - **en**: "Model:"
     - **da**: "Model:"
     - **nl**: "Model:"
     - **de**: "Modell:"
     - **fr**: "Modèle :"
     - **it-IT**: "Modello:"
     - **nb**: "Modell:"
     - **pt-PT**: "Modelo:"
     - **ru**: "Модель:"
     - **es**: "Modelo:"
     - **sv**: "Modell:"
     - **fi**: "Malli:"
     - **zh-Hans**: "型号："
     - **zh-Hant**: "型號："
     - **id**: "Model:"
     - **fil**: "Modelo:"
     - **ja**: "モデル："
 */
  public static func about_this_speaker_screen_model() -> String {
     return localizedString(
         key:"about_this_speaker_screen_model",
         defaultValue:"Model:",
         substitutions: [:])
  }

 /**
"Name:"

     - **en**: "Name:"
     - **da**: "Navn:"
     - **nl**: "Naam:"
     - **de**: "Name:"
     - **fr**: "Nom :"
     - **it-IT**: "Nome:"
     - **nb**: "Navn:"
     - **pt-PT**: "Nome:"
     - **ru**: "Название:"
     - **es**: "Nombre:"
     - **sv**: "Namn:"
     - **fi**: "Nimi:"
     - **zh-Hans**: "名称："
     - **zh-Hant**: "名稱："
     - **id**: "Nama:"
     - **fil**: "Pangalan:"
     - **ja**: "名前："
 */
  public static func about_this_speaker_screen_name() -> String {
     return localizedString(
         key:"about_this_speaker_screen_name",
         defaultValue:"Name:",
         substitutions: [:])
  }

 /**
"Serial Number:"

     - **en**: "Serial Number:"
     - **da**: "Serienummer:"
     - **nl**: "Serienummer:"
     - **de**: "Seriennummer:"
     - **fr**: "Numéro de série :"
     - **it-IT**: "Numero seriale:"
     - **nb**: "Serienummer:"
     - **pt-PT**: "Número de série:"
     - **ru**: "Серийный номер:"
     - **es**: "Número de serie:"
     - **sv**: "Serienummer:"
     - **fi**: "Sarjanumero:"
     - **zh-Hans**: "序列号："
     - **zh-Hant**: "序號："
     - **id**: "Nomor Seri:"
     - **fil**: "Numero ng Serial:"
     - **ja**: "シリアル番号："
 */
  public static func about_this_speaker_screen_serial_number() -> String {
     return localizedString(
         key:"about_this_speaker_screen_serial_number",
         defaultValue:"Serial Number:",
         substitutions: [:])
  }

 /**
"Firmware Version:"

     - **en**: "Firmware Version:"
     - **da**: "Firmware-version:"
     - **nl**: "Firmwareversie:"
     - **de**: "Firmwareversion:"
     - **fr**: "Version du firmware :"
     - **it-IT**: "Versione firmware:"
     - **nb**: "Fastvareversjon:"
     - **pt-PT**: "Versão de firmware:"
     - **ru**: "Версия прошивки:"
     - **es**: "Versión de firmware:"
     - **sv**: "Version av fast programvara:"
     - **fi**: "Laiteohjelmiston versio:"
     - **zh-Hans**: "固件版本："
     - **zh-Hant**: "韌體版本："
     - **id**: "Versi Firmware:"
     - **fil**: "Bersiyon ng Firmware:"
     - **ja**: "ファームウェア バージョン:"
 */
  public static func about_this_speaker_screen_system() -> String {
     return localizedString(
         key:"about_this_speaker_screen_system",
         defaultValue:"Firmware Version:",
         substitutions: [:])
  }

 /**
"1.   Download the Amazon Alexa app.\n\n2.   Launch the app and complete the setup.\n\n3.   Close this screen."

     - **en**: "1.   Download the Amazon Alexa app.\n\n2.   Launch the app and complete the setup.\n\n3.   Close this screen."
     - **da**: "1.   Download Amazon Alexa-appen.\n\n2. Start appen, og fuldend opsætningen.\n\n3. Luk denne skærm."
     - **nl**: "1.   Download de Amazon Alexa-app.\n\n2.   Start de app en voltooi de setup.\n\n3.   Sluit dit scherm."
     - **de**: "1. Lade die Amazon Alexa App herunter.\n\n2. Starte die App und schließe die Einrichtung ab.\nn3. Schließe diesen Bildschirm."
     - **fr**: "1.   Téléchargez l’application Amazon Alexa.\n\n2.   Lancez l’application et terminez la configuration.\n\n3.   Fermez la page."
     - **it-IT**: "1.   Scarica l’app Amazon Alexa.\n\n2.   Avvia l’app e completa la configurazione.\n\n3.   Chiudi questa schermata."
     - **nb**: "1.   Last ned Amazon Alexa-appen.\n\n2.   Åpne appen og fullfør konfigureringen.\n\n3.   Lukk denne skjermen."
     - **pt-PT**: "1.   Faça o download da aplicação Amazon Alexa.\n\n2.   Inicie a aplicação e conclua a definição.\n\n3.   Feche este ecrã."
     - **ru**: "1.   Скачайте приложение Amazon Alexa.\n\n2.   Запустите приложение и выполните настройку.\n\n3.   Закройте этот экран."
     - **es**: "1.   Descarga la aplicación Amazon Alexa.\n\n2.   Abre la aplicación y completa la configuración.\n\n3.   Cierra esta pantalla."
     - **sv**: "1.   Ladda ner Amazon Alexa-appen.\n\n2.   Öppna appen och slutför installationen.\n\n3.   Stäng det här fönstret."
     - **fi**: "1.   Lataa Amazon Alexa -sovellus.\n\n2.   Käynnistä sovellus ja suorita käyttöönotto.\n\n3.   Sulje tämä näkymä."
     - **zh-Hans**: "1.   下载 Amazon Alexa 应用程序。\n\n2.   启动应用程序并完成设置。\n\n3.   关闭此屏幕。"
     - **zh-Hant**: "1.   下載 Amazon Alexa 應用程式。\n\n2.  啟動應用程式並完成設置。\n\n3.   關閉此畫面。"
     - **id**: "1.   Unduh aplikasi Google Assistant.\n\n2.   Jalankan aplikasi dan selesaikan pengaturan.\n\3.   Tutup layar ini."
     - **fil**: "1.   I-download ang Amazon Alexa app.\n\n2.   Ilunsad ang app at kumpletuhin ang setup.\n\n3.   Isara ang screen na ito."
     - **ja**: "1.   Amazon Alexaアプリをダウンロードする。●●2.   アプリを開き、設定を完了する。●●3.   スクリーンを閉じる。"
 */
  public static func amazon_alexa_screen_body() -> String {
     return localizedString(
         key:"amazon_alexa_screen_body",
         defaultValue:"1.   Download the Amazon Alexa app.\n\n2.   Launch the app and complete the setup.\n\n3.   Close this screen.",
         substitutions: [:])
  }

 /**
"Alexa, set volume to 10\n\nAlexa, play new rock music"

     - **en**: "Alexa, set volume to 10\n\nAlexa, play new rock music"
     - **da**: "Alexa, sæt volumen til 10\n\nAlexa, spil ny rockmusik"
     - **nl**: "Alexa, zet volume op 10\n\nAlexa, speel nieuwe rockmuziek af"
     - **de**: "Alexa, stell die Lautstärke auf 10\n\n\nAlexa, spiel neue Rockmusik"
     - **fr**: "Alexa, mets le volume à 10\n\nAlexa, passe du rock récent"
     - **it-IT**: "Alexa, imposta il volume su 10\n\nAlexa, riproduci una nuova musica rock"
     - **nb**: "Alexa, skru lydnivået til 10\n\nAlexa, spill ny rockemusikk"
     - **pt-PT**: "Alexa, põe o volume em 10\n\nAlexa, toca música rock moderna"
     - **ru**: "Alexa, установи громкость на 10\n\nAlexa, воспроизведи рок-музыку"
     - **es**: "Alexa, fija el volumen en 10\n\nAlexa, reproduce música de rock nueva"
     - **sv**: "Alexa, sätt volymen till 10\n\nAlexa, spela ny rockmusik"
     - **fi**: "Alexa, aseta äänenvoimakkuudeksi 10\n\nAlexa ja soita uutta rockmusiikkia"
     - **zh-Hans**: "Alexa，设置音量为 10\n\nAlexa，播放新的摇滚音乐"
     - **zh-Hant**: "Alexa，將音量設定為 10\n\nAlexa，並播放新的搖滾音樂"
     - **id**: "Alexa, atur volume menjadi 10\n\nAlexa, putar musik rock yang baru"
     - **fil**: "Alexa, itakda ang volume sa 10\n\nAlexa, mag-play ng bagong rock na musika"
     - **ja**: "Alexa、ボリュームを10に設定して/n/nAlexa、ロックミュージックを再生して"
 */
  public static func amazon_alexa_screen_cloud() -> String {
     return localizedString(
         key:"amazon_alexa_screen_cloud",
         defaultValue:"Alexa, set volume to 10\n\nAlexa, play new rock music",
         substitutions: [:])
  }

 /**
"Complete the setup to use the M-Button with Amazon Alexa."

     - **en**: "Complete the setup to use the M-Button with Amazon Alexa."
     - **da**: "Fuldend opsætningen for at bruge M-knappen med Amazon Alexa."
     - **nl**: "Voltooi de setup om de M-knop te gebruiken met Amazon Alexa."
     - **de**: "Schließe die Einrichtung ab, um die M-Taste mit Amazon Alexa verwenden zu können."
     - **fr**: "Configurez le bouton M pour l’utiliser avec Amazon Alexa."
     - **it-IT**: "Completa la configurazione per usare il pulsante M con Amazon Alexa."
     - **nb**: "Fullfør oppsettet for å bruke M-knappen med Amazon Alexa."
     - **pt-PT**: "Conclua a definição para usar o botão M com a Amazon Alexa."
     - **ru**: "Выполните настройку для использования M-кнопки с Amazon Alexa."
     - **es**: "Completa la configuración para usar el botón M con Amazon Alexa."
     - **sv**: "Slutför installationen för att använda M-knappen med Amazon Alexa."
     - **fi**: "Suorita käyttöönotto käyttääksesi M-painiketta Amazon Alexan kanssa."
     - **zh-Hans**: "完成设置以将 M-按钮用于 Amazon Alexa。"
     - **zh-Hant**: "完成設置以將 M 按鈕與您的 Amazon Alexa 配合使用。"
     - **id**: "Selesaikan pengaturan untuk menggunakan M-Button dengan Amazon Alexa Anda."
     - **fil**: "Kumpletuhin ang setup upang magamit ang M-Button sa Amazon Alexa."
     - **ja**: "MボタンとAmazon Alexaを使用するために設定を完了する。"
 */
  public static func amazon_alexa_screen_header() -> String {
     return localizedString(
         key:"amazon_alexa_screen_header",
         defaultValue:"Complete the setup to use the M-Button with Amazon Alexa.",
         substitutions: [:])
  }

 /**
"AMAZON ALEXA"

     - **en**: "AMAZON ALEXA"
     - **da**: "AMAZON ALEXA"
     - **nl**: "AMAZON ALEXA"
     - **de**: "AMAZON ALEXA"
     - **fr**: "AMAZON ALEXA"
     - **it-IT**: "AMAZON ALEXA"
     - **nb**: "AMAZON ALEXA"
     - **pt-PT**: "AMAZON ALEXA"
     - **ru**: "AMAZON ALEXA"
     - **es**: "AMAZON ALEXA"
     - **sv**: "AMAZON ALEXA"
     - **fi**: "AMAZON ALEXA"
     - **zh-Hans**: "AMAZON ALEXA"
     - **zh-Hant**: "AMAZON ALEXA"
     - **id**: "AMAZON ALEXA"
     - **fil**: "AMAZON ALEXA"
     - **ja**: "AMAZON ALEXA"
 */
  public static func amazon_alexa_screen_title_uc() -> String {
     return localizedString(
         key:"amazon_alexa_screen_title_uc",
         defaultValue:"AMAZON ALEXA",
         substitutions: [:])
  }

 /**
"Adjust the amount of Monitor transparency."

     - **en**: "Adjust the amount of Monitor transparency."
     - **da**: "Juster mængden af Monitor-gennemsigtighed."
     - **nl**: "Pas de transparantie van de monitor aan."
     - **de**: "Lege die Stärke der Monitor-Transparenz fest."
     - **fr**: "Ajustez le niveau de transparence du mode Monitoring."
     - **it-IT**: "Regola la quantità di trasparenza del monitor."
     - **nb**: "Juster monitorens transparens."
     - **pt-PT**: "Ajuste o nível de transparência do monitor."
     - **ru**: "Выберите уровень прозрачности Monitor."
     - **es**: "Ajusta la cantidad de transparencia para el modo de monitorización."
     - **sv**: "Justera nivå för Monitor."
     - **fi**: "Säädä tarkkailun läpäisevyyttä."
     - **zh-Hans**: "调整监听环境声的通透度。"
     - **zh-Hant**: "調整監控透明度。"
     - **id**: "Atur jumlah transparansi Monitor."
     - **fil**: "I-adjust ang antas ng Monitor transparency."
     - **ja**: "モニターの透明度を調節します。"
 */
  public static func anc_settings_screen_monitoring_subtitle() -> String {
     return localizedString(
         key:"anc_settings_screen_monitoring_subtitle",
         defaultValue:"Adjust the amount of Monitor transparency.",
         substitutions: [:])
  }

 /**
"MON"

     - **en**: "MON"
     - **da**: "MON"
     - **nl**: "MON"
     - **de**: "MON"
     - **fr**: "MON"
     - **it-IT**: "MON"
     - **nb**: "MON"
     - **pt-PT**: "MON"
     - **ru**: "MON"
     - **es**: "MON"
     - **sv**: "MON"
     - **fi**: "TARKKAILU"
     - **zh-Hans**: "监听"
     - **zh-Hant**: "MON"
     - **id**: "MONITOR"
     - **fil**: "MON"
     - **ja**: "MON"
 */
  public static func anc_settings_screen_monitoring_uc() -> String {
     return localizedString(
         key:"anc_settings_screen_monitoring_uc",
         defaultValue:"MON",
         substitutions: [:])
  }

 /**
"Turn off ANC to save power."

     - **en**: "Turn off ANC to save power."
     - **da**: "Sluk ANC for at spare batteri."
     - **nl**: "Schakel ANC uit om stroom te besparen."
     - **de**: "Deaktiviere ANC, um Energie zu sparen."
     - **fr**: "Éteignez la fonction ANC pour économiser de la batterie."
     - **it-IT**: "Spegni ANC per risparmiare la batteria."
     - **nb**: "Slå av ANC for å spare strøm."
     - **pt-PT**: "Desligue o ANC para poupar energia."
     - **ru**: "Для экономии батареи выключите ANC."
     - **es**: "Desactiva la ANC para ahorrar batería."
     - **sv**: "Slå av aktiv brusreducering för att spara batteri."
     - **fi**: "Kytke vastamelu pois käytöstä säästääksesi virtaa."
     - **zh-Hans**: "关闭 ANC 以节省电量。"
     - **zh-Hant**: "關閉 ANC 以節省電源。"
     - **id**: "Matikan ANC untuk menghemat daya."
     - **fil**: "I-off ang ANC upang makatipid ng enerhiya."
     - **ja**: "ANCをオフにして電源を節約します。"
 */
  public static func anc_settings_screen_off_subtitle() -> String {
     return localizedString(
         key:"anc_settings_screen_off_subtitle",
         defaultValue:"Turn off ANC to save power.",
         substitutions: [:])
  }

 /**
"OFF"

     - **en**: "OFF"
     - **da**: "SLUKKET"
     - **nl**: "UIT"
     - **de**: "AUS"
     - **fr**: "ÉTEINT"
     - **it-IT**: "SPENTO"
     - **nb**: "AV"
     - **pt-PT**: "DESLIGADO"
     - **ru**: "ВЫКЛЮЧЕНО"
     - **es**: "APAGADO"
     - **sv**: "AV"
     - **fi**: "POIS PÄÄLTÄ"
     - **zh-Hans**: "关"
     - **zh-Hant**: "關閉"
     - **id**: "MATIKAN"
     - **fil**: "I-OFF"
     - **ja**: "オフ"
 */
  public static func anc_settings_screen_off_uc() -> String {
     return localizedString(
         key:"anc_settings_screen_off_uc",
         defaultValue:"OFF",
         substitutions: [:])
  }

 /**
"Adjust the amount of Active Noice Cancelling."

     - **en**: "Adjust the amount of Active Noice Cancelling."
     - **da**: "Juster mængden af Active Noice Cancelling."
     - **nl**: "Pas de hoeveelheid actieve ruisonderdrukking aan."
     - **de**: "Lege die Stärke der aktiven Geräuschunterdrückung fest."
     - **fr**: "Ajustez le niveau de réduction de bruit active."
     - **it-IT**: "Regola la quantità di Cancellazione del rumore attiva."
     - **nb**: "Juster mengden aktiv støydemping."
     - **pt-PT**: "Ajuste o nível de cancelamento de ruído ativo."
     - **ru**: "Настройте уровень активного шумоподавления."
     - **es**: "Ajusta la cantidad de cancelación activa de ruido."
     - **sv**: "Justera nivå för aktiv brusreducering."
     - **fi**: "Säädä vastamelun määrää."
     - **zh-Hans**: "调整主动噪声消除量。"
     - **zh-Hant**: "調整主動降噪音量。"
     - **id**: "Atur level Active Noice Cancelling."
     - **fil**: "I-adjust ang antas ng Active Noice Cancelling."
     - **ja**: "アクティブノイズキャンセリングの程度を調節します。"
 */
  public static func anc_settings_screen_on_subtitle() -> String {
     return localizedString(
         key:"anc_settings_screen_on_subtitle",
         defaultValue:"Adjust the amount of Active Noice Cancelling.",
         substitutions: [:])
  }

 /**
"ON"

     - **en**: "ON"
     - **da**: "TÆNDT"
     - **nl**: "AAN"
     - **de**: "EIN"
     - **fr**: "EN MARCHE"
     - **it-IT**: "ACCESO"
     - **nb**: "PÅ"
     - **pt-PT**: "LIGADO"
     - **ru**: "ВКЛЮЧЕНО"
     - **es**: "ENCENDIDO"
     - **sv**: "PÅ"
     - **fi**: "PÄÄLLÄ"
     - **zh-Hans**: "开"
     - **zh-Hant**: "開啟"
     - **id**: "NYALAKAN"
     - **fil**: "I-ON"
     - **ja**: "オン"
 */
  public static func anc_settings_screen_on_uc() -> String {
     return localizedString(
         key:"anc_settings_screen_on_uc",
         defaultValue:"ON",
         substitutions: [:])
  }

 /**
"Blocks out wind noise"

     - **en**: "Blocks out wind noise"
     - **da**: "Reducere vindstøj"
     - **nl**: "Blokkeert windgeluiden"
     - **de**: "Filtert Windgeräusche heraus"
     - **fr**: "Bloque le bruit du vent"
     - **it-IT**: "Elimina il rumore del vento"
     - **nb**: "Stenger ute vindstøy"
     - **pt-PT**: "Bloqueia o ruído do vento"
     - **ru**: "Блокирует шум ветра"
     - **es**: "Bloquea el ruido del viento"
     - **sv**: "Stänger ute ljud från vind"
     - **fi**: "Sulkee pois tuulen äänen"
     - **zh-Hans**: "隔绝风噪"
     - **zh-Hant**: "阻擋風聲"
     - **id**: "Memblokir derau angin"
     - **fil**: "Harangan ang ingay ng hangin"
     - **ja**: "ウインドノイズをブロックする"
 */
  public static func anc_settings_screen_outdoor_mode_footer() -> String {
     return localizedString(
         key:"anc_settings_screen_outdoor_mode_footer",
         defaultValue:"Blocks out wind noise",
         substitutions: [:])
  }

 /**
"Outdoor Mode"

     - **en**: "Outdoor Mode"
     - **da**: "Udendørs mode"
     - **nl**: "Buitenmodus"
     - **de**: "Außenmodus"
     - **fr**: "Mode Outdoor"
     - **it-IT**: "Modalità outdoor"
     - **nb**: "Utendørsmodus"
     - **pt-PT**: "Modo Outdoor"
     - **ru**: "Уличный режим"
     - **es**: "Modo aire libre"
     - **sv**: "Läget utomhus"
     - **fi**: "Ulkoilutila"
     - **zh-Hans**: "户外模式"
     - **zh-Hant**: "戶外模式"
     - **id**: "Mode Luar Ruangan"
     - **fil**: "Outdoor na Mode"
     - **ja**: "アウトドアモード"
 */
  public static func anc_settings_screen_outdoor_mode_title() -> String {
     return localizedString(
         key:"anc_settings_screen_outdoor_mode_title",
         defaultValue:"Outdoor Mode",
         substitutions: [:])
  }

 /**
"Decouple"

     - **en**: "Decouple"
     - **da**: "Skil ad"
     - **nl**: "Ontkoppelen"
     - **de**: "Entkoppeln"
     - **fr**: "Découpler"
     - **it-IT**: "Separa"
     - **nb**: "Koble fra"
     - **pt-PT**: "Desemparelhar"
     - **ru**: "Разъединить"
     - **es**: "Desacoplar"
     - **sv**: "Koppla isär"
     - **fi**: "Poista laitepari"
     - **zh-Hans**: "解除配对"
     - **zh-Hant**: "取消配對"
     - **id**: "Pisahkan"
     - **fil**: "Alisin sa pagkakapares"
     - **ja**: "デカップリング"
 */
  public static func android_device_settings_menu_item_decouple() -> String {
     return localizedString(
         key:"android_device_settings_menu_item_decouple",
         defaultValue:"Decouple",
         substitutions: [:])
  }

 /**
"Go to Settings on this device, turn on Location and try again."

     - **en**: "Go to Settings on this device, turn on Location and try again."
     - **da**: "Gå til Indstillinger på denne enhed, aktivér Placering og prøv igen."
     - **nl**: "Ga naar Instellingen op dit apparaat, schakel Locatie in en probeer het opnieuw."
     - **de**: "Gehe in die Einstellungen für dieses Gerät, schalte die Standortdienste ein und versuche es erneut."
     - **fr**: "Accédez aux paramètres de cet appareil, activez la Localisation et réessayez."
     - **it-IT**: "Vai alle Impostazioni del dispositivo, attiva la localizzazione e prova di nuovo."
     - **nb**: "Gå til Innstillinger på denne enheten, slå på Lokasjon og prøv igjen."
     - **pt-PT**: "Vá a Definições neste dispositivo, ligue o Localizador e tente novamente."
     - **ru**: "Перейдите в меню «Параметры» на этом устройстве, включите определение местоположения и попробуйте еще раз."
     - **es**: "Abre los ajustes de este dispositivo, activa la ubicación y vuelve a intentarlo."
     - **sv**: "Gå till Inställningar på den här enheten, aktivera Platstjänsten och försök igen."
     - **fi**: "Siirry laitteen asetuksiin, aseta sijainti päälle ja yritä uudelleen."
     - **zh-Hans**: "前往设备的“设置”部分，打开位置并重试。"
     - **zh-Hant**: "前往此裝置的「設定」，開啟「位置」然後再試一次。"
     - **id**: "Buka Pengaturan dalam perangkat ini, nyalakan LOKASI, dan coba lagi."
     - **fil**: "Pumunta sa Mga Setting sa device na ito, i-on ang Lokasyon at subukan muli."
     - **ja**: "デバイスの［設定］に進み、位置情報をオンにしてからもう一度試してください。"
 */
  public static func android_error_no_location_subtitle() -> String {
     return localizedString(
         key:"android_error_no_location_subtitle",
         defaultValue:"Go to Settings on this device, turn on Location and try again.",
         substitutions: [:])
  }

 /**
"LOCATION IS OFF"

     - **en**: "LOCATION IS OFF"
     - **da**: "PLACERING ER DEAKTIVERET"
     - **nl**: "LOCATIE IS UITGESCHAKELD"
     - **de**: "STANDORTDIENSTE SIND DEAKTIVIERT"
     - **fr**: "LOCALISATION DÉSACTIVÉE"
     - **it-IT**: "LA LOCALIZZAZIONE È DISATTIVATA"
     - **nb**: "LOKASJON ER AV"
     - **pt-PT**: "O LOCALIZADOR ESTÁ DESLIGADO"
     - **ru**: "ОБНАРУЖЕНИЕ МЕСТОПОЛОЖЕНИЯ ОТКЛЮЧЕНО"
     - **es**: "LA UBICACIÓN ESTÁ DESACTIVADA"
     - **sv**: "PLATSTJÄNSTEN ÄR AVSTÄNGD"
     - **fi**: "SIJAINTI ON POIS PÄÄLTÄ"
     - **zh-Hans**: "定位关闭"
     - **zh-Hant**: "位置為關閉"
     - **id**: "LOKASI MATI"
     - **fil**: "NAKA-OFF ANG LOKASYON"
     - **ja**: "位置情報がオフです"
 */
  public static func android_error_no_location_title_uc() -> String {
     return localizedString(
         key:"android_error_no_location_title_uc",
         defaultValue:"LOCATION IS OFF",
         substitutions: [:])
  }

 /**
"WE NEED ACCESS TO YOUR LOCATION\n\nSince we use Bluetooth® Low Energy (BLE) to communicate with your Marshall product, location services must be enabled on your phone."

     - **en**: "WE NEED ACCESS TO YOUR LOCATION\n\nSince we use Bluetooth® Low Energy (BLE) to communicate with your Marshall product, location services must be enabled on your phone."
     - **da**: ""
     - **nl**: ""
     - **de**: ""
     - **fr**: ""
     - **it-IT**: ""
     - **nb**: ""
     - **pt-PT**: ""
     - **ru**: ""
     - **es**: ""
     - **sv**: ""
     - **fi**: ""
     - **zh-Hans**: ""
     - **zh-Hant**: ""
     - **id**: ""
     - **fil**: ""
     - **ja**: ""
 */
  public static func android_home_permission_localization() -> String {
     return localizedString(
         key:"android_home_permission_localization",
         defaultValue:"WE NEED ACCESS TO YOUR LOCATION\n\nSince we use Bluetooth® Low Energy (BLE) to communicate with your Marshall product, location services must be enabled on your phone.",
         substitutions: [:])
  }

 /**
"You should allow storage to make the app running"

     - **en**: "You should allow storage to make the app running"
     - **da**: "Du skal give adgang til hukommelse for at appen kan fungere."
     - **nl**: "U moet opslag toestaan om de app te kunnen laten draaien."
     - **de**: "Du musst Speicherplatz freigeben, um diese App zu verwenden."
     - **fr**: "Vous devez accepter le stockage pour que l’application fonctionne."
     - **it-IT**: "Consenti la memorizzazione per far funzionare l’app."
     - **nb**: "Du bør tillate lagring for å få appen til å kjøre."
     - **pt-PT**: "Tem de permitir que a memória corra a app."
     - **ru**: "Надо разрешить памяти запустить приложение."
     - **es**: "Debes tener espacio suficiente para que funcione la aplicación."
     - **sv**: "Du bör tillåta lagring för att appen ska kunna användas."
     - **fi**: "Sovelluksen toiminta edellyttää pääsyä tallennustilaan."
     - **zh-Hans**: "您应该允许存储使应用程序运行。"
     - **zh-Hant**: "您應該允許儲存，讓應用程式得以執行。"
     - **id**: "Anda harus mengizinkan penyimpanan agar aplikasi dapat berjalan."
     - **fil**: "Dapat mong hayaan ang storage na panatilihing gumagana ang app."
     - **ja**: "ストレージでアプリを実行できるよう設定してください。"
 */
  public static func android_home_permission_storage() -> String {
     return localizedString(
         key:"android_home_permission_storage",
         defaultValue:"You should allow storage to make the app running",
         substitutions: [:])
  }

 /**
"About"

     - **en**: "About"
     - **da**: "Om"
     - **nl**: "Over"
     - **de**: "Über"
     - **fr**: "À propos"
     - **it-IT**: "Informazioni"
     - **nb**: "Om"
     - **pt-PT**: "Sobre"
     - **ru**: "О системе"
     - **es**: "Acerca de"
     - **sv**: "Om"
     - **fi**: "Yleistä"
     - **zh-Hans**: "关于音箱"
     - **zh-Hant**: "關於"
     - **id**: "Tentang"
     - **fil**: "Tungkol sa"
     - **ja**: "バージョン情報"
 */
  public static func appwide_about() -> String {
     return localizedString(
         key:"appwide_about",
         defaultValue:"About",
         substitutions: [:])
  }

 /**
"ABOUT"

     - **en**: "ABOUT"
     - **da**: "OM"
     - **nl**: "OVER"
     - **de**: "ÜBER"
     - **fr**: "À PROPOS"
     - **it-IT**: "INFORMAZIONI"
     - **nb**: "OM"
     - **pt-PT**: "SOBRE"
     - **ru**: "О СИСТЕМЕ"
     - **es**: "ACERCA DE"
     - **sv**: "OM"
     - **fi**: "YLEISTÄ"
     - **zh-Hans**: "关于音箱"
     - **zh-Hant**: "關於"
     - **id**: "TENTANG"
     - **fil**: "TUNGKOL SA"
     - **ja**: "バージョン情報"
 */
  public static func appwide_about_uc() -> String {
     return localizedString(
         key:"appwide_about_uc",
         defaultValue:"ABOUT",
         substitutions: [:])
  }

 /**
"ACTIVATE"

     - **en**: "ACTIVATE"
     - **da**: "AKTIVER"
     - **nl**: "ACTIVEER"
     - **de**: "AKTIVIEREN"
     - **fr**: "ACTIVER"
     - **it-IT**: "ATTIVA"
     - **nb**: "AKTIVERE"
     - **pt-PT**: "ATIVAR"
     - **ru**: "АКТИВИРОВАТЬ"
     - **es**: "ACTIVAR"
     - **sv**: "AKTIVERA"
     - **fi**: "AKTIVOI"
     - **zh-Hans**: "激活"
     - **zh-Hant**: "啟用"
     - **id**: "AKTIFKAN"
     - **fil**: "PAGANAHIN"
     - **ja**: "有効にする"
 */
  public static func appwide_activate_uc() -> String {
     return localizedString(
         key:"appwide_activate_uc",
         defaultValue:"ACTIVATE",
         substitutions: [:])
  }

 /**
"ACTIVATED"

     - **en**: "ACTIVATED"
     - **da**: "AKTIVERET"
     - **nl**: "GEACTIVEERD"
     - **de**: "AKTIVIERT"
     - **fr**: "ACTIVÉ"
     - **it-IT**: "ATTIVATA"
     - **nb**: "AKTIVERT"
     - **pt-PT**: "ATIVADO"
     - **ru**: "АКТИВИРОВАН"
     - **es**: "ACTIVADO"
     - **sv**: "AKTIVERAT"
     - **fi**: "AKTIVOITU"
     - **zh-Hans**: "已激活"
     - **zh-Hant**: "已啟用"
     - **id**: "DIAKTIFKAN"
     - **fil**: "GUMAGANA"
     - **ja**: "有効"
 */
  public static func appwide_activated_uc() -> String {
     return localizedString(
         key:"appwide_activated_uc",
         defaultValue:"ACTIVATED",
         substitutions: [:])
  }

 /**
"CANCEL"

     - **en**: "CANCEL"
     - **da**: "ANNULLER"
     - **nl**: "ANNULEREN"
     - **de**: "ABBRECHEN"
     - **fr**: "ANNULER"
     - **it-IT**: "ANNULLA"
     - **nb**: "AVBRYT"
     - **pt-PT**: "CANCELAR"
     - **ru**: "ОТМЕНИТЬ"
     - **es**: "CANCELAR"
     - **sv**: "AVBRYT"
     - **fi**: "PERUUTA"
     - **zh-Hans**: "取消"
     - **zh-Hant**: "取消"
     - **id**: "BATAL"
     - **fil**: "KANSELAHIN"
     - **ja**: "キャンセル"
 */
  public static func appwide_cancel_uc() -> String {
     return localizedString(
         key:"appwide_cancel_uc",
         defaultValue:"CANCEL",
         substitutions: [:])
  }

 /**
"CONNECT"

     - **en**: "CONNECT"
     - **da**: "FORBIND"
     - **nl**: "VERBINDEN"
     - **de**: "VERBINDEN"
     - **fr**: "CONNECTER"
     - **it-IT**: "CONNETTI"
     - **nb**: "KOBLE TIL"
     - **pt-PT**: "LIGAR"
     - **ru**: "ПОДКЛЮЧИТЬ"
     - **es**: "CONECTAR"
     - **sv**: "ANSLUT"
     - **fi**: "YHDISTÄ"
     - **zh-Hans**: "点击连接"
     - **zh-Hant**: "連線"
     - **id**: "HUBUNGKAN"
     - **fil**: "IKONEKTA"
     - **ja**: "接続"
 */
  public static func appwide_connect_uc() -> String {
     return localizedString(
         key:"appwide_connect_uc",
         defaultValue:"CONNECT",
         substitutions: [:])
  }

 /**
"DONE"

     - **en**: "DONE"
     - **da**: "FÆRDIG"
     - **nl**: "KLAAR"
     - **de**: "FERTIG"
     - **fr**: "TERMINÉ"
     - **it-IT**: "FATTO"
     - **nb**: "FERDIG"
     - **pt-PT**: "CONCLUÍDO"
     - **ru**: "ГОТОВО"
     - **es**: "HECHO"
     - **sv**: "KLART"
     - **fi**: "VALMISTA"
     - **zh-Hans**: "完成"
     - **zh-Hant**: "完成"
     - **id**: "SELESAI"
     - **fil**: "TAPOS NA"
     - **ja**: "完了"
 */
  public static func appwide_done_uc() -> String {
     return localizedString(
         key:"appwide_done_uc",
         defaultValue:"DONE",
         substitutions: [:])
  }

 /**
"GO TO SETTINGS"

     - **en**: "GO TO SETTINGS"
     - **da**: "GÅ TIL INDSTILLINGER"
     - **nl**: "GA NAAR INSTELLINGEN"
     - **de**: "ZU DEN EINSTELLUNGEN"
     - **fr**: "ACCÉDER AUX PARAMÈTRES"
     - **it-IT**: "VAI ALLE IMPOSTAZIONI"
     - **nb**: "GÅ TIL INNSTILLINGER"
     - **pt-PT**: "IR PARA AS DEFINIÇÕES"
     - **ru**: "ПЕРЕЙТИ К НАСТРОЙКАМ"
     - **es**: "IR A AJUSTES"
     - **sv**: "GÅ TILL INSTÄLLNINGAR"
     - **fi**: "SIIRRY ASETUKSIIN"
     - **zh-Hans**: "前往设置"
     - **zh-Hant**: "前往設定"
     - **id**: "BUKA PENGATURAN"
     - **fil**: "PUMUNTA SA MGA SETTING"
     - **ja**: "設定に移動"
 */
  public static func appwide_go_to_settings_uc() -> String {
     return localizedString(
         key:"appwide_go_to_settings_uc",
         defaultValue:"GO TO SETTINGS",
         substitutions: [:])
  }

 /**
"GO TO WEBSITE"

     - **en**: "GO TO WEBSITE"
     - **da**: "GÅ TIL HJEMMESIDE"
     - **nl**: "GA NAAR WEBSITE"
     - **de**: "ZUR WEBSITE"
     - **fr**: "ACCÉDER AU SITE WEB"
     - **it-IT**: "VAI AL SITO"
     - **nb**: "GÅ TIL NETTSTEDET"
     - **pt-PT**: "IR PARA O SITE"
     - **ru**: "ПЕРЕЙТИ НА ВЕБ-САЙТ"
     - **es**: "IR AL SITIO WEB"
     - **sv**: "GÅ TILL WEBBPLATSEN"
     - **fi**: "SIIRRY KOTISIVULLE"
     - **zh-Hans**: "访问网站"
     - **zh-Hant**: "前往網站"
     - **id**: "BUKA SITUS WEB"
     - **fil**: "PUMUNTA SA WEBSITE"
     - **ja**: "ウェブサイトに移動"
 */
  public static func appwide_go_to_website_uc() -> String {
     return localizedString(
         key:"appwide_go_to_website_uc",
         defaultValue:"GO TO WEBSITE",
         substitutions: [:])
  }

 /**
"NEXT"

     - **en**: "NEXT"
     - **da**: "NÆSTE"
     - **nl**: "VOLGENDE"
     - **de**: "WEITER"
     - **fr**: "SUIVANT"
     - **it-IT**: "AVANTI"
     - **nb**: "NESTE"
     - **pt-PT**: "SEGUINTE"
     - **ru**: "ДАЛЕЕ"
     - **es**: "SIGUIENTE"
     - **sv**: "NÄSTA"
     - **fi**: "SEURAAVA"
     - **zh-Hans**: "下一步"
     - **zh-Hant**: "下一步"
     - **id**: "BERIKUTNYA"
     - **fil**: "SUSUNOD"
     - **ja**: "次へ"
 */
  public static func appwide_next_uc() -> String {
     return localizedString(
         key:"appwide_next_uc",
         defaultValue:"NEXT",
         substitutions: [:])
  }

 /**
"SKIP"

     - **en**: "SKIP"
     - **da**: "SPRING OVER"
     - **nl**: "OVERSLAAN"
     - **de**: "ÜBERSPRINGEN"
     - **fr**: "IGNORER"
     - **it-IT**: "SALTA"
     - **nb**: "HOPP OVER"
     - **pt-PT**: "IGNORAR"
     - **ru**: "ПРОПУСТИТЬ"
     - **es**: "SALTAR"
     - **sv**: "HOPPA ÖVER"
     - **fi**: "OHITA"
     - **zh-Hans**: "跳过"
     - **zh-Hant**: "略過"
     - **id**: "LEWATI"
     - **fil**: "LAKTAWAN"
     - **ja**: "スキップ"
 */
  public static func appwide_skip_uc() -> String {
     return localizedString(
         key:"appwide_skip_uc",
         defaultValue:"SKIP",
         substitutions: [:])
  }

 /**
"TRY AGAIN"

     - **en**: "TRY AGAIN"
     - **da**: "PRØV IGEN"
     - **nl**: "NOGMAALS PROBEREN"
     - **de**: "ERNEUT VERSUCHEN"
     - **fr**: "RÉESSAYER"
     - **it-IT**: "RIPROVA"
     - **nb**: "PRØV IGJEN"
     - **pt-PT**: "TENTAR NOVAMENTE"
     - **ru**: "ПОПРОБОВАТЬ ЕЩЕ РАЗ"
     - **es**: "VOLVER A INTENTARLO"
     - **sv**: "FÖRSÖK IGEN"
     - **fi**: "YRITÄ UUDELLEEN"
     - **zh-Hans**: "重试"
     - **zh-Hant**: "重試"
     - **id**: "COBA LAGI"
     - **fil**: "SUBUKAN MULI"
     - **ja**: "もう一度試してください"
 */
  public static func appwide_try_again_uc() -> String {
     return localizedString(
         key:"appwide_try_again_uc",
         defaultValue:"TRY AGAIN",
         substitutions: [:])
  }

 /**
"Unknown"

     - **en**: "Unknown"
     - **da**: "Ukendt"
     - **nl**: "Onbekend"
     - **de**: "Unbekannt"
     - **fr**: "Inconnu"
     - **it-IT**: "Non identificato"
     - **nb**: "Ukjent"
     - **pt-PT**: "Desconhecido"
     - **ru**: "Неизвестно"
     - **es**: "Desconocido"
     - **sv**: "Okänt"
     - **fi**: "Tuntematon"
     - **zh-Hans**: "未知"
     - **zh-Hant**: "未知"
     - **id**: "Tidak diketahui"
     - **fil**: "Di-alam"
     - **ja**: "不明"
 */
  public static func appwide_unknown() -> String {
     return localizedString(
         key:"appwide_unknown",
         defaultValue:"Unknown",
         substitutions: [:])
  }

 /**
"UPDATE HEADPHONES"

     - **en**: "UPDATE HEADPHONES"
     - **da**: "OPDATER HOVEDTELEFONER"
     - **nl**: "HOOFDTELEFOON BIJWERKEN"
     - **de**: "KOPFHÖRER AKTUALISIEREN"
     - **fr**: "MISE À JOUR DU CASQUE"
     - **it-IT**: "AGGIORNARE LE CUFFIE"
     - **nb**: "OPPDATER HODETELEFONER"
     - **pt-PT**: "ATUALIZAR AUSCULTADORES"
     - **ru**: "ОБНОВИТЬ НАУШНИКИ"
     - **es**: "ACTUALIZAR AURICULARES"
     - **sv**: "UPPDATERA HÖRLURAR"
     - **fi**: "PÄIVITÄ KUULOKKEET"
     - **zh-Hans**: "更新耳机"
     - **zh-Hant**: "更新耳機"
     - **id**: "PEMBARUAN HEADPHONE"
     - **fil**: "I-UPDATE ANG MGA HEADPHONE"
     - **ja**: "ヘッドフォンを更新"
 */
  public static func appwide_update_headphones_uc() -> String {
     return localizedString(
         key:"appwide_update_headphones_uc",
         defaultValue:"UPDATE HEADPHONES",
         substitutions: [:])
  }

 /**
"UPDATE SPEAKER"

     - **en**: "UPDATE SPEAKER"
     - **da**: "OPDATER HØJTTALER"
     - **nl**: "LUIDSPREKER UPDATEN"
     - **de**: "LAUTSPRECHER AKTUALISIEREN"
     - **fr**: "METTRE À JOUR L’ENCEINTE"
     - **it-IT**: "AGGIORNA IL DIFFUSORE"
     - **nb**: "OPPDATER HØYTTALER"
     - **pt-PT**: "ATUALIZAR ALTIFALANTE"
     - **ru**: "ОБНОВИТЬ АКУСТИЧ. СИСТЕМУ"
     - **es**: "ACTUALIZAR ALTAVOZ"
     - **sv**: "UPPDATERA HÖGTALARE"
     - **fi**: "PÄIVITÄ KAIUTIN"
     - **zh-Hans**: "更新音箱"
     - **zh-Hant**: "更新喇叭"
     - **id**: "MEMPERBARUI SPEAKER"
     - **fil**: "I-UPDATE ANG SPEAKER"
     - **ja**: "スピーカーの更新"
 */
  public static func appwide_update_speaker_uc() -> String {
     return localizedString(
         key:"appwide_update_speaker_uc",
         defaultValue:"UPDATE SPEAKER",
         substitutions: [:])
  }

 /**
"START TIMER"

     - **en**: "START TIMER"
     - **da**: "START TIMER"
     - **nl**: "TIMER STARTEN"
     - **de**: "TIMER STARTEN"
     - **fr**: "LANCER LE MINUTEUR"
     - **it-IT**: "AVVIA TIMER"
     - **nb**: "START TIDSUR"
     - **pt-PT**: "INICIAR TEMPORIZADOR"
     - **ru**: "ВКЛЮЧЕНИЕ ТАЙМЕРА"
     - **es**: "EMPEZAR TEMPORIZADOR"
     - **sv**: "STARTA TIMER"
     - **fi**: "KÄYNNISTÄ AJASTIN"
     - **zh-Hans**: "启动计时器"
     - **zh-Hant**: "啟動計時器"
     - **id**: "NYALAKAN PENGATUR WAKTU"
     - **fil**: "UMPISAHAN ANG TIMER"
     - **ja**: "タイマー開始"
 */
  public static func auto_off_timer_button_start_uc() -> String {
     return localizedString(
         key:"auto_off_timer_button_start_uc",
         defaultValue:"START TIMER",
         substitutions: [:])
  }

 /**
"STOP TIMER"

     - **en**: "STOP TIMER"
     - **da**: "STOP TIMER"
     - **nl**: "TIMER STOPPEN"
     - **de**: "TIMER ANHALTEN"
     - **fr**: "ARRÊTER LE MINUTEUR"
     - **it-IT**: "FERMA TIMER"
     - **nb**: "STOPP TIDSUR"
     - **pt-PT**: "TEMPO DE PARAGEM"
     - **ru**: "ВЫКЛЮЧЕНИЕ ТАЙМЕРА"
     - **es**: "DETENER TEMPORIZADOR"
     - **sv**: "STOPPA TIMER"
     - **fi**: "PYSÄYTÄ AJASTIN"
     - **zh-Hans**: "停止计时器"
     - **zh-Hant**: "停止計時器"
     - **id**: "HENTIKAN PENGATUR WAKTU"
     - **fil**: "IHINTO ANG TIMER"
     - **ja**: "タイマー停止"
 */
  public static func auto_off_timer_button_stop_uc() -> String {
     return localizedString(
         key:"auto_off_timer_button_stop_uc",
         defaultValue:"STOP TIMER",
         substitutions: [:])
  }

 /**
"Hours"

     - **en**: "Hours"
     - **da**: "Timer"
     - **nl**: "Uur"
     - **de**: "Stunden"
     - **fr**: "Heures"
     - **it-IT**: "Ore"
     - **nb**: "Timer"
     - **pt-PT**: "Horas"
     - **ru**: "Часы"
     - **es**: "Horas"
     - **sv**: "Timmar"
     - **fi**: "Tuntia"
     - **zh-Hans**: "小时"
     - **zh-Hant**: "小時"
     - **id**: "Jam"
     - **fil**: "Oras"
     - **ja**: "時間"
 */
  public static func auto_off_timer_screen_hours() -> String {
     return localizedString(
         key:"auto_off_timer_screen_hours",
         defaultValue:"Hours",
         substitutions: [:])
  }

 /**
"Minutes"

     - **en**: "Minutes"
     - **da**: "Minutter"
     - **nl**: "Minuten"
     - **de**: "Minuten"
     - **fr**: "Minutes"
     - **it-IT**: "Minuti"
     - **nb**: "Minutter"
     - **pt-PT**: "Minutos"
     - **ru**: "Минуты"
     - **es**: "Minutos"
     - **sv**: "Minuter"
     - **fi**: "Minuuttia"
     - **zh-Hans**: "分钟"
     - **zh-Hant**: "分鐘"
     - **id**: "Menit"
     - **fil**: "Minuto"
     - **ja**: "分"
 */
  public static func auto_off_timer_screen_minutes() -> String {
     return localizedString(
         key:"auto_off_timer_screen_minutes",
         defaultValue:"Minutes",
         substitutions: [:])
  }

 /**
"Turn off my headphones after"

     - **en**: "Turn off my headphones after"
     - **da**: "Sluk for mine hovedtelefoner efter"
     - **nl**: "Schakel mijn hoofdtelefoon uit na"
     - **de**: "Meinen Kopfhörer ausschalten nach"
     - **fr**: "Éteindre mon casque au bout de"
     - **it-IT**: "Spegni le cuffie dopo"
     - **nb**: "Slå av hodetelefonene etter"
     - **pt-PT**: "Desligar os meus auscultadores depois"
     - **ru**: "Отключить наушники после"
     - **es**: "Apagar mis auriculares después de"
     - **sv**: "Stäng av mina hörlurar efter"
     - **fi**: "Aika kuulokkeideni kytkeytymiseen pois päältä"
     - **zh-Hans**: "在以下时间后关闭我的耳机"
     - **zh-Hant**: "在此時間後關閉我的耳機"
     - **id**: "Matikan headphone saya setelah"
     - **fil**: "I-off ang aking mga headphone pagkatapos"
     - **ja**: "ヘッドフォンをオフにするまで"
 */
  public static func auto_off_timer_screen_subtitle_off() -> String {
     return localizedString(
         key:"auto_off_timer_screen_subtitle_off",
         defaultValue:"Turn off my headphones after",
         substitutions: [:])
  }

 /**
"Your headphones will turn off in..."

     - **en**: "Your headphones will turn off in..."
     - **da**: "Dine hovedtelefoner slukkes om ..."
     - **nl**: "Je hoofdtelefoon zal worden uitgeschakeld over..."
     - **de**: "Dein Kopfhörer schaltet sich aus in …"
     - **fr**: "Votre casque s’éteindra dans…"
     - **it-IT**: "Le cuffie si spegneranno tra…"
     - **nb**: "Hodetelefonene slår seg av om…"
     - **pt-PT**: "Os seus auscultadores serão desligados dentro de..."
     - **ru**: "Наушники отключатся через…"
     - **es**: "Tus auriculares se apagarán en…"
     - **sv**: "Dina hörlurar stängs av om…"
     - **fi**: "Aika kuulokkeidesi kytkeytymiseen pois päältä…"
     - **zh-Hans**: "您的耳机将在以下时间后关闭..."
     - **zh-Hant**: "您的耳機將在此時間後關閉..."
     - **id**: "Headphone Anda akan mati dalam..."
     - **fil**: "Mag-o-off ang iyong mga headphone sa loob ng..."
     - **ja**: "ヘッドフォンがオフになるまで…"
 */
  public static func auto_off_timer_screen_subtitle_on() -> String {
     return localizedString(
         key:"auto_off_timer_screen_subtitle_on",
         defaultValue:"Your headphones will turn off in...",
         substitutions: [:])
  }

 /**
"Do not forget activate Bluetooth on\n%s"

     - **en**: "Do not forget activate Bluetooth on\n%s"
     - **da**: "Husk at aktivere Bluetooth på\n%s"
     - **nl**: "Vergeet niet Bluetooth te activeren op\n%s"
     - **de**: "Vergiss nicht, Bluetooth auf\n%s zu aktivieren"
     - **fr**: "N’oubliez pas d’activer le Bluetooth sur\n%s"
     - **it-IT**: "Non dimenticarti di attivare il Bluetooth su\n%s"
     - **nb**: "Ikke glem å aktivere Bluetooth på\n%s"
     - **pt-PT**: "Não se esqueça de ativar o Bluetooth em\n%s"
     - **ru**: "Проверьте активацию Bluetooth на \n%s"
     - **es**: "No olvides activar el Bluetooth de tu\n%s"
     - **sv**: "Glöm inte att aktivera Bluetooth på\n%s"
     - **fi**: "Muistathan kytkeä \n%s:n Bluetoothin päälle."
     - **zh-Hans**: "请勿忘记激活\n%s 上的蓝牙"
     - **zh-Hant**: "不要忘記啟用 \n%s 上的 Bluetooth。"
     - **id**: "Jangan lupa mengaktifkan Bluetooth pada\n%s"
     - **fil**: "Huwag kalimutang paganahin ang Bluetooth sa\n%s"
     - **ja**: "●%sでBluetoothを有効にするのを忘れないで下さい"
 */
  public static func bluetooth_body() -> String {
     return localizedString(
         key:"bluetooth_body",
         defaultValue:"Do not forget activate Bluetooth on\n%s",
         substitutions: [:])
  }

 /**
"Do not forget activate Bluetooth on %s"

     - **en**: "Do not forget activate Bluetooth on %s"
     - **da**: "Husk at aktivere Bluetooth på %s"
     - **nl**: "Vergeet niet Bluetooth te activeren op %s"
     - **de**: "Vergiss nicht, Bluetooth auf %s zu aktivieren"
     - **fr**: "N’oubliez pas d’activer le Bluetooth sur %s"
     - **it-IT**: "Non dimenticarti di attivare il Bluetooth su %s"
     - **nb**: "Ikke glem å aktivere Bluetooth på %s"
     - **pt-PT**: "Não se esqueça de ativar o Bluetooth em %s"
     - **ru**: "Проверьте активацию Bluetooth на \n%s"
     - **es**: "No olvides activar el Bluetooth de tu %s"
     - **sv**: "Glöm inte att aktivera Bluetooth på %s"
     - **fi**: "Muistathan kytkeä %s:n Bluetoothin päälle."
     - **zh-Hans**: "请勿忘记激活 %s 上的蓝牙"
     - **zh-Hant**: "不要忘記啟用 \n%s 上的 Bluetooth。"
     - **id**: "Jangan lupa mengaktifkan Bluetooth pada %s"
     - **fil**: "Huwag kalimutang paganahin ang Bluetooth sa\n%s"
     - **ja**: "%sでBluetoothを有効にするのを忘れないで下さい"
 */
  public static func bluetooth_body_v1() -> String {
     return localizedString(
         key:"bluetooth_body_v1",
         defaultValue:"Do not forget activate Bluetooth on %s",
         substitutions: [:])
  }

 /**
"%s\n is currently not playing anything"

     - **en**: "%s\n is currently not playing anything"
     - **da**: "%s\n afspiller ikke noget nu"
     - **nl**: "%s\n speelt op dit moment niets af"
     - **de**: "%s\n spielt zurzeit nichts ab"
     - **fr**: "%s\n n’est actuellement pas en cours de lecture"
     - **it-IT**: "%s\n non sta riproducendo nulla al momento"
     - **nb**: "%s\n spiller for øyeblikket ikke noe"
     - **pt-PT**: "%s\n de momento não está a reproduzir nada"
     - **ru**: "%s\n сейчас ничего не воспроизводит"
     - **es**: "%s\n no está reproduciendo nada en este momento"
     - **sv**: "%s\n spelar inte någonting för närvarande"
     - **fi**: "%s\n ei toista tällä hetkellä mitään"
     - **zh-Hans**: "%s\n  当前未播放任何内容"
     - **zh-Hant**: "%s\n 目前沒有播放任何內容。"
     - **id**: "%s\n saat ini tidak memutar apa pun"
     - **fil**: "%s\n ay kasalukuyang hindi nagpe-play ng kahit ano"
     - **ja**: "%s●は現在何も再生していません"
 */
  public static func bluetooth_not_playing() -> String {
     return localizedString(
         key:"bluetooth_not_playing",
         defaultValue:"%s\n is currently not playing anything",
         substitutions: [:])
  }

 /**
"Connected to %s"

     - **en**: "Connected to %s"
     - **da**: "Forbundet til%s"
     - **nl**: "Verbonden met %s"
     - **de**: "Verbunden mit %s"
     - **fr**: "Connecté à %s"
     - **it-IT**: "Connesso a %s"
     - **nb**: "Koblet til %s"
     - **pt-PT**: "Ligado a %s"
     - **ru**: "Подключено к %s"
     - **es**: "Conectado a %s"
     - **sv**: "Ansluten till %s"
     - **fi**: "Yhdistetty kohteeseen %s"
     - **zh-Hans**: "已连接至 %s"
     - **zh-Hant**: "已連接至 %s"
     - **id**: "Terhubung ke %s"
     - **fil**: "Nakakonekta sa %s"
     - **ja**: "%sに接続しました"
 */
  public static func bluetooth_screen_subtitle_connected() -> String {
     return localizedString(
         key:"bluetooth_screen_subtitle_connected",
         defaultValue:"Connected to %s",
         substitutions: [:])
  }

 /**
"Connected to \n%s"

     - **en**: "Connected to \n%s"
     - **da**: "Tilsluttet til \n%s"
     - **nl**: "Verbonden met \n%s"
     - **de**: "Mit \n%s verbunden"
     - **fr**: "Connecté à \n%s"
     - **it-IT**: "Connesso a \n%s"
     - **nb**: "Tilkoblet \n%s"
     - **pt-PT**: "Ligado a \n%s"
     - **ru**: "Подключено к \n%s"
     - **es**: "Conectado a \n%s"
     - **sv**: "Ansluten till \n%s"
     - **fi**: "Liitetty \n%s"
     - **zh-Hans**: "已连接到 \n%s"
     - **zh-Hant**: "連接至 \n%s"
     - **id**: "Terhubung ke \n%s"
     - **fil**: "Nakakonekta sa \n%s"
     - **ja**: "●%sに接続されました"
 */
  public static func bluetooth_screen_subtitle_connected_v1() -> String {
     return localizedString(
         key:"bluetooth_screen_subtitle_connected_v1",
         defaultValue:"Connected to \n%s",
         substitutions: [:])
  }

 /**
"CONTACT SUPPORT"

     - **en**: "CONTACT SUPPORT"
     - **da**: "KONTAKT SUPPORT"
     - **nl**: "CONTACT OPNEMEN MET ONDERSTEUNING"
     - **de**: "SUPPORT KONTAKTIEREN"
     - **fr**: "CONTACTER L’ASSISTANCE"
     - **it-IT**: "CONTATTA L’ASSISTENZA"
     - **nb**: "KONTAKT KUNDESTØTTE"
     - **pt-PT**: "CONTACTAR O APOIO TÉCNICO"
     - **ru**: "КОНТАКТЫ СЛУЖБЫ ПОДДЕРЖКИ"
     - **es**: "CONTACTAR CON ASISTENCIA"
     - **sv**: "KONTAKTA SUPPORT"
     - **fi**: "OTA YHTEYTTÄ TUKEEN"
     - **zh-Hans**: "帮助"
     - **zh-Hant**: "連絡客戶支援"
     - **id**: "HUBUNGI DUKUNGAN"
     - **fil**: "KONTAKIN ANG SUPORTA"
     - **ja**: "サポートに連絡"
 */
  public static func contact_screen_contact_support_uc() -> String {
     return localizedString(
         key:"contact_screen_contact_support_uc",
         defaultValue:"CONTACT SUPPORT",
         substitutions: [:])
  }

 /**
"Visit our website at www.marshallheadphones.com or contact our support."

     - **en**: "Visit our website at www.marshallheadphones.com or contact our support."
     - **da**: "Besøg vores hjemmeside på www.marshallheadphones.com eller kontakt vores support."
     - **nl**: "Bezoek onze website op www.marshallheadphones.com of neem contact op met onze ondersteuning."
     - **de**: "Besuche unsere Website unter www.marshallheadphones.com oder kontaktiere unseren Support."
     - **fr**: "Consultez notre site Web à l’adresse www.marshallheadphones.com ou contactez notre service d’assistance."
     - **it-IT**: "Visita il nostro sito web www.marshallheadphones.com o contatta l’assistenza."
     - **nb**: "Besøk nettstedet vårt på www.marshallheadphones.com eller kontakt kundestøtte."
     - **pt-PT**: "Visite o nosso site em www.marshallheadphones.com ou entre em contacto com o nosso apoio técnico."
     - **ru**: "Посетите наш веб-сайт www.marshallheadphones.com или обратитесь в нашу службу поддержки."
     - **es**: "Visita nuestra página web www.marshallheadphones.com o contacta con nuestro servicio de asistencia."
     - **sv**: "Du kan besöka vår webbplats på www.marshallheadphones.com eller kontakta vår support."
     - **fi**: "Käy verkkosivuillamme osoitteessa www.marshallheadphones.com tai ota yhteyttä tukipalveluun."
     - **zh-Hans**: "请访问我们的网站 www.marshallheadphones.com 或浏览我们的帮助页面。"
     - **zh-Hant**: "造訪我們的網站 www.marshallheadphones.com，或聯絡我們的支援部。"
     - **id**: "Kunjungi situs web kami di www.marshallheadphones.com atau hubungi dukungan kami."
     - **fil**: "Pumunta sa aming website sa www.marshallheadphones.com o makipag-ugnay sa suporta."
     - **ja**: "ウェブサイトwww.marshallheadphones.com.にアクセスするか、サポートまでご連絡ください。"
 */
  public static func contact_screen_subtitle() -> String {
     return localizedString(
         key:"contact_screen_subtitle",
         defaultValue:"Visit our website at www.marshallheadphones.com or contact our support.",
         substitutions: [:])
  }

 /**
"Decoupling..."

     - **en**: "Decoupling..."
     - **da**: "Adskiller..."
     - **nl**: "Ontkoppelen..."
     - **de**: "Entkoppeln …"
     - **fr**: "Découplage…"
     - **it-IT**: "Separazione..."
     - **nb**: "Kobler fra …"
     - **pt-PT**: "A desemparelhar…"
     - **ru**: "Идет отсоединение…"
     - **es**: "Desemparejando…"
     - **sv**: "Kopplar bort …"
     - **fi**: "Pariliitosta puretaan..."
     - **zh-Hans**: "正在去耦... "
     - **zh-Hant**: "正在取消配對... "
     - **id**: "Melepaskan pemasangan..."
     - **fil**: "Decoupling..."
     - **ja**: "カップリング解除中... "
 */
  public static func couple_screen_decouple_spinner() -> String {
     return localizedString(
         key:"couple_screen_decouple_spinner",
         defaultValue:"Decoupling...",
         substitutions: [:])
  }

 /**
"YES I'M SURE"

     - **en**: "YES I'M SURE"
     - **da**: "JA, JEG ER SIKKER"
     - **nl**: "JA, IK WEET HET ZEKER"
     - **de**: "JA, ICH BIN SICHER."
     - **fr**: "OUI, JE SUIS SÛR(E)"
     - **it-IT**: "SÌ, SONO SICURO"
     - **nb**: "JA, JEG ER SIKKER"
     - **pt-PT**: "SIM, TENHO A CERTEZA"
     - **ru**: "ДА, УВЕРЕН(-А)"
     - **es**: "SÍ, LO TENGO CLARO"
     - **sv**: "JA, JAG ÄR SÄKER"
     - **fi**: "KYLLÄ, OLEN VARMA"
     - **zh-Hans**: "确定"
     - **zh-Hant**: "是，我確定"
     - **id**: "YA, SAYA YAKIN"
     - **fil**: "OO, SIGURADO AKO"
     - **ja**: "はい、続行します"
 */
  public static func couple_screen_heads_up_button() -> String {
     return localizedString(
         key:"couple_screen_heads_up_button",
         defaultValue:"YES I'M SURE",
         substitutions: [:])
  }

 /**
"Make sure you are only connected to %s and stay disconnected from %s in the Settings menu."

     - **en**: "Make sure you are only connected to %s and stay disconnected from %s in the Settings menu."
     - **da**: "Sørg for, at du kun er forbundet med %s, og ikke forbinder med %s i menuen Indstillinger."
     - **nl**: "Zorg ervoor dat u alleen verbonden bent met %s en dat u niet verbonden bent met %s in het menu Instellingen."
     - **de**: "Stelle über das Einstellungsmenü sicher, dass du nur mit %s verbunden bist und keine Verbindung zu %s aufgebaut wird."
     - **fr**: "Assurez-vous de n’être connecté qu’à la %s et de bien rester déconnecté de la %s dans le menu Paramètres."
     - **it-IT**: "Assicurati di essere collegato solo a %s e scollegato da %s tramite il menu Impostazioni."
     - **nb**: "Sørg for at du kun er koblet til %s, og hold deg frakoblet %s i Innstillingsmenyen."
     - **pt-PT**: "Assegure-se de que está apenas ligado a %s e que se mantém desligado de %s no menu Definições."
     - **ru**: "Проверьте, что вы подключены только к %s, и отключитесь от %s в меню «Параметры»."
     - **es**: "Asegúrate de que solo estás conectado a %s y desconectado de %s en el menú de la configuración."
     - **sv**: "Kontrollera i inställningsmenyn att du enbart är ansluten till %s och att du är bortkopplad från %s."
     - **fi**: "Varmista Asetukset-valikosta, että yhteys kaiuttimeen %s on luotu ja että yhteys kaiuttimeen %s on katkaistu."
     - **zh-Hans**: "在设置菜单中，现已与%s连接，将与%s断开。"
     - **zh-Hant**: "請確保只連接至 %s，且在「設定」功能表中保持與 %s 中斷連線。"
     - **id**: "Pastikan Anda hanya terhubung dengan %s dan tidak terhubung dengan %s pada menu Pengaturan."
     - **fil**: "Siguraduhin mong nakakonekta ka lamang sa %s at manatiling nakadiskonekta mula sa %s sa menu ng Mga Setting."
     - **ja**: "設定メニューで%sのみに接続しており、%sからは切断されていることを確認してください。"
 */
  public static func couple_screen_heads_up_subtitle() -> String {
     return localizedString(
         key:"couple_screen_heads_up_subtitle",
         defaultValue:"Make sure you are only connected to %s and stay disconnected from %s in the Settings menu.",
         substitutions: [:])
  }

 /**
"HEADS UP!"

     - **en**: "HEADS UP!"
     - **da**: "BEMÆRK!"
     - **nl**: "LET OP!"
     - **de**: "AUFGEPASST!"
     - **fr**: "ATTENTION !"
     - **it-IT**: "ATTENZIONE!"
     - **nb**: "OBS!"
     - **pt-PT**: "MANTENHA-SE LIGADO!"
     - **ru**: "АНОНС!"
     - **es**: "¡ATENCIÓN!"
     - **sv**: "OBS!"
     - **fi**: "HUOMIO!"
     - **zh-Hans**: "提示"
     - **zh-Hant**: "注意！"
     - **id**: "PERINGATAN!"
     - **fil**: "ANUNSIYO!"
     - **ja**: "警告! "
 */
  public static func couple_screen_heads_up_title_uc() -> String {
     return localizedString(
         key:"couple_screen_heads_up_title_uc",
         defaultValue:"HEADS UP!",
         substitutions: [:])
  }

 /**
"Ambient"

     - **en**: "Ambient"
     - **da**: "Omsluttende"
     - **nl**: "Ambient"
     - **de**: "Umgebung"
     - **fr**: "Ambient"
     - **it-IT**: "Ambiente"
     - **nb**: "Omgivelse"
     - **pt-PT**: "Ambiente"
     - **ru**: "Пространственный"
     - **es**: "Ambiente"
     - **sv**: "Ambient"
     - **fi**: "Ambient"
     - **zh-Hans**: "环境模式"
     - **zh-Hant**: "環境"
     - **id**: "Ambient"
     - **fil**: "Ambient"
     - **ja**: "Ambient"
 */
  public static func couple_screen_select_mode_ambient() -> String {
     return localizedString(
         key:"couple_screen_select_mode_ambient",
         defaultValue:"Ambient",
         substitutions: [:])
  }

 /**
"Ambient mode links two speaker to play the same source synchronised. Each speaker plays both the left and right channels.\n\nStereo mode links two speakers as a stereo pair. One speaker is the left channel and the other the right."

     - **en**: "Ambient mode links two speaker to play the same source synchronised. Each speaker plays both the left and right channels.\n\nStereo mode links two speakers as a stereo pair. One speaker is the left channel and the other the right."
     - **da**: "Med omsluttende tilstand forbindes til højttalerne, så de afspiller samme lyd synkroniseret. Hver højttaler afspiller både venstre og højre kanal.\n\nMed stereotilstand forbindes to højttalere som ét stereopar. Hvor én højttaler afspiller venstre kanal, og den anden afspiller højre."
     - **nl**: "In de ambient-modus worden twee luidsprekers verbonden zodat ze dezelfde bron gesynchroniseerd afspelen. Elke luidspreker speelt zowel het linker- als het rechterkanaal.\n\nIn de stereo-modus worden twee luidsprekers als stereopaar aan elkaar gekoppeld. Eén luidspreker is het linkerkanaal en de andere is het rechterkanaal."
     - **de**: "Der Umgebungsmodus verbindet zwei Lautsprecher, die synchron denselben Ton wiedergeben. Jeder Lautsprecher gibt sowohl den linken als auch den rechten Kanal wieder.\n\nDer Stereomodus verbindet zwei gleich große Lautsprecher als Stereopaar miteinander. Dabei gibt ein Lautsprecher den linken und der andere den rechten Kanal wieder."
     - **fr**: "Le mode Ambient relie les enceintes qui lisent la même source de manière synchronisée. Chaque enceinte lit à la fois le canal droit et le canal gauche.\n\nLe mode Stéréo relie deux enceintes en tant que paire stéréo, avec une enceinte comme canal droit et l’autre comme canal gauche."
     - **it-IT**: "In modalità ambiente due diffusori sono collegati per riprodurre lo stesso audio simultaneamente. Entrambi riproducono quindi sia il canale destro che quello sinistro.\n\nIn modalità stereo due diffusori sono collegati come una coppia stereo: uno riproduce il canale sinistro e l’altro il canale destro."
     - **nb**: "Omgivelsesmodus kobler to høyttalere og spiller av samme kilde synkronisert. Hver høyttaler spiller både venstre og høyre kanal.\n\nStereomodus forbinder to høyttalere som et stereopar. En høyttaler er den venstre kanalen og den andre den høyre."
     - **pt-PT**: "O modo ambiente liga a altifalantes para reproduzirem a mesma fonte sincronizada. Cada altifalante reproduz tanto o canal esquerdo como o direito.\n\nO modo estéreo liga dois altifalantes como um par de estéreo. Um altifalante reproduz o áudio do canal direito e o outro do canal esquerdo."
     - **ru**: "Пространственный режим: колонки воспроизводят один и тот же синхронизированный источник. Каждая колонка проигрывает как левый, так и правый канал.\n\nСтереорежим объединяет две колонки в стереопару: одна проигрывает левый канал, а другая — правый."
     - **es**: "Modo ambiente: dos altavoces reproducen el mismo sonido de forma sincronizada. Cada altavoz reproduce ambos canales, el izquierdo y el derecho.\n\nModo estéreo: dos altavoces se emparejan en modo estéreo. Un altavoz emite el canal izquierdo y el otro el derecho."
     - **sv**: "Ambient-läge kopplar två högtalare att spela upp samma ljud synkroniserat. Varje högtalare spelar både vänster- och högerkanalerna.\n\nStereo-läge kopplar två högtalare av samma storlek som ett stereopar. Den ena högtalaren kopplas till den vänstra kanalen och den andra till den högra."
     - **fi**: "Ambient-tila yhdistää kaksi kaiutinta soittamaan synkronoidusti samaa lähdettä. Kumpikin kaiutin toistaa sekä vasemman- että oikeanpuolista kanavaa.\n\nStereo-tila yhdistää kaksi kaiutinta stereo-pariksi. Yksi kaiutin toistaa vasenta ja toinen kaiutin oikeaa kanavaa."
     - **zh-Hans**: "环境模式连接两台音箱，同步播放相同的音源。每台音箱均在左右声道播放。\n\n立体声模式连接两台音箱，构成一对立体声音箱。一台音箱为左声道，另一台为右声道。"
     - **zh-Hant**: "環境模式會連結喇叭以同步播放相同的音訊，每個喇叭均播放左聲道和右聲道。\n\n立體聲模式會連結兩個喇叭，當成一組立體聲音響，一個喇叭為左聲道，另一個喇叭為右聲道。"
     - **id**: "Mode Ambient tersambung ke speaker untuk memutar audio yang sama melalui sinkronisasi. Setiap speaker memutar saluran kiri dan kanan.\n\nMode stereo memasangkan dua speaker sebagai pasangan stereo, dengan satu speaker sebagai saluran kiri dan yang lain sebagai saluran kanan."
     - **fil**: "Kumokonekta ang ambient mode sa mga speaker para patugtugin ang parehong pagkukunan nang magkasabay. Ang bawat speaker ay tumutugtog ng parehong kaliwa at kanang channel.\n\nKumokonekta ang stereo mode sa dalawang speaker bilang isang pares ng stereo. Ang isang speaker ay ang kaliwang channel at ang isa ay ang kanan."
     - **ja**: "Ambient Modeは2つのスピーカーをつないで、同じ入力源をシンクロナイズさせて再生します。各スピーカーは左と右、両方のチャンネルから再生します。\n\nStereo Modeは2つのスピーカーをステレオペアとしてつなぎます。一つのスピーカーが左チャンネルを担い、もう一つが右チャンネルを担います。"
 */
  public static func couple_screen_select_mode_ambient_and_stereo_desc() -> String {
     return localizedString(
         key:"couple_screen_select_mode_ambient_and_stereo_desc",
         defaultValue:"Ambient mode links two speaker to play the same source synchronised. Each speaker plays both the left and right channels.\n\nStereo mode links two speakers as a stereo pair. One speaker is the left channel and the other the right.",
         substitutions: [:])
  }

 /**
"Ambient mode links two speakers to play the same source synchronised. Each speaker plays both the left and right channels.\n\nChoose this mode if your speakers are placed at different heights, direction or areas of the room."

     - **en**: "Ambient mode links two speakers to play the same source synchronised. Each speaker plays both the left and right channels.\n\nChoose this mode if your speakers are placed at different heights, direction or areas of the room."
     - **da**: "Med omsluttende tilstand forbindes til højttalerne, så de afspiller samme lyd synkroniseret. Hver højttaler afspiller både venstre og højre kanal.\n\nVælg denne tilstand, hvis dine højttalere er placeret i forskellig højde, peger i forskellig retning eller står forskellige steder i rummet."
     - **nl**: "In de ambient-modus worden twee luidsprekers verbonden zodat ze dezelfde bron gesynchroniseerd afspelen. Elke luidspreker speelt zowel het linker- als het rechterkanaal.\n\nKies deze modus als je luidsprekers op verschillende hoogtes, in verschillende richtingen of in verschillende delen van de kamer zijn geplaatst."
     - **de**: "Der Umgebungsmodus verbindet zwei Lautsprecher, die synchron denselben Ton wiedergeben. Jeder Lautsprecher gibt sowohl den linken als auch den rechten Kanal wieder.\n\nWähle diesen Modus, wenn deine Lautsprecher auf unterschiedlichen Höhen, in verschiedener Richtung oder in unterschiedlichen Bereichen des Raumes angeordnet sind."
     - **fr**: "Le mode Ambient relie les enceintes qui lisent la même source de manière synchronisée. Chaque enceinte lit à la fois le canal droit et le canal gauche.\n\nChoisissez ce mode si vos enceintes sont positionnées à différentes hauteurs, orientations ou dans différentes zones de la pièce."
     - **it-IT**: "In modalità ambiente due diffusori sono collegati per riprodurre lo stesso audio simultaneamente. Entrambi riproducono quindi sia il canale destro che quello sinistro.\n\nScegli questa modalità se i diffusori sono posizionati in direzioni, zone o altezze della stanza diverse."
     - **nb**: "Omgivelsesmodus kobler to høyttalere og spiller av samme kilde synkronisert. Hver høyttaler spiller både venstre og høyre kanal.\n\nVelg denne modusen hvis høyttalerne dine er plassert i forskjellige høyder, retninger eller områder i rommet."
     - **pt-PT**: "O modo ambiente liga a altifalantes para reproduzirem a mesma fonte sincronizada. Cada altifalante reproduz tanto o canal esquerdo como o direito.\n\nEscolha este modo se os altifalantes estiverem colocados a diferentes alturas, direções ou zonas da divisão."
     - **ru**: "Пространственный режим: колонки воспроизводят один и тот же синхронизированный источник. Каждая колонка проигрывает как левый, так и правый канал.\n\nВыберите этот режим при установке колонок на разной высоте, в разных направлениях или в разных углах помещения."
     - **es**: "Modo ambiente: dos altavoces reproducen el mismo sonido de forma sincronizada. Cada altavoz reproduce ambos canales, el izquierdo y el derecho.\n\nElige este modo de funcionamiento si los altavoces están instalados a alturas diferentes, en direcciones diferentes o en zonas diferentes de la sala."
     - **sv**: "Ambient-läge kopplar två högtalare att spela upp samma ljud synkroniserat. Varje högtalare spelar både vänster- och högerkanalerna.\n\nVälj detta läge om dina högtalare är placerade på olika höjder, riktningar eller områden i rummet."
     - **fi**: "Ambient-tila yhdistää kaksi kaiutinta soittamaan synkronoidusti samaa lähdettä. Kumpikin kaiutin toistaa sekä vasemman- että oikeanpuolista kanavaa.\n\nValitse tämä tila, jos kaiuttimet on sijoitettu huoneessa eri korkeuksiin, suuntiin tai paikkoihin."
     - **zh-Hans**: "环境模式：连接两台音箱，同步播放相同的音源。\n每台音箱均在左右声道播放。"
     - **zh-Hant**: "環境模式會連結喇叭以同步播放相同的音訊，每個喇叭均播放左聲道和右聲道。\n\n如果您的喇叭放在房間內不同的高度、方向或區域，請選擇此模式。"
     - **id**: "Mode Ambient tersambung ke speaker untuk memutar audio yang sama melalui sinkronisasi. Setiap speaker memutar saluran kiri dan kanan.\n\nPilih mode ini jika speaker Anda ditempatkan pada ketinggian, arah, atau area ruangan yang berbeda."
     - **fil**: "Kumokonekta ang ambient mode sa mga speaker para patugtugin ang parehong pagkukunan nang magkasabay. Ang bawat speaker ay tumutugtog ng parehong kaliwa at kanang channel.\n\nPiliin ang mode na ito kung ang iyong mga speaker ay nasa magkakaibang taas, direksiyon o lugar sa kuwarto."
     - **ja**: "Ambient Modeは2つのスピーカーをつないで、同じ入力源をシンクロナイズさせて再生します。各スピーカーは左と右、両方のチャンネルから再生します。\n\nご使用のスピーカーが違う高さに設置されていたり違う方向を向いている、あるいは部屋の中の別のエリアにある場合にこのモードを選択してください。"
 */
  public static func couple_screen_select_mode_ambient_desc() -> String {
     return localizedString(
         key:"couple_screen_select_mode_ambient_desc",
         defaultValue:"Ambient mode links two speakers to play the same source synchronised. Each speaker plays both the left and right channels.\n\nChoose this mode if your speakers are placed at different heights, direction or areas of the room.",
         substitutions: [:])
  }

 /**
"Stereo"

     - **en**: "Stereo"
     - **da**: "Stereo"
     - **nl**: "Stereo"
     - **de**: "Stereo"
     - **fr**: "Stéréo"
     - **it-IT**: "Stereo"
     - **nb**: "Stereo"
     - **pt-PT**: "Estéreo"
     - **ru**: "Стерео"
     - **es**: "Estéreo"
     - **sv**: "Stereo"
     - **fi**: "Stereo"
     - **zh-Hans**: "立体声模式"
     - **zh-Hant**: "立體聲"
     - **id**: "Stereo"
     - **fil**: "Stereo"
     - **ja**: "Stereo"
 */
  public static func couple_screen_select_mode_stereo() -> String {
     return localizedString(
         key:"couple_screen_select_mode_stereo",
         defaultValue:"Stereo",
         substitutions: [:])
  }

 /**
"Stereo mode links two speakers as a stereo pair. One speaker is the left channel and the other the right.\n\nChoose this mode if the speakers are placed at the same height, equal distance and facing the same direction."

     - **en**: "Stereo mode links two speakers as a stereo pair. One speaker is the left channel and the other the right.\n\nChoose this mode if the speakers are placed at the same height, equal distance and facing the same direction."
     - **da**: "Med stereotilstand forbindes to højttalere som ét stereopar. Hvor én højttaler afspiller venstre kanal, og den anden afspiller højre.\n\nVælg denne tilstand, hvis højttalerne er placeret i samme højde, lige langt fra afspilleren og peger i samme retning."
     - **nl**: "In de stereo-modus worden twee luidsprekers als stereopaar aan elkaar gekoppeld. Eén luidspreker is het linkerkanaal en de andere is het rechterkanaal.\n\nKies deze modus als de luidsprekers zich op dezelfde hoogte, op dezelfde afstand en in dezelfde richting bevinden."
     - **de**: "Der Stereomodus verbindet zwei gleich große Lautsprecher als Stereopaar miteinander. Dabei gibt ein Lautsprecher den linken und der andere den rechten Kanal wieder.\n\nWähle diesen Modus, wenn die Lautsprecher auf derselben Höhe und in gleichem Abstand angeordnet sind und in dieselbe Richtung weisen."
     - **fr**: "Le mode Stéréo relie deux enceintes en tant que paire stéréo, avec une enceinte comme canal droit et l’autre comme canal gauche.\n\nChoisissez ce mode si les enceintes sont placées à la même hauteur, distance et dirigées dans la même direction."
     - **it-IT**: "In modalità stereo due diffusori sono collegati come una coppia stereo: uno riproduce il canale sinistro e l’altro il canale destro.\n\nScegli questa modalità se i diffusori sono posizionati alla stessa altezza, a pari distanza e nella stessa direzione."
     - **nb**: "Stereomodus forbinder to høyttalere som et stereopar. En høyttaler er den venstre kanalen og den andre den høyre.\n\nVelg denne modusen hvis høyttalerne er plassert i samme høyde, lik avstand og i samme retning."
     - **pt-PT**: "O modo estéreo liga dois altifalantes como um par de estéreo. Um altifalante reproduz o áudio do canal direito e o outro do canal esquerdo.\n\nEscolha este modo se os altifalantes estiverem colocados à mesma altura, à mesma distância e voltados na mesma direção."
     - **ru**: "Стереорежим объединяет две колонки в стереопару: одна проигрывает левый канал, а другая — правый.\n\nВыберите этот режим при размещении колонок на одной высоте, на равном расстоянии и в одном направлении."
     - **es**: "Modo estéreo: dos altavoces se emparejan en modo estéreo. Un altavoz emite el canal izquierdo y el otro el derecho.\n\nElige este modo si los altavoces están instalados a la misma altura, a la misma distancia y orientados en la misma dirección."
     - **sv**: "Stereo-läge kopplar två högtalare av samma storlek som ett stereopar. Den ena högtalaren kopplas till den vänstra kanalen och den andra till den högra.\n\nVälj detta läge om högtalarna är placerade i samma höjd, samma avstånd och i samma riktning."
     - **fi**: "Stereo-tila yhdistää kaksi kaiutinta stereo-pariksi. Yksi kaiutin toistaa vasenta ja toinen kaiutin oikeaa kanavaa.\n\nValitse tämä tila, jos kaiuttimet on sijoitettu samalle korkeudelle, saman etäisyyden päähän ja samansuuntaisesti."
     - **zh-Hans**: "立体声模式：连接两台音箱，构成一对立体声音箱。\n一台音箱为左声道，另一台为右声道。"
     - **zh-Hant**: "立體聲模式會連結兩個喇叭，當成一組立體聲音響，一個喇叭為左聲道，另一個喇叭為右聲道。"
     - **id**: "Mode stereo memasangkan dua speaker sebagai pasangan stereo, dengan satu speaker sebagai saluran kiri dan yang lain sebagai saluran kanan.\n\nPilih mode ini jika speaker ditempatkan pada ketinggian yang sama, jarak yang sama, dan menghadap ke arah yang sama."
     - **fil**: "Kumokonekta ang stereo mode sa dalawang speaker bilang isang pares ng stereo. Ang isang speaker ay ang kaliwang channel at ang isa ay ang kanan.\n\nPiliin ang mode na ito kung ang mga speaker ay pareho ang taas, patas ang layo at nakaharap sa parehong direksyon."
     - **ja**: "Stereo Modeは2つのスピーカーをステレオペアとしてつなぎます。一つのスピーカーが左チャンネルを担い、もう一つが右チャンネルを担います。\n\nご使用のスピーカーが同じ高さ、同じ距離、そして同じ方向を向いて設置されている場合にこのモードを選択してください。"
 */
  public static func couple_screen_select_mode_stereo_desc() -> String {
     return localizedString(
         key:"couple_screen_select_mode_stereo_desc",
         defaultValue:"Stereo mode links two speakers as a stereo pair. One speaker is the left channel and the other the right.\n\nChoose this mode if the speakers are placed at the same height, equal distance and facing the same direction.",
         substitutions: [:])
  }

 /**
"Select ambient or stereo mode."

     - **en**: "Select ambient or stereo mode."
     - **da**: "Vælg omsluttende eller stereotilstand."
     - **nl**: "Selecteer de ambient- of stereo-modus."
     - **de**: "Umgebungsmodus oder Stereomodus auswählen."
     - **fr**: "Sélectionnez le mode Ambient ou Stéréo."
     - **it-IT**: "Seleziona la modalità ambiente o stereo."
     - **nb**: "Velg omgivelses- eller stereomodus."
     - **pt-PT**: "Selecionar o modo ambiente ou estéreo."
     - **ru**: "Выберите пространственный или стерео режим."
     - **es**: "Selecciona el modo ambiente o estéreo."
     - **sv**: "Markera Ambient- eller Stereo-läge."
     - **fi**: "Valitse ambient- tai stereo-tila."
     - **zh-Hans**: "选择环境模式或立体声模式"
     - **zh-Hant**: "選擇環境或立體聲模式。"
     - **id**: "Pilih mode ambient atau stereo."
     - **fil**: "Piliin nag ambient o stereo mode."
     - **ja**: "Ambient ModeまたはStereo Modeを選択してください。"
 */
  public static func couple_screen_select_mode_subtitle() -> String {
     return localizedString(
         key:"couple_screen_select_mode_subtitle",
         defaultValue:"Select ambient or stereo mode.",
         substitutions: [:])
  }

 /**
"Select speakers to couple."

     - **en**: "Select speakers to couple."
     - **da**: "Vælg højttalere at parre."
     - **nl**: "Selecteer de te koppelen luidsprekers."
     - **de**: "Lautsprecher zum Koppeln auswählen."
     - **fr**: "Sélectionnez les enceintes à coupler."
     - **it-IT**: "Seleziona il diffusore da associare."
     - **nb**: "Velg høyttalere for å pare."
     - **pt-PT**: "Selecionar altifalantes para emparelhar."
     - **ru**: "Выберите колонки для объединения."
     - **es**: "Selecciona los altavoces para emparejarlos."
     - **sv**: "Markera högtalare att parkoppla."
     - **fi**: "Valitse laiteparin muodostavat kaiuttimet."
     - **zh-Hans**: "选择音箱进行组合配对"
     - **zh-Hant**: "選擇要配對的喇叭。"
     - **id**: "Pilih speaker untuk dipasangkan."
     - **fil**: "Piliin ang mga speaker upang ipagpares."
     - **ja**: "カップリングするスピーカーを選択してください。"
 */
  public static func couple_screen_select_speaker_subtitle() -> String {
     return localizedString(
         key:"couple_screen_select_speaker_subtitle",
         defaultValue:"Select speakers to couple.",
         substitutions: [:])
  }

 /**
"Stereo - Select left and right speaker."

     - **en**: "Stereo - Select left and right speaker."
     - **da**: "Stereo - vælg venstre og højre højttaler."
     - **nl**: "Stereo - Selecteer linker- en rechterluidspreker."
     - **de**: "Stereo – Linken und rechten Kanal auswählen."
     - **fr**: "Stéréo - Sélectionne l’enceinte gauche et droite."
     - **it-IT**: "Stereo - Seleziona il diffusore destro e sinistro."
     - **nb**: "Stereo – Velg venstre og høyre høyttaler."
     - **pt-PT**: "Estéreo - Selecionar o altifalante esquerdo e direito."
     - **ru**: "Стерео – выберите левую и правую колонки. "
     - **es**: "Estéreo - Selecciona el altavoz izquierdo y el derecho."
     - **sv**: "Stereo – Markera vänster och höger högtalare."
     - **fi**: "Stereo – Valitse vasen ja oikea kaiutin."
     - **zh-Hans**: "立体声 - 选择左右声道"
     - **zh-Hant**: "立體聲 - 選擇左右喇叭。"
     - **id**: "Stereo - Pilih speaker kiri dan kanan."
     - **fil**: "Stereo - Piliin ang kaliwa at kanang speaker."
     - **ja**: "Stereo - 左と右のスピーカーを選択します。"
 */
  public static func couple_screen_title_stereo() -> String {
     return localizedString(
         key:"couple_screen_title_stereo",
         defaultValue:"Stereo - Select left and right speaker.",
         substitutions: [:])
  }

 /**
"About"

     - **en**: "About"
     - **da**: "Om"
     - **nl**: "Over"
     - **de**: "Über"
     - **fr**: "À propos"
     - **it-IT**: "Informazioni"
     - **nb**: "Om"
     - **pt-PT**: "Sobre"
     - **ru**: "О системе"
     - **es**: "Acerca de"
     - **sv**: "Om"
     - **fi**: "Yleistä"
     - **zh-Hans**: "关于音箱"
     - **zh-Hant**: "關於"
     - **id**: "Tentang"
     - **fil**: "Tungkol sa"
     - **ja**: "バージョン情報"
 */
  public static func device_settings_menu_item_about() -> String {
     return localizedString(
         key:"device_settings_menu_item_about",
         defaultValue:"About",
         substitutions: [:])
  }

 /**
"ABOUT HEADPHONES"

     - **en**: "ABOUT HEADPHONES"
     - **da**: "OM HOVEDTELEFONERNE"
     - **nl**: "OVER HOOFDTELEFOONS"
     - **de**: "ÜBER DIE KOPFHÖRER"
     - **fr**: "À PROPOS DU CASQUE"
     - **it-IT**: "INFORMAZIONI SULLE CUFFIE"
     - **nb**: "OM HODETELEFONER"
     - **pt-PT**: "SOBRE AUSCULTADORES"
     - **ru**: "О НАУШНИКАХ"
     - **es**: "SOBRE LOS AURICULARES"
     - **sv**: "OM HÖRLURAR"
     - **fi**: "TIETOA KUULOKKEISTA"
     - **zh-Hans**: "关于耳机"
     - **zh-Hant**: "關於耳機"
     - **id**: "TENTANG HEADPHONE"
     - **fil**: "TUNGKOL SA MGA HEADPHONE"
     - **ja**: "ヘッドフォンについて"
 */
  public static func device_settings_menu_item_about_headphone_uc() -> String {
     return localizedString(
         key:"device_settings_menu_item_about_headphone_uc",
         defaultValue:"ABOUT HEADPHONES",
         substitutions: [:])
  }

 /**
"ABOUT SPEAKER"

     - **en**: "ABOUT SPEAKER"
     - **da**: "OM HØJTTALER"
     - **nl**: "OVER DE LUIDSPREKER"
     - **de**: "ÜBER DEN LAUTSPRECHER"
     - **fr**: "À PROPOS DE L’ENCEINTE"
     - **it-IT**: "INFORMAZIONI SUL DIFFUSORE"
     - **nb**: "OM HØYTTALERE"
     - **pt-PT**: "SOBRE O ALTIFALANTE"
     - **ru**: "ОБ АКУСТИЧЕСКОЙ СИСТЕМЕ"
     - **es**: "ACERCA DEL ALTAVOZ"
     - **sv**: "OM HÖGTALAREN"
     - **fi**: "TIETOJA KAIUTTIMESTA"
     - **zh-Hans**: "关于音箱"
     - **zh-Hant**: "關於喇叭"
     - **id**: "TENTANG SPEAKER"
     - **fil**: "TUNGKOL SA SPEAKER"
     - **ja**: "スピーカー情報"
 */
  public static func device_settings_menu_item_about_speaker_uc() -> String {
     return localizedString(
         key:"device_settings_menu_item_about_speaker_uc",
         defaultValue:"ABOUT SPEAKER",
         substitutions: [:])
  }

 /**
"ANC"

     - **en**: "ANC"
     - **da**: "ANC"
     - **nl**: "ANC"
     - **de**: "ANC"
     - **fr**: "ANC"
     - **it-IT**: "ANC"
     - **nb**: "ANC"
     - **pt-PT**: "ANC"
     - **ru**: "ANC"
     - **es**: "ANC"
     - **sv**: "ANC"
     - **fi**: "ANC"
     - **zh-Hans**: "ANC"
     - **zh-Hant**: "ANC"
     - **id**: "ANC"
     - **fil**: "ANC"
     - **ja**: "ANC"
 */
  public static func device_settings_menu_item_anc_uc() -> String {
     return localizedString(
         key:"device_settings_menu_item_anc_uc",
         defaultValue:"ANC",
         substitutions: [:])
  }

 /**
"Auto Off Timer"

     - **en**: "Auto Off Timer"
     - **da**: "Automatisk fra timer"
     - **nl**: "Timer voor automatisch uitschakelen"
     - **de**: "Timer zum automatischen Abschalten"
     - **fr**: "Minuteur d’arrêt automatique"
     - **it-IT**: "Spegnimento automatico timer"
     - **nb**: "Auto Off Timer"
     - **pt-PT**: "Temporizador Auto Off"
     - **ru**: "Таймер автовыключения"
     - **es**: "Temporizador de apagado automático"
     - **sv**: "Timer för autoavstängning"
     - **fi**: "Poiskytkentäajastin"
     - **zh-Hans**: "自动关闭计时器"
     - **zh-Hant**: "自動關閉計時器"
     - **id**: "Pengatur Waktu Mati Otomatis"
     - **fil**: "Auto Off na Timer"
     - **ja**: "自動オフタイマー"
 */
  public static func device_settings_menu_item_auto_off_timer() -> String {
     return localizedString(
         key:"device_settings_menu_item_auto_off_timer",
         defaultValue:"Auto Off Timer",
         substitutions: [:])
  }

 /**
"AUTO OFF TIMER"

     - **en**: "AUTO OFF TIMER"
     - **da**: "AUTOMATISK FRA TIMER"
     - **nl**: "TIMER VOOR AUTOMATISCH UITSCHAKELEN"
     - **de**: "TIMER ZUM AUTOMATISCHEN ABSCHALTEN"
     - **fr**: "MINUTEUR D’ARRÊT AUTOMATIQUE"
     - **it-IT**: "SPEGNIMENTO AUTOMATICO TIMER"
     - **nb**: "AUTO OFF TIMER"
     - **pt-PT**: "TEMPORIZADOR AUTO OFF"
     - **ru**: "ТАЙМЕР АВТОВЫКЛЮЧЕНИЯ"
     - **es**: "TEMPORIZADOR DE APAGADO AUTOMÁTICO"
     - **sv**: "TIMER FÖR AUTOAVSTÄNGNING"
     - **fi**: "POISKYTKENTÄAJASTIN"
     - **zh-Hans**: "自动关闭计时器"
     - **zh-Hant**: "自動關閉計時器"
     - **id**: "PENGATUR WAKTU MATI OTOMATIS"
     - **fil**: "AUTO OFF NA TIMER"
     - **ja**: "自動オフタイマー"
 */
  public static func device_settings_menu_item_auto_off_timer_uc() -> String {
     return localizedString(
         key:"device_settings_menu_item_auto_off_timer_uc",
         defaultValue:"AUTO OFF TIMER",
         substitutions: [:])
  }

 /**
"Couple Speakers"

     - **en**: "Couple Speakers"
     - **da**: "Parring af højttalere"
     - **nl**: "Koppelen van luidsprekers"
     - **de**: "Lautsprecher koppeln"
     - **fr**: "Coupler les enceintes"
     - **it-IT**: "Associa più diffusori"
     - **nb**: "Sammenkoblede høyttalere"
     - **pt-PT**: "Acoplar altifalantes"
     - **ru**: "Объединение колонок"
     - **es**: "Emparejar altavoces"
     - **sv**: "Koppla ihop högtalare"
     - **fi**: "Parikaiuttimet"
     - **zh-Hans**: "音箱组合配对"
     - **zh-Hant**: "雙喇叭"
     - **id**: "Speaker Pasangan"
     - **fil**: "Ipares ang Mga Speaker"
     - **ja**: "スピーカーのカップリング"
 */
  public static func device_settings_menu_item_couple() -> String {
     return localizedString(
         key:"device_settings_menu_item_couple",
         defaultValue:"Couple Speakers",
         substitutions: [:])
  }

 /**
"COUPLE SPEAKERS"

     - **en**: "COUPLE SPEAKERS"
     - **da**: "PARRING AF HØJTTALERE"
     - **nl**: "KOPPELEN VAN LUIDSPREKERS"
     - **de**: "LAUTSPRECHER KOPPELN"
     - **fr**: "COUPLER LES ENCEINTES"
     - **it-IT**: "ASSOCIA PIÙ DIFFUSORI"
     - **nb**: "SAMMENKOBLEDE HØYTTALERE"
     - **pt-PT**: "ACOPLAR ALTIFALANTES"
     - **ru**: "ОБЪЕДИНИТЬ КОЛОНКИ"
     - **es**: "EMPAREJAR ALTAVOCES"
     - **sv**: "KOPPLA IHOP HÖGTALARE"
     - **fi**: "PARIKAIUTTIMET"
     - **zh-Hans**: "音箱组合配对"
     - **zh-Hant**: "雙喇叭"
     - **id**: "SPEAKER PASANGAN"
     - **fil**: "IPARES ANG MGA SPEAKER"
     - **ja**: "スピーカーのカップリング"
 */
  public static func device_settings_menu_item_couple_uc() -> String {
     return localizedString(
         key:"device_settings_menu_item_couple_uc",
         defaultValue:"COUPLE SPEAKERS",
         substitutions: [:])
  }

 /**
"Equaliser"

     - **en**: "Equaliser"
     - **da**: "Equalizer"
     - **nl**: "Toonregeling"
     - **de**: "Equalizer"
     - **fr**: "Égaliseur"
     - **it-IT**: "Equalizzatore"
     - **nb**: "Equalizer"
     - **pt-PT**: "Equalizador"
     - **ru**: "Эквалайзер"
     - **es**: "Ecualizador"
     - **sv**: "Equalizer (EQ)"
     - **fi**: "Taajuuskorjain"
     - **zh-Hans**: "均衡器"
     - **zh-Hant**: "均衡器"
     - **id**: "Equaliser"
     - **fil**: "Equalizer"
     - **ja**: "イコライザー"
 */
  public static func device_settings_menu_item_equaliser() -> String {
     return localizedString(
         key:"device_settings_menu_item_equaliser",
         defaultValue:"Equaliser",
         substitutions: [:])
  }

 /**
"EQUALISER"

     - **en**: "EQUALISER"
     - **da**: "EQUALIZER"
     - **nl**: "TOONREGELING"
     - **de**: "EQUALIZER"
     - **fr**: "ÉGALISEUR"
     - **it-IT**: "EQUALIZZATORE"
     - **nb**: "EQUALIZER"
     - **pt-PT**: "EQUALIZADOR"
     - **ru**: "ЭКВАЛАЙЗЕР"
     - **es**: "ECUALIZADOR"
     - **sv**: "EQUALIZER (EQ)"
     - **fi**: "TAAJUUSKORJAIN"
     - **zh-Hans**: "均衡器"
     - **zh-Hant**: "均衡器"
     - **id**: "EQUALISER"
     - **fil**: "EQUALIZER"
     - **ja**: "イコライザー"
 */
  public static func device_settings_menu_item_equaliser_uc() -> String {
     return localizedString(
         key:"device_settings_menu_item_equaliser_uc",
         defaultValue:"EQUALISER",
         substitutions: [:])
  }

 /**
"Feedback Sounds"

     - **en**: "Feedback Sounds"
     - **da**: "Feedback lyde"
     - **nl**: "Feedback geluiden"
     - **de**: "Feedback-Töne"
     - **fr**: "Larsens"
     - **it-IT**: "Suoni di feedback"
     - **nb**: "Feedback-lyder"
     - **pt-PT**: "Sons de feedback"
     - **ru**: "Звуки обратной связи"
     - **es**: "Sonidos de acople"
     - **sv**: "Feedbackljud"
     - **fi**: "Palautteen äänet"
     - **zh-Hans**: "反馈声"
     - **zh-Hant**: "回音"
     - **id**: "Suara Umpan Balik"
     - **fil**: "Feedback na mga Sound"
     - **ja**: "フィードバックサウンド"
 */
  public static func device_settings_menu_item_feedback_sounds() -> String {
     return localizedString(
         key:"device_settings_menu_item_feedback_sounds",
         defaultValue:"Feedback Sounds",
         substitutions: [:])
  }

 /**
"FEEDBACK SOUNDS"

     - **en**: "FEEDBACK SOUNDS"
     - **da**: "FEEDBACK LYDE"
     - **nl**: "FEEDBACK GELUIDEN"
     - **de**: "FEEDBACK-TÖNE"
     - **fr**: "LARSENS"
     - **it-IT**: "SUONI DI FEEDBACK"
     - **nb**: "FEEDBACK-LYDER"
     - **pt-PT**: "SONS DE FEEDBACK"
     - **ru**: "ЗВУКИ ОБРАТНОЙ СВЯЗИ"
     - **es**: "SONIDOS DE ACOPLE"
     - **sv**: "FEEDBACKLJUD"
     - **fi**: "PALAUTTEEN ÄÄNET"
     - **zh-Hans**: "反馈声"
     - **zh-Hant**: "回音"
     - **id**: "SUARA UMPAN BALIK"
     - **fil**: "FEEDBACK NA MGA SOUNDS"
     - **ja**: "フィードバックサウンド"
 */
  public static func device_settings_menu_item_feedback_sounds_uc() -> String {
     return localizedString(
         key:"device_settings_menu_item_feedback_sounds_uc",
         defaultValue:"FEEDBACK SOUNDS",
         substitutions: [:])
  }

 /**
"Forget Speaker"

     - **en**: "Forget Speaker"
     - **da**: "Glem højttaler"
     - **nl**: "Luidspreker vergeten"
     - **de**: "Lautsprecher vergessen"
     - **fr**: "Ignorer l’enceinte"
     - **it-IT**: "Rimuovi il diffusore"
     - **nb**: "Glem høyttaler"
     - **pt-PT**: "Esquecer altifalante"
     - **ru**: "Забыть колонку"
     - **es**: "Olvidar altavoz"
     - **sv**: "Glöm högtalare"
     - **fi**: "Unohda kaiutin"
     - **zh-Hans**: "忽略音箱"
     - **zh-Hant**: "忘記喇叭"
     - **id**: "Lupakan Speaker"
     - **fil**: "Kalimutan ang Speaker"
     - **ja**: "スピーカーを削除"
 */
  public static func device_settings_menu_item_forget() -> String {
     return localizedString(
         key:"device_settings_menu_item_forget",
         defaultValue:"Forget Speaker",
         substitutions: [:])
  }

 /**
"Forget Headphones"

     - **en**: "Forget Headphones"
     - **da**: "Glem hovedtelefoner"
     - **nl**: "Hoofdtelefoon vergeten"
     - **de**: "Kopfhörer vergessen"
     - **fr**: "Oublier le casque"
     - **it-IT**: "Dimentica cuffie"
     - **nb**: "Glem hodetelefoner"
     - **pt-PT**: "Esquecer auscultadores"
     - **ru**: "Забыть наушники"
     - **es**: "Olvidar auriculares"
     - **sv**: "Glöm hörlurar"
     - **fi**: "Kuulokkeiden unohtaminen"
     - **zh-Hans**: "忽略耳机"
     - **zh-Hant**: "忘記耳機"
     - **id**: "Lupakan Headphone"
     - **fil**: "Kalimutan ang mga Headphone"
     - **ja**: "ヘッドフォンを削除"
 */
  public static func device_settings_menu_item_forget_headphone() -> String {
     return localizedString(
         key:"device_settings_menu_item_forget_headphone",
         defaultValue:"Forget Headphones",
         substitutions: [:])
  }

 /**
"FORGET HEADPHONES"

     - **en**: "FORGET HEADPHONES"
     - **da**: "GLEM HOVEDTELEFONER"
     - **nl**: "HOOFDTELEFOON VERGETEN"
     - **de**: "KOPFHÖRER VERGESSEN"
     - **fr**: "OUBLIER LE CASQUE"
     - **it-IT**: "DIMENTICA CUFFIE"
     - **nb**: "GLEM HODETELEFONER"
     - **pt-PT**: "ESQUECER AUSCULTADORES"
     - **ru**: "ЗАБЫТЬ НАУШНИКИ"
     - **es**: "OLVIDAR AURICULARES"
     - **sv**: "GLÖM HÖRLURAR"
     - **fi**: "KUULOKKEIDEN UNOHTAMINEN"
     - **zh-Hans**: "忽略耳机"
     - **zh-Hant**: "忘記耳機"
     - **id**: "LUPAKAN HEADPHONE"
     - **fil**: "KALIMUTAN ANG MGA HEADPHONE"
     - **ja**: "ヘッドフォンを削除"
 */
  public static func device_settings_menu_item_forget_headphone_uc() -> String {
     return localizedString(
         key:"device_settings_menu_item_forget_headphone_uc",
         defaultValue:"FORGET HEADPHONES",
         substitutions: [:])
  }

 /**
"FORGET SPEAKER"

     - **en**: "FORGET SPEAKER"
     - **da**: "GLEM HØJTTALER"
     - **nl**: "LUIDSPREKER VERGETEN"
     - **de**: "LAUTSPRECHER VERGESSEN"
     - **fr**: "IGNORER L’ENCEINTE"
     - **it-IT**: "RIMUOVI IL DIFFUSORE"
     - **nb**: "GLEM HØYTTALER"
     - **pt-PT**: "ESQUECER ALTIFALANTE"
     - **ru**: "ЗАБЫТЬ КОЛОНКУ"
     - **es**: "OLVIDAR ALTAVOZ"
     - **sv**: "GLÖM HÖGTALARE"
     - **fi**: "UNOHDA KAIUTIN"
     - **zh-Hans**: "忽略音箱"
     - **zh-Hant**: "忘記喇叭"
     - **id**: "LUPAKAN SPEAKER"
     - **fil**: "KALIMUTAN ANG SPEAKER"
     - **ja**: "スピーカーを削除"
 */
  public static func device_settings_menu_item_forget_uc() -> String {
     return localizedString(
         key:"device_settings_menu_item_forget_uc",
         defaultValue:"FORGET SPEAKER",
         substitutions: [:])
  }

 /**
"Light"

     - **en**: "Light"
     - **da**: "Lys"
     - **nl**: "Licht"
     - **de**: "Licht"
     - **fr**: "Lumière"
     - **it-IT**: "Luce"
     - **nb**: "Lys"
     - **pt-PT**: "Luz"
     - **ru**: "Свет"
     - **es**: "Luz"
     - **sv**: "Ljus"
     - **fi**: "Valo"
     - **zh-Hans**: "指示灯亮度调节"
     - **zh-Hant**: "燈光"
     - **id**: "Lampu"
     - **fil**: "Ilaw"
     - **ja**: "ライト"
 */
  public static func device_settings_menu_item_light() -> String {
     return localizedString(
         key:"device_settings_menu_item_light",
         defaultValue:"Light",
         substitutions: [:])
  }

 /**
"LIGHT"

     - **en**: "LIGHT"
     - **da**: "LYS"
     - **nl**: "LICHT"
     - **de**: "LICHT"
     - **fr**: "LUMIÈRE"
     - **it-IT**: "LUCE"
     - **nb**: "LYS"
     - **pt-PT**: "LUZ"
     - **ru**: "СВЕТ"
     - **es**: "LUZ"
     - **sv**: "LJUS"
     - **fi**: "VALO"
     - **zh-Hans**: "指示灯亮度调节"
     - **zh-Hant**: "燈光"
     - **id**: "LAMPU"
     - **fil**: "LIGHT"
     - **ja**: "ライト"
 */
  public static func device_settings_menu_item_light_uc() -> String {
     return localizedString(
         key:"device_settings_menu_item_light_uc",
         defaultValue:"LIGHT",
         substitutions: [:])
  }

 /**
"M-Button"

     - **en**: "M-Button"
     - **da**: "M-Button"
     - **nl**: "M-knop"
     - **de**: "M-Taste"
     - **fr**: "Bouton M"
     - **it-IT**: "Pulsante M"
     - **nb**: "M-knapp"
     - **pt-PT**: "Botão M"
     - **ru**: "M-кнопка"
     - **es**: "Botón M"
     - **sv**: "M-knapp"
     - **fi**: "M-painike"
     - **zh-Hans**: "M-按钮"
     - **zh-Hant**: "M 按鈕"
     - **id**: "M-Button"
     - **fil**: "M-Button"
     - **ja**: "Mボタン"
 */
  public static func device_settings_menu_item_m_button() -> String {
     return localizedString(
         key:"device_settings_menu_item_m_button",
         defaultValue:"M-Button",
         substitutions: [:])
  }

 /**
"M-BUTTON"

     - **en**: "M-BUTTON"
     - **da**: "M-BUTTON"
     - **nl**: "M-KNOP"
     - **de**: "M-TASTE"
     - **fr**: "BOUTON M"
     - **it-IT**: "PULSANTE M"
     - **nb**: "M-KNAPP"
     - **pt-PT**: "BOTÃO M"
     - **ru**: "M-КНОПКА"
     - **es**: "BOTÓN M"
     - **sv**: "M-KNAPP"
     - **fi**: "M-PAINIKE"
     - **zh-Hans**: "M-按钮"
     - **zh-Hant**: "M 按鈕"
     - **id**: "M-BUTTON"
     - **fil**: "M-BUTTON"
     - **ja**: "Mボタン"
 */
  public static func device_settings_menu_item_m_button_uc() -> String {
     return localizedString(
         key:"device_settings_menu_item_m_button_uc",
         defaultValue:"M-BUTTON",
         substitutions: [:])
  }

 /**
"Rename"

     - **en**: "Rename"
     - **da**: "Nyt navn"
     - **nl**: "Hernoemen"
     - **de**: "Umbenennen"
     - **fr**: "Renommer"
     - **it-IT**: "Rinomina"
     - **nb**: "GI NYTT NAVN"
     - **pt-PT**: "Mudar o nome"
     - **ru**: "Переименовать"
     - **es**: "Cambiar nombre"
     - **sv**: "Byt namn"
     - **fi**: "Nimeä uudelleen"
     - **zh-Hans**: "音箱重命名"
     - **zh-Hant**: "重新命名"
     - **id**: "Ubah nama"
     - **fil**: "Palitan ang pangalan"
     - **ja**: "名前の変更"
 */
  public static func device_settings_menu_item_rename() -> String {
     return localizedString(
         key:"device_settings_menu_item_rename",
         defaultValue:"Rename",
         substitutions: [:])
  }

 /**
"Rename Headphones"

     - **en**: "Rename Headphones"
     - **da**: "Omdøb hovedtelefoner"
     - **nl**: "Hoofdtelefoon nieuwe naam geven"
     - **de**: "Kopfhörer umbenennen"
     - **fr**: "Renommer le casque"
     - **it-IT**: "Rinomina le cuffie"
     - **nb**: "Gi hodetelefoner nytt navn"
     - **pt-PT**: "Renomear auscultadores"
     - **ru**: "Переименовать наушники"
     - **es**: "Cambiar nombre a los auriculares"
     - **sv**: "Döp om hörlurar"
     - **fi**: "Nimeä kuulokkeet uudelleen"
     - **zh-Hans**: "重命名耳机"
     - **zh-Hant**: "重新命名耳機"
     - **id**: "Ubah Nama Headphone"
     - **fil**: "Baguhin ang pangalan ng mga Headphone"
     - **ja**: "ヘッドフォン名の変更"
 */
  public static func device_settings_menu_item_rename_headphone() -> String {
     return localizedString(
         key:"device_settings_menu_item_rename_headphone",
         defaultValue:"Rename Headphones",
         substitutions: [:])
  }

 /**
"RENAME HEADPHONES"

     - **en**: "RENAME HEADPHONES"
     - **da**: "OMDØB HOVEDTELEFONER"
     - **nl**: "HOOFDTELEFOON NIEUWE NAAM GEVEN"
     - **de**: "KOPFHÖRER UMBENENNEN"
     - **fr**: "RENOMMER LE CASQUE"
     - **it-IT**: "RINOMINARE LE CUFFIE"
     - **nb**: "GI HODETELEFONER NYTT NAVN"
     - **pt-PT**: "RENOMEAR AUSCULTADORES"
     - **ru**: "ПЕРЕИМЕНОВАТЬ НАУШНИКИ"
     - **es**: "CAMBIAR NOMBRE A LOS AURICULARES"
     - **sv**: "DÖP OM HÖRLURAR"
     - **fi**: "NIMEÄ KUULOKKEET UUDELLEEN"
     - **zh-Hans**: "重命名耳机"
     - **zh-Hant**: "重新命名耳機"
     - **id**: "UBAH NAMA HEADPHONE"
     - **fil**: "BAGUHIN ANG PANGALAN NG MGA HEADPHONE"
     - **ja**: "ヘッドフォン名の変更"
 */
  public static func device_settings_menu_item_rename_headphone_uc() -> String {
     return localizedString(
         key:"device_settings_menu_item_rename_headphone_uc",
         defaultValue:"RENAME HEADPHONES",
         substitutions: [:])
  }

 /**
"RENAME"

     - **en**: "RENAME"
     - **da**: "NYT NAVN"
     - **nl**: "HERNOEMEN"
     - **de**: "UMBENENNEN"
     - **fr**: "RENOMMER"
     - **it-IT**: "RINOMINA"
     - **nb**: "GI NYTT NAVN"
     - **pt-PT**: "MUDAR O NOME"
     - **ru**: "ПЕРЕИМЕНОВАТЬ"
     - **es**: "CAMBIAR NOMBRE"
     - **sv**: "BYT NAMN"
     - **fi**: "NIMEÄ UUDELLEEN"
     - **zh-Hans**: "音箱重命名"
     - **zh-Hant**: "重新命名"
     - **id**: "UBAH NAMA"
     - **fil**: "PALITAN ANG PANGALAN"
     - **ja**: "名前の変更"
 */
  public static func device_settings_menu_item_rename_uc() -> String {
     return localizedString(
         key:"device_settings_menu_item_rename_uc",
         defaultValue:"RENAME",
         substitutions: [:])
  }

 /**
"SPEAKER SETTINGS"

     - **en**: "SPEAKER SETTINGS"
     - **da**: "HØJTTALERINDSTILLINGER"
     - **nl**: "INSTELLINGEN VAN LUIDSPREKER"
     - **de**: "LAUTSPRECHEREINSTELLUNGEN"
     - **fr**: "PARAMÈTRES DE L’ENCEINTE"
     - **it-IT**: "IMPOSTAZIONI DEL DIFFUSORE"
     - **nb**: "INNSTILLINGER FOR HØYTTALERE"
     - **pt-PT**: "DEFINIÇÕES DE ALTIFALANTES"
     - **ru**: "ПАРАМЕТРЫ АКУСТИЧЕСКОЙ СИСТЕМЫ"
     - **es**: "AJUSTES DE LOS ALTAVOCES"
     - **sv**: "HÖGTALARINSTÄLLNINGAR"
     - **fi**: "KAIUTINASETUKSET"
     - **zh-Hans**: "音箱设置"
     - **zh-Hant**: "喇叭設定"
     - **id**: "PENGATURAN SPEAKER"
     - **fil**: "MGA SETTING NG SPEAKER"
     - **ja**: "スピーカーの設定"
 */
  public static func device_settings_menu_item_settings_uc() -> String {
     return localizedString(
         key:"device_settings_menu_item_settings_uc",
         defaultValue:"SPEAKER SETTINGS",
         substitutions: [:])
  }

 /**
"Sounds"

     - **en**: "Sounds"
     - **da**: "Lyde"
     - **nl**: "Geluiden"
     - **de**: "Klänge"
     - **fr**: "Sons"
     - **it-IT**: "Suoni"
     - **nb**: "Lyd"
     - **pt-PT**: "Sons"
     - **ru**: "Звучание"
     - **es**: "Sonidos"
     - **sv**: "Ljud"
     - **fi**: "Äänet"
     - **zh-Hans**: "提示音"
     - **zh-Hant**: "聲音"
     - **id**: "Suara"
     - **fil**: "Mga Tunog"
     - **ja**: "サウンド"
 */
  public static func device_settings_menu_item_sounds() -> String {
     return localizedString(
         key:"device_settings_menu_item_sounds",
         defaultValue:"Sounds",
         substitutions: [:])
  }

 /**
"SOUNDS"

     - **en**: "SOUNDS"
     - **da**: "LYDE"
     - **nl**: "GELUIDEN"
     - **de**: "KLÄNGE"
     - **fr**: "SONS"
     - **it-IT**: "SUONI"
     - **nb**: "LYD"
     - **pt-PT**: "SONS"
     - **ru**: "ЗВУЧАНИЕ"
     - **es**: "SONIDOS"
     - **sv**: "LJUD"
     - **fi**: "ÄÄNET"
     - **zh-Hans**: "提示音"
     - **zh-Hant**: "聲音"
     - **id**: "SUARA"
     - **fil**: "MGA TUNOG"
     - **ja**: "サウンド"
 */
  public static func device_settings_menu_item_sounds_uc() -> String {
     return localizedString(
         key:"device_settings_menu_item_sounds_uc",
         defaultValue:"SOUNDS",
         substitutions: [:])
  }

 /**
"SETTINGS"

     - **en**: "SETTINGS"
     - **da**: "INDSTILLINGER"
     - **nl**: "INSTELLINGEN"
     - **de**: "EINSTELLUNGEN"
     - **fr**: "PARAMÈTRES"
     - **it-IT**: "IMPOSTAZIONI"
     - **nb**: "INNSTILLINGER"
     - **pt-PT**: "DEFINIÇÕES"
     - **ru**: "ПАРАМЕТРЫ"
     - **es**: "AJUSTES"
     - **sv**: "INSTÄLLNINGAR"
     - **fi**: "ASETUKSET"
     - **zh-Hans**: "设置"
     - **zh-Hant**: "設定"
     - **id**: "PENGATURAN"
     - **fil**: "MGA SETTING"
     - **ja**: "設定"
 */
  public static func device_settings_menu_title_settings_uc() -> String {
     return localizedString(
         key:"device_settings_menu_title_settings_uc",
         defaultValue:"SETTINGS",
         substitutions: [:])
  }

 /**
"Press and hold the control jog for 2 seconds,"

     - **en**: "Press and hold the control jog for 2 seconds,"
     - **da**: "1.    Tryk og hold på kontrolknappen i 2 sekunder for at slukke hovedtelefonerne."
     - **nl**: "1.    Druk de bedieningsknop 2 seconden in om de hoofdtelefoon uit te schakelen."
     - **de**: "1. Halte den Steuerungshebel 2 Sekunden lang gedrückt, um den Kopfhörer auszuschalten."
     - **fr**: "1.    Éteignez le casque en appuyant sur le contrôleur pendant 2 secondes."
     - **it-IT**: "1.    Premi la manopola comandi per 2 secondi per spegnere le cuffie."
     - **nb**: "1.    Slå av hodetelefonene ved å trykke på kontrollknotten i to sekunder."
     - **pt-PT**: "1.    Pressione o botão de controlo durante 2 segundos para desligar os auscultadores."
     - **ru**: "1. Сначала выключите наушники, нажав и удерживая рычажок управления нажатым в течение 2 секунд."
     - **es**: "1.    Pulsa el botón de mando durante 2 segundos para apagar los auriculares."
     - **sv**: "1.    Tryck ner kontrollknappen i 2 sekunder för att slå på hörlurarna."
     - **fi**: "1.    Kytke ensin kuulokkeet pois päältä painamalla säädintä 2 sekunnin ajan."
     - **zh-Hans**: "1.    按下控制点动旋钮 2 秒，关闭耳机。"
     - **zh-Hant**: "1. 按住控制旋扭 2 秒鐘以關閉耳機。"
     - **id**: "1.    Tekan tombol kontrol selama 2 detik untuk mematikan headphone."
     - **fil**: "1.    Pindutin ang control jog nang 2 segundo upang i-off ang mga headphone."
     - **ja**: "1.    コントロールジョグを2秒間押し、ヘッドフォンの電源をオフにします。"
 */
  public static func enable_pairing_description_1() -> String {
     return localizedString(
         key:"enable_pairing_description_1",
         defaultValue:"Press and hold the control jog for 2 seconds,",
         substitutions: [:])
  }

 /**
"Then press and hold again for 4 seconds until the light turns blue."

     - **en**: "Then press and hold again for 4 seconds until the light turns blue."
     - **da**: "2.    Tryk og hold på kontrolknappen i 4 sekunder indtil LED’en blinker blåt."
     - **nl**: "2.    Houd de bedieningsknop 4 seconden ingedrukt tot de led blauw wordt."
     - **de**: "2. Halte den Bedienknopf 4 Sekunden lang gedrückt, bis die LED blau leuchtet."
     - **fr**: "2.    Appuyez sur le contrôleur jusqu’à ce que le voyant LED devienne bleu (4 secondes)."
     - **it-IT**: "2.    Premi la manopola comandi per 4 secondi fino a quando l’indicatore LED diventa blu. "
     - **nb**: "2.    Trykk og hold kontrollknotten i fire sekunder til LED-lampen blinker blått. "
     - **pt-PT**: "2.    Pressione o botão de controlo durante 4 segundos até o LED ficar azul."
     - **ru**: "2. Нажмите и удерживайте рычажок управления в течение 4 секунд до тех пор, пока светодиодный индикатор не загорится синим."
     - **es**: "2.    Pulsa el botón de mando durante 4 segundos hasta que el indicador LED cambie a azul."
     - **sv**: "2.    Tryck ner kontrollknappen i 4 sekunder tills LED-indikatorn lyser blått."
     - **fi**: "2.    Paina nyt säädintä 4 sekuntia, kunnes LED-valo muuttuu siniseksi."
     - **zh-Hans**: "2.    按下控制点动按钮 4 秒，直到 LED 变为蓝色。"
     - **zh-Hant**: "2. 按住控制旋鈕 4 秒鐘，直到 LED 變藍。"
     - **id**: "2.    Tekan tombol kontrol selama 4 detik hingga lampu LED menyala biru."
     - **fil**: "2.    Pindutin ang control jog nang 4 segundo hanggang maging asul ang LED."
     - **ja**: "2.    LEDが青になるよう、コントロールジョグを4秒間押します。"
 */
  public static func enable_pairing_description_2() -> String {
     return localizedString(
         key:"enable_pairing_description_2",
         defaultValue:"Then press and hold again for 4 seconds until the light turns blue.",
         substitutions: [:])
  }

 /**
"Wait for FastPairing notification and proceed according to instructions."

     - **en**: "Wait for FastPairing notification and proceed according to instructions."
     - **da**: "Tryk på meddelelsen Google Fast Pair, og gør opsætningen færdig."
     - **nl**: "Klik op de Google Fast Pair-melding en rond het instellen af."
     - **de**: "Tippe auf die Google Fast Pair-Benachrichtigung und schließe die Einrichtung ab."
     - **fr**: "Appuyer sur la notification Google Fast Pair et terminez la configuration."
     - **it-IT**: "Toccare la notifica Google Fast Pair e completare la configurazione."
     - **nb**: "Trykk «Google Fast Pair»-varslingen og fullfør oppsett."
     - **pt-PT**: "Toque na notificação Google Fast Pair e conclua a configuração."
     - **ru**: "Нажмите на уведомление Google Fast Pair и завершите установку."
     - **es**: "Toca el aviso de Google Fast Pair y completa la configuración."
     - **sv**: "Klicka på Google Fast Pair-meddelandet och slutför installationen."
     - **fi**: "Napauta Google Fast Pair -ilmoitusta ja suorita asennus loppuun."
     - **zh-Hans**: "轻触 Google Fast Pair 通知并完成设置。"
     - **zh-Hant**: "點按 Google Fast Pair 通知並完成設定。"
     - **id**: "Ketuk pemberitahuan Google Fast Pair dan selesaikan pengaturan."
     - **fil**: "I-tap ang notipikasyon ng Google Fast Pair at kumpletuhin ang pag-setup."
     - **ja**: "Google Fast Pair の通知をタップして、セットアップを完了します。"
 */
  public static func enable_pairing_description_3() -> String {
     return localizedString(
         key:"enable_pairing_description_3",
         defaultValue:"Wait for FastPairing notification and proceed according to instructions.",
         substitutions: [:])
  }

 /**
"Tap \'Tap to finish\' dialog to finalise setup and restart the app"

     - **en**: "Tap \'Tap to finish\' dialog to finalise setup and restart the app"
     - **da**: "Tryk på \'Tryk for at afslutte\' for at færdiggøre opsætningen og genstarte appen"
     - **nl**: "Tik op \'Tik om af te ronden\' om het instellen af te ronden en herstart de app"
     - **de**: "Tippe auf \'Tap to finish\', um die Einrichtung abzuschließen und die App neu zu starten"
     - **fr**: "Appuyez sur \'Appuyer pour terminer\' pour finaliser la configuration et redémarrer l’application"
     - **it-IT**: "Toccare \“Tocca per completare\” per finalizzare la configurazione e riavviare l’app"
     - **nb**: "Trykk «Tap to finish» for å fullføre oppsett og starte opp appen på nytt"
     - **pt-PT**: "Toque em \'Toque para concluir\' para finalizar a configuração e reiniciar a aplicação"
     - **ru**: "Нажмите «Нажать для завершения», чтобы завершить установку, и перезапустите приложение"
     - **es**: "Toca \“Tocar para terminar\” para acabar la configuración y reiniciar la aplicación"
     - **sv**: "Klicka på \'Klicka för att slutföra\' för att avsluta installationen och starta om appen"
     - **fi**: "Napauta ”\Tap to finish\” suorittaaksesi asennuksen loppuun ja käynnistääksesi sovelluksen uudelleen"
     - **zh-Hans**: "轻触\“轻触以完成”\以完成设置并重新启动应用"
     - **zh-Hant**: "點按\'Tap to finish\'完成設定並重新啟動應用程式"
     - **id**: "Ketuk \'Tap to finish\' untuk menyelesaikan pengaturan dan memulai ulang aplikasi"
     - **fil**: "I-tap ang \'I-tap para tapusin\' upang matapos ang pag-setup at i-restart ang app"
     - **ja**: "［タップして終了］をタップして、セットアップを完了し、アプリを再起動します"
 */
  public static func enable_pairing_description_4() -> String {
     return localizedString(
         key:"enable_pairing_description_4",
         defaultValue:"Tap \'Tap to finish\' dialog to finalise setup and restart the app",
         substitutions: [:])
  }

 /**
"ALL FINISHED"

     - **en**: "ALL FINISHED"
     - **da**: "FÆRDIG"
     - **nl**: "HELEMAAL KLAAR"
     - **de**: "ALLES ERLEDIGT"
     - **fr**: "OPÉRATION FINALISÉE"
     - **it-IT**: "COMPLETATO"
     - **nb**: "HELT KLART"
     - **pt-PT**: "TUDO CONCLUÍDO"
     - **ru**: "ВСЕ ГОТОВО"
     - **es**: "FINALIZADO"
     - **sv**: "SLUTFÖRT"
     - **fi**: "KAIKKI ON VALMISTA"
     - **zh-Hans**: "全部完成"
     - **zh-Hant**: "已全部完成"
     - **id**: "SEMUA SELESAI"
     - **fil**: "TAPOS NA LAHAT"
     - **ja**: "すべて完了"
 */
  public static func enable_pairing_done_title_uc() -> String {
     return localizedString(
         key:"enable_pairing_done_title_uc",
         defaultValue:"ALL FINISHED",
         substitutions: [:])
  }

 /**
"PAIRING FAILED"

     - **en**: "PAIRING FAILED"
     - **da**: "PARRING MISLYKKEDES"
     - **nl**: "KOPPELEN MISLUKT"
     - **de**: "KOPPELN FEHLGESCHLAGEN"
     - **fr**: "ÉCHEC DE L’ASSOCIATION"
     - **it-IT**: "ASSOCIAZIONE NON RIUSCITA"
     - **nb**: "PARING MISLYKTES"
     - **pt-PT**: "O EMPARELHAMENTO FALHOU"
     - **ru**: "СБОЙ СОПРЯЖЕНИЯ"
     - **es**: "ERROR AL EMPAREJAR"
     - **sv**: "PARKOPPLING MISSLYCKADES"
     - **fi**: "PARINMUODOSTUS EPÄONNISTUI"
     - **zh-Hans**: "配对失败"
     - **zh-Hant**: "配對失敗"
     - **id**: "PAIRING GAGAL"
     - **fil**: "NABIGO ANG PAGPAPARES"
     - **ja**: "ペアリングに失敗しました"
 */
  public static func enable_pairing_failed_title_uc() -> String {
     return localizedString(
         key:"enable_pairing_failed_title_uc",
         defaultValue:"PAIRING FAILED",
         substitutions: [:])
  }

 /**
"FAST PAIRING"

     - **en**: "FAST PAIRING"
     - **da**: "FAST PAIRING"
     - **nl**: "SNEL KOPPELEN"
     - **de**: "SCHNELLES KOPPELN"
     - **fr**: "APPAIRAGE RAPIDE"
     - **it-IT**: "ABBINAMENTO RAPIDO"
     - **nb**: "RASK PARING"
     - **pt-PT**: "EMPARELHAMENTO RÁPIDO"
     - **ru**: "БЫСТРОЕ СОПРЯЖЕНИЕ"
     - **es**: "EMPAREJAMIENTO RÁPIDO"
     - **sv**: "FAST PAIRING"
     - **fi**: "LAITEPARIN PIKAMUODOSTUS"
     - **zh-Hans**: "快速配对"
     - **zh-Hant**: "快速配對"
     - **id**: "MODE BERPASANGAN CEPAT"
     - **fil**: "MABILISANG PAGPAPARES"
     - **ja**: "ファストペアリング"
 */
  public static func enable_pairing_ongoing_fast_pairing_uc() -> String {
     return localizedString(
         key:"enable_pairing_ongoing_fast_pairing_uc",
         defaultValue:"FAST PAIRING",
         substitutions: [:])
  }

 /**
"TURN ON PAIRING"

     - **en**: "TURN ON PAIRING"
     - **da**: "TÆND FOR PARRING"
     - **nl**: "KOPPELING INSCHAKELEN"
     - **de**: "KOPPLUNG AKTIVIEREN"
     - **fr**: "ACTIVER L’APPAIRAGE"
     - **it-IT**: "ATTIVA L’ACCOPPIAMENTO"
     - **nb**: "SLÅ PÅ PARING"
     - **pt-PT**: "LIGAR EMPARELHAMENTO"
     - **ru**: "ВКЛЮЧИТЬ СОПРЯЖЕНИЕ"
     - **es**: "ACTIVAR EMPAREJAMIENTO"
     - **sv**: "SLÅ PÅ PARKOPPLING"
     - **fi**: "MUODOSTA PARILIITOS"
     - **zh-Hans**: "打开配对"
     - **zh-Hant**: "開啟配對"
     - **id**: "AKTIFKAN MODE BERPASANGAN"
     - **fil**: "I-TURN ON ANG PAGPARES"
     - **ja**: "ペアリングをオンにする"
 */
  public static func enable_pairing_ongoing_title_uc() -> String {
     return localizedString(
         key:"enable_pairing_ongoing_title_uc",
         defaultValue:"TURN ON PAIRING",
         substitutions: [:])
  }

 /**
"160 Hz"

     - **en**: "160 Hz"
     - **da**: "160 Hz"
     - **nl**: "160 Hz"
     - **de**: "160 Hz"
     - **fr**: "160 Hz"
     - **it-IT**: "160 Hz"
     - **nb**: "160 Hz"
     - **pt-PT**: "160 Hz"
     - **ru**: "160 Hz"
     - **es**: "160 Hz"
     - **sv**: "160 Hz"
     - **fi**: "160 Hz"
     - **zh-Hans**: "160 Hz"
     - **zh-Hant**: "160 Hz"
     - **id**: "160 Hz"
     - **fil**: "160 Hz"
     - **ja**: "160 Hz"
 */
  public static func equaliser_screen_preset_bass() -> String {
     return localizedString(
         key:"equaliser_screen_preset_bass",
         defaultValue:"160 Hz",
         substitutions: [:])
  }

 /**
"CUSTOM"

     - **en**: "CUSTOM"
     - **da**: "TILPASSET"
     - **nl**: "OP MAAT"
     - **de**: "BENUTZERDEFINIERT"
     - **fr**: "PERSONNALISÉ"
     - **it-IT**: "PERSONALIZZATA"
     - **nb**: "CUSTOM"
     - **pt-PT**: "PERSONALIZADO"
     - **ru**: "ИНДИВИДУАЛЬНЫЕ НАСТРОЙКИ"
     - **es**: "PERSONALIZAR"
     - **sv**: "ANPASSAD"
     - **fi**: "RÄÄTÄLÖITY"
     - **zh-Hans**: "自定义"
     - **zh-Hant**: "自訂"
     - **id**: "CUSTOM"
     - **fil**: "PASADYA"
     - **ja**: "カスタム"
 */
  public static func equaliser_screen_preset_custom_uc() -> String {
     return localizedString(
         key:"equaliser_screen_preset_custom_uc",
         defaultValue:"CUSTOM",
         substitutions: [:])
  }

 /**
"ELECTRONIC"

     - **en**: "ELECTRONIC"
     - **da**: "ELECTRONIC"
     - **nl**: "ELEKTRONISCH"
     - **de**: "ELECTRONIC"
     - **fr**: "ELECTRONIC"
     - **it-IT**: "ELETTRONICA"
     - **nb**: "ELECTRONIC"
     - **pt-PT**: "ELETRÓNICA"
     - **ru**: "ЭЛЕКТРОННАЯ"
     - **es**: "ELECTRÓNICA"
     - **sv**: "ELEKTRONISK"
     - **fi**: "ELEKTRONINEN"
     - **zh-Hans**: "电子"
     - **zh-Hant**: "電子"
     - **id**: "ELECTRONIC"
     - **fil**: "ELECTRONIC"
     - **ja**: "エレクトロニック"
 */
  public static func equaliser_screen_preset_electronic_uc() -> String {
     return localizedString(
         key:"equaliser_screen_preset_electronic_uc",
         defaultValue:"ELECTRONIC",
         substitutions: [:])
  }

 /**
"FLAT"

     - **en**: "FLAT"
     - **da**: "FLAD"
     - **nl**: "FLAT"
     - **de**: "FLACH"
     - **fr**: "FLAT"
     - **it-IT**: "PIATTA"
     - **nb**: "FLAT"
     - **pt-PT**: "FLAT"
     - **ru**: "FLAT"
     - **es**: "PLANO"
     - **sv**: "RAK"
     - **fi**: "FLAT"
     - **zh-Hans**: "均衡"
     - **zh-Hant**: "降"
     - **id**: "FLAT"
     - **fil**: "FLAT"
     - **ja**: "フラット"
 */
  public static func equaliser_screen_preset_flat_uc() -> String {
     return localizedString(
         key:"equaliser_screen_preset_flat_uc",
         defaultValue:"FLAT",
         substitutions: [:])
  }

 /**
"6.25 kHz"

     - **en**: "6.25 kHz"
     - **da**: "6,25 kHz"
     - **nl**: "6,25 kHz"
     - **de**: "6,25 kHz"
     - **fr**: "6,25 kHz"
     - **it-IT**: "6,25 kHz"
     - **nb**: "6,25 kHz"
     - **pt-PT**: "6,25 kHz"
     - **ru**: "6,25 kHz"
     - **es**: "6,25 kHz"
     - **sv**: "6,25 kHz"
     - **fi**: "6,25 kHz"
     - **zh-Hans**: "6,25 kHz"
     - **zh-Hant**: "6,25 kHz"
     - **id**: "6,25 kHz"
     - **fil**: "6,25 kHz"
     - **ja**: "6,25 kHz"
 */
  public static func equaliser_screen_preset_high() -> String {
     return localizedString(
         key:"equaliser_screen_preset_high",
         defaultValue:"6.25 kHz",
         substitutions: [:])
  }

 /**
"HIP-HOP"

     - **en**: "HIP-HOP"
     - **da**: "HIP-HOP"
     - **nl**: "HIP-HOP"
     - **de**: "HIP-HOP"
     - **fr**: "HIP-HOP"
     - **it-IT**: "HIP-HOP"
     - **nb**: "HIP-HOP"
     - **pt-PT**: "HIP-HOP"
     - **ru**: "ХИП-ХОП"
     - **es**: "HIP-HOP"
     - **sv**: "HIP HOP"
     - **fi**: "HIPHOP"
     - **zh-Hans**: "嘻哈"
     - **zh-Hant**: "嘻哈"
     - **id**: "HIP-HOP"
     - **fil**: "HIP-HOP"
     - **ja**: "ヒップホップ"
 */
  public static func equaliser_screen_preset_hip_hop_uc() -> String {
     return localizedString(
         key:"equaliser_screen_preset_hip_hop_uc",
         defaultValue:"HIP-HOP",
         substitutions: [:])
  }

 /**
"JAZZ"

     - **en**: "JAZZ"
     - **da**: "JAZZ"
     - **nl**: "JAZZ"
     - **de**: "JAZZ"
     - **fr**: "JAZZ"
     - **it-IT**: "JAZZ"
     - **nb**: "JAZZ"
     - **pt-PT**: "JAZZ"
     - **ru**: "ДЖАЗ"
     - **es**: "JAZZ"
     - **sv**: "JAZZ"
     - **fi**: "JAZZ"
     - **zh-Hans**: "爵士"
     - **zh-Hant**: "爵士"
     - **id**: "JAZZ"
     - **fil**: "JAZZ"
     - **ja**: "ジャズ"
 */
  public static func equaliser_screen_preset_jazz_uc() -> String {
     return localizedString(
         key:"equaliser_screen_preset_jazz_uc",
         defaultValue:"JAZZ",
         substitutions: [:])
  }

 /**
"400 Hz"

     - **en**: "400 Hz"
     - **da**: "400 Hz"
     - **nl**: "400 Hz"
     - **de**: "400 Hz"
     - **fr**: "400 Hz"
     - **it-IT**: "400 Hz"
     - **nb**: "400 Hz"
     - **pt-PT**: "400 Hz"
     - **ru**: "400 Hz"
     - **es**: "400 Hz"
     - **sv**: "400 Hz"
     - **fi**: "400 Hz"
     - **zh-Hans**: "400 Hz"
     - **zh-Hant**: "400 Hz"
     - **id**: "400 Hz"
     - **fil**: "400 Hz"
     - **ja**: "400 Hz"
 */
  public static func equaliser_screen_preset_low() -> String {
     return localizedString(
         key:"equaliser_screen_preset_low",
         defaultValue:"400 Hz",
         substitutions: [:])
  }

 /**
"MARSHALL"

     - **en**: "MARSHALL"
     - **da**: "MARSHALL"
     - **nl**: "MARSHALL"
     - **de**: "MARSHALL"
     - **fr**: "MARSHALL"
     - **it-IT**: "MARSHALL"
     - **nb**: "MARSHALL"
     - **pt-PT**: "MARSHALL"
     - **ru**: "MARSHALL"
     - **es**: "MARSHALL"
     - **sv**: "MARSHALL"
     - **fi**: "MARSHALL"
     - **zh-Hans**: "MARSHALL"
     - **zh-Hant**: "MARSHALL"
     - **id**: "MARSHALL"
     - **fil**: "MARSHALL"
     - **ja**: "MARSHALL"
 */
  public static func equaliser_screen_preset_marshall_uc() -> String {
     return localizedString(
         key:"equaliser_screen_preset_marshall_uc",
         defaultValue:"MARSHALL",
         substitutions: [:])
  }

 /**
"METAL"

     - **en**: "METAL"
     - **da**: "METAL"
     - **nl**: "METAL"
     - **de**: "METAL"
     - **fr**: "METAL"
     - **it-IT**: "METAL"
     - **nb**: "METAL"
     - **pt-PT**: "METAL"
     - **ru**: "МЕТАЛЛ"
     - **es**: "METAL"
     - **sv**: "METAL"
     - **fi**: "METALLI"
     - **zh-Hans**: "金属"
     - **zh-Hant**: "重金屬"
     - **id**: "METAL"
     - **fil**: "METAL"
     - **ja**: "メタル"
 */
  public static func equaliser_screen_preset_metal_uc() -> String {
     return localizedString(
         key:"equaliser_screen_preset_metal_uc",
         defaultValue:"METAL",
         substitutions: [:])
  }

 /**
"1 kHz"

     - **en**: "1 kHz"
     - **da**: "1 kHz"
     - **nl**: "1 kHz"
     - **de**: "1 kHz"
     - **fr**: "1 kHz"
     - **it-IT**: "1 kHz"
     - **nb**: "1 kHz"
     - **pt-PT**: "1 kHz"
     - **ru**: "1 kHz"
     - **es**: "1 kHz"
     - **sv**: "1 kHz"
     - **fi**: "1 kHz"
     - **zh-Hans**: "1 kHz"
     - **zh-Hant**: "1 kHz"
     - **id**: "1 kHz"
     - **fil**: "1 kHz"
     - **ja**: "1 kHz"
 */
  public static func equaliser_screen_preset_mid() -> String {
     return localizedString(
         key:"equaliser_screen_preset_mid",
         defaultValue:"1 kHz",
         substitutions: [:])
  }

 /**
"POP"

     - **en**: "POP"
     - **da**: "POP"
     - **nl**: "POP"
     - **de**: "POP"
     - **fr**: "POP"
     - **it-IT**: "POP"
     - **nb**: "POP"
     - **pt-PT**: "POP"
     - **ru**: "ПОП"
     - **es**: "POP"
     - **sv**: "POP"
     - **fi**: "POP"
     - **zh-Hans**: "流行"
     - **zh-Hant**: "流行"
     - **id**: "POP"
     - **fil**: "POP"
     - **ja**: "ポップ"
 */
  public static func equaliser_screen_preset_pop_uc() -> String {
     return localizedString(
         key:"equaliser_screen_preset_pop_uc",
         defaultValue:"POP",
         substitutions: [:])
  }

 /**
"ROCK"

     - **en**: "ROCK"
     - **da**: "ROCK"
     - **nl**: "ROCK"
     - **de**: "ROCK"
     - **fr**: "ROCK"
     - **it-IT**: "ROCK"
     - **nb**: "ROCK"
     - **pt-PT**: "ROCK"
     - **ru**: "РОК"
     - **es**: "ROCK"
     - **sv**: "ROCK"
     - **fi**: "ROCK"
     - **zh-Hans**: "摇滚"
     - **zh-Hant**: "搖滾"
     - **id**: "ROCK"
     - **fil**: "ROCK"
     - **ja**: "ロック"
 */
  public static func equaliser_screen_preset_rock_uc() -> String {
     return localizedString(
         key:"equaliser_screen_preset_rock_uc",
         defaultValue:"ROCK",
         substitutions: [:])
  }

 /**
"SPOKEN"

     - **en**: "SPOKEN"
     - **da**: "TALE"
     - **nl**: "GESPROKEN"
     - **de**: "SPRACHE"
     - **fr**: "SPOKEN"
     - **it-IT**: "VOCALE"
     - **nb**: "SNAKKET"
     - **pt-PT**: "FALADO"
     - **ru**: "ГОЛОСОВОЙ"
     - **es**: "HABLADO"
     - **sv**: "SPOKEN"
     - **fi**: "PUHUTTUNA"
     - **zh-Hans**: "讲述"
     - **zh-Hant**: "已訴說"
     - **id**: "LISAN"
     - **fil**: "SINASALITA"
     - **ja**: "SPOKEN"
 */
  public static func equaliser_screen_preset_spoken_uc() -> String {
     return localizedString(
         key:"equaliser_screen_preset_spoken_uc",
         defaultValue:"SPOKEN",
         substitutions: [:])
  }

 /**
"2.5 kHz"

     - **en**: "2.5 kHz"
     - **da**: "2,5 kHz"
     - **nl**: "2,5 kHz"
     - **de**: "2,5 kHz"
     - **fr**: "2,5 kHz"
     - **it-IT**: "2,5 kHz"
     - **nb**: "2,5 kHz"
     - **pt-PT**: "2,5 kHz"
     - **ru**: "2,5 kHz"
     - **es**: "2,5 kHz"
     - **sv**: "2,5 kHz"
     - **fi**: "2,5 kHz"
     - **zh-Hans**: "2,5 kHz"
     - **zh-Hant**: "2,5 kHz"
     - **id**: "2,5 kHz"
     - **fil**: "2,5 kHz"
     - **ja**: "2,5 kHz"
 */
  public static func equaliser_screen_preset_upper() -> String {
     return localizedString(
         key:"equaliser_screen_preset_upper",
         defaultValue:"2.5 kHz",
         substitutions: [:])
  }

 /**
"Adjust the audio of your speaker."

     - **en**: "Adjust the audio of your speaker."
     - **da**: "Juster lyden i din højttaler"
     - **nl**: "Pas het geluid van je luidspreker aan."
     - **de**: "Stelle den Klang deines Lautsprechers ein."
     - **fr**: "Réglage audio de votre enceinte."
     - **it-IT**: "Regola l’audio del tuo diffusore."
     - **nb**: "Juster lyden til høyttaleren."
     - **pt-PT**: "Ajuste o áudio do seu altifalante."
     - **ru**: "Отрегулировать аудиопараметры колонки."
     - **es**: "Ajusta el sonido de tu altavoz."
     - **sv**: "Justera ljudet på din högtalare"
     - **fi**: "Säädä kaiuttimesi ääntä."
     - **zh-Hans**: "调整音箱的音效"
     - **zh-Hant**: "調整喇叭的音訊。"
     - **id**: "Sesuaikan audio speaker Anda."
     - **fil**: "I-adjust ang audio ng iyong speaker."
     - **ja**: "お使いのスピーカーのオーディオを調節します。"
 */
  public static func equaliser_screen_subtitle() -> String {
     return localizedString(
         key:"equaliser_screen_subtitle",
         defaultValue:"Adjust the audio of your speaker.",
         substitutions: [:])
  }

 /**
"The original Marshall sound."

     - **en**: "The original Marshall sound."
     - **da**: "Den originale Marshall-lyd."
     - **nl**: "Het originele Marshall-geluid."
     - **de**: "Der Original Marshall-Sound."
     - **fr**: "Le son Marshall original."
     - **it-IT**: "Il suono originale Marshall."
     - **nb**: "Den originale Marshall-lyden."
     - **pt-PT**: "O som original Marshall."
     - **ru**: "Оригинальный звук Marshall."
     - **es**: "El sonido original de Marshall."
     - **sv**: "Det ursprungliga Marshall-ljudet."
     - **fi**: "Alkuperäinen Marshall-ääni."
     - **zh-Hans**: "Marshall 原声。"
     - **zh-Hant**: "原創的 Marshall 音質。"
     - **id**: "Suara Marshall yang asli."
     - **fil**: "Ang orihinal na Marshall sound."
     - **ja**: "オリジナルMarshallサウンド。"
 */
  public static func equaliser_settings_screen_m1_subtitle() -> String {
     return localizedString(
         key:"equaliser_settings_screen_m1_subtitle",
         defaultValue:"The original Marshall sound.",
         substitutions: [:])
  }

 /**
"Select a preset or create your own."

     - **en**: "Select a preset or create your own."
     - **da**: "Vælg en forudindstilling, eller opret din egen."
     - **nl**: "Selecteer een preset of creëer je eigen preset."
     - **de**: "Wähle eine Voreinstellung oder erstelle deine eigene."
     - **fr**: "Sélectionnez un préréglage ou créez le vôtre."
     - **it-IT**: "Seleziona un preset o crea il tuo."
     - **nb**: "Velg en forhåndsinnstilling eller lag din egen."
     - **pt-PT**: "Selecione uma predefinição ou crie a sua."
     - **ru**: "Выберите настройку или создайте свою."
     - **es**: "Selecciona una preselección o crea una propia."
     - **sv**: "Välj en förinställning eller skapa din egen."
     - **fi**: "Valitse esiasetus tai luo omasi."
     - **zh-Hans**: "选择一项预设或创建您自己的预设。"
     - **zh-Hant**: "選擇或創建一個屬於自己的預設或。"
     - **id**: "Pilih preset atau buat baru."
     - **fil**: "Pumili ng preset o lumikha ng sarili mo."
     - **ja**: "プリセットを選択または自分だけの設定を作成します。"
 */
  public static func equaliser_settings_screen_m2_m3_subtitle() -> String {
     return localizedString(
         key:"equaliser_settings_screen_m2_m3_subtitle",
         defaultValue:"Select a preset or create your own.",
         substitutions: [:])
  }

 /**
"Try again"

     - **en**: "Try again"
     - **da**: "Prøv igen"
     - **nl**: "Nogmaals proberen"
     - **de**: "Erneut versuchen"
     - **fr**: "Réessayer"
     - **it-IT**: "Riprova"
     - **nb**: "Prøv igjen"
     - **pt-PT**: "Tentar novamente"
     - **ru**: "Попробовать еще раз"
     - **es**: "Volver a intentarlo"
     - **sv**: "Försök igen"
     - **fi**: "Yritä uudelleen"
     - **zh-Hans**: "重试"
     - **zh-Hant**: "重試"
     - **id**: "Coba lagi"
     - **fil**: "Subukan muli"
     - **ja**: "もう一度試してください"
 */
  public static func error_action_try_again() -> String {
     return localizedString(
         key:"error_action_try_again",
         defaultValue:"Try again",
         substitutions: [:])
  }

 /**
"Something went wrong."

     - **en**: "Something went wrong."
     - **da**: "Noget gik galt."
     - **nl**: "Er is iets fout gegaan."
     - **de**: "Etwas ist fehlgeschlagen."
     - **fr**: "Une erreur s’est produite."
     - **it-IT**: "Qualcosa è andato storto."
     - **nb**: "Noe gikk galt."
     - **pt-PT**: "Ocorreu um problema."
     - **ru**: "Возникла ошибка."
     - **es**: "Ha ocurrido un error."
     - **sv**: "Ett fel inträffade."
     - **fi**: "Jokin meni vikaan."
     - **zh-Hans**: "出现错误。"
     - **zh-Hant**: "發生錯誤。"
     - **id**: "Terjadi kesalahan."
     - **fil**: "May maling naganap."
     - **ja**: "問題が発生しました。"
 */
  public static func error_common() -> String {
     return localizedString(
         key:"error_common",
         defaultValue:"Something went wrong.",
         substitutions: [:])
  }

 /**
"Error while updating speaker."

     - **en**: "Error while updating speaker."
     - **da**: "Fejl under opdatering af højttaler."
     - **nl**: "Fout tijdens updaten van luidsprekers."
     - **de**: "Fehler bei der Aktualisierung des Lautsprechers."
     - **fr**: "Une erreur est survenue lors de la mise à jour de l’enceinte."
     - **it-IT**: "Errore nell’aggiornamento del diffusore."
     - **nb**: "Feil under oppdatering av høyttaler."
     - **pt-PT**: "Erro ao atualizar o altifalante."
     - **ru**: "Ошибка при обновлении программного обеспечения акустической системы."
     - **es**: "Error al actualizar altavoz."
     - **sv**: "Fel vid uppdatering av högtalare."
     - **fi**: "Virhe päivitettäessä kaiutinta."
     - **zh-Hans**: "更新音箱时出错。"
     - **zh-Hant**: "在更新喇叭時發生錯誤。"
     - **id**: "Kesalahan saat memperbarui speaker."
     - **fil**: "Mali sa pag-update ng speaker."
     - **ja**: "スピーカーの更新中にエラーが発生しました。"
 */
  public static func error_firmware_update_subtitle() -> String {
     return localizedString(
         key:"error_firmware_update_subtitle",
         defaultValue:"Error while updating speaker.",
         substitutions: [:])
  }

 /**
"FIRMWARE UPDATE ERROR"

     - **en**: "FIRMWARE UPDATE ERROR"
     - **da**: "FEJL VED FIRMWARE-OPDATERING"
     - **nl**: "FOUT BIJ FIRMWARE-UPDATE"
     - **de**: "FEHLER BEI FIRMWARE-UPDATE"
     - **fr**: "ERREUR DE MISE À JOUR DU FIRMWARE"
     - **it-IT**: "ERRORE NELL’AGGIORNAMENTO DEL FIRMWARE"
     - **nb**: "FEIL VED OPPDATERING AV FASTVARE"
     - **pt-PT**: "ERRO DE ATUALIZAÇÃO DE FIRMWARE"
     - **ru**: "ОШИБКА ОБНОВЛЕНИЯ ПРОШИВКИ"
     - **es**: "ERROR DE ACTUALIZACIÓN DE FIRMWARE"
     - **sv**: "FEL VID UPPDATERING AV FIRMWARE"
     - **fi**: "LAITEOHJELMISTON PÄIVITYSVIRHE"
     - **zh-Hans**: "固件更新错误"
     - **zh-Hant**: "韌體更新錯誤"
     - **id**: "KESALAHAN PEMBARUAN FIRMWARE"
     - **fil**: "MALI SA PAG-UPDATE NG FIRMWARE"
     - **ja**: "ファームウェア更新エラー"
 */
  public static func error_firmware_update_title_uc() -> String {
     return localizedString(
         key:"error_firmware_update_title_uc",
         defaultValue:"FIRMWARE UPDATE ERROR",
         substitutions: [:])
  }

 /**
"Go to Settings on this device, turn on Bluetooth and try again."

     - **en**: "Go to Settings on this device, turn on Bluetooth and try again."
     - **da**: "Gå til Indstillinger på denne enhed, aktivér Bluetooth og prøv igen."
     - **nl**: "Ga naar Instellingen op dit apparaat, schakel Bluetooth in en probeer het opnieuw."
     - **de**: "Gehe in die Einstellungen für dieses Gerät, schalte Bluetooth ein und versuche es erneut."
     - **fr**: "Accédez aux paramètres de cet appareil, activez le Bluetooth et réessayez."
     - **it-IT**: "Vai alle Impostazioni del dispositivo, attiva il Bluetooth e prova di nuovo."
     - **nb**: "Gå til Innstillinger på denne enheten, slå på Bluetooth og prøv igjen."
     - **pt-PT**: "Vá a Definições neste dispositivo, ligue o Bluetooth e tente novamente."
     - **ru**: "Перейдите в меню «Параметры» на этом устройстве, включите Bluetooth и попробуйте еще раз."
     - **es**: "Abre los ajustes de este dispositivo, enciende el Bluetooth y vuelve a intentarlo."
     - **sv**: "Gå till Inställningar på den här enheten, sätt på Bluetooth och försök igen."
     - **fi**: "Mene laitteen asetuksiin, aseta Bluetooth päälle ja yritä uudelleen."
     - **zh-Hans**: "前往设备的“设置”部分，打开蓝牙并重试。"
     - **zh-Hant**: "前往此裝置的設定，開啟藍牙然後再試一次。"
     - **id**: "Buka Pengaturan dalam perangkat ini, nyalakan Bluetooth, dan coba lagi."
     - **fil**: "Pumunta sa Mga Setting sa device na ito, i-on ang Bluetooth at subukan muli."
     - **ja**: "デバイスの「設定」に進み、Bluetoothをオンにしてからもう一度試してください"
 */
  public static func error_no_bluetooth_subtitle() -> String {
     return localizedString(
         key:"error_no_bluetooth_subtitle",
         defaultValue:"Go to Settings on this device, turn on Bluetooth and try again.",
         substitutions: [:])
  }

 /**
"BLUETOOTH IS OFF"

     - **en**: "BLUETOOTH IS OFF"
     - **da**: "BLUETOOTH ER DEAKTIVERET"
     - **nl**: "BLUETOOTH IS UITGESCHAKELD"
     - **de**: "BLUETOOTH IST AUSGESCHALTET"
     - **fr**: "BLUETOOTH DÉSACTIVÉ"
     - **it-IT**: "IL BLUETOOTH È DISATTIVATO"
     - **nb**: "BLUETOOTH ER AV"
     - **pt-PT**: "O BLUETOOTH ESTÁ DESLIGADO"
     - **ru**: "BLUETOOTH ВЫКЛЮЧЕН"
     - **es**: "BLUETOOTH ESTÁ APAGADO"
     - **sv**: "BLUETOOTH ÄR AV"
     - **fi**: "BLUETOOTH ON POIS PÄÄLTÄ"
     - **zh-Hans**: "蓝牙关闭"
     - **zh-Hant**: "藍牙為關閉"
     - **id**: "BLUETOOTH MATI"
     - **fil**: "NAKA-OFF ANG BLUETOOTH"
     - **ja**: "BLUETOOTHはオフになっています"
 */
  public static func error_no_bluetooth_title_uc() -> String {
     return localizedString(
         key:"error_no_bluetooth_title_uc",
         defaultValue:"BLUETOOTH IS OFF",
         substitutions: [:])
  }

 /**
"We could not find any devices to connect to.\n To see if your devices is compatible head over to the help section."

     - **en**: "We could not find any devices to connect to.\n To see if your devices is compatible head over to the help section."
     - **da**: "Vi kunne ikke finde nogen enheder at forbinde til.\nKig i sektionen Hjælp for at se, om dine enheder er kompatible."
     - **nl**: "We hebben geen apparaten kunnen vinden waarmee we verbinding konden maken.\nGa naar het helpgedeelte om te zien of je apparaten compatibel zijn."
     - **de**: "Es wurden keine Geräte zum Verbinden gefunden.\nUm zu sehen, ob dein Gerät kompatibel ist, wechsle in den Hilfebereich."
     - **fr**: "Nous n’avons trouvé aucun appareil auquel se connecter.\nPour vérifier la compatibilité de vos appareils, consultez la section Aide."
     - **it-IT**: "Non abbiamo rilevato nessun dispositivo da connettere.\nVerifica la compatibilità del tuo dispositivo nella sezione Aiuto."
     - **nb**: "Vi kunne ikke finne noen enheter til å koble til.\nFor å se om enhetene dine er kompatible, kan du gå til hjelpeseksjonen."
     - **pt-PT**: "Não conseguimos encontrar nenhum dispositivo para se ligar.\nPara ver se os seus dispositivos são compatíveis vá para a secção de ajuda."
     - **ru**: "Мы не нашли никаких устройств для подключения.\nЧтобы проверить совместимость устройств, перейдите в раздел «Помощь»."
     - **es**: "No hemos encontrado ningún dispositivo para conectarnos.\nPara ver si tus dispositivos son compatibles, consulta la sección de ayuda."
     - **sv**: "Vi kunde inte hitta några enheter att ansluta till.\nFör att se om dina enheter är kompatibla, gå till hjälpsektionen."
     - **fi**: "Emme löytäneet yhtään yhdistettävää laitetta.\nKatso ohje tarkistaaksesi laitteesi yhteensopivuus."
     - **zh-Hans**: "我们未找到任何可连接的设备。\n如要查看您的设备是否兼容，请查看“帮助”部分。"
     - **zh-Hant**: "我們找不到可連接的任何裝置。\n若要檢視您的裝置是否相容，請前往說明區段。"
     - **id**: "Kami tidak dapat menemukan perangkat apa pun untuk dihubungkan.\nUntuk melihat jika perangkat Anda kompatibel, buka bagian bantuan."
     - **fil**: "Wala kaming mahanap na device na kokonektahan.\nPara makita kung compatible ang iyong mga device, pumunta sa seksiyon ng tulong ."
     - **ja**: "接続するデバイスが見つかりませんでした。\nご使用のデバイスに対応しているかを確認するには、ヘルプセクションをご覧ください。"
 */
  public static func error_no_devices_found_long_subtitle() -> String {
     return localizedString(
         key:"error_no_devices_found_long_subtitle",
         defaultValue:"We could not find any devices to connect to.\n To see if your devices is compatible head over to the help section.",
         substitutions: [:])
  }

 /**
"Pull down to refresh."

     - **en**: "Pull down to refresh."
     - **da**: "Træk ned for at opdatere."
     - **nl**: "Trek naar beneden om te verversen."
     - **de**: "Ziehe die Liste herunter, um zu aktualisieren."
     - **fr**: "Dérouler pour rafraîchir."
     - **it-IT**: "Tira verso il basso per aggiornare."
     - **nb**: "Trekk ned for å oppdatere."
     - **pt-PT**: "Puxar para baixo para atualizar."
     - **ru**: "Потянуть вниз для обновления."
     - **es**: "Desliza hacia abajo para actualizar la pantalla."
     - **sv**: "Dra nedåt för att uppdatera."
     - **fi**: "Vedä alaspäin päivittääksesi."
     - **zh-Hans**: "下拉以刷新。"
     - **zh-Hant**: "向下拉以重新整理。"
     - **id**: "Tarik ke bawah untuk memuat ulang."
     - **fil**: "Hatakin pababa para i-refresh."
     - **ja**: "更新するには、プルダウンしてください。"
 */
  public static func error_no_devices_found_short_subtitle() -> String {
     return localizedString(
         key:"error_no_devices_found_short_subtitle",
         defaultValue:"Pull down to refresh.",
         substitutions: [:])
  }

 /**
"NO DEVICES FOUND"

     - **en**: "NO DEVICES FOUND"
     - **da**: "INGEN ENHEDER FUNDET"
     - **nl**: "GEEN APPARATEN GEVONDEN"
     - **de**: "KEIN GERÄT GEFUNDEN"
     - **fr**: "AUCUN APPAREIL TROUVÉ"
     - **it-IT**: "NESSUN DISPOSITIVO RILEVATO"
     - **nb**: "FANT INGEN ENHETER"
     - **pt-PT**: "NÃO FOI ENCONTRADO NENHUM DISPOSITIVO"
     - **ru**: "УСТРОЙСТВА НЕ НАЙДЕНЫ"
     - **es**: "NO SE HAN ENCONTRADO DISPOSITIVOS"
     - **sv**: "INGA ENHETER HITTADES"
     - **fi**: "LAITTEITA EI LÖYTYNYT"
     - **zh-Hans**: "未找到设备"
     - **zh-Hant**: "找不到裝置"
     - **id**: "PERANGKAT TIDAK DITEMUKAN"
     - **fil**: "WALANG NAHANAP NA MGA DEVICE"
     - **ja**: "デバイスが見つかりません"
 */
  public static func error_no_devices_found_title_uc() -> String {
     return localizedString(
         key:"error_no_devices_found_title_uc",
         defaultValue:"NO DEVICES FOUND",
         substitutions: [:])
  }

 /**
"Please check your Internet connection settings."

     - **en**: "Please check your Internet connection settings."
     - **da**: "Tjek indstillingerne for din internetforbindelse."
     - **nl**: "Controleer de instellingen van je internetverbinding."
     - **de**: "Bitte prüfe die Einstellungen für deine Internetverbindung."
     - **fr**: "Veuillez vérifier vos paramètres de connexion Internet."
     - **it-IT**: "Verifica le tue impostazioni di connessione Internet."
     - **nb**: "Sjekk innstillingene for Internett-tilkobling."
     - **pt-PT**: "Verifique as suas definições de ligação à Internet."
     - **ru**: "Проверьте свои настройки соединения с Интернетом."
     - **es**: "Comprueba los ajustes de tu conexión a internet."
     - **sv**: "Kontrollera inställningarna för din internetanslutning."
     - **fi**: "Tarkista Internet-yhteyden asetukset."
     - **zh-Hans**: "请检查您的互联网连接设置。"
     - **zh-Hant**: "請檢查您的網際網路連線設定。"
     - **id**: "Periksalah pengaturan koneksi Internet Anda."
     - **fil**: "Pakitingnan ang iyong koneksyon sa Internet."
     - **ja**: "インターネットの接続設定を確認してください。"
 */
  public static func error_no_internet_subtitle() -> String {
     return localizedString(
         key:"error_no_internet_subtitle",
         defaultValue:"Please check your Internet connection settings.",
         substitutions: [:])
  }

 /**
"NO INTERNET CONNECTION"

     - **en**: "NO INTERNET CONNECTION"
     - **da**: "INGEN INTERNETFORBINDELSE"
     - **nl**: "GEEN INTERNETVERBINDING"
     - **de**: "KEINE INTERNETVERBINDUNG"
     - **fr**: "AUCUNE CONNEXION INTERNET"
     - **it-IT**: "NESSUNA CONNESSIONE INTERNET"
     - **nb**: "INGEN INTERNETT-TILKOBLING"
     - **pt-PT**: "NÃO EXISTE LIGAÇÃO À INTERNET"
     - **ru**: "ОТСУТСТВУЕТ СОЕДИНЕНИЕ С ИНТЕРНЕТОМ"
     - **es**: "NO HAY CONEXIÓN A INTERNET"
     - **sv**: "INGEN INTERNETANSLUTNING"
     - **fi**: "EI INTERNET-YHTEYTTÄ"
     - **zh-Hans**: "未连接互联网"
     - **zh-Hant**: "沒有網際網路連線"
     - **id**: "TIDAK ADA KONEKSI INTERNET"
     - **fil**: "WALANG KONEKSIYON SA INTERNET "
     - **ja**: "インターネットに接続されていません"
 */
  public static func error_no_internet_title_uc() -> String {
     return localizedString(
         key:"error_no_internet_title_uc",
         defaultValue:"NO INTERNET CONNECTION",
         substitutions: [:])
  }

 /**
"We could not download the latest firmware, please check your internet connection and try again."

     - **en**: "We could not download the latest firmware, please check your internet connection and try again."
     - **da**: "Vi kunne ikke downloade den nyeste firmware. Tjek din internetforbindelse og prøv igen."
     - **nl**: "We hebben de laatste firmware niet kunnen downloaden, controleer uw internetverbinding en probeer het opnieuw."
     - **de**: "Die neueste Firmware konnte nicht heruntergeladen werden. Bitte überprüfe deine Internetverbindung und versuche es erneut."
     - **fr**: "Nous n’avons pas pu télécharger le dernier firmware. Veuillez vérifier votre connexion Internet et essayer à nouveau."
     - **it-IT**: "Non abbiamo potuto scaricare l’ultima versione del firmware. Verifica la tua connessione internet e riprova."
     - **nb**: "Vi kunne ikke laste ned den nyeste fastvaren, vennligst sjekk internettforbindelsen din og prøv igjen."
     - **pt-PT**: "Não foi possível descarregarmos a mais recente firmware, pelo que deve verificar a sua ligação à internet e tentar novamente."
     - **ru**: "Не удалось скачать последнюю версию прошивки, проверьте соединение с Интернетом и попробуйте еще раз."
     - **es**: "No hemos podido descargar la versión más reciente del firmware. Comprueba tu conexión a internet y vuelve a intentarlo."
     - **sv**: "Vi kunde inte ladda ner det senaste firmwaret, kontrollera din internetanslutning och försök igen."
     - **fi**: "Uusinta laiteohjelmistoa ei voitu ladata, tarkista Internet-yhteys ja yritä uudelleen."
     - **zh-Hans**: "我们无法下载最新固件，请检查您的互联网连接并重试。"
     - **zh-Hant**: "我們無法下載最新韌體，請檢查您的網際網路連線然後重試。"
     - **id**: "Kami tidak dapat mengunduh firmware terbaru. Periksa hubungan internet Anda dan coba lagi."
     - **fil**: "Hindi namin ma-download ang pinakabagong firmware, mangyaring tingnan ang iyong koneksyon sa internet at subukan muli."
     - **ja**: "最新ファームウェアのダウンロードができません。インターネットに接続していることを確認して再試行してください。"
 */
  public static func error_ota_downloading_description() -> String {
     return localizedString(
         key:"error_ota_downloading_description",
         defaultValue:"We could not download the latest firmware, please check your internet connection and try again.",
         substitutions: [:])
  }

 /**
"UPDATE FAILED!"

     - **en**: "UPDATE FAILED!"
     - **da**: "OPDATERING MISLYKKET!"
     - **nl**: "UPDATE MISLUKT!"
     - **de**: "UPDATE FEHLGESCHLAGEN!"
     - **fr**: "ÉCHEC DE LA MISE À JOUR !"
     - **it-IT**: "AGGIORNAMENTO NON RIUSCITO!"
     - **nb**: "FEIL VED OPPDATERING!"
     - **pt-PT**: "ATUALIZAÇÃO FALHOU!"
     - **ru**: "НЕ УДАЛОСЬ УСТАНОВИТЬ ОБНОВЛЕНИЕ!"
     - **es**: "¡ACTUALIZACIÓN FALLIDA!"
     - **sv**: "UPPDATERING MISSLYCKADES!"
     - **fi**: "PÄIVITYS EPÄONNISTUI!"
     - **zh-Hans**: "更新失败！"
     - **zh-Hant**: "更新失敗！"
     - **id**: "PEMBARUAN GAGAL!"
     - **fil**: "HINDI NA-UPDATE!"
     - **ja**: "アップデート失敗! "
 */
  public static func error_ota_undefined() -> String {
     return localizedString(
         key:"error_ota_undefined",
         defaultValue:"UPDATE FAILED!",
         substitutions: [:])
  }

 /**
"We ran into a problem updating to the latest firmware. Let's give it another try."

     - **en**: "We ran into a problem updating to the latest firmware. Let's give it another try."
     - **da**: "Vi havde problemer med opdateringen til den nyeste firmware. Lad os prøve igen."
     - **nl**: "Er is een probleem opgetreden bij het updaten naar de nieuwste firmware. Laten we het nog eens proberen."
     - **de**: "Beim Update auf die neueste Firmware ist ein Problem aufgetreten. Lass es uns erneut versuchen."
     - **fr**: "Nous avons rencontré un problème lors de la mise à jour du dernier firmware. Essayez à nouveau."
     - **it-IT**: "Abbiamo riscontrato un problema durante l’aggiornamento dell’ultima versione del firmware. Riprova."
     - **nb**: "Vi oppdaget et problem med å oppdatere til den nyeste fastvaren. La oss gi det et nytt forsøk."
     - **pt-PT**: "Tivemos um problema ao atualizar a mais recente firmware. Vamos tentar novamente."
     - **ru**: "При обновлении до последней прошивки произошла ошибка. Попробуйте еще раз."
     - **es**: "Hemos encontrado un problema al actualizar el firmware. Vamos a intentarlo de nuevo."
     - **sv**: "Ett problem inträffade vid uppdatering av det senaste firmwaret. Låt oss försöka igen."
     - **fi**: "Uusimman laiteohjelmiston päivityksessä ilmaantui ongelma. Yritetäänpä uudelleen."
     - **zh-Hans**: "我们在更新最新固件时遇到了问题。请重试。"
     - **zh-Hant**: "我們在更新至最新韌體時遭遇到問題。我們再試一次。"
     - **id**: "Kami mengalami masalah dalam memperbarui firmware. Ayo kita coba lagi."
     - **fil**: "Nagkaroon kami ng problema sa pag-update sa pinakabagong firmware. Subukan natin muli."
     - **ja**: "最新のファームウェアにアップデートする際に問題が発生しました。再試行してください。"
 */
  public static func error_ota_undefined_subtitle() -> String {
     return localizedString(
         key:"error_ota_undefined_subtitle",
         defaultValue:"We ran into a problem updating to the latest firmware. Let's give it another try.",
         substitutions: [:])
  }

 /**
"Forgetting the headphones will remove them from the device list in the home screen.\n\nYou can add the headphones by pairing them again."

     - **en**: "Forgetting the headphones will remove them from the device list in the home screen.\n\nYou can add the headphones by pairing them again."
     - **da**: "Ved at glemme hovedtelefonerne fjernes de fra listen over enheder på startskærmen.\n\nDu kan tilføje hovedtelefonerne igen ved at parre dem igen."
     - **nl**: "Als je de koptelefoon vergeet, verdwijnt hij van de lijst met apparaten op het startscherm.\n\nJe kunt de hoofdtelefoon weer toevoegen door hem opnieuw te koppelen."
     - **de**: "Wenn ein Kopfhörer vergessen wird, wird er aus der Geräteliste auf dem Startbildschirm entfernt.\n\nDu kannst den Kopfhörer wieder hinzufügen, indem du ihn erneut koppelst."
     - **fr**: "La fonction d’oubli supprime le casque de la liste des appareils sur l’écran d’accueil.\n\nVous pourrez ajouter le casque à nouveau en l’appairant une nouvelle fois."
     - **it-IT**: "La funzione Dimentica le cuffie le rimuove dall’elenco dei dispositivi sulla schermata principale.\n\nPuoi riaggiungere le cuffie abbinandole di nuovo."
     - **nb**: "Glemmer du hodetelefonene, vil de fjernes fra enhetslisten på startskjermen.\n\nDu kan legge til hodetelefonene igjen ved å pare dem på nytt."
     - **pt-PT**: "Ao esquecer o auscultador irá removê-lo da lista de dispositivos no ecrã inicial.\n\nPode voltar a adicionar o auscultador emparelhando-o novamente."
     - **ru**: "При этом они будут удалены из списка устройств на главном экране.\n\nМожно вновь добавить наушники при их повторном сопряжении."
     - **es**: "Si eliges olvidar el auricular desaparecerá de la lista de dispositivos incluida en la pantalla de inicio.\n\nPuedes añadirlo de nuevo volviendo a emparejarlo."
     - **sv**: "Glöm hörlurar kommer ta bort dem från enhetslistan på din startskärm.\n\nDu kan lägga till hörlurarna igen genom att parkoppla dem på nytt."
     - **fi**: "Kuulokkeiden unohtaminen poistaa ne aloitusnäytön laiteluettelosta.\n\nVoit lisätä kuulokkeet uudelleen muodostamalla uudelleen pariliitoksen."
     - **zh-Hans**: "忽略耳机将把它从主屏幕的设备列表中删除。\n\n您可以通过重新配对来重新添加耳机。"
     - **zh-Hant**: "忘記耳機會把耳機從主螢幕的設備列表中刪除。\n\n 您可以使用重新配對來再次添加耳機。"
     - **id**: "Melupakan headphone akan menghapus headphone dari daftar perangkat yang terletak di layar beranda.\n\nAnda dapat menambahkan headphone dengan memasangkannya kembali."
     - **fil**: "Ang pagkalimot sa headphone ay aalisin ito mula sa listahan ng device sa home screen.\n\nMaaari mo muling idagdag ang headphone sa pamamagitan ng muling pagpares."
     - **ja**: "ヘッドフォンを削除すると、デバイスはホームスクリーンのデバイスリストから削除されます。●●ペアリングすることでヘッドフォンを再び追加することができます。"
 */
  public static func forget_headphone_screen_subtitle() -> String {
     return localizedString(
         key:"forget_headphone_screen_subtitle",
         defaultValue:"Forgetting the headphones will remove them from the device list in the home screen.\n\nYou can add the headphones by pairing them again.",
         substitutions: [:])
  }

 /**
"Forgetting the speaker will remove it from the device list in the home screen.\nYou can later add the speaker by pairing it again."

     - **en**: "Forgetting the speaker will remove it from the device list in the home screen.\nYou can later add the speaker by pairing it again."
     - **da**: "Hvis du glemmer højttaleren vil den blive fjernet fra enhedslisten på startsiden.\nDu kan tilføje den senere ved at parre den igen."
     - **nl**: "Als je de luidspreker ”vergeet”, wordt deze uit de apparatenlijst op het beginscherm verwijderd.\nJe kunt de luidspreker later toevoegen door hem weer te koppelen."
     - **de**: "Wenn du den Lautsprecher vergisst, entfernt es das Gerät aus der Liste auf dem Homescreen.\nDu kannst den Lautsprecher später wieder hinzufügen, indem du ihn erneut verbindest."
     - **fr**: "Ignorer l’enceinte la supprimera de la liste d’appareils de l’écran d’accueil.\nVous pourrez rajouter l’enceinte ultérieurement en l’associant à nouveau."
     - **it-IT**: "Rimuovendo il diffusore lo si eliminerà dalla lista dei dispositivi nella schermata iniziale.\nPuoi aggiungere lo speaker abbinandolo di nuovo più tardi."
     - **nb**: "Hvis du glemmer høyttaleren fjernes den fra enhetslisten på startskjermen.\nDu kan senere legge til høyttaleren ved å pare den på nytt."
     - **pt-PT**: "Esquecendo o altifalante irá removê-lo da lista de dispositivos no ecrã inicial.\nPode adicionar mais tarde o altifalante emparelhando-o novamente."
     - **ru**: "Надо забыть колонку, чтобы удалить ее из списка устройств в главном окне.\nМожно добавить ее потом, установив сопряжение."
     - **es**: "Si olvidas el altavoz, desaparecerá de la lista de dispositivos incluida en la pantalla de inicio.\nPodrás volver a emparejar el altavoz de nuevo para añadirlo cuando quieras."
     - **sv**: "Glömma högtalaren innebär att de tas den bort från enhetslistan på startskärmen.\nDu kan lägga till högtalaren genom att parkoppla den igen."
     - **fi**: "Laitteen unohtaminen poistaa sen aloitusnäytön laiteluettelosta.\nVoit lisätä kaiuttimen myöhemmin muodostamalla laiteparin uudelleen."
     - **zh-Hans**: "忽略此音箱会使其从主屏幕的设备列表中移除。\n你稍后可以通过蓝牙重新配对来添加音箱。"
     - **zh-Hant**: "忘記喇叭會將其從主畫面中的裝置清單中移除。\n您可以稍後再配對一次以新增此喇叭。"
     - **id**: "Melupakan speaker akan menghapusnya dari daftar perangkat pada layar beranda.\nNantinya, Anda dapat menambahkan speaker dengan memasangkannya kembali."
     - **fil**: "Ang pagkalimot sa speaker ay mag-aalis nito mula sa listahan ng device sa home screen.\nMaidadagdag mo ito sa kalaunan sa pamamagitan ng muling pagpapares."
     - **ja**: "スピーカーを削除すると、ホーム画面のデバイスリストからそれが削除されます。\n再度ペアリングを行うことで、スピーカーを後から追加できます。"
 */
  public static func forget_screen_subtitle() -> String {
     return localizedString(
         key:"forget_screen_subtitle",
         defaultValue:"Forgetting the speaker will remove it from the device list in the home screen.\nYou can later add the speaker by pairing it again.",
         substitutions: [:])
  }

 /**
"The list of Free and Open Source Software can be viewed on the Marshall website."

     - **en**: "The list of Free and Open Source Software can be viewed on the Marshall website."
     - **da**: "Listen over gratis og open source-software kan ses på Marshalls hjemmeside."
     - **nl**: "De lijst met gratis en opensourcesoftware kan worden bekeken op de website van Marshall."
     - **de**: "Die Liste der kostenlosen und Open-Source-Software kann auf der Marshall-Website eingesehen werden."
     - **fr**: "La version complète du Logiciel libre à code source ouvert est disponible sur le site Internet de Marshall."
     - **it-IT**: "L’elenco dei software liberi e open source può essere visualizzato sul sito di Marshall."
     - **nb**: "Listen over gratis og åpen kildekodeprogramvare kan leses på Marshalls nettside."
     - **pt-PT**: "A lista do software gratuito e de código aberto pode ser visualizada no site da Marshall."
     - **ru**: "Ознакомиться с полной версией Программного обеспечения с открытым исходным кодом можно на веб-сайте Marshall."
     - **es**: "La lista de software gratuito y de código abierto puede consultarse en el sitio web de Marshall."
     - **sv**: "Listan över den fria programvaran och programvaran med öppen källkod finns att tillgå på Marshalls webbplats."
     - **fi**: "Vapaan ja avoimen lähdekoodin ohjelmiston luettelo löytyy Marshallin kotisivuilta."
     - **zh-Hans**: "可在 Marshall 网站上查看免费和开源软件列表。"
     - **zh-Hant**: "在 Marshall 網站上可檢視免費和開放源碼的軟體清單。"
     - **id**: "Daftar Perangkat Lunak Gratis dan Sumber Terbuka dapat dilihat di situs web Marshall."
     - **fil**: "Ang listahan ng Libre at Open Source Software ay makikita sa Marshall website."
     - **ja**: "フリーソフトウェアとオープンソフトウェアのリストはMarshallのウェブサイトでご覧いただけます。"
 */
  public static func foss_subtitle() -> String {
     return localizedString(
         key:"foss_subtitle",
         defaultValue:"The list of Free and Open Source Software can be viewed on the Marshall website.",
         substitutions: [:])
  }

 /**
"1.   Download the Google Assistant app.\n\n2.   Launch the app and complete the setup.\n\n3.   Close this screen."

     - **en**: "1.   Download the Google Assistant app.\n\n2.   Launch the app and complete the setup.\n\n3.   Close this screen."
     - **da**: "1.   Download Google Assistent-appen.\n\n2. Start appen, og fuldend opsætningen.\n\n3. Luk denne skærm."
     - **nl**: "1.   Download de Google Assistent-app.\n\n2.   Start de app en voltooi de setup.\n\n3.   Sluit dit scherm."
     - **de**: "1. Lade die Google Assistant App herunter.\n\n2. Starte die App und schließe die Einrichtung ab.\nn3. Schließe diesen Bildschirm."
     - **fr**: "1.   Téléchargez l’application Assistant Google\n\n2.   Lancez l’application et terminez la configuration.\n\n3.   Fermez la page."
     - **it-IT**: "1.   Scarica l’app Assistente Google.\n\n2.   Avvia l’app e completa la configurazione.\n\n3.   Chiudi questa schermata."
     - **nb**: "1.   Last ned Google Assistent-appen.\n\n2.   Åpne appen og fullfør konfigureringen.\n\n3.   Lukk denne skjermen."
     - **pt-PT**: "1.   Faça o download da aplicação Google Assistente.\n\n2.   Inicie a aplicação e conclua a definição.\n\n3.   Feche este ecrã."
     - **ru**: "1.   Скачайте приложение Google Assistant.\n\n2.   Запустте приложение и выполните настройку.\n\n3.   Закройте этот экран."
     - **es**: "1.   Descarga la aplicación Asistente de Google.\n\n2.   Abre la aplicación y completa la configuración.\n\n3.   Cierra esta pantalla."
     - **sv**: "1.   Ladda ner Google Assistent-appen.\n\n2.   Öppna appen och slutför installationen.\n\n3.   Stäng det här fönstret."
     - **fi**: "1.   Lataa Google Assistant -sovellus.\n\n2.   Käynnistä sovellus ja suorita käyttöönotto.\n\n3.   Sulje tämä näkymä."
     - **zh-Hans**: "1.   下载 Google Assistant 应用程序。\n\n2.   启动应用程序并完成设置。\n\n3.   关闭此屏幕。"
     - **zh-Hant**: "1.  下載 Google Assistant 應用程式。\n\n2.  啟動應用程式並完成設置。\n\n3.   關閉此畫面。"
     - **id**: "1.   Unduh aplikasi Google Assistant.\n\n2.   Jalankan aplikasi dan selesaikan pengaturan.\n\3.   Tutup layar ini."
     - **fil**: "1.   I-download ang Google Assistant app.\n\n2.   Ilunsad ang app at kumpletuhin ang setup.\n\n3.   Isara ang screen na ito."
     - **ja**: "1.   Googleアシスタントアプリをダウンロードする。●●2.   アプリを開き、設定を完了する。●●3.   スクリーンを閉じる。"
 */
  public static func google_assistant_screen_body() -> String {
     return localizedString(
         key:"google_assistant_screen_body",
         defaultValue:"1.   Download the Google Assistant app.\n\n2.   Launch the app and complete the setup.\n\n3.   Close this screen.",
         substitutions: [:])
  }

 /**
"Hey Google, play some rock and turn the volume to 10"

     - **en**: "Hey Google, play some rock and turn the volume to 10"
     - **da**: "Hey Google, play some rock and turn the volume to 10"
     - **nl**: "Hey Google, play some rock and turn the volume to 10"
     - **de**: "Hey Google, play some rock and turn the volume to 10"
     - **fr**: "Hey Google, play some rock and turn the volume to 10"
     - **it-IT**: "Hey Google, play some rock and turn the volume to 10"
     - **nb**: "Hey Google, play some rock and turn the volume to 10"
     - **pt-PT**: "Hey Google, play some rock and turn the volume to 10"
     - **ru**: "Hey Google, play some rock and turn the volume to 10"
     - **es**: "Hey Google, play some rock and turn the volume to 10"
     - **sv**: "Hey Google, play some rock and turn the volume to 10"
     - **fi**: "Hey Google, play some rock and turn the volume to 10"
     - **zh-Hans**: "Hey Google, play some rock and turn the volume to 10"
     - **zh-Hant**: "Hey Google, play some rock and turn the volume to 10"
     - **id**: "Hey Google, play some rock and turn the volume to 10"
     - **fil**: "Hey Google, play some rock and turn the volume to 10"
     - **ja**: "Hey Google, play some rock and turn the volume to 10"
 */
  public static func google_assistant_screen_cloud() -> String {
     return localizedString(
         key:"google_assistant_screen_cloud",
         defaultValue:"Hey Google, play some rock and turn the volume to 10",
         substitutions: [:])
  }

 /**
"Complete the setup to use the M-Button with your Google Assistant."

     - **en**: "Complete the setup to use the M-Button with your Google Assistant."
     - **da**: "Fuldend opsætningen for at bruge M-knappen med din Google Assistent."
     - **nl**: "Voltooi de setup om de M-knop te gebruiken met je Google Assistent."
     - **de**: "Schließe die Einrichtung ab, um die M-Taste mit deinem Google Assistant verwenden zu können."
     - **fr**: "Configurez le bouton M pour pouvoir l’utiliser avec votre Assistant Google."
     - **it-IT**: "Completa la configurazione per usare il pulsante M con l’Assistente Google."
     - **nb**: "Fullfør oppsettet for å bruke M-knappen med Google Assistent."
     - **pt-PT**: "Conclua a definição para usar o botão M com o seu Google Assistente."
     - **ru**: "Выполните настройку для использования M-кнопки с Google Assistant."
     - **es**: "Completa la configuración para usar el botón M con tu Asistente de Google."
     - **sv**: "Slutför installationen för att använda M-knappen med din Google Assistent."
     - **fi**: "Suorita käyttöönotto käyttääksesi M-painiketta Google Assistantin kanssa."
     - **zh-Hans**: "完成设置以将 M-按钮用于 Google Assistant。"
     - **zh-Hant**: "完成設置以將 M 按鈕與您的 Google Assistant 配合使用。"
     - **id**: "Selesaikan pengaturan untuk menggunakan M-Button dengan Google Assistant Anda."
     - **fil**: "Kumpletuhin ang setup upang magamit ang M-Button sa iyong Google Assistant."
     - **ja**: "MボタンとGoogleアシスタントを使用するために設定を完了する。"
 */
  public static func google_assistant_screen_header() -> String {
     return localizedString(
         key:"google_assistant_screen_header",
         defaultValue:"Complete the setup to use the M-Button with your Google Assistant.",
         substitutions: [:])
  }

 /**
"GOOGLE ASSISTANT"

     - **en**: "GOOGLE ASSISTANT"
     - **da**: "GOOGLE ASSISTENT"
     - **nl**: "GOOGLE ASSISTENT"
     - **de**: "GOOGLE ASSISTANT"
     - **fr**: "ASSISTANT GOOGLE"
     - **it-IT**: "ASSISTENTE GOOGLE"
     - **nb**: "GOOGLE ASSISTENT"
     - **pt-PT**: "GOOGLE ASSISTENTE"
     - **ru**: "GOOGLE ASSISTANT"
     - **es**: "ASISTENTE DE GOOGLE"
     - **sv**: "GOOGLE ASSISTENT"
     - **fi**: "GOOGLE ASSISTANT"
     - **zh-Hans**: "GOOGLE ASSISTANT"
     - **zh-Hant**: "GOOGLE ASSISTANT"
     - **id**: "GOOGLE ASSISTANT"
     - **fil**: "GOOGLE ASSISTANT"
     - **ja**: "GOOGLEアシスタント"
 */
  public static func google_assistant_screen_title_uc() -> String {
     return localizedString(
         key:"google_assistant_screen_title_uc",
         defaultValue:"GOOGLE ASSISTANT",
         substitutions: [:])
  }

 /**
"DISCONNECT"

     - **en**: "DISCONNECT"
     - **da**: "FRAKOBL"
     - **nl**: "VERBREKEN"
     - **de**: "TRENNEN"
     - **fr**: "DÉCONNECTER"
     - **it-IT**: "DISCONNETTI"
     - **nb**: "KOBLE FRA"
     - **pt-PT**: "DESLIGAR"
     - **ru**: "ОТКЛЮЧИТЬ"
     - **es**: "DESCONECTAR"
     - **sv**: "KOPPLA FRÅN"
     - **fi**: "KATKAISE YHTEYS"
     - **zh-Hans**: "断开连接"
     - **zh-Hant**: "取消連線"
     - **id**: "PUTUSKAN"
     - **fil**: "IDISKONEKTA"
     - **ja**: "接続解除"
 */
  public static func ios_appwide_disconnect_uc() -> String {
     return localizedString(
         key:"ios_appwide_disconnect_uc",
         defaultValue:"DISCONNECT",
         substitutions: [:])
  }

 /**
"OK"

     - **en**: "OK"
     - **da**: "OK"
     - **nl**: "OK"
     - **de**: "OK"
     - **fr**: "OK"
     - **it-IT**: "OK"
     - **nb**: "OK"
     - **pt-PT**: "OK"
     - **ru**: "ОК"
     - **es**: "ACEPTAR"
     - **sv**: "OK"
     - **fi**: "OK"
     - **zh-Hans**: "确定"
     - **zh-Hant**: "確定"
     - **id**: "OKE"
     - **fil**: "OK"
     - **ja**: "OK"
 */
  public static func ios_appwide_ok_uc() -> String {
     return localizedString(
         key:"ios_appwide_ok_uc",
         defaultValue:"OK",
         substitutions: [:])
  }

 /**
"SETUP"

     - **en**: "SETUP"
     - **da**: "OPSÆTNING"
     - **nl**: "INSTELLEN"
     - **de**: "SETUP"
     - **fr**: "RÉGLAGES"
     - **it-IT**: "CONFIGURAZIONE"
     - **nb**: "OPPSETT"
     - **pt-PT**: "CONFIGURAÇÃO"
     - **ru**: "УСТАНОВИТЬ"
     - **es**: "CONFIGURACIÓN"
     - **sv**: "KONFIGURERA"
     - **fi**: "ASETUKSET"
     - **zh-Hans**: "设置"
     - **zh-Hant**: "設定"
     - **id**: "SETELAN"
     - **fil**: "I-SETUP"
     - **ja**: "セットアップ"
 */
  public static func ios_appwide_setup_uc() -> String {
     return localizedString(
         key:"ios_appwide_setup_uc",
         defaultValue:"SETUP",
         substitutions: [:])
  }

 /**
"Decouple the speakers to undo the stereo pairing."

     - **en**: "Decouple the speakers to undo the stereo pairing."
     - **da**: "Skil højttalerne ad for at annullere stereo-parring."
     - **nl**: "Ontkoppel de luidsprekers om de stereokoppeling ongedaan te maken."
     - **de**: "Lautsprecher entkoppeln, um die Stereokoppelung zu beenden."
     - **fr**: "Découpler les enceintes pour annuler l’assemblage stéréo."
     - **it-IT**: "Separa i diffusori per annullare l’abbinamento stereo."
     - **nb**: "Koble fra høyttalerne for å angre stereoparingen."
     - **pt-PT**: "Desemparelhar os altifalantes para anular o emparelhamento estéreo."
     - **ru**: "Разъединить колонки, чтобы убрать стереозвук."
     - **es**: "Desacopla los altavoces para anular el emparejamiento estéreo."
     - **sv**: "Koppla isär högtalarna för att ta bort stereokopplingen."
     - **fi**: "Poista laitepari kaiuttimien väliltä stereoparin peruuttamiseksi."
     - **zh-Hans**: "解耦音箱，以取消立体声配对。"
     - **zh-Hant**: "取消喇叭配對以取消立體聲配對。"
     - **id**: "Pisahkan sambungan speaker untuk membatalkan pemasangan stereo."
     - **fil**: "Alisin sa pagkakapares ang mga speaker upang i-undo ang pagpapares ng speaker."
     - **ja**: "ステレオペアリングを解除するには、スピーカーをデカップリングします。"
 */
  public static func ios_couple_screen_decouple_subtitle() -> String {
     return localizedString(
         key:"ios_couple_screen_decouple_subtitle",
         defaultValue:"Decouple the speakers to undo the stereo pairing.",
         substitutions: [:])
  }

 /**
"DECOUPLE "

     - **en**: "DECOUPLE "
     - **da**: "SKIL AD "
     - **nl**: "ONTKOPPELEN "
     - **de**: "ENTKOPPELN "
     - **fr**: "DÉCOUPLER "
     - **it-IT**: "SEPARA "
     - **nb**: "KOBLE FRA "
     - **pt-PT**: "DESEMPARELHAR "
     - **ru**: "РАЗЪЕДИНИТЬ "
     - **es**: "DESACOPLAR "
     - **sv**: "KOPPLA ISÄR"
     - **fi**: "POISTA LAITEPARI "
     - **zh-Hans**: "解除配对"
     - **zh-Hant**: "取消配對 "
     - **id**: "PISAHKAN "
     - **fil**: "ALISIN SA PAGKAKAPARES "
     - **ja**: "デカップリング "
 */
  public static func ios_couple_screen_decouple_uc() -> String {
     return localizedString(
         key:"ios_couple_screen_decouple_uc",
         defaultValue:"DECOUPLE ",
         substitutions: [:])
  }

 /**
"Push and hold the Source button until the Bluetooth indicator starts blinking."

     - **en**: "Push and hold the Source button until the Bluetooth indicator starts blinking."
     - **da**: "Tryk og hold Kilde-knappen nede, indtil Bluetooth-indikatoren begynder at blinke."
     - **nl**: "Houd de Source-knop ingedrukt totdat de Bluetooth-indicator begint te knipperen."
     - **de**: "Drück und halte die Source-Taste, bis die Bluetooth-Leuchte blinkt."
     - **fr**: "Maintenez le bouton Source enfoncé jusqu’à ce que le voyant Bluetooth clignote."
     - **it-IT**: "Tieni premuto il pulsante Source fino a quando l’indicatore Bluetooth comincia a lampeggiare."
     - **nb**: "Trykk og hold Source-knappen inntil Bluetooth-indikatoren begynner å blinke."
     - **pt-PT**: "Pressione e segure o botão Source até o indicador Bluetooth começar a piscar."
     - **ru**: "Нажмите и удерживайте кнопку Source до тех пор, пока индикатор Bluetooth не начнет мигать."
     - **es**: "Pulsa y mantén pulsado el botón Source hasta que el indicador de Bluetooth empiece a parpadear."
     - **sv**: "Håll Source-knappen intryckt tills Bluetooth-indikatorn börjar blinka."
     - **fi**: "Paina ja pidä pohjassa Source-painiketta, kunnes Bluetooth-merkkivalo alkaa vilkkua."
     - **zh-Hans**: "长按 Source 按钮，直至 Bluetooth 指示灯开始闪烁。"
     - **zh-Hant**: "按住 Source 按鈕，直到 Bluetooth 指示燈開始閃爍為止。"
     - **id**: "Tekan dan tahan tombol Source sampai indikator mulai berkedip."
     - **fil**: "Pindutin at panatilihing nakapindot ang Source button hanggang sa magsimulang kumurap ang Bluetooth indicator."
     - **ja**: "インジケーターが点滅し始めるまでSourceボタンを押し続けます。"
 */
  public static func ios_ota_onboarding_point1() -> String {
     return localizedString(
         key:"ios_ota_onboarding_point1",
         defaultValue:"Push and hold the Source button until the Bluetooth indicator starts blinking.",
         substitutions: [:])
  }

 /**
"Go to Settings on your audio source."

     - **en**: "Go to Settings on your audio source."
     - **da**: "Gå til Indstillinger på din lydkilde."
     - **nl**: "Ga op je audiobron naar Instellingen."
     - **de**: "Geh in deiner Audio-Source zu den Einstellungen."
     - **fr**: "Sélectionnez Paramètres sur votre source audio."
     - **it-IT**: "Vai alle Impostazioni della tua sorgente audio."
     - **nb**: "Gå til Innstillinger på din audio-kilde."
     - **pt-PT**: "Abra as Definições da sua fonte de áudio."
     - **ru**: "Перейдите в настройки своей аудиосистемы."
     - **es**: "Vete a Ajustes en tu fuente de sonido."
     - **sv**: "Gå till inställningar på din ljudkälla."
     - **fi**: "Siirry oman äänilähteesi asetuksiin."
     - **zh-Hans**: "进入您音频源的设置。"
     - **zh-Hant**: "移至音訊來源上的「設定」。"
     - **id**: "Buka Pengaturan pada sumber audio Anda."
     - **fil**: "Pumunta sa Settings sa iyong audio source."
     - **ja**: "オーディオソースの[設定]に進みます。"
 */
  public static func ios_ota_onboarding_point2() -> String {
     return localizedString(
         key:"ios_ota_onboarding_point2",
         defaultValue:"Go to Settings on your audio source.",
         substitutions: [:])
  }

 /**
"Select the speaker from the list of available Bluetooth devices."

     - **en**: "Select the speaker from the list of available Bluetooth devices."
     - **da**: "Vælg højttaleren på listen over tilgængelige Bluetooth-enheder."
     - **nl**: "Selecteer de luidspreker in de lijst met beschikbare Bluetooth-apparaten."
     - **de**: "Wähle deinen Lautsprecher aus der Liste der verfügbaren Bluetooth-Geräte."
     - **fr**: "Sélectionnez l’enceinte dans la liste des appareils Bluetooth disponibles."
     - **it-IT**: "Seleziona il diffusore dall’elenco dei dispositivi Bluetooth disponibili."
     - **nb**: "Velg høyttaleren fra listen over tilgjengelige Bluetooth-enheter."
     - **pt-PT**: "Selecione o altifalante na lista de dispositivos Bluetooth disponíveis."
     - **ru**: "Выберите акустическую систему из списка доступных устройств Bluetooth."
     - **es**: "Selecciona el altavoz de la lista de dispositivos Bluetooth disponibles."
     - **sv**: "Markera högtalaren i listan över tillgängliga Bluetooth-enheter."
     - **fi**: "Valitse kaiutin käytettävissä olevien Bluetooth-laitteiden listalta."
     - **zh-Hans**: "从可用的 Bluetooth 设备列表中选择音箱。"
     - **zh-Hant**: "從可用的 Bluetooth 裝置清單中選擇喇叭。"
     - **id**: "Pilih speaker dari daftar perangkat Bluetooth yang tersedia."
     - **fil**: "Piliin ang speaker mula sa listahan ng mga magagamit na Bluetooth device."
     - **ja**: "利用可能なBluetoothデバイスのリストからスピーカーを選択します。"
 */
  public static func ios_ota_onboarding_point3() -> String {
     return localizedString(
         key:"ios_ota_onboarding_point3",
         defaultValue:"Select the speaker from the list of available Bluetooth devices.",
         substitutions: [:])
  }

 /**
"When the indicator turns steady, the pairing is complete and you can return to this app."

     - **en**: "When the indicator turns steady, the pairing is complete and you can return to this app."
     - **da**: "Når indikatoren bliver stabil, er parringen færdig, og du kan vende tilbage til denne app."
     - **nl**: "Als de indicator blijft branden, is de verbinding gemaakt en kan je naar deze app teruggaan."
     - **de**: "Wenn die Leuchte dauerhaft leuchtet, wurden die Geräte erfolgreich miteinander verbunden und du kannst zurück in die App gehen. "
     - **fr**: "Quand l’indicateur reste allumé, l’appairage est effectué et vous pouvez retourner à cette application."
     - **it-IT**: "Quando la luce dell’indicatore diventa fissa, l’abbinamento è avvenuto e puoi tornare a questa app."
     - **nb**: "Når indikatoren blir stabil, er paringen fullført og du kan vende tilbake til appen."
     - **pt-PT**: "Quando o indicador ficar constante, o emparelhamento está concluído e pode voltar a esta aplicação."
     - **ru**: "Когда индикатор перестанет мигать, это значит, что соединение завершено и можно вернуться к приложению."
     - **es**: "Cuando el indicador ya no parpadea, el emparejado está hecho y puedes volver a esta aplicación."
     - **sv**: "När indikatorn slutar blinka är parkopplingen klar och du kan återvända till den här appen."
     - **fi**: "Kun merkkivalo lopettaa vilkkumisen, on pariliitos valmis, ja voit palata tähän sovellukseen."
     - **zh-Hans**: "当指示灯常亮时，配对已完成，您可以返回该应用程序。"
     - **zh-Hant**: "指示燈穩定不變表示配對已完成，您可以返回此應用程式。"
     - **id**: "Setelah indikator stabil, pemasangan ini selesai dan Anda dapat kembali ke aplikasi ini."
     - **fil**: "Kapag steady na ang indicator, kumpleto na ang pagpapares at pwede ka nang bumalik sa app na ito."
     - **ja**: "インジケーターの点灯は、ペアリングの完了を意味するので、このアプリに戻ることができます。"
 */
  public static func ios_ota_onboarding_point4() -> String {
     return localizedString(
         key:"ios_ota_onboarding_point4",
         defaultValue:"When the indicator turns steady, the pairing is complete and you can return to this app.",
         substitutions: [:])
  }

 /**
"You need to accept the terms and conditions to continue."

     - **en**: "You need to accept the terms and conditions to continue."
     - **da**: "Du skal acceptere vores vilkår og betingelser for at fortsætte."
     - **nl**: "U moet de algemene voorwaarden accepteren om verder te kunnen gaan."
     - **de**: "Du musst die Nutzungsbedingungen akzeptieren, um fortzufahren."
     - **fr**: "Vous devez accepter les conditions générales pour pouvoir continuer."
     - **it-IT**: "Devi accettare i termini e le condizioni prima di proseguire."
     - **nb**: "Du må godta vilkår og betingelser for å fortsette."
     - **pt-PT**: "Tem de aceitar os termos e condições para continuar."
     - **ru**: "Чтобы продолжить, необходимо принять условия."
     - **es**: "Necesitas aceptar los términos y condiciones para continuar."
     - **sv**: "Du måste godkänna villkoren för att fortsätta."
     - **fi**: "Jatkaminen edellyttää ehtojen hyväksymistä."
     - **zh-Hans**: "您需要接受条款和条件才能继续。"
     - **zh-Hant**: "您必須接受條款與條件以繼續。"
     - **id**: "Anda harus menerima syarat dan ketentuan untuk melanjutkan."
     - **fil**: "Kailangan mong tanggapin ang mga takda at kondisyon para magpatuloy."
     - **ja**: "続行するには、利用規約に同意する必要があります。"
 */
  public static func ios_terms_and_conditions_screen_alert_message() -> String {
     return localizedString(
         key:"ios_terms_and_conditions_screen_alert_message",
         defaultValue:"You need to accept the terms and conditions to continue.",
         substitutions: [:])
  }

 /**
"TERMS AND CONDITIONS"

     - **en**: "TERMS AND CONDITIONS"
     - **da**: "VILKÅR OG BETINGELSER"
     - **nl**: "ALGEMENE VOORWAARDEN"
     - **de**: "VERKAUFS- UND LIEFERBEDINGUNGEN"
     - **fr**: "CONDITIONS GÉNÉRALES"
     - **it-IT**: "TERMINI E CONDIZIONI"
     - **nb**: "VILKÅR"
     - **pt-PT**: "TERMOS E CONDIÇÕES"
     - **ru**: "ПРАВИЛА И УСЛОВИЯ ИСПОЛЬЗОВАНИЯ"
     - **es**: "TÉRMINOS Y CONDICIONES"
     - **sv**: "REGLER OCH VILLKOR"
     - **fi**: "KÄYTTÖEHDOT"
     - **zh-Hans**: "条款与条件"
     - **zh-Hant**: "條款與條件"
     - **id**: "SYARAT DAN KETENTUAN"
     - **fil**: "MGA TAKDA AT KONDISYON"
     - **ja**: "利用規約"
 */
  public static func ios_terms_and_conditions_screen_title_uc() -> String {
     return localizedString(
         key:"ios_terms_and_conditions_screen_title_uc",
         defaultValue:"TERMS AND CONDITIONS",
         substitutions: [:])
  }

 /**
"Adjust the LED intensity on your speaker."

     - **en**: "Adjust the LED intensity on your speaker."
     - **da**: "Justér LED-styrken på din højtaler."
     - **nl**: "Pas de led-intensiteit van de luidspreker aan."
     - **de**: "Stelle die LED-Lichtintensität an deinem Lautsprecher ein."
     - **fr**: "Réglage de la luminosité des LED de votre enceinte."
     - **it-IT**: "Regola l’intensità dei LED sul tuo diffusore."
     - **nb**: "Juster LED-intensiteten på høyttaleren."
     - **pt-PT**: "Ajuste a intensidade do LED no seu altifalante."
     - **ru**: "Отрегулируйте интенсивность свечения светодиода на колонке."
     - **es**: "Ajusta la intensidad del LED de tu altavoz."
     - **sv**: "Justera LED-ljusstyrkan på din högtalare."
     - **fi**: "Säädä kaiuttimesi LED-voimakkuutta."
     - **zh-Hans**: "调整音箱上的LED指示灯亮度"
     - **zh-Hant**: "調整喇叭的 LED 亮度。"
     - **id**: "Sesuaikan intensitas LED pada speaker Anda."
     - **fil**: "I-adjust ang tindi ng LED sa iyong speaker."
     - **ja**: "スピーカーのLEDの明るさを調節します。"
 */
  public static func light_screen_subtitle() -> String {
     return localizedString(
         key:"light_screen_subtitle",
         defaultValue:"Adjust the LED intensity on your speaker.",
         substitutions: [:])
  }

 /**
"Use the M-Button to talk to Amazon Alexa"

     - **en**: "Use the M-Button to talk to Amazon Alexa"
     - **da**: "Brug M-knappen til at tale med Amazon Alexa"
     - **nl**: "Gebruik de M-knop om met Amazon Alexa te praten"
     - **de**: "Verwende die M-Taste, um mit Amazon Alexa zu sprechen"
     - **fr**: "Utilisez le bouton M pour parler à Amazon Alexa"
     - **it-IT**: "Usa il pulsante M per parlare con Amazon Alexa"
     - **nb**: "Bruk M-knappen til å snakke med Amazon Alexa"
     - **pt-PT**: "Use o botão M para falar com a Amazon Alexa"
     - **ru**: "Для обращения к Amazon Alexa используйте M-кнопку"
     - **es**: "Usa el botón M para hablar con Amazon Alexa"
     - **sv**: "Använd M-knappen för att prata med Amazon Alexa"
     - **fi**: "Käytä M-painiketta puhuaksesi Amazon Alexalle"
     - **zh-Hans**: "使用 M-按钮与 Amazon Alexa 通话"
     - **zh-Hant**: "使用 M 按鈕與 Amazon Alexa 通話"
     - **id**: "Tekan M-Button untuk berbicara dengan Amazon Alexa"
     - **fil**: "Gamitin ang M-Button upang kausapin ang Amazon Alexa"
     - **ja**: "Mボタンを使用して、Amazon Alexaと会話する"
 */
  public static func m_button_screen_option_alexa_subtitle() -> String {
     return localizedString(
         key:"m_button_screen_option_alexa_subtitle",
         defaultValue:"Use the M-Button to talk to Amazon Alexa",
         substitutions: [:])
  }

 /**
"Amazon Alexa"

     - **en**: "Amazon Alexa"
     - **da**: "Amazon Alexa"
     - **nl**: "Amazon Alexa"
     - **de**: "Amazon Alexa"
     - **fr**: "Amazon Alexa"
     - **it-IT**: "Amazon Alexa"
     - **nb**: "Amazon Alexa"
     - **pt-PT**: "Amazon Alexa"
     - **ru**: "Amazon Alexa"
     - **es**: "Amazon Alexa"
     - **sv**: "Amazon Alexa"
     - **fi**: "Amazon Alexa"
     - **zh-Hans**: "Amazon Alexa"
     - **zh-Hant**: "Amazon Alexa"
     - **id**: "Amazon Alexa"
     - **fil**: "Amazon Alexa"
     - **ja**: "Amazon Alexa"
 */
  public static func m_button_screen_option_alexa_title() -> String {
     return localizedString(
         key:"m_button_screen_option_alexa_title",
         defaultValue:"Amazon Alexa",
         substitutions: [:])
  }

 /**
"The M-Button toggles between EQ 1, 2 & 3"

     - **en**: "The M-Button toggles between EQ 1, 2 & 3"
     - **da**: "M-knappen skifter mellem EQ 1, 2 og 3"
     - **nl**: "De M-knop wisselt tussen EQ 1, 2 en 3"
     - **de**: "Die M-Taste wechselt zwischen EQ 1, 2 und 3"
     - **fr**: "Le bouton M permet de basculer entre les égalisations 1, 2 et 3"
     - **it-IT**: "Il pulsante M permette di passare tra EQ 1, 2 e 3"
     - **nb**: "M-knappen skifter mellom EQ 1, 2 og 3"
     - **pt-PT**: "O botão M alterna entre os equalizadores 1, 2 e 3"
     - **ru**: "M-кнопка переключается между 1, 2 и 3 режимами эквалайзера"
     - **es**: "El botón M cambia entre ecualizador 1, 2, y 3"
     - **sv**: "M-knappen växlar mellan EQ 1, 2 och 3"
     - **fi**: "M-painike vaihtaa taajuusmuunninten 1, 2 ja 3 välillä"
     - **zh-Hans**: "M-按钮在 EQ 1、2 和 3 之间切换"
     - **zh-Hant**: "M 按鈕可在 EQ 1、2 和 3 之間切換"
     - **id**: "M-Button beralih antara EQ 1, 2, & 3"
     - **fil**: "Ang M-Button ay nagta-toggle sa pagitan ng EQ 1, 2 at 3"
     - **ja**: "EQ 1、2、3をMボタンで切り替える"
 */
  public static func m_button_screen_option_eq_subtitle() -> String {
     return localizedString(
         key:"m_button_screen_option_eq_subtitle",
         defaultValue:"The M-Button toggles between EQ 1, 2 & 3",
         substitutions: [:])
  }

 /**
"Equaliser"

     - **en**: "Equaliser"
     - **da**: "Equalizer"
     - **nl**: "Toonregeling"
     - **de**: "Equalizer"
     - **fr**: "Égaliseur"
     - **it-IT**: "Equalizzatore"
     - **nb**: "Equalizer"
     - **pt-PT**: "Equalizador"
     - **ru**: "Эквалайзер"
     - **es**: "Ecualizador"
     - **sv**: "Equalizer (EQ)"
     - **fi**: "Taajuuskorjain"
     - **zh-Hans**: "均衡器"
     - **zh-Hant**: "均衡器"
     - **id**: "Equaliser"
     - **fil**: "Equalizer"
     - **ja**: "イコライザー"
 */
  public static func m_button_screen_option_eq_title() -> String {
     return localizedString(
         key:"m_button_screen_option_eq_title",
         defaultValue:"Equaliser",
         substitutions: [:])
  }

 /**
"Use the M-Button to talk to the Google Assistant"

     - **en**: "Use the M-Button to talk to the Google Assistant"
     - **da**: "Brug M-knappen til at tale med Google Assistent"
     - **nl**: "Gebruik de M-knop om met Google Assistent te praten"
     - **de**: "Verwende die M-Taste, um mit dem Google Assistant zu reden"
     - **fr**: "Utilisez le bouton M pour parler à l’Assistant Google"
     - **it-IT**: "Usa il Pulsante M per parlare con l’Assistente Google"
     - **nb**: "Bruk M-knappen til å snakke med Google Assistent"
     - **pt-PT**: "Use o botão M para falar com o Google Assistente"
     - **ru**: "Для обращения к Google Assistant используйте M-кнопку"
     - **es**: "Usa el botón M para hablar con el Asistente de Google"
     - **sv**: "Använd M-knappen för att prata med Google Assistent"
     - **fi**: "Käytä M-painiketta puhuaksesi Google Assistantille"
     - **zh-Hans**: "使用 M-按钮与Google Assistant 通话"
     - **zh-Hant**: "使用 M 按鈕與 Google Assistant 通話"
     - **id**: "Tekan M-Button untuk berbicara dengan Google Assistant"
     - **fil**: "Gamitin ang M-Button upang kausapin ang Google Assistant"
     - **ja**: "Mボタンを使用して、Googleアシスタントと会話する"
 */
  public static func m_button_screen_option_gva_subtitle() -> String {
     return localizedString(
         key:"m_button_screen_option_gva_subtitle",
         defaultValue:"Use the M-Button to talk to the Google Assistant",
         substitutions: [:])
  }

 /**
"Google Assistant"

     - **en**: "Google Assistant"
     - **da**: "Google Assistent"
     - **nl**: "Google Assistent"
     - **de**: "Google Assistant"
     - **fr**: "Assistant Google"
     - **it-IT**: "Assistente Google"
     - **nb**: "Google Assistent"
     - **pt-PT**: "Google Assistente"
     - **ru**: "Google Assistant"
     - **es**: "Asistente de Google"
     - **sv**: "Google Assistent"
     - **fi**: "Google Assistant"
     - **zh-Hans**: "Google Assistant"
     - **zh-Hant**: "Google Assistant"
     - **id**: "Google Assistant"
     - **fil**: "Google Assistant"
     - **ja**: "Googleアシスタント"
 */
  public static func m_button_screen_option_gva_title() -> String {
     return localizedString(
         key:"m_button_screen_option_gva_title",
         defaultValue:"Google Assistant",
         substitutions: [:])
  }

 /**
"Use the M-Button to talk to Native Voice Assistant"

     - **en**: "Use the M-Button to talk to Native Voice Assistant"
     - **da**: "M-knappen aktiverer den tilgængelige stemmetjeneste på den parrede enhed."
     - **nl**: "De M-knop activeert de spraakdienst die beschikbaar is op het gekoppelde apparaat."
     - **de**: "Die M-Taste aktiviert den Sprachassistenten, der auf dem gekoppelten Gerät verfügbar ist."
     - **fr**: "Le bouton M active le service vocal disponible sur l’appareil appairé."
     - **it-IT**: "Il pulsante M attiva il servizio vocale disponibile sul dispositivo accoppiato."
     - **nb**: "M-knappen aktivere taletjenesten som er tilgjengelig på den parede enheten."
     - **pt-PT**: "O botão M ativa o serviço de voz disponível no dispositivo emparelhado."
     - **ru**: "M-кнопка активирует голосовую службу, доступную на сопряженном устройстве."
     - **es**: "El botón M activa el servicio de voz que esté disponible en el dispositivo emparejado."
     - **sv**: "M-knappen aktiverar rösttjänsten som finns tillgänglig på den parkopplade enheten."
     - **fi**: "M-painike aktivoi äänipalvelun, joka on saatavana pariliitoksella yhdistetyssä laitteessa."
     - **zh-Hans**: "M-按钮可激活已配对设备上提供的语音服务。"
     - **zh-Hant**: "M 按鈕可啟動配對設備上可用的語音服務。"
     - **id**: "Tombol M akan mengaktifkan layanan suara yang tersedia pada perangkat berpasangan."
     - **fil**: "Ia-activate ng M-button ang voice service na available sa nakapares na device."
     - **ja**: "Mボタンはペアリングされたデバイスで利用可能な音声サービスを有効にします。"
 */
  public static func m_button_screen_option_nva_subtitle() -> String {
     return localizedString(
         key:"m_button_screen_option_nva_subtitle",
         defaultValue:"Use the M-Button to talk to Native Voice Assistant",
         substitutions: [:])
  }

 /**
"Native Voice Assistant"

     - **en**: "Native Voice Assistant"
     - **da**: "Indbygget stemmeassistent"
     - **nl**: "Native Voice Assistant"
     - **de**: "Integrierter Sprachassistent"
     - **fr**: "Assistant vocal natif"
     - **it-IT**: "Assistente vocale nativo"
     - **nb**: "Innebygd taleassistent"
     - **pt-PT**: "Assistente de voz nativo"
     - **ru**: "Собственный голосовой ассистент"
     - **es**: "Asistente de voz nativo"
     - **sv**: "Förinställd röstassistent"
     - **fi**: "Alkuperäinen ääniavustin"
     - **zh-Hans**: "本地语音助手"
     - **zh-Hant**: "本機語音助理"
     - **id**: "Asisten Suara Bahasa Asli"
     - **fil**: "Native na Voice Assistant"
     - **ja**: "ネイティブ音声アシスタント"
 */
  public static func m_button_screen_option_nva_title() -> String {
     return localizedString(
         key:"m_button_screen_option_nva_title",
         defaultValue:"Native Voice Assistant",
         substitutions: [:])
  }

 /**
"Use the M-Button to talk to Siri"

     - **en**: "Use the M-Button to talk to Siri"
     - **da**: "Brug M-knappen til at tale med Siri"
     - **nl**: "Gebruik de M-knop om met Siri te praten"
     - **de**: "Verwende die M-Taste, um mit Siri zu sprechen"
     - **fr**: "Utilisez le bouton M pour parler à Siri"
     - **it-IT**: "Usa il pulsante M per parlare con Siri"
     - **nb**: "Bruk M-knappen til å snakke med Siri"
     - **pt-PT**: "Use o botão M para falar com a Siri"
     - **ru**: "Для обращения к Siri нажмите M-кнопку"
     - **es**: "Usa el botón M para hablar con Siri"
     - **sv**: "Använd M-knappen för att prata med Siri"
     - **fi**: "Käytä M-painiketta puhuaksesi Sirille"
     - **zh-Hans**: "使用 M-按钮与 Siri 通话"
     - **zh-Hant**: "使用 M 按鈕與 Siri 通話"
     - **id**: "Tekan M-Button untuk berbicara dengan Siri"
     - **fil**: "Gamitin ang M-Button upang kausapin ang Siri"
     - **ja**: "Mボタンを使用して、Siriと会話する"
 */
  public static func m_button_screen_option_siri_subtitle() -> String {
     return localizedString(
         key:"m_button_screen_option_siri_subtitle",
         defaultValue:"Use the M-Button to talk to Siri",
         substitutions: [:])
  }

 /**
"Siri"

     - **en**: "Siri"
     - **da**: "Siri"
     - **nl**: "Siri"
     - **de**: "Siri"
     - **fr**: "Siri"
     - **it-IT**: "Siri"
     - **nb**: "Siri"
     - **pt-PT**: "Siri"
     - **ru**: "Siri"
     - **es**: "Siri"
     - **sv**: "Siri"
     - **fi**: "Siri"
     - **zh-Hans**: "Siri"
     - **zh-Hant**: "Siri"
     - **id**: "Siri"
     - **fil**: "Siri"
     - **ja**: "Siri"
 */
  public static func m_button_screen_option_siri_title() -> String {
     return localizedString(
         key:"m_button_screen_option_siri_title",
         defaultValue:"Siri",
         substitutions: [:])
  }

 /**
"Select a M-Button function."

     - **en**: "Select a M-Button function."
     - **da**: "Vælg en M-knap-funktion."
     - **nl**: "Selecteer een functie van de M-knop."
     - **de**: "Wähle eine Funktion für die M-Taste."
     - **fr**: "Sélectionnez une fonction pour le bouton M."
     - **it-IT**: "Seleziona una funzione del pulsante M."
     - **nb**: "Velg en M-knappfunksjon."
     - **pt-PT**: "Selecione uma função do botão M."
     - **ru**: "Выберите функцию M-кнопки."
     - **es**: "Selecciona una función del botón M."
     - **sv**: "Välj en M-knappfunktion."
     - **fi**: "Valitse M-painikkeen toiminto."
     - **zh-Hans**: "选择一个 M-按钮功能。"
     - **zh-Hant**: "選擇一個 M 按鈕功能。"
     - **id**: "Pilih fungsi M-Button."
     - **fil**: "Pumili ng function ng M-Button."
     - **ja**: "Mボタンの機能を選択します。"
 */
  public static func m_button_screen_subtitle() -> String {
     return localizedString(
         key:"m_button_screen_subtitle",
         defaultValue:"Select a M-Button function.",
         substitutions: [:])
  }

 /**
"Marshall Bluetooth"

     - **en**: "Marshall Bluetooth"
     - **da**: "Marshall Bluetooth"
     - **nl**: "Marshall Bluetooth"
     - **de**: "Marshall Bluetooth"
     - **fr**: "Marshall Bluetooth"
     - **it-IT**: "Marshall Bluetooth"
     - **nb**: "Marshall Bluetooth"
     - **pt-PT**: "Marshall Bluetooth"
     - **ru**: "Marshall Bluetooth"
     - **es**: "Marshall Bluetooth"
     - **sv**: "Marshall Bluetooth"
     - **fi**: "Marshall Bluetooth"
     - **zh-Hans**: "Marshall Bluetooth"
     - **zh-Hant**: "Marshall Bluetooth"
     - **id**: "Marshall Bluetooth"
     - **fil**: "Marshall Bluetooth"
     - **ja**: "Marshall Bluetooth"
 */
  public static func main_menu_about_name() -> String {
     return localizedString(
         key:"main_menu_about_name",
         defaultValue:"Marshall Bluetooth",
         substitutions: [:])
  }

 /**
"Marshall"

     - **en**: "Marshall"
     - **da**: "Marshall"
     - **nl**: "Marshall"
     - **de**: "Marshall"
     - **fr**: "Marshall"
     - **it-IT**: "Marshall"
     - **nb**: "Marshall"
     - **pt-PT**: "Marshall"
     - **ru**: "Marshall"
     - **es**: "Marshall"
     - **sv**: "Marshall"
     - **fi**: "Marshall"
     - **zh-Hans**: "Marshall"
     - **zh-Hant**: "Marshall"
     - **id**: "Marshall"
     - **fil**: "Marshall"
     - **ja**: "Marshall"
 */
  public static func main_menu_about_name_v1() -> String {
     return localizedString(
         key:"main_menu_about_name_v1",
         defaultValue:"Marshall",
         substitutions: [:])
  }

 /**
"Version"

     - **en**: "Version"
     - **da**: "Version"
     - **nl**: "Versie"
     - **de**: "Version"
     - **fr**: "Version"
     - **it-IT**: "Versione"
     - **nb**: "Versjon"
     - **pt-PT**: "Versão"
     - **ru**: "Версия"
     - **es**: "Versión"
     - **sv**: "Version"
     - **fi**: "Versio"
     - **zh-Hans**: "版本"
     - **zh-Hant**: "版本"
     - **id**: "Versi"
     - **fil**: "Bersyon"
     - **ja**: "バージョン"
 */
  public static func main_menu_about_version() -> String {
     return localizedString(
         key:"main_menu_about_version",
         defaultValue:"Version",
         substitutions: [:])
  }

 /**
"Analytics"

     - **en**: "Analytics"
     - **da**: "Analyse"
     - **nl**: "Analyses"
     - **de**: "Analyse"
     - **fr**: "Analyses"
     - **it-IT**: "Analisi"
     - **nb**: "Analyse"
     - **pt-PT**: "Análise"
     - **ru**: "Аналитика"
     - **es**: "Analítica"
     - **sv**: "Analys"
     - **fi**: "Analytiikka"
     - **zh-Hans**: "改进计划"
     - **zh-Hant**: "分析"
     - **id**: "Analytics"
     - **fil**: "Analytics"
     - **ja**: "分析"
 */
  public static func main_menu_item_analytics() -> String {
     return localizedString(
         key:"main_menu_item_analytics",
         defaultValue:"Analytics",
         substitutions: [:])
  }

 /**
"ANC Button"

     - **en**: "ANC Button"
     - **da**: "ANC-knap"
     - **nl**: "ANC-knop"
     - **de**: "ANC-Taste"
     - **fr**: "Bouton ANC"
     - **it-IT**: "Pulsante ANC"
     - **nb**: "ANC-knapp"
     - **pt-PT**: "Botão ANC"
     - **ru**: "Кнопка ANC"
     - **es**: "Botón de ANC"
     - **sv**: "Brusreduceringsknapp"
     - **fi**: "ANC-painike"
     - **zh-Hans**: "ANC 按钮"
     - **zh-Hant**: "ANC 按鈕"
     - **id**: "Tombol ANC"
     - **fil**: "ANC Button"
     - **ja**: "ANCボタン"
 */
  public static func main_menu_item_anc_button() -> String {
     return localizedString(
         key:"main_menu_item_anc_button",
         defaultValue:"ANC Button",
         substitutions: [:])
  }

 /**
"ANC BUTTON"

     - **en**: "ANC BUTTON"
     - **da**: "ANC-KNAP"
     - **nl**: "ANC-KNOP"
     - **de**: "ANC-TASTE"
     - **fr**: "BOUTON ANC"
     - **it-IT**: "PULSANTE ANC"
     - **nb**: "ANC-KNAPP"
     - **pt-PT**: "BOTÃO ANC"
     - **ru**: "КНОПКА ANC"
     - **es**: "BOTÓN DE ANC"
     - **sv**: "BRUSREDUCERINGSKNAPP"
     - **fi**: "Vastamelupainike (ANC)"
     - **zh-Hans**: "ANC 按钮"
     - **zh-Hant**: "ANC 按鈕"
     - **id**: "TOMBOL ANC"
     - **fil**: "ANC BUTTON"
     - **ja**: "ANCボタン"
 */
  public static func main_menu_item_anc_button_uc() -> String {
     return localizedString(
         key:"main_menu_item_anc_button_uc",
         defaultValue:"ANC BUTTON",
         substitutions: [:])
  }

 /**
"Bluetooth pairing"

     - **en**: "Bluetooth pairing"
     - **da**: "Bluetooth-parring"
     - **nl**: "Bluetooth koppelen"
     - **de**: "Bluetooth-Koppelung"
     - **fr**: "Association Bluetooth"
     - **it-IT**: "Abbinamento Bluetooth"
     - **nb**: "Bluetooth-paring"
     - **pt-PT**: "Emparelhamento por Bluetooth"
     - **ru**: "Сопряжение устройств по Bluetooth"
     - **es**: "Emparejamiento Bluetooth"
     - **sv**: "Parkoppla via Bluetooth"
     - **fi**: "Bluetooth-laiteparin muodostaminen"
     - **zh-Hans**: "蓝牙配对"
     - **zh-Hant**: "藍牙配對"
     - **id**: "Pemasangan Bluetooth"
     - **fil**: "Pagpapares ng Bluetooth"
     - **ja**: "Bluetoothのペアリング"
 */
  public static func main_menu_item_bluetooth_pairing() -> String {
     return localizedString(
         key:"main_menu_item_bluetooth_pairing",
         defaultValue:"Bluetooth pairing",
         substitutions: [:])
  }

 /**
"BLUETOOTH PAIRING"

     - **en**: "BLUETOOTH PAIRING"
     - **da**: "BLUETOOTH-PARRING"
     - **nl**: "BLUETOOTH KOPPELEN"
     - **de**: "BLUETOOTH-KOPPELUNG"
     - **fr**: "ASSOCIATION BLUETOOTH"
     - **it-IT**: "ABBINAMENTO BLUETOOTH"
     - **nb**: "BLUETOOTH-PARING"
     - **pt-PT**: "EMPARELHAMENTO POR BLUETOOTH"
     - **ru**: "СОПРЯЖЕНИЕ УСТРОЙСТВ ПО BLUETOOTH"
     - **es**: "EMPAREJAMIENTO BLUETOOTH"
     - **sv**: "PARKOPPLA VIA BLUETOOTH"
     - **fi**: "BLUETOOTH-LAITEPARIN MUODOSTAMINEN"
     - **zh-Hans**: "蓝牙配对"
     - **zh-Hant**: "藍牙配對"
     - **id**: "PEMASANGAN BLUETOOTH"
     - **fil**: "PAGPAPARES NG BLUETOOTH"
     - **ja**: "BLUETOOTHのペアリング"
 */
  public static func main_menu_item_bluetooth_pairing_uc() -> String {
     return localizedString(
         key:"main_menu_item_bluetooth_pairing_uc",
         defaultValue:"BLUETOOTH PAIRING",
         substitutions: [:])
  }

 /**
"Contact"

     - **en**: "Contact"
     - **da**: "Kontakt"
     - **nl**: "Contact"
     - **de**: "Kontakt"
     - **fr**: "Contact"
     - **it-IT**: "Contatto"
     - **nb**: "Kontakt"
     - **pt-PT**: "Contacto"
     - **ru**: "Контакты"
     - **es**: "Contacto"
     - **sv**: "Kontakt"
     - **fi**: "Ota yhteyttä"
     - **zh-Hans**: "联系我们"
     - **zh-Hant**: "聯絡"
     - **id**: "Kontak"
     - **fil**: "Makipag-ugnayan"
     - **ja**: "お問い合わせ"
 */
  public static func main_menu_item_contact() -> String {
     return localizedString(
         key:"main_menu_item_contact",
         defaultValue:"Contact",
         substitutions: [:])
  }

 /**
"CONTACT"

     - **en**: "CONTACT"
     - **da**: "KONTAKT"
     - **nl**: "CONTACT"
     - **de**: "KONTAKT"
     - **fr**: "CONTACT"
     - **it-IT**: "CONTATTO"
     - **nb**: "KONTAKT"
     - **pt-PT**: "CONTACTO"
     - **ru**: "КОНТАКТЫ"
     - **es**: "CONTACTO"
     - **sv**: "KONTAKT"
     - **fi**: "OTA YHTEYTTÄ"
     - **zh-Hans**: "联系我们"
     - **zh-Hant**: "聯絡"
     - **id**: "KONTAK"
     - **fil**: "MAKIPAG-UGNAYAN"
     - **ja**: "お問い合わせ"
 */
  public static func main_menu_item_contact_uc() -> String {
     return localizedString(
         key:"main_menu_item_contact_uc",
         defaultValue:"CONTACT",
         substitutions: [:])
  }

 /**
"Control Knob"

     - **en**: "Control Knob"
     - **da**: "Kontrolknap"
     - **nl**: "Bedieningsknop"
     - **de**: "Bedienknopf"
     - **fr**: "Bouton de contrôle"
     - **it-IT**: "Manopola di controllo"
     - **nb**: "Kontrollknapp"
     - **pt-PT**: "Botão de controlo"
     - **ru**: "Кнопка управления"
     - **es**: "Botón de control"
     - **sv**: "Kontrollknapp"
     - **fi**: "Säätönuppi"
     - **zh-Hans**: "控制旋钮"
     - **zh-Hant**: "控制旋鈕"
     - **id**: "Knop Kontrol"
     - **fil**: "Control Knob"
     - **ja**: "コントロールノブ"
 */
  public static func main_menu_item_control_knob() -> String {
     return localizedString(
         key:"main_menu_item_control_knob",
         defaultValue:"Control Knob",
         substitutions: [:])
  }

 /**
"CONTROL KNOB"

     - **en**: "CONTROL KNOB"
     - **da**: "KONTROLKNAP"
     - **nl**: "BEDIENINGSKNOP"
     - **de**: "BEDIENKNOPF"
     - **fr**: "BOUTON DE CONTRÔLE"
     - **it-IT**: "MANOPOLA DI CONTROLLO"
     - **nb**: "KONTROLLKNAPP"
     - **pt-PT**: "BOTÃO DE CONTROLO"
     - **ru**: "КНОПКА УПРАВЛЕНИЯ"
     - **es**: "BOTÓN DE CONTROL"
     - **sv**: "KONTROLLKNAPP"
     - **fi**: "SÄÄTÖNUPPI"
     - **zh-Hans**: "控制旋钮"
     - **zh-Hant**: "控制旋鈕"
     - **id**: "KNOP KONTROL"
     - **fil**: "CONTROL KNOB"
     - **ja**: "コントロールノブ"
 */
  public static func main_menu_item_control_knob_uc() -> String {
     return localizedString(
         key:"main_menu_item_control_knob_uc",
         defaultValue:"CONTROL KNOB",
         substitutions: [:])
  }

 /**
"Couple Speakers"

     - **en**: "Couple Speakers"
     - **da**: "Parring af højttalere"
     - **nl**: "Koppelen van luidsprekers"
     - **de**: "Lautsprecher koppeln"
     - **fr**: "Coupler les enceintes"
     - **it-IT**: "Associa più diffusori"
     - **nb**: "Sammenkoblede høyttalere"
     - **pt-PT**: "Acoplar altifalantes"
     - **ru**: "Объединение колонок"
     - **es**: "Emparejar altavoces"
     - **sv**: "Koppla ihop högtalare"
     - **fi**: "Parikaiuttimet"
     - **zh-Hans**: "音箱组合配对"
     - **zh-Hant**: "雙喇叭"
     - **id**: "Speaker Pasangan"
     - **fil**: "Ipares ang Mga Speaker"
     - **ja**: "スピーカーのカップリング"
 */
  public static func main_menu_item_couple_speakers() -> String {
     return localizedString(
         key:"main_menu_item_couple_speakers",
         defaultValue:"Couple Speakers",
         substitutions: [:])
  }

 /**
"E-mail subscription"

     - **en**: "E-mail subscription"
     - **da**: "E-mail-abonnement"
     - **nl**: "E-mail abonnement"
     - **de**: "E-Mail-Abonnement"
     - **fr**: "Abonnement e-mail"
     - **it-IT**: "Iscrizione e-mail"
     - **nb**: "E-postabonnement"
     - **pt-PT**: "Subscrição de e-mail"
     - **ru**: "Электронная подписка"
     - **es**: "Suscripción de correo electrónico"
     - **sv**: "E-postprenumeration"
     - **fi**: "Sähköpostitilaus"
     - **zh-Hans**: "电子邮件订阅"
     - **zh-Hant**: "電子郵件訂閱"
     - **id**: "Berlangganan Email"
     - **fil**: "Subskripsiyon sa E-mail "
     - **ja**: "Eメール配信登録"
 */
  public static func main_menu_item_email_subscription() -> String {
     return localizedString(
         key:"main_menu_item_email_subscription",
         defaultValue:"E-mail subscription",
         substitutions: [:])
  }

 /**
"EMAIL SUBSCRIPTION"

     - **en**: "EMAIL SUBSCRIPTION"
     - **da**: "E-MAIL-ABONNEMENT"
     - **nl**: "E-MAIL ABONNEMENT"
     - **de**: "E-MAIL-ABONNEMENT"
     - **fr**: "ABONNEMENT E-MAIL"
     - **it-IT**: "ISCRIZIONE E-MAIL"
     - **nb**: "E-POSTABONNEMENT"
     - **pt-PT**: "SUBSCRIÇÃO DE E-MAIL"
     - **ru**: "ЭЛЕКТРОННАЯ ПОДПИСКА"
     - **es**: "SUSCRIPCIÓN DE CORREO ELECTRÓNICO"
     - **sv**: "E-POSTPRENUMERATION"
     - **fi**: "SÄHKÖPOSTITILAUS"
     - **zh-Hans**: "电子邮件订阅"
     - **zh-Hant**: "電子郵件訂閱"
     - **id**: "BERLANGGANAN EMAIL"
     - **fil**: "SUBSKRIPSIYON SA EMAIL"
     - **ja**: "Eメール配信登録"
 */
  public static func main_menu_item_email_subscription__v1_uc() -> String {
     return localizedString(
         key:"main_menu_item_email_subscription__v1_uc",
         defaultValue:"EMAIL SUBSCRIPTION",
         substitutions: [:])
  }

 /**
"E-MAIL SUBSCRIPTION"

     - **en**: "E-MAIL SUBSCRIPTION"
     - **da**: "E-MAIL-ABONNEMENT"
     - **nl**: "E-MAIL ABONNEMENT"
     - **de**: "E-MAIL-ABONNEMENT"
     - **fr**: "ABONNEMENT E-MAIL"
     - **it-IT**: "ISCRIZIONE E-MAIL"
     - **nb**: "E-POSTABONNEMENT"
     - **pt-PT**: "SUBSCRIÇÃO DE E-MAIL"
     - **ru**: "ЭЛЕКТРОННАЯ ПОДПИСКА"
     - **es**: "SUSCRIPCIÓN DE CORREO ELECTRÓNICO"
     - **sv**: "E-POSTPRENUMERATION"
     - **fi**: "SÄHKÖPOSTITILAUS"
     - **zh-Hans**: "电子邮件订阅"
     - **zh-Hant**: "電子郵件訂閱"
     - **id**: "BERLANGGANAN EMAIL"
     - **fil**: "SUBSKRIPSIYON SA E-MAIL "
     - **ja**: "Eメール配信登録"
 */
  public static func main_menu_item_email_subscription_uc() -> String {
     return localizedString(
         key:"main_menu_item_email_subscription_uc",
         defaultValue:"E-MAIL SUBSCRIPTION",
         substitutions: [:])
  }

 /**
"Email subscription"

     - **en**: "Email subscription"
     - **da**: "E-mail-abonnement"
     - **nl**: "E-mail abonnement"
     - **de**: "E-Mail-Abonnement"
     - **fr**: "Abonnement e-mail"
     - **it-IT**: "Iscrizione e-mail"
     - **nb**: "E-postabonnement"
     - **pt-PT**: "Subscrição de e-mail"
     - **ru**: "Электронная подписка"
     - **es**: "Suscripción de correo electrónico"
     - **sv**: "E-postprenumeration"
     - **fi**: "Sähköpostitilaus"
     - **zh-Hans**: "电子邮件订阅"
     - **zh-Hant**: "電子郵件訂閱"
     - **id**: "Berlangganan Email"
     - **fil**: "Subskripsiyon sa email"
     - **ja**: "Eメール配信登録"
 */
  public static func main_menu_item_email_subscription_v1() -> String {
     return localizedString(
         key:"main_menu_item_email_subscription_v1",
         defaultValue:"Email subscription",
         substitutions: [:])
  }

 /**
"Free and Open Source Software"

     - **en**: "Free and Open Source Software"
     - **da**: "Gratis og open source-software"
     - **nl**: "Gratis en opensourcesoftware"
     - **de**: "Kostenlose und Open-Source-Software"
     - **fr**: "Logiciel libre à code source ouvert"
     - **it-IT**: "Software libero e open source"
     - **nb**: "Gratis programvare med åpen kildekode"
     - **pt-PT**: "Software gratuito e de código aberto"
     - **ru**: "Программное обеспечение с открытым исходным кодом"
     - **es**: "Software gratuito y de código abierto"
     - **sv**: "Fri programvara och programvara med öppen källkod"
     - **fi**: "Vapaa ja avoimen lähdekoodin ohjelmisto"
     - **zh-Hans**: "免费和开源软件"
     - **zh-Hant**: "免費和開放源碼軟體"
     - **id**: "Perangkat Lunak Gratis dan Sumber Terbuka"
     - **fil**: "Software na Libre at Open Source "
     - **ja**: "フリーソフトウェアとオープンソースソフトウェア"
 */
  public static func main_menu_item_foss() -> String {
     return localizedString(
         key:"main_menu_item_foss",
         defaultValue:"Free and Open Source Software",
         substitutions: [:])
  }

 /**
"FOSS"

     - **en**: "FOSS"
     - **da**: "FOSS"
     - **nl**: "FOSS"
     - **de**: "FOSS"
     - **fr**: "FOSS"
     - **it-IT**: "FOSS"
     - **nb**: "FOSS"
     - **pt-PT**: "FOSS"
     - **ru**: "FOSS"
     - **es**: "FOSS"
     - **sv**: "FOSS"
     - **fi**: "Vapaa ja avoimen lähdekoodin ohjelmisto"
     - **zh-Hans**: "免费和开源软件"
     - **zh-Hant**: "FOSS"
     - **id**: "FOSS"
     - **fil**: "FOSS"
     - **ja**: "FOSS"
 */
  public static func main_menu_item_foss_uc() -> String {
     return localizedString(
         key:"main_menu_item_foss_uc",
         defaultValue:"FOSS",
         substitutions: [:])
  }

 /**
"Help "

     - **en**: "Help "
     - **da**: "Hjælp "
     - **nl**: "Hulp "
     - **de**: "Hilfe "
     - **fr**: "Aide "
     - **it-IT**: "Aiuto "
     - **nb**: "Hjelp "
     - **pt-PT**: "Ajuda "
     - **ru**: "Помощь "
     - **es**: "Ayuda "
     - **sv**: "Hjälp "
     - **fi**: "Ohje "
     - **zh-Hans**: "帮助 "
     - **zh-Hant**: "說明 "
     - **id**: "Bantuan "
     - **fil**: "Tulong "
     - **ja**: "ヘルプ "
 */
  public static func main_menu_item_help() -> String {
     return localizedString(
         key:"main_menu_item_help",
         defaultValue:"Help ",
         substitutions: [:])
  }

 /**
"HELP"

     - **en**: "HELP"
     - **da**: "HJÆLP"
     - **nl**: "HULP"
     - **de**: "HILFE"
     - **fr**: "AIDE"
     - **it-IT**: "AIUTO"
     - **nb**: "HJELP"
     - **pt-PT**: "AJUDA"
     - **ru**: "ПОМОЩЬ"
     - **es**: "AYUDA"
     - **sv**: "HJÄLP"
     - **fi**: "OHJE"
     - **zh-Hans**: "帮助"
     - **zh-Hant**: "說明"
     - **id**: "BANTUAN"
     - **fil**: "TULONG"
     - **ja**: "ヘルプ"
 */
  public static func main_menu_item_help_uc() -> String {
     return localizedString(
         key:"main_menu_item_help_uc",
         defaultValue:"HELP",
         substitutions: [:])
  }

 /**
"M-Button"

     - **en**: "M-Button"
     - **da**: "M-Button"
     - **nl**: "M-knop"
     - **de**: "M-Taste"
     - **fr**: "Bouton M"
     - **it-IT**: "Pulsante M"
     - **nb**: "M-knapp"
     - **pt-PT**: "Botão M"
     - **ru**: "M-кнопка"
     - **es**: "Botón M"
     - **sv**: "M-knapp"
     - **fi**: "M-painike"
     - **zh-Hans**: "M-按钮"
     - **zh-Hant**: "M 按鈕"
     - **id**: "M-Button"
     - **fil**: "M-Button"
     - **ja**: "Mボタン"
 */
  public static func main_menu_item_m_button() -> String {
     return localizedString(
         key:"main_menu_item_m_button",
         defaultValue:"M-Button",
         substitutions: [:])
  }

 /**
"M-BUTTON"

     - **en**: "M-BUTTON"
     - **da**: "M-BUTTON"
     - **nl**: "M-KNOP"
     - **de**: "M-TASTE"
     - **fr**: "BOUTON M"
     - **it-IT**: "PULSANTE M"
     - **nb**: "M-KNAPP"
     - **pt-PT**: "BOTÃO M"
     - **ru**: "M-КНОПКА"
     - **es**: "BOTÓN M"
     - **sv**: "M-KNAPP"
     - **fi**: "M-PAINIKE"
     - **zh-Hans**: "M-按钮"
     - **zh-Hant**: "M 按鈕"
     - **id**: "M-BUTTON"
     - **fil**: "M-BUTTON"
     - **ja**: "Mボタン"
 */
  public static func main_menu_item_m_button_uc() -> String {
     return localizedString(
         key:"main_menu_item_m_button_uc",
         defaultValue:"M-BUTTON",
         substitutions: [:])
  }

 /**
"User Manual"

     - **en**: "User Manual"
     - **da**: "Brugervejledning"
     - **nl**: "Gebruiksaanwijzing"
     - **de**: "Bedienungsanleitung"
     - **fr**: "Manuel de l’utilisateur"
     - **it-IT**: "Manuale utente"
     - **nb**: "Brukerhåndbok"
     - **pt-PT**: "Manual do utilizador"
     - **ru**: "Руководство пользователя"
     - **es**: "Manual de usuario"
     - **sv**: "Användarmanual"
     - **fi**: "Käyttöohje"
     - **zh-Hans**: "用户手册"
     - **zh-Hant**: "使用者手冊"
     - **id**: "Panduan pengguna"
     - **fil**: "Manwal ng gumagamit"
     - **ja**: "ユーザーマニュアル"
 */
  public static func main_menu_item_online_manual() -> String {
     return localizedString(
         key:"main_menu_item_online_manual",
         defaultValue:"User Manual",
         substitutions: [:])
  }

 /**
"USER MANUAL"

     - **en**: "USER MANUAL"
     - **da**: "BRUGERVEJLEDNING"
     - **nl**: "GEBRUIKSAANWIJZING"
     - **de**: "BEDIENUNGSANLEITUNG"
     - **fr**: "MANUEL DE L’UTILISATEUR"
     - **it-IT**: "MANUALE UTENTE"
     - **nb**: "BRUKERHÅNDBOK"
     - **pt-PT**: "MANUAL DO UTILIZADOR"
     - **ru**: "РУКОВОДСТВО ПОЛЬЗОВАТЕЛЯ"
     - **es**: "MANUAL DE USUARIO"
     - **sv**: "ANVÄNDARMANUAL"
     - **fi**: "KÄYTTÖOHJE"
     - **zh-Hans**: "用户手册"
     - **zh-Hant**: "使用者手冊"
     - **id**: "PANDUAN PENGGUNA"
     - **fil**: "MANWAL NG GUMAGAMIT"
     - **ja**: "ユーザーマニュアル"
 */
  public static func main_menu_item_online_manual_uc() -> String {
     return localizedString(
         key:"main_menu_item_online_manual_uc",
         defaultValue:"USER MANUAL",
         substitutions: [:])
  }

 /**
"Play/pause button"

     - **en**: "Play/pause button"
     - **da**: "Afspilnings-/pauseknap"
     - **nl**: "Afspeel/pauze-knop"
     - **de**: "Wiedergabe-/Pause-Taste"
     - **fr**: "Bouton Lecture/Pause"
     - **it-IT**: "Pulsante Play/Pausa"
     - **nb**: "Avspilling/pause-knapp"
     - **pt-PT**: "Botão Reproduzir/Pausa"
     - **ru**: "Кнопка «Воспроизведение/Пауза»"
     - **es**: "Botón Reproducir/Detener"
     - **sv**: "Spela/paus-knapp"
     - **fi**: "Toista/Tauko-painike"
     - **zh-Hans**: "播放/暂停按钮"
     - **zh-Hant**: "播放/暫停按鈕"
     - **id**: "Tombol putar/jeda"
     - **fil**: "Button para sa play/pause"
     - **ja**: "再生／一時停止ボタン"
 */
  public static func main_menu_item_play_pause_button() -> String {
     return localizedString(
         key:"main_menu_item_play_pause_button",
         defaultValue:"Play/pause button",
         substitutions: [:])
  }

 /**
"PLAY/PAUSE BUTTON"

     - **en**: "PLAY/PAUSE BUTTON"
     - **da**: "AFSPILNINGS-/PAUSEKNAP"
     - **nl**: "AFSPEEL/PAUZE-KNOP"
     - **de**: "WIEDERGABE-/PAUSE-TASTE"
     - **fr**: "BOUTON LECTURE/PAUSE"
     - **it-IT**: "PULSANTE PLAY/PAUSA"
     - **nb**: "AVSPILLING/PAUSE-KNAPP"
     - **pt-PT**: "BOTÃO REPRODUZIR/PAUSA"
     - **ru**: "КНОПКА «ВОСПРОИЗВЕДЕНИЕ/ПАУЗА»"
     - **es**: "BOTÓN REPRODUCIR/DETENER"
     - **sv**: "SPELA/PAUS-KNAPP"
     - **fi**: "TOISTA/TAUKO-PAINIKE"
     - **zh-Hans**: "播放/暂停按钮"
     - **zh-Hant**: "播放/暫停按鈕"
     - **id**: "TOMBOL PUTAR/JEDA"
     - **fil**: "BUTTON NG PLAY/PAUSE"
     - **ja**: "再生／一時停止ボタン"
 */
  public static func main_menu_item_play_pause_button_uc() -> String {
     return localizedString(
         key:"main_menu_item_play_pause_button_uc",
         defaultValue:"PLAY/PAUSE BUTTON",
         substitutions: [:])
  }

 /**
"Quick Guide"

     - **en**: "Quick Guide"
     - **da**: "Hurtigguide"
     - **nl**: "Snelgids"
     - **de**: "Kurzanleitung"
     - **fr**: "Guide rapide"
     - **it-IT**: "Guida rapida"
     - **nb**: "Hurtigveiledning"
     - **pt-PT**: "Guia rápido"
     - **ru**: "Краткое руководство"
     - **es**: "Guía rápida"
     - **sv**: "Snabbguide"
     - **fi**: "Pikaopas"
     - **zh-Hans**: "快速指南"
     - **zh-Hant**: "快速指南"
     - **id**: "Panduan Cepat"
     - **fil**: "Dagliang Gabay"
     - **ja**: "クイックガイド"
 */
  public static func main_menu_item_quick_guide() -> String {
     return localizedString(
         key:"main_menu_item_quick_guide",
         defaultValue:"Quick Guide",
         substitutions: [:])
  }

 /**
"Acton II"

     - **en**: "Acton II"
     - **da**: "Acton II"
     - **nl**: "Acton II"
     - **de**: "Acton II"
     - **fr**: "Acton II"
     - **it-IT**: "Acton II"
     - **nb**: "Acton II"
     - **pt-PT**: "Acton II"
     - **ru**: "Acton II"
     - **es**: "Acton II"
     - **sv**: "Acton II"
     - **fi**: "Acton II"
     - **zh-Hans**: "Acton II"
     - **zh-Hant**: "Acton II"
     - **id**: "Acton II"
     - **fil**: "Acton II"
     - **ja**: "Acton II"
 */
  public static func main_menu_item_quick_guide_item_1() -> String {
     return localizedString(
         key:"main_menu_item_quick_guide_item_1",
         defaultValue:"Acton II",
         substitutions: [:])
  }

 /**
"Stanmore II"

     - **en**: "Stanmore II"
     - **da**: "Stanmore II"
     - **nl**: "Stanmore II"
     - **de**: "Stanmore II"
     - **fr**: "Stanmore II"
     - **it-IT**: "Stanmore II"
     - **nb**: "Stanmore II"
     - **pt-PT**: "Stanmore II"
     - **ru**: "Stanmore II"
     - **es**: "Stanmore II"
     - **sv**: "Stanmore II"
     - **fi**: "Stanmore II"
     - **zh-Hans**: "Stanmore II"
     - **zh-Hant**: "Stanmore II"
     - **id**: "Stanmore II"
     - **fil**: "Stanmore II"
     - **ja**: "Stanmore II"
 */
  public static func main_menu_item_quick_guide_item_2() -> String {
     return localizedString(
         key:"main_menu_item_quick_guide_item_2",
         defaultValue:"Stanmore II",
         substitutions: [:])
  }

 /**
"Woburn II"

     - **en**: "Woburn II"
     - **da**: "Woburn II"
     - **nl**: "Woburn II"
     - **de**: "Woburn II"
     - **fr**: "Woburn II"
     - **it-IT**: "Woburn II"
     - **nb**: "Woburn II"
     - **pt-PT**: "Woburn II"
     - **ru**: "Woburn II"
     - **es**: "Woburn II"
     - **sv**: "Woburn II"
     - **fi**: "Woburn II"
     - **zh-Hans**: "Woburn II"
     - **zh-Hant**: "Woburn II"
     - **id**: "Woburn II"
     - **fil**: "Woburn II"
     - **ja**: "Woburn II"
 */
  public static func main_menu_item_quick_guide_item_3() -> String {
     return localizedString(
         key:"main_menu_item_quick_guide_item_3",
         defaultValue:"Woburn II",
         substitutions: [:])
  }

 /**
"Monitor II A.N.C."

     - **en**: "Monitor II A.N.C."
     - **da**: "Monitor II A.N.C."
     - **nl**: "Monitor II A.N.C."
     - **de**: "Monitor II A.N.C."
     - **fr**: "Monitor II A.N.C."
     - **it-IT**: "Monitor II A.N.C."
     - **nb**: "Monitor II A.N.C."
     - **pt-PT**: "Monitor II A.N.C."
     - **ru**: "Monitor II A.N.C."
     - **es**: "Monitor II A.N.C."
     - **sv**: "Monitor II A.N.C."
     - **fi**: "Monitor II A.N.C."
     - **zh-Hans**: "Monitor II A.N.C."
     - **zh-Hant**: "Monitor II A.N.C."
     - **id**: "Monitor II A.N.C."
     - **fil**: "Monitor II A.N.C."
     - **ja**: "Monitor II A.N.C."
 */
  public static func main_menu_item_quick_guide_item_4() -> String {
     return localizedString(
         key:"main_menu_item_quick_guide_item_4",
         defaultValue:"Monitor II A.N.C.",
         substitutions: [:])
  }

 /**
"QUICK GUIDE"

     - **en**: "QUICK GUIDE"
     - **da**: "HURTIGGUIDE"
     - **nl**: "SNELGIDS"
     - **de**: "KURZANLEITUNG"
     - **fr**: "GUIDE RAPIDE"
     - **it-IT**: "GUIDA RAPIDA"
     - **nb**: "HURTIGVEILEDNING"
     - **pt-PT**: "GUIA RÁPIDO"
     - **ru**: "КРАТКОЕ РУКОВОДСТВО"
     - **es**: "GUÍA RÁPIDA"
     - **sv**: "SNABBGUIDE"
     - **fi**: "PIKAOPAS"
     - **zh-Hans**: "快速指南"
     - **zh-Hant**: "快速指南"
     - **id**: "PANDUAN CEPAT"
     - **fil**: "MABILIS NA GABAY"
     - **ja**: "クイックガイド"
 */
  public static func main_menu_item_quick_guide_uc() -> String {
     return localizedString(
         key:"main_menu_item_quick_guide_uc",
         defaultValue:"QUICK GUIDE",
         substitutions: [:])
  }

 /**
"Settings"

     - **en**: "Settings"
     - **da**: "Indstillinger"
     - **nl**: "Instellingen"
     - **de**: "Einstellungen"
     - **fr**: "Paramètres"
     - **it-IT**: "Impostazioni"
     - **nb**: "Innstillinger"
     - **pt-PT**: "Definições"
     - **ru**: "Параметры"
     - **es**: "Ajustes"
     - **sv**: "Inställningar"
     - **fi**: "Asetukset"
     - **zh-Hans**: "设置"
     - **zh-Hant**: "設定"
     - **id**: "Pengaturan"
     - **fil**: "Mga Setting"
     - **ja**: "設定"
 */
  public static func main_menu_item_settings() -> String {
     return localizedString(
         key:"main_menu_item_settings",
         defaultValue:"Settings",
         substitutions: [:])
  }

 /**
"APP SETTINGS"

     - **en**: "APP SETTINGS"
     - **da**: "APP-INDSTILLINGER"
     - **nl**: "INSTELLINGEN VAN APP"
     - **de**: "APP-EINSTELLUNGEN"
     - **fr**: "PARAMÈTRES DE L’APPLICATION"
     - **it-IT**: "IMPOSTAZIONI APP"
     - **nb**: "APPINNSTILLINGER"
     - **pt-PT**: "DEFINIÇÕES DA APP"
     - **ru**: "ПАРАМЕТРЫ ПРИЛОЖЕНИЯ"
     - **es**: "Ajustes de app"
     - **sv**: "APP-INSTÄLLNINGAR"
     - **fi**: "SOVELLUSASETUKSET"
     - **zh-Hans**: "应用设置"
     - **zh-Hant**: "應用程式設定"
     - **id**: "PENGATURAN APP"
     - **fil**: "MGA SETTING NG APP"
     - **ja**: "アプリの設定"
 */
  public static func main_menu_item_settings_uc() -> String {
     return localizedString(
         key:"main_menu_item_settings_uc",
         defaultValue:"APP SETTINGS",
         substitutions: [:])
  }

 /**
"Shop"

     - **en**: "Shop"
     - **da**: "Køb"
     - **nl**: "Winkel"
     - **de**: "Kaufen"
     - **fr**: "Acheter"
     - **it-IT**: "Negozio"
     - **nb**: "Butikk"
     - **pt-PT**: "Comprar"
     - **ru**: "Магазин"
     - **es**: "Comprar"
     - **sv**: "Butik"
     - **fi**: "Kauppa"
     - **zh-Hans**: "商店"
     - **zh-Hant**: "購買"
     - **id**: "Toko"
     - **fil**: "Tindahan"
     - **ja**: "ショップ"
 */
  public static func main_menu_item_shop() -> String {
     return localizedString(
         key:"main_menu_item_shop",
         defaultValue:"Shop",
         substitutions: [:])
  }

 /**
"Terms and Conditions"

     - **en**: "Terms and Conditions"
     - **da**: "Vilkår og betingelser"
     - **nl**: "Algemene voorwaarden"
     - **de**: "Verkaufs- und Lieferbedingungen"
     - **fr**: "Conditions générales"
     - **it-IT**: "Termini e Condizioni"
     - **nb**: "Vilkår"
     - **pt-PT**: "Termos e Condições"
     - **ru**: "Правила и условия использования"
     - **es**: "Términos y condiciones"
     - **sv**: "Regler och villkor"
     - **fi**: "Käyttöehdot"
     - **zh-Hans**: "使用条款"
     - **zh-Hant**: "條款與條件"
     - **id**: "Syarat dan Ketentuan"
     - **fil**: "Mga Takda at Kondisyon"
     - **ja**: "利用規約"
 */
  public static func main_menu_item_terms_and_conditions() -> String {
     return localizedString(
         key:"main_menu_item_terms_and_conditions",
         defaultValue:"Terms and Conditions",
         substitutions: [:])
  }

 /**
"TERMS AND CONDITIONS"

     - **en**: "TERMS AND CONDITIONS"
     - **da**: "VILKÅR OG BETINGELSER"
     - **nl**: "ALGEMENE VOORWAARDEN"
     - **de**: "VERKAUFS- UND LIEFERBEDINGUNGEN"
     - **fr**: "CONDITIONS GÉNÉRALES"
     - **it-IT**: "TERMINI E CONDIZIONI"
     - **nb**: "VILKÅR"
     - **pt-PT**: "TERMOS E CONDIÇÕES"
     - **ru**: "ПРАВИЛА И УСЛОВИЯ ИСПОЛЬЗОВАНИЯ"
     - **es**: "TÉRMINOS Y CONDICIONES"
     - **sv**: "REGLER OCH VILLKOR"
     - **fi**: "KÄYTTÖEHDOT"
     - **zh-Hans**: "使用条款"
     - **zh-Hant**: "條款與條件"
     - **id**: "SYARAT DAN KETENTUAN"
     - **fil**: "MGA TAKDA AT KONDISYON"
     - **ja**: "利用規約"
 */
  public static func main_menu_item_terms_and_conditions_uc() -> String {
     return localizedString(
         key:"main_menu_item_terms_and_conditions_uc",
         defaultValue:"TERMS AND CONDITIONS",
         substitutions: [:])
  }

 /**
"USER MANUAL"

     - **en**: "USER MANUAL"
     - **da**: "BRUGERVEJLEDNING"
     - **nl**: "GEBRUIKSAANWIJZING"
     - **de**: "BEDIENUNGSANLEITUNG"
     - **fr**: "MANUEL DE L’UTILISATEUR"
     - **it-IT**: "MANUALE UTENTE"
     - **nb**: "BRUKERHÅNDBOK"
     - **pt-PT**: "MANUAL DO UTILIZADOR"
     - **ru**: "РУКОВОДСТВО ПОЛЬЗОВАТЕЛЯ"
     - **es**: "MANUAL DE USUARIO"
     - **sv**: "ANVÄNDARMANUAL"
     - **fi**: "KÄYTTÖOHJE"
     - **zh-Hans**: "用户手册"
     - **zh-Hant**: "使用者手冊"
     - **id**: "PANDUAN PENGGUNA"
     - **fil**: "MANWAL NG GUMAGAMIT"
     - **ja**: "ユーザーマニュアル"
 */
  public static func main_menu_item_user_manual_uc() -> String {
     return localizedString(
         key:"main_menu_item_user_manual_uc",
         defaultValue:"USER MANUAL",
         substitutions: [:])
  }

 /**
"Use the Marshall Bluetooth app to perfect your sound according to the room you're in. You can adjust the volume, treble and bass with ease."

     - **en**: "Use the Marshall Bluetooth app to perfect your sound according to the room you're in. You can adjust the volume, treble and bass with ease."
     - **da**: "Brug Marshall Bluetooth-appen for at gøre lyden perfekt i forhold til det rum, du er i. Du kan nemt justere volumen, diskant og bas."
     - **nl**: "Gebruik de Marshall Bluetooth app om je geluid te perfectioneren afhankelijk van de kamer waarin je je bevindt. Je kunt het volume, de hoge en de lage tonen gemakkelijk aanpassen."
     - **de**: "Verwende die Marshall Bluetooth App, um deinen Sound auf den Raum abzustimmen, in dem du dich befindest. Du kannst die Lautstärke, die Höhen und den Bass ganz einfach einstellen."
     - **fr**: "Grâce à l’application Marshall Bluetooth, adaptez votre son à la pièce dans laquelle vous vous trouvez. Vous pouvez régler facilement le volume, les aigus et les basses."
     - **it-IT**: "Usa l’app Marshall Bluetooth per perfezionare l’esperienza di ascolto in base alla stanza in cui ti trovi. Puoi regolare il volume, gli alti e i bassi con facilità."
     - **nb**: "Bruk Marshall Bluetooth-appen til å perfeksjonere lyden din i henhold til rommet du er i. Du kan enkelt justere volum, diskant og bass."
     - **pt-PT**: "Use a app Bluetooth Marshall para melhorar o som de acordo com a divisão onde está. Pode ajustar o volume, graves e agudos com facilidade."
     - **ru**: "В приложении Marshall Bluetooth можно оптимизировать звук для определенного помещения. Можно без труда регулировать громкость, высокие и низкие частоты."
     - **es**: "Utiliza la app de Bluetooth de Marshall para perfeccionar el sonido en función de la habitación en la que estés. Es fácil ajustar el volumen, los altos y los bajos."
     - **sv**: "Använd Marshall Bluetooth-appen för att ställa in det perfekta ljuded för det rum du är i. Du kan enkelt justera volymen, diskanten och basen."
     - **fi**: "Käytä Marshallin Bluetooth-sovellusta säätääksesi äänen ihanteelliseksi tilan mukaan. Voit säätää äänenvoimakkuutta, diskanttia ja bassoa helposti."
     - **zh-Hans**: "使用 Marshall 蓝牙应用根据您所在的房间改善音效。您可以轻松调节音量、高音和低音。"
     - **zh-Hant**: "使用 Marshall 藍牙應用程式，根據您所在的空間調節出最完美的聲音。您可以輕鬆調整音量、高音和低音。"
     - **id**: "Gunakan aplikasi Marshall Bluetooth untuk menyempurnakan suara Anda sesuai dengan ruang yang digunakan. Anda dapat mengatur volume, treble, dan bass dengan mudah."
     - **fil**: "Gamitin ang Marshall Bluetooth app para ma-perfect ang iyong sound ayon sa kuwarto kung nasaan ka. Maia-adjust mo nang madali ang volume, treble at bass ."
     - **ja**: "Marshall Bluetoothアプリを利用することで、ご自分がいる部屋にサウンドを合わせることができます。音量、高音および低音を簡単に調節できます。"
 */
  public static func ota_screen_customize_subtitle() -> String {
     return localizedString(
         key:"ota_screen_customize_subtitle",
         defaultValue:"Use the Marshall Bluetooth app to perfect your sound according to the room you're in. You can adjust the volume, treble and bass with ease.",
         substitutions: [:])
  }

 /**
"CUSTOMISE YOUR SOUND"

     - **en**: "CUSTOMISE YOUR SOUND"
     - **da**: "TILPAS DIN LYD"
     - **nl**: "PERSONALISEER JE GELUID"
     - **de**: "OPTIMIERE DEINEN SOUND"
     - **fr**: "PERSONNALISEZ VOTRE SON"
     - **it-IT**: "PERSONALIZZA IL TUO ASCOLTO"
     - **nb**: "TILPASS LYDEN DIN"
     - **pt-PT**: "PERSONALIZAR O SEU SOM"
     - **ru**: "ПЕРСОНАЛИЗАЦИЯ ЗВУЧАНИЯ"
     - **es**: "PERSONALIZA TU SONIDO"
     - **sv**: "ANPASSA DITT SOUND"
     - **fi**: "RÄÄTÄLÖI ÄÄNI MAKUSI MUKAAN"
     - **zh-Hans**: "音效自定义"
     - **zh-Hant**: "自訂聲音"
     - **id**: "SESUAIKAN SUARA ANDA"
     - **fil**: "I-CUSTOMIZE ANG IYONG SOUND "
     - **ja**: "サウンドのカスタマイズ"
 */
  public static func ota_screen_customize_title_uc() -> String {
     return localizedString(
         key:"ota_screen_customize_title_uc",
         defaultValue:"CUSTOMISE YOUR SOUND",
         substitutions: [:])
  }

 /**
"Downloading firmware..."

     - **en**: "Downloading firmware..."
     - **da**: "Downloader firmware ..."
     - **nl**: "Firmware downloaden..."
     - **de**: "Firmware wird heruntergeladen…"
     - **fr**: "Téléchargement du firmware…"
     - **it-IT**: "Download del firmware in corso..."
     - **nb**: "Nedlasting av fastvare…"
     - **pt-PT**: "A descarregar o firmware..."
     - **ru**: "Загружается прошивка…"
     - **es**: "Descargando firmware..."
     - **sv**: "Laddar ner firmware…"
     - **fi**: "Ladataan laiteohjelmistoa..."
     - **zh-Hans**: "正在下载固件……"
     - **zh-Hant**: "正在下載韌體... "
     - **id**: "Mengunduh firmware..."
     - **fil**: "Dina-download ang firmware..."
     - **ja**: "ファームウェアをダウンロード中…"
 */
  public static func ota_screen_downloading_firmware() -> String {
     return localizedString(
         key:"ota_screen_downloading_firmware",
         defaultValue:"Downloading firmware...",
         substitutions: [:])
  }

 /**
"Choose one of the several EQ presets or manually adjust the bass, low, mid upper and high frequencies to your sound preferences."

     - **en**: "Choose one of the several EQ presets or manually adjust the bass, low, mid upper and high frequencies to your sound preferences."
     - **da**: "Vælg en af eq-forprogrammeringerne eller justér manuelt bas, lav, mellemste og høj mellemtone samt diskant efter din egen smag."
     - **nl**: "Kies een van de verschillende EQ-voorinstellingen of pas de bas, lage, middelhoge en hoge frequenties handmatig aan je geluidsvoorkeuren aan."
     - **de**: "Wähle eine von mehreren EQ-Voreinstellungen oder stelle den Bass, die Höhen, Tiefen, Mitten, oberen Mitten und Hochfrequenzen nach deinen klanglichen Vorlieben ein."
     - **fr**: "Choisissez l’un des nombreux préréglages de l’égaliseur ou ajustez manuellement les fréquences basses, faibles, mid, plus et hautes en fonction de vos préférences."
     - **it-IT**: "Scegli una delle numerose impostazioni di equalizzazione predefinite oppure regola manualmente le frequenze basse, medie e alte in base alle tue preferenze."
     - **nb**: "Velg en av de flere forhåndsinnstillingene for EQ eller juster bass, lav, mid, øvre og høye frekvenser manuelt, som du ønsker."
     - **pt-PT**: "Escolha uma das várias predefinições EQ ou ajuste manualmente as frequências graves, baixas, médias, superiores e altas de acordo com a sua preferência de som."
     - **ru**: "Выберите одну или несколько предварительных настроек эквалайзера или отрегулируйте басы, низкие, средне-высокие и высокие частоты вручную по своему вкусу."
     - **es**: "Elige una de las preselecciones del ecualizador, o ajusta las frecuencias graves, bajas, medias y agudas conforme a tus preferencias."
     - **sv**: "Använd en av flera olika förinställningar för EQ eller ställ manuellt in diskanten, basen, lägre mellanregister, mellanregister och högre frekvenser enligt dina ljudpreferenser."
     - **fi**: "Valitse jokin lukuisista EQ-esiasetuksista tai säädä bassoa, matalia, keskitason, ylempiä ja korkeita taajuuksia manuaalisesti omien äänimieltymyksiesi mukaisesti."
     - **zh-Hans**: "选择几种 EQ 预设中的一种，或根据您的偏好手动调节低音、低、中、高频音效。"
     - **zh-Hant**: "從多個 EQ 預設設定中進行選擇，或根據您的聲音偏好手動調整低音、低頻，中高頻和高頻。"
     - **id**: "Pilih salah satu dari sejumlah pengaturan awal EQ atau sesuaikan secara manual bass, frekuensi low, mid upper, dan high menurut preferensi suara Anda."
     - **fil**: "Pumili ng isa sa ilang EQ preset o mano-manong i-adjust ang bass, low, mid upper at high frequency na mga gusto mo sa tunog."
     - **ja**: "いくつかあるEQプリセットの中から一つを選ぶか、低音、低、中高および高周波数をお好みのサウンドに手動で調節します。"
 */
  public static func ota_screen_eq_presets_subtitle() -> String {
     return localizedString(
         key:"ota_screen_eq_presets_subtitle",
         defaultValue:"Choose one of the several EQ presets or manually adjust the bass, low, mid upper and high frequencies to your sound preferences.",
         substitutions: [:])
  }

 /**
"EQ PRESETS"

     - **en**: "EQ PRESETS"
     - **da**: "EQ-FORPROGRAMMERINGER"
     - **nl**: "EQ-VOORINSTELLINGEN"
     - **de**: "EQ-VORSTEINSTELLUNGEN"
     - **fr**: "PRÉRÉGLAGES DE L’ÉGALISEUR"
     - **it-IT**: "EQUALIZZAZIONE PREIMPOSTATA"
     - **nb**: "EQ-FORHÅNDINNSTILLINGER"
     - **pt-PT**: "PREDEFINIÇÕES DE EQ"
     - **ru**: "ПРЕДНАСТРОЙКИ ЭКВАЛАЙЗЕРА"
     - **es**: "PRESELECCIONES DEL EQ"
     - **sv**: "FÖRINSTÄLLNINGAR FÖR EQ"
     - **fi**: "EQ-ESIASETUKSET"
     - **zh-Hans**: "EQ 预设"
     - **zh-Hant**: "EQ 預設設定"
     - **id**: "PENGATURAN AWAL EQ"
     - **fil**: "MGA EQ PRESET"
     - **ja**: "EQプリセット"
 */
  public static func ota_screen_eq_presets_title_uc() -> String {
     return localizedString(
         key:"ota_screen_eq_presets_title_uc",
         defaultValue:"EQ PRESETS",
         substitutions: [:])
  }

 /**
"You're all ready to enjoy the Marshall Home Bluetooth System."

     - **en**: "You're all ready to enjoy the Marshall Home Bluetooth System."
     - **da**: "Du er klar til nyde lyden i dit Marshall Home Bluetooth System."
     - **nl**: "Je bent gereed om te genieten van het Marshall Home Bluetooth-systeem."
     - **de**: "Jetzt kannst du dein Marshall Home Bluetooth-System genießen."
     - **fr**: "Vous êtes maintenant prêt à profiter de votre système Marshall Home Bluetooth."
     - **it-IT**: "Ora sei pronto a goderti il sistema Marshall Bluetooth."
     - **nb**: "Nå er Marshall Home Bluetooth-systemet klart."
     - **pt-PT**: "E já está pronto para desfrutar do sistema Home Bluetooth Marshall."
     - **ru**: "Можете использовать домашнюю систему Marshall с Bluetooth в свое удовольствие."
     - **es**: "Ya lo tienes todo listo para disfrutar del sistema Marshall Home Bluetooth."
     - **sv**: "Du kan nu njuta av Marshall Home Bluetooth System."
     - **fi**: "Nyt voit nauttia Marshallin Bluetooth-kotijärjestelmästä."
     - **zh-Hans**: "现在可用尽情体验 Marshall 家庭蓝牙系统。"
     - **zh-Hant**: "您已準備好享受 Marshall Home Bluetooth System。"
     - **id**: "Anda siap menikmati Marshall Home Bluetooth System."
     - **fil**: "Handa ka nang mag-enjoy sa Marshall Home Bluetooth System."
     - **ja**: "Marshall Home Bluetooth Systemをお楽しみいただくための準備が完了しました。"
 */
  public static func ota_screen_finished_subtitle() -> String {
     return localizedString(
         key:"ota_screen_finished_subtitle",
         defaultValue:"You're all ready to enjoy the Marshall Home Bluetooth System.",
         substitutions: [:])
  }

 /**
"You're all ready to enjoy Monitor II A.N.C."

     - **en**: "You're all ready to enjoy Monitor II A.N.C."
     - **da**: "Du er helt klar til at nyde Monitor II A.N.C."
     - **nl**: "Je bent helemaal klaar om te genieten van Monitor II A.N.C."
     - **de**: "Jetzt kannst du deinen Monitor II A.N.C. genießen."
     - **fr**: "Vous êtes maintenant prêt(e) à profiter de votre casque Monitor II A.N.C."
     - **it-IT**: "Sei pronto a goderti Monitor II A.N.C."
     - **nb**: "Du er klar til å nyte Monitor II A.N.C."
     - **pt-PT**: "Está pronto para desfrutar do Monitor II A.N.C."
     - **ru**: "Вы готовы к использованию наушников Monitor II A.N.C."
     - **es**: "Estás listo para disfrutar de Monitor II A.N.C."
     - **sv**: "Du är redo att njuta av Monitor II A.N.C."
     - **fi**: "Olet valmis nauttimaan Monitor II A.N.C. -kuulokkeista."
     - **zh-Hans**: "一切就绪，您可以享受 Monitor II A.N.C. 了"
     - **zh-Hant**: "您已經準備好享受 Monitor II A.N.C."
     - **id**: "Anda siap menikmati Monitor II A.N.C."
     - **fil**: "Nakahanda ka nang tamasahin ang Monitor II A.N.C."
     - **ja**: "Monitor II A.N.C. を楽しむ準備がすべて整いました"
 */
  public static func ota_screen_finished_subtitle_v1() -> String {
     return localizedString(
         key:"ota_screen_finished_subtitle_v1",
         defaultValue:"You're all ready to enjoy Monitor II A.N.C.",
         substitutions: [:])
  }

 /**
"ALL FINISHED"

     - **en**: "ALL FINISHED"
     - **da**: "FÆRDIG"
     - **nl**: "HELEMAAL KLAAR"
     - **de**: "ALLES ERLEDIGT"
     - **fr**: "OPÉRATION FINALISÉE"
     - **it-IT**: "COMPLETATO"
     - **nb**: "HELT KLART"
     - **pt-PT**: "TUDO CONCLUÍDO"
     - **ru**: "ВСЕ ГОТОВО"
     - **es**: "FINALIZADO"
     - **sv**: "SLUTFÖRT"
     - **fi**: "KAIKKI ON VALMISTA"
     - **zh-Hans**: "全部完成"
     - **zh-Hant**: "已全部完成"
     - **id**: "SEMUA SELESAI"
     - **fil**: "TAPOS NA LAHAT"
     - **ja**: "すべて完了"
 */
  public static func ota_screen_finished_title_uc() -> String {
     return localizedString(
         key:"ota_screen_finished_title_uc",
         defaultValue:"ALL FINISHED",
         substitutions: [:])
  }

 /**
"This will take up to 10 minutes."

     - **en**: "This will take up to 10 minutes."
     - **da**: "Dette vil tage op til 10 minutter."
     - **nl**: "Dit duurt maximaal 10 minuten."
     - **de**: "Dies dauert bis zu 10 Minuten."
     - **fr**: "Cela prendra environ 10 minutes."
     - **it-IT**: "Richiederà non più di 10 minuti."
     - **nb**: "Dette tar opptil 10 minutter."
     - **pt-PT**: "Demorará cerca de 10 minutos."
     - **ru**: "Обновление займет до 10 минут."
     - **es**: "Puede tardar hasta 10 minutos."
     - **sv**: "Detta kommer att ta upp till 10 minuter."
     - **fi**: "Päivitys kestää 10 minuuttia."
     - **zh-Hans**: "大约需要 10 分钟时间。"
     - **zh-Hant**: "這需要 10 分鐘的時間。"
     - **id**: "Hal ini akan memakan waktu hingga 10 menit."
     - **fil**: "Tatagal ito ng hanggang 10 minuto."
     - **ja**: "更新には最大10分かかります。"
 */
  public static func ota_screen_notification_subtitle() -> String {
     return localizedString(
         key:"ota_screen_notification_subtitle",
         defaultValue:"This will take up to 10 minutes.",
         substitutions: [:])
  }

 /**
"This update can take up to 10 minutes"

     - **en**: "This update can take up to 10 minutes"
     - **da**: "Denne opdatering kan tage op til 10 minutter"
     - **nl**: "Deze update kan maximaal 10 minuten duren"
     - **de**: "Diese Aktualisierung kann bis zu 10 Minuten dauern"
     - **fr**: "Cette mise à jour peut prendre jusqu’à 10 minutes"
     - **it-IT**: "Questo aggiornamento può richiedere fino a 10 minuti"
     - **nb**: "Denne oppdateringen kan ta opptil 10 minutter"
     - **pt-PT**: "Esta atualização pode levar até 10 minutos"
     - **ru**: "Обновление может занять до 10 минут"
     - **es**: "Esta actualización puede llevar hasta 10 minutos"
     - **sv**: "Denna uppdatering kan ta upp till 10 minuter"
     - **fi**: "Tämä päivitys voi kestää 10 minuuttia"
     - **zh-Hans**: "此更新最多可能需要 10 分钟"
     - **zh-Hant**: "此更新最多可能需要 10 分鐘"
     - **id**: "Pembaruan ini dapat memakan waktu hingga 10 menit"
     - **fil**: "Ang update na ito ay maaaring aabot nang 10 minuto"
     - **ja**: "更新には最高10分掛かる場合があります"
 */
  public static func ota_screen_notification_subtitle_v1() -> String {
     return localizedString(
         key:"ota_screen_notification_subtitle_v1",
         defaultValue:"This update can take up to 10 minutes",
         substitutions: [:])
  }

 /**
"THERE IS A FIRMWARE UPDATED AVAILABLE"

     - **en**: "THERE IS A FIRMWARE UPDATED AVAILABLE"
     - **da**: "DER ER EN FIRMWARE-OPDATERING TILGÆNGELIG"
     - **nl**: "ER IS EEN BIJGEWERKTE FIRMWARE BESCHIKBAAR"
     - **de**: "EIN FIRMWARE-UPDATE IST VERFÜGBAR"
     - **fr**: "UNE MISE À JOUR DU FIRMWARE EST DISPONIBLE"
     - **it-IT**: "È DISPONIBILE UN AGGIORNAMENTO DEL FIRMWARE"
     - **nb**: "EN OPPDATERT FASTVARE KAN LASTES NED"
     - **pt-PT**: "EXISTE UM FIRMWARE ATUALIZADO DISPONÍVEL"
     - **ru**: "ДОСТУПНО ОБНОВЛЕНИЕ ПРОШИВКИ"
     - **es**: "HAY DISPONIBLE UNA ACTUALIZACIÓN DE FIRMWARE"
     - **sv**: "DET FINNS EN UPPDATERAD FIRMWARE"
     - **fi**: "PÄIVITETTY LAITEOHJELMISTO ON SAATAVILLA"
     - **zh-Hans**: "有可用的固件更新"
     - **zh-Hant**: "有可更新的韌體"
     - **id**: "TERSEDIA PEMBARUAN FIRMWARE"
     - **fil**: "MAY MAKUKUHANG FIRMWARE UPDATE"
     - **ja**: "ファームウェア更新プログラムが利用可能になりました"
 */
  public static func ota_screen_notification_title_uc() -> String {
     return localizedString(
         key:"ota_screen_notification_title_uc",
         defaultValue:"THERE IS A FIRMWARE UPDATED AVAILABLE",
         substitutions: [:])
  }

 /**
"Update completed."

     - **en**: "Update completed."
     - **da**: "Opdatering afsluttet"
     - **nl**: "Update voltooid"
     - **de**: "Update abgeschlossen"
     - **fr**: "Mise à jour terminée"
     - **it-IT**: "Aggiornamento effettuato"
     - **nb**: "Oppdatering utført"
     - **pt-PT**: "Atualização concluída"
     - **ru**: "Обновление установлено"
     - **es**: "Actualización completada"
     - **sv**: "Uppdatering klar"
     - **fi**: "Päivitys valmis"
     - **zh-Hans**: "完成更新"
     - **zh-Hant**: "更新完成"
     - **id**: "Pembaruan selesai"
     - **fil**: "Tapos na ang pag-update"
     - **ja**: "アップデート完了"
 */
  public static func ota_screen_state_completed() -> String {
     return localizedString(
         key:"ota_screen_state_completed",
         defaultValue:"Update completed.",
         substitutions: [:])
  }

 /**
"Update not started..."

     - **en**: "Update not started..."
     - **da**: "Opdatering ikke startet..."
     - **nl**: "Update is niet gestart..."
     - **de**: "Update nicht gestartet ..."
     - **fr**: "La mise à jour n’a pas commencé…"
     - **it-IT**: "Aggiornamento non iniziato..."
     - **nb**: "Oppdatering ikke startet …"
     - **pt-PT**: "Atualização não iniciada…"
     - **ru**: "Обновление не запущено…"
     - **es**: "La actualización no ha comenzado…"
     - **sv**: "Uppdatering ej påbörjad …"
     - **fi**: "Päivitys ei ole alkanut..."
     - **zh-Hans**: "未开始更新... "
     - **zh-Hant**: "更新尚未開始... "
     - **id**: "Pembaruan tidak dimulai..."
     - **fil**: "Hindi nasimulan ang update..."
     - **ja**: "アップデートが開始していません... "
 */
  public static func ota_screen_state_not_started() -> String {
     return localizedString(
         key:"ota_screen_state_not_started",
         defaultValue:"Update not started...",
         substitutions: [:])
  }

 /**
"Unzipping files…"

     - **en**: "Unzipping files…"
     - **da**: "Udpakker filer..."
     - **nl**: "Bestanden uitpakken…"
     - **de**: "Dateien werden entpackt …"
     - **fr**: "Décompression des fichiers…"
     - **it-IT**: "Decompressione file in corso…"
     - **nb**: "Pakker ut filer ..."
     - **pt-PT**: "A descomprimir ficheiros…"
     - **ru**: "Идет распаковка файлов…"
     - **es**: "Descomprimiendo archivos…"
     - **sv**: "Packar upp filer …."
     - **fi**: "Tiedostoja puretaan..."
     - **zh-Hans**: "正在解压文件... "
     - **zh-Hant**: "正在解壓縮檔案... "
     - **id**: "Mengekstrak file…"
     - **fil**: "Ina-unzip ang mga file..."
     - **ja**: "ファイルの解凍中... "
 */
  public static func ota_screen_state_unzipping() -> String {
     return localizedString(
         key:"ota_screen_state_unzipping",
         defaultValue:"Unzipping files…",
         substitutions: [:])
  }

 /**
"Updating firmware…"

     - **en**: "Updating firmware…"
     - **da**: "Opdaterer firmware..."
     - **nl**: "Firmware updaten…"
     - **de**: "Firmware wird aktualisiert …"
     - **fr**: "Mise à jour du firmware…"
     - **it-IT**: "Aggiornamento firmware…"
     - **nb**: "Oppdaterer fastvare ..."
     - **pt-PT**: "A atualizar a firmware…"
     - **ru**: "Прошивка обновляется…"
     - **es**: "Actualizando firmware…"
     - **sv**: "Uppdaterar firmwaret …"
     - **fi**: "Laiteohjelmistoa päivitetään..."
     - **zh-Hans**: "正在更新固件... "
     - **zh-Hant**: "正在更新韌體... "
     - **id**: "Memperbarui firmware…"
     - **fil**: "Ina-update ang firmware..."
     - **ja**: "ファームウェアのアップデート中... "
 */
  public static func ota_screen_state_updating() -> String {
     return localizedString(
         key:"ota_screen_state_updating",
         defaultValue:"Updating firmware…",
         substitutions: [:])
  }

 /**
"Uploading firmware to device…"

     - **en**: "Uploading firmware to device…"
     - **da**: "Uploader firmware til enhed..."
     - **nl**: "Firmware naar het apparaat uploaden…"
     - **de**: "Firmware wird auf das Gerät geladen …"
     - **fr**: "Téléchargement du firmware sur l’appareil…"
     - **it-IT**: "Caricamento firmware sul dispositivo…"
     - **nb**: "Laster opp fastvare til enhet ..."
     - **pt-PT**: "A transferir a firmware para o dispositivo…"
     - **ru**: "Идет обновление прошивки на устройстве…"
     - **es**: "Cargando firmware en el dispositivo…"
     - **sv**: "Laddar upp firmwaret till enheten …"
     - **fi**: "Laiteohjelmistoa ladataan laitteelle..."
     - **zh-Hans**: "正在向设备上传固件... "
     - **zh-Hant**: "正在將韌體上傳至裝置... "
     - **id**: "Mengunggah firmware ke perangkat…"
     - **fil**: "Ina-upload ang firmware sa device..."
     - **ja**: "ファームウェアのデバイスへのアップロード中... "
 */
  public static func ota_screen_state_uploading_to_device() -> String {
     return localizedString(
         key:"ota_screen_state_uploading_to_device",
         defaultValue:"Uploading firmware to device…",
         substitutions: [:])
  }

 /**
"Pair two Marshall Bluetooth speakers together for stereo sound. This allows you to play with split audio, utilising the left and right channels."

     - **en**: "Pair two Marshall Bluetooth speakers together for stereo sound. This allows you to play with split audio, utilising the left and right channels."
     - **da**: "Par to Marshall Bluetooth-højttalere sammen for at få stereolyd. På den måde kan du afspille med delt lyd og bruge venstre og højre kanal."
     - **nl**: "Koppel twee Marshall Bluetooth luidsprekers aan elkaar voor een stereogeluid. Dit stelt je in staat om gesplitste audio af te spelen, door gebruik te maken van het linker- en rechterkanaal."
     - **de**: "Kopple zwei Marshall Bluetooth-Lautsprecher zusammen für Stereo-Sound. Dies ermöglicht dir, mit Split-Audio auf dem linken und rechten Kanal zu experimentieren."
     - **fr**: "Assemblez deux enceintes Bluetooth Marshall pour un son stéréo. Ceci vous permet de lire des pistes audio séparées en utilisant les canaux droite et gauche."
     - **it-IT**: "Abbina due diffusori Bluetooth Marshall per un suono stereo. Questo ti consente di riprodurre l’audio separatamente sui canali destro e sinistro."
     - **nb**: "Par to Marshall Bluetooth-høyttalere sammen for stereolyd. Dette lar deg spille med delt lyd, ved hjelp av venstre og høyre kanaler."
     - **pt-PT**: "Emparelhe dois altifalantes Bluetooth Marshall para som estéreo. Isso permite a reprodução de áudio com separação, utilizando os canais esquerdo e direito."
     - **ru**: "Свяжите две колонки Marshall с Bluetooth для создания стереозвучания. Благодаря этому можно проигрывать звук раздельно через левый и правый каналы."
     - **es**: "Asocia dos altavoces Marshall con Bluetooth para disfrutar de sonido en estéreo; podrás dividir la señal de audio en dos canales, uno izquierdo y otro derecho."
     - **sv**: "Koppla ihop två Marshall Bluetooth-högtalare för stereoljud. Detta låter dig spela upp med delat stereoljud, med hjälp av vänster och höger kanal."
     - **fi**: "Muodosta laitepariksi kaksi Marshallin Bluetooth-kaiutinta stereoäänen luomiseksi. Näin voit jakaa äänen käyttämällä vasenta ja oikeaa kanavaa."
     - **zh-Hans**: "将两个 Marshall 蓝牙音箱配对以获得立体声。允许您使用左右声道以分割音频进行播放。"
     - **zh-Hant**: "配對兩個 Marshall 藍牙喇叭以達到立體聲音效，讓您使用左右聲道播放分開的音訊。"
     - **id**: "Pasangkan dua speaker Bluetooth Marshall bersamaan untuk menghadirkan suara stereo. Ini memungkinkan Anda memutar dua audio terpisah dengan memanfaatkan saluran kiri dan kanan."
     - **fil**: "Magpares ng dalawang Marshall Bluetooth speaker nang magkasama para sa stereo sound. Pahihintulutan ka nitong magpatugtog ng split audio, gamit ang kaliwa at kanang channel."
     - **ja**: "ステレオサウンドを実現するには、2つのMarshall Bluetoothスピーカーをペアリングします。それにより、左と右のチャンネルを分岐させてオーディオを再生できます。"
 */
  public static func ota_screen_stereo_pairing_subtitle_uc() -> String {
     return localizedString(
         key:"ota_screen_stereo_pairing_subtitle_uc",
         defaultValue:"Pair two Marshall Bluetooth speakers together for stereo sound. This allows you to play with split audio, utilising the left and right channels.",
         substitutions: [:])
  }

 /**
"STEREO PAIRING"

     - **en**: "STEREO PAIRING"
     - **da**: "STEREOPARRING"
     - **nl**: "STEREO KOPPELEN"
     - **de**: "STEREO-KOPPELUNG"
     - **fr**: "ASSEMBLAGE STÉRÉO"
     - **it-IT**: "ABBINAMENTO STEREO"
     - **nb**: "STEREOPARING"
     - **pt-PT**: "EMPARELHAMENTO ESTÉREO"
     - **ru**: "СВЯЗЫВАНИЕ СТЕРЕОУСТРОЙСТВ"
     - **es**: "CONEXIÓN ESTÉREO"
     - **sv**: "STEREOANSLUTNING"
     - **fi**: "STEREOPARILIITOS"
     - **zh-Hans**: "立体声配对"
     - **zh-Hant**: "立體聲配對"
     - **id**: "PEMASANGAN STEREO"
     - **fil**: "PAGPAPARES SA STEREO"
     - **ja**: "ステレオのペアリング"
 */
  public static func ota_screen_stereo_pairing_title_uc() -> String {
     return localizedString(
         key:"ota_screen_stereo_pairing_title_uc",
         defaultValue:"STEREO PAIRING",
         substitutions: [:])
  }

 /**
"Monitor II A.N.C. has powerful audio control capabilities. Press the ANC button on the left earcap to toggle between Noise Cancelling and Social Mode. Press and hold the button to turn ANC off/on."

     - **en**: "Monitor II A.N.C. has powerful audio control capabilities. Press the ANC button on the left earcap to toggle between Noise Cancelling and Social Mode. Press and hold the button to turn ANC off/on."
     - **da**: "Monitor II A.N.C. har kraftige lydkontrolfunktioner. Tryk på ANC-knappen på venstre øremuffe for at skifte mellem Noise Cancelling og Social Mode. Tryk på knappen og hold den nede for at slå ANC til/fra."
     - **nl**: "Monitor II A.N.C. heeft krachtige audiobedieningsmogelijkheden. Druk op de ANC-knop op de linkeroordop om te wisselen tussen ruisonderdrukking en de sociale modus. Houd de knop ingedrukt om ANC uit of in te schakelen."
     - **de**: "Monitor II A.N.C. hat leistungsstarke Funktionen zur Steuerung des Klangs. Drücke die ANC-Taste auf der linken Ohrmuschel, um zwischen Geräuschunterdrückung und Unterhaltungsmodus zu wechseln. Halte die Taste gedrückt, um ANC ein- oder auszuschalten."
     - **fr**: "Le Monitor II A.N.C. dispose de capacités de contrôle audio performantes. Appuyez sur le bouton ANC du pavillon d’écouteur gauche afin de basculer entre les modes Contrôle du bruit et Social. Maintenez le bouton le bouton enfoncé pour allumer/éteindre la fonction ANC."
     - **it-IT**: "Monitor II A.N.C. ha potenti funzioni di controllo del suono. Premi il pulsante ANC sul padiglione sinistro per passare tra la modalità di cancellazione del rumore e quella Social. Tieni premuto il pulsante per accendere e spegnere l’ANC."
     - **nb**: "Monitor II A.N.C. har effektive funksjoner for lydkontroll. Trykk på ANC-knappen på venstre ørekopp for å skifte mellom støydemping og sosial modus. Trykk og hold knappen for å slå ANC av/på."
     - **pt-PT**: "O monitor II A.N.C. tem poderosos recursos de controlo de áudio. Pressione o botão ANC no auscultador esquerdo para alternar entre o cancelamento de ruído e o modo social. Mantenha pressionado o botão para ativar/desativar o ANC."
     - **ru**: "Наушники Monitor II A.N.C. обеспечивают расширенные функции управления звуком. Нажмите кнопку ANC на левой чашке наушников для переключения между шумоподавлением и режимом Social. Нажмите и удерживайте кнопку для включения/выключения ANC."
     - **es**: "Monitor II A.N.C. cuenta con potentes capacidades de control del sonido. Pulsa el botón ANC en la carcasa izquierda para cambiar entre cancelación de ruido y modo social. Mantén pulsado el botón para activar y desactivar la ANC."
     - **sv**: "Monitor II A.N.C. har kraftfulla ljudkontrollsfunktioner. Tryck på brusreduceringsknappen på den vänstra öronkåpan för att växla mellan aktiv brusreducering och Social Mode. Tryck och håll ner knappen för att slå på/av aktiv brusreducering."
     - **fi**: "Monitor II A.N.C. -kuulokkeissa on tehokkaat äänenhallintaominaisuudet. Paina vasemmanpuoleisen korvakupin kyljessä olevaa vastamelupainiketta (ANC) vaihtaaksesi vastamelun ja sosiaalisen tilan välillä."
     - **zh-Hans**: "Monitor II A.N.C. 具有强大的音频控制功能。按下左耳套上的 ANC 按钮可在噪声消除和社交模式之间切换。按住该按钮可打开/关闭 ANC。"
     - **zh-Hant**: "Monitor II A.N.C. 具有強大的音訊控制功能。按下左耳帽上的 ANC 按鈕在降噪和社交模式之間切換。長按住此按鈕可開啟/關閉 ANC。"
     - **id**: "Monitor II A.N.C mempunyai kapabilitas kontrol audio yang istimewa. Tekan tombol ANC di earcap kiri untuk beralih antara Mode Peredam Kebisingan dengan Mode Sosial. Tekan dan tahan untuk menyalakan/mematikan ANC."
     - **fil**: "Ang Monitor II A.N.C. ay may matinding mga kakayanan upang kontrolin ang audio. Pindutin ang ANC button sa kaliwang earcap upang i-toggle sa pagitan ng Noise Cancelling at Social Mode. Pindutin nang matagal ang button upang mag-off/on ang ANC."
     - **ja**: "Monitor II A.N.C. はパワフルなオーディオコントロール機能が搭載されています。左側のイヤーキャップにあるANCボタンを押すと、ノイズキャンセリングとソーシャルモード間を切り替えることができます。ボタンを長押しすると、ANCをオン／オフすることができます。"
 */
  public static func ozzy_onboarding_anc_subtitle() -> String {
     return localizedString(
         key:"ozzy_onboarding_anc_subtitle",
         defaultValue:"Monitor II A.N.C. has powerful audio control capabilities. Press the ANC button on the left earcap to toggle between Noise Cancelling and Social Mode. Press and hold the button to turn ANC off/on.",
         substitutions: [:])
  }

 /**
"ACTIVE NOISE CANCELLING"

     - **en**: "ACTIVE NOISE CANCELLING"
     - **da**: "ACTIVE NOISE CANCELLING"
     - **nl**: "ACTIEVE RUISONDERDRUKKING"
     - **de**: "AKTIVE GERÄUSCHUNTERDRÜCKUNG"
     - **fr**: "RÉDUCTION DE BRUIT ACTIVE"
     - **it-IT**: "CANCELLAZIONE DEL RUMORE ATTIVA"
     - **nb**: "AKTIV STØYDEMPING"
     - **pt-PT**: "CANCELAMENTO DE RUÍDO ATIVO"
     - **ru**: "АКТИВНОЕ ШУМОПОДАВЛЕНИЕ"
     - **es**: "CANCELACIÓN ACTIVA DE RUIDO"
     - **sv**: "AKTIV BRUSREDUCERING"
     - **fi**: "VASTAMELU"
     - **zh-Hans**: "主动噪声消除"
     - **zh-Hant**: "主動降噪"
     - **id**: "ACTIVE NOISE CANCELLING"
     - **fil**: "ACTIVE NOISE CANCELLING"
     - **ja**: "ノイズキャンセリングを有効にする"
 */
  public static func ozzy_onboarding_anc_title_uc() -> String {
     return localizedString(
         key:"ozzy_onboarding_anc_title_uc",
         defaultValue:"ACTIVE NOISE CANCELLING",
         substitutions: [:])
  }

 /**
"Choose one of the several EQ presets, or manually fine tune the equaliser to fit your preferences."

     - **en**: "Choose one of the several EQ presets, or manually fine tune the equaliser to fit your preferences."
     - **da**: "Vælg en af de flere EQ-forudindstillinger, eller finjuster equalizeren manuelt, så den passer til dine præferencer."
     - **nl**: "Kies een van de verschillende EQ-presets of stel de equalizer handmatig af volgens je voorkeuren."
     - **de**: "Wähle aus mehreren EQ-Voreinstellungen aus oder stelle den Equalizer manuell ganz nach deinen Wünschen ein."
     - **fr**: "Choisissez parmi plusieurs préréglages d’égalisation ou ajustez l’égaliseur manuellement en fonction de vos préférences."
     - **it-IT**: "Scegli uno dei diversi preset EQ oppure sintonizza manualmente l’equalizzatore in base alle tue preferenze."
     - **nb**: "Velg en av EQ-innstillingene eller finjuster equalizeren selv etter dine egne ønsker."
     - **pt-PT**: "Escolha uma das várias predefinições de equalização ou ajuste manualmente o equalizador para atender às suas preferências."
     - **ru**: "Можно выбрать одну из нескольких настроек эквалайзера или вручную задать его параметры в соответствии с вашими предпочтениями."
     - **es**: "Elige una de las varias preselecciones de ecualizador, o ajusta manualmente el ecualizador según tus preferencias."
     - **sv**: "Välj en av flera EQ-förinställningar eller finjustera ljudet för att passa dina preferenser manuellt."
     - **fi**: "Valitse yksi useasta taajuuskorjaimen esiasetuksesta tai hienosäädä taajuuskorjain manuaalisesti asetuksiisi sopivaksi."
     - **zh-Hans**: "选择多项 EQ 预设之一，或手动微调均衡器以匹配您的偏好。"
     - **zh-Hant**: "選擇多個 EQ 預設之一，或手動微調等化器以適合您的偏好。"
     - **id**: "Pilih satu dari beberapa preset EQ, atau atur equaliser secara manual sesuai dengan preferensi Anda."
     - **fil**: "Pumili ng isa sa ilang mga EQ preset, o mano-manong i-fine tune ang equaliser upang umakma ito sa mga gusto mo."
     - **ja**: "複数のEQプリセットから1つ選択、またはイコライザーをお好みで微調整します。"
 */
  public static func ozzy_onboarding_eq_subtitle() -> String {
     return localizedString(
         key:"ozzy_onboarding_eq_subtitle",
         defaultValue:"Choose one of the several EQ presets, or manually fine tune the equaliser to fit your preferences.",
         substitutions: [:])
  }

 /**
"FIVE BAND EQUALISER"

     - **en**: "FIVE BAND EQUALISER"
     - **da**: "FEMBÅNDS EQUALIZER"
     - **nl**: "VIJF-BANDS EQUALIZER"
     - **de**: "FÜNF-BAND-EQUALIZER"
     - **fr**: "ÉGALISEUR À CINQ BANDES"
     - **it-IT**: "EQUALIZZATORE A CINQUE BANDE"
     - **nb**: "FEMBÅNDS EQUALIZER"
     - **pt-PT**: "EQUALIZADOR DE CINCO BANDAS"
     - **ru**: "ПЯТИПОЛОСНЫЙ ЭКВАЛАЙЗЕР"
     - **es**: "ECUALIZADOR DE CINCO BANDAS"
     - **sv**: "FEMBANDS-EQ"
     - **fi**: "VIISIKAISTAINEN TAAJUUSKORJAIN"
     - **zh-Hans**: "五频段均衡器"
     - **zh-Hant**: "五頻段等化器"
     - **id**: "EQUALISER LIMA BAND"
     - **fil**: "LIMANG BAND NA EQUALISER"
     - **ja**: "ファイブバンド・イコライザー"
 */
  public static func ozzy_onboarding_eq_title_uc() -> String {
     return localizedString(
         key:"ozzy_onboarding_eq_title_uc",
         defaultValue:"FIVE BAND EQUALISER",
         substitutions: [:])
  }

 /**
"The multi-function M-Button on the right earcap lets you switch between three EQ presets, alternatively set it to instantly access Google Voice Assistant or Apple Siri."

     - **en**: "The multi-function M-Button on the right earcap lets you switch between three EQ presets, alternatively set it to instantly access Google Voice Assistant or Apple Siri."
     - **da**: "Den multifunktionelle M-knap på højre øremuffe giver dig mulighed for at skifte mellem tre EQ-forudindstillinger, alternativt kan du indstille den til øjeblikkelig adgang til Google Voice Assistant eller Apple Siri."
     - **nl**: "Met de multifunctionele M-knop op de rechteroordop kun je wisselen tussen drie EQ-presets of je kunt hem instellen om direct toegang te krijgen tot Google Voice Assistant of Apple Siri."
     - **de**: "Die multifunktionale M-Taste auf der rechten Ohrmuschel ermöglicht das Wechseln zwischen drei EQ-Voreinstellungen, du kannst sie aber auch so einstellen, dass sie sofort den Google Voice Assistant oder Apple Siri aktiviert."
     - **fr**: "Le bouton M multifonctionnel placé sur le pavillon d’écouteur droit vous permet de choisir entre trois préréglages d’égalisation, ou bien il peut être configuré pour accéder instantanément à l’Assistant Vocal Google ou à Apple Siri."
     - **it-IT**: "Il pulsante M multifunzione sul padiglione destro ti permette di passare tra tre preset EQ, oppure puoi impostarlo per accedere istantaneamente all’Assistente vocale Google o a Siri (Apple)."
     - **nb**: "Med flerfunksjonsknappen, M-knappen, på høyre ørekopp, kan du skifte mellom tre EQ-innstillinger, eventuelt gi tilgang til Google Voice Assistant eller Apple Siri."
     - **pt-PT**: "O botão M multifuncional no auscultador direito permite alternar entre três predefinições de equalização, ou, em alternativa, ser definido para aceder instantaneamente ao Assistente de Voz Google ou à Siri da Apple."
     - **ru**: "Многофункциональная M-кнопка на правой чашке наушников позволяет выбрать одну из трех настроек эквалайзера, а также быстро включить Google Voice Assistant или Apple Siri."
     - **es**: "El botón M multifunción en la carcasa derecha te permite cambiar entre tres preselecciones de ecualizador, o fijarlo para que acceda al instante al Asistente de voz de Google o a Apple Siri."
     - **sv**: "Med den multifunktionella M-knappen på den högra öronkåpan kan du växla mellan tre EQ-förinställningar alternativt få direkt tillgång till Google Voice Assistant eller Apple Siri."
     - **fi**: "Oikeanpuoleisen korvakupin kyljessä olevalla M-monitoimipainikkeella voit vaihdella kolmen taajuuskorjaimen esiasetuksen välillä tai asettaa kuulokkeet käyttämään välittömästi Google Voice Assistantia tai Apple Siriä."
     - **zh-Hans**: "通过右耳套上的多功能 M-按钮可在三项 EQ 预设之间切换，也可将其设置为立即访问 Google Voice Assistant 或 Apple Siri。"
     - **zh-Hant**: "右耳帽上的多功能 M 按鈕可讓您在三個 EQ 預設之間切換，或者將其設置為立即與 Google Voice Assistant 或 Apple Siri 通話。"
     - **id**: "M-Button multifungsi di earcap kanan dapat digunakan untuk beralih antara tiga preset EQ. Selain itu, tombol ini dapat digunakan sebagai akses instan ke Google Voice Assistant atau Apple Siri."
     - **fil**: "Ang multi-function na M-Button sa kanang earcap ay hahayaan kang lumipat sa pagitan ng tatlong EQ preset, bilang alternatibo ay itakda ito upang agarang maka-access sa Google Voice Assistant o Apple Siri."
     - **ja**: "右側のイヤーキャップにあるマルチ機能のMボタンは、3つのEQ設定間の切り替え、またはGoogle音声アシスタント、Apple Siriの切り替えを行うことができます。"
 */
  public static func ozzy_onboarding_mbutton_subtitle() -> String {
     return localizedString(
         key:"ozzy_onboarding_mbutton_subtitle",
         defaultValue:"The multi-function M-Button on the right earcap lets you switch between three EQ presets, alternatively set it to instantly access Google Voice Assistant or Apple Siri.",
         substitutions: [:])
  }

 /**
"CUSTOMISABLE M-BUTTON"

     - **en**: "CUSTOMISABLE M-BUTTON"
     - **da**: "M-KNAP SOM KAN TILPASSES"
     - **nl**: "AANPASBARE M-KNOP"
     - **de**: "INDIVIDUELL BELEGBARE M-TASTE"
     - **fr**: "BOUTON M PERSONNALISABLE"
     - **it-IT**: "PULSANTE M PERSONALIZZABILE"
     - **nb**: "EGENDEFINERBAR M-KNAPP"
     - **pt-PT**: "BOTÃO M PERSONALIZÁVEL"
     - **ru**: "ИНДИВИДУАЛЬНО НАСТРАИВАЕМАЯ M-КНОПКА"
     - **es**: "BOTÓN M PERSONALIZABLE"
     - **sv**: "ANPASSNINGSBAR M-KNAPP"
     - **fi**: "MUKAUTETTAVA M-PAINIKE"
     - **zh-Hans**: "可定制的 M-按钮"
     - **zh-Hant**: "可自訂的 M 按鈕"
     - **id**: "M-BUTTON YANG DAPAT DISESUAIKAN"
     - **fil**: "NAKO-CUSTOMIZE NA M-BUTTON"
     - **ja**: "カスタマイズ可能なMボタン"
 */
  public static func ozzy_onboarding_mbutton_title_uc() -> String {
     return localizedString(
         key:"ozzy_onboarding_mbutton_title_uc",
         defaultValue:"CUSTOMISABLE M-BUTTON",
         substitutions: [:])
  }

 /**
"You are all ready to enjoy %s."

     - **en**: "You are all ready to enjoy %s."
     - **da**: "Du er helt klar til at nyde %s."
     - **nl**: "Je bent helemaal klaar om te genieten van %s."
     - **de**: "Jetzt kannst du %s genießen."
     - **fr**: "Vous êtes prêt(e) à profiter de %s."
     - **it-IT**: "Sei pronto a goderti %s."
     - **nb**: "Du er klar til å nyte %s."
     - **pt-PT**: "Está pronto para desfrutar de %s."
     - **ru**: "Вы готовы к полноценному использованию %s."
     - **es**: "Ya estás listo para disfrutar de tu %s."
     - **sv**: "Du är redo att njuta av %s."
     - **fi**: "Olet valmis nauttimaan %s."
     - **zh-Hans**: "一切就绪，您可以享受 %s 了。"
     - **zh-Hant**: "您已經準備好享受 ％s。"
     - **id**: "Anda siap untuk menikmati %s."
     - **fil**: "Nakahanda ka nang tamasahin ang %s."
     - **ja**: "%s を楽しむ準備がすべて整いました。"
 */
  public static func pairing_screen_finished_subtitle() -> String {
     return localizedString(
         key:"pairing_screen_finished_subtitle",
         defaultValue:"You are all ready to enjoy %s.",
         substitutions: [:])
  }

 /**
"SELECT YOUR HEADPHONES"

     - **en**: "SELECT YOUR HEADPHONES"
     - **da**: "VÆLG DINE HOVEDTELEFONER"
     - **nl**: "KIES JE HOOFDTELEFOON"
     - **de**: "WÄHLE DEINEN KOPFHÖRER AUS"
     - **fr**: "SÉLECTIONNEZ VOTRE CASQUE"
     - **it-IT**: "SELEZIONA LE TUE CUFFIE"
     - **nb**: "VELG HODETELEFONENE DINE"
     - **pt-PT**: "SELECIONE OS SEUS AUSCULTADORES"
     - **ru**: "ВЫБЕРИТЕ НАУШНИКИ"
     - **es**: "SELECCIONAR TUS AURICULARES"
     - **sv**: "ANSLUT DINA HÖRLURAR"
     - **fi**: "VALITSE OMAT KUULOKKEESI"
     - **zh-Hans**: "选择您的耳机"
     - **zh-Hant**: "選擇您的耳機"
     - **id**: "PILIH HEADPHONE ANDA"
     - **fil**: "PILIIN ANG IYONG MGA HEADPHONE"
     - **ja**: "ヘッドフォンを選択する"
 */
  public static func pairing_select_device_title_uc() -> String {
     return localizedString(
         key:"pairing_select_device_title_uc",
         defaultValue:"SELECT YOUR HEADPHONES",
         substitutions: [:])
  }

 /**
"Your headphones are now connected."

     - **en**: "Your headphones are now connected."
     - **da**: "Dine hovedtelefoner er nu tilsluttet."
     - **nl**: "Je hoofdtelefoon is nu verbonden."
     - **de**: "Dein Kopfhörer ist jetzt verbunden."
     - **fr**: "Votre casque est maintenant connecté."
     - **it-IT**: "Le tue cuffie sono connesse."
     - **nb**: "Hodetelefonene er tilkoblet."
     - **pt-PT**: "Os seus auscultadores estão agora conectados."
     - **ru**: "Наушники подключены."
     - **es**: "Tus auriculares ya están conectados."
     - **sv**: "Dina hörlurar är nu anslutna."
     - **fi**: "Kuulokkeesi on nyt liitetty."
     - **zh-Hans**: "您的耳机现已连接。"
     - **zh-Hant**: "您的耳機現在已連接。"
     - **id**: "Headphone Anda telah terhubung."
     - **fil**: "Nakakonekta na ang iyong mga headphone."
     - **ja**: "ヘッドフォンが接続されました。"
 */
  public static func pairing_successful_subtitle() -> String {
     return localizedString(
         key:"pairing_successful_subtitle",
         defaultValue:"Your headphones are now connected.",
         substitutions: [:])
  }

 /**
"PAIRING SUCCESSFUL"

     - **en**: "PAIRING SUCCESSFUL"
     - **da**: "PARRING VELLYKKET"
     - **nl**: "KOPPELEN GELUKT"
     - **de**: "KOPPELN ERFOLGREICH"
     - **fr**: "APPAIRAGE RÉUSSI"
     - **it-IT**: "ABBINAMENTO RIUSCITO"
     - **nb**: "PARINGEN LYKTES IKKE"
     - **pt-PT**: "EMPARELHAMENTO BEM-SUCEDIDO"
     - **ru**: "СОПРЯЖЕНИЕ ВЫПОЛНЕНО"
     - **es**: "EMPAREJAMIENTO SATISFACTORIO"
     - **sv**: "PARKOPPLING LYCKADES"
     - **fi**: "LAITEPARIN MUODOSTUS ONNISTUI"
     - **zh-Hans**: "配对成功"
     - **zh-Hant**: "配對成功"
     - **id**: "PEMASANGAN BERHASIL"
     - **fil**: "MATAGUMPAY ANG PAGPAPARES"
     - **ja**: "ペアリング成功"
 */
  public static func pairing_successful_title_uc() -> String {
     return localizedString(
         key:"pairing_successful_title_uc",
         defaultValue:"PAIRING SUCCESSFUL",
         substitutions: [:])
  }

 /**
"It can take a few minutes before your headphones appear in the list."

     - **en**: "It can take a few minutes before your headphones appear in the list."
     - **da**: "Det kan tage et par minutter, før dine hovedtelefoner vises på listen."
     - **nl**: "Het kan enkele minuten duren voordat je hoofdtelefoon in de lijst wordt weergegeven."
     - **de**: "Es kann einige Minuten dauern, bis dein Kopfhörer in der Liste aufgeführt wird."
     - **fr**: "L’apparition de votre casque dans la liste peut prendre quelques minutes."
     - **it-IT**: "Potrebbero volerci alcuni minuti prima di visualizzare le tue cuffie nell’elenco."
     - **nb**: "Det kan ta noen minutter før hodetelefonene dine vises på listen."
     - **pt-PT**: "Pode demorar alguns minutos até os auscultadores aparecerem na lista."
     - **ru**: "Наушники могут появиться в списке через несколько минут."
     - **es**: "Es posible que pasen unos minutos antes de que tus auriculares aparezcan en la lista."
     - **sv**: "Det kan ta några minuter innan dina hörlurar visas i listan."
     - **fi**: "Saattaa kulua muutama minuutti, ennen kuin kuulokkeesi näkyvät luettelossa."
     - **zh-Hans**: "您的耳机可能需要几分钟才能显示在列表中。"
     - **zh-Hant**: "耳機可能需要幾分鐘才能顯示在列表中。"
     - **id**: "Dibutuhkan beberapa menit hingga headphone Anda muncul dalam daftar."
     - **fil**: "Maaaring abutin nang ilang minuto bago lilitaw ang iyong mga headphone sa listahan."
     - **ja**: "ヘッドフォンがリストに表示されるまで数分間かかる場合があります。"
 */
  public static func pairing_waiting_description() -> String {
     return localizedString(
         key:"pairing_waiting_description",
         defaultValue:"It can take a few minutes before your headphones appear in the list.",
         substitutions: [:])
  }

 /**
"AUX"

     - **en**: "AUX"
     - **da**: "AUX"
     - **nl**: "AUX"
     - **de**: "AUX"
     - **fr**: "AUX"
     - **it-IT**: "AUX"
     - **nb**: "AUX"
     - **pt-PT**: "Entrada auxiliar"
     - **ru**: "Выход для внешнего сигнала (AUX)"
     - **es**: "AUX"
     - **sv**: "AUX"
     - **fi**: "AUX"
     - **zh-Hans**: "AUX 接口"
     - **zh-Hant**: "AUX"
     - **id**: "AUX"
     - **fil**: "AUX"
     - **ja**: "AUX"
 */
  public static func player_audio_source_aux() -> String {
     return localizedString(
         key:"player_audio_source_aux",
         defaultValue:"AUX",
         substitutions: [:])
  }

 /**
"Bluetooth audio is disconnected while connected via 3.5 mm cable"

     - **en**: "Bluetooth audio is disconnected while connected via 3.5 mm cable"
     - **da**: "Bluetooth-lyd kobles fra, mens den er tilsluttet via 3,5 mm kablet"
     - **nl**: "Bluetooth-audio is losgekoppeld terwijl er een verbinding via een 3,5 mm-kabel is"
     - **de**: "Bluetooth-Audio wird getrennt, wenn eine Verbindung über ein 3,5-mm-Kabel hergestellt wird"
     - **fr**: "L’audio Bluetooth est déconnecté lorsque le casque est connecté via le câble de 3,5 mm"
     - **it-IT**: "L’audio Bluetooth viene scollegato durante il collegamento tramite cavo da 3,5 mm"
     - **nb**: "Bluetooth-lyd kobles fra når tilkoblet via 3,5 mm-kabelen"
     - **pt-PT**: "O áudio Bluetooth é desconectado enquanto conectado via cabo de 3,5 mm"
     - **ru**: "При подключении кабелем 3,5 мм Bluetooth-аудио отключается"
     - **es**: "El audio por Bluetooth se desconecta mientras está conectado el cable de 3,5 mm"
     - **sv**: "Bluetooth-ljud slås av medan du är ansluten via 3,5 mm-kabeln"
     - **fi**: "Bluetooth-ääni kytketään pois käytöstä, kun kuulokkeet liitetään 3,5 mm:n johdolla"
     - **zh-Hans**: "通过 3.5 毫米线缆连接时，蓝牙音频会断开连接"
     - **zh-Hant**: "使用 3.5 公厘充電線做連接時，Bluetooth 音訊會停止連接"
     - **id**: "Audio Bluetooth terputus ketika disambungkan melalui kabel 3,5 mm"
     - **fil**: "Ang Bluetooth audio ay nakadiskonekta habang nakakonekta sa pamamagitan ng 3.5 mm na cable"
     - **ja**: "3.5mmケーブルが接続されている間は、Bluetoothオーディオ接続は解除されます"
 */
  public static func player_audio_source_aux_activated() -> String {
     return localizedString(
         key:"player_audio_source_aux_activated",
         defaultValue:"Bluetooth audio is disconnected while connected via 3.5 mm cable",
         substitutions: [:])
  }

 /**
"BLUETOOTH"

     - **en**: "BLUETOOTH"
     - **da**: "BLUETOOTH"
     - **nl**: "BLUETOOTH"
     - **de**: "BLUETOOTH"
     - **fr**: "BLUETOOTH"
     - **it-IT**: "BLUETOOTH"
     - **nb**: "BLUETOOTH"
     - **pt-PT**: "BLUETOOTH"
     - **ru**: "BLUETOOTH"
     - **es**: "BLUETOOTH"
     - **sv**: "BLUETOOTH"
     - **fi**: "BLUETOOTH"
     - **zh-Hans**: "蓝牙"
     - **zh-Hant**: "藍牙"
     - **id**: "BLUETOOTH"
     - **fil**: "BLUETOOTH"
     - **ja**: "BLUETOOTH"
 */
  public static func player_audio_source_bluetooth() -> String {
     return localizedString(
         key:"player_audio_source_bluetooth",
         defaultValue:"BLUETOOTH",
         substitutions: [:])
  }

 /**
"RCA"

     - **en**: "RCA"
     - **da**: "RCA"
     - **nl**: "RCA"
     - **de**: "RCA"
     - **fr**: "RCA"
     - **it-IT**: "RCA"
     - **nb**: "RCA"
     - **pt-PT**: "RCA"
     - **ru**: "RCA"
     - **es**: "RCA"
     - **sv**: "RCA"
     - **fi**: "RCA"
     - **zh-Hans**: "RCA 接口"
     - **zh-Hant**: "RCA"
     - **id**: "RCA"
     - **fil**: "RCA"
     - **ja**: "RCA"
 */
  public static func player_audio_source_rca() -> String {
     return localizedString(
         key:"player_audio_source_rca",
         defaultValue:"RCA",
         substitutions: [:])
  }

 /**
"SOURCES"

     - **en**: "SOURCES"
     - **da**: "KILDER"
     - **nl**: "BRONNEN"
     - **de**: "QUELLEN"
     - **fr**: "SOURCES"
     - **it-IT**: "FONTI"
     - **nb**: "KILDER"
     - **pt-PT**: "FONTES"
     - **ru**: "ИСТОЧНИКИ"
     - **es**: "FUENTES"
     - **sv**: "KÄLLOR"
     - **fi**: "LÄHTEET"
     - **zh-Hans**: "来源"
     - **zh-Hant**: "音源"
     - **id**: "SUMBER"
     - **fil**: "MGA SANGGUNIAN"
     - **ja**: "音源"
 */
  public static func player_audio_sources() -> String {
     return localizedString(
         key:"player_audio_sources",
         defaultValue:"SOURCES",
         substitutions: [:])
  }

 /**
"PLAYER"

     - **en**: "PLAYER"
     - **da**: "AFSPILLER"
     - **nl**: "SPELER"
     - **de**: "PLAYER"
     - **fr**: "LECTEUR"
     - **it-IT**: "LETTORE"
     - **nb**: "AVSPILLER"
     - **pt-PT**: "LEITOR"
     - **ru**: "ПЛЕЕР"
     - **es**: "REPRODUCTOR"
     - **sv**: "SPELARE"
     - **fi**: "SOITIN"
     - **zh-Hans**: "播放器"
     - **zh-Hant**: "播放器"
     - **id**: "PLAYER"
     - **fil**: "PLAYER"
     - **ja**: "プレイヤー"
 */
  public static func player_audio_uc() -> String {
     return localizedString(
         key:"player_audio_uc",
         defaultValue:"PLAYER",
         substitutions: [:])
  }

 /**
"Select Bluetooth as source by pushing the source button to toggle input sources."

     - **en**: "Select Bluetooth as source by pushing the source button to toggle input sources."
     - **da**: "Vælg Bluetooth som lydkilde ved at trykke på source-knappen for at skifte lydkilde."
     - **nl**: "Selecteer Bluetooth als bron door op de bronknop te drukken om tussen de invoerbronnen te schakelen."
     - **de**: "Wähle Bluetooth als Eingangsquelle durch Drücken der Quellenwahltaste."
     - **fr**: "Sélectionnez le Bluetooth comme source en appuyant sur le bouton Source pour naviguer entre les sources d’entrée."
     - **it-IT**: "Premi il tasto Source, facendo scorrere le sorgenti di ingresso per selezionare l’opzione Bluetooth."
     - **nb**: "Velg Bluetooth som kilde ved å trykke på kildeknappen for å skifte inngangskilder."
     - **pt-PT**: "Selecione bluetooth como fonte premindo o botão fonte para alternar entre fontes de entrada."
     - **ru**: "Выберите Bluetooth как источник, нажав кнопку «Источник» для переключения между источниками  звука."
     - **es**: "Para seleccionar la entrada de Bluetooth, pulsa el botón de fuente a fin de alternar entre las diferentes fuentes de sonido."
     - **sv**: "Markera Bluetooth som ingång genom att trycka på source-knappen för att växla mellan ingångskällorna."
     - **fi**: "Valitse lähteeksi Bluetooth painamalla Lähde-painiketta ja vaihtelemalla tulolähteitä."
     - **zh-Hans**: "轻按音源按钮，切换输入源，将蓝牙选为音源。"
     - **zh-Hant**: "按下音源按鈕以切換輸入源，選擇藍牙做為音源。"
     - **id**: "Pilih Bluetooth sebagai sumber dengan menekan tombol sumber untuk berpindah sumber input."
     - **fil**: "Piliin ang Bluetooth bilang mapagkukunan sa pamamagitan ng pagpindot sa button na pinagmulan upang makakapili ng mga input source."
     - **ja**: "Sourceボタンを押して入力源を切り替えることで、Bluetoothを入力源に選択します。"
 */
  public static func quick_guide_bluetooth_pairing_part1() -> String {
     return localizedString(
         key:"quick_guide_bluetooth_pairing_part1",
         defaultValue:"Select Bluetooth as source by pushing the source button to toggle input sources.",
         substitutions: [:])
  }

 /**
"Push and hold the Source button for 2 seconds.\nThe Bluetooth indicator starts blinking."

     - **en**: "Push and hold the Source button for 2 seconds.\nThe Bluetooth indicator starts blinking."
     - **da**: "Tryk og hold Source-knappen i to sekunder.\nBluetooth-indikatoren begynder at blinke."
     - **nl**: "Druk de Source-knop in en houd deze 2 seconden vast.\nDe Bluetooth-indicator begint te knipperen."
     - **de**: "Halte die Source-Taste 2 Sekunden lang gedrückt.\nDie Bluetooth-Anzeige beginnt zu blinken."
     - **fr**: "Appuyez et maintenez le bouton Source pendant 2 secondes.\nLe voyant Bluetooth se met à clignoter."
     - **it-IT**: "Tieni premuto il tasto Source per 2 secondi.\nL’indicatore Bluetooth comincia a lampeggiare."
     - **nb**: "Trykk og hold kildeknappen inne i 2 sekunder.\nBluetooth-lampen begynner å blinke."
     - **pt-PT**: "Mantenha premido o botão Fonte durante 2 segundos.\nA luz indicadora de Bluetooth começa a piscar."
     - **ru**: "Нажмите и удерживайте кнопку «Источник»  в течение 2 секунд.\nИндикатор Bluetooth начнет мигать."
     - **es**: "Mantén presionado el botón SOURCE durante 2 segundos.\nEl indicador de Bluetooth comenzará a parpadear."
     - **sv**: "Tryck på och håll SOURCE-knappen i två sekunder.\nBluetooth-indikatorn börjar blinka."
     - **fi**: "Paina Lähde-painiketta ja pidä sitä alhaalla kaksi sekuntia.\nBluetooth-merkkivalo alkaa välkkyä."
     - **zh-Hans**: "按住音源按钮 2 秒。\n蓝牙指示灯开始闪烁。"
     - **zh-Hant**: "按住音源按鈕 2 秒鐘。\n藍牙指示燈會開始閃爍。"
     - **id**: "Tekan dan tahan tombol Sumber selama 2 detik.\nIndikator Bluetooth mulai berkedip."
     - **fil**: "Pindutin at i-hold ang button na Source ng 2 segundo.\nMagsisimulang kumislap ang Bluetooth indicator."
     - **ja**: "Sourceボタンを2秒間長押しします。\nBluetoothのインジケーターが点滅をはじめます。"
 */
  public static func quick_guide_bluetooth_pairing_part2() -> String {
     return localizedString(
         key:"quick_guide_bluetooth_pairing_part2",
         defaultValue:"Push and hold the Source button for 2 seconds.\nThe Bluetooth indicator starts blinking.",
         substitutions: [:])
  }

 /**
"Enable Bluetooth on your phone, tablet or computer."

     - **en**: "Enable Bluetooth on your phone, tablet or computer."
     - **da**: "Slå Bluetooth til på din smartphone, tablet eller computer."
     - **nl**: "Schakel Bluetooth in op je telefoon, tablet of computer."
     - **de**: "Aktiviere Bluetooth auf deinem Smartphone, Tablet oder Computer."
     - **fr**: "Activez le Bluetooth sur votre smartphone, tablette ou ordinateur."
     - **it-IT**: "Attiva il Bluetooth sul tuo smartphone, tablet o computer."
     - **nb**: "Aktiver Bluetooth på telefonen, nettbrettet eller datamaskinen."
     - **pt-PT**: "Ative o Bluetooth no seu smartphone, tablet ou computador."
     - **ru**: "Включите Bluetooth на своем телефоне, планшете или компьютере."
     - **es**: "Activa el Bluetooth en tu teléfono, tableta u ordenador."
     - **sv**: "Aktivera Bluetooth på din smarttelefon, surfplatta eller dator."
     - **fi**: "Laita älypuhelimesi, tablettisi tai tietokoneesi Bluetooth-yhteys päälle."
     - **zh-Hans**: "启用手机、平板电脑或电脑上的蓝牙。"
     - **zh-Hant**: "啟用手機、平板電腦或電腦上的藍牙。"
     - **id**: "Aktifkan Bluetooth pada ponsel pintar, tablet, atau komputer Anda."
     - **fil**: "I-enable ang Bluetooth sa iyong telepono, tablet o computer."
     - **ja**: "ご使用のスマートフォン、タブレット、コンピューターでBluetoothをオンにします。"
 */
  public static func quick_guide_bluetooth_pairing_part3() -> String {
     return localizedString(
         key:"quick_guide_bluetooth_pairing_part3",
         defaultValue:"Enable Bluetooth on your phone, tablet or computer.",
         substitutions: [:])
  }

 /**
"Select the speaker from the list of available devices.\nThe indicator turns steady when pairing is completed."

     - **en**: "Select the speaker from the list of available devices.\nThe indicator turns steady when pairing is completed."
     - **da**: "Vælg højttaleren på listen over tilgængelige enheder.\nIndikatoren lyser kontinuerligt, når parringen er færdig."
     - **nl**: "Update"
     - **de**: "Wähle deinen Lautsprecher aus der Liste der verfügbaren Geräte.\nDie Anzeige leuchtet dauerhaft, wenn die Kopplung erfolgt ist."
     - **fr**: "Sélectionnez l’enceinte dans la liste des appareils disponibles.\nLe voyant reste fixe lorsque l’association est effectuée."
     - **it-IT**: "Seleziona il diffusore dall’elenco dei dispositivi disponibili.\nL’indicatore diventa fisso ad abbinamento avvenuto."
     - **nb**: "Velg høyttaleren fra listen over tilgjengelige enheter.\nLampen blir stabil når paringen er fullført."
     - **pt-PT**: "Selecione o altifalante na lista de dispositivos disponíveis.\nO indicador desligar-se-á após a conclusão do emparelhamento."
     - **ru**: "Выберите акустическую систему из списка доступных устройств.\nПо завершении сопряжения индикатор будет гореть постоянно."
     - **es**: "Selecciona tu altavoz de la lista de dispositivos disponibles.\nUna vez emparejado, el indicador se queda encendido sin parpadear."
     - **sv**: "Markera högtalaren i listan över tillgängliga enheter.\nIndikatorn slutar blinka när parningen är klar."
     - **fi**: "Valitse kaiutin käytettävissä olevien laitteiden listalta.\nMerkkivalo lakkaa välkkymästä, kun pariliitos on muodostettu."
     - **zh-Hans**: "从可用的设备列表中选择音箱。\n当配对结束时，指示灯停止闪烁"
     - **zh-Hant**: "從可用裝置清單中選擇喇叭。\n當配對完成時，指示燈會變成靜止不動。"
     - **id**: "Pilih speaker dari daftar perangkat yang tersedia.\nIndikator menjadi stabil saat pemasangan selesai."
     - **fil**: "Piliin ang speaker mula sa listahan ng mga magagamit na device.\nMagiging steady ang indicator kapag natapos na ang pagpapares."
     - **ja**: "利用可能なデバイスのリストからスピーカ―を選択します。\nペアリングが完了するとインジケーターが点灯したままになります。"
 */
  public static func quick_guide_bluetooth_pairing_part4() -> String {
     return localizedString(
         key:"quick_guide_bluetooth_pairing_part4",
         defaultValue:"Select the speaker from the list of available devices.\nThe indicator turns steady when pairing is completed.",
         substitutions: [:])
  }

 /**
"Two speakers play the same audio synchronised. Each speaker plays both the left and right channels. Choose this mode if your speakers are placed at different heights, direction or areas of the room."

     - **en**: "Two speakers play the same audio synchronised. Each speaker plays both the left and right channels. Choose this mode if your speakers are placed at different heights, direction or areas of the room."
     - **da**: "To højttalere afspiller samme lyd synkroniseret. Begge højttalere afspiller både venstre og højre kanal. Vælg denne tilstand, hvis dine højttalere er placeret i forskellig højde, peger i forskellig retning eller står forskellige steder i rummet."
     - **nl**: "Twee luidsprekers spelen dezelfde audio gesynchroniseerd af. Elke luidspreker speelt zowel het linker- als het rechterkanaal af. Kies deze modus als je luidsprekers op verschillende hoogtes, in verschillende richtingen of in verschillende delen van de kamer zijn geplaatst."
     - **de**: "Zwei Lautsprecher geben synchron denselben Ton wieder. Jeder Lautsprecher gibt sowohl den linken als auch den rechten Kanal wieder. Wähle diesen Modus, wenn deine Lautsprecher auf unterschiedlichen Höhen, in verschiedener Richtung oder in unterschiedlichen Bereichen des Raumes angeordnet sind."
     - **fr**: "Deux enceintes lisent la même piste audio de manière synchronisée. Choisissez ce mode si vos enceintes sont positionnées à différentes hauteurs, orientations ou dans différentes zones de la pièce."
     - **it-IT**: "I due diffusori riproducono simultaneamente lo stesso audio. Entrambi riproducono quindi sia il canale destro che quello sinistro. Scegli questa modalità se i diffusori sono posizionati in direzioni, zone o altezze della stanza diverse."
     - **nb**: "To høyttalere spiller den samme lyden synkronisert. Hver høyttaler spiller både venstre og høyre kanal. Velg denne modusen hvis høyttalerne dine er plassert i forskjellige høyder, retninger eller områder i rommet."
     - **pt-PT**: "Dois altifalantes reproduzem o mesmo audio sincronizados. Cada altifalante reproduz os canais esquerdo e direito. Escolha este modo se os altifalantes estiverem colocados a diferentes alturas, direções ou zonas da divisão."
     - **ru**: "Две колонки проигрывают один и тот же синхронизированный аудиоисточник. Обе колонки проигрывают левый и правый каналы. Выберите этот режим при установке колонок на разной высоте, в разных направлениях или в разных углах помещения."
     - **es**: "Dos altavoces reproducen el mismo sonido sincronizado. Cada altavoz reproduce los dos canales, el izquierdo y el derecho. Elige este modo de funcionamiento si los altavoces están instalados a alturas diferentes, en direcciones diferentes o en zonas diferentes de la sala."
     - **sv**: "Två högtalare spelar samma ljud synkroniserat. Varje högtalare spelar både vänster och höger kanal. Välj detta läge om dina högtalare är placerade på olika höjder, riktningar eller områden i rummet."
     - **fi**: "Kaksi kaiutinta soittaa samaa ääntä synkronoidusti. Kumpikin kaiutin käyttää sekä vasenta että oikeaa kanavaa. Valitse tämä tila, jos kaiuttimet on sijoitettu huoneessa eri korkeuksiin, suuntiin tai paikkoihin."
     - **zh-Hans**: "两台音箱同步播放相同的音频。每台音箱均在左右声道播放。如果您的音箱放在房间内的不同高度、方向或区域，请选择此模式。"
     - **zh-Hant**: "兩個喇叭同步播放相同的音訊，每個喇叭均播放左聲道和右聲道。如果您的喇叭放在房間內不同的高度、方向或區域，請選擇此模式。"
     - **id**: "Dua speaker memutar audio yang sama melalui sinkronisasi. Setiap speaker memutar saluran kiri dan kanan. Pilih mode ini jika speaker Anda ditempatkan pada ketinggian, arah, atau area ruangan yang berbeda."
     - **fil**: "Dalawang speaker ang nagpapatugtog ng parehong audio na naka-synchronise. Ang bawat speaker ay nagpapatugtog ng kaliwa at kanang channel. Piliin ang mode na ito kung ang iyong mga speaker ay nasa magkakaibang taas, direksiyon o lugar sa kuwarto."
     - **ja**: "2つのスピーカーが同じオーディオをシンクロナイズさせて再生します。各スピーカーは左と右、両方のチャンネルから再生します。ご使用のスピーカーが違う高さに設置されていたり違う方向を向いている、あるいは部屋の中の別のエリアにある場合にこのモードを選択してください。"
 */
  public static func quick_guide_couple_ambient_subtitle() -> String {
     return localizedString(
         key:"quick_guide_couple_ambient_subtitle",
         defaultValue:"Two speakers play the same audio synchronised. Each speaker plays both the left and right channels. Choose this mode if your speakers are placed at different heights, direction or areas of the room.",
         substitutions: [:])
  }

 /**
"AMBIENT MODE"

     - **en**: "AMBIENT MODE"
     - **da**: "OMSLUTTENDE TILSTAND"
     - **nl**: "AMBIENT-MODUS"
     - **de**: "UMGEBUNGSMODUS"
     - **fr**: "MODE AMBIENT"
     - **it-IT**: "MODALITÀ AMBIENTE"
     - **nb**: "OMGIVELSESMODUS"
     - **pt-PT**: "MODO AMBIENTE"
     - **ru**: "ПРОСТРАНСТВЕННЫЙ РЕЖИМ"
     - **es**: "MODO AMBIENTE"
     - **sv**: "AMBIENT-LÄGE"
     - **fi**: "AMBIENT-TILA"
     - **zh-Hans**: "环境模式"
     - **zh-Hant**: "環境模式"
     - **id**: "MODE AMBIENT"
     - **fil**: "AMBIENT MODE"
     - **ja**: "AMBIENT MODE"
 */
  public static func quick_guide_couple_ambient_title_uc() -> String {
     return localizedString(
         key:"quick_guide_couple_ambient_title_uc",
         defaultValue:"AMBIENT MODE",
         substitutions: [:])
  }

 /**
"Two speakers of the same size are coupled as a stereo pair. With one speaker the left channel and the other the right. Choose this mode if the speakers are placed at the same height, equal distance and facing the same direction."

     - **en**: "Two speakers of the same size are coupled as a stereo pair. With one speaker the left channel and the other the right. Choose this mode if the speakers are placed at the same height, equal distance and facing the same direction."
     - **da**: "To højttalere af samme størrelse forbindes som et stereopar. Den ene højttaler afspiller venstre kanal, den anden afspiller højre. Vælg denne tilstand, hvis højttalerne er placeret i samme højde, lige langt fra afspilleren og peger i samme retning."
     - **nl**: "Twee luidsprekers van dezelfde grootte zijn gekoppeld als een stereopaar. Met één luidspreker als het linkerkanaal en de andere als het rechterkanaal. Kies deze modus als de luidsprekers zich op dezelfde hoogte, op dezelfde afstand en in dezelfde richting bevinden."
     - **de**: "Zwei gleich große Lautsprecher werden als Stereopaar gekoppelt. Dabei gibt ein Lautsprecher den linken und der andere den rechten Kanal wieder. Wähle diesen Modus, wenn die Lautsprecher auf derselben Höhe und in gleichem Abstand angeordnet sind und in dieselbe Richtung weisen."
     - **fr**: "Deux enceintes de la même taille sont associées en tant que paire stéréo avec une enceinte comme canal droit et l’autre comme canal gauche. Choisissez ce mode si les enceintes sont placées à la même hauteur, distance et dirigées dans la même direction."
     - **it-IT**: "Due diffusori delle stesse dimensioni sono associati come una coppia di diffusori stereo. Uno riproduce il canale sinistro, l’altro il canale destro. Scegli questa modalità se i diffusori sono posizionati alla stessa altezza, a pari distanza e nella stessa direzione."
     - **nb**: "To høyttalere av samme størrelse kobles som et stereopar, hvorav en er venstrekanal og den andre høyre. Velg denne modusen hvis høyttalerne er plassert i samme høyde, lik avstand og i samme retning."
     - **pt-PT**: "Dois altifalantes do mesmo tamanho são emparelhados como um par estéreo. Com um altifalante o canal esquerdo e com o outro o canal direito. Escolha este modo se os altifalantes estiverem colocados à mesma altura, à mesma distância e voltados na mesma direção."
     - **ru**: "Две колонки оного размера объединены в стереопару: одна проигрывает левый, а другая правый канал. Выберите этот режим при размещении колонок на одной высоте, на равном расстоянии и в одном направлении."
     - **es**: "Dos altavoces del mismo tamaño se acoplan como un par de altavoces en estéreo, es decir, uno reproduce el canal izquierdo y el otro el derecho. Elige este modo si los altavoces están instalados a la misma altura, a la misma distancia y orientados en la misma dirección."
     - **sv**: "Två högtalare av samma storlek parkopplas som ett stereopar. Med en högtalare som vänster kanal och den andra som höger. Välj detta läge om högtalarna är placerade i samma höjd, samma avstånd och i samma riktning."
     - **fi**: "Kaksi samankokoista kaiutinta muodostaa stereolaiteparin. Toinen kaiutin käyttää vasenta kanavaa ja toinen oikeaa. Valitse tämä tila, jos kaiuttimet on sijoitettu samalle korkeudelle, saman etäisyyden päähän ja samansuuntaisesti."
     - **zh-Hans**: "相同尺寸的两台音箱配对为一对立体声音箱。一台音箱为左声道，另一台为右声道。如果扬声器放置在相同的高度、相等的距离且面朝相同的方向，请选择此模式。"
     - **zh-Hant**: "兩個相同大小的喇叭互相連接當做一組立體聲音響，一個喇叭是左聲道，另一個喇叭是右聲道。如果喇叭放在相同的高度、等距且面對同樣的方向，請選擇此模式。"
     - **id**: "Dua speaker sebagai pasangan stereo, dengan satu speaker sebagai saluran kiri dan yang lain sebagai saluran kanan. Pilih mode ini jika speaker ditempatkan pada ketinggian yang sama, jarak yang sama, dan menghadap ke arah yang sama."
     - **fil**: "Dalawang speaker na pareho ang laki ay ipapares bilang stereo pair. Ang isang speaker sa kaliwa at ang isa sa kanan. Piliin ang mode na ito kung ang mga speaker ay pareho ang taas, patas ang layo at nakaharap sa parehong direksyon."
     - **ja**: "同じサイズの2つのスピーカーが、ステレオペアとしてカップリングされます。一つのスピーカーが左チャンネルを担い、もう一つが右チャンネルを担います。ご使用のスピーカーが同じ高さ、同じ距離、そして同じ方向を向いて設置されている場合にこのモードを選択してください。"
 */
  public static func quick_guide_couple_stereo_subtitle() -> String {
     return localizedString(
         key:"quick_guide_couple_stereo_subtitle",
         defaultValue:"Two speakers of the same size are coupled as a stereo pair. With one speaker the left channel and the other the right. Choose this mode if the speakers are placed at the same height, equal distance and facing the same direction.",
         substitutions: [:])
  }

 /**
"STEREO MODE"

     - **en**: "STEREO MODE"
     - **da**: "STEREOTILSTAND"
     - **nl**: "STEREO-MODUS"
     - **de**: "STEREOMODUS"
     - **fr**: "MODE STÉRÉO"
     - **it-IT**: "MODALITÀ STEREO"
     - **nb**: "STEREOMODUS"
     - **pt-PT**: "MODO ESTÉREO"
     - **ru**: "РЕЖИМ СТЕРЕО"
     - **es**: "MODO ESTÉREO"
     - **sv**: "STEREO-LÄGE"
     - **fi**: "STEREO-TILA"
     - **zh-Hans**: "立体声模式"
     - **zh-Hant**: "立體聲模式"
     - **id**: "MODE STEREO"
     - **fil**: "STEREO MODE"
     - **ja**: "STEREO MODE"
 */
  public static func quick_guide_couple_stereo_title_uc() -> String {
     return localizedString(
         key:"quick_guide_couple_stereo_title_uc",
         defaultValue:"STEREO MODE",
         substitutions: [:])
  }

 /**
"You can couple two Acton II, Stanmore II or Woburn II to act as one sound system. There are two coupling modes, ambient and stereo mode."

     - **en**: "You can couple two Acton II, Stanmore II or Woburn II to act as one sound system. There are two coupling modes, ambient and stereo mode."
     - **da**: "Du kan parre to Acton II, Stanmore II eller Woburn II, så de fungerer som ét lydsystem. Det kan gøres med to indstillinger, omsluttende tilstand og stereotilstand."
     - **nl**: "Je kunt twee Acton II, Stanmore II of Woburn II aan elkaar koppelen zodat ze als één geluidssysteem functioneren. Er zijn twee koppelingsmodi, de ambient- en stereo-modus."
     - **de**: "Du kannst zwei Acton II, Stanmore II oder Woburn II koppeln, die als eigenständiges Soundsystem agieren. Es gibt zwei Kopplungsmodi, Umgebungsmodus und Stereomodus."
     - **fr**: "Vous pouvez coupler deux Acton II, Stanmore II ou Woburn II pour les faire fonctionner comme un seul système audio. Il existe deux modes d’association, Ambient et Stéréo."
     - **it-IT**: "Puoi associare due diffusori Acton II, Stanmore II o Woburn II affinché funzionino come un unico sistema audio. Ci sono due possibili modalità di associazione: ambiente e stereo."
     - **nb**: "Du kan pare to Acton II, Stanmore II eller Woburn II til å fungere som ett lydsystem. Det er to paringsmoduser, omgivelses- og stereomodus."
     - **pt-PT**: "Pode emparelhar dois Acton II, Stanmore II ou Woburn II para funcionar como um sistema de som. Existem dois modos de emparelhamento, modo ambiente e estéreo."
     - **ru**: "Можно объединить две колонки Acton II, Stanmore II или Woburn II в одну акустическую систему. Существует два режима объединения: пространственный и стерео."
     - **es**: "Puedes acoplar dos Acton II, Stanmore II o Woburn II para que actúen como un único sistema de sonido. Existen dos tipos de acoplamiento: ambiente y estéreo."
     - **sv**: "Du kan parkoppla två Acton II, Stanmore II eller Woburn II för att få dem att fungera som ett ljudsystem. Det finns två olika parkopplingslägen, Ambient- och Stereo-läge."
     - **fi**: "Voit muodostaa pariliitoksen kahden Acton II:n, Stanmore II:n tai Woburn II:n välille, jolloin ne toimivat yhtenäisenä äänijärjestelmänä. Valitse kahden pariliitostilan välillä: ambient- ja stereo-tila."
     - **zh-Hans**: "你可以将两个Acton II、Stanmore II或Woburn II 同型号音箱配对为一个音响系统，共有两种配对模式：\n环境模式和立体声模式。"
     - **zh-Hant**: "您可以將兩個 Acton II、Stanmore II 或 Woburn II 配對以當做一個音響系統。配對模式有兩種，分別是環境和立體聲模式。"
     - **id**: "Anda dapat memasangkan dua Acton II, Stanmore II, atau Woburn II yang berfungsi sebagai satu sistem suara. Terdapat dua mode pemasangan, yakni mode ambient dan stereo."
     - **fil**: "Maaari kang magpares ng dalawang Acton II, Stanmore II o Woburn II para maging isang sound system. May dalawang mode sa pagpapares, ang ambient at ang stereo mode."
     - **ja**: "2つのActon II、Stanmore IIまたはWoburn IIをカップリングして、一つのサウンドシステムとして機能させることができます。2つのCoupling Modeと、Ambient ModeおよびStereo Modeがあります。"
 */
  public static func quick_guide_couple_subtitle() -> String {
     return localizedString(
         key:"quick_guide_couple_subtitle",
         defaultValue:"You can couple two Acton II, Stanmore II or Woburn II to act as one sound system. There are two coupling modes, ambient and stereo mode.",
         substitutions: [:])
  }

 /**
"The ANC function can be set to block out the outside noise or to amplify your surroundings. You can adjust the level of Noise Cancelling or Monitoring from the ANC settings menu.\n\nThe ANC Button on the left earcap lets you:\n\n•   Single click to toggle between Noise Cancelling and Monitoring.\n\n•   Click and hold for 2 seconds to toggle ANC off/on."

     - **en**: "The ANC function can be set to block out the outside noise or to amplify your surroundings. You can adjust the level of Noise Cancelling or Monitoring from the ANC settings menu.\n\nThe ANC Button on the left earcap lets you:\n\n•   Single click to toggle between Noise Cancelling and Monitoring.\n\n•   Click and hold for 2 seconds to toggle ANC off/on."
     - **da**: "ANC-funktionen kan indstilles til at reducere omgivelsesstøj eller for at forstærke dine omgivelser. Du kan justere niveauet for Noise Cancelling eller Monitoring fra menuen for ANC-indstillinger.\n\nANC-knappen på venstre øremuffe giver dig mulighed for:\n\n•  At skifte mellem Noise Cancelling and Monitoring med et enkelt tryk.\n\n•   Klik og hold i 2 sekunder for at slå ANC til/fra."
     - **nl**: "De ANC-functie kan worden ingesteld om het omgevingsgeluid te blokkeren of om je omgeving te versterken. Je kunt het niveau van de Ruisonderdrukking of Monitoring aanpassen via het ANC-instellingenmenu.\n\nMet de ANC-knop op de linkeroordop kun je:\n\n•   Met een enkele klik wisselen tussen Ruisonderdrukking en Monitoring.\n\n•   Klik en houd 2 seconden ingedrukt om ANC uit/in te schakelen."
     - **de**: "Die ANC-Funktion kann so eingestellt werden, dass sie Umgebungsgeräusche blockiert oder auch verstärkt. Du kannst das Niveau der Geräuschunterdrückung oder der Überwachung über das ANC-Einstellungsmenü ändern.\n\nDie ANC-Taste auf der linken Ohrmuschel kannst du:\n\n• einmal drücken, um zwischen Geräuschunterdrückung und Überwachungsmodus zu wechseln.\n\n• 2 Sekunden gedrückt halten, um ANC ein- oder auszuschalten."
     - **fr**: "La fonction ANC peut être configurée pour bloquer les bruits extérieurs ou pour amplifier votre environnement sonore. Vous pouvez ajuster les niveaux de Contrôle du bruit et de Monitoring à partir du menu paramètres ANC.\n\nLe bouton ANC situé sur le pavillon d’écouteur gauche vous permet :\n\n•   de basculer entre les modes Contrôle de bruit et Monitoring en un simple clic.\n\n•   d’allumer ou d’éteindre la fonction ANC en le maintenant enfoncé pendant 2 secondes."
     - **it-IT**: "La funzione ANC può essere impostata per eliminare i rumori esterni o sentire meglio l’ambiente circostante. Puoi regolare i livelli di cancellazione del rumore o monitoraggio dal menu delle impostazioni ANC.\n\nIl pulsante ANC sul padiglione sinistro ti permette di:\n\n•   Premere una volta per passare tra cancellazione del rumore e monitoraggio.\n\n•   Tenere premuto per 2 secondi per accendere o spegnere l’ANC."
     - **nb**: "ANC-funksjonen kan angis til å blokkere utvendig støy eller til å forsterke omgivelsene. Du kan justere støydempingen eller overvåkingen fra innstillingsmenyen for ANC.\n\nMed ANC-knappen på venstre ørekopp kan du:\n\n•   Enkeltklikk for å bytte mellom støydemping og overvåking.\n\n•   Klikk og hold i 2 sekunder for å slå ANC av/på."
     - **pt-PT**: "A função ANC pode ser definida para bloquear o ruído exterior ou para ampliar o espaço à sua volta. Pode ajustar o nível de cancelamento ou de controlo de ruído no menu de definições do ANC.\n\nO botão ANC no auscultador esquerdo permite:\n\n•   Clicar uma vez para alternar entre o cancelamento e o controlo de ruído.\n\n•   Clicar e manter o botão pressionado durante 2 segundos para ativar/desativar o ANC."
     - **ru**: "Функция ANC может использоваться как для ограничения внешнего шума, так и для усиления окружающих звуков. Уровень шумоподавления и мониторинга регулируется в настройках ANC, \n\n Кнопка ANC на левой чашке наушников позволяет: \n\n• При одном нажатии переключать между режимом шумоподавления и мониторинга.\n\n• Нажмите и удерживайте 2 секунда для включения/выключения ANC. "
     - **es**: "La función ANC se puede fijar para que bloquee el ruido exterior o para que amplifique lo que ocurre a tu alrededor. Puedes ajustar el nivel de cancelación de ruido o de monitorización en el menú de ajustes de ANC.\n\nEl botón de ANC en la carcasa izquierda te permite:\n\n•   Un solo clic para cambiar entre cancelación de ruido y monitorización.\n\n•   Clic y mantener 2 segundos para activar o desactivar ANC."
     - **sv**: "Brusreduceringsfunktionen kan ställas in för att stänga ut ljud från omvärlden eller förstärka din omgivning. Du kan justera nivåer för aktiv brusreducering och Monitoring Mode i menyn för aktiv brusreducering.\n\nBrusreduceringsknappen på den vänstra öronkåpan låter dig:\n\n•   Enkelklicka för att växla mellan aktiv brusreducering och Monitoring Mode.\n\n•   Trycka och hålla ner i 2 sekunder för att slå på/av aktiv brusreducering."
     - **fi**: "ANC-toiminnolla voidaan sulkea ulkomaailman melu pois tai ympäristön ääniä voimistaa. Voit säätää vastamelun tai tarkkailun tasoa ANC-asetusvalikosta.\n\nVasemmanpuoleisen korvakupin kyljessä olevassa ANC-painikkeessa on seuraavat toiminnot:\n\n•   Yksi napsautus vaihtaa vastamelun ja tarkkailun välillä.\n\n•   Painikkeen pitäminen alaspainettuna 2 sekunnin ajan vaihtaa vastamelun pois käytöstä / käyttöön."
     - **zh-Hans**: "可以设置 ANC 功能来屏蔽外部噪音或放大环境声。您可以在 ANC 设置菜单中调整噪声消除或监听级别。\n\n左耳罩上的 ANC 按钮功能：\n\n•   单击可在噪声消除和监听之间切换。\n\n•   单击并按住 2 秒钟可打开/关闭 ANC。"
     - **zh-Hant**: "可以設置 ANC 功能來隔絕外界噪音或放大周圍環境的聲音。您可以從 ANC 設置選單中調整降噪或監控的級別。\n\n 左耳帽上的 ANC 按鈕可讓您：\n\n• 單擊以在降噪和監控之間切換。\n\n• 單擊並按住 2 秒鐘以打開/關閉 ANC。"
     - **id**: "Fungsi ANC dapat diatur untuk memblokir suara dari luar atau menguatkan suara di sekitar Anda. Anda dapat mengatur tingkat Noise Cancelling atau Monitoring dari menu pengaturan ANC.\n\nTombol ANC di earcap kiri dapat digunakan untuk:\n\n*   Klik sekali untuk beralih antara mode Peredam Kebisingan atau Monitoring.\n\n*   Klik dan tahan selama 2 detik untuk mengaktifkan/mematikan ANC."
     - **fil**: "Ang ANC function ay maaaring itakda upang harangan ang ingay sa labas o upang i-amplify ang iyong kapaligiran. Maaari mong i-adjust ang antas ng Noise Cancelling o Monitoring mula sa mga setting menu ng ANC.\n\nAng ANC Button sa kaliwang earcap ay hahayaan kang:\n\n•   Pindutin nang isahan upang mag-toggle sa pagitan ng Noise Cancelling at Monitoring.\n\n•   Pindutin nang 2 segundo upang mag-toggle sa ANC off/on."
     - **ja**: "ANC機能は、外部ノイズをブロックまたは周囲の音を増幅することができます。ノイズキャンセリングやモニタリングのレベルは、ANC設定メニューで調節することが可能です。●●左側のイヤーキャップにあるANCボタンは、以下の操作が可能です。●●•   1回クリック：ノイズキャンセリングとモニタリング間を切り替え。●●•   2秒間長押し：ANCのオン／オフ切り替え。"
 */
  public static func quick_guide_ozzy_anc_button() -> String {
     return localizedString(
         key:"quick_guide_ozzy_anc_button",
         defaultValue:"The ANC function can be set to block out the outside noise or to amplify your surroundings. You can adjust the level of Noise Cancelling or Monitoring from the ANC settings menu.\n\nThe ANC Button on the left earcap lets you:\n\n•   Single click to toggle between Noise Cancelling and Monitoring.\n\n•   Click and hold for 2 seconds to toggle ANC off/on.",
         substitutions: [:])
  }

 /**
"Single click to toggle between Noise Cancelling and Monitoring."

     - **en**: "Single click to toggle between Noise Cancelling and Monitoring."
     - **da**: "At skifte mellem Noise Cancelling and Monitoring med et enkelt tryk."
     - **nl**: "Met een enkele klik wisselen tussen Ruisonderdrukking en Monitoring."
     - **de**: "einmal drücken, um zwischen Geräuschunterdrückung und Überwachungsmodus zu wechseln."
     - **fr**: "de basculer entre les modes Contrôle de bruit et Monitoring en un simple clic."
     - **it-IT**: "Premere una volta per passare tra cancellazione del rumore e monitoraggio."
     - **nb**: "Enkeltklikk for å bytte mellom støydemping og overvåking."
     - **pt-PT**: "Clicar uma vez para alternar entre o cancelamento e o controlo de ruído."
     - **ru**: "При одном нажатии переключать между режимом шумоподавления и мониторинга."
     - **es**: "Un solo clic para cambiar entre cancelación de ruido y monitorización."
     - **sv**: "Enkelklicka för att växla mellan aktiv brusreducering och Monitoring Mode."
     - **fi**: "Yksi napsautus vaihtaa vastamelun ja tarkkailun välillä."
     - **zh-Hans**: "单击可在噪声消除和监听之间切换。"
     - **zh-Hant**: "單擊以在降噪和監控之間切換。"
     - **id**: "Klik sekali untuk beralih antara mode Peredam Kebisingan atau Monitoring."
     - **fil**: "Pindutin nang isahan upang mag-toggle sa pagitan ng Noise Cancelling at Monitoring."
     - **ja**: "1回クリック：ノイズキャンセリングとモニタリング間を切り替え。"
 */
  public static func quick_guide_ozzy_anc_button_bullet_1() -> String {
     return localizedString(
         key:"quick_guide_ozzy_anc_button_bullet_1",
         defaultValue:"Single click to toggle between Noise Cancelling and Monitoring.",
         substitutions: [:])
  }

 /**
"Click and hold for 2 seconds to toggle ANC off/on."

     - **en**: "Click and hold for 2 seconds to toggle ANC off/on."
     - **da**: "Klik og hold i 2 sekunder for at slå ANC til/fra."
     - **nl**: "Klik en houd 2 seconden ingedrukt om ANC uit/in te schakelen."
     - **de**: "2 Sekunden gedrückt halten, um ANC ein- oder auszuschalten."
     - **fr**: "d’allumer ou d’éteindre la fonction ANC en le maintenant enfoncé pendant 2 secondes."
     - **it-IT**: "Tenere premuto per 2 secondi per accendere o spegnere l’ANC."
     - **nb**: "Klikk og hold i 2 sekunder for å slå ANC av/på."
     - **pt-PT**: "Clicar e manter o botão pressionado durante 2 segundos para ativar/desativar o ANC."
     - **ru**: "Нажмите и удерживайте 2 секунда для включения/выключения ANC. "
     - **es**: "Clic y mantener 2 segundos para activar o desactivar ANC."
     - **sv**: "Trycka och hålla ner i 2 sekunder för att slå på/av aktiv brusreducering."
     - **fi**: "Painikkeen pitäminen alaspainettuna 2 sekunnin ajan vaihtaa vastamelun pois käytöstä / käyttöön."
     - **zh-Hans**: "单击并按住 2 秒钟可打开/关闭 ANC。"
     - **zh-Hant**: "單擊並按住 2 秒鐘以打開/關閉 ANC。"
     - **id**: "Klik dan tahan selama 2 detik untuk mengaktifkan/mematikan ANC."
     - **fil**: "Pindutin nang 2 segundo upang mag-toggle sa ANC off/on."
     - **ja**: "2秒間長押し：ANCのオン／オフ切り替え。"
 */
  public static func quick_guide_ozzy_anc_button_bullet_2() -> String {
     return localizedString(
         key:"quick_guide_ozzy_anc_button_bullet_2",
         defaultValue:"Click and hold for 2 seconds to toggle ANC off/on.",
         substitutions: [:])
  }

 /**
"The ANC function can be set to block out the outside noise or to amplify your surroundings. You can adjust the level of Noise Cancelling or Monitoring from the ANC settings menu.\n\nThe ANC Button on the left earcap lets you:"

     - **en**: "The ANC function can be set to block out the outside noise or to amplify your surroundings. You can adjust the level of Noise Cancelling or Monitoring from the ANC settings menu.\n\nThe ANC Button on the left earcap lets you:"
     - **da**: "ANC-funktionen kan indstilles til at reducere omgivelsesstøj eller for at forstærke dine omgivelser. Du kan justere niveauet for Noise Cancelling eller Monitoring fra menuen for ANC-indstillinger.\n\nANC-knappen på venstre øremuffe giver dig mulighed for:"
     - **nl**: "De ANC-functie kan worden ingesteld om het omgevingsgeluid te blokkeren of om je omgeving te versterken. Je kunt het niveau van de Ruisonderdrukking of Monitoring aanpassen via het ANC-instellingenmenu.\n\nMet de ANC-knop op de linkeroordop kun je:"
     - **de**: "Die ANC-Funktion kann so eingestellt werden, dass sie Umgebungsgeräusche blockiert oder auch verstärkt. Du kannst das Niveau der Geräuschunterdrückung oder der Überwachung über das ANC-Einstellungsmenü ändern.\n\nDie ANC-Taste auf der linken Ohrmuschel kannst du:"
     - **fr**: "La fonction ANC peut être configurée pour bloquer les bruits extérieurs ou pour amplifier votre environnement sonore. Vous pouvez ajuster les niveaux de Contrôle du bruit et de Monitoring à partir du menu paramètres ANC.\n\nLe bouton ANC situé sur le pavillon d’écouteur gauche vous permet :"
     - **it-IT**: "La funzione ANC può essere impostata per eliminare i rumori esterni o sentire meglio l’ambiente circostante. Puoi regolare i livelli di cancellazione del rumore o monitoraggio dal menu delle impostazioni ANC.\n\nIl pulsante ANC sul padiglione sinistro ti permette di:"
     - **nb**: "ANC-funksjonen kan angis til å blokkere utvendig støy eller til å forsterke omgivelsene. Du kan justere støydempingen eller overvåkingen fra innstillingsmenyen for ANC.\n\nMed ANC-knappen på venstre ørekopp kan du:"
     - **pt-PT**: "A função ANC pode ser definida para bloquear o ruído exterior ou para ampliar o espaço à sua volta. Pode ajustar o nível de cancelamento ou de controlo de ruído no menu de definições do ANC.\n\nO botão ANC no auscultador esquerdo permite:"
     - **ru**: "Функция ANC может использоваться как для ограничения внешнего шума, так и для усиления окружающих звуков. Уровень шумоподавления и мониторинга регулируется в настройках ANC, \n\n Кнопка ANC на левой чашке наушников позволяет: "
     - **es**: "La función ANC se puede fijar para que bloquee el ruido exterior o para que amplifique lo que ocurre a tu alrededor. Puedes ajustar el nivel de cancelación de ruido o de monitorización en el menú de ajustes de ANC.\n\nEl botón de ANC en la carcasa izquierda te permite:"
     - **sv**: "Brusreduceringsfunktionen kan ställas in för att stänga ut ljud från omvärlden eller förstärka din omgivning. Du kan justera nivåer för aktiv brusreducering och Monitoring Mode i menyn för aktiv brusreducering.\n\nBrusreduceringsknappen på den vänstra öronkåpan låter dig:"
     - **fi**: "ANC-toiminnolla voidaan sulkea ulkomaailman melu pois tai ympäristön ääniä voimistaa. Voit säätää vastamelun tai tarkkailun tasoa ANC-asetusvalikosta.\n\nVasemmanpuoleisen korvakupin kyljessä olevassa ANC-painikkeessa on seuraavat toiminnot:"
     - **zh-Hans**: "可以设置 ANC 功能来屏蔽外部噪音或放大环境声。您可以在 ANC 设置菜单中调整噪声消除或监听级别。\n\n左耳罩上的 ANC 按钮功能："
     - **zh-Hant**: "可以設置 ANC 功能來隔絕外界噪音或放大周圍環境的聲音。您可以從 ANC 設置選單中調整降噪或監控的級別。\n\n 左耳帽上的 ANC 按鈕可讓您："
     - **id**: "Fungsi ANC dapat diatur untuk memblokir suara dari luar atau menguatkan suara di sekitar Anda. Anda dapat mengatur tingkat Noise Cancelling atau Monitoring dari menu pengaturan ANC.\n\nTombol ANC di earcap kiri dapat digunakan untuk:"
     - **fil**: "Ang ANC function ay maaaring itakda upang harangan ang ingay sa labas o upang i-amplify ang iyong kapaligiran. Maaari mong i-adjust ang antas ng Noise Cancelling o Monitoring mula sa mga setting menu ng ANC.\n\nAng ANC Button sa kaliwang earcap ay hahayaan kang:"
     - **ja**: "ANC機能は、外部ノイズをブロックまたは周囲の音を増幅することができます。ノイズキャンセリングやモニタリングのレベルは、ANC設定メニューで調節することが可能です。●●左側のイヤーキャップにあるANCボタンは、以下の操作が可能です。"
 */
  public static func quick_guide_ozzy_anc_button_label() -> String {
     return localizedString(
         key:"quick_guide_ozzy_anc_button_label",
         defaultValue:"The ANC function can be set to block out the outside noise or to amplify your surroundings. You can adjust the level of Noise Cancelling or Monitoring from the ANC settings menu.\n\nThe ANC Button on the left earcap lets you:",
         substitutions: [:])
  }

 /**
"1   Start with your headphones turned off.\n\n2   Push and hold the control knob for 4 seconds until the LED indicator blinks blue.\n\n3   Select MONITOR II from your sound device’s Bluetooth® list.\nThe indicator turns off when pairing is complete.\n\n4   Press play and enjoy your Marshall  headphones."

     - **en**: "1   Start with your headphones turned off.\n\n2   Push and hold the control knob for 4 seconds until the LED indicator blinks blue.\n\n3   Select MONITOR II from your sound device’s Bluetooth® list.\nThe indicator turns off when pairing is complete.\n\n4   Press play and enjoy your Marshall  headphones."
     - **da**: "1   Start med dine hovedtelefoner slukket.\n\n2   Tryk på kontrolknappen og hold den nede i 4 sekunder, indtil LED-indikatoren blinker blåt.\n\n3   Vælg MONITOR II fra din lydenheds Bluetooth®-liste.\n Indikatoren slukkes, når parringen er afsluttet.\n\n4   Tryk på afspil og nyd dine Marshall-hovedtelefoner."
     - **nl**: "1   Begin met een uitgeschakelde hoofdtelefoon.\n\n2   Druk de bedieningsknop 4 seconden in totdat de led-indicator blauw knippert.\n\n3   Selecteer MONITOR II uit de Bluetooth®-lijst van je audioapparaat.\nDe indicator gaat uit als het koppelen is voltooid.\n\n4   Druk op play en geniet van je Marshall-hoofdtelefoon."
     - **de**: "1 Lass deinen Kopfhörer zunächst ausgeschaltet.\n\n2 Halte den Bedienknopf 4 Sekunden lang gedrückt, bis die LED-Anzeige blau blinkt.\n\n3 Wähle MONITOR II aus der Liste der Bluetooth®-Geräte.\nDie Anzeige schaltet sich aus, sobald die Kopplung erfolgt ist.\n\n4 Drücke auf Wiedergabe und genieße deinen Kopfhörer von Marshall."
     - **fr**: "1   Tout d’abord, votre casque doit être éteint.\n\n2   Maintenez le bouton de contrôle enfoncé pendant 4 secondes, jusqu’à ce que le témoin LED clignote en bleu.\n\n3   Sélectionnez MONITOR II dans votre liste d’appareils audio Bluetooth®.\nLe témoin s’éteint lorsque l’appairage est effectué.\n\n4   Appuyez sur Lecture et profitez de votre casque Marshall."
     - **it-IT**: "1   Inizia con le cuffie spente.\n\n2   Tieni premuta la manopola di controllo per 4 secondi fino a quando l’indicatore LED lampeggia blu.\n\n3   Seleziona MONITOR II dall’elenco dei tuoi dispositivi audio Bluetooth®.\nL’indicatore si spegne quando l’abbinamento è completo.\n\n4   Premi Play e goditi le tue Marshall headphones."
     - **nb**: "1   Start med hodetelefonene avslått.\n\n2   Trykk og hold kontrollknappen i 4 sekunder til LED-indikatoren blinker blått.\n\n3   Velg MONITOR II fra lydenhetens Bluetooth®-liste.\nIndikatoren slår seg av når paringen er ferdig.\n\n4   Trykk på play og nyt dine Marshall-hodetelefoner."
     - **pt-PT**: "1   Comece com os auscultadores desligados.\n\n2   Mantenha pressionado o botão de controlo durante 4 segundos até o indicador LED piscar a azul.\n\n3   Selecione MONITOR II na lista Bluetooth® do seu dispositivo de áudio.\nO indicador desliga-se quando o emparelhamento está completo.\n\n4   Pressione play e desfrute dos seus auscultadores Marshall."
     - **ru**: "1   Наушники должны быть выключены.\n\n2   Нажмите и удерживайте кнопку управления в течение 4 секунд, пока светодиодный индикатор не замигает синим.\n\n3   Выберите MONITOR II из списка Bluetooth® вашего устройства. \n После выполнения сопряжения индикатор погаснет.\n\n4   Запустите воспроизведение и наслаждайтесь звуком Marshall Headphones."
     - **es**: "1   Empieza con los auriculares apagados.\n\n2   Mantén pulsado el botón de control durante 4 segundos hasta que el indicador LED parpadee en azul.\n\n3   Selecciona MONITOR II en la lista de Bluetooth® de tu dispositivo de sonido.\nEl indicador se apaga cuando el emparejamiento ha terminado.\n\n4   Pulsa reproducir y disfruta de tus auriculares Marshall."
     - **sv**: "1   Börja med dina hörlurar avslagna.\n\n2   Tryck och håll ner kontrollknappen i 4 sekunder tills LED-indikatorn blinkar blått.\n\n3   Välj MONITOR II från din ljudenhets Bluetooth®-lista.\nIndikatorn slås av när parkopplingen är slutförd.\n\n4   Tryck play och njut av dina Marshallhörlurar."
     - **fi**: "1   Kytke kuulokkeesi aluksi pois käytöstä.\n\n2   Paina säätönuppia ja pidä sitä painettuna alas 4 sekunnin ajan, kunnes LED-merkkivalo vilkkuu sinisenä.\n\n3  Valitse audiolaitteesi Bluetooth®-luettelosta MONITOR II.\nMerkkivalo sammuu, kun pariliitos on muodostettu.\n\n4   Paina toistopainiketta ja nauti Marshall-kuulokkeistasi."
     - **zh-Hans**: "1   首先关闭耳机。\n\n2   按住控制旋钮 4 秒钟，直到 LED 指示灯闪烁蓝色。\n\n3   从音频设备的 Bluetooth® 列表中选择MONITOR II。\n配对完成时，指示灯会熄灭。\n\n4   按下播放，享受 Marshall Headphones 的精彩。"
     - **zh-Hant**: "1  從關閉耳機開始。\n\n 2  按住控制旋鈕 4 秒鐘，直到 LED 指示燈閃爍藍色。\n\n 3  從音訊設備的 Bluetooth® 列表中選擇 MONITOR II。 \n 配對完成後，指示燈熄滅。\n\n 4  按下播放享受您的 Marshall 耳機。"
     - **id**: "1   Mulai dengan headphone dalam keadaan mati.\n\n2   Tekan dan tahan knop kontrol selama 4 detik hingga lampu indikator LED berkedip biru.\n\n3   Pilih MONITOR II dari daftar perangkat audio Bluetooth® Anda.\nLampu indikator mati ketika pemasangan berhasil.\n\n4   Tekan tombol mulai dan gunakan headphone Marshall Anda."
     - **fil**: "1   Mag-umpisa nang naka-off ang iyong mga headphone.\n\n2   Pindutin nang matagagal ang control knob nang 4 na segundo hanggang sa magpatay-sindi ang LED na indicator nang asul.\n\n3   Piliin ang MONITOR II mula sa listahan ng iyong audio ng device Bluetooth®.\nMag-o-off ang indicator kapag nakumpleto na ang pagpapares.\n\n4   Pindutin ang play at tamasahin ang iyong Marshall na mga headphone."
     - **ja**: "1   ヘッドフォンの電源がオフの状態で始めます。●●2   LEDランプが青色に点滅するまで、コントロールノブを4秒間押し続けます。●●3   お使いのサウンドデバイスのBluetooth®リストから[MONITOR II]を選択します。●ペアリングが完了すると、インジケーターがオフになります。●●4   再生を押して、Marshall ヘッドフォンをお楽しみください。"
 */
  public static func quick_guide_ozzy_bluetooth_pairing() -> String {
     return localizedString(
         key:"quick_guide_ozzy_bluetooth_pairing",
         defaultValue:"1   Start with your headphones turned off.\n\n2   Push and hold the control knob for 4 seconds until the LED indicator blinks blue.\n\n3   Select MONITOR II from your sound device’s Bluetooth® list.\nThe indicator turns off when pairing is complete.\n\n4   Press play and enjoy your Marshall  headphones.",
         substitutions: [:])
  }

 /**
"Start with your headphones turned off."

     - **en**: "Start with your headphones turned off."
     - **da**: "Start med dine hovedtelefoner slukket."
     - **nl**: "Begin met een uitgeschakelde hoofdtelefoon."
     - **de**: "Lass deinen Kopfhörer zunächst ausgeschaltet."
     - **fr**: "Tout d’abord, votre casque doit être éteint."
     - **it-IT**: "Inizia con le cuffie spente."
     - **nb**: "Start med hodetelefonene avslått."
     - **pt-PT**: "Comece com os auscultadores desligados."
     - **ru**: "Наушники должны быть выключены."
     - **es**: "Empieza con los auriculares apagados."
     - **sv**: "Börja med dina hörlurar avslagna."
     - **fi**: "Kytke kuulokkeesi aluksi pois käytöstä."
     - **zh-Hans**: "首先关闭耳机。"
     - **zh-Hant**: "從關閉耳機開始。"
     - **id**: "Mulai dengan headphone dalam keadaan mati."
     - **fil**: "Mag-umpisa nang naka-off ang iyong mga headphone."
     - **ja**: "ヘッドフォンの電源がオフの状態で始めます。"
 */
  public static func quick_guide_ozzy_bluetooth_pairing_item_1() -> String {
     return localizedString(
         key:"quick_guide_ozzy_bluetooth_pairing_item_1",
         defaultValue:"Start with your headphones turned off.",
         substitutions: [:])
  }

 /**
"Push and hold the control knob for 4 seconds until the LED indicator blinks blue."

     - **en**: "Push and hold the control knob for 4 seconds until the LED indicator blinks blue."
     - **da**: "Tryk på kontrolknappen og hold den nede i 4 sekunder, indtil LED-indikatoren blinker blåt."
     - **nl**: "Druk de bedieningsknop 4 seconden in totdat de led-indicator blauw knippert."
     - **de**: "Halte den Bedienknopf 4 Sekunden lang gedrückt, bis die LED-Anzeige blau blinkt."
     - **fr**: "Maintenez le bouton de contrôle enfoncé pendant 4 secondes, jusqu’à ce que le témoin LED clignote en bleu."
     - **it-IT**: "Tieni premuta la manopola di controllo per 4 secondi fino a quando l’indicatore LED lampeggia blu."
     - **nb**: "Trykk og hold kontrollknappen i 4 sekunder til LED-indikatoren blinker blått."
     - **pt-PT**: "Mantenha pressionado o botão de controlo durante 4 segundos até o indicador LED piscar a azul."
     - **ru**: "Нажмите и удерживайте кнопку управления в течение 4 секунд, пока светодиодный индикатор не замигает синим."
     - **es**: "Mantén pulsado el botón de control durante 4 segundos hasta que el indicador LED parpadee en azul."
     - **sv**: "Tryck och håll ner kontrollknappen i 4 sekunder tills LED-indikatorn blinkar blått."
     - **fi**: "Paina säätönuppia ja pidä sitä painettuna alas 4 sekunnin ajan, kunnes LED-merkkivalo vilkkuu sinisenä."
     - **zh-Hans**: "按住控制旋钮 4 秒钟，直到 LED 指示灯闪烁蓝色。"
     - **zh-Hant**: "按住控制旋鈕 4 秒鐘，直到 LED 指示燈閃爍藍色。"
     - **id**: "Tekan dan tahan knop kontrol selama 4 detik hingga lampu indikator LED berkedip biru."
     - **fil**: "Pindutin nang matagagal ang control knob nang 4 na segundo hanggang sa magpatay-sindi ang LED na indicator nang asul."
     - **ja**: "LEDランプが青色に点滅するまで、コントロールノブを4秒間押し続けます。"
 */
  public static func quick_guide_ozzy_bluetooth_pairing_item_2() -> String {
     return localizedString(
         key:"quick_guide_ozzy_bluetooth_pairing_item_2",
         defaultValue:"Push and hold the control knob for 4 seconds until the LED indicator blinks blue.",
         substitutions: [:])
  }

 /**
"Select MONITOR II from your sound device’s Bluetooth® list.\nThe indicator turns off when pairing is complete."

     - **en**: "Select MONITOR II from your sound device’s Bluetooth® list.\nThe indicator turns off when pairing is complete."
     - **da**: "Vælg MONITOR II fra din lydenheds Bluetooth®-liste.\n Indikatoren slukkes, når parringen er afsluttet."
     - **nl**: "Selecteer MONITOR II uit de Bluetooth®-lijst van je audioapparaat.\nDe indicator gaat uit als het koppelen is voltooid."
     - **de**: "Wähle MONITOR II aus der Liste der Bluetooth®-Geräte.\nDie Anzeige schaltet sich aus, sobald die Kopplung erfolgt ist."
     - **fr**: "Sélectionnez MONITOR II dans votre liste d’appareils audio Bluetooth®.\nLe témoin s’éteint lorsque l’appairage est effectué."
     - **it-IT**: "Seleziona MONITOR II dall’elenco dei tuoi dispositivi audio Bluetooth®.\nL’indicatore si spegne quando l’abbinamento è completo."
     - **nb**: "Velg MONITOR II fra lydenhetens Bluetooth®-liste.\nIndikatoren slår seg av når paringen er ferdig."
     - **pt-PT**: "Selecione MONITOR II na lista Bluetooth® do seu dispositivo de áudio.\nO indicador desliga-se quando o emparelhamento está completo."
     - **ru**: "Выберите MONITOR II из списка Bluetooth® вашего устройства. \n После выполнения сопряжения индикатор погаснет."
     - **es**: "Selecciona MONITOR II en la lista de Bluetooth® de tu dispositivo de sonido.\nEl indicador se apaga cuando el emparejamiento ha terminado."
     - **sv**: "Välj MONITOR II från din ljudenhets Bluetooth®-lista.\nIndikatorn slås av när parkopplingen är slutförd."
     - **fi**: "Valitse audiolaitteesi Bluetooth®-luettelosta MONITOR II.\nMerkkivalo sammuu, kun pariliitos on muodostettu."
     - **zh-Hans**: "从音频设备的 Bluetooth® 列表中选择MONITOR II。\n配对完成时，指示灯会熄灭。"
     - **zh-Hant**: "從音訊設備的 Bluetooth® 列表中選擇 MONITOR II。 \n 配對完成後，指示燈熄滅。"
     - **id**: "Pilih MONITOR II dari daftar perangkat audio Bluetooth® Anda.\nLampu indikator mati ketika pemasangan berhasil."
     - **fil**: "Piliin ang MONITOR II mula sa listahan ng iyong audio ng device Bluetooth®.\nMag-o-off ang indicator kapag nakumpleto na ang pagpapares."
     - **ja**: "お使いのサウンドデバイスのBluetooth®リストから[MONITOR II]を選択します。\nペアリングが完了すると、インジケーターがオフになります。"
 */
  public static func quick_guide_ozzy_bluetooth_pairing_item_3() -> String {
     return localizedString(
         key:"quick_guide_ozzy_bluetooth_pairing_item_3",
         defaultValue:"Select MONITOR II from your sound device’s Bluetooth® list.\nThe indicator turns off when pairing is complete.",
         substitutions: [:])
  }

 /**
"Press play and enjoy your Marshall  headphones."

     - **en**: "Press play and enjoy your Marshall  headphones."
     - **da**: "Tryk på afspil og nyd dine Marshall-hovedtelefoner."
     - **nl**: "Druk op play en geniet van je Marshall-hoofdtelefoon."
     - **de**: "Drücke auf Wiedergabe und genieße deinen Kopfhörer von Marshall."
     - **fr**: "Appuyez sur Lecture et profitez de votre casque Marshall."
     - **it-IT**: "Premi Play e goditi le tue Marshall headphones."
     - **nb**: "Trykk på play og nyt dine Marshall-hodetelefoner."
     - **pt-PT**: "Pressione play e desfrute dos seus auscultadores Marshall."
     - **ru**: "Запустите воспроизведение и наслаждайтесь звуком Marshall Headphones."
     - **es**: "Pulsa reproducir y disfruta de tus auriculares Marshall."
     - **sv**: "Tryck play och njut av dina Marshallhörlurar."
     - **fi**: "Paina toistopainiketta ja nauti Marshall-kuulokkeistasi."
     - **zh-Hans**: "按下播放，享受 Marshall Headphones 的精彩。"
     - **zh-Hant**: "按下播放享受您的 Marshall 耳機。"
     - **id**: "Tekan tombol mulai dan gunakan headphone Marshall Anda."
     - **fil**: "Pindutin ang play at tamasahin ang iyong Marshall na mga headphone."
     - **ja**: "再生を押して、Marshall ヘッドフォンをお楽しみください。"
 */
  public static func quick_guide_ozzy_bluetooth_pairing_item_4() -> String {
     return localizedString(
         key:"quick_guide_ozzy_bluetooth_pairing_item_4",
         defaultValue:"Press play and enjoy your Marshall  headphones.",
         substitutions: [:])
  }

 /**
"•   Start with your headphones turned off.\n\n•   Push and hold the control knob for 4 seconds until the LED indicator blinks blue.\n\n•   Select MONITOR II from your sound device’s Bluetooth® list.\nThe indicator turns off when pairing is complete.\n\n•   Press play and enjoy your Marshall  headphones."

     - **en**: "•   Start with your headphones turned off.\n\n•   Push and hold the control knob for 4 seconds until the LED indicator blinks blue.\n\n•   Select MONITOR II from your sound device’s Bluetooth® list.\nThe indicator turns off when pairing is complete.\n\n•   Press play and enjoy your Marshall  headphones."
     - **da**: "•   Start med dine hovedtelefoner slukket.\n\n•   Tryk på kontrolknappen og hold den nede i 4 sekunder, indtil LED-indikatoren blinker blåt.\n\n•   Vælg MONITOR II fra din lydenheds Bluetooth®-liste.\nIndikatoren slukkes, når parringen er afsluttet.\n\n•   Tryk på afspil og nyd dine Marshall-hovedtelefoner."
     - **nl**: "•   Begin met een uitgeschakelde hoofdtelefoon.\n\n•   Druk de bedieningsknop 4 seconden in totdat de led-indicator blauw knippert.\n\n•   Selecteer MONITOR II uit de Bluetooth®-lijst van je audioapparaat.\nDe indicator gaat uit als het koppelen is voltooid.\n\n•   Druk op play en geniet van je Marshall-hoofdtelefoon."
     - **de**: "• Lass deinen Kopfhörer zunächst ausgeschaltet.\n\n• Halte den Bedienknopf 4 Sekunden lang gedrückt, bis die LED-Anzeige blau blinkt.\n\n• Wähle MONITOR II aus der Liste der Bluetooth®-Geräte.\nDie Anzeige schaltet sich aus, sobald die Kopplung erfolgt ist.\n\n• Drücke auf Wiedergabe und genieße deinen Kopfhörer von Marshall."
     - **fr**: "•   Tout d’abord, votre casque doit être éteint.\n\n•   Maintenez le bouton de contrôle enfoncé pendant 4 secondes, jusqu’à ce que le témoin LED clignote en bleu.\n\n•   Sélectionnez MONITOR II dans votre liste d’appareils audio Bluetooth®.\nLe témoin s’éteint lorsque l’appairage est effectué.\n\n•   Appuyez sur Lecture et profitez de votre casque Marshall."
     - **it-IT**: "•   Inizia con le cuffie spente.\n\n•  Tieni premuta la manopola di controllo per 4 secondi fino a quando l’indicatore LED lampeggia blu.\n\n•  Seleziona MONITOR II dall’elenco dei tuoi dispositivi audio Bluetooth®.\nL’indicatore si spegne quando l’abbinamento è completo.\n\n•   Premi Play e goditi le tue Marshall headphones."
     - **nb**: "•   Start med hodetelefonene avslått.\n\n•   Trykk og hold kontrollknappen i 4 sekunder til LED-indikatoren blinker blått.\n\n•   Velg MONITOR II fra lydenhetens Bluetooth®-liste.\nIndikatoren slår seg av når paringen er ferdig.\n\n•   Trykk på play og nyt dine Marshall-hodetelefoner."
     - **pt-PT**: "Comece com os auscultadores desligados.\n\n•   Mantenha pressionado o botão de controlo durante 4 segundos até o indicador LED piscar a azul.\n\n•   Selecione MONITOR II na lista Bluetooth® do seu dispositivo de áudio.\nO indicador desliga-se quando o emparelhamento está completo.\n\n•   Pressione play e desfrute dos seus auscultadores Marshall."
     - **ru**: "•   Наушники должны быть выключены.\n\n•   Нажмите и удерживайте кнопку управления в течение 4 секунд, пока светодиодный индикатор не замигает синим.\n\n•   Выберите MONITOR II из списка Bluetooth® вашего устройства. \n\n•   После выполнения сопряжения индикатор погаснет.\n\n•   Запустите воспроизведение и наслаждайтесь звуком Marshall Headphones."
     - **es**: "•   Empieza con los auriculares apagados.\n\n•   Mantén pulsado el botón de control durante 4 segundos hasta que el indicador LED parpadee en azul.\n\n•   Selecciona MONITOR II en la lista de Bluetooth® de tu dispositivo de sonido.\nEl indicador se apaga cuando el emparejamiento ha terminado.\n\n•   Pulsa reproducir y disfruta de tus auriculares Marshall."
     - **sv**: "•   Börja med dina hörlurar avslagna.\n\n•   Tryck och håll ner kontrollknappen i 4 sekunder tills LED-indikatorn blinkar blått.\n\n•   Välj MONITOR II från din ljudenhets Bluetooth®-lista.\nIndikatorn slås av när parkopplingen är slutförd.\n\n•   Tryck play och njut av dina Marshallhörlurar."
     - **fi**: "•   Kytke kuulokkeesi aluksi pois käytöstä.\n\n•   Paina säätönuppia ja pidä sitä alaspainettuna 4 sekunnin ajan, kunnes LED-merkkivalo vilkkuu sinisenä.\n\n•  Valitse audiolaitteesi Bluetooth®-luettelosta MONITOR II.\nMerkkivalo sammuu, kun pariliitos on muodostettu.\n\n•   Paina toistopainiketta ja nauti Marshall-kuulokkeistasi."
     - **zh-Hans**: "•   首先关闭耳机。\n\n•   按住控制旋钮 4 秒钟，直到 LED 指示灯闪烁蓝色。\n\n•   从音频设备的 Bluetooth® 列表中选择MONITOR II。\n配对完成时，指示灯会熄灭。\n\n•   按下播放，享受 Marshall Headphones 的精彩。"
     - **zh-Hant**: "•  從關閉耳機開始。\n\n•  按住控制旋鈕 4 秒鐘，直到 LED 指示燈閃爍藍色。\n\n•  從音訊設備的 Bluetooth® 列表中選擇 MONITOR II。\n 配對完成後，指示燈會熄滅 。\n\n•  按下播放享受您的 Marshall 耳機。"
     - **id**: "1   Mulai dengan headphone dalam keadaan mati.\n\n•   Tekan dan tahan knop kontrol selama 4 detik hingga lampu indikator LED berkedip biru.\n\n•   Pilih MONITOR II dari daftar perangkat audio Bluetooth® Anda.\nLampu indikator mati ketika pemasangan berhasil.\n\n•   Tekan tombol mulai dan gunakan headphone Marshall Anda."
     - **fil**: "•   Mag-umpisa nang naka-off ang iyong mga headphone.\n\n•   Pindutin ang control knob nang 4 na segundo hanggang sa magpatay-sindi ang LED na indicator nang asul.\n\n•   Piliin ang MONITOR II mula sa listahan ng iyong audio ng device Bluetooth®.\nMag-o-off ang indicator kapag nakumpleto na ang pagpapares.\n\n•   Pindutin ang play at tamasahin ang iyong Marshall na mga headphone."
     - **ja**: "•   ヘッドフォンの電源がオフの状態で始めます。●●•   LEDランプが青色に点滅するまで、コントロールノブを4秒間押し続けます。●●•   お使いのサウンドデバイスのBluetooth®リストから[MONITOR II]を選択します。●ペアリングが完了すると、インジケーターがオフになります。●●•   再生を押して、Marshall ヘッドフォンをお楽しみください。"
 */
  public static func quick_guide_ozzy_bluetooth_pairing_v1() -> String {
     return localizedString(
         key:"quick_guide_ozzy_bluetooth_pairing_v1",
         defaultValue:"•   Start with your headphones turned off.\n\n•   Push and hold the control knob for 4 seconds until the LED indicator blinks blue.\n\n•   Select MONITOR II from your sound device’s Bluetooth® list.\nThe indicator turns off when pairing is complete.\n\n•   Press play and enjoy your Marshall  headphones.",
         substitutions: [:])
  }

 /**
"Use the control knob on the right earcap to control your heaphones.\n\n1   Power on/off - push and hold for 2 seconds\n2   Play/pause – single click\n3   Skip forward/back - push to the right or left\n4   Turn the volume up/down – push upwards or downwards\n5   Answer/end call – single click\n6   Reject an incoming call - double-click"

     - **en**: "Use the control knob on the right earcap to control your heaphones.\n\n1   Power on/off - push and hold for 2 seconds\n2   Play/pause – single click\n3   Skip forward/back - push to the right or left\n4   Turn the volume up/down – push upwards or downwards\n5   Answer/end call – single click\n6   Reject an incoming call - double-click"
     - **da**: "Brug kontrolknappen på den højre øremuffe til at kontrollere dine hovedtelefoner.\n\n1   Tænd/sluk - tryk og hold i 2 sekunder\n2   Afspil/pause – enkelt klik\n3   Spring frem/tilbage – skub til højre eller venstre\n4   Skru lyden op/ned – skub op eller ned\n5   Besvar/afslut opkald – enkelt klik\n6   Afvis et indgående opkald – dobbeltklik"
     - **nl**: "Gebruik de bedieningsknop op de rechteroordop om je hoofdtelefoon te bedienen.\n\n1   Aan/uitzetten - houd 2 seconden ingedrukt\n2   Afspelen/pauzeren - één keer klikken\n3   Spring vooruit/achteruit - duw naar rechts of naar links\n4   Volume omhoog/omlaag - duw naar boven of naar beneden\n5   Beantwoord/beëindig gesprek - één keer klikken\n6   Een binnenkomend gesprek weigeren - dubbelklik"
     - **de**: "Verwende den Bedienknopf auf der rechten Ohrmuschel, um deinen Kopfhörer zu steuern.\n\n1 Ein-/Ausschalten – 2 Sekunden gedrückt halten\n2 Wiedergabe/Pause – einmal drücken\n3 Zurück oder vorwärts springen – nach links oder rechts drücken\n4 Lautstärke erhöhen/senken – nach oben oder unten drücken\n5 Anruf entgegennehmen/beenden – einmal drücken\n6 Einen Anruf abweisen – zweimal drücken"
     - **fr**: "Utilisez le bouton de contrôle situé sur le pavillon d’écouteur droit pour contrôler votre casque.\n\n1   Marche/arrêt - maintenez enfoncé pendant 2 secondes\n2   Lecture/pause - cliquez une fois\n3   Avance/retour rapide - appuyez vers la droite ou la gauche\n4   Monter/baisser le volume - appuyez vers le haut/vers le bas\n5   Répondre à un appel/raccrocher - cliquez une fois\n6   Rejeter un appel - double-clic"
     - **it-IT**: "Usa la manopola di controllo sul padiglione destro per controllare le cuffie.\n\n1   Accensione/spegnimento - tieni premuto per 2 secondi\n2   Play/pausa – premi una volta\n3   Traccia successiva/precedente - premi a destra o a sinistra\n4   Alzare o abbassare il volume – premi verso l’alto o verso il basso\n5   Rispondere a o terminare una chiamata – premi una volta\n6   Rifiuta una chiamata in entrata - premi due volte"
     - **nb**: "Bruk kontrollknappen på høyre ørekopp til å kontrollere hodetelefonene.\n\n1   Slå av/på – Trykk og hold i 2 sekunder\n2   Play/pause – Enkeltklikk\n3   Hopp fremover/bakover – Skyv til høyre eller venstre\n4   Skru volumet opp/ned – Skyv opp eller ned\n5   Svare på/avslutte anrop – Enkeltklikk\n6   Avvis et innkommende anrop – Dobbeltklikk"
     - **pt-PT**: "Use o botão de controlo no auscultador direito para controlar os seus auscultadores.\n\n1   Ligar/desligar – pressione e mantenha pressionado durante 2 segundos\n2   Tocar/pausa – clique uma vez\n3   Avançar/retroceder – pressione para a direita ou para a esquerda\n4   Aumentar/diminuir o volume – pressione para cima ou para baixo\n5   Atender/terminar chamada – clique uma vez\n6   Rejeitar uma chamada recebida – clique duas vezes"
     - **ru**: "Для управления наушниками используйте кнопку управления на правой чашке. \n\n1 Включение/выключение: нажмите и удерживайте 2 секунды \n2 Воспроизведение/пауза: одинарное нажатие \n3 Переход вперед/назад: нажатие справа или слева \n4 Увеличение/уменьшение громкости: нажатие вверху или внизу \n5 Ответить/завершить звонок: единичное нажатие \n6 Отклонить входящий звонок: двойное нажатие"
     - **es**: "Usa el botón de control en la carcasa derecha para controlar tus auriculares.\n\n1   Encender y apagar: mantenlo pulsado 2 segundos\n2   Reproducir/pausar: un único clic\n3   Saltar hacia adelante/atrás: pulsa a la derecha o a la izquierda\n4   Subir/bajar el volumen: pulsa hacia arriba o hacia abajo\n5   Descolgar/colgar llamadas: un único clic\n6   Rechazar una llamada entrante: doble clic"
     - **sv**: "Använd kontrollknappen på den högra öronkåpan för att kontrollera dina hörlurar.\n\n1   Slå på/av – tryck och håll ner i 2 sekunder\n2   Spela/pausa – enkelklicka\n3   Hoppa framåt/bakåt – tryck till höger eller vänster\n4   Höj/sänk volymen – tryck uppåt eller neråt\n5   Svara på/avsluta samtal – enkelklicka\n6   Avvisa ett inkommande samtal – dubbelklicka"
     - **fi**: "Ohjaa kuulokkeitasi oikeanpuoleisen korvakupin kyljessä olevalla säätönupilla.\n\n1   Virta päälle/pois – paina ja pidä alhaalla 2 sekunnin ajan\n2   Toisto/pysäytys – yksi napsautus\n3   Siirtyminen eteenpäin/taaksepäin – työnnä oikealle tai vasemmalle\n4   Äänenvoimakkuuden säätäminen ylöspäin/alaspäin – työnnä ylöspäin tai alaspäin\n5   Puheluun vastaaminen / sen lopettaminen – yksi napsautus\n6   Saapuvan puhelun hylkääminen – kaksoisnapsautus"
     - **zh-Hans**: "使用右耳套上的控制旋钮控制您的耳机。\n\n1   电源开/关 - 按住 2 秒\n2   播放/暂停 - 单击\n3   快进/后退 - 向右或向左推\n4   调高/调低音量 - 向上或向下推\n5   接听/挂断电话 - 单击\n6   拒绝来电 - 双击"
     - **zh-Hant**: "使用右耳帽上的控制旋鈕控制您的耳機。\n\n1  開機/關機 - 按住 2 秒鐘 \n2  播放/暫停–單擊 \n3  向前/向後跳過 - 向右或向左推 \n4  向上/向下調節音量 - 向上或向下推 \n5  接聽/掛斷電話–單擊 \n6  拒絕來電 - 雙擊"
     - **id**: "Gunakan knop kontrol pada earcap kanan untuk mengatur headphone Anda.\n\n1   Nyalakan/Matikan - tekan dan tahan selama 2 detik\n2   Putar/jeda – sekali klik\n3   Lompat ke lagu berikutnya/sebelumnya - Tekan ke kanan atau kiri\n4   Atur tinggi/rendah volume – tekan ke arah atas atau ke bawah\n5   Angkat/akhiri panggilan – sekali klik\n6   Tolak panggilan masuk - klik dua kali"
     - **fil**: "Gamitin ang control knob sa kanang earcap upang kontrolin ang iyong mga heaphone.\n\n1   Power on/off - pindutin nang 2 seconds\n2   Play/pause – isahang pindot\n3   Lumaktaw pasulong/pabalik - pindutin pakanan o pakaliwa\n4   Lakasan/hinaan ang volume – pindutin pataas o pababa\n5   Sagutin/wakasan ang tawag – isahang pindot\n6   Tanggihan ang papasok na tawag - dobleng pindot"
     - **ja**: "右側のイヤーキャップのコントロールノブを使用して、ヘッドフォンをコントロールします。●●1   電源オン／オフ - 2秒間長押し●2   再生／停止 – 1回クリック●3   早送り／早戻し - 右または左へと押す●4   音量を上げる／下げる – 上または下へと押す●5   電話に出る／終了する – 1回クリック●6   着信を拒否する - 2回クリック"
 */
  public static func quick_guide_ozzy_item_control_knob() -> String {
     return localizedString(
         key:"quick_guide_ozzy_item_control_knob",
         defaultValue:"Use the control knob on the right earcap to control your heaphones.\n\n1   Power on/off - push and hold for 2 seconds\n2   Play/pause – single click\n3   Skip forward/back - push to the right or left\n4   Turn the volume up/down – push upwards or downwards\n5   Answer/end call – single click\n6   Reject an incoming call - double-click",
         substitutions: [:])
  }

 /**
"Use the control knob on the right earcap to control your heaphones.\n\n•   Power on/off - push and hold for 2 seconds\n•   Play/pause – single click\n•   Skip forward/back - push to the right or left\n•   Turn the volume up/down – push upwards or downwards\n•   Answer/end call – single click\n•   Reject an incoming call - double-click"

     - **en**: "Use the control knob on the right earcap to control your heaphones.\n\n•   Power on/off - push and hold for 2 seconds\n•   Play/pause – single click\n•   Skip forward/back - push to the right or left\n•   Turn the volume up/down – push upwards or downwards\n•   Answer/end call – single click\n•   Reject an incoming call - double-click"
     - **da**: "Brug kontrolknappen på den højre øremuffe til at kontrollere dine hovedtelefoner.\n\n•   Tænd/sluk - tryk og hold i 2 sekunder\n•   Afspil/pause – enkelt klik\n•   Spring frem/tilbage – skub til højre eller venstre\n•   Skru lyden op/ned – skub op eller ned\n•   Besvar/afslut opkald – enkelt klik\n•   Afvis et indgående opkald - dobbeltklik"
     - **nl**: "Gebruik de bedieningsknop op de rechteroordop om je hoofdtelefoon te bedienen.\n\n•   Aan/uitzetten - houd 2 seconden ingedrukt\n•   Afspelen/pauzeren - één keer klikken\n•   Spring vooruit/achteruit - duw naar rechts of naar links\n•   Volume omhoog/omlaag - duw naar boven of naar beneden\n•   Beantwoord/beëindig gesprek - één keer klikken\n•   Een binnenkomend gesprek weigeren - dubbelklik"
     - **de**: "Verwende den Bedienknopf auf der rechten Ohrmuschel, um deinen Kopfhörer zu steuern.\n\n• Ein-/Ausschalten – 2 Sekunden gedrückt halten\n• Wiedergabe/Pause – einmal drücken\n• Zurück oder vorwärts springen – nach links oder rechts drücken\n• Lautstärke erhöhen/senken – nach oben oder unten drücken\n• Anruf entgegennehmen/beenden – einmal drücken\n• Einen Anruf abweisen – zweimal drücken"
     - **fr**: "Utilisez le bouton de contrôle situé sur le pavillon d’écouteur droit pour contrôler votre casque.\n\n•   Marche/arrêt - maintenez enfoncé pendant 2 secondes\n•   Lecture/pause - cliquez une fois\n•   Avance/retour rapide - appuyez vers la droite ou la gauche\n•   Monter/baisser le volume - appuyez vers le haut/vers le bas\n•   Répondre à un appel/raccrocher - cliquez une fois\n•   Rejeter un appel - double-clic"
     - **it-IT**: "Usa la manopola di controllo sul padiglione destro per controllare le cuffie.\n\n• Accensione/spegnimento - tieni premuto per 2 secondi\n•   Play/pausa – premi una volta\n•  Traccia successiva/precedente - premi a destra o a sinistra\n•   Alzare o abbassare il volume – premi verso l’alto o verso il basso\n•  Rispondere a o terminare una chiamata – premi una vota\n•   Rifiuta una chiamata in entrata - premi due volte"
     - **nb**: "Bruk kontrollknappen på høyre ørekopp til å kontrollere hodetelefonene.\n\n•   Slå av/på – Trykk og hold i 2 sekunder\n•   Play/pause – Enkeltklikk\n•   Hopp fremover/bakover – Skyv til høyre eller venstre\n•   Skru volumet opp/ned – Skyv opp eller ned\n•   Svare på/avslutte anrop – Enkeltklikk\n•   Avvise et innkommende anrop – Dobbeltklikk"
     - **pt-PT**: "Use o botão de controlo no auscultador direito para controlar os seus auscultadores.\n\n•   Ligar/desligar – pressione e mantenha pressionado durante 2 segundos\n•   Tocar/pausa – clique uma vez\n•   Avançar/retroceder – pressione para a direita ou para a esquerda\n•   Aumentar/diminuir o volume – pressione para cima ou para baixo\n•   Atender/terminar chamada – clique uma vez\n•   Rejeitar uma chamada recebida – clique duas vezes"
     - **ru**: "Для управления наушниками используйте кнопку управления на правой чашке. \n\n• Включение/выключение: нажмите и удерживайте 2 секунды \n• Воспроизведение/пауза: одинарное нажатие \n• Переход вперед/назад: нажатие справа или слева \n• Увеличение/уменьшение громкости: нажатие вверху или внизу \n• Ответить/завершить звонок: единичное нажатие \n• Отклонить входящий звонок: двойное нажатие"
     - **es**: "Usa el botón de control en la carcasa derecha para controlar tus auriculares.\n\n•   Encender y apagar: mantenlo pulsado 2 segundos\n•   Reproducir/pausar: un único clic\n•   Saltar hacia adelante/atrás: pulsa a la derecha o a la izquierda\n•   Subir/bajar el volumen: pulsa hacia arriba o hacia abajo\n•   Descolgar/colgar llamadas: un único clic\n•   Rechazar una llamada entrante: doble clic"
     - **sv**: "Använd kontrollknappen på den högra öronkåpan för att kontrollera dina hörlurar.\n\n•   Slå på/av – tryck och håll ner i 2 sekunder\n•   Spela/pausa – enkelklicka\n•   Hoppa framåt/bakåt – tryck till höger eller vänster\n•   Höj/sänk volymen – tryck uppåt eller neråt\n•    Svara på/avsluta samtal – enkelklicka\n•   Avvisa ett inkommande samtal – dubbelklicka"
     - **fi**: "Ohjaa kuulokkeitasi oikeanpuoleisen korvakupin kyljessä olevalla säätönupilla.\n\n•   Virta päälle/pois – paina ja pidä alhaalla 2 sekunnin ajan\n•   Toisto/pysäytys – yksi napsautus\n•   Siirtyminen eteenpäin/taaksepäin – työnnä oikealle tai vasemmalle\n•   Äänenvoimakkuuden säätäminen ylöspäin/alaspäin – työnnä ylöspäin tai alaspäin\n•   Puheluun vastaaminen / sen lopettaminen – yksi napsautus\n•   Saapuvan puhelun hylkääminen – kaksoisnapsautus"
     - **zh-Hans**: "使用右耳套上的控制旋钮控制您的耳机。\n\n•   电源开/关 - 按住 2 秒\n•   播放/暂停 - 单击\n•   快进/后退 - 向右或向左推\n•   调高/调低音量 - 向上或向下推\n•   接听/挂断电话 - 单击\n•   拒绝来电 - 双击"
     - **zh-Hant**: "使用右耳帽上的控制旋鈕控制您的耳機。\n\n1  開機/關機 - 按住 2 秒鐘 \n2  播放/暫停–單擊 \n3  向前/向後跳過 - 向右或向左推 \n4  向上/向下調節音量 - 向上或向下推 \n5  接聽/掛斷電話–單擊 \n6  拒絕來電 - 雙擊"
     - **id**: "Gunakan knop kontrol pada earcap kanan untuk mengatur headphone Anda.\n\n•   Nyalakan/Matikan - tekan dan tahan selama 2 detik\n•   Putar/jeda – sekali klik\n•   Lompat ke lagu berikutnya/sebelumnya - Tekan ke kanan atau kiri\n•   Atur tinggi/rendah volume – tekan ke arah atas atau ke bawah\n•   Angkat/akhiri panggilan – sekali klik\n•   Tolak panggilan masuk - klik dua kali"
     - **fil**: "Gamitin ang control knob sa kanang earcap upang kontrolin ang iyong mga heaphone.\n\n•   Power on/off - pindutin nang 2 seconds\n•   Play/pause – isahang pindot\n•   Lumaktaw pasulong/pabalik - pindutin pakanan o pakaliwa\n•   Lakasan/hinaan ang volume – pindutin pataas o pababa\n•   Sagutin/wakasan ang tawag – isahang pindot\n•   Tanggihan ang papasok na tawag - dobleng pindot"
     - **ja**: "右側のイヤーキャップのコントロールノブを使用して、ヘッドフォンをコントロールします。●●•   電源オン／オフ - 2秒間長押し●•   再生／停止 – 1回クリック●•   早送り／早戻し - 右または左へと押す●•   音量を上げる／下げる – 上または下へと押す●•   電話に出る／終了する – 1回クリック●•   着信を拒否する - 2回クリック"
 */
  public static func quick_guide_ozzy_item_control_knob_v1() -> String {
     return localizedString(
         key:"quick_guide_ozzy_item_control_knob_v1",
         defaultValue:"Use the control knob on the right earcap to control your heaphones.\n\n•   Power on/off - push and hold for 2 seconds\n•   Play/pause – single click\n•   Skip forward/back - push to the right or left\n•   Turn the volume up/down – push upwards or downwards\n•   Answer/end call – single click\n•   Reject an incoming call - double-click",
         substitutions: [:])
  }

 /**
"Use the control knob on the right earcap to control your heaphones."

     - **en**: "Use the control knob on the right earcap to control your heaphones."
     - **da**: "Brug kontrolknappen på den højre øremuffe til at kontrollere dine hovedtelefoner."
     - **nl**: "Gebruik de bedieningsknop op de rechteroordop om je hoofdtelefoon te bedienen."
     - **de**: "Verwende den Bedienknopf auf der rechten Ohrmuschel, um deinen Kopfhörer zu steuern."
     - **fr**: "Utilisez le bouton de contrôle situé sur le pavillon d’écouteur droit pour contrôler votre casque."
     - **it-IT**: "Usa la manopola di controllo sul padiglione destro per controllare le cuffie."
     - **nb**: "Bruk kontrollknappen på høyre ørekopp til å kontrollere hodetelefonene."
     - **pt-PT**: "Use o botão de controlo no auscultador direito para controlar os seus auscultadores."
     - **ru**: "Для управления наушниками используйте кнопку управления на правой чашке."
     - **es**: "Usa el botón de control en la carcasa derecha para controlar tus auriculares."
     - **sv**: "Använd kontrollknappen på den högra öronkåpan för att kontrollera dina hörlurar."
     - **fi**: "Ohjaa kuulokkeitasi oikeanpuoleisen korvakupin kyljessä olevalla säätönupilla."
     - **zh-Hans**: "使用右耳套上的控制旋钮控制您的耳机。"
     - **zh-Hant**: "使用右耳帽上的控制旋鈕控制您的耳機。"
     - **id**: "Gunakan knop kontrol pada earcap kanan untuk mengatur headphone Anda."
     - **fil**: "Gamitin ang control knob sa kanang earcap upang kontrolin ang iyong mga heaphone."
     - **ja**: "右側のイヤーキャップのコントロールノブを使用して、ヘッドフォンをコントロールします。"
 */
  public static func quick_guide_ozzy_item_control_knob_v2() -> String {
     return localizedString(
         key:"quick_guide_ozzy_item_control_knob_v2",
         defaultValue:"Use the control knob on the right earcap to control your heaphones.",
         substitutions: [:])
  }

 /**
"Power on/off - push and hold for 2 seconds"

     - **en**: "Power on/off - push and hold for 2 seconds"
     - **da**: "Tænd/sluk - tryk og hold i 2 sekunder"
     - **nl**: "Aan/uitzetten - houd 2 seconden ingedrukt"
     - **de**: "Ein-/Ausschalten – 2 Sekunden gedrückt halten"
     - **fr**: "Marche/arrêt - maintenez enfoncé pendant 2 secondes"
     - **it-IT**: "Accensione/spegnimento - tieni premuto per 2 secondi"
     - **nb**: "Slå av/på – Trykk og hold i 2 sekunder"
     - **pt-PT**: "Ligar/desligar – pressione e mantenha pressionado durante 2 segundos"
     - **ru**: "Включение/выключение: нажмите и удерживайте 2 секунды "
     - **es**: "Encender y apagar: mantenlo pulsado 2 segundos"
     - **sv**: "Slå på/av – tryck och håll ner i 2 sekunder"
     - **fi**: "Virta päälle/pois – paina ja pidä alhaalla 2 sekunnin ajan"
     - **zh-Hans**: "电源开/关 - 按住 2 秒"
     - **zh-Hant**: "開機/關機 - 按住 2 秒鐘 "
     - **id**: "Nyalakan/Matikan - tekan dan tahan selama 2 detik"
     - **fil**: "Power on/off - pindutin nang 2 seconds"
     - **ja**: "電源オン／オフ - 2秒間長押し"
 */
  public static func quick_guide_ozzy_item_control_knob_v2_bullet_1() -> String {
     return localizedString(
         key:"quick_guide_ozzy_item_control_knob_v2_bullet_1",
         defaultValue:"Power on/off - push and hold for 2 seconds",
         substitutions: [:])
  }

 /**
"Play/pause – single click"

     - **en**: "Play/pause – single click"
     - **da**: "Afspil/pause – enkelt klik"
     - **nl**: "Afspelen/pauzeren - één keer klikken"
     - **de**: "Wiedergabe/Pause – einmal drücken"
     - **fr**: "Lecture/pause - cliquez une fois"
     - **it-IT**: "Play/pausa – premi una volta"
     - **nb**: "Play/pause – Enkeltklikk"
     - **pt-PT**: "Tocar/pausa – clique uma vez"
     - **ru**: "Воспроизведение/пауза: одинарное нажатие "
     - **es**: "Reproducir/pausar: un único clic"
     - **sv**: "Spela/pausa – enkelklicka"
     - **fi**: "Toisto/pysäytys – yksi napsautus"
     - **zh-Hans**: "播放/暂停 - 单击"
     - **zh-Hant**: "播放/暫停–單擊 "
     - **id**: "Putar/jeda – sekali klik"
     - **fil**: "Play/pause – isahang pindot"
     - **ja**: "再生／停止 – 1回クリック"
 */
  public static func quick_guide_ozzy_item_control_knob_v2_bullet_2() -> String {
     return localizedString(
         key:"quick_guide_ozzy_item_control_knob_v2_bullet_2",
         defaultValue:"Play/pause – single click",
         substitutions: [:])
  }

 /**
"Skip forward/back - push to the right or left"

     - **en**: "Skip forward/back - push to the right or left"
     - **da**: "Spring frem/tilbage – skub til højre eller venstre"
     - **nl**: "Spring vooruit/achteruit - duw naar rechts of naar links"
     - **de**: "Zurück oder vorwärts springen – nach links oder rechts drücken"
     - **fr**: "Avance/retour rapide - appuyez vers la droite ou la gauche"
     - **it-IT**: "Traccia successiva/precedente - premi a destra o a sinistra"
     - **nb**: "Hopp fremover/bakover – Skyv til høyre eller venstre"
     - **pt-PT**: "Avançar/retroceder – pressione para a direita ou para a esquerda"
     - **ru**: "Переход вперед/назад: нажатие справа или слева "
     - **es**: "Saltar hacia adelante/atrás: pulsa a la derecha o a la izquierda"
     - **sv**: "Hoppa framåt/bakåt – tryck till höger eller vänster"
     - **fi**: "Siirtyminen eteenpäin/taaksepäin – työnnä oikealle tai vasemmalle"
     - **zh-Hans**: "快进/后退 - 向右或向左推"
     - **zh-Hant**: "向前/向後跳過 - 向右或向左推 "
     - **id**: "Lompat ke lagu berikutnya/sebelumnya - Tekan ke kanan atau kiri"
     - **fil**: "Lumaktaw pasulong/pabalik - pindutin pakanan o pakaliwa"
     - **ja**: "早送り／早戻し - 右または左へと押す"
 */
  public static func quick_guide_ozzy_item_control_knob_v2_bullet_3() -> String {
     return localizedString(
         key:"quick_guide_ozzy_item_control_knob_v2_bullet_3",
         defaultValue:"Skip forward/back - push to the right or left",
         substitutions: [:])
  }

 /**
"Turn the volume up/down – push upwards or downwards"

     - **en**: "Turn the volume up/down – push upwards or downwards"
     - **da**: "Skru lyden op/ned – skub op eller ned"
     - **nl**: "Volume omhoog/omlaag - duw naar boven of naar beneden"
     - **de**: "Lautstärke erhöhen/senken – nach oben oder unten drücken"
     - **fr**: "Monter/baisser le volume - appuyez vers le haut/vers le bas"
     - **it-IT**: "Alzare o abbassare il volume – premi verso l’alto o verso il basso"
     - **nb**: "Skru volumet opp/ned – Skyv opp eller ned"
     - **pt-PT**: "Aumentar/diminuir o volume – pressione para cima ou para baixo"
     - **ru**: "Увеличение/уменьшение громкости: нажатие вверху или внизу "
     - **es**: "Subir/bajar el volumen: pulsa hacia arriba o hacia abajo"
     - **sv**: "Höj/sänk volymen – tryck uppåt eller neråt"
     - **fi**: "Äänenvoimakkuuden säätäminen ylöspäin/alaspäin – työnnä ylöspäin tai alaspäin"
     - **zh-Hans**: "调高/调低音量 - 向上或向下推"
     - **zh-Hant**: "向上/向下調節音量 - 向上或向下推 "
     - **id**: "Atur tinggi/rendah volume – tekan ke arah atas atau ke bawah"
     - **fil**: "Lakasan/hinaan ang volume – pindutin pataas o pababa"
     - **ja**: "音量を上げる／下げる – 上または下へと押す"
 */
  public static func quick_guide_ozzy_item_control_knob_v2_bullet_4() -> String {
     return localizedString(
         key:"quick_guide_ozzy_item_control_knob_v2_bullet_4",
         defaultValue:"Turn the volume up/down – push upwards or downwards",
         substitutions: [:])
  }

 /**
"Answer/end call – single click"

     - **en**: "Answer/end call – single click"
     - **da**: "Besvar/afslut opkald – enkelt klik"
     - **nl**: "Beantwoord/beëindig gesprek - één keer klikken"
     - **de**: "Anruf entgegennehmen/beenden – einmal drücken"
     - **fr**: "Répondre à un appel/raccrocher - cliquez une fois"
     - **it-IT**: "Rispondere a o terminare una chiamata – premi una volta"
     - **nb**: "Svare på/avslutte anrop – Enkeltklikk"
     - **pt-PT**: "Atender/terminar chamada – clique uma vez"
     - **ru**: "Ответить/завершить звонок: единичное нажатие"
     - **es**: "Descolgar/colgar llamadas: un único clic"
     - **sv**: "Svara på/avsluta samtal – enkelklicka"
     - **fi**: "Puheluun vastaaminen / sen lopettaminen – yksi napsautus"
     - **zh-Hans**: "接听/挂断电话 - 单击"
     - **zh-Hant**: "接聽/掛斷電話–單擊 "
     - **id**: "Angkat/akhiri panggilan – sekali klik"
     - **fil**: "Sagutin/wakasan ang tawag – isahang pindot"
     - **ja**: "電話に出る／終了する – 1回クリック"
 */
  public static func quick_guide_ozzy_item_control_knob_v2_bullet_5() -> String {
     return localizedString(
         key:"quick_guide_ozzy_item_control_knob_v2_bullet_5",
         defaultValue:"Answer/end call – single click",
         substitutions: [:])
  }

 /**
"Reject an incoming call - double-click"

     - **en**: "Reject an incoming call - double-click"
     - **da**: "Afvis et indgående opkald – dobbeltklik"
     - **nl**: "Een binnenkomend gesprek weigeren - dubbelklik"
     - **de**: "Einen Anruf abweisen – zweimal drücken"
     - **fr**: "Rejeter un appel - double-clic"
     - **it-IT**: "Rifiuta una chiamata in entrata - premi due volte"
     - **nb**: "Avvis et innkommende anrop – Dobbeltklikk"
     - **pt-PT**: "Rejeitar uma chamada recebida – clique duas vezes"
     - **ru**: " Отклонить входящий звонок: двойное нажатие"
     - **es**: "Rechazar una llamada entrante: doble clic"
     - **sv**: "Avvisa ett inkommande samtal – dubbelklicka"
     - **fi**: "Saapuvan puhelun hylkääminen – kaksoisnapsautus"
     - **zh-Hans**: "拒绝来电 - 双击"
     - **zh-Hant**: "拒絕來電 - 雙擊"
     - **id**: "Tolak panggilan masuk - klik dua kali"
     - **fil**: "Tanggihan ang papasok na tawag - dobleng pindot"
     - **ja**: "着信を拒否する - 2回クリック"
 */
  public static func quick_guide_ozzy_item_control_knob_v2_bullet_6() -> String {
     return localizedString(
         key:"quick_guide_ozzy_item_control_knob_v2_bullet_6",
         defaultValue:"Reject an incoming call - double-click",
         substitutions: [:])
  }

 /**
"The mutli-function M-button lets you switch between three equaliser presets or alternatively, access your voice assistant on-the-go.\nGo to the M-Button settings menu and set the M-button for Equaliser, Google Assistant or Siri.\n\nEQALISER\nChoose three favourite EQ settings. Click the M-Button to toggle between them.\n\nGOOGLE ASSISTANT\nUse the M-Button to interact with the Google Assistant. See the Google Assistant app for how to interact with the Google Assistant.\n\nAPPLE SIRI\nClick the M-Button to start and stop Siri."

     - **en**: "The mutli-function M-button lets you switch between three equaliser presets or alternatively, access your voice assistant on-the-go.\nGo to the M-Button settings menu and set the M-button for Equaliser, Google Assistant or Siri.\n\nEQALISER\nChoose three favourite EQ settings. Click the M-Button to toggle between them.\n\nGOOGLE ASSISTANT\nUse the M-Button to interact with the Google Assistant. See the Google Assistant app for how to interact with the Google Assistant.\n\nAPPLE SIRI\nClick the M-Button to start and stop Siri."
     - **da**: "Den multi-funktionelle M-knap giver dig mulighed for at skifte mellem tre forudindstillinger af equalizer eller alternativt få adgang til din stemmeassistent på farten.\nGå til menuen i M-knap-indstillinger og indstil M-knappen for Equalizer, Google Assistent eller Siri.\n\nEQUALIZER\nVælg tre foretrukne EQ-indstillinger. Klik på M-knappen for at skifte mellem dem. \n\nGOOGLE ASSISTENT\nBrug M-knappen til at interagere med Google Assistent. Se Google Assistent-appen for, hvordan du interagerer med Google Assistent.\n\nAPPLE SIRI\nKlik på M-knappen for at starte og stoppe Siri."
     - **nl**: "Met de multifunctionele M-knop kun je wisselen tussen drie equalizer presets of eventueel onderweg je stemassistent gebruiken.\nGa naar het instellingenmenu van de M-knop en stel de M-knop in op Equalizer, Google Assistent of Siri.\n\nEQALIZER\nKies drie favoriete EQ-instellingen. Klik op de M-knop om tussen deze instellingen te schakelen.\n\nGebruik de M-knop om met de Google Assistent te communiceren. Raadpleeg de Google Assistent-app voor meer informatie over de interactie met de Google Assistent.\n\nAPPLE SIRI\nKlik op de M-knop om Siri te starten en te stoppen."
     - **de**: "Die multifunktionale M-Taste ermöglicht das Wechseln zwischen drei verschiedenen Equalizer-Voreinstellungen oder alternativ das Aktivieren des Sprachassistenten unterwegs.\nÖffne das Einstellungsmenü für die M-Taste und richte sie für den Equalizer, Google Assistant oder Siri ein.\n\nEQUALIZER\nWähle drei bevorzugte EQ-Einstellungen aus. Drücke die M-Taste, um zwischen ihnen zu wechseln.\n\nGOOGLE ASSISTANT\nVerwende die M-Taste, um mit dem Google Assistant zu interagieren. Sieh dir in der Google Assistant App an, wie das funktioniert.\n\nAPPLE SIRI\nDrücke die M-Taste, um Siri zu starten und anzuhalten."
     - **fr**: "Le bouton M multifonctionnel vous permet de naviguer entre trois préréglages d’égalisation ou d’accéder à votre assistant vocal lors de vos déplacements.\nRendez-vous dans le menu des paramètres du bouton M et configurez-le pour l’égaliseur, Assistant Google ou Siri.\n\nÉGALISEUR\nChoisissez vos trois réglages d’égalisation favoris. Cliquez sur le bouton M pour passer d’un réglage à l’autre.\n\nASSISTANT GOOGLE\nUtilisez le bouton M pour interagir avec l’Assistant Google. Veuillez consulter l’application Assistant Google pour découvrir comment interagir avec cette fonctionnalité.\n\nAPPLE SIRI\nCliquez sur le bouton M pour activer/désactiver Siri."
     - **it-IT**: "Il pulsante multi-funzione M ti fa scegliere fra tre diversi preset dell’equalizzatore o, in alternativa, accedere al tuo assistente vocale.\nAccedi al menu delle impostazioni del pulsante M e impostalo come Equalizzatore, Assistente Google o Siri.\n\nScegli tra le tue tre impostazioni EQ preferite. Clicca sul pulsante M per passare dall’una all’altra.\n\nASSISTENTE GOOGLE\nUsa il pulsante M per interagire con l’Assistente Google. Fai riferimento all’app Assistente Google per scoprire come interagire con l’Assistente Google.\n\nAPPLE SIRI\nClicca sul pulsante M per avviare e fermare Siri."
     - **nb**: "Med multifunksjonsknappen, M-knappen, kan du bytte mellom tre equalizer-innstillinger eller få tilgang til taleassistenten din på farten.\nGå til innstillingermenyen for M-knappen og angi M-knappen for equalizer, Google Assistent eller Siri.\n\nEQUALIZER\nVelg tre favorittinnstillinger for EQ. Klikk på M-knappen for å bytte mellom dem.\n\nGOOGLE ASSISTENT\nBruk M-knappen til å samhandle med Google Assistent. Se Google Assistent-appen for å finne ut hvordan du samhandler med Google Assistent.\n\nAPPLE SIRI\nKlikk på M-knappen for å starte og stoppe Siri."
     - **pt-PT**: "O botão M mutlifuncional permite alternar entre três predefinições do equalizador ou, em alternativa, aceder ao assistente de voz em movimento.\nVá ao menu de definições do botão M e defina o botão M para Equalizador, Google Assistente ou Siri.\n\nEQUALIZADOR\nEscolha três definições de equalizador da sua preferência. Clique no botão M para alternar entre elas.\n\nGOOGLE ASSISTENTE\nUse o botão M para interagir com o Google Assistente. Consulte a aplicação Google Assistente para saber como interagir com o Google Assistente.\n\nSIRI DA APPLE\nClique no botão M para iniciar e parar a Siri."
     - **ru**: "Многофункциональная M-кнопка на правой чашке наушников позволяет выбрать одну из трех настроек эквалайзера, а также быстро включить голосовой ассистент. \n Перейдите в меню настроек M-кнопки и выберите для нее эквалайзер, Google Assistant или Siri.\n\nЭКВАЛАЙЗЕР\n Выберите три любимые настройки эквалайзера. Нажимайте M-кнопку для переключения между ними.\n\n GOOGLE ASSISTANT\n Нажимайте M-кнопку для взаимодействия с Google Assistant. Подробнее о взаимодействии с Google Assistant см. приложение Google Assistant. \n\nAPPLE SIRI\n Для начала и прекращения работы Siri нажимайте M-кнопку."
     - **es**: "El botón M multifunción te permite cambiar entre tres ecualizadores predefinidos o acceder a tu asistente de voz.\nVete al menú de ajustes del botón M y defínelo para ecualizador, Asistente de voz de Google o Siri.\n\nECUALIZADOR\nElige tus 3 configuraciones de ecualizador favoritas. Haz clic en el botón M para cambiar entre ellas.\n\nASISTENTE DE GOOGLE\nUsa el botón M para interaccionar con el Asistente de Google. Consulta la aplicación Asistente de Google para ver cómo interaccionar con el Asistente de Google.\n\nAPPLE SIRI\nHaz clic en el botón M para empezar a hablar y para dejar de hablar con Siri."
     - **sv**: "Den multifunktionella M-knappen låter dig växla mellan tre EQ-förinställningar alternativt använda din röstassistent när du är på språng.\nGå till menyn för M-knappen och ställ in den för EQ, Google Assistent eller Siri.\n\nEQUALIZER\nVälj tre favoritinställningar för EQ. Klicka på M-knappen för att växla mellan dem.\n\nGOOGLE ASSISTENT\nAnvänd M-knappen för att prata med Google Assistent. Se Google Assistent-appen för hur man pratar med Google Assistent.\n\nAPPLE SIRI\nKlicka på M-knappen för att starta och stoppa Siri."
     - **fi**: "M-monitoimipainikkeella voit vahtaa kolmen taajuuskorjaimen esiasetuksen välillä tai vaihtaa ääniavustajaa lennosta.\nM-painikkeen asetusvalikossa aseta M-painike käyttämään taajuuskorjainta, Google Assistantia tai Siriä.\n\nTAAJUUSKORJAIN\nValitse kolme taajuuskorjaimen suosikkiasetustasi. Käytä M-painiketta vaihtaaksesi niiden välillä.\n\nGOOGLE ASSISTANT\nOle M-painikkeen avulla vuorovaikutuksessa Google Assistantin kanssa. Katso Google Assistant -sovelluksesta, kuinka voit olla vuorovaikutuksessa Google Assistantin kanssa.\n\nAPPLE SIRI\nNapsauta M-painiketta käynnistääksesi tai pysäyttääksesi Siri."
     - **zh-Hans**: "使用多功能 M-按钮可在三项均衡器预设之间进行切换，也可以随时随地访问语音助手。\n转到 M-按钮设置菜单，然后为均衡器、Google Assistant 或 Siri 设置 M-按钮。\n\n均衡器\n选择三项您喜欢的 EQ 设置。单击 M-按钮可在它们之间切换。\n\nGoogle Assistant\n使用 M-按钮与 Google Assistant 进行交互。请参阅 Google Assistant 应用程序，了解如何与 Google Assistant 进行交互。\n\nAPPLE SIRI\n单击 M-按钮以启动和停止 Siri。"
     - **zh-Hant**: "多功能 M 按鈕可讓您在三個等化器預設之間進行切換，或者隨時隨地使用語音助理。\n 轉到 M 按鈕設置選單，然後設定等化器、Google Assistant 或 Siri。\n\n 等化器 \n 選擇三個喜歡的 EQ 設置。單擊 M 按鈕在它們之間切換。\n\n Google Assistant \n 使用 M 按鈕與 Google Assistant 進行互動。請參閱 Google Assistant 應用程式以了解如何與 Google Assistant 互動。\n\n APPLE SIRI \n 單擊 M 按鈕以啟動和停止 Siri。"
     - **id**: "M-Button multifungsi dapat digunakan untuk beralih antara tiga preset equaliser atau mengakses asisten suara sembari beraktivitas.\nPergi ke menu pengaturan M-Button dan atur M-Button untuk Equaliser, Google Assistant  atau Siri.\n]nEQUALISER\nPilih tiga pengaturan EQ favorit. Klik M-Button untuk beralih antara preset equaliser favorit Anda.\n\nGOOGLE ASSISTANT\nGunakan M-Button untuk berinteraksi dengan Google Assistant. Buka aplikasi Google Assistant untuk mengetahui cara berinteraksi dengan Google Assistant.\n\nAPPLE SIRI\nKlik M-Button untuk memulai dan menghentikan Siri."
     - **fil**: "Ang mutli-function na M-button ay hahayaan kang lumipat sa pagitan ng tatlong equaliser preset o bilang alternatibo, i-access ang iyong voice assistant on-the-go.\nPumunta sa mga setting menu ng M-Button at itakda ang M-button para sa Equaliser, Google Assistant o Siri.\n\nEQALISER\nPumili ng tatlong paboritong settings ng EQ. Pindutin ang M-Button upang mag-toggle sa pagitan ng mga ito.\n\nGOOGLE ASSISTANT\nGamitin ang M-Button upang mag-interact sa Google Assistant. Tingnan ang Google Assistant app para sa kung paano mag-interact sa Google Assistant.\n\nAPPLE SIRI\nPindutin ang M-Button upang magsimula at huminto sa Siri."
     - **ja**: "マルチ機能のMボタンは、3つのイコライザープリセット間を切り替え、または起動中の音声アシスタントへのアクセスを可能にします。●Mボタン設定メニューを開き、Mボタンをイコライザー、Google Voice、Amazon AlexaまたはSiriへ設定してください。●●イコライザー●3つのお好みのEQ設定を選択する。Mボタンをクリックして、イコライザー間を切り替える。●●GOOGLEアシスタント●Mボタンを使用して、Googleアシスタントと会話する。Googleアシスタントとの会話方法はGoogleアシスタントアプリを参照してください。●●APPLE SIRI●Mボタンをクリックして、Siriを開始／停止する。"
 */
  public static func quick_guide_ozzy_item_m_button() -> String {
     return localizedString(
         key:"quick_guide_ozzy_item_m_button",
         defaultValue:"The mutli-function M-button lets you switch between three equaliser presets or alternatively, access your voice assistant on-the-go.\nGo to the M-Button settings menu and set the M-button for Equaliser, Google Assistant or Siri.\n\nEQALISER\nChoose three favourite EQ settings. Click the M-Button to toggle between them.\n\nGOOGLE ASSISTANT\nUse the M-Button to interact with the Google Assistant. See the Google Assistant app for how to interact with the Google Assistant.\n\nAPPLE SIRI\nClick the M-Button to start and stop Siri.",
         substitutions: [:])
  }

 /**
"Single-click to either play or pause."

     - **en**: "Single-click to either play or pause."
     - **da**: "Klik én gang for at afspille eller sætte på pause."
     - **nl**: "Eén klik om te spelen of te pauzeren."
     - **de**: "Drücken Sie einmal, um Musik abzuspielen oder anzuhalten."
     - **fr**: "Appuyez une seule fois pour lire ou mettre en pause."
     - **it-IT**: "Premi una volta per avviare la riproduzione o metterla in pausa."
     - **nb**: "Klikk én gang for å spille av eller sette på pause."
     - **pt-PT**: "Carregue uma vez para reproduzir o áudio ou colocá-lo em pausa."
     - **ru**: "Нажмите один раз, чтобы воспроизвести или поставить на паузу."
     - **es**: "Pulsa una vez el botón para reproducir o detener la música."
     - **sv**: "Klicka en gång för att spela eller pausa."
     - **fi**: "Yksi napsautus toistaa tai keskeyttää äänen."
     - **zh-Hans**: "单击播放或暂停"
     - **zh-Hant**: "按一下以播放或暫停。"
     - **id**: "Klik sekali untuk memutar atau menjeda."
     - **fil**: "Isang beses na pag-click upang mag-play o pause."
     - **ja**: "一度のクリックで、再生、あるいは一時停止します。"
 */
  public static func quick_guide_playpause_part1() -> String {
     return localizedString(
         key:"quick_guide_playpause_part1",
         defaultValue:"Single-click to either play or pause.",
         substitutions: [:])
  }

 /**
"Double-click to skip forward."

     - **en**: "Double-click to skip forward."
     - **da**: "Klik to gange for at springe fremad."
     - **nl**: "Twee keer klikken om verder te gaan."
     - **de**: "Drücken Sie zweimal, um vorwärts zu springen."
     - **fr**: "Appuyez deux fois pour passer à la piste suivante."
     - **it-IT**: "Premi due volte per saltare alla traccia successiva."
     - **nb**: "Dobbeltklikk for å hoppe fremover."
     - **pt-PT**: "Carregue duas vezes para avançar."
     - **ru**: "Нажмите два раза, чтобы прокрутить вперед."
     - **es**: "Pulsa dos veces para saltar un tema."
     - **sv**: "Dubbelklicka för att hoppa framåt."
     - **fi**: "Kaksoisnapsautus hyppää eteenpäin."
     - **zh-Hans**: "双击播放下一首"
     - **zh-Hant**: "按兩下以跳至下一首。"
     - **id**: "Klik dua kali untuk maju."
     - **fil**: "Dalawang beses na pag-click upang lumaktaw pasulong."
     - **ja**: "ダブルクリックで、早送りします。"
 */
  public static func quick_guide_playpause_part2() -> String {
     return localizedString(
         key:"quick_guide_playpause_part2",
         defaultValue:"Double-click to skip forward.",
         substitutions: [:])
  }

 /**
"Triple-click to skip back."

     - **en**: "Triple-click to skip back."
     - **da**: "Klik tre gange for at springe tilbage"
     - **nl**: "Drie keer klikken om terug te gaan"
     - **de**: "Drücken Sie dreimal, um rückwärts zu springen"
     - **fr**: "Appuyez trois fois pour revenir à la piste précédente."
     - **it-IT**: "Premi tre volte per tornare alla traccia precedente"
     - **nb**: "Trippelklikk for å hoppe bakover"
     - **pt-PT**: "Carregue três vezes para retroceder"
     - **ru**: "Нажмите три раза, чтобы перемотать назад"
     - **es**: "Pulsa tres veces para volver al tema anterior."
     - **sv**: "Trippelklicka för att hoppa bakåt"
     - **fi**: "Kolmoisnapsautus hyppää taaksepäin"
     - **zh-Hans**: "三击播放上一首"
     - **zh-Hant**: "按三下以跳至上一首"
     - **id**: "Klik tiga kali untuk mundur"
     - **fil**: "Tatlong beses na pag-click upang lumaktaw pabalik"
     - **ja**: "三回クリックするとスキップします"
 */
  public static func quick_guide_playpause_part3() -> String {
     return localizedString(
         key:"quick_guide_playpause_part3",
         defaultValue:"Triple-click to skip back.",
         substitutions: [:])
  }

 /**
"Double-click and hold to fast forward."

     - **en**: "Double-click and hold to fast forward."
     - **da**: "Klik to gange og hold inde for at spole frem"
     - **nl**: "Twee keer klikken en vasthouden om snel vooruit te spoelen"
     - **de**: "Zum Vorspulen doppelklicken und halten."
     - **fr**: "Appuyez deux fois et maintenez pour l’avance rapide."
     - **it-IT**: "Fai doppio click e tieni premuto per l’avanzamento veloce"
     - **nb**: "Dobbeltklikk og hold nede for å spole fremover"
     - **pt-PT**: "Carregue duas vezes e mantenha pressionado para avançar rapidamente"
     - **ru**: "Нажмите два раза и удерживайте кнопку для ускоренного перехода"
     - **es**: "Pulsa dos veces y mantén pulsado para avanzar"
     - **sv**: "Dubbelklicka och håll för att spola framåt"
     - **fi**: "Kaksoisnapsautus ja alhaalla pito kelaa eteenpäin"
     - **zh-Hans**: "双击并按住可快进"
     - **zh-Hant**: "按兩下並按住以快轉"
     - **id**: "Klik dua kali dan tahan untuk mempercepat"
     - **fil**: "I-click ng dalawang beses at hawakang nakapindot para mag-fast forward"
     - **ja**: "ダブルクリックして長押しすると、早送りできます"
 */
  public static func quick_guide_playpause_part4() -> String {
     return localizedString(
         key:"quick_guide_playpause_part4",
         defaultValue:"Double-click and hold to fast forward.",
         substitutions: [:])
  }

 /**
"Triple-click and hold to rewind."

     - **en**: "Triple-click and hold to rewind."
     - **da**: "Klik tre gange og hold inde for spole tilbage"
     - **nl**: "Drie keer klikken en vasthouden om terug te spoelen"
     - **de**: "Zum Zurückspulen dreimal klicken und halten."
     - **fr**: "Appuyez trois fois et maintenez pour le retour rapide."
     - **it-IT**: "Fai triplo click e tieni premuto per tornare indietro"
     - **nb**: "Trippelklikk og hold nede for å spole tilbake"
     - **pt-PT**: "Carregue três vezes e mantenha pressionado para retroceder"
     - **ru**: "Нажмите три раза для обратной перемотки "
     - **es**: "Pulsa tres veces y mantén pulsado para retroceder"
     - **sv**: "Trippelklicka och håll för att spola tillbaka"
     - **fi**: "Kolmoisnapsautus ja alhaalla pito kelaa taaksepäin"
     - **zh-Hans**: "三击并按住可快退"
     - **zh-Hant**: "按三下並按住以倒轉"
     - **id**: "Klik tiga kali dan tahan untuk mundur cepat"
     - **fil**: "I-click ng tatlong beses at hawakang nakapindot para sa pag-rewind"
     - **ja**: "三回クリックして長押しすると、巻き戻しできます"
 */
  public static func quick_guide_playpause_part5() -> String {
     return localizedString(
         key:"quick_guide_playpause_part5",
         defaultValue:"Triple-click and hold to rewind.",
         substitutions: [:])
  }

 /**
"The play/pause button on the top panel can control the music when playing via Bluetooth."

     - **en**: "The play/pause button on the top panel can control the music when playing via Bluetooth."
     - **da**: "Afspilnings-/pauseknappen på toppanelet kan kontrollere musikken, når der afspilles via Bluetooth"
     - **nl**: "De afspeel-/pauzeknop op het bovenpaneel kan de muziek bedienen tijdens het afspelen via Bluetooth"
     - **de**: "Die Taste für Wiedergabe/Pause auf dem oberen Bedienfeld kann bei Wiedergabe über Bluetooth die Musik steuern."
     - **fr**: "Le bouton Lecture/Pause du panneau haut peut contrôler la musique lors de la lecture via Bluetooth."
     - **it-IT**: "Il pulsante play/pausa sul pannello superiore controlla la musica durante la riproduzione via Bluetooth"
     - **nb**: "Spill av-/pause-knappen på toppanelet kan styre musikken når du spiller via Bluetooth"
     - **pt-PT**: "O botão reproduzir/pausa no painel superior pode controlar a música quando reproduzir via Bluetooth"
     - **ru**: "Проигрывание музыки через Bluetooth регулируется кнопкой «Воспроизведение/пауза»"
     - **es**: "El botón de reproducir/detener incluido en el panel superior permite controlar la música que se reproduce por Bluetooth"
     - **sv**: "Uppspelnings-/pausknappen på toppanelen kan kontrollera musiken när du spelar via Bluetooth"
     - **fi**: "Yläpaneelin Toista/Tauko-painikkeesta hallitaan musiikkia, kun soitetaan Bluetoothin kautta"
     - **zh-Hans**: "蓝牙连接时，可通过顶部面板上的播放/暂停按钮控制音乐"
     - **zh-Hant**: "在透過藍牙播放時，頂部面板的播放/暫停按鈕可控制音樂"
     - **id**: "Tombol putar/jeda pada panel atas dapat mengontrol musik saat memutar melalui Bluetooth"
     - **fil**: "Ang play/pause button sa itaas na panel ay makakakontrol sa musika kapag pinatutugtog sa pamamagitan ng Bluetooth"
     - **ja**: "トップパネル上の再生／一時停止ボタンによって、Bluetoothを介して再生中の音楽のコントロールが可能です。"
 */
  public static func quick_guide_playpause_subtitle() -> String {
     return localizedString(
         key:"quick_guide_playpause_subtitle",
         defaultValue:"The play/pause button on the top panel can control the music when playing via Bluetooth.",
         substitutions: [:])
  }

 /**
"%d characters left"

     - **en**: "%d characters left"
     - **da**: "%d tegn tilbage"
     - **nl**: "%d resterende tekens"
     - **de**: "%d Zeichen verbleiben"
     - **fr**: "%d caractères restants"
     - **it-IT**: "%d caratteri rimasti"
     - **nb**: "%d tegn igjen"
     - **pt-PT**: "%d caracteres restantes"
     - **ru**: "Осталось %d символов"
     - **es**: "Quedan %d caracteres"
     - **sv**: "%d tecken kvar"
     - **fi**: "%d merkkiä jäljellä"
     - **zh-Hans**: "剩余 %d 个字符"
     - **zh-Hant**: "剩餘 %d 個字元"
     - **id**: "%d karakter yang tersisa"
     - **fil**: "%d mga karakter na natira"
     - **ja**: "残り文字数：%d文字"
 */
  public static func rename_screen_characters_left() -> String {
     return localizedString(
         key:"rename_screen_characters_left",
         defaultValue:"%d characters left",
         substitutions: [:])
  }

 /**
"Name is too long"

     - **en**: "Name is too long"
     - **da**: "Navnet er for langt"
     - **nl**: "Naam is te lang"
     - **de**: "Der Name ist zu lang"
     - **fr**: "Le nom est trop long"
     - **it-IT**: "Il nome è troppo lungo"
     - **nb**: "Navnet er for langt"
     - **pt-PT**: "O nome é demasiado longo"
     - **ru**: "Слишком длинное название"
     - **es**: "El nombre es demasiado largo"
     - **sv**: "Namnet är för långt"
     - **fi**: "Nimi on liian pitkä"
     - **zh-Hans**: "名称太长"
     - **zh-Hant**: "名稱過長"
     - **id**: "Nama terlalu panjang"
     - **fil**: "Masyadong mahaba ang pangalan"
     - **ja**: "名前が長すぎます"
 */
  public static func rename_screen_name_too_long() -> String {
     return localizedString(
         key:"rename_screen_name_too_long",
         defaultValue:"Name is too long",
         substitutions: [:])
  }

 /**
"No characters left"

     - **en**: "No characters left"
     - **da**: "Ingen tegn tilbage"
     - **nl**: "Geen resterende tekens"
     - **de**: "Keine Zeichen verbleiben"
     - **fr**: "Aucun caractère restant"
     - **it-IT**: "Nessun carattere rimasto"
     - **nb**: "Ingen tegn igjen"
     - **pt-PT**: "Nenhum caractere restante"
     - **ru**: "Не осталось символов"
     - **es**: "No quedan caracteres"
     - **sv**: "Inga tecken kvar"
     - **fi**: "Merkkejä ei ole jäljellä"
     - **zh-Hans**: "未剩余字符"
     - **zh-Hant**: "沒有剩餘的字元"
     - **id**: "Tidak ada karakter yang tersisa"
     - **fil**: "Walang natirang karakter"
     - **ja**: "これ以上入力できません"
 */
  public static func rename_screen_reached_limit() -> String {
     return localizedString(
         key:"rename_screen_reached_limit",
         defaultValue:"No characters left",
         substitutions: [:])
  }

 /**
"Partake in the quality program. Anonymous usage statistics will be sent to help Marshall to improve this product."

     - **en**: "Partake in the quality program. Anonymous usage statistics will be sent to help Marshall to improve this product."
     - **da**: "Deltag i kvalitetsprogrammet. Der vil blive sendt anonym brugerstatistik for at hjælpe Marshall med at forbedre dette produkt."
     - **nl**: "Deelnemen aan het kwaliteitsprogramma. Er zullen anonieme gebruiksstatistieken worden verzonden om Marshall te helpen dit product te verbeteren."
     - **de**: "Am Qualitätsprogramm teilnehmen. Anonyme Nutzungsstatistiken werden gesendet, um Marshall bei der Verbesserung dieses Produkts zu unterstützen."
     - **fr**: "Participez à notre programme qualité. Des statistiques anonymes d’utilisation seront envoyées à Marshall à des fins d’amélioration de ce produit."
     - **it-IT**: "Partecipa al programma qualità. Marshall riceverà delle statistiche anonime sull’utilizzo del prodotto per poterlo migliorare."
     - **nb**: "Ta del i kvalitetsprogrammet. Anonym bruksstatistikk vil bli sendt for å hjelpe Marshall med å forbedre dette produktet."
     - **pt-PT**: "Participar no programa de qualidade. Estatísticas de utilização anónimas serão enviadas para ajudar a Marshall a melhorar este produto."
     - **ru**: "Примите участие в нашей программе обеспечения и контроля качества. Marshall будет получать анонимную статистику использования с целью дальнейшего улучшения качества этого продукта."
     - **es**: "Participa en el programa de calidad. Se enviarán datos estadísticos de uso anónimos para ayudar a Marshall a mejorar este producto."
     - **sv**: "Delta i kvalitetsprogrammet. Anonym användarstatistik skickas för att hjälpa Marshall att förbättra denna produkt."
     - **fi**: "Osallistu laatuohjelmaan. Anonyymejä käyttötilastoja tullaan lähettämään, jotta Marshall voi parantaa tätä tuotetta."
     - **zh-Hans**: "系统会将用户的使用数据以匿名形式自动发送 以帮助Marshall改进相关产品。"
     - **zh-Hant**: "參加品質計劃。將傳送匿名使用統計資料以協助 Marshall 改善此產品。"
     - **id**: "Ikut serta dalam program kualitas. Statistik penggunaan anonim akan dikirimkan untuk membantu Marshall meningkatkan produk ini."
     - **fil**: "Sumali sa programa ng kalidad. Ang walang pagkakakilanlan na istatistiko ng paggamit ay ipadadala upang matulungan ang Marshall na mapabuti ang produktong ito."
     - **ja**: "品質プログラムに参加する。Marshallが本製品の改善に役立てることができるよう、匿名形式で利用統計情報が送信されます。"
 */
  public static func screen_analytics_body() -> String {
     return localizedString(
         key:"screen_analytics_body",
         defaultValue:"Partake in the quality program. Anonymous usage statistics will be sent to help Marshall to improve this product.",
         substitutions: [:])
  }

 /**
"Data Collection"

     - **en**: "Data Collection"
     - **da**: "Dataindsamling"
     - **nl**: "Gegevensverzameling"
     - **de**: "Datenerfassung"
     - **fr**: "Collecte des données"
     - **it-IT**: "Raccolta dati"
     - **nb**: "Datainnhenting"
     - **pt-PT**: "Recolha de dados"
     - **ru**: "Сбор данных"
     - **es**: "Recopilación de datos"
     - **sv**: "Datainsamling"
     - **fi**: "Datan kerääminen"
     - **zh-Hans**: "数据收集"
     - **zh-Hant**: "資料收集"
     - **id**: "Pengumpulan Data"
     - **fil**: "Koleksyon ng Datos"
     - **ja**: "データの収集"
 */
  public static func screen_analytics_switcher() -> String {
     return localizedString(
         key:"screen_analytics_switcher",
         defaultValue:"Data Collection",
         substitutions: [:])
  }

 /**
"ANALYTICS"

     - **en**: "ANALYTICS"
     - **da**: "ANALYSE"
     - **nl**: "ANALYSES"
     - **de**: "ANALYSE"
     - **fr**: "ANALYSES"
     - **it-IT**: "ANALISI"
     - **nb**: "ANALYSE"
     - **pt-PT**: "ANÁLISE"
     - **ru**: "АНАЛИТИКА"
     - **es**: "ANALÍTICA"
     - **sv**: "ANALYS"
     - **fi**: "ANALYTIIKKA"
     - **zh-Hans**: "分析"
     - **zh-Hant**: "分析"
     - **id**: "ANALITIK"
     - **fil**: "ANALYTICS"
     - **ja**: "分析"
 */
  public static func screen_analytics_title_uc() -> String {
     return localizedString(
         key:"screen_analytics_title_uc",
         defaultValue:"ANALYTICS",
         substitutions: [:])
  }

 /**
"Select An Accessory"

     - **en**: "Select An Accessory"
     - **da**: "Vælg et tilbehør"
     - **nl**: "Selecteer een accessoire"
     - **de**: "Wähle ein Accessoire"
     - **fr**: "Sélectionnez un accessoire"
     - **it-IT**: "Seleziona un accessorio"
     - **nb**: "Velg et tilbehør"
     - **pt-PT**: "Selecione Um Acessório"
     - **ru**: "Выбрать аксессуар"
     - **es**: "Seleccionar un accesorio"
     - **sv**: "Välj en accessoar"
     - **fi**: "Valitse lisävaruste"
     - **zh-Hans**: "选择一个配件"
     - **zh-Hant**: "選擇一個配件"
     - **id**: "Pilih Aksesori"
     - **fil**: "Pumili ng Aksesorya"
     - **ja**: "周辺機器を選択する"
 */
  public static func select_an_accessory_header_uc() -> String {
     return localizedString(
         key:"select_an_accessory_header_uc",
         defaultValue:"Select An Accessory",
         substitutions: [:])
  }

 /**
"Feature interactions to make the most used features easier and better."

     - **en**: "Feature interactions to make the most used features easier and better."
     - **da**: "Bruger af funktioner for at gøre de mest brugte funktioner nemmere at bruge og bedre."
     - **nl**: "Interacties met functies om de meest gebruikte functies eenvoudiger en beter te maken."
     - **de**: "Nutzung von Funktionen, um die am häufigsten genutzten Funktionen einfacher und besser zu gestalten."
     - **fr**: "Interactions des fonctionnalités afin d’améliorer et simplifier les fonctionnalités les plus utilisées."
     - **it-IT**: "L’interazione con le diverse funzionalità, per migliorare e semplificare le più utilizzate."
     - **nb**: "Funksjonsinteraksjoner for å gjøre de mest brukte funksjonene enklere og bedre."
     - **pt-PT**: "Interações de funcionalidades para melhorar e tornar mais fáceis as funcionalidades mais utilizadas."
     - **ru**: "взаимодействие функций для упрощения и оптимизации наиболее популярных функций;"
     - **es**: "Interacciones con las diferentes funciones para que las más usadas sean mejores y más fáciles de usar."
     - **sv**: "Funktionsinteraktioner för att göra de mest använda funktionerna enklare och bättre."
     - **fi**: "Ominaisuuden interaktioita tehdäkseen käytetyimmistä ominaisuuksista helppokäyttöisempiä ja parempia."
     - **zh-Hans**: "交互特征使得常用功能更加简单和完善"
     - **zh-Hant**: "功能互動，讓最常用的功能運作更好且更易於使用。"
     - **id**: "Interaksi fitur untuk menjadikan fitur yang paling sering digunakan lebih mudah dan lebih baik."
     - **fil**: "Pagtampok ng mga interaksiyon para mas mapadali at mas mapaganda ang pinakamadalas gamiting mga tampok."
     - **ja**: "機能の利用情報ーー最も利用されている機能をより使いやすく、より良いものとするため。"
 */
  public static func share_data_screen_content_part_1() -> String {
     return localizedString(
         key:"share_data_screen_content_part_1",
         defaultValue:"Feature interactions to make the most used features easier and better.",
         substitutions: [:])
  }

 /**
"Connection to other devices to better understand what products to support."

     - **en**: "Connection to other devices to better understand what products to support."
     - **da**: "Forbindelse til andre enheder for bedre at forstå, hvilke produkter der skal understøttes."
     - **nl**: "Verbinding met andere apparaten om meer inzicht te krijgen in de te ondersteunen producten."
     - **de**: "Verbindung mit anderen Geräten, um zu erfahren, welche Produkte unterstützt werden sollen."
     - **fr**: "Connexion à d’autres appareils pour mieux comprendre quels types de produits rendre compatibles."
     - **it-IT**: "La connessione ad altri dispositivi per meglio comprendere quali prodotti vanno supportati."
     - **nb**: "Tilkobling til andre enheter for bedre å forstå hvilke produkter som skal støttes."
     - **pt-PT**: "Ligação a outros dispositivos para compreender melhor que produtos deve suportar."
     - **ru**: "соединение с другими устройствами для лучшего понимания того, какие продукты требуют поддержки;"
     - **es**: "Conexión con otros dispositivos, para comprender mejor de qué productos necesitamos ofrecer asistencia técnica."
     - **sv**: "Anslutningar till andra enheter för att bättre förstå vilka produkter som ska stödjas."
     - **fi**: "Yhteyttä muihin laitteisiin ymmärtääkseen paremmin, mitä tuotteita tukea."
     - **zh-Hans**: "设备连接状况以更好地支持相关设备"
     - **zh-Hant**: "與其他裝置的連線，以更了解要支援的產品。"
     - **id**: "Koneksi ke perangkat lain untuk lebih memahami produk yang didukung."
     - **fil**: "Pagkonekta sa mga ibang device para mas mabuting maunawaan kung anong mga produkto ang susuportahan."
     - **ja**: "他のデバイスへの接続情報ーーサポートすべき製品をより理解するため。"
 */
  public static func share_data_screen_content_part_2() -> String {
     return localizedString(
         key:"share_data_screen_content_part_2",
         defaultValue:"Connection to other devices to better understand what products to support.",
         substitutions: [:])
  }

 /**
"System stability to prevent future crashes and system failure."

     - **en**: "System stability to prevent future crashes and system failure."
     - **da**: "Systemstabilitet for forhindre fremtidige sammenbrud og systemfejl."
     - **nl**: "Stabiliteit van het systeem om toekomstige crashes en systeemstoringen te voorkomen."
     - **de**: "Systemstabilität, um Programmabstürze und Systemausfälle in Zukunft zu vermeiden."
     - **fr**: "Stabilité du système afin d’éviter les pannes et défaillances système."
     - **it-IT**: "La stabilità del sistema per prevenire futuri blocchi ed errori."
     - **nb**: "Systemstabilitet for å hindre fremtidige krasj og systemfeil."
     - **pt-PT**: "A estabilidade do sistema para evitar crashes futuros e falhas no sistema."
     - **ru**: "стабильность системы для предотвращения ее сбоев и отказов в будущем."
     - **es**: "Estabilidad del sistema para prevenir caídas del sistema y averías."
     - **sv**: "Systemstabilitet för att förhindra framtida kraschar och systemfel."
     - **fi**: "Järjestelmän vakautta tulevien kaatumisten ja järjestelmävirheiden estämiseksi."
     - **zh-Hans**: "系统的稳定性以防止出现崩溃和系统故障"
     - **zh-Hant**: "系統穩定性，以防止未來損壞及系統故障。"
     - **id**: "Stabilitas sistem untuk mencegah kerusakan dan kegagalan sistem pada masa mendatang."
     - **fil**: "Katatagan ng sistema upang maiwasan ang mga pag-crash sa hinaharap at pagpalya ng system."
     - **ja**: "システムの安定性ーーさらなるクラッシュおよびシステムエラーを防止するため。"
 */
  public static func share_data_screen_content_part_3() -> String {
     return localizedString(
         key:"share_data_screen_content_part_3",
         defaultValue:"System stability to prevent future crashes and system failure.",
         substitutions: [:])
  }

 /**
"Marshall analyzes non-personal information such as:"

     - **en**: "Marshall analyzes non-personal information such as:"
     - **da**: "Marshall analyserer ikke-personlige oplysninger som:"
     - **nl**: "Marshall analyseert niet-persoonlijke informatie zoals:"
     - **de**: "Marshall analysiert nicht-persönliche Daten wie:"
     - **fr**: "Marshall analyse les informations non-personnelles telles que :"
     - **it-IT**: "Marshall analizza dati di carattere non personale come:"
     - **nb**: "Marshall analyserer ikke-personlig informasjon som:"
     - **pt-PT**: "A Marshall analisa informações não pessoais, tais como:"
     - **ru**: "Marshall анализирует следующую информацию, не содержащую персональных данных:"
     - **es**: "Marshall analiza datos no personales, como por ejemplo:"
     - **sv**: "Marshall analyserar icke-personliga uppgifter som:"
     - **fi**: "Marshall analysoi muita kuin henkilötietoja, kuten:"
     - **zh-Hans**: "Marshall 将分析下列非个人信息："
     - **zh-Hant**: "Marshall 會分析非個人資訊，例如："
     - **id**: "Marshall menganalisis informasi non-pribadi, seperti:"
     - **fil**: "Sinusuri ng Marshall ang hindi personal na impormasyon tulad ng:"
     - **ja**: "Marshallは次のような非個人情報を分析します："
 */
  public static func share_data_screen_content_title() -> String {
     return localizedString(
         key:"share_data_screen_content_title",
         defaultValue:"Marshall analyzes non-personal information such as:",
         substitutions: [:])
  }

 /**
"I want to share my anonymous data"

     - **en**: "I want to share my anonymous data"
     - **da**: "Jeg vil gerne dele mine anonyme data"
     - **nl**: "Ik wil mijn anonieme gegevens delen"
     - **de**: "Ich möchte meine anonymen Daten teilen"
     - **fr**: "Je souhaite partager mes données anonymes."
     - **it-IT**: "Desidero condividere i miei dati in forma anonima"
     - **nb**: "Jeg vil dele mine anonyme data"
     - **pt-PT**: "Quero partilhar os meus dados anónimos"
     - **ru**: "Я хочу передавать свои анонимные данные"
     - **es**: "Quiero compartir mis datos anónimos"
     - **sv**: "Jag vill dela mina anonyma uppgifter"
     - **fi**: "Haluan jakaa anonyymit tietoni"
     - **zh-Hans**: "我想共享我的匿名数据"
     - **zh-Hant**: "我想分享我的匿名資料"
     - **id**: "Saya ingin berbagi data anonim saya"
     - **fil**: "Gusto kong ibahagi ang aking walang pagkakakilanlang datos"
     - **ja**: "私は自分の匿名データを共有することを希望します"
 */
  public static func share_data_screen_i_want_to_share() -> String {
     return localizedString(
         key:"share_data_screen_i_want_to_share",
         defaultValue:"I want to share my anonymous data",
         substitutions: [:])
  }

 /**
"I want to share anonymous data"

     - **en**: "I want to share anonymous data"
     - **da**: "Jeg vil gerne dele anonyme data"
     - **nl**: "Ik wil anonieme gegevens delen"
     - **de**: "Ich möchte anonyme Daten teilen"
     - **fr**: "Je souhaite partager des données anonymes"
     - **it-IT**: "Desidero condividere i dati in forma anonima"
     - **nb**: "Jeg vil dele mine anonyme data"
     - **pt-PT**: "Quero partilhar dados anónimos"
     - **ru**: "Я хочу передавать анонимные данные"
     - **es**: "Quiero compartir datos anónimos"
     - **sv**: "Jag vill dela anonyma data"
     - **fi**: "Haluan jakaa anonyymeja tietoja"
     - **zh-Hans**: "我想共享匿名数据"
     - **zh-Hant**: "我想分享匿名資料"
     - **id**: "Saya ingin membagikan data anonim"
     - **fil**: "Gusto kong magbahagi ng anonimo na datos"
     - **ja**: "私は匿名データを共有することを希望します"
 */
  public static func share_data_screen_i_want_to_share_v1() -> String {
     return localizedString(
         key:"share_data_screen_i_want_to_share_v1",
         defaultValue:"I want to share anonymous data",
         substitutions: [:])
  }

 /**
"Partake in the quality program.\nAnonymous usage statistics will be sent to help Marshall to improve this product."

     - **en**: "Partake in the quality program.\nAnonymous usage statistics will be sent to help Marshall to improve this product."
     - **da**: "Deltag i kvalitetsprogrammet.\nDer vil blive sendt anonym brugerstatistik for at hjælpe Marshall med at forbedre dette produkt."
     - **nl**: "Deelnemen aan het kwaliteitsprogramma.\nEr zullen anonieme gebruiksstatistieken worden verzonden om Marshall te helpen dit product te verbeteren."
     - **de**: "Am Qualitätsprogramm teilnehmen.\nAnonyme Nutzungsstatistiken werden gesendet, um Marshall bei der Verbesserung dieses Produkts zu unterstützen."
     - **fr**: "Participez à notre programme qualité.\nDes statistiques anonymes d’utilisation seront envoyées à Marshall à des fins d’amélioration de ce produit."
     - **it-IT**: "Partecipa al programma qualità.\nMarshall riceverà delle statistiche anonime sull’utilizzo del prodotto per poterlo migliorare."
     - **nb**: "Ta del i kvalitetsprogrammet.\nAnonym bruksstatistikk vil bli sendt for å hjelpe Marshall med å forbedre dette produktet."
     - **pt-PT**: "Participar no programa de qualidade.\nEstatísticas de utilização anónimas serão enviadas para ajudar a Marshall a melhorar este produto."
     - **ru**: "Примите участие в нашей программе обеспечения и контроля качества.\nMarshall будет получать анонимную статистику использования с целью дальнейшего улучшения качества этого продукта."
     - **es**: "Participa en el programa de calidad.\nSe enviarán datos estadísticos de uso anónimos para ayudar a Marshall a mejorar este producto."
     - **sv**: "Delta i kvalitetsprogrammet.\nAnonym användarstatistik skickas för att hjälpa Marshall att förbättra denna produkt."
     - **fi**: "Osallistu laatuohjelmaan.\nAnonyymejä käyttötilastoja tullaan lähettämään, jotta Marshall voi parantaa tätä tuotetta."
     - **zh-Hans**: "系统会将用户的使用数据以匿名形式自动发送\n以帮助Marshall改进相关产品。"
     - **zh-Hant**: "參加品質計劃。\n將傳送匿名使用統計資料以協助 Marshall 改善此產品。"
     - **id**: "Ikut serta dalam program kualitas.\nStatistik penggunaan anonim akan dikirimkan untuk membantu Marshall meningkatkan produk ini."
     - **fil**: "Sumali sa programa ng kalidad.\nAng walang pagkakakilanlan na istatistiko ng paggamit ay ipadadala upang matulungan ang Marshall na mapabuti ang produktong ito."
     - **ja**: "品質プログラムに参加する。\nMarshallが本製品の改善に役立てることができるよう、匿名形式で利用統計情報が送信されます。"
 */
  public static func share_data_screen_subtitle() -> String {
     return localizedString(
         key:"share_data_screen_subtitle",
         defaultValue:"Partake in the quality program.\nAnonymous usage statistics will be sent to help Marshall to improve this product.",
         substitutions: [:])
  }

 /**
"SHARE DATA"

     - **en**: "SHARE DATA"
     - **da**: "DEL DATA"
     - **nl**: "GEGEVENS DELEN"
     - **de**: "DATEN TEILEN"
     - **fr**: "PARTAGER LES DONNÉES"
     - **it-IT**: "CONDIVIDI I DATI"
     - **nb**: "DEL DATA"
     - **pt-PT**: "PARTILHAR DADOS"
     - **ru**: "ПОДЕЛИТЬСЯ ДАННЫМИ"
     - **es**: "COMPARTIR DATOS"
     - **sv**: "DELA UPPGIFTER"
     - **fi**: "JAA TIEDOT"
     - **zh-Hans**: "参与改进计划"
     - **zh-Hant**: "分享資料"
     - **id**: "BAGIKAN DATA"
     - **fil**: "MAGBAHAGI NG DATOS"
     - **ja**: "データの共有"
 */
  public static func share_data_screen_title_uc() -> String {
     return localizedString(
         key:"share_data_screen_title_uc",
         defaultValue:"SHARE DATA",
         substitutions: [:])
  }

 /**
"UNSUBSCRIBE"

     - **en**: "UNSUBSCRIBE"
     - **da**: "FRAMELD"
     - **nl**: "AFMELDEN"
     - **de**: "ABMELDEN"
     - **fr**: "SE DÉSINSCRIRE"
     - **it-IT**: "CANCELLA L’ISCRIZIONE"
     - **nb**: "AVSLUTTE ABONNEMENTET"
     - **pt-PT**: "CANCELAR SUBSCRIÇÃO"
     - **ru**: "ОТПИСАТЬСЯ"
     - **es**: "ANULAR SUSCRIPCIÓN"
     - **sv**: "AVSLUTA PRENUMERATION"
     - **fi**: "PERUUTA TILAUS"
     - **zh-Hans**: "取消订阅"
     - **zh-Hant**: "取消訂閱"
     - **id**: "BERHENTI BERLANGGANAN"
     - **fil**: "MAG-UNSUBSCRIBE"
     - **ja**: "登録解除"
 */
  public static func share_data_screen_unsubscribe_uc() -> String {
     return localizedString(
         key:"share_data_screen_unsubscribe_uc",
         defaultValue:"UNSUBSCRIBE",
         substitutions: [:])
  }

 /**
"ANC"

     - **en**: "ANC"
     - **da**: "ANC"
     - **nl**: "ANC"
     - **de**: "ANC"
     - **fr**: "ANC"
     - **it-IT**: "ANC"
     - **nb**: "ANC"
     - **pt-PT**: "ANC"
     - **ru**: "ANC"
     - **es**: "ANC"
     - **sv**: "Aktiv brusreducering"
     - **fi**: "VASTAMELU"
     - **zh-Hans**: "ANC"
     - **zh-Hant**: "ANC"
     - **id**: "ANC"
     - **fil**: "ANC"
     - **ja**: "ANC"
 */
  public static func sounds_screen_anc() -> String {
     return localizedString(
         key:"sounds_screen_anc",
         defaultValue:"ANC",
         substitutions: [:])
  }

 /**
"M-Button"

     - **en**: "M-Button"
     - **da**: "M-Button"
     - **nl**: "M-knop"
     - **de**: "M-Taste"
     - **fr**: "Bouton M"
     - **it-IT**: "Pulsante M"
     - **nb**: "M-knapp"
     - **pt-PT**: "Botão M"
     - **ru**: "M-кнопка"
     - **es**: "Botón M"
     - **sv**: "M-knapp"
     - **fi**: "M-Button-painike"
     - **zh-Hans**: "M-按钮"
     - **zh-Hant**: "M 按鈕"
     - **id**: "M-Button"
     - **fil**: "M-Button"
     - **ja**: "Mボタン"
 */
  public static func sounds_screen_m_button() -> String {
     return localizedString(
         key:"sounds_screen_m_button",
         defaultValue:"M-Button",
         substitutions: [:])
  }

 /**
"Media Control"

     - **en**: "Media Control"
     - **da**: "Mediekontrol"
     - **nl**: "Mediabeheer"
     - **de**: "Media Control"
     - **fr**: "Contrôle média"
     - **it-IT**: "Controllo media"
     - **nb**: "Mediakontroll"
     - **pt-PT**: "Controlo dos média"
     - **ru**: "Управление мультимедийными устройствами"
     - **es**: "Control de medios"
     - **sv**: "Mediekontroll"
     - **fi**: "Median hallinta"
     - **zh-Hans**: "媒体控制提示音"
     - **zh-Hant**: "媒體控制"
     - **id**: "Kontrol Media"
     - **fil**: "Kontrol sa Media"
     - **ja**: "メディアコントロール"
 */
  public static func sounds_screen_media_control() -> String {
     return localizedString(
         key:"sounds_screen_media_control",
         defaultValue:"Media Control",
         substitutions: [:])
  }

 /**
"Power"

     - **en**: "Power"
     - **da**: "Strøm"
     - **nl**: "Vermogen"
     - **de**: "Leistung"
     - **fr**: "Alimentation"
     - **it-IT**: "Alimentazione"
     - **nb**: "Strøm"
     - **pt-PT**: "Potência"
     - **ru**: "Питание"
     - **es**: "Alimentación"
     - **sv**: "På och av"
     - **fi**: "Virta"
     - **zh-Hans**: "电源开关提示音"
     - **zh-Hant**: "電源"
     - **id**: "Daya"
     - **fil**: "Power"
     - **ja**: "電源"
 */
  public static func sounds_screen_power() -> String {
     return localizedString(
         key:"sounds_screen_power",
         defaultValue:"Power",
         substitutions: [:])
  }

 /**
"Power On/Off"

     - **en**: "Power On/Off"
     - **da**: "Tænd/sluk"
     - **nl**: "Aan/Uit"
     - **de**: "Ein-/Ausschalten"
     - **fr**: "Marche/arrêt"
     - **it-IT**: "Accensione e spegnimento  "
     - **nb**: "Strøm på/av"
     - **pt-PT**: "Ligar/Desligar"
     - **ru**: "Включение/выключение"
     - **es**: "Encendido/apagado"
     - **sv**: "Slå På/Av"
     - **fi**: "Virta päälle / pois päältä"
     - **zh-Hans**: "电源开/关"
     - **zh-Hant**: "電源開/關  "
     - **id**: "Hidupkan/Matikan"
     - **fil**: "I-power On/Off"
     - **ja**: "電源オン／オフ"
 */
  public static func sounds_screen_power_v1() -> String {
     return localizedString(
         key:"sounds_screen_power_v1",
         defaultValue:"Power On/Off",
         substitutions: [:])
  }

 /**
"ANC"

     - **en**: "ANC"
     - **da**: "ANC"
     - **nl**: "ANC"
     - **de**: "ANC"
     - **fr**: "ANC"
     - **it-IT**: "ANC"
     - **nb**: "ANC"
     - **pt-PT**: "ANC"
     - **ru**: "ANC"
     - **es**: "ANC"
     - **sv**: "ANC"
     - **fi**: "ANC"
     - **zh-Hans**: "ANC"
     - **zh-Hant**: "ANC"
     - **id**: "ANC"
     - **fil**: "ANC"
     - **ja**: "ANC"
 */
  public static func speaker_list_item_ANC_link_uc() -> String {
     return localizedString(
         key:"speaker_list_item_ANC_link_uc",
         defaultValue:"ANC",
         substitutions: [:])
  }

 /**
"EQ"

     - **en**: "EQ"
     - **da**: "EQ"
     - **nl**: "EQ"
     - **de**: "EQ"
     - **fr**: "EQ"
     - **it-IT**: "EQ"
     - **nb**: "EQ"
     - **pt-PT**: "EQ"
     - **ru**: "EQ"
     - **es**: "EQ"
     - **sv**: "EQ"
     - **fi**: "EQ"
     - **zh-Hans**: "EQ"
     - **zh-Hant**: "EQ"
     - **id**: "EQ"
     - **fil**: "EQ"
     - **ja**: "EQ"
 */
  public static func speaker_list_item_EQ_link_uc() -> String {
     return localizedString(
         key:"speaker_list_item_EQ_link_uc",
         defaultValue:"EQ",
         substitutions: [:])
  }

 /**
"ANC"

     - **en**: "ANC"
     - **da**: "ANC"
     - **nl**: "ANC"
     - **de**: "ANC"
     - **fr**: "ANC"
     - **it-IT**: "ANC"
     - **nb**: "ANC"
     - **pt-PT**: "ANC"
     - **ru**: "ANC"
     - **es**: "ANC"
     - **sv**: "AKTIV BRUSREDUCERING"
     - **fi**: "ANC"
     - **zh-Hans**: "ANC"
     - **zh-Hant**: "ANC"
     - **id**: "ANC"
     - **fil**: "ANC"
     - **ja**: "ANC"
 */
  public static func speaker_status_anc_uc() -> String {
     return localizedString(
         key:"speaker_status_anc_uc",
         defaultValue:"ANC",
         substitutions: [:])
  }

 /**
"3.5 mm input"

     - **en**: "3.5 mm input"
     - **da**: "3,5 mm-indgang"
     - **nl**: "3,5 mm ingang"
     - **de**: "3,5-mm-Eingang"
     - **fr**: "Entrée 3,5 mm"
     - **it-IT**: "Ingresso da 3,5 mm"
     - **nb**: "3,5 mm inngang"
     - **pt-PT**: "Entrada de 3,5 mm"
     - **ru**: "Вход 3,5 мм"
     - **es**: "Entrada de 3,5 mm"
     - **sv**: "3,5 mm-ingång"
     - **fi**: "3,5 mm:n tuloliitäntä"
     - **zh-Hans**: "3.5 毫米输入"
     - **zh-Hant**: "3.5 公厘輸入連接埠"
     - **id**: "Input 3,5 mm"
     - **fil**: "3.5 mm na input"
     - **ja**: "3.5 mm入力"
 */
  public static func speaker_status_aux() -> String {
     return localizedString(
         key:"speaker_status_aux",
         defaultValue:"3.5 mm input",
         substitutions: [:])
  }

 /**
"AUX Mode"

     - **en**: "AUX Mode"
     - **da**: "AUX-tilstand"
     - **nl**: "AUX-modus"
     - **de**: "AUX-Modus"
     - **fr**: "Mode AUX"
     - **it-IT**: "Modalità AUX"
     - **nb**: "AUX-modus"
     - **pt-PT**: "Modo AUX"
     - **ru**: "Режим AUX"
     - **es**: "Modo AUX"
     - **sv**: "AUX-läge"
     - **fi**: "AUX-tila"
     - **zh-Hans**: "AUX 模式"
     - **zh-Hant**: "AUX 模式"
     - **id**: "Mode AUX"
     - **fil**: "AUX Mode"
     - **ja**: "AUXモード"
 */
  public static func speaker_status_aux_v1() -> String {
     return localizedString(
         key:"speaker_status_aux_v1",
         defaultValue:"AUX Mode",
         substitutions: [:])
  }

 /**
""Pairing...""

     - **en**: ""Pairing...""
     - **da**: ""Parrer ...""
     - **nl**: ""Verbinding maken...""
     - **de**: "„Koppeln ...“"
     - **fr**: "« Association… »"
     - **it-IT**: "“Associazione in corso...”"
     - **nb**: "«Parer ...»"
     - **pt-PT**: ""A emparelhar..""
     - **ru**: "«Сопряжение выполняется...»"
     - **es**: "“Emparejando…”"
     - **sv**: "”Parkopplar…”"
     - **fi**: "”Parinmuodostus käynnissä…”"
     - **zh-Hans**: "“正在配对…”"
     - **zh-Hant**: "「配對中...」"
     - **id**: ""Pairing...""
     - **fil**: ""Nagpapares...""
     - **ja**: "「ペアリング中...」"
 */
  public static func speaker_status_bonding() -> String {
     return localizedString(
         key:"speaker_status_bonding",
         defaultValue:"\"Pairing...\"",
         substitutions: [:])
  }

 /**
"Connected"

     - **en**: "Connected"
     - **da**: "Forbundet"
     - **nl**: "Verbonden"
     - **de**: "Verbunden"
     - **fr**: "Connecté"
     - **it-IT**: "Connesso"
     - **nb**: "Tilkoblet"
     - **pt-PT**: "Ligado"
     - **ru**: "Подключена"
     - **es**: "Conectado"
     - **sv**: "Ansluten"
     - **fi**: "Yhdistetty"
     - **zh-Hans**: "已连接"
     - **zh-Hant**: "已連接"
     - **id**: "Terhubung"
     - **fil**: "Nakakonekta"
     - **ja**: "接続済み"
 */
  public static func speaker_status_connected() -> String {
     return localizedString(
         key:"speaker_status_connected",
         defaultValue:"Connected",
         substitutions: [:])
  }

 /**
"Connecting..."

     - **en**: "Connecting..."
     - **da**: "Forbinder …"
     - **nl**: "Verbinding maken…"
     - **de**: "Verbinden…"
     - **fr**: "Connexion…"
     - **it-IT**: "Collegamento in corso..."
     - **nb**: "Kobler til…"
     - **pt-PT**: "A ligar..."
     - **ru**: "Подключается…"
     - **es**: "Conectando..."
     - **sv**: "Ansluter…"
     - **fi**: "Yhdistää..."
     - **zh-Hans**: "正在连接……"
     - **zh-Hant**: "連線中... "
     - **id**: "Menghubungkan..."
     - **fil**: "Kumokonekta..."
     - **ja**: "接続中…"
 */
  public static func speaker_status_connecting() -> String {
     return localizedString(
         key:"speaker_status_connecting",
         defaultValue:"Connecting...",
         substitutions: [:])
  }

 /**
"COUPLE"

     - **en**: "COUPLE"
     - **da**: "SAMMENSÆT"
     - **nl**: "KOPPEL"
     - **de**: "KOPPELN"
     - **fr**: "COUPLAGE"
     - **it-IT**: "ABBINARE"
     - **nb**: "KOBLE"
     - **pt-PT**: "EMPARELHAR"
     - **ru**: "ВЫПОЛНИТЬ СОПРЯЖЕНИЕ"
     - **es**: "EMPAREJAR"
     - **sv**: "KOPPLA"
     - **fi**: "PARILIITOS"
     - **zh-Hans**: "耦合"
     - **zh-Hant**: "配對"
     - **id**: "BERPASANGAN"
     - **fil**: "COUPLE"
     - **ja**: "カップリング"
 */
  public static func speaker_status_couple_uc() -> String {
     return localizedString(
         key:"speaker_status_couple_uc",
         defaultValue:"COUPLE",
         substitutions: [:])
  }

 /**
"Coupled"

     - **en**: "Coupled"
     - **da**: "Sat sammen"
     - **nl**: "Gekoppeld"
     - **de**: "Gekoppelt"
     - **fr**: "Couplé"
     - **it-IT**: "Abbinato"
     - **nb**: "Koblet"
     - **pt-PT**: "Emparelhado"
     - **ru**: "УСТРОЙСТВА СВЯЗАНЫ"
     - **es**: "Emparejado"
     - **sv**: "Kopplade"
     - **fi**: "Yhdistetty pariliitoksella"
     - **zh-Hans**: "已耦合"
     - **zh-Hant**: "已配對"
     - **id**: "Dipasangkan"
     - **fil**: "Coupled"
     - **ja**: "カップリング完了"
 */
  public static func speaker_status_coupled() -> String {
     return localizedString(
         key:"speaker_status_coupled",
         defaultValue:"Coupled",
         substitutions: [:])
  }

 /**
"COUPLED"

     - **en**: "COUPLED"
     - **da**: "SAT SAMMEN"
     - **nl**: "GEKOPPELD"
     - **de**: "GEKOPPELT"
     - **fr**: "COUPLÉS"
     - **it-IT**: "ABBINATO"
     - **nb**: "KOBLET"
     - **pt-PT**: "EMPARELHADO"
     - **ru**: "УСТРОЙСТВА СВЯЗАНЫ"
     - **es**: "EMPAREJADO"
     - **sv**: "KOPPLADE"
     - **fi**: "PARILIITOS"
     - **zh-Hans**: "已配对"
     - **zh-Hant**: "已配對"
     - **id**: "DIPASANGKAN"
     - **fil**: "COUPLED"
     - **ja**: "カップリング完了"
 */
  public static func speaker_status_coupled_uc() -> String {
     return localizedString(
         key:"speaker_status_coupled_uc",
         defaultValue:"COUPLED",
         substitutions: [:])
  }

 /**
"EQ"

     - **en**: "EQ"
     - **da**: "EQ"
     - **nl**: "EQ"
     - **de**: "EQ"
     - **fr**: "ÉGALISEUR"
     - **it-IT**: "EQ"
     - **nb**: "EQ"
     - **pt-PT**: "EQ"
     - **ru**: "EQ"
     - **es**: "EQ"
     - **sv**: "EQ"
     - **fi**: "Taajuuskorjain"
     - **zh-Hans**: "EQ"
     - **zh-Hant**: "EQ"
     - **id**: "EQ"
     - **fil**: "EQ"
     - **ja**: "EQ"
 */
  public static func speaker_status_eq_uc() -> String {
     return localizedString(
         key:"speaker_status_eq_uc",
         defaultValue:"EQ",
         substitutions: [:])
  }

 /**
"Left and Right"

     - **en**: "Left and Right"
     - **da**: "Venstre og højre"
     - **nl**: "Linker en rechter"
     - **de**: "Links und rechts"
     - **fr**: "Gauche et droite"
     - **it-IT**: "Destro e Sinistro"
     - **nb**: "Venstre og høyre"
     - **pt-PT**: "Esquerdo e direito"
     - **ru**: "Левая и правая"
     - **es**: "Izquierda y derecha"
     - **sv**: "Vänster och höger"
     - **fi**: "Vasen ja oikea"
     - **zh-Hans**: "左右声道"
     - **zh-Hant**: "左和右"
     - **id**: "Kiri dan Kanan"
     - **fil**: "Kaliwa at Kanan"
     - **ja**: "左と右"
 */
  public static func speaker_status_left_and_right() -> String {
     return localizedString(
         key:"speaker_status_left_and_right",
         defaultValue:"Left and Right",
         substitutions: [:])
  }

 /**
"LEFT"

     - **en**: "LEFT"
     - **da**: "VENSTRE"
     - **nl**: "LINKS"
     - **de**: "LINKS"
     - **fr**: "GAUCHE"
     - **it-IT**: "SINISTRO"
     - **nb**: "VENSTRE"
     - **pt-PT**: "ESQUERDO"
     - **ru**: "ЛЕВАЯ"
     - **es**: "IZQUIERDO"
     - **sv**: "VÄNSTER"
     - **fi**: "VASEN"
     - **zh-Hans**: "左"
     - **zh-Hant**: "左"
     - **id**: "KIRI"
     - **fil**: "KALIWA"
     - **ja**: "左"
 */
  public static func speaker_status_left_uc() -> String {
     return localizedString(
         key:"speaker_status_left_uc",
         defaultValue:"LEFT",
         substitutions: [:])
  }

 /**
"Not connected"

     - **en**: "Not connected"
     - **da**: "Ikke forbundet"
     - **nl**: "Niet verbonden"
     - **de**: "Nicht verbunden"
     - **fr**: "Déconnecté"
     - **it-IT**: "Non connesso"
     - **nb**: "Ikke tilkoblet"
     - **pt-PT**: "Não ligado"
     - **ru**: "Не подключена"
     - **es**: "Sin conectar"
     - **sv**: "Ej ansluten"
     - **fi**: "Ei yhdistetty"
     - **zh-Hans**: "未连接"
     - **zh-Hant**: "未連接"
     - **id**: "Tidak terhubung"
     - **fil**: "Hindi nakakonekta"
     - **ja**: "未接続"
 */
  public static func speaker_status_not_connected() -> String {
     return localizedString(
         key:"speaker_status_not_connected",
         defaultValue:"Not connected",
         substitutions: [:])
  }

 /**
"Playing"

     - **en**: "Playing"
     - **da**: "Afspiller "
     - **nl**: "Afspelen"
     - **de**: "Abspielen"
     - **fr**: "En cours de lecture"
     - **it-IT**: "Riproduzione"
     - **nb**: "Avspilling"
     - **pt-PT**: "A reproduzir"
     - **ru**: "Воспроизведение"
     - **es**: "Reproduciendo"
     - **sv**: "Spelar upp "
     - **fi**: "Toistaa kohteesta"
     - **zh-Hans**: "正在播放"
     - **zh-Hant**: "正在播放"
     - **id**: "Memutar"
     - **fil**: "Tumutugtog"
     - **ja**: "再生中"
 */
  public static func speaker_status_playing() -> String {
     return localizedString(
         key:"speaker_status_playing",
         defaultValue:"Playing",
         substitutions: [:])
  }

 /**
"RIGHT"

     - **en**: "RIGHT"
     - **da**: "HØJRE"
     - **nl**: "RECHTS"
     - **de**: "RECHTS"
     - **fr**: "DROITE"
     - **it-IT**: "DESTRO"
     - **nb**: "HØYRE"
     - **pt-PT**: "DIREITO"
     - **ru**: "ПРАВАЯ"
     - **es**: "DERECHO"
     - **sv**: "HÖGER"
     - **fi**: "OIKEA"
     - **zh-Hans**: "右"
     - **zh-Hant**: "右"
     - **id**: "KANAN"
     - **fil**: "KANAN"
     - **ja**: "右"
 */
  public static func speaker_status_right_uc() -> String {
     return localizedString(
         key:"speaker_status_right_uc",
         defaultValue:"RIGHT",
         substitutions: [:])
  }

 /**
"Saving details"

     - **en**: "Saving details"
     - **da**: "Gemmer oplysninger"
     - **nl**: "Gegevens opslaan"
     - **de**: "Details speichern"
     - **fr**: "Enregistrement des informations"
     - **it-IT**: "Salvataggio informazioni"
     - **nb**: "Lagrer detaljer"
     - **pt-PT**: "A guardar detalhes"
     - **ru**: "Сведения сохраняются"
     - **es**: "Guardando detalles"
     - **sv**: "Sparar uppgifter"
     - **fi**: "Tallennetaan tietoja"
     - **zh-Hans**: "保存详细信息"
     - **zh-Hant**: "正在儲存詳細資料"
     - **id**: "Menyimpan perincian"
     - **fil**: "Sine-save ang mga detalye"
     - **ja**: "詳細を保存中"
 */
  public static func spinner_screen_savings() -> String {
     return localizedString(
         key:"spinner_screen_savings",
         defaultValue:"Saving details",
         substitutions: [:])
  }

 /**
"Saving details…"

     - **en**: "Saving details…"
     - **da**: "Gemmer oplysninger …"
     - **nl**: "Gegevens opslaan…"
     - **de**: "Details werden gespeichert …"
     - **fr**: "Enregistrement des informations…"
     - **it-IT**: "Salvataggio informazioni…"
     - **nb**: "Lagrer informasjon…"
     - **pt-PT**: "A guardar detalhes…"
     - **ru**: "Сведения сохраняются…"
     - **es**: "Guardando detalles…"
     - **sv**: "Sparar uppgifter…"
     - **fi**: "Tietoja tallennetaan…"
     - **zh-Hans**: "保存详细信息…"
     - **zh-Hant**: "正在儲存詳細資料……"
     - **id**: "Menyimpan perincian..."
     - **fil**: "Sine-save ang mga detalye…"
     - **ja**: "詳細を保存中…"
 */
  public static func spinner_screen_savings_v1() -> String {
     return localizedString(
         key:"spinner_screen_savings_v1",
         defaultValue:"Saving details…",
         substitutions: [:])
  }

 /**
"Searching for speakers…"

     - **en**: "Searching for speakers…"
     - **da**: "Søger efter højttalere …"
     - **nl**: "Luidsprekers zoeken…"
     - **de**: "Lautsprecher werden gesucht…"
     - **fr**: "Recherche des enceintes…"
     - **it-IT**: "Ricerca diffusori in corso…"
     - **nb**: "Søker etter høyttalere…"
     - **pt-PT**: "A procurar altifalantes…"
     - **ru**: "Поиск акустических систем…"
     - **es**: "Buscando altavoces..."
     - **sv**: "Söker efter högtalare…"
     - **fi**: "Haetaan kaiuttimia..."
     - **zh-Hans**: "正在搜索音箱……"
     - **zh-Hant**: "正在搜尋喇叭…"
     - **id**: "Mencari speaker…"
     - **fil**: "Naghahanap ng mga speaker…"
     - **ja**: "スピーカーを検索中…"
 */
  public static func spinner_screen_searching() -> String {
     return localizedString(
         key:"spinner_screen_searching",
         defaultValue:"Searching for speakers…",
         substitutions: [:])
  }

 /**
"Searching for speakers & headphones…"

     - **en**: "Searching for speakers & headphones…"
     - **da**: "Søger efter højttalere og hovedtelefoner ..."
     - **nl**: "Zoeken naar luidsprekers en hoofdtelefoon…"
     - **de**: "Lautsprecher und Kopfhörer werden gesucht …"
     - **fr**: "Recherche d’enceintes et de casques en cours…"
     - **it-IT**: "Ricerca di diffusori e cuffie…"
     - **nb**: "Søker etter høyttalere og hodetelefoner…"
     - **pt-PT**: "A procurar altifalantes e auscultadores..."
     - **ru**: "Поиск акустических систем и наушников…"
     - **es**: "Buscando altavoces y auriculares…"
     - **sv**: "Söker efter högtalare och hörlurar…"
     - **fi**: "Etsitään kaiuttimia ja kuulokkeita…"
     - **zh-Hans**: "正在搜索音箱和耳机…"
     - **zh-Hant**: "正在搜索揚聲器和耳機……"
     - **id**: "Mencari speaker & headphone..."
     - **fil**: "Naghahanap ng mga speaker at headphone…"
     - **ja**: "スピーカーとヘッドフォンを検出しています…"
 */
  public static func spinner_screen_searching_speakers_headphones() -> String {
     return localizedString(
         key:"spinner_screen_searching_speakers_headphones",
         defaultValue:"Searching for speakers & headphones…",
         substitutions: [:])
  }

 /**
"Searching for speakers &amp; headphones."

     - **en**: "Searching for speakers &amp; headphones."
     - **da**: "Søger efter højttalere og hovedtelefoner."
     - **nl**: "Zoeken naar luidsprekers en hoofdtelefoon."
     - **de**: "Lautsprecher und Kopfhörer werden gesucht."
     - **fr**: "Recherche d’enceintes et de casques en cours."
     - **it-IT**: "Ricerca di diffusori e cuffie."
     - **nb**: "Søker etter høyttalere og hodetelefoner."
     - **pt-PT**: "A procurar altifalantes e auscultadores."
     - **ru**: "Поиск акустических систем и наушников…"
     - **es**: "Buscando altavoces y auriculares."
     - **sv**: "Söker efter högtalare och hörlurar."
     - **fi**: "Etsitään kaiuttimia ja kuulokkeita."
     - **zh-Hans**: "搜索音箱和耳机。"
     - **zh-Hant**: "搜索揚聲器和耳機。"
     - **id**: "Mencari speaker & headphone."
     - **fil**: "Naghahanap ng mga speaker at headphone."
     - **ja**: "スピーカーとヘッドフォンを検出中です。"
 */
  public static func spinner_screen_searching_speakers_headphones_v1() -> String {
     return localizedString(
         key:"spinner_screen_searching_speakers_headphones_v1",
         defaultValue:"Searching for speakers &amp; headphones.",
         substitutions: [:])
  }

 /**
"You will not get any future updates on improvements of your product."

     - **en**: "You will not get any future updates on improvements of your product."
     - **da**: "Fremover vil du ikke få nogen opdateringer af forbedringer af dit produkt."
     - **nl**: "Je zult in de toekomst geen updates over verbeteringen aan je product ontvangen."
     - **de**: "Du erhältst keine weiteren Benachrichtigungen zu Verbesserungen deines Produkts."
     - **fr**: "Vous ne recevrez plus nos prochaines mises à jour d’amélioration de votre produit."
     - **it-IT**: "Non riceverai alcun aggiornamento in futuro sui miglioramenti di questo prodotto."
     - **nb**: "Du vil ikke motta flere oppdateringer om forbedringer av produktet."
     - **pt-PT**: "Não vai obter futuras atualizações sobre melhorias do seu produto."
     - **ru**: "Вы больше не будете получать обновления об улучшениях нашего продукта."
     - **es**: "Dejarás de recibir información de las mejoras que hagamos en tu producto."
     - **sv**: "Du kommer inte att få eventuella framtida uppdateringar gällande förbättringar av din produkt."
     - **fi**: "Et saa jatkossa enää tuotettasi koskevia päivityksiä."
     - **zh-Hans**: "您未来将不会获得有关产品改进的任何更新。"
     - **zh-Hant**: "您將不會收到關於改善產品的任何未來更新。"
     - **id**: "Anda tidak akan mendapatkan pembaruan mendatang mengenai peningkatan produk Anda."
     - **fil**: "Wala kang matatanggap na anumang update sa hinaharap sa mga pagpapabuti sa iyong produkto."
     - **ja**: "今後、ご使用の製品の改善点についての最新情報を受け取ることはできません。"
 */
  public static func stay_updated_confirm_subtitle() -> String {
     return localizedString(
         key:"stay_updated_confirm_subtitle",
         defaultValue:"You will not get any future updates on improvements of your product.",
         substitutions: [:])
  }

 /**
"By confirming below, you will stop receiving emails about new feature updates, special offers and inspirational reads."

     - **en**: "By confirming below, you will stop receiving emails about new feature updates, special offers and inspirational reads."
     - **da**: "Ved at bekræfte nedenfor, stopper du med at modtage e-mails om nye funktionsopdateringer, specialtilbud og inspirerende læsestof."
     - **nl**: "Door hieronder te bevestigen, zult je geen e-mails meer ontvangen over updates van functies, speciale aanbiedingen en inspirerende artikelen."
     - **de**: "Wenn du Folgendes bestätigst, erhältst du keine E-Mails mehr zu Funktions-Updates, Sonderangeboten und inspirierender Lektüre."
     - **fr**: "Une fois que vous aurez donné votre confirmation ci-dessous, vous ne recevrez plus d’e-mails concernant les mises à jour sur les nouvelles fonctionnalités et les offres spéciales, ni de contenu pour vous inspirer."
     - **it-IT**: "Confermando quanto segue, non riceverai più e-mail con aggiornamenti, offerte speciali e consigli utili."
     - **nb**: "Ved å bekrefte nedenfor vil du ikke lenger motta e-poster om nye funksjonsoppdateringer, spesialtilbud og inspirerende lesning."
     - **pt-PT**: "Ao confirmar abaixo, deixará de receber e-mails sobre atualizações de novos recursos, ofertas especiais e leituras inspiradoras."
     - **ru**: "Подтверждая ниже, вы отказываетесь от получения электронных писем о новых функциях, специальных предложениях и с интересными материалами."
     - **es**: "Al confirmar, dejarás de recibir correos electrónicos sobre nuevas actualizaciones de características, ofertas especiales y lecturas inspiradoras."
     - **sv**: "Genom att bekräfta nedan kommer du sluta att få e-post om nya funktionsuppdateringar, specialerbjudanden och inspirerande läsning."
     - **fi**: "Alla antamastasi vahvistuksesta seuraa se, että et enää saa sähköpostiviestejä uusista ominaisuuksien päivityksistä, erikoistarjouksia tai inspiroivaa luettavaa."
     - **zh-Hans**: "在下方进行确认，即表示您将停止接收有关新功能更新、特殊优惠和精彩文章的电子邮件。"
     - **zh-Hant**: "藉由在下面進行確認，您將停止接收新功能更新、特別優惠和啟發靈感的文章的電子郵件。"
     - **id**: "Dengan mengonfirmasi di bawah, Anda akan berhenti menerima email pembaruan fitur baru, penawaran spesial, dan materi inspiratif kami."
     - **fil**: "Sa pamamagitan ng pagkumpirma sa ibaba, hindi ka na makakatanggap ng mga email tungkol sa mga update sa bagong tampok, mga espesyal na alok at mga nagbibigay-inspirasyong babasahin."
     - **ja**: "以下を確認することで、新しい機能アップデート、特別オファー、そして興味深い記事の受け取りを停止します。"
 */
  public static func stay_updated_confirm_subtitle_v1() -> String {
     return localizedString(
         key:"stay_updated_confirm_subtitle_v1",
         defaultValue:"By confirming below, you will stop receiving emails about new feature updates, special offers and inspirational reads.",
         substitutions: [:])
  }

 /**
"ARE YOU SURE?"

     - **en**: "ARE YOU SURE?"
     - **da**: "ER DU SIKKER?"
     - **nl**: "WEET JE HET ZEKER?"
     - **de**: "BIST DU SICHER?"
     - **fr**: "ÊTES-VOUS SÛR ?"
     - **it-IT**: "SEI SICURO?"
     - **nb**: "ER DU SIKKER?"
     - **pt-PT**: "TEM A CERTEZA?"
     - **ru**: "ВЫ УВЕРЕНЫ?"
     - **es**: "¿ESTÁS SEGURO?"
     - **sv**: "ÄR DU SÄKER?"
     - **fi**: "OLETKO VARMA?"
     - **zh-Hans**: "是否确定？"
     - **zh-Hant**: "您確定嗎？"
     - **id**: "APAKAH ANDA YAKIN?"
     - **fil**: "SIGURADO KA BA?"
     - **ja**: "続行しますか？"
 */
  public static func stay_updated_confirm_title_uc() -> String {
     return localizedString(
         key:"stay_updated_confirm_title_uc",
         defaultValue:"ARE YOU SURE?",
         substitutions: [:])
  }

 /**
"YES, I'M SURE"

     - **en**: "YES, I'M SURE"
     - **da**: "JA, JEG ER SIKKER"
     - **nl**: "JA, IK WEET HET ZEKER"
     - **de**: "JA, ICH BIN SICHER."
     - **fr**: "OUI, JE SUIS SÛR"
     - **it-IT**: "SÌ, SONO SICURO"
     - **nb**: "JA, JEG ER SIKKER"
     - **pt-PT**: "SIM, TENHO A CERTEZA"
     - **ru**: "ДА, УВЕРЕН(-А)"
     - **es**: "SÍ, LO TENGO CLARO"
     - **sv**: "JA, JAG ÄR SÄKER"
     - **fi**: "KYLLÄ, OLEN VARMA"
     - **zh-Hans**: "是，确定"
     - **zh-Hant**: "是，我確定"
     - **id**: "YA, SAYA YAKIN"
     - **fil**: "OO, SIGURADO AKO"
     - **ja**: "はい、続行します"
 */
  public static func stay_updated_confirm_yes_uc() -> String {
     return localizedString(
         key:"stay_updated_confirm_yes_uc",
         defaultValue:"YES, I'M SURE",
         substitutions: [:])
  }

 /**
"e-mail address"

     - **en**: "e-mail address"
     - **da**: "e-mailadresse"
     - **nl**: "e-mailadres"
     - **de**: "E-Mail-Adresse"
     - **fr**: "adresse e-mail"
     - **it-IT**: "indirizzo e-mail"
     - **nb**: "e-postadresse"
     - **pt-PT**: "endereço de e-mail"
     - **ru**: "адрес электронной почты"
     - **es**: "dirección de correo electrónico"
     - **sv**: "e-postadress"
     - **fi**: "sähköpostiosoite"
     - **zh-Hans**: "请输入你的邮箱地址"
     - **zh-Hant**: "電子郵件地址"
     - **id**: "alamat email"
     - **fil**: "e-mail adres"
     - **ja**: "Eメールアドレス"
 */
  public static func stay_updated_email_address() -> String {
     return localizedString(
         key:"stay_updated_email_address",
         defaultValue:"e-mail address",
         substitutions: [:])
  }

 /**
"email address"

     - **en**: "email address"
     - **da**: "e-mailadresse"
     - **nl**: "e-mailadres"
     - **de**: "E-Mail-Adresse"
     - **fr**: "Adresse e-mail"
     - **it-IT**: "Indirizzo e-mail"
     - **nb**: "e-postadresse"
     - **pt-PT**: "endereço de e-mail"
     - **ru**: "адрес электронной почты"
     - **es**: "Dirección de correo electrónico"
     - **sv**: "e-postadress"
     - **fi**: "sähköpostiosoite"
     - **zh-Hans**: "电子邮件地址"
     - **zh-Hant**: "電子郵件地址"
     - **id**: "alamat email"
     - **fil**: "email address"
     - **ja**: "Eメールアドレス"
 */
  public static func stay_updated_email_address_v1() -> String {
     return localizedString(
         key:"stay_updated_email_address_v1",
         defaultValue:"email address",
         substitutions: [:])
  }

 /**
"You've signed up with:"

     - **en**: "You've signed up with:"
     - **da**: "Du har tilmeldt dig med;"
     - **nl**: "Je bent ingelogd met:"
     - **de**: "Du hast dich angemeldet mit:"
     - **fr**: "Vous vous êtes inscrit avec :"
     - **it-IT**: "Ti sei iscritto con:"
     - **nb**: "Du er registrert med:"
     - **pt-PT**: "Subscreveu com:"
     - **ru**: "Вы подписались со следующими данными:"
     - **es**: "Te has inscrito con:"
     - **sv**: "Du har anmält dig till:"
     - **fi**: "Olet kirjautunut:"
     - **zh-Hans**: "您已注册："
     - **zh-Hant**: "您已註冊："
     - **id**: "Anda telah mendaftar dengan:"
     - **fil**: "Nag-sign up ka gamit ang:"
     - **ja**: "お客様は次を使用してサインアップされました："
 */
  public static func stay_updated_email_confirmation() -> String {
     return localizedString(
         key:"stay_updated_email_confirmation",
         defaultValue:"You've signed up with:",
         substitutions: [:])
  }

 /**
"Please enter a correct email address."

     - **en**: "Please enter a correct email address."
     - **da**: "Indtast venligst en korrekt e-mailadresse"
     - **nl**: "Voer een correct e-mailadres in."
     - **de**: "Bitte gib eine gültige E-Mail-Adresse ein."
     - **fr**: "Veuillez saisir une adresse e-mail valide."
     - **it-IT**: "Inserisci un indirizzo email valido."
     - **nb**: "Skriv inn en korrekt e-postadresse."
     - **pt-PT**: "Introduza um endereço de e-mail válido."
     - **ru**: "Введите правильный адрес электронной почты."
     - **es**: "Introduce una dirección de correo correcta."
     - **sv**: "Ange en korrekt e-postadress."
     - **fi**: "Kirjoita oikea sähköpostiosoite."
     - **zh-Hans**: "请输入正确的电子邮件地址。"
     - **zh-Hant**: "請輸入正確的電子郵件地址。"
     - **id**: "Masukkanlah alamat email yang benar."
     - **fil**: "Pakilagay ng tamang email adres."
     - **ja**: "正しいEメールアドレスを入力してください。"
 */
  public static func stay_updated_email_validation_fail() -> String {
     return localizedString(
         key:"stay_updated_email_validation_fail",
         defaultValue:"Please enter a correct email address.",
         substitutions: [:])
  }

 /**
"Looks good!"

     - **en**: "Looks good!"
     - **da**: "Det ser godt ud!"
     - **nl**: "Dat ziet er goed uit!"
     - **de**: "Alles klar!"
     - **fr**: "Parfait !"
     - **it-IT**: "Ottimo!"
     - **nb**: "Ser bra ut!"
     - **pt-PT**: "Parece bem!"
     - **ru**: "Все отлично!"
     - **es**: "¡Tiene buena pinta!"
     - **sv**: "Ser bra ut!"
     - **fi**: "Hyvältä näyttää!"
     - **zh-Hans**: "看起来不错！"
     - **zh-Hant**: "看起來很好！"
     - **id**: "Bagus!"
     - **fil**: "Mukhang magandan!"
     - **ja**: "情報が一致しました！"
 */
  public static func stay_updated_email_validation_ok() -> String {
     return localizedString(
         key:"stay_updated_email_validation_ok",
         defaultValue:"Looks good!",
         substitutions: [:])
  }

 /**
"Your personal data is safe with us.\nBy continuing you agree to our "

     - **en**: "Your personal data is safe with us.\nBy continuing you agree to our "
     - **da**: "Dine personlige oplysninger er i sikkerhed hos os.\nVed at fortsætte accepterer du vores "
     - **nl**: "Je persoonlijke gegevens zijn veilig bij ons.\nDoor verder te gaan stem je in met ons "
     - **de**: "Deine persönlichen Daten sind bei uns sicher.\nWenn du weitermachst, akzeptierst du unsere "
     - **fr**: "Avec nous, vos données personnelles sont en sécurité.\nEn continuant vous acceptez notre "
     - **it-IT**: "I tuoi dati personali sono al sicuro con noi.\nProseguendo dichiari di accettare la nostra "
     - **nb**: "Dine personopplysninger er trygge hos oss.\nVed å fortsette godtar du "
     - **pt-PT**: "Os seus dados pessoais estão seguros connosco.\nAo continuar, concorda com a nossa "
     - **ru**: "Ваши персональные данные у нас в безопасности.\nПродолжая пользоваться веб-сайтом, вы принимаете нашу  "
     - **es**: "Tus datos personales están seguros en nuestras manos.\nSi continúas, indicas que aceptas nuestra "
     - **sv**: "Dina personuppgifter är säkra med oss.\nGenom att fortsätta samtycker du till vår "
     - **fi**: "Henkilötietosi ovat meillä suojassa.\nJatkamalla hyväksyt "
     - **zh-Hans**: "我们会妥善保管你的个人数据。\n如点击下一步，表示你同意我们的"
     - **zh-Hant**: "我們會保護您的個人資料安全。\n繼續即表示您同意我們的 "
     - **id**: "Data pribadi Anda aman bersama kami.\nDengan melanjutkan, Anda menyetujui "
     - **fil**: "Ligtas sa amin ang iyong personal na datos.\nSa pagpapatuloy na sumasang-ayon ka sa aming "
     - **ja**: "お客様の個人データは当社で安全に保存されます。\n続行することで、当社の "
 */
  public static func stay_updated_footer() -> String {
     return localizedString(
         key:"stay_updated_footer",
         defaultValue:"Your personal data is safe with us.\nBy continuing you agree to our ",
         substitutions: [:])
  }

 /**
"Your email address is safe with us.\nBy clicking the button you agree to our "

     - **en**: "Your email address is safe with us.\nBy clicking the button you agree to our "
     - **da**: "Din e-mailadresse er sikker hos os.\nVed at klikke på knappen nedenfor, accepterer du vores "
     - **nl**: "Je e-mailadres is veilig bij ons.\nDoor op de knop te klikken ga je akkoord met ons "
     - **de**: "Wir verwahren deine E-Mail-Adresse sicher.\nDurch Klicken auf die Schaltfläche akzeptierst du unsere "
     - **fr**: "Avec nous, votre adresse e-mail est en sécurité.\nEn cliquant sur ce bouton, vous acceptez nos "
     - **it-IT**: "Il tuo indirizzo e-mail è al sicuro con noi.\nCliccando sul pulsante accetti la nostra "
     - **nb**: "E-postadressen din er trygg hos oss.\nVed å klikke på knappen nedenfor godtar du vår "
     - **pt-PT**: "O seu endereço de e-mail está seguro connosco. Ao clicar no botão está a concordar com a nossa "
     - **ru**: "Мы заботимся о безопасности вашего адреса электронной почты. \nНажимая на кнопку ниже, вы соглашаетесь с нашей "
     - **es**: "Tu dirección de correo electrónico está a salvo con nosotros.\nAl hacer clic en el botón, confirmas que estás de acuerdo con nuestra "
     - **sv**: "Din e-postadress är säker hos oss.\nGenom att klicka på knappen godkänner du våra "
     - **fi**: "Säilytämme sähköpostiosoitteesi suojattuna.\nNapsauttamalla alla olevaa painiketta hyväksyt yhtiömme "
     - **zh-Hans**: "我们会谨慎保护您的电子邮件地址。\n点击该按钮，即表示您同意我们的"
     - **zh-Hant**: "您的電子郵件地址在我們這裡很安全。\n 透過單擊按鈕，您同意我們的"
     - **id**: "Alamat email Anda aman bersama kami.\nDengan mengeklik tombol, Anda setuju dengan"
     - **fil**: "Ang iyong email address ay ligtas sa amin.\nSa pamamagitan ng pagpindot ay sumasang-ayon ka sa aming "
     - **ja**: "お使いのEメールアドレスは安全です。●ボタンをクリックすると、当社の "
 */
  public static func stay_updated_footer_v1() -> String {
     return localizedString(
         key:"stay_updated_footer_v1",
         defaultValue:"Your email address is safe with us.\nBy clicking the button you agree to our ",
         substitutions: [:])
  }

 /**
"Privacy Policy"

     - **en**: "Privacy Policy"
     - **da**: "Politik for beskyttelse af personlige oplysninger"
     - **nl**: "Privacybeleid"
     - **de**: "Datenschutzrichtlinie"
     - **fr**: "Politique de confidentialité"
     - **it-IT**: "politica sulla privacy"
     - **nb**: "personvernerklæringen"
     - **pt-PT**: "Política de Privacidade"
     - **ru**: "Политику конфиденциальности"
     - **es**: "Política de privacidad"
     - **sv**: "sekretesspolicy."
     - **fi**: "tietosuojakäytäntömme"
     - **zh-Hans**: "隐私政策"
     - **zh-Hant**: "隱私權政策"
     - **id**: "Kebijakan Privasi kami"
     - **fil**: "Patakaran sa Pagkapribado"
     - **ja**: "プライバシーポリシーに同意することとなります。"
 */
  public static func stay_updated_privacy_policy() -> String {
     return localizedString(
         key:"stay_updated_privacy_policy",
         defaultValue:"Privacy Policy",
         substitutions: [:])
  }

 /**
"Learn more about how to get started with your speaker, receive new feature updates, special offers and inspirational reads."

     - **en**: "Learn more about how to get started with your speaker, receive new feature updates, special offers and inspirational reads."
     - **da**: "Få flere oplysninger om, hvordan du kommer i gang med din højttaler, modtag nye funktionsopdateringer, specialtilbud og inspirerende læsestof."
     - **nl**: "Ontdek hoe je met je luidspreker aan de slag gaat, ontvang updates van functies, speciale aanbiedingen en inspirerende artikels."
     - **de**: "Lerne noch mehr über deine neuen Lautsprecher, erhalte Updates mit neuen Funktionen, Angebote und Lektüre, die dich inspiriert. "
     - **fr**: "Apprenez-en davantage sur la manière de bien utiliser votre enceinte, recevez des mises à jour sur les nouvelles fonctionnalités, des offres spéciales et du contenu pour vous inspirer."
     - **it-IT**: "Scopri di più su come iniziare a utilizzare il tuo diffusore e su come ricevere aggiornamenti, offerte speciali e consigli utili."
     - **nb**: "Få vite mer om hvordan du kommer i gang med din høyttaler, motta nye funksjonsoppdateringer, spesialtilbud og lesestoff til inspirasjon."
     - **pt-PT**: "Saiba mais sobre como começar a utilizar o seu altifalante, receba novas atualizações de funcionalidades, ofertas especiais e leituras interessantes."
     - **ru**: "Узнайте подробнее о том, как начать работу со своей акустической системой, получайте новости о новых возможностях, специальные предложения и вдохновляющие материалы."
     - **es**: "Infórmate de cómo empezar a usar tu altavoz, recibe actualizaciones de nuevas características y ofertas especiales y disfruta de lecturas inspiradoras."
     - **sv**: "Ta reda mer om hur du kommer igång med din högtalare, få nya funktionsuppdateringar, specialerbjudanden och inspirerande läsning."
     - **fi**: "Opi lisää oman kaiuttimesi käytön aloittamisesta, vastaanota uusia ominaisuuksien päivityksiä, erikoistarjouksia ja inspiroivaa luettavaa."
     - **zh-Hans**: "你提供的邮箱将用于接收相关信息，例如\n了解如何使用音箱、新功能更新、特殊优惠等"
     - **zh-Hant**: "詳細了解如何開始使用喇叭、接收新功能更新、特別優惠和啟發靈感的文章。"
     - **id**: "Pelajari lebih lanjut mengenai cara memulai speaker Anda, dapatkan pembaruan fitur baru, penawaran khusus, dan materi penuh inspirasi."
     - **fil**: "Alamin ang higit pa kung paano magsisimula sa iyong speaker, tumanggap ng mga bagong feature update, mga espesyal na alok at nagbibigay-inspirasyong babasahin."
     - **ja**: "スピーカーの使用を開始する方法の詳細、新しい機能アップデート、特別オファー、そして興味深い記事を受け取ることができます。"
 */
  public static func stay_updated_subtitle() -> String {
     return localizedString(
         key:"stay_updated_subtitle",
         defaultValue:"Learn more about how to get started with your speaker, receive new feature updates, special offers and inspirational reads.",
         substitutions: [:])
  }

 /**
"Learn more about how to get started with your product, receive new feature updates, special offers and inspirational reads."

     - **en**: "Learn more about how to get started with your product, receive new feature updates, special offers and inspirational reads."
     - **da**: "Få flere oplysninger om, hvordan du kommer i gang med dit produkt, modtag nye funktionsopdateringer, specialtilbud og inspirerende læsestof."
     - **nl**: "Ontdek hoe je met je product aan de slag gaat, ontvang updates van functies, speciale aanbiedingen en inspirerende artikelen."
     - **de**: "Lerne noch mehr über dein neues Produkt, erhalte Updates mit neuen Funktionen, Angeboten und Lektüre, die dich inspiriert."
     - **fr**: "Apprenez-en davantage sur la manière de bien utiliser votre produit, recevez des mises à jour sur les nouvelles fonctionnalités, des offres spéciales et du contenu pour vous inspirer."
     - **it-IT**: "Scopri di più su come iniziare a utilizzare il tuo prodotto e su come ricevere aggiornamenti, offerte speciali e consigli utili."
     - **nb**: "Få vite mer om hvordan du kommer i gang med produktet ditt, motta nye funksjonsoppdateringer, spesialtilbud og lesestoff til inspirasjon."
     - **pt-PT**: "Saiba mais sobre como começar a utilizar o seu produto, receba novas atualizações de funcionalidades, ofertas especiais e leituras interessantes."
     - **ru**: "Узнайте подробнее о том, как начать работу с продуктом, получайте новости о новых возможностях, специальные предложения и интересные материалы."
     - **es**: "Infórmate de cómo empezar a usar tu producto, recibe actualizaciones de nuevas características y ofertas especiales, y disfruta de lecturas inspiradoras."
     - **sv**: "Lär dig mer om hur du kommer igång med din produkt, få nya funktionsuppdateringar, specialerbjudanden och inspirerande läsning."
     - **fi**: "Lue lisää oman tuotteesi käytön aloittamisesta ja vastaanota uusia ominaisuuksien päivityksiä, erikoistarjouksia ja inspiroivaa luettavaa."
     - **zh-Hans**: "深入了解如何使用您的产品、接收新功能更新、特殊优惠和精彩文章。"
     - **zh-Hant**: "詳細了解如何開始使用產品、接收新功能更新、特別優惠和啟發靈感的文章。"
     - **id**: "Pelajari lebih lanjut mengenai cara menggunakan produk Anda, dapatkan pembaruan fitur baru, penawaran khusus, dan materi penuh inspirasi."
     - **fil**: "Alamin ang higit pa tungkol sa kung paano magsisimula sa iyong produkto, tumanggap ng mga bagong update sa feature, mga espesyal na alok at mga nagbibigay-inspirasyong babasahin."
     - **ja**: "製品の使用を開始する方法の詳細、新しい機能アップデート、特別オファー、そして興味深い記事を受け取ることができます。"
 */
  public static func stay_updated_subtitle_v1() -> String {
     return localizedString(
         key:"stay_updated_subtitle_v1",
         defaultValue:"Learn more about how to get started with your product, receive new feature updates, special offers and inspirational reads.",
         substitutions: [:])
  }

 /**
"STAY UPDATED"

     - **en**: "STAY UPDATED"
     - **da**: "HOLD DIG OPDATERET"
     - **nl**: "BLIJF OP DE HOOGTE"
     - **de**: "BLEIB INFORMIERT"
     - **fr**: "RESTER INFORMÉ"
     - **it-IT**: "RESTA AGGIORNATO"
     - **nb**: "HOLD DEG OPPDATERT"
     - **pt-PT**: "FIQUE ATUALIZADO"
     - **ru**: "ВСЕГДА БУДЬТЕ В КУРСЕ"
     - **es**: "MANTENTE INFORMADO"
     - **sv**: "HÅLL DIG UPPDATERAD"
     - **fi**: "PYSY AJAN TASALLA"
     - **zh-Hans**: "接收消息"
     - **zh-Hant**: "保持更新"
     - **id**: "TERUS IKUTI PEMBARUAN"
     - **fil**: "MANATILING UPDATED"
     - **ja**: "最新情報を手に入れる"
 */
  public static func stay_updated_title_uc() -> String {
     return localizedString(
         key:"stay_updated_title_uc",
         defaultValue:"STAY UPDATED",
         substitutions: [:])
  }

 /**
"TERMS &amp; CONDITIONS"

     - **en**: "TERMS &amp; CONDITIONS"
     - **da**: "VILKÅR OG BETINGELSER"
     - **nl**: "ALGEMENE VOORWAARDEN"
     - **de**: "ALLGEMEINE GESCHÄFTSBEDINGUNGEN"
     - **fr**: "CONDITIONS GÉNÉRALES"
     - **it-IT**: "TERMINI E CONDIZIONI"
     - **nb**: "VILKÅR OG BETINGELSER"
     - **pt-PT**: "TERMOS E CONDIÇÕES"
     - **ru**: "ПРАВИЛА И УСЛОВИЯ ПОЛЬЗОВАНИЯ"
     - **es**: "TÉRMINOS Y CONDICIONES"
     - **sv**: "REGLER OCH VILLKOR"
     - **fi**: "KÄYTTÖEHDOT"
     - **zh-Hans**: "条款与条件"
     - **zh-Hant**: "條款與條件"
     - **id**: "SYARAT & KETENTUAN"
     - **fil**: "MGA TUNTUNIN AT KUNDISYON"
     - **ja**: "利用規約"
 */
  public static func terms_and_conditions_screen_title_uc_v1() -> String {
     return localizedString(
         key:"terms_and_conditions_screen_title_uc_v1",
         defaultValue:"TERMS &amp; CONDITIONS",
         substitutions: [:])
  }

 /**
"Wait for FastPairing notification and preceed according to instructions."

     - **en**: "Wait for FastPairing notification and preceed according to instructions."
     - **da**: "Vent på FastPairing-meddelelsen og følg til instruktionerne."
     - **nl**: "Wacht op de melding van Snel koppelen en ga verder volgens de instructies."
     - **de**: "Warte auf die Benachrichtigung zum schnellen Koppeln und fahre gemäß den Anweisungen fort."
     - **fr**: "Attendez de recevoir une notification d’appairage rapide puis suivez les instructions."
     - **it-IT**: "Aspetta la notifica di Abbinamento rapido e segui le istruzioni."
     - **nb**: "Vent på rask paring-varsel og fortsett i samsvar med instruksene."
     - **pt-PT**: "Aguarde a notificação do FastPairing e proceda de acordo com as instruções."
     - **ru**: "Подождите уведомления FastPairing и действуйте согласно инструкции."
     - **es**: "Espera el aviso de emparejamiento rápido y continúa según las instrucciones."
     - **sv**: "Vänta på FastPairing-meddelande och fortsätt enligt instruktionerna."
     - **fi**: "Odota ilmoitusta laiteparin pikamuodostuksesta (FastPairing) ja etene ohjeiden mukaan."
     - **zh-Hans**: "等待快速配对通知并按照说明进行操作。"
     - **zh-Hant**: "等待快速配對通知並按照說明進行操作。"
     - **id**: "Tunggu pemberitahuan FastPairing dan lanjutkan sesuai petunjuk."
     - **fil**: "Maghintay ng abiso ng MabilisangPagpares at magpatuloy ayon sa mga tagubilin."
     - **ja**: "ファストペアリング通知を待ち、説明に従って進めてください。"
 */
  public static func turn_on_pairing_subtitle_3() -> String {
     return localizedString(
         key:"turn_on_pairing_subtitle_3",
         defaultValue:"Wait for FastPairing notification and preceed according to instructions.",
         substitutions: [:])
  }

 /**
"I agree to the "

     - **en**: "I agree to the "
     - **da**: "Jeg accepterer disse "
     - **nl**: "Ik stem in met de "
     - **de**: "Ich akzeptiere die "
     - **fr**: "J’accepte les "
     - **it-IT**: "Accetto i "
     - **nb**: "Jeg godtar "
     - **pt-PT**: "Concordo com os "
     - **ru**: "Принимаю "
     - **es**: "Acepto los "
     - **sv**: "Jag godkänner "
     - **fi**: "Hyväksyn "
     - **zh-Hans**: "我同意 "
     - **zh-Hant**: "我同意 "
     - **id**: "Saya menyetujui"
     - **fil**: "Sumasang-ayon ako sa "
     - **ja**: "私は"
 */
  public static func welcome_screen_agree() -> String {
     return localizedString(
         key:"welcome_screen_agree",
         defaultValue:"I agree to the ",
         substitutions: [:])
  }

 /**
"This Bluetooth speaker taps into Marshall's heritage of big stage performance and brings it out of the arena and into your home."

     - **en**: "This Bluetooth speaker taps into Marshall's heritage of big stage performance and brings it out of the arena and into your home."
     - **da**: "Denne Bluetooth-højttaler trækker på Marshalls tradition for stor scenelyd og flytter den fra scenen og ind i dit hjem."
     - **nl**: "Deze Bluetooth-luidspreker haakt in op Marshall's erfgoed van krachtige podiumprestaties en brengt die van de arena naar je huis."
     - **de**: "Dieser Bluetooth-Lautsprecher überträgt das Marshall-Erbe von der großen Bühne direkt in dein Zuhause."
     - **fr**: "Cette enceinte Bluetooth reproduit l’expérience Marshall des grandes scènes de concert à l’intérieur de votre maison."
     - **it-IT**: "Questo diffusore Bluetooth porta la tradizione sonora del marchio Marshall dai grandi palchi alle mura di casa."
     - **nb**: "Denne Bluetooth-høyttaleren bringer Marshalls kjente og storslåtte show fra arenaen rett til hjemmet ditt."
     - **pt-PT**: "Este altifalante Bluetooth explora a herança da Marshall de desempenho da ribalta e leva-o para fora do estádio até sua casa."
     - **ru**: "Акустическая система с каналом Bluetooth опирается на многолетний опыт Marshall в организации концертного звука и привносит аналогичное качество в ваш дом."
     - **es**: "Este altavoz con Bluetooth aprovecha el legado de Marshall sobre grandes escenarios y te lo lleva directamente a tu casa."
     - **sv**: "Denna Bluetooth-högtalare tar Marshalls arv av stora scenframträdanden från arenorna in i ditt hem."
     - **fi**: "Tämä Bluetooth-kaiutin siirtää Marshallin suurten lavojen äänentoiston tehon perinteen suoraan kotiisi."
     - **zh-Hans**: "这款蓝牙音箱充分体现了 Marshall 的大舞台表演传统，让您在家中亦能体验到竞技场的感觉。"
     - **zh-Hant**: "此款藍牙喇叭承襲 Marshall 一慣的大型舞台優異表現，並將環形劇場效果帶入您家中。"
     - **id**: "Speaker Bluetooth ini menggunakan warisan pertunjukan pertunjukan panggung megah khas Marshall dan menghadirkan suasana bak arena pertunjukan ke dalam rumah Anda."
     - **fil**: "Ginagamit ng  Bluetooth speaker na ito na minana ng Marshall na pangmalaking stage na performance at inilalabas ito sa arena papunta sa iyong tahanan."
     - **ja**: "このBluetoothスピーカーは、Marshallの長年にわたる大型ステージの音響性能をお客様のご自宅で実現します。"
 */
  public static func welcome_screen_subtitle() -> String {
     return localizedString(
         key:"welcome_screen_subtitle",
         defaultValue:"This Bluetooth speaker taps into Marshall's heritage of big stage performance and brings it out of the arena and into your home.",
         substitutions: [:])
  }

 /**
"This Bluetooth product taps into Marshall's heritage of big stage performance and brings it out of the arena and into your life."

     - **en**: "This Bluetooth product taps into Marshall's heritage of big stage performance and brings it out of the arena and into your life."
     - **da**: "Dette Bluetooth-produkt trækker på Marshalls tradition for stor scenelyd og flytter den fra scenen og ind i dit liv."
     - **nl**: "Dit Bluetooth-product haakt in op Marshall's erfgoed van krachtige podiumprestaties en brengt die van de arena naar jouw leven."
     - **de**: "Dieses Bluetooth-Produkt überträgt das Marshall-Erbe von der großen Bühne direkt in dein Leben."
     - **fr**: "Ce produit Bluetooth reproduit l’expérience Marshall des grandes scènes de concert dans votre vie quotidienne."
     - **it-IT**: "Questo prodotto Bluetooth porta la tradizione sonora del marchio Marshall dai grandi palchi alla tua vita quotidiana."
     - **nb**: "Dette Bluetooth-produktet bringer Marshalls kjente og storslåtte show fra arenaen rett til livet ditt."
     - **pt-PT**: "Este produto Bluetooth explora a herança da Marshall de alto desempenho e leva-o para fora do estádio e para a sua vida."
     - **ru**: "В основе этого Bluetooth-устройства положен многолетний опыт Marshall в организации концертного звука, и оно привносит аналогичное качество в вашу жизнь."
     - **es**: "Este producto con Bluetooth aprovecha el legado de Marshall sobre grandes escenarios y lo incorpora directamente a tu vida."
     - **sv**: "Denna Bluetooth-produkt tar Marshalls arv av stora scenframträdanden från arenorna in i ditt liv."
     - **fi**: "Tämä Bluetooth-tuote siirtää Marshallin äänentoistotehon perinteen suurilta lavoilta suoraan kotiisi."
     - **zh-Hans**: "这款蓝牙产品充分体现了 Marshall 的大舞台表演传统，让您在日常生活中亦能体验到竞技场的感觉。"
     - **zh-Hant**: "此款 Bluetooth 產品承襲 Marshall 一慣的大型舞台優異表現，並將環形劇場效果帶入您家中。"
     - **id**: "Produk Bluetooth ini menggunakan warisan audio pertunjukan megah khas Marshall dan menghadirkan kualitas panggung profesional ke dalam hidup Anda."
     - **fil**: "Ginagamit ng produktong Bluetooth na ito ang pag-aari ng Marshall malakihang pagganap sa stage at inilalabas ito sa arena papunta sa iyong buhay."
     - **ja**: "このBluetooth製品は、Marshallの長年に亘る大型ステージの音響性能をお客様の生活で実現します。"
 */
  public static func welcome_screen_subtitle_v1() -> String {
     return localizedString(
         key:"welcome_screen_subtitle_v1",
         defaultValue:"This Bluetooth product taps into Marshall's heritage of big stage performance and brings it out of the arena and into your life.",
         substitutions: [:])
  }

 /**
"terms and conditions"

     - **en**: "terms and conditions"
     - **da**: "vilkår og betingelser"
     - **nl**: "algemene voorwaarden"
     - **de**: "Verkaufs- und Lieferbedingungen"
     - **fr**: "conditions générales"
     - **it-IT**: "termini e condizioni"
     - **nb**: "vilkårene"
     - **pt-PT**: "termos e condições"
     - **ru**: "Правила и условия использования"
     - **es**: "términos y condiciones"
     - **sv**: "reglerna och villkoren"
     - **fi**: "käyttöehdot"
     - **zh-Hans**: "条款与条件"
     - **zh-Hant**: "條款與條件"
     - **id**: "syarat dan ketentuan"
     - **fil**: "mga takda at kondisyon"
     - **ja**: "利用規約に同意します"
 */
  public static func welcome_screen_terms_and_conditions() -> String {
     return localizedString(
         key:"welcome_screen_terms_and_conditions",
         defaultValue:"terms and conditions",
         substitutions: [:])
  }

 /**
"WELCOME"

     - **en**: "WELCOME"
     - **da**: "VELKOMMEN"
     - **nl**: "WELKOM"
     - **de**: "WILLKOMMEN"
     - **fr**: "BIENVENUE"
     - **it-IT**: "BENVENUTO"
     - **nb**: "VELKOMMEN"
     - **pt-PT**: "BEM-VINDO"
     - **ru**: "ДОБРО ПОЖАЛОВАТЬ!"
     - **es**: "BIENVENIDO/A"
     - **sv**: "VÄLKOMMEN"
     - **fi**: "TERVETULOA"
     - **zh-Hans**: "欢迎"
     - **zh-Hant**: "歡迎使用"
     - **id**: "SELAMAT DATANG"
     - **fil**: "MAGLIGAYANG PAGDATING"
     - **ja**: "ようこそ"
 */
  public static func welcome_screen_title_uc() -> String {
     return localizedString(
         key:"welcome_screen_title_uc",
         defaultValue:"WELCOME",
         substitutions: [:])
  }

     /**
    "CONTINUE"

         - **en**: "CONTINUE"
         - **da**: "CONTINUE"
         - **nl**: "CONTINUE"
         - **de**: "CONTINUE"
         - **fr**: "CONTINUE"
         - **it-IT**: "CONTINUE"
         - **nb**: "CONTINUE"
         - **pt-PT**: "CONTINUE"
         - **ru**: "CONTINUE"
         - **es**: "CONTINUE"
         - **sv**: "CONTINUE"
         - **fi**: "CONTINUE"
         - **zh-Hans**: "CONTINUE"
         - **zh-Hant**: "CONTINUE"
         - **id**: "CONTINUE"
         - **fil**: "CONTINUE"
         - **ja**: "CONTINUE"
     */
      public static func appwide_continue_uc() -> String {
         return localizedString(
             key:"appwide_continue_uc",
             defaultValue:"CONTINUE",
             substitutions: [:])
      }


}
