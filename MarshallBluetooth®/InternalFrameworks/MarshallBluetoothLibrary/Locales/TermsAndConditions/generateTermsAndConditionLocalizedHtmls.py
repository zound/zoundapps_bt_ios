#!/usr/bin/env python
#
# Usage example:
# find ~/Desktop/terms/ -type f | python generateTermsAndConditionLocalizedHtmls.py

import os
import sys
from os.path import basename

TARGET_DIR_COMPONENT = ".lproj/TermsAndConditions.html"

H2 = "<h2>"
H2_END = "</h2>"
P = "<p>"
P_END = "</p>"

NEW_LINE = "\n"

END_TAG = { H2: H2_END, P: P_END }

TAGS_ORDERED = \
[H2] + [P]*1 + \
[H2] + [P]*7 + \
[H2] + [P]*1 + \
[H2] + [P]*3 + \
[H2] + [P]*1 + \
[H2] + [P]*4 + \
[H2] + [P]*2 + \
[H2] + [P]*1 + \
[H2] + [P]*2 + \
[H2] + [P]*2 + \
[H2] + [P]*1 + \
[H2] + [P]*1 + \
[H2] + [P]*4 + \
[H2] + [P]*2 + \
[H2] + [P]*1 + \
[H2] + [P]*4 + \
[H2] + [P]*12


for path in [line.strip() for line in sys.stdin.read().splitlines()]:
    language = os.path.splitext(basename(path))[0]
    destinationPath = language + TARGET_DIR_COMPONENT
    if not os.path.exists(destinationPath):
        print "Destination path not exists:", destinationPath
        continue
    with open(path, 'r') as sourceFile, open(destinationPath, 'w+') as destinationFile:
        content = filter(None, [line.strip() for line in sourceFile.read().splitlines()])
        for index, element in enumerate(content[1:]):
            try:
                tag = TAGS_ORDERED[index]
            except IndexError:
                print "Lack of index:", index
                continue
            destinationFile.write(tag + element + END_TAG[tag] + NEW_LINE)
