//
//  Language+Extensions.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 01/10/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

public extension Language {
    var onlineManualLanguageComponent: String {
        switch self {
        case .en, .da, .nl, .de, .fr, .it, .pt, .ru, .es, .fi, .id:
            return rawValue
        case .nb:
            return "no"
        case .sv:
            return "se"
        case .zh_Hans:
            return "zh-cn"
        case .zh_Hant:
            return "zh-tw"
        case .fil:
            return "ph"
        case .ja:
            return "jp"
        }
    }
    var privacyPolicyLanguageComponent: String {
        switch self {
        case .en, .fr, .de, .nl, .fi, .es, .ru, .id:
            return rawValue
        case .it:
            return "it"
        case .pt:
            return "pt"
        case .da:
            return "dk"
        case .sv:
            return "se"
        case .zh_Hans:
            return "cn-simp"
        case .zh_Hant:
            return "cn-trad"
        case .ja:
            return "jp"
        case .fil:
            return "ph"
        case .nb:
            return "no"
        }
    }
}
