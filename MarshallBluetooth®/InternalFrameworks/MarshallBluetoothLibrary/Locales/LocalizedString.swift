//
//  LocalizedString.swift
//  MarshallBluetoothLibrary
//

// It's heavily inspired by the Kickstarter iOS app
// https://github.com/kickstarter/ios-oss

import Foundation

/// Finds a localized string for a provided key and interpolates it with substitutions.
///
/// - Parameters:
///   - key: The key of the string to find in a bundle
///   - defaultValue: Optional value to use in case a string could not be found for the provided key
///   - substitutions: A dictionary of key/value substitutions to be made.
///   - env: An app environment to derive the language from
///   - bundle: bundle containing translations resource
/// - Returns: localized, interpolated string
public func localizedString(key: String,
                            defaultValue: String = "",
                            substitutions: [String:String] = [:],
                            env: Environment =  AppEnvironment.current,
                            bundle: BundleType = stringsBundle) -> String {
    let lprojName = lprojFileNameForLanguage(env.language)
    let localized = bundle.path(forResource: lprojName, ofType: "lproj")
        .flatMap(type(of: bundle).create(path:))
        .flatMap { $0.localizedString(forKey: key, value: nil, table: nil) }
        .filter { $0.caseInsensitiveCompare(key) != .orderedSame }
        .filter { !$0.isEmpty }
        .coalesceWith(defaultValue)
    
        return substitute(localized, with: substitutions)
}

// Performs simple string interpolation on keys of the form `%{key}`
private func substitute(_ string: String, with substitutions: [String: String]) -> String {
    return substitutions.reduce(string) { accum, sub in
        return accum.replacingOccurrences(of: "%{\(sub.0)}", with: sub.1)
    }
}

private func lprojFileNameForLanguage(_ language: Language) -> String {
    return language.rawValue == "en" ? "Base" : language.rawValue
}

private class Pin {}
public let stringsBundle = Bundle(for: Pin.self)
