//
//  HtmlStringProviding.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 21/09/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

private struct PathComponent {
    static let htmlExtension = "html"
    static let baseSuffix = "Base"
}

private struct  HtmlTemplatePlaceholder {
    static let body = "${body}"
}

enum HtmlFile: String {
    case termsAndConditions = "TermsAndConditions"
}

protocol HtmlStringProviding {
    func htmlString(for htmlFile: HtmlFile) -> String
}

extension HtmlStringProviding {
    func htmlString(for htmlFile: HtmlFile) -> String {
        guard let basePath = Bundle.framework.path(forResource: htmlFile.rawValue + PathComponent.baseSuffix, ofType: PathComponent.htmlExtension),
              let path = Bundle.framework.path(forResource: htmlFile.rawValue, ofType: PathComponent.htmlExtension, inDirectory: nil, forLocalization: AppEnvironment.current.language.rawValue),
              var htmlBase = try? String(contentsOfFile: basePath, encoding: String.Encoding.utf8),
              let html = (try? String(contentsOfFile: path, encoding: String.Encoding.utf8)) ??
                         (try? String(contentsOfFile: path, encoding: String.Encoding.shiftJIS)) else {
            return String()
        }
        htmlBase = htmlBase.replacingOccurrences(of: HtmlTemplatePlaceholder.body, with: html)
        return htmlBase
    }
}
