//
//  File.swift
//  MarshallBluetoothLibrary
//

// Code in this file is used for:
// - loading and parsing JSON file containing all of the app translations
// - creating translation (Localization.strings) files for the app
// - building Strings.swift, which contains bunch of static functions being wrappers around
// localizedString function defined in LocalizedString.swift file.
//
// It can be used in command line by invoking 'make strings'

import Foundation

public let stringComponentSeparator = "%s"

/// Supported languages

public enum Language: String {
    case en
    case da                  // danish
    case nl                  // dutch
    case de                  // german
    case fr                  // french
    case it = "it-IT"        // italian
    case nb                  // norwegian
    case pt = "pt-PT"        // portuguese
    case ru                  // russian
    case es                  // spanish
    case sv                  // swedish
    case fi                  // finnish
    case zh_Hans = "zh-Hans" // chinese simplified
    case zh_Hant = "zh-Hant" // chinese traditional
    case id                  // indonesian
    case fil                 // filipinian
    case ja                  // japanese

    public static let allLanguages: [Language] = [.en, .da, .nl, .de, .fr, .it, .nb, .pt, .ru, .es, .sv, .fi, .zh_Hans, .zh_Hant, .id, .fil, ja]
    
    public init?(languageString language: String) {
        switch language.prefix(3) {
        case "en-": self = .en
        case "da-": self = .da    // danish
        case "nl-": self = .nl    // dutch
        case "de-": self = .de    // german
        case "fr-": self = .fr    // french
        case "it-": self = .it    // italian
        case "nb-": self = .nb    // norwegian
        case "pt-": self = .pt    // portuguese
        case "ru-": self = .ru    // russian
        case "es-": self = .es    // spanish
        case "sv-": self = .sv    // swedish
        case "fi-": self = .fi    // finnish
        case "zh-": self = language == "zh-Hant" ? .zh_Hant : .zh_Hans
        case "id-": self = .id    // indonesian
        case "fil": self = .fil  // filipinian
        case "ja-": self = .ja    // japanese
        default: return nil
        }
    }
    
    public init?(languageStrings languages: [String]) {
        guard let language = languages
            .lazy
            .map ({ String($0.prefix(7)) })
            .compactMap(Language.init(languageString:))
            .first else {
                return nil
        }
        self = language
    }
}

public let translationsFilePath: String? = "MarshallBluetooth®/InternalFrameworks/MarshallBluetoothLibrary/Locales/Translations.json"

extension String: Error {}

public struct TranslationEntity : Codable {
    public let lang: String
    public let translation: String
}

public struct TranslationResource: Codable {
    public let label: String
    public let translations: [TranslationEntity]
}

public struct Translations: Codable {
    public let all: [TranslationResource]
}

///Extends TranslationsResource struct if needed (adding Base translation using translations for selected language identifier as prototype)
extension TranslationResource {
    public static func extend(baseLanguage prototype: String) -> ((TranslationResource) -> TranslationResource) {
        return { resource in
            var augmented = resource.translations
            let baseTranslationPrototype = resource.translations.filter {
                $0.lang == prototype
            }.first!
            let baseTranslation = TranslationEntity(lang: "Base", translation: baseTranslationPrototype.translation)
            augmented.append(baseTranslation)
            return TranslationResource(label: resource.label, translations: augmented)
        }
    }
}

extension Array where Element: Hashable {
    public func distincts(_ eq: (Element, Element) -> Bool) -> Array {
        var result = Array()
        forEach { x in
            if !result.contains(where: { eq(x, $0) }) {
                result.append(x)
            }
        }
        return result
    }
}

public func escaped(_ string: String) -> String {
    return string
        .replacingOccurrences(of: "\n", with: "\\n")
        .replacingOccurrences(of: "\"", with: "\\\"")
}

public func checkMissing(translations: Translations, for languages: [Language]) throws {
    try translations.all.forEach { resource in
        try languages.forEach { language in
            guard let _ = resource.translations
                .filter({ $0.lang == language.rawValue })
                .first?.translation else {
                    throw ("Translation for label: \(resource.label), for language: \(language.rawValue) not found")
            }
        }
        
    }
}

public func translationFileContents(_ entries: Translations, forLanguage language: Language) -> String {
    var components = [String]()
    
    entries.all.sorted { $0.label < $1.label }
        .forEach { resource in
            
            let translatedString = resource.translations
                .filter { $0.lang == language.rawValue }
                .first!
                .translation
            components.append("\"\(resource.label)\" = \"\(escaped(translatedString))\";")
    }
    return components.joined(separator: "\n")
}

public func funcArgumentNames(_ string: String) -> [String] {
    return string
        .components(separatedBy: "%{").flatMap { $0.components(separatedBy: "}") }
        .enumerated()
        .filter {idx, _ in idx % 2 == 1 }
        .map {_, x in x }
        .distincts(==)
}

public func funcArguments(_ argumentNames: [String]) -> String {
    return argumentNames
        .map { x in
            let type =  "String"
            return "\(x): \(type)"
        }
        .joined(separator: ", ")
}

public func funcSubstitutions(_ string: String) -> String {
    let insides = string
        .components(separatedBy: "%{")
        .flatMap { $0.components(separatedBy: "}") }
        .enumerated()
        .filter { idx, _ in idx % 2 == 1 }
        .map { _, x in  "\"\(x)\": \(x)"}
        .distincts(==)
        .joined(separator: ", ")
    
    if insides.isEmpty {
        return "[:]"
    }
    return "[\(insides)]"
}

private func checkMissingTranslations(_ translations: Translations, languages: [Language]) {
    do {
        try checkMissing(translations: translations, for: languages)
    } catch {
        fatalError(error as! String)
    }
}

private func createLocalizationStrings(_ translations: Translations, languages: [Language], baseId: String = "Base", baseLanguage: Language = Language.en) throws {
    try languages.forEach {
        let content = translationFileContents(translations, forLanguage: $0)
        let path = "MarshallBluetooth®/InternalFrameworks/MarshallBluetoothLibrary/Locales/\($0.rawValue).lproj/Localizable.strings"
        try content.write(toFile: path, atomically: true, encoding: .utf8)
    }
    let content = translationFileContents(translations, forLanguage: baseLanguage)
    let path = "MarshallBluetooth®/InternalFrameworks/MarshallBluetoothLibrary/Locales/\(baseId).lproj/Localizable.strings"
    try content.write(toFile: path, atomically: true, encoding: .utf8)
}

private func generateStringsCode(_ entries: Translations, languages: [Language], base: Language = Language.en) throws {
    
    var staticStringsLines: [String] = []
    
    staticStringsLines.append("//=======================================================================")
    staticStringsLines.append("//")
    staticStringsLines.append("// This file is generated. Do not edit.")
    staticStringsLines.append("//")
    staticStringsLines.append("//=======================================================================")
    staticStringsLines.append("")
    staticStringsLines.append("// swiftlint:disable valid_docs")
    staticStringsLines.append("// swiftlint:disable line_length")
    staticStringsLines.append("public enum Strings {")

    
    entries.all.sorted { $0.label < $1.label }
        .forEach { resource in
    
            let baseEntry = resource.translations.filter {
                $0.lang == base.rawValue
                }.first!
            
            staticStringsLines.append(" /**")
            staticStringsLines.append("\"\(baseEntry.translation)\"\n")
            
            languages.forEach { language in
                let entry = resource.translations.filter {
                    $0.lang == language.rawValue
                    }.first!
                staticStringsLines.append("     - **\(language.rawValue)**: \"\(entry.translation)\"")
            }
            staticStringsLines.append(" */")
            
            let funcName = resource.label.replacingOccurrences(of: ".", with: "_")
            let argumentNames = funcArgumentNames(baseEntry.translation)
            
            staticStringsLines.append("  public static func \(funcName)(\(funcArguments(argumentNames))) -> String {")
            staticStringsLines.append("     return localizedString(")
            staticStringsLines.append("         key:\"\(resource.label)\",")
            staticStringsLines.append("         defaultValue:\"\(escaped(baseEntry.translation))\",")
            staticStringsLines.append("         substitutions: \(funcSubstitutions(baseEntry.translation)))")
            staticStringsLines.append("  }\n")
    }
    staticStringsLines.append("}\n")
    let path = "MarshallBluetooth®/InternalFrameworks/MarshallBluetoothLibrary/Locales/Strings.swift"
    
    try staticStringsLines
        .joined(separator: "\n")
        .write(toFile: path, atomically: true, encoding: .utf8)
}

public func parseTranslations(path: String) {
    
    let jsonDecoder = JSONDecoder()
    do {
        let translations = try translationsFilePath
            .flatMap(String.init(contentsOfFile:))
            .flatMap { $0.data(using: .utf8) }
            .flatMap { try jsonDecoder.decode(Translations.self, from: $0) }
        guard let parsed = translations else {
            fatalError("Error while trying to parse translations file")
        }

        // Check parsed translations for missing keys
        checkMissingTranslations(parsed, languages: Language.allLanguages)

        // Generate Localization.strings files for every supported language (+ Base translation)
        try createLocalizationStrings(parsed, languages: Language.allLanguages)

        // Generate Strings.swift file
        try generateStringsCode(parsed, languages: Language.allLanguages)
        
    } catch {
        dump(error)
        fatalError("Error while parsing translations on path: \(path)")
    }
}

