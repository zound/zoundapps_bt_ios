//
//  VersionTests.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Bartosz Dolewski on 19.09.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

class VersionTests: XCTestCase {
    func testIfNewer() {
        let testVersion = Version(version: "1.2.3")
        
        XCTAssertTrue(testVersion > Version(version: "1.2.2"))
        XCTAssertTrue(testVersion > Version(version: "1.0.0"))
        
        XCTAssertTrue(testVersion > Version(version: "1.0"))
        XCTAssertTrue(testVersion > Version(version: "1.2"))
        XCTAssertTrue(testVersion > Version(version: "1"))
        
        XCTAssertTrue(testVersion > Version(version: ""))
    }
    
    func testIfOlder() {
        let testVersion = Version(version: "5.6.7")
        
        XCTAssertTrue(testVersion < Version(version: "10.2.2"))
        XCTAssertTrue(testVersion < Version(version: "10.0.0"))
        
        XCTAssertTrue(testVersion < Version(version: "10.0"))
        XCTAssertTrue(testVersion < Version(version: "10.2"))
        XCTAssertTrue(testVersion < Version(version: "10"))
    }
    
    func testIfTheSame() {        
        XCTAssertTrue(Version(version: "3.2.1") == Version(version: "3.2.1"))
        XCTAssertTrue(Version(version: "1.2") == Version(version: "1.2.0"))
        XCTAssertTrue(Version(version: "1.2") == Version(version: "1.2.0"))
        XCTAssertTrue(Version(version: "1") == Version(version: "1.0.0"))
        XCTAssertTrue(Version(version: "") == Version(version: "0.0.0"))
    }
}
