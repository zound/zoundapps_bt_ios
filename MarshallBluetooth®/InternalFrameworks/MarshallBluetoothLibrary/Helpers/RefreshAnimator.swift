//
//  RefreshAnimator.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 16/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import CoreGraphics
import UIKit

class RefreshAnimator:RefreshViewAnimator {
    
    init(refreshView: DevicesListRefreshView) {
        self.refreshView = refreshView
    }
    
    func animate(_ state: State) {
        
        switch state {
        case .initial:
            applyTransformFoProgress(progress: 0)
        case .releasing(let progress):
            applyTransformFoProgress(progress: progress)
        case .loading:
            refreshView.refreshIndicator.rotate(1.0)
        case .finished:
            refreshView.refreshIndicator.stopRotation()
            UIView.animate(withDuration: 0.25, delay: 0.0, options: [.beginFromCurrentState], animations: { [weak self] in
                self?.refreshView.refreshIndicator.alpha = 0.0
                }, completion: nil)
            
        }
    }
    
    func applyTransformFoProgress(progress: CGFloat) {
        
        self.refreshView.refreshIndicator.alpha = 1.0
        var transform  = CGAffineTransform.identity
        transform = transform.rotated(by: (progress * 2 * CGFloat.pi))
        transform = transform.scaledBy(x: min(1.0,progress), y: min(1.0,progress))
        
        refreshView.refreshIndicator.transform = transform
    }
    
    private let refreshView: DevicesListRefreshView
}

