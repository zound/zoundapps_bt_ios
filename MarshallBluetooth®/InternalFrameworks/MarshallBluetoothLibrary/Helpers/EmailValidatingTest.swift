//
//  EmailValidatingTest.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Dolewski Bartosz A (Ext) on 14.03.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
@testable import MarshallBluetoothLibrary

struct TestValidator: EmailValidating {}

class EmailValidatingTest: XCTestCase {
    
    fileprivate let validator = TestValidator()
    
    func testCorrectEmailAddress() {
        XCTAssertTrue(validator.validate(email: "some.user@domain.com"))
        XCTAssertTrue(validator.validate(email: "someuser@domain.com"))
        XCTAssertTrue(validator.validate(email: "USER@domain.com"))
        XCTAssertTrue(validator.validate(email: "some.user@DOMAIN.COM"))
    }
    
    func testIncorrectEmailAddress() {
        // empty e-mail
        XCTAssertFalse(validator.validate(email: ""))
        
        // bad domain
        XCTAssertFalse(validator.validate(email: "user@domain..com"))
        XCTAssertFalse(validator.validate(email: "user@domain,com"))
        XCTAssertFalse(validator.validate(email: "user@domain?com"))
        XCTAssertFalse(validator.validate(email: "@"))
        XCTAssertFalse(validator.validate(email: "user"))
        
        // bad usernames
        XCTAssertFalse(validator.validate(email: "@domain.com"))
        XCTAssertFalse(validator.validate(email: "@@domain.com"))
        XCTAssertFalse(validator.validate(email: "some user@domain.com"))
        XCTAssertFalse(validator.validate(email: "some,user@domain.com"))
        XCTAssertFalse(validator.validate(email: "user;@domain.com"))
        XCTAssertFalse(validator.validate(email: "user<>@domain.com"))
        XCTAssertFalse(validator.validate(email: "user..@domain.com"))
        XCTAssertFalse(validator.validate(email: "1234567890123456789012345678901234567890123456789012345678901234+x@@domain.com"))
    }
}
