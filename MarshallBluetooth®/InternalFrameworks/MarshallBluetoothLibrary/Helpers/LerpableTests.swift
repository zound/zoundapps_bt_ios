//
//  LerpableTests.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Bartosz Dolewski on 29.05.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//


import XCTest
@testable import MarshallBluetoothLibrary

fileprivate struct LedFilterInterpolator: Lerpable {}

class LerpableTests: XCTestCase {
    fileprivate let interpolator = LedFilterInterpolator()
    
    func testTypicalOK() {
        let x = 50.0
        let y = 100.0
        let percentage = 0.5
        
        if let result = interpolator.lerp(x: x, y: y, percentage: percentage) {
            XCTAssertEqual(result, 75.0)
        } else {
            XCTFail("Percentage result cannot be nil in this scenario")
        }
    }
    
    func testNegativeValues() {
        var x = -100.0
        var y = -50.0
        let percentage = 0.5
        
        if let result = interpolator.lerp(x: x, y: y, percentage: percentage) {
            XCTAssertEqual(result, -75.0)
        } else {
            XCTFail("Percentage result cannot be nil in this scenario")
        }
        
        x = -50.0
        y = -100.0
        if let result = interpolator.lerp(x: x, y: y, percentage: percentage) {
            XCTAssertEqual(result, -75.0)
        } else {
            XCTFail("Percentage result cannot be nil in this scenario")
        }
        
        x = -50.0
        y = 50.0
        if let result = interpolator.lerp(x: x, y: y, percentage: percentage) {
            XCTAssertEqual(result, 0.0)
        } else {
            XCTFail("Percentage result cannot be nil in this scenario")
        }
    }
    
    func testPercentageIsNoFromZeroToOne() {
        let x = 50.0
        let y = 100.0

        XCTAssertNil(interpolator.lerp(x: x, y: y, percentage: 2.1), "Should be nil in this scenario")
        XCTAssertNil(interpolator.lerp(x: x, y: y, percentage: -2.1), "Should be nil in this scenario")
    }
    
    func testPercentageIsZero() {
        let x = 50.0
        let y = 100.0
        let percentage = 0.0
        
        if let result = interpolator.lerp(x: x, y: y, percentage: percentage) {
            XCTAssertEqual(result, 50)
        } else {
            XCTFail("Percentage result cannot be nil in this scenario")
        }
    }
    
    func testPercentageIsOne() {
        let x = 50.0
        let y = 100.0
        let percentage = 1.0
        
        if let result = interpolator.lerp(x: x, y: y, percentage: percentage) {
            XCTAssertEqual(result, 100.0)
        } else {
            XCTFail("Percentage result cannot be nil in this scenario")
        }
    }
}

