//
//  Strongify.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 02/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

/// Syntactic-sugar helpers to avoid weak-strong manipulations inside closures
/// and keep possibiblity to use point-free style coding, ensuring there are no memory leaks
/// Inspired by:
/// http://merowing.info/2017/04/stop-weak-strong-dance/

import Foundation

func noArgumentsStrongify<WeakRef: AnyObject>(_ weakRef: WeakRef, _ closure: @escaping ((WeakRef) -> () -> Void)) -> (() -> Void) {
    return { [weak weakRef] in
        guard let strongified = weakRef else { return }
        closure(strongified)()
    }
}

func strongify<WeakRef: AnyObject, Input, Output>(_ weakRef: WeakRef,
                                                           _ valueIfWeakNilled: Output,
                                                           _ closure: @escaping ((WeakRef) -> (Input) -> Output)) -> (Input) -> Output {
    return { [weak weakRef] input in
        guard let strongified = weakRef else { return valueIfWeakNilled }
        return closure(strongified)(input)
    }
}

func strongify<WeakRef: AnyObject, Input1, Input2, Output>(_ weakRef: WeakRef,
                                                  _ valueIfWeakNilled: Output,
                                                  _ closure: @escaping ((WeakRef) -> (Input1, Input2) -> Output)) -> (Input1, Input2) -> Output {
    return { [weak weakRef] (input1, input2) in
        guard let strongified = weakRef else { return valueIfWeakNilled }
        return closure(strongified)(input1, input2)
    }
}
