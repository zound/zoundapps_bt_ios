//
//  DevicesListPullToRefresh.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 16/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

final class DevicesListPullToRefresh: PullToRefresh {
    
    convenience init() {
        let refreshView = UIView.instantiate(DevicesListRefreshView.self)
        refreshView.translatesAutoresizingMaskIntoConstraints = true
        refreshView.autoresizingMask = [.flexibleWidth]
        let animator = RefreshAnimator(refreshView: refreshView)
        self.init(refreshView: refreshView, animator: animator, height: 0, position: .top)
    }
    
}

