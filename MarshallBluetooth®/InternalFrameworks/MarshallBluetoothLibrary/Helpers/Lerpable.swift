//
//  File.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 29.05.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

public protocol Lerpable {
    func lerp(x: Double, y: Double, percentage: Double) -> Double?
}

extension Lerpable {
    func lerp(x: Double, y: Double, percentage: Double) -> Double? {
        guard (0.0 ... 1.0).contains(percentage) else { return nil }
        
        return  (1 - percentage) * x + percentage * y;
    }
}
