//
//  Keyboard.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 21/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift

public struct KeyboardInfo {
    var frame: CGRect
    var duration: TimeInterval
    var options: UIViewAnimationOptions
    var notificationName: Notification.Name
}

public final class Keyboard {
    
    public static let shared  = Keyboard()

    public static var info: PublishSubject<KeyboardInfo> {
        return self.shared.changedInfo
    }
    
    private let changedInfo = PublishSubject<KeyboardInfo>()
    
    private init() {
        NotificationCenter.default.addObserver(
            self, selector: #selector(updateInfo(_:)), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(
            self, selector: #selector(updateInfo(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc private func updateInfo(_ notification: Notification) {
        guard let userInfo = notification.userInfo,
              let frame = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue,
              let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber,
              let curveNumber = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber,
              let curve = UIViewAnimationCurve(rawValue: curveNumber.intValue) else {
            return
        }
        changedInfo.onNext(KeyboardInfo(frame: frame.cgRectValue,
                                        duration: duration.doubleValue,
                                        options: UIViewAnimationOptions(rawValue: UInt(curve.rawValue)),
                                        notificationName: notification.name))
    }
}
