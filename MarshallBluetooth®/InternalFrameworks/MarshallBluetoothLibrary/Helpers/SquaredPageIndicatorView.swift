//
//  SquaredPageIndicatorView.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 13/04/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

final class Square: UIView {
    func configure(color: UIColor, size: CGSize) {
        self.backgroundColor = color
        NSLayoutConstraint.activate([
            self.widthAnchor.constraint(equalToConstant: size.width),
            self.heightAnchor.constraint(equalToConstant: size.height)
        ])
    }
}

final class SquaredPageIndicatorView: UIView {
    var stack: UIStackView!
    var currentIdx: Int = 0
    var inactiveColor = Color.pageIndicatorInactive
    var activeColor = Color.pageIndicatorActive
    var indicators = [UIView]()
    
    func configure(numPages: Int) {
        for idx in 0..<numPages {
            let square = Square()
            square.configure(color: idx == currentIdx ? activeColor : inactiveColor, size: CGSize.squaredPageIndicator())
            indicators.append(square)
        }
        stack = UIStackView(arrangedSubviews: indicators)
        stack.spacing = 10.0
        stack.distribution = .fillEqually
        stack.alignment = .center
        stack.translatesAutoresizingMaskIntoConstraints = false
    
        self.addSubview(stack)
    
        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: self.topAnchor),
            stack.leftAnchor.constraint(equalTo: self.leftAnchor),
            stack.rightAnchor.constraint(equalTo: self.rightAnchor),
            stack.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])

    }
    
    var currentPage: Int = 0 {
        didSet {
            guard currentPage < indicators.count else {
                fatalError("Page idx outside of bounds!")
            }
            indicators[currentIdx].backgroundColor = Color.pageIndicatorInactive
            indicators[currentPage].backgroundColor = Color.pageIndicatorActive
            currentIdx = currentPage
        }
    }
}
