//
//  EmailValidating.swift
//  MarshallBluetoothLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 14.03.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

/// Protocol for validating e-mail adress
protocol EmailValidating {
    /// E-mail adress validation
    ///
    /// - Parameter email: e-mail adress to be validated
    /// - Returns: true if e-mail is valid, false otherwise
    func validate(email: String) -> Bool
}

// MARK: - Default implementation of EmailValidating
extension EmailValidating {
    
    /// Default implementation of e-mail validation (compliant to RFC 2822)
    ///
    /// - Parameter email: e-mail adress to be validated
    /// - Returns: true if e-mail is valid, false otherwise
    func validate(email: String) -> Bool {
        let emailRegEx = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"+"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"+"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"+"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"+"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"+"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailPredicate = NSPredicate(format: "SELF MATCHES[c] %@", emailRegEx)
        return emailPredicate.evaluate(with: email)
    }
}
