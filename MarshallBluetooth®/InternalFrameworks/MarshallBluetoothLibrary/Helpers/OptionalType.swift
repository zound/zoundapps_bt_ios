/// An optional protocol for use in type constraints
public protocol OptionalType {
    /// The type contained in the optional.
    associatedtype Wrapped
    
    /// Extracts an optional from the receiver.
    var optional: Wrapped? { get }
}

extension Optional: OptionalType {
    public var optional: Wrapped? {
        return self
    }
}

extension OptionalType {
    public func filter(_ predicate: (Wrapped) -> Bool) -> Wrapped? {
        guard let value = self.optional, predicate(value) else {
            return nil
        }
        return value
    }
    
    public func coalesceWith(_ value: @autoclosure () -> Wrapped) -> Wrapped {
        return self.optional ?? value()
    }
}
