//
//  PercentableTests.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Bartosz Dolewski on 29.05.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
@testable import MarshallBluetoothLibrary

fileprivate struct LedDoubleensityFormatter: Percentable {}

class PercentableTests: XCTestCase {
    
    fileprivate let formatter = LedDoubleensityFormatter()
    
    func testTypicalOK() {
        var min = 50.0
        var max = 100.0
        var current = 75.0
        
        if let result = formatter.percentage(min: min, max: max, current: current) {
            XCTAssertEqual(result, 0.5)
        } else {
            XCTFail("Percentage result cannot be nil in this scenario")
        }
        
         min = 5.0
         max = 10.0
         current = 7.5
        
        if let result = formatter.percentage(min: min, max: max, current: current) {
            XCTAssertEqual(result, 0.5)
        } else {
            XCTFail("Percentage result cannot be nil in this scenario")
        }
        
        min = 5.0
        max = 10.0
        current = 5.0
        
        if let result = formatter.percentage(min: min, max: max, current: current) {
            XCTAssertEqual(result, 0.0)
        } else {
            XCTFail("Percentage result cannot be nil in this scenario")
        }
        
        min = 5.0
        max = 10.0
        current = 10.0
        
        if let result = formatter.percentage(min: min, max: max, current: current) {
            XCTAssertEqual(result, 1.0)
        } else {
            XCTFail("Percentage result cannot be nil in this scenario")
        }
        
        min = 0.0
        max = 10.0
        current = 4.3
        
        if let result = formatter.percentage(min: min, max: max, current: current) {
            XCTAssertEqual(result, 0.43)
        } else {
            XCTFail("Percentage result cannot be nil in this scenario")
        }
    }
    
    func testMinimumIsTooBig() {
        let min = 200.0
        let max = 100.0
        let current = 75.0
        
        let result = formatter.percentage(min: min, max: max, current: current)
        XCTAssertNil(result, "Should be nil in this scenario")
    }
    
    func testCurrentOutsideScale() {
        let min = 50.0
        let max = 100.0
        let current = 750.0
        
        let result = formatter.percentage(min: min, max: max, current: current)
        XCTAssertNil(result, "Should be nil in this scenario")
    }
    
    func testIfDividingByZero() {
        let min = 100.0
        let max = 100.0
        let current = 75.0
        
        let result = formatter.percentage(min: min, max: max, current: current)
        XCTAssertNil(result, "Should be nil in this scenario")
    }
}
