//
//  Percentable.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 29.05.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

public protocol Percentable {
    func percentage(min: Double, max: Double, current: Double) -> Double?
}


extension Percentable {
    func percentage(min: Double, max: Double, current: Double) -> Double? {
        guard (max - min) != 0 else { return nil }
        guard max >= min else { return nil }
        guard current <= max && current >= min else { return nil }
        
        return ((current - min)) / (max - min)
    }
}

