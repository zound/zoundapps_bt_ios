//
//  DeviceModel.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 10/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

private struct DeviceModelName {
    static let actonII = "ACTON II"
    static let monitorII = "MONITOR II A.N.C."
    static let stanmoreII = "STANMORE II"
    static let woburnII = "WOBURN II"
}

public enum DeviceModel {
    case actonII
    case monitorII
    case stanmoreII
    case woburnII

    init?(name: String) {
        switch name {
        case DeviceModelName.actonII:
            self = .actonII
        case DeviceModelName.monitorII:
            self = .monitorII
        case DeviceModelName.stanmoreII:
            self = .stanmoreII
        case DeviceModelName.woburnII:
            self = .woburnII
        default:
            return nil
        }
    }
}

public extension DeviceModel {
    var name: String {
        switch self {
        case .actonII:
            return DeviceModelName.actonII
        case .monitorII:
            return DeviceModelName.monitorII
        case .stanmoreII:
            return DeviceModelName.stanmoreII
        case .woburnII:
            return DeviceModelName.woburnII
        }
    }
}
