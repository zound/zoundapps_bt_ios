//
//  JSONMockOTAAdapter.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 28/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//
import RxSwift
import GUMA
import RCER

public final class JSONMockOTAAdapter: OTAAdapterTypeInput, OTAAdapterTypeOutput, OTAAdapterType {
    public func completeUpdate(deviceID: String) { }
    public func checkForcedOTA(deviceID: String) -> Single<UpdateAvailableInfo?> {
        return Single.just(nil)
    }
    
    public func startUpdate(cachedFirmware: FirmwareCache?, interfaceError: BehaviorRelay<DeviceAdapterError?>, processMode: PublishRelay<ApplicationProcessMode>) { }
    
    public func checkUpdateAvailable(deviceID: String) -> PrimitiveSequence<SingleTrait, UpdateAvailableInfo?> {
        return Single.just(nil)
    }
    
    public var adapterError = BehaviorRelay<AnyError<OTAError>?>.init(value: nil)

    public var inputs: OTAAdapterTypeInput { return self }
    public var outputs: OTAAdapterTypeOutput { return self }
    public var deviceID: String
    
    public func startUpdate(requestType: OTARequestType, cachedFirmware: FirmwareCache?, interfaceError: BehaviorRelay<DeviceAdapterError?>) {
        
        guard _updateTickTimer == nil || !_updateTickTimer!.isValid else { return }

        let timeInterval = TimeInterval(exactly: 0.1)!
        let fireDate    = Date(timeIntervalSinceNow: timeInterval)

        _currentUpdateIdx = _mockedOTAFlow.startIndex

        _updateTickTimer = Timer(fire: fireDate, interval: timeInterval, repeats: true, block: { [unowned self, updateProgress, _mockedOTAFlow] timer in

            let currentProgress = _mockedOTAFlow[self._currentUpdateIdx]

            if case .completed = currentProgress.status {
                updateProgress.onNext(OTAUpdateInfo(deviceID: currentProgress.deviceID,
                                                    status: currentProgress.status,
                                                    error: nil))
                updateProgress.onCompleted()
                self.updateProgress = PublishSubject<OTAUpdateInfo>()
                timer.invalidate()
                return

            }
            updateProgress.onNext(OTAUpdateInfo(deviceID: currentProgress.deviceID,
                                                status: currentProgress.status,
                                                error: nil))
            self._currentUpdateIdx = self._currentUpdateIdx.advanced(by: 1)
        })
        guard let timer = _updateTickTimer else { return }
        RunLoop.main.add(timer, forMode: RunLoopMode.commonModes)
    }

    public func deviceDisconnected() { }
    
    public func cancel() { }
    
    init(hwDevice device: JSONMockHardwareDevice, hasUpdate: Bool) {
        self._underlyingHw = device
        self.deviceID = _underlyingHw.id
        self._hasUpdate = hasUpdate
    
        self._mockedOTAFlow = [OTAUpdateInfo(deviceID: deviceID, status: .checkingFirmware, error: nil)]
        self._mockedOTAFlow.append(contentsOf: generateMockedDownloadingFirmwareEvents())
        self._mockedOTAFlow.append(contentsOf: generateMockedUploadFirmwareEvents())
        self._mockedOTAFlow.append(OTAUpdateInfo(deviceID: deviceID, status: .uploadCompleted(0.0), error: nil))
        self._mockedOTAFlow.append(OTAUpdateInfo(deviceID: deviceID, status: .completed, error: nil))
    }

    private func generateMockedDownloadingFirmwareEvents() -> [OTAUpdateInfo] {
        return stride(from: 0, to: 1, by: 0.05).map { OTAUpdateInfo(deviceID: deviceID, status: .downloadingFirmware($0, nil), error: nil) }
    }
    
    private func generateMockedUploadFirmwareEvents() -> [OTAUpdateInfo] {
        return stride(from: 0, to: 1, by: 0.05).map { OTAUpdateInfo(deviceID: deviceID, status: .uploadingToDevice($0), error: nil) }
    }
    
    public var updateAvailable = PublishSubject<Bool>()
    public var updateProgress = PublishSubject<OTAUpdateInfo>()
    var updateEnded = PublishSubject<Bool>()

    private var _updateTickTimer: Timer?
    private var _currentUpdateIdx = 0
    
    private var _underlyingHw: JSONMockHardwareDevice
    private var _hasUpdate: Bool
    private var _mockedOTAFlow: [OTAUpdateInfo]
}
