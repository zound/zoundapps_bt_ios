//
//  HelperJSONMockDeviceState.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 22/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

struct HelperJSONMockNameValidator: CustomNameValidator {
    func validate(name: String) -> CustomNameValidationResult { return .pass }
}

fileprivate var helperJSONMockTrackInfo = TrackInfo(title: "Freedy Freeloader",
                                                    artist: "Miles Davis",
                                                    album: "Kind of Blue",
                                                    number: "2",
                                                    totalNumber: "5",
                                                    genre: "Jazz",
                                                    playingTime: "9:46")

public final class HelperJSONMockDeviceState: DeviceStateType {
    
    public var inputs: DeviceStateInput { return self }
    public var outputs: DeviceStateOutput { return self }
    
    public static let supportedFeatures: [Feature] = [.hasContinuousVolume,
                                                      .hasConnectivityStatus,
                                                      .hasDeviceInfo,
                                                      .hasGraphicalEqualizer,
                                                      .hasLedBrightnessControl,
                                                      .hasConnectivityStatus,
                                                      .hasCustomizableName,
                                                      .hasForcedOTA,
                                                      .hasSwitchableAudioSource,
                                                      .hasBatteryStatus,
                                                      .hasAudioPlayback]
    public let perDeviceTypeFeatures: [Feature]
    init(underlyingHardware: JSONMockHardwareDevice) {
        
        self._name = AnyState<String>(feature: .hasCustomizableName,
                                      supportsNotifications: false,
                                      initialValue: { return String() })
        self._batteryStatus = AnyState<BatteryStatus>(feature: .hasBatteryStatus,
                                                      supportsNotifications: false, initialValue: { .l40(45) })
        self._volumeState = AnyState<Int>(feature: .hasContinuousVolume,
                                          supportsNotifications: false,
                                          initialValue: { return Int() })
        
        self._connectivityStatus = AnyState<ConnectivityStatus>(feature: .hasConnectivityStatus,
                                                                supportsNotifications: false,
                                                                initialValue: { return .notConfigured })
        
//        self._connectMode = AnyState<ConnectMode>(feature: .hasConnectivityStatus,
//                                                  supportsNotifications: false,
//                                                  initialValue: { return .idle })
        
        self._ledBrightness = AnyState<Int>(feature: .hasLedBrightnessControl,
                                            supportsNotifications: false,
                                            initialValue: { return Int() })
        
        self._soundsControlStatus = AnyState<SoundsControl>(feature: .hasSoundsControl,
                                                            supportsNotifications: false, initialValue: { return .powerCue(on: false) })
        
        self._currentAudioSource = AnyState<AudioSource>(feature: .hasSwitchableAudioSource,
                                                         supportsNotifications: false,
                                                         initialValue: { return AudioSource.bluetooth })
        
        self._currentPlaybackStatus = AnyState<PlaybackStatus>(feature: .hasAudioPlayback,
                                                               supportsNotifications: false,
                                                               initialValue: { return PlaybackStatus.unknown })
        
        self._currentTrackInfo = AnyState<TrackInfo>(feature: .hasAudioPlayback,
                                                     supportsNotifications: false,
                                                     initialValue: { return helperJSONMockTrackInfo })
        self._underlyingHW = underlyingHardware

        guard let type = MockedUnitType(manufacturerData: Data(bytes: underlyingHardware.advertisementBytes)) else {
            self.perDeviceTypeFeatures = []
            return
        }
        switch type {
        case .actonII, .stanmoreII, .woburnII:
            self.perDeviceTypeFeatures = HelperJSONMockDeviceState.supportedFeatures.filter { $0 != .hasBatteryStatus }
        case .actonIILite, .stanmoreIILite, .woburnIILite:
            self.perDeviceTypeFeatures = HelperJSONMockDeviceState.supportedFeatures.filter { $0 != .hasForcedOTA && $0 != .hasBatteryStatus }
        case .ozzy, .ozzyAnc:
            self.perDeviceTypeFeatures = [.hasContinuousVolume,
                                          .hasDeviceInfo,
                                          .hasGraphicalEqualizer,
                                          .hasCustomizableName,
                                          .hasConnectivityStatus,
                                          .hasAudioPlayback,
                                          .hasBatteryStatus]
        }
    }
    
    fileprivate let _name: AnyState<String>
    fileprivate let _batteryStatus: AnyState<BatteryStatus>
    fileprivate let _underlyingHW: JSONMockHardwareDevice
    fileprivate let _volumeState: AnyState<Int>
    fileprivate let _connectivityStatus: AnyState<ConnectivityStatus>
    //fileprivate let _connectMode: AnyState<ConnectMode>
    fileprivate let _ledBrightness: AnyState<Int>
    fileprivate let _soundsControlStatus: AnyState<SoundsControl>
    fileprivate let _currentAudioSource: AnyState<AudioSource>
    fileprivate let _currentPlaybackStatus: AnyState<PlaybackStatus>
    fileprivate let _currentTrackInfo: AnyState<TrackInfo>
    fileprivate let disposeBag = DisposeBag()
    fileprivate var _activeMonitors = Set<Monitor>()
}

extension HelperJSONMockDeviceState: DeviceStateInput {
    public func startMonitoringEqualizerButtonStep() {}
    public func stopMonitoringEqualizerButtonStep() {}
    public func requestCurrentTrackInfo() {}
    public func set(volume: Int) {}
    public func changeVolume(value: NormalizedVolume, immediateUpdate: Bool) {}
    public func requestVolume() {}
    public func connect() {
        self.connectivityStatus().stateObservable().accept(.connected)
    }
    public func requestPlaybackStatus() {
        let connected = self.connectivityStatus().stateObservable().value == .connected
        self._currentPlaybackStatus.stateObservable().accept(connected ? .playing : .paused)
    }
    public func refreshName() {}
    public func requestBatteryStatus() {
        self._batteryStatus.stateObservable().accept(.l60(67))
    }
    public func requestSoundsControlStatus() {}
    public func write(soundControl: SoundsControl) {}
    public func requestLedBrightness() {}
    public func set(connectivityStatus: ConnectivityStatus) {
        _connectivityStatus.stateObservable().accept(connectivityStatus)
    }
    public func requestCurrentAudioSource() {
        _currentAudioSource.stateObservable().accept(.aux)
    }
    public func startMonitoringAudioSources() {}
    public func stopMonitoringAudioSources() {}
    public func startMonitoringVolume() {}
    public func stopMonitoringVolume() {}
    public func activateBluetooth() {
        _currentAudioSource.stateObservable().accept(.bluetooth)
    }
    public func activateAUX() {
        _currentAudioSource.stateObservable().accept(.aux)
    }
}

extension HelperJSONMockDeviceState: DeviceStateOutput {
    public var activeMonitors: Set<Monitor> {
        get {
            return _activeMonitors
        }
        set(newValue) {
            _activeMonitors = newValue
        }
    }
    public func name() -> AnyState<String> {
        return _name
    }
    public func ledBrightness() -> AnyState<Int> {
        return _ledBrightness
    }
    public func batteryStatus() -> AnyState<BatteryStatus> {
        return _batteryStatus
    }
    public func volume() -> AnyState<Int> {
        return _volumeState
    }
    public func monitoredVolume() -> AnyState<Int> {
        return _volumeState
    }
    public func soundsControlStatus() -> AnyState<SoundsControl> {
        return _soundsControlStatus
    }
    public func connectivityStatus() -> AnyState<ConnectivityStatus> {
        return _connectivityStatus
    }
    public func supportedAudioSources() -> [AudioSource] {
        return [.bluetooth, .aux]
    }
    public func currentAudioSource() -> AnyState<AudioSource> {
        return _currentAudioSource
    }
    public func playbackStatus() -> AnyState<PlaybackStatus> {
        return _currentPlaybackStatus
    }
    public func trackInfo() -> AnyState<TrackInfo> {
        return _currentTrackInfo
    }
}
