//
//  JSONMockDevice.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 10/10/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import GUMA
import RxSwift

private extension MockedUnitType {
    var modelName: String {
        switch self {
        case .actonII:
            return "ACTON II"
        case .stanmoreII:
            return "STANMORE II"
        case .woburnII:
            return "WOBURN II"
        case .actonIILite:
            return "ACTON II LITE"
        case .stanmoreIILite:
            return "STANMORE II LITE"
        case .woburnIILite:
            return "WOBURN II LITE"
        case .ozzy:
            return "MONITOR II"
        case .ozzyAnc:
            return "MONITOR II ANC"
        }
    }
}

private struct Config {
    static let connectionTimeout = TimeInterval.seconds(5.0)
}

public final class JSONMockDevice: DeviceType {
    public func bleConnect() -> Single<Void> {
        return Single.just(())
    }
    public var configured: Bool = false
    
    public func resetSlave() -> Single<Void> {
        return Single.just(())
    }
   
    public func decouple() -> Single<Void> {
        return Single.just(())
    }
    public var mac: MAC?
    
    public func initialSetup(with storedEntry: StoredDeviceInfoEntry?) -> Single<String> {
        return Single.just(String())
    }

    public var modelName: String {
        let manufacturerData = Data(bytes: _underlyingHw.advertisementBytes)
        let unitType = MockedUnitType(manufacturerData: manufacturerData)
        return unitType?.modelName ?? String()
    }
    
    public var connectionTimeout: TimeInterval {
       return Config.connectionTimeout
    }
    
    public var otaAdapter: OTAAdapterType? {
        return _otaAdapter
    }
    
    public let platform: Platform
    
    public var name = BehaviorRelay<String>.init(value: String())
    
    public var id: String {
        return _underlyingHw.id
    }
    public var hardwareType: HardwareType {
        let manufacturerData = Data(bytes: _underlyingHw.advertisementBytes)
        guard let unitType = MockedUnitType(manufacturerData: manufacturerData) else {
            return .speaker
        }
        switch unitType {
        case .ozzy, .ozzyAnc:
            return .headset
        default: return .speaker
        }
    }
    public var image: DeviceImageProviding = MockedDeviceImage(speakerType: .actonII(.notSupported), mac: String())
    
    public var state: DeviceStateType {
        return _state
    }
    
    init(hwDevice device: JSONMockHardwareDevice, hasUpdate: Bool, platform: Platform) {
        self._underlyingHw = device
        self._otaAdapter = JSONMockOTAAdapter(hwDevice: device, hasUpdate: hasUpdate)
        self._state = HelperJSONMockDeviceState(underlyingHardware: device)
        let manufacturerData = Data(bytes: _underlyingHw.advertisementBytes)
        guard let unitType = MockedUnitType(manufacturerData: manufacturerData) else {
            self.name.accept(String())
            _state.inputs.set(connectivityStatus: _underlyingHw.connected ? .connected : .notConfigured)
            self.platform = platform
            return
        }
        self.name.accept(unitType.modelName + "(JSON)")
        self.image = MockedDeviceImage(speakerType: unitType, mac: String())
        self.platform = platform
    }
    
    private var _state: DeviceStateType
    private var _otaAdapter: JSONMockOTAAdapter
    private var _underlyingHw: JSONMockHardwareDevice
}
