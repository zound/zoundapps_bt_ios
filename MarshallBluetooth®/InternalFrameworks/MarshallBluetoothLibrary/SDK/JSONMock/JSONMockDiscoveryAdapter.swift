//
//  JSONMockDiscoveryAdapter.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 22/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

struct JsonMockNameValidator: CustomNameValidator {
    func validate(name: String) -> CustomNameValidationResult {
        // everything is fine for this parser
        return .pass
    }
}

struct DeviceBlankImageProvider: DeviceImageProviding {
    var smallImage: UIImage {
        return UIImage()
    }
    var mediumImage: UIImage {
        return UIImage()
    }
    var largeImage: UIImage {
        return UIImage()
    }
    var pairingSetupImage: UIImage {
        return UIImage()
    }
}

let localJSON = """
{
    "scanDelay" : 500,
    "continuousScan" : false,
    "devs" : [
        {"name" : "STANMORE II LITE", "id" : "00:a2:c2:d5:d4:a3", "playing" : false, "connected" : false, "hasUpdate" : true, "advertisementBytes": [18, 4]},
        {"name" : "ACTON II LITE", "id" : "00:1c:d3:a1:14:a2",  "playing" : true, "connected" : true, "hasUpdate" : false, "advertisementBytes": [18, 3]},
        {"name" : "WOBURN II", "id" : "00:2c:1c:a1:f5:a3",  "playing" : false, "connected" : false, "hasUpdate" : false, "advertisementBytes": [16, 2]},
        {"name" : "ACTON II WHITE", "id" : "00:ff:3e:37:d3:d4",  "playing" : false, "connected" : false, "hasUpdate" : false, "advertisementBytes": [16, 0]},
        {"name" : "OZZY ANC", "id" : "00:fe:dd:37:d3:a5",  "playing" : true, "connected" : true, "hasUpdate" : false, "advertisementBytes": [18, 17, 156, 13, 172, 4, 148, 228, 0, 0]}
    ]
}
"""

public struct MocksProvider: Codable {
    let scanDelay: Int
    let continuousScan: Bool
    let devs: [JSONMockHardwareDevice]
}

public class JSONMockDeviceAdapter: DeviceAdapterType {
    
    public func setupDevices() -> Single<Void> {
        return Single.just(())
    }
    
    //public var globalConnectMode: ConnectMode? = nil
    
    public func configurationInProgress() -> Bool { return false }
    
    //public func set(mode: ConnectMode, for deviceID: String) { }

    public let discovered = PublishRelay<DeviceType>()
    public let removed = PublishRelay<DeviceType>()

    public var adapterState = BehaviorRelay<DeviceAdapterState>.init(value: .ready)
    public var scanInProgress = BehaviorRelay<Bool>.init(value: false)
    public var preprocessingDiscoveredInProgress = BehaviorRelay<Bool>.init(value: false)
    
    public static var platform: Platform = Platform(id: "joplinBTMock",
                                                    traits: PlatformTraits(connectionTimeout: TimeInterval.seconds(2.0)))
    
    public static let shared = JSONMockDeviceAdapter()

    public func startScan(mocked: Bool = false) {
        scanInProgress.accept(true)
        restartAdapter()
        requestForMock(remote: !mocked) { [unowned self] provider in
            self.mocksProvider = provider
            self.scanDelay = provider.scanDelay
            self.continuousScan = provider.continuousScan
        }
    }
    
    public func managesDevice(deviceID: String) -> Bool {
        return internalDevicesList.value.contains(where: { $0.id == deviceID })
    }
    
    public func forget(device: DeviceType) {
        let currentList = internalDevicesList.value
        internalDevicesList.accept(currentList.removing(device))
        removed.accept(device)
    }
    
    public func stopScan() {
        scanTimer?.invalidate()
        scanInProgress.accept(false)
    }

    private func requestForMock(remote: Bool = true, completion: @escaping (MocksProvider) -> ()) {
        
        guard remote else {
            let data = localJSON.data(using: .utf8)
            do {
                let newProvider = try decoder.decode(MocksProvider.self, from: data!)
                completion(newProvider)
            } catch {
                dump(error)
                fatalError()
            }
            return
        }
        
        let endpoint = "https://artifactorypro.shared.pub.tds.tieto.com/artifactory/zoundindustries/test/mock_config.json"
        let session = URLSession.shared
        var request = URLRequest(url: URL(string: endpoint)!)
        
        let authString = "srv4cntub:qwerty123#".data(using: String.Encoding.utf8)!
        
        request.addValue("application/json", forHTTPHeaderField: "content-type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Basic ".appending(authString.base64EncodedString()), forHTTPHeaderField: "Authorization");
        
        let _ = session.dataTask(with: request) { [unowned self] data, response, error in
            do {
                let newProvider = try self.decoder.decode(MocksProvider.self, from: data!)
                completion(newProvider)
            } catch {
                dump(error)
                fatalError()
            }
        }.resume()
    }
    
    private func restartAdapter() {
        
        scanTimer?.invalidate()
        
        self.internalDevicesList.accept([])
        guard let mocksProvider = mocksProvider else {
            return
        }
        
        let timeInterval = TimeInterval(exactly: Double(self.scanDelay) / 1000.0)!
        let bluetoothDisableTimeInterval = TimeInterval.seconds(3600)
        let bluetoothEnableTimeInterval = TimeInterval.seconds(3615)
        
        let fireDate = Date(timeIntervalSinceNow: timeInterval)
        let bluetoothDisableFireDate = Date(timeIntervalSinceNow: bluetoothDisableTimeInterval)
        let bluetoothEnableFireDate = Date(timeIntervalSinceNow: bluetoothEnableTimeInterval)
        
        let numDevices = mocksProvider.devs.count
        var currentDeviceIdx = 0
        
        if btDisableTimer == nil {
            btDisableTimer = Timer(fire: bluetoothDisableFireDate, interval: bluetoothDisableTimeInterval, repeats: true, block: { [weak self] timer in
                guard let strongSelf = self else { return }
                strongSelf.adapterState.accept(.btDisabled)
                strongSelf.btDisableTimer?.invalidate()
                strongSelf.stopScan()
            })
            guard let btDisableTimer = btDisableTimer else { return }
            RunLoop.main.add(btDisableTimer, forMode: RunLoopMode.defaultRunLoopMode)
        }
        if btEnableTimer == nil {
            btEnableTimer = Timer(fire: bluetoothEnableFireDate, interval: bluetoothEnableTimeInterval, repeats: true, block: { [weak self] timer in
                guard let strongSelf = self else { return }
                strongSelf.adapterState.accept(.ready)
                strongSelf.btEnableTimer?.invalidate()
                strongSelf.startScan()
            })
            guard let btEnableTimer = btEnableTimer else { return }
            RunLoop.main.add(btEnableTimer, forMode: RunLoopMode.defaultRunLoopMode)
        }
        scanTimer = Timer(fire: fireDate, interval: timeInterval, repeats: true, block: { [weak self] timer in
            
            guard let strongSelf = self else { return }
            
            guard currentDeviceIdx < numDevices else {
                strongSelf.scanTimer?.invalidate()
                // Delete non-existing json mock devices
                return
            }
            let hwDevice = mocksProvider.devs[currentDeviceIdx]
            let jsonDevice = JSONMockDevice(hwDevice: hwDevice, hasUpdate: mocksProvider.devs[currentDeviceIdx].hasUpdate, platform: type(of: strongSelf).platform)
            jsonDevice.state.outputs.name().stateObservable().accept(jsonDevice.name.value)
            if jsonDevice.name.value == "STANMORE II LITE(JSON)" {
                jsonDevice.state.outputs.connectivityStatus().stateObservable().accept(.connected)
                jsonDevice.state.outputs.playbackStatus().stateObservable().accept(.playing)
                jsonDevice.state.outputs.trackInfo().stateObservable().accept(TrackInfo(title: "My favourite things",
                                                                                        artist: "John Coltrane",
                                                                                        album: "My Favourite Things",
                                                                                        number: nil,
                                                                                        totalNumber: nil,
                                                                                        genre: nil,
                                                                                        playingTime: nil))
            }
            if jsonDevice.name.value == "ACTON II LITE(JSON)" {
                jsonDevice.state.outputs.connectivityStatus().stateObservable().accept(.connecting)
            }
            self?.discovered.accept(jsonDevice)
            currentDeviceIdx = currentDeviceIdx.advanced(by: 1)
        })
        guard let timer = scanTimer else { return }
        RunLoop.main.add(timer, forMode: RunLoopMode.defaultRunLoopMode)
    }

    private var scanDelay: Int = 0 {
        didSet {
            if oldValue != scanDelay {
                restartAdapter()
            }
        }
    }
    
    private var continuousScan: Bool = false {
        didSet {
            if oldValue != continuousScan {
                restartAdapter()
            }
        }
    }
    public var internalDevicesList = BehaviorRelay<[DeviceType]>(value: [])
    private var mocksProvider: MocksProvider? = nil
    private var decoder = JSONDecoder()
    private var scanTimer: Timer?
    private var btDisableTimer: Timer?
    private var btEnableTimer: Timer?
}

