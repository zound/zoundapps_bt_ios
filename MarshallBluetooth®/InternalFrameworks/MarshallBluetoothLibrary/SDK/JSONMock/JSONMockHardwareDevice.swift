//
//  JSONMockHardwareDevice.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 10/10/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

public struct JSONMockHardwareDevice: Codable {
    let name: String
    let id: String
    let playing: Bool
    let connected: Bool
    let hasUpdate: Bool
    let advertisementBytes: [UInt8]
}

extension JSONMockHardwareDevice: Equatable {
    public static func ==(lhs: JSONMockHardwareDevice, rhs: JSONMockHardwareDevice) -> Bool {
        return lhs.id == rhs.id
    }
}

