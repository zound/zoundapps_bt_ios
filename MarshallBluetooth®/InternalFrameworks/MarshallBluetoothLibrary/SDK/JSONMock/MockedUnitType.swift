//
//  MockedUnitType.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 10/10/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//



public enum MockedUnitType {
    case actonII(MockedSpeakerColor?)
    case stanmoreII(MockedSpeakerColor?)
    case woburnII(MockedSpeakerColor?)
    case actonIILite(MockedSpeakerLiteColor)
    case stanmoreIILite(MockedSpeakerLiteColor)
    case woburnIILite(MockedSpeakerLiteColor)
    case ozzy(MockedHeadsetColor)
    case ozzyAnc(MockedHeadsetColor)
}

public enum MockedSpeakerColor: UInt8 {
    case notSupported = 0x10
    case white = 0x11
    case black = 0x12
    case brown = 0x13
}

public enum MockedSpeakerLiteColor: UInt8 {
    case white = 0x11
    case black = 0x12
    case brown = 0x13
}

public enum MockedHeadsetColor: UInt8 {
    case white = 0x11
    case black = 0x12
    case brown = 0x13
}

private struct Const {
    struct MockedUnitType {
        static let actonII: UInt8 = 0x00
        static let stanmoreII: UInt8 = 0x01
        static let woburnII: UInt8 = 0x02
        static let actonIILite: UInt8 = 0x03
        static let stanmoreIILite: UInt8 = 0x04
        static let woburnIILite: UInt8 = 0x05
        static let ozzy: UInt8 = 0x10
        static let ozzyAnc: UInt8 = 0x11
    }
}

extension MockedUnitType {
    public init?(manufacturerData: Data) {
        let sizeByte = [UInt8](manufacturerData)[optional: 1]
        guard let colorByte = [UInt8](manufacturerData)[optional: 0] else {
            return nil
        }
        switch sizeByte {
        case .some(Const.MockedUnitType.actonII):
            self = .actonII(MockedSpeakerColor(rawValue: colorByte))
        case .some(Const.MockedUnitType.stanmoreII):
            self = .stanmoreII(MockedSpeakerColor(rawValue: colorByte))
        case .some(Const.MockedUnitType.woburnII):
            self = .woburnII(MockedSpeakerColor(rawValue: colorByte))
        case .some(Const.MockedUnitType.actonIILite):
            self = .actonIILite(MockedSpeakerLiteColor(rawValue: colorByte) ?? .black)
        case .some(Const.MockedUnitType.stanmoreIILite):
            self = .stanmoreIILite(MockedSpeakerLiteColor(rawValue: colorByte) ?? .black)
        case .some(Const.MockedUnitType.woburnIILite):
            self = .woburnIILite(MockedSpeakerLiteColor(rawValue: colorByte) ?? .black)
        case .some(Const.MockedUnitType.ozzy):
            self = .ozzy(MockedHeadsetColor(rawValue: colorByte) ?? .black)
        case .some(Const.MockedUnitType.ozzyAnc):
            self = .ozzyAnc(MockedHeadsetColor(rawValue: colorByte) ?? .black)
        default:
            return nil
        }
    }
}
