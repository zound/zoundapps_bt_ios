//
//  BaseContextualErrorReporter.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 31/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RCER

public class BaseContextualErrorReporter<BaseContext, BaseErrorType>: ContextualErrorReporter where BaseErrorType: Equatable {
    
    public var errorViewController: ErrorViewController?
    
    public func configureErrorSources(for types: [BaseErrorType]) { }
    
    public func setupErrorActions(for viewController: ErrorViewController, with context: BaseContext, error type: BaseErrorType) { }
    
    public func handleError(_ error: BaseErrorType) { }
    
    public func viewModel(for errorType: BaseErrorType) -> ErrorViewModel? {
        fatalError("Should be implemented in subclass")
    }
    
    public func errorViewController(for errorType: BaseErrorType) -> ErrorViewController? {
        let viewController = UIViewController.instantiate(ErrorViewController.self)
        guard let viewModel = viewModel(for: errorType) else { return nil }
        viewController.viewModel = viewModel
        return viewController
    }
    
    public func showError(parentViewController: UIViewController?, _ error: BaseErrorType) {
        guard let viewController = parentViewController else {
            return
        }
        presentingViewController = viewController
        guard let errorViewController = errorViewController(for: error) else {
            return
        }
        viewController.modalPresentationStyle = .fullScreen
        viewController.present(errorViewController, animated: false) { [weak self] in
            self?.errorViewController = errorViewController
        }
    }
    
    public func dismissError(completion: @escaping () -> Void) {
        guard let presented = errorViewController else {
            completion()
            return
        }
        presented.dismiss(animated: false, completion: { [weak self] in
            if #available(iOS 13.0, *) {
                self?.presentingViewController?.viewDidAppear(true)
            }
            self?.errorViewController = nil
            completion()
        })
    }
    public typealias ErrorType = BaseErrorType
    public typealias Context = BaseContext
    public typealias ErrorViewModelType = ErrorViewModel
    public typealias ErrorViewControllerType = ErrorViewController

    private var presentingViewController: UIViewController?
}
