//
//  AppEnvironment.swift
//  MarshallBluetoothLibrary
//

// It's heavily inspired by the Kickstarter iOS app
// https://github.com/kickstarter/ios-oss

import Foundation


/// Global stack capturing state of global objects we want access to
public struct AppEnvironment {
    
    ///Stacked environments
    fileprivate static var stack: [Environment] = [Environment()]

    /// Current environment
    public static var current: Environment! {
        return stack.last
    }
    

    /// Push environment into the stack
    public static func pushEnvironment(_ env: Environment) {
        stack.append(env)
    }
    
    /// Pop an environment off the stack
    @discardableResult
    public static func popEnvironment() -> Environment? {
        let last = stack.popLast()
        let next = current ?? Environment()
        saveEnvironment(next)
        return last
    }
    
    
    /// Replaces current environment with the new environment
    public static func replaceCurrentEnvironment(_ env: Environment) {
        pushEnvironment(env)
        stack.remove(at: stack.count - 2)
    }
    
    
    /// Pushes new environment onto the stack, replacing subset of current global dependencies
    public static func pushEnvironment(
        language: Language
        ) {
        pushEnvironment(Environment(language: language))
    }
    
    
    /// Replaces current environment with an environment that contains changes for subset of global dependencies
    public static func replaceEnvironment(
        language: Language
        ) {
        replaceCurrentEnvironment(Environment(language: language))
    }
    
    
    /// Saves some data for current environment
    public static func saveEnvironment(_ environment: Environment) {
        fatalError("Implement storage")
    }
    
    /// Goes to the devices settings
    public static func goToSettings() {
        guard let settingsURL = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }
        guard UIApplication.shared.canOpenURL(settingsURL) else { return }
    
        UIApplication.shared.open(settingsURL) { success in
            print(success)
        }
    }

    /// Starts phone call
    ///
    /// - Parameter phone: phone number
    public static func call(phone : String) {
        if let url = URL(string: "tel://\(phone)") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

    /// Starts safari application with dedicated page
    ///
    /// - Parameter url: web page path
    public static func showWebsite(url: String) {
        UIApplication.shared.open(URL(string : url)!, options: [:], completionHandler: nil)
    }
}
