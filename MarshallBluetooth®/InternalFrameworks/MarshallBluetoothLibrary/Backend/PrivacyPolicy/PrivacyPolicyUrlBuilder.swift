//
//  PrivacyPolicyUrlBuilder.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 01/10/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

private struct PathComponents {
    static let scheme = "https"
    static let host = "www.marshallheadphones.com"
    static let path = "marshall-bluetooth-app-pp-"
}

class PrivacyPolicyUrlBuilder {

    private var language: Language = .en

    private var urlComponents: URLComponents {
        var urlComponents = URLComponents()
        urlComponents.scheme = PathComponents.scheme
        urlComponents.host = PathComponents.host
        return urlComponents
    }

    var url: URL {
        guard var url = urlComponents.url else {
            fatalError()
        }
        url.appendPathComponent(PathComponents.path.appending(language.privacyPolicyLanguageComponent))
        return url
    }

    @discardableResult
    func add(_ language: Language?) -> PrivacyPolicyUrlBuilder {
        guard let language = language else { return self }
        self.language = language
        return self
    }

}
