//
//  OnlineManualUrlRequestBuilder.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 15/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

private let credential = "secretKey" // TODO: Change once known

private struct HttpHeader {
    static let authField = "Authorization"
    static let authValue = "Basic \(credential)"
}

private struct PathComponents {
    static let scheme = "https"
    static let host = "deyu07r2qbkt7.cloudfront.net"
    static let path = "marshall-bluetooth-online-output"
    static let file = "index.html"
}

private extension DeviceModel {
    var pathComponent: String {
        switch self {
        case .actonII:
            return "acton-ii"
        case .monitorII:
            return "monitor-ii-anc"
        case .stanmoreII:
            return "stanmore-ii"
        case .woburnII:
            return "woburn-ii"
        }
    }
}

class OnlineManualUrlRequestBuilder {

    private var deviceModel: DeviceModel = .actonII
    private var language: Language = .en

    private var urlComponents: URLComponents {
        var urlComponents = URLComponents()
        urlComponents.scheme = PathComponents.scheme
        urlComponents.host = PathComponents.host
        return urlComponents
    }

    var request: URLRequest? {
        guard var url = urlComponents.url else {
            return nil
        }
        [PathComponents.path, language.onlineManualLanguageComponent, deviceModel.pathComponent, PathComponents.file].forEach {
            url.appendPathComponent($0)
        }
        var request = URLRequest(url: url)
        request.setValue(HttpHeader.authValue, forHTTPHeaderField: HttpHeader.authField)
        return request
    }

    @discardableResult
    func add(_ deviceModel: DeviceModel) -> OnlineManualUrlRequestBuilder {
        self.deviceModel = deviceModel
        return self
    }

    @discardableResult
    func add(_ language: Language?) -> OnlineManualUrlRequestBuilder {
        guard let language = language else { return self }
        self.language = language
        return self
    }

}
