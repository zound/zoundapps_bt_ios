//
//  DeviceSettingsCell.swift
//  MarshallBluetoothLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 01.06.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

class DeviceSettingsCell: UITableViewCell {
    
    @IBOutlet weak var settingsLabel: UILabel!
    @IBOutlet weak var notificationImage: UIImageView!
    
    var viewModel: DeviceSettingsCellViewModelType?
    
    private var disposeBag = DisposeBag()
    
    internal override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        settingsLabel.textColor = highlighted ? Color.textGrayedOut : Color.text
    }
    
    internal func configure(with viewModel: DeviceSettingsCellViewModel) {
        self.viewModel = viewModel
        
        guard let viewModel = self.viewModel else { return }
        
        viewModel.outputs.updateNotification
            .asObservable()
            .bind(to: notificationImage.rx.visible)
            .disposed(by: disposeBag)
        
        Observable.combineLatest(viewModel.outputs.name, viewModel.outputs.enabled)
            .map { UIFont.attributedText($0, color: $1 ? Color.text : Color.textGrayedOut) }
            .asDriver(onErrorJustReturn: NSAttributedString(string: ""))
            .drive(settingsLabel.rx.attributedText)
            .disposed(by: disposeBag)
        
        let enabled = viewModel.outputs.enabled
        
        enabled
            .subscribeOn(MainScheduler.instance)
            .bind(to: self.rx.isUserInteractionEnabled)
            .disposed(by: disposeBag)
        
        enabled
            .subscribeOn(MainScheduler.instance)
            .bind(to: settingsLabel.rx.enabled)
            .disposed(by: disposeBag)
        
        enabled
            .subscribeOn(MainScheduler.instance)
            .map { enabled -> UIImageView in
                // change accessory view (arrow on the right side)
                let arrow = UIImageView(image: UIImage.forwardArrow)
                arrow.image = arrow.image?.withRenderingMode(.alwaysTemplate)
                arrow.tintColor = enabled ? Color.text : Color.textGrayedOut
                return arrow
            }
            .subscribe(onNext: { [weak self] arrow in
                self?.accessoryView = arrow
            }).disposed(by: disposeBag)
    }
}
