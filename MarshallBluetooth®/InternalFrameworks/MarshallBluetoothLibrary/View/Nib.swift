//
//  Nib.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 13/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

// MARK: - Extension to Bundle for wrapping library's bundle name
extension Bundle {
    
    /// Get Bundle for MarshallBluetoothLibrary
    public static var framework: Bundle {
        return Bundle(for: WelcomeViewController.self)
    }
}

/// Wrapper for every view controller in MarshallBluetoothLibrary to load it's XIB file
public enum Nib: String {
    case WelcomeViewController
    case RootViewController
    case CircularProgressViewController
    case DevicesListViewController
    case HomeViewController
    case StayUpdatedViewController
    case ShareDataViewController
    case SaveDataViewController
    case WelcomeTermsAndConditionsViewController
    case InAppTermsAndConditionsViewController
    case OTAFinishedViewController
    case OTAUpdateAvailableViewController
    case OTAOnboardingViewController
    case EqPresetsAdvertViewController
    case OzzyAdvertisementViewController
    case OzzyAdvertANCViewController
    case OzzyAdvertMButtonViewController
    case OzzyAdvertEqualizerViewController
    case JoplinAdvertisementViewController
    case CustomizeAdvertViewController
    case CoupleSpeakersAdvertViewController
    case HamburgerMenuViewController
    case EqualizerViewController
    case EqualizerSettingsViewController
    case ActiveNoiceCancellingViewController
    case AutoOffTimerViewController
    case MButtonViewController
    case DeviceSettingsViewController
    case AboutThisSpeakerViewController
    case LightViewController
    case CoupleSpeakerListViewController
    case CoupleSpeakerModeSelectionViewController
    case CoupleSpeakerModeConfigurationViewController
    case CoupleSpeakerHeadsUpViewController
    case RenameViewController
    case PlayerSourcesViewController
    case AnalogAudioSourceViewController
    case BluetoothPlayerSourceViewController
    case ForgetSpeakerViewController
    case SoundsViewController
    case CommonMenuViewController
    case UnsubscribeConfirmationViewController
    case ErrorViewController
    case ContactScreenViewController
    case QuickGuideViewController
    case FossScreenViewController
    case CommonGuideViewController
    case GenericGuideViewController
    case BluetoothGuideViewController
    case SpeakersGuideViewController
    case PlayPauseGuideViewController
    case OnlineManualViewController
    case PairingDoneViewController
    case PairingViewController
    case OnboardingMButtonViewController
    case OzzyPlayerViewController
    case GoogleAssistantViewController
    
    // Views
    case DevicesListRefreshView
    
    // Table view cells
    case NoDevicesFoundCell
    case DeviceSettingsCell
    case CoupleSpeakerListTableViewCell
    case CoupleSpeakerModeSelectionTableViewCell
    case CoupleSpeakerModeConfigurationTableViewCell
    case AudioSourceCell
    case RcaPlayerSourceCell
    case AuxPlayerSourceCell
    case BluetoothPlayerSourceCell
    case CommonMenuCell
    
    /// Create particular view controller and load it's related XIB file (which should be named the same as Swift file)
    ///
    /// - Parameters:
    ///   - viewController: "self" of particular view controller
    ///   - bundle: bundle that contains XIB file (default is MarshallBluetoothLibrary's bundle)
    /// - Returns: ready to use view controller
    public func instantiate<VC: UIViewController>(_ viewController: VC.Type, inBundle bundle: Bundle = .framework) -> VC {
        let vc = VC.init(nibName: self.rawValue, bundle: .framework)
        return vc
    }
    
    /// Create particular view and load it's related XIB file (which should be named the same as Swift file)
    ///
    /// - Parameters:
    ///   - view: "self" of particular view
    ///   - bundle: bundle that contains XIB file (default is MarshallBluetoothLibrary's bundle)
    /// - Returns: ready to use view
    public func instantiate<V: UIView>(_ view: V.Type, inBundle bundle: Bundle = .framework) -> V {
        return bundle.loadNibNamed(self.rawValue, owner: nil, options: nil)!.first as! V
    }
    
}
