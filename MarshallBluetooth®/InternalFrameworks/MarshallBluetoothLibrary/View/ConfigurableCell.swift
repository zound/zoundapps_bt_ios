//
//  ConfigurableCell.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 15/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

public protocol ConfigurableCell: class {
    associatedtype Value
    static var defaultReusableId: String { get }
    func configureWith(value: Value)
}

extension UIView {
    public static var defaultReusableId: String {
        return self.description()
            .components(separatedBy: ".")
            .dropFirst()
            .joined(separator: ".")
    }
}
