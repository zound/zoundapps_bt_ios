//
//  DevicesListBatteryStatusView.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 31/10/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift
import GUMA

private struct Const {
    static let batteryIconWidth: CGFloat = 16.0
    static let batteryIconHeight: CGFloat = 10.0
    static let separatorWidth: CGFloat = 4.0
}
final class DevicesListBatteryStatusView: UIView {
    var batteryStatus: BatteryStatus = .unknown {
        didSet {
            self.batteryStatusIconImageView.image = batteryStatus.icon()
            self.batteryLevelLabel.attributedText = UIFont.attributedBatteryLabelStatus(batteryStatus.description())
        }
    }
    lazy var batteryStatusIconImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .center
        return imageView
    }()
    lazy var batteryLevelLabel: UILabel = {
       return UILabel()
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupConstraints() {
        [batteryStatusIconImageView, batteryLevelLabel].forEach {
            autolayoutStyle($0)
            addSubview($0)
        }
        NSLayoutConstraint.activate([
            batteryStatusIconImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            batteryStatusIconImageView.trailingAnchor.constraint(equalTo: batteryLevelLabel.leadingAnchor, constant: -Const.separatorWidth),
            batteryStatusIconImageView.widthAnchor.constraint(equalToConstant: Const.batteryIconWidth),
            batteryStatusIconImageView.heightAnchor.constraint(equalToConstant: Const.batteryIconHeight),
            batteryStatusIconImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            batteryLevelLabel.topAnchor.constraint(equalTo: topAnchor),
            batteryLevelLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            batteryLevelLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            batteryLevelLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    private var disposeBag = DisposeBag()
}
