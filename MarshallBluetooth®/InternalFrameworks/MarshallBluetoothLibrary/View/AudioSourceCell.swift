//
//  AudioSourceCell.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 25.06.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

class AudioSourceCell: UICollectionViewCell, ConfigurableCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    internal var viewModel: AudioSourceCellViewModelType?
    
    func configureWith(value: AudioSourceCellViewModelType) {
        viewModel = value
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)

        imageView?.image = imageView?.image?.withRenderingMode(.alwaysTemplate)
        var tintColor = UIColor.white
        
        if(layoutAttributes.alpha >= 0.9) {
            tintColor = UIColor.darkBeige
        }
        
        UIView.animate(withDuration: TimeInterval.seconds(0.1)) {
            self.imageView.tintColor = tintColor
        }
    }
}
