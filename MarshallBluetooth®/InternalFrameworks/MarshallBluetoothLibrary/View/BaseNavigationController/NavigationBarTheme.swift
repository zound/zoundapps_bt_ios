//
//  NavigationBarTheme.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 16/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

enum NavigationBarTheme {
    case dark
    case light

    var tintColor: UIColor {
        return self == .dark ? .white : .blackOne
    }
    var barTintColor: UIColor {
        return self == .dark ? .blackOne : .white
    }
}
