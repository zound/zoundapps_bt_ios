//
//  NavigationBarThemeChangeable.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 16/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

protocol NavigationBarThemeChangeable: class {
    var navigationBarTheme: NavigationBarTheme { get }
}

extension NavigationBarThemeChangeable {
    var navigationBarTheme: NavigationBarTheme {
        return .dark
    }
}
