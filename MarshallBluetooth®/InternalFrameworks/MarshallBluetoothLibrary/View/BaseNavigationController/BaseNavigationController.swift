//
//  BaseNavigationController.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 16/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {

    private var previousViewController: UIViewController? {
        guard viewControllers.count > 1 else {
            return nil
        }
        return viewControllers[viewControllers.count - 2]
    }

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if let navigationBarThemeChangeable = viewController as? NavigationBarThemeChangeable {
            setNavigationBar(theme: navigationBarThemeChangeable.navigationBarTheme)
        }
        super.pushViewController(viewController, animated: animated)
    }

    @discardableResult
    override func popViewController(animated: Bool) -> UIViewController? {
        if let navigationBarThemeChangeable = self.previousViewController as? NavigationBarThemeChangeable {
            setNavigationBar(theme: navigationBarThemeChangeable.navigationBarTheme)
        }
        return super.popViewController(animated: animated)
    }

    private func setNavigationBar(theme: NavigationBarTheme) {
        navigationBar.tintColor = theme.tintColor
        navigationBar.barTintColor = theme.barTintColor
    }

    func updateNavigationBarThemeOnTopViewController() {
        if let navigationBarThemeChangeable = topViewController as? NavigationBarThemeChangeable {
            setNavigationBar(theme: navigationBarThemeChangeable.navigationBarTheme)
        }
    }
}
