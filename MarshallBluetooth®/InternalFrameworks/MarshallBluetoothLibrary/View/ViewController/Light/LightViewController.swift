//
//  LightViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 22.05.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

class LightViewController: BaseViewController {
    
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var ledImageView: UIImageView!
    @IBOutlet weak var intensitySlider: UISlider!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var labelConstraint: NSLayoutConstraint!
    
    private var baseLedImage: UIImage!
    private var alreadyFetched = false
    private var disposeBag = DisposeBag()
    private let animationDuration = RxTimeInterval.seconds(0.5)
    
    internal var viewModel: LightViewModelType?
    
    var intensity: Int = Int() {
        didSet {
            guard oldValue != intensity else { return }
            viewModel?.inputs.change(intensity: intensity)
        }
    }
    
    deinit {
        print(#function, String(describing: self))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar(title: UIFont.attributedTitle(Strings.device_settings_menu_item_light_uc()))
        
        baseLedImage = ledImageView.image
        viewModel?.inputs.original(image: baseLedImage)
        
        subTitleLabel.attributedText = UIFont.attributedBodyText(Strings.light_screen_subtitle())
        intensitySlider.setThumb(image: UIImage.sliderThumb)
        intensitySlider.addTarget(self, action: #selector(onTouchUpInside(_:)), for: .touchUpInside)
        
        percentageLabel.alpha = 0.0
        
        // display image
        viewModel?.outputs.ledImage
            .asDriver()
            .drive(ledImageView.rx.image)
            .disposed(by: disposeBag)
        
        // one-time setup for
        viewModel?.outputs.ledIntensity
            .asObservable()
            .takeOne()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] intensity in
                self?.intensitySlider.value = Float(intensity)
                self?.updateLabelConstraint()
            }).disposed(by: disposeBag)
        
        viewModel?.outputs.ledIntensityScale
            .asObservable()
            .takeOne() // only initialize this, do not change this in life-time of this view
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (min, max) in
                self?.intensitySlider.minimumValue = Float(min)
                self?.intensitySlider.maximumValue = Float(max)
            }).disposed(by: disposeBag)
        
        viewModel?.outputs.ledPercentage
            .asObservable()
            .observeOn(MainScheduler.instance)
            .map { [weak self] percentageValue in
                self?.textFormattedForLabel(brightness: percentageValue) }
            .bind(to: percentageLabel.rx.attributedText)
            .disposed(by: disposeBag)
        
        viewModel?.outputs.dataFetched
            .asObservable()
            .observeOn(MainScheduler.instance)
            .skipWhile { $0 == false }
            .subscribe(onNext: { [weak self] _ in
                self?.removeLoadingView()
                self?.alreadyFetched = true
            })
            .disposed(by: disposeBag)
        
        viewModel?.outputs.appMode
            .observeOn(MainScheduler.instance)
            .subscribeNext(weak: self, LightViewController.appModeChanged)
            .disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if alreadyFetched == false {
            addLoadingView(animated: true)
        }
    }
    
    @objc func appDidBecomeActive(_ notification: Notification) {
        viewModel?.inputs.viewDidBecameActive()
    }
    
    @IBAction func onSliderValueChanged(_ sender: UISlider) {
        intensity = Int(sender.value)
        updateLabelConstraint()
    }

    @objc func onTouchUpInside(_ sender: UISlider!) {
        viewModel?.inputs.logLightIntensityChangeAnalyticsEvent(with: intensity)
    }
    
    @IBAction func onTouchDown(_ sender: UISlider) {
        UIView.animate(withDuration: RxTimeInterval.seconds(animationDuration)) {
            self.percentageLabel.alpha = 1.0
        }
    }
    
    @IBAction func onTouchUp(_ sender: UISlider) {
        UIView.animate(withDuration: RxTimeInterval.seconds(animationDuration)) {
            self.percentageLabel.alpha = 0.0
        }
    }
    
    private func updateLabelConstraint() {
        let trackRect = intensitySlider.trackRect(forBounds: intensitySlider.bounds)
        let thumbRect = intensitySlider.thumbRect(forBounds: intensitySlider.bounds, trackRect: trackRect, value: intensitySlider.value)
        
        let labelCenterX = intensitySlider.convert(thumbRect, to: nil).midX
        labelConstraint.constant = labelCenterX
        view.layoutIfNeeded()
    }
    
    private func textFormattedForLabel(brightness: Double) -> NSAttributedString? {
        // format text for our label (present as percentage brightness)
        let text = String("\(Int(brightness * 100))%")
        return UIFont.attributedNumber(text)
    }
    
    private func appModeChanged(_ mode: AppMode) {
        if mode == .foreground {
            viewModel?.inputs.viewDidBecameActive()
        }
    }
}
