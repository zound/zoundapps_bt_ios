//
//  EqualizerSettingsViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 15/11/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift
import GUMA

private struct Config {
    struct SliderAnimation {
        static let duration: TimeInterval = 0.4
        static let options: UIViewAnimationOptions = .curveEaseInOut
    }
    struct DropDownList {
        static let bottomMargin = CGFloat(50)
    }
    struct VeritcalSlider {
        static let enabledAlpha: CGFloat = 1
        static let disabledAlpha: CGFloat = 0.3
    }
    static let mButtonInsets = UIEdgeInsets(top: 16, left: 10, bottom: 10, right: 10)
}

class EqualizerSettingsViewController: BaseViewController {

    @IBOutlet weak var underlineSegmentedControlContainerView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dropDownList: DropDownList!
    @IBOutlet weak var bassVerticalSlider: VerticalSlider!
    @IBOutlet weak var lowVerticalSlider: VerticalSlider!
    @IBOutlet weak var midVerticalSlider: VerticalSlider!
    @IBOutlet weak var upperVerticalSlider: VerticalSlider!
    @IBOutlet weak var highVerticalSlider: VerticalSlider!
    @IBOutlet weak var bassLabel: UILabel!
    @IBOutlet weak var lowLabel: UILabel!
    @IBOutlet weak var midLabel: UILabel!
    @IBOutlet weak var upperLabel: UILabel!
    @IBOutlet weak var highLabel: UILabel!

    private var underlineSegmentedControl: UnderlineSegmentedControl?

    var viewModel: EqualizerSettingsViewModel?
    private let disposeBag = DisposeBag()

    private var verticalSliders: [VerticalSlider] {
        return [bassVerticalSlider, lowVerticalSlider, midVerticalSlider, upperVerticalSlider, highVerticalSlider]
    }

    private var currentEqualizerSettings: EqualizerSettings?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: UIFont.attributedTitle(Strings.device_settings_menu_item_equaliser_uc()))
        applyTextForFrequencyLabels()
        configureUnderlineSegmentedControl()
        configureVerticalSlidersTargetActions()
        configureDropDownList()
        let outputEqualizerSettingsInit = viewModel?.outputs.outputEqualizerSettingsInit.asObservable().observeOn(MainScheduler.instance)
        outputEqualizerSettingsInit?.subscribe({ [weak self] outputEqualizerSettingsInit in
            guard let strongSelf = self, let outputEqualizerSettings = outputEqualizerSettingsInit.element else { return }
            strongSelf.currentEqualizerSettings = outputEqualizerSettings
            strongSelf.underlineSegmentedControl?.selectedSegmentIndex = outputEqualizerSettings.step.rawValue
            strongSelf.updateMButtonRelatedUIComponents()
            strongSelf.removeLoadingView(animated: false)
        }).disposed(by: disposeBag)
        let outputEqualizerSettings = viewModel?.outputs.outputEqualizerSettings.asObservable().observeOn(MainScheduler.instance)
        outputEqualizerSettings?.subscribe({ [weak self] outputEqualizerSettingsElement in
            guard let strongSelf = self, let outputEqualizerSettings = outputEqualizerSettingsElement.element else { return }
            strongSelf.currentEqualizerSettings = outputEqualizerSettings
            strongSelf.underlineSegmentedControl?.selectedSegmentIndex = outputEqualizerSettings.step.rawValue
            strongSelf.updateMButtonRelatedUIComponents()
        }).disposed(by: disposeBag)
        let outputEqualizerButtonStep = viewModel?.outputs.outputEqualizerButtonStep.asObservable().observeOn(MainScheduler.instance)
        outputEqualizerButtonStep?.subscribe({ [weak self] outputEqualizerButtonStepElement in
            guard let strongSelf = self, let outputEqualizerButtonStep = outputEqualizerButtonStepElement.element else { return }
            strongSelf.currentEqualizerSettings = strongSelf.currentEqualizerSettings?.changing(step: outputEqualizerButtonStep)
            strongSelf.underlineSegmentedControl?.selectedSegmentIndex = outputEqualizerButtonStep.rawValue
            strongSelf.updateMButtonRelatedUIComponents()
        }).disposed(by: disposeBag)
        NotificationCenter.default.addObserver(self, selector: #selector(appWillResignActive), name: .UIApplicationWillResignActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: .UIApplicationDidBecomeActive, object: nil)
        viewModel?.inputs.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setDropDownListHeight()
        if currentEqualizerSettings == nil {
            addLoadingView(animated: true)
        }
        viewModel?.inputs.viewDidAppear()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel?.inputs.viewWillResignActive()
    }

    @objc func appDidBecomeActive(_ notification: Notification) {
        viewModel?.inputs.viewDidBecameActive()
    }

    @objc func appWillResignActive(_ notification: Notification) {
        viewModel?.inputs.viewWillResignActive()
    }
    deinit {
        print(#function, String(describing: self))
    }
}

private extension EqualizerSettingsViewController {
    func applyDescriptionLabel(text: String) {
        descriptionLabel.attributedText = UIFont.attributedText(text, color: .white)
    }
    func applyTextForFrequencyLabels() {
        zip([bassLabel, lowLabel, midLabel, upperLabel, highLabel],
            [Strings.equaliser_screen_preset_bass(),
             Strings.equaliser_screen_preset_low(),
             Strings.equaliser_screen_preset_mid(),
             Strings.equaliser_screen_preset_upper(),
             Strings.equaliser_screen_preset_high()]
        ).forEach {
            $0.0?.attributedText = UIFont.attributedSkip($0.1, color: UIColor.mediumGrey)
        }
    }
    var optionsTextForDropDownList: [NSAttributedString] {
        return [
            UIFont.attributedDropdownList(Strings.equaliser_screen_preset_marshall_uc()),
            UIFont.attributedDropdownList(Strings.equaliser_screen_preset_custom_uc()),
            UIFont.attributedDropdownList(Strings.equaliser_screen_preset_rock_uc()),
            UIFont.attributedDropdownList(Strings.equaliser_screen_preset_spoken_uc()),
            UIFont.attributedDropdownList(Strings.equaliser_screen_preset_pop_uc()),
            UIFont.attributedDropdownList(Strings.equaliser_screen_preset_hip_hop_uc()),
            UIFont.attributedDropdownList(Strings.equaliser_screen_preset_electronic_uc()),
            UIFont.attributedDropdownList(Strings.equaliser_screen_preset_jazz_uc())
        ]
    }
}

extension EqualizerSettingsViewController: DropDownListDelegate {
    func didSelect(index: Int) {
        guard let equalizerButtonStepPreset = EqualizerButtonStepPreset.buttonStepPreset(for: index),
              let step = currentEqualizerSettings?.step else {
            return
        }
        switch step {
        case .step2:
            currentEqualizerSettings = currentEqualizerSettings?.changing(step2Preset: equalizerButtonStepPreset)
            viewModel?.inputs.inputEqualizerButtonStep2Preset.accept(equalizerButtonStepPreset)
        case .step3:
            currentEqualizerSettings = currentEqualizerSettings?.changing(step3Preset: equalizerButtonStepPreset)
            viewModel?.inputs.inputEqualizerButtonStep3Preset.accept(equalizerButtonStepPreset)
        default:
            break
        }
        updateSliders(to: equalizerButtonStepPreset, customGraphicalEqualizer: currentEqualizerSettings!.customGraphicalEqualizer, animated: true)
    }
}

private extension EqualizerSettingsViewController {
    func updateMButtonRelatedUIComponents() {
        guard let step = currentEqualizerSettings?.step else {
            return
        }
        switch step {
        case .step1:
            dropDownList.set(enabled: false, withSelected: EqualizerButtonStepPreset.flat.index)
            updateSliders(to: EqualizerButtonStepPreset.flat,
                          customGraphicalEqualizer: GraphicalEqualizer(), animated: true) { [weak self] in
                self?.setUserInteractionForSliders(enabled: false)
            }
            setSliders(enabledWithAlpha: false)
            applyDescriptionLabel(text: Strings.equaliser_settings_screen_m1_subtitle())
        case .step2:
            dropDownList.set(enabled: true, withSelected: currentEqualizerSettings!.step2Preset.index)
            updateSliders(to: currentEqualizerSettings!.step2Preset,
                          customGraphicalEqualizer: currentEqualizerSettings!.customGraphicalEqualizer, animated: true) { [weak self] in
                self?.setUserInteractionForSliders(enabled: true)
                self?.setSliders(enabledWithAlpha: true)
            }
            applyDescriptionLabel(text: Strings.equaliser_settings_screen_m2_m3_subtitle())
        case .step3:
            dropDownList.set(enabled: true, withSelected: currentEqualizerSettings!.step3Preset.index)
            updateSliders(to: currentEqualizerSettings!.step3Preset,
                          customGraphicalEqualizer: currentEqualizerSettings!.customGraphicalEqualizer, animated: true) { [weak self] in
                self?.setUserInteractionForSliders(enabled: true)
                self?.setSliders(enabledWithAlpha: true)
            }
            applyDescriptionLabel(text: Strings.equaliser_settings_screen_m2_m3_subtitle())
        }
    }
}

private extension EqualizerSettingsViewController {
    func configureUnderlineSegmentedControl() {
        underlineSegmentedControl = UnderlineSegmentedControl(segments: [
            UIImage.EqualizerButton.m1.with(insets: Config.mButtonInsets)!,
            UIImage.EqualizerButton.m2.with(insets: Config.mButtonInsets)!,
            UIImage.EqualizerButton.m3.with(insets: Config.mButtonInsets)!
        ])
        underlineSegmentedControl?.addTarget(self, action: #selector(onValueChange(_:)), for: .valueChanged)
        underlineSegmentedControlContainerView.addSubview(underlineSegmentedControl!)
        underlineSegmentedControl!.translatesAutoresizingMaskIntoConstraints = false
        underlineSegmentedControl!.topAnchor.constraint(equalTo: underlineSegmentedControlContainerView.topAnchor).isActive = true
        underlineSegmentedControl!.leadingAnchor.constraint(equalTo: underlineSegmentedControlContainerView.leadingAnchor).isActive = true
        underlineSegmentedControl!.trailingAnchor.constraint(equalTo: underlineSegmentedControlContainerView.trailingAnchor).isActive = true
    }
    func configureDropDownList() {
        dropDownList.setup(options: optionsTextForDropDownList)
        dropDownList.delegate = self
        dropDownList.set(enabled: false, withSelected: EqualizerButtonStepPreset.flat.index)
    }
    func setDropDownListHeight() {
        let dropDownListHeight = view.frame.size.height - (dropDownList.frame.origin.y + dropDownList.frame.size.height + Config.DropDownList.bottomMargin)
        let rowHeight = (dropDownListHeight / CGFloat(optionsTextForDropDownList.count)).rounded(.down)
        dropDownList.set(rowHeight: rowHeight)
    }
    func configureVerticalSlidersTargetActions() {
        verticalSliders.forEach {
            $0.maximumValue = Float(GraphicalEqualizer.max)
            $0.addTarget(self, action: #selector(onAnySliderChange(_:)), for: .valueChanged)
        }
        updateSliders(to: .flat, customGraphicalEqualizer: GraphicalEqualizer(), animated: false, completion: nil)
        setSliders(enabledWithAlpha: false)
    }
    func updateSliders(to preset: EqualizerButtonStepPreset, customGraphicalEqualizer: GraphicalEqualizer, animated: Bool, completion: (() -> Void)? = nil) {
        setUserInteractionForSliders(enabled: false)
        let animations = { [weak self] in
            let graphicalEqualizer = preset.graphicalEqualizer ?? customGraphicalEqualizer
            self?.bassVerticalSlider.value = Float(graphicalEqualizer.bass)
            self?.lowVerticalSlider.value = Float(graphicalEqualizer.low)
            self?.midVerticalSlider.value = Float(graphicalEqualizer.mid)
            self?.upperVerticalSlider.value = Float(graphicalEqualizer.upper)
            self?.highVerticalSlider.value = Float(graphicalEqualizer.high)
        }
        let completionBlock: (Bool) -> Void = { [weak self] _ in
            self?.setUserInteractionForSliders(enabled: true)
            completion?()
        }
        UIView.animate(withDuration: animated ? Config.SliderAnimation.duration : TimeInterval(),
                       delay: TimeInterval(),
                       options: Config.SliderAnimation.options,
                       animations: animations,
                       completion: completionBlock)
    }
    func setUserInteractionForSliders(enabled: Bool) {
        verticalSliders.forEach {
            $0.isUserInteractionEnabled = enabled
        }
    }
    func setSliders(enabledWithAlpha: Bool) {
        verticalSliders.forEach {
            $0.set(alpha: enabledWithAlpha ? Config.VeritcalSlider.enabledAlpha : Config.VeritcalSlider.disabledAlpha)
        }
    }
    @objc func onAnySliderChange(_ sender: UISlider!) {
        let graphicalEqualizer = GraphicalEqualizer(bass: UInt8(bassVerticalSlider.value),
                                                    low: UInt8(lowVerticalSlider.value),
                                                    mid: UInt8(midVerticalSlider.value),
                                                    upper: UInt8(upperVerticalSlider.value),
                                                    high: UInt8(highVerticalSlider.value))


        guard let step = currentEqualizerSettings?.step else {
            return
        }
        switch step {
        case .step2 where currentEqualizerSettings?.step2Preset != .custom:
            currentEqualizerSettings = currentEqualizerSettings?.changing(step2Preset: .custom)
            viewModel?.inputs.inputEqualizerButtonStep2Preset.accept(.custom)
        case .step3 where currentEqualizerSettings?.step3Preset != .custom:
            currentEqualizerSettings = currentEqualizerSettings?.changing(step3Preset: .custom)
            viewModel?.inputs.inputEqualizerButtonStep3Preset.accept(.custom)
        default:
            break
        }
        dropDownList.selectedIndex = EqualizerButtonStepPreset.custom.index
        currentEqualizerSettings = currentEqualizerSettings?.changing(customGraphicalEqualizer: graphicalEqualizer)
        viewModel?.inputCustomPresetGraphicalEqualizer.accept(graphicalEqualizer)
    }
    @objc func onValueChange(_ sender: UnderlineSegmentedControl!) {
        guard let equalizerButtonStep = EqualizerButtonStep(rawValue: sender.selectedSegmentIndex) else {
            return
        }
        viewModel?.inputEqualizerButtonStep.accept(equalizerButtonStep)
        currentEqualizerSettings = currentEqualizerSettings?.changing(step: equalizerButtonStep)
        updateMButtonRelatedUIComponents()
    }
}

extension EqualizerButtonStepPreset {
    private struct Index {
        static let flat = 0
        static let custom = 1
        static let rock = 2
        static let metal = 3
        static let pop = 4
        static let hipHop = 5
        static let electronic = 6
        static let jazz = 7
    }
    var index: Int {
        switch self {
        case .flat:
            return Index.flat
        case .custom:
            return Index.custom
        case .rock:
            return Index.rock
        case .metal:
            return Index.metal
        case .pop:
            return Index.pop
        case .hipHop:
            return Index.hipHop
        case .electronic:
            return Index.electronic
        case .jazz:
            return Index.jazz
        }
    }
    static func buttonStepPreset(for index: Int) -> EqualizerButtonStepPreset? {
        switch index {
        case Index.flat:
            return .flat
        case Index.custom:
            return .custom
        case Index.rock:
            return .rock
        case Index.metal:
            return .metal
        case Index.pop:
            return .pop
        case Index.hipHop:
            return .hipHop
        case Index.electronic:
            return .electronic
        case Index.jazz:
            return .jazz
        default:
            return nil
        }
    }
}
