//
//  File.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 11.04.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

/// Customize advertisement view.
/// No view model because of no inputs/outputs (static screen with no interaction with user)
class CustomizeAdvertViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var advertImageView: UIImageView!
    
    override func viewDidLoad() {
        titleLabel.resizableAttributedFont(text: UIFont.attributedLargeTitle(Strings.ota_screen_customize_title_uc()))
        subtitleLabel.resizableAttributedFont(text: UIFont.attributedText(Strings.ota_screen_customize_subtitle(), color: Color.text), numberOfLines: 0)
        subtitleLabel.textAlignment = .center
    }
}
