//
//  PairingViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 10/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Const {
    static let alphaMin: CGFloat = 0.2
    static let alphaMax: CGFloat = 1.0
    static let pairingCoverViewAlpha: CGFloat = 1.0
    static let pairingCoverViewTopMargin: CGFloat = 30.0
    static let pairingCoverViewBottomMargin: CGFloat = 30.0
}
private extension Dimension {
    static var safeAreaBottomInset: CGFloat {
        if #available(iOS 11.0, *) {
            return UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? .zero
        } else {
            return .zero
        }
    }
    static let statusBarHeight: CGFloat = UIApplication.shared.statusBarFrame.height
    static let screenWidth: CGFloat = UIScreen.main.bounds.size.width
    static let screenHeight18: CGFloat = UIScreen.main.bounds.size.height / 8 * 1
    static let screenHeight58: CGFloat = UIScreen.main.bounds.size.height / 8 * 5
    static let headerSmallBottomMargin: CGFloat = 10
    static let progressIndicatorBottomMargin: CGFloat = -30
    static let progressViewWidth: CGFloat = 50
    static let progressViewHeight: CGFloat = 50
    static let imageViewWidth: CGFloat = 224.0
    static let imageViewHeight: CGFloat = 283.0
    static let stateDescriptionImageViewVerticalSpacing: CGFloat = 30
}
public class PairingViewController: UIViewController {
    var viewModel: PairingViewModelType!
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    private lazy var progressView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage.commonSpinner
        return imageView
    }()
    private lazy var headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.attributedText = UIFont.attributedLargeTitle(Strings.enable_pairing_ongoing_title_uc())
        headerLabel.textAlignment = .center
        headerLabel.numberOfLines = 2
        return headerLabel
    }()
    private lazy var disableDeviceLabel: UILabel = {
        let descriptionLabel = UILabel()
        
        descriptionLabel.attributedText = UIFont.attributedText(Strings.enable_pairing_description_1(), color: Color.text)
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = Int.zero
        return descriptionLabel
    }()
    private lazy var setIntoPairingModeLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.attributedText = UIFont.attributedText(Strings.enable_pairing_description_2(), color: Color.text)
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = .zero
        return descriptionLabel
    }()
    private let pairingCoverView: PairingCoverView = {
        let pairingCoverView =  PairingCoverView()
        pairingCoverView.backgroundColor = UIColor.withAlphaComponent(Color.background)(Const.pairingCoverViewAlpha)
        pairingCoverView.isHidden = true
        return pairingCoverView
    }()
    public override func viewDidLoad() {
        super.viewDidLoad()
        setupConstraints()
        viewModel?.inputs.viewDidLoad()
        progressView.rotate()
        imageView.image = viewModel?.outputs.pairingImage
        showPairingCoverView()
        viewModel?.outputs.pairingState.asDriver()
            .drive(onNext: { [weak self] state in
                self?.handlePairing(state)
            })
            .disposed(by: rx_disposeBag)
    }
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel?.inputs.viewDidAppear()
    }
    private func setupConstraints() {
        [imageView, headerLabel, disableDeviceLabel, setIntoPairingModeLabel, pairingCoverView, progressView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }
        view.bringSubview(toFront: pairingCoverView)
        view.bringSubview(toFront: progressView)
        NSLayoutConstraint.activate([
            headerLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: Dimension.screenHeight18),
            headerLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            headerLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            disableDeviceLabel.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: Dimension.headerSmallBottomMargin),
            disableDeviceLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            disableDeviceLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            setIntoPairingModeLabel.topAnchor.constraint(equalTo: disableDeviceLabel.bottomAnchor),
            setIntoPairingModeLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Dimension.edgeMargin),
            setIntoPairingModeLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Dimension.edgeMargin),
            imageView.topAnchor.constraint(equalTo: setIntoPairingModeLabel.bottomAnchor, constant: Dimension.stateDescriptionImageViewVerticalSpacing),
            imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            imageView.widthAnchor.constraint(equalToConstant: Dimension.imageViewWidth),
            imageView.heightAnchor.constraint(equalToConstant: Dimension.imageViewHeight),
            progressView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            progressView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            progressView.widthAnchor.constraint(equalToConstant: Dimension.progressViewWidth),
            progressView.heightAnchor.constraint(equalToConstant: Dimension.progressViewHeight),
            pairingCoverView.topAnchor.constraint(equalTo: view.topAnchor, constant: Const.pairingCoverViewTopMargin),
            pairingCoverView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Const.pairingCoverViewBottomMargin),
            pairingCoverView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            pairingCoverView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
    }
    private func handlePairing(_ state: DevicePairingState) {
        switch state {
        case .backgrounded, .foregrounded, .pairing:
            showPairingCoverView()
        case .completed:
            fadeOutViews()
        default:
            return
        }
    }
    private func update(description1Label alpha1: CGFloat,  description2Label alpha2: CGFloat) {
        UIView.animate(withDuration: .t025, delay: .zero, options: .curveEaseInOut, animations: { [weak self] in
            self?.disableDeviceLabel.alpha = alpha1
        })
        UIView.animate(withDuration: .t025, delay: .zero, options: .curveEaseInOut, animations: { [weak self] in
            self?.setIntoPairingModeLabel.alpha = alpha2
        })
    }
}
extension PairingViewController {
    func indicateDisconnectWill() {
        update(description1Label: Const.alphaMax, description2Label: Const.alphaMin)
    }
    func indicateLongPressConnectWill() {
        update(description1Label: Const.alphaMin, description2Label: Const.alphaMax)
    }
    func showPairingCoverView() {
        pairingCoverView.headerLabelAttributedText = UIFont.attributedLargeTitle(Strings.pairing_select_device_title_uc())
        pairingCoverView.isHidden = false
    }
    func fadeOutViews() {
        viewModel?.inputs.pairingFadeOutCompleted()
    }
}
