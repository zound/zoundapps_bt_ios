//
//  StayUpdatedViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 20.03.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

class StayUpdatedViewController: UIViewController, UIViewControllerTransitioningDelegate {
    
    //UNSUBSCRIBE:
    @IBOutlet weak var successInfo: UILabel!
    @IBOutlet weak var currentMailLabel: UILabel!
    @IBOutlet weak var emailArea: UIView!
    @IBOutlet weak var unsubscribeButton: UIButton!
    
    // EMAIL SUBSCRIPTION:
    @IBOutlet weak var markIcon: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var descriptionText: UILabel!
    @IBOutlet weak var editorArea: UIView!
    
    // COMMON:
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var privacyPolicyText: UITextView!
    
    @IBOutlet weak var subtitleTopAllign: NSLayoutConstraint!
    
    var viewModel: StayUpdatedViewModelType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = Color.background
        titleLabel.resizableAttributedFont(text: UIFont.attributedLargeTitle(Strings.stay_updated_title_uc()))
        subtitleLabel.attributedText = UIFont.attributedText(Strings.stay_updated_subtitle_v1(), color: Color.text)
        
        skipButton.setAttributedTitle(UIFont.attributedSecondaryButtonLabel(Strings.appwide_skip_uc()), for: .normal)
        nextButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.appwide_next_uc()), for: .normal)
        unsubscribeButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.share_data_screen_unsubscribe_uc()), for: .normal)

        setupPrivacyPolicyString()

        viewModel?.outputs.skipButtonVisible
            .asDriver()
            .drive(skipButton.rx.visible)
            .disposed(by: rx_disposeBag)
        
        viewModel?.outputs.indicatorVisible
            .asDriver()
            .drive(onNext: { [unowned self] visible in
                self.indicator.isHidden = !visible
                if visible {
                    self.indicator.startAnimating()
                } else {
                    self.indicator.stopAnimating()
                }
            }).disposed(by: rx_disposeBag)
        
        viewModel?.outputs.emailAreaVisible
            .asDriver()
            .drive(emailArea.rx.visible)
            .disposed(by: rx_disposeBag)
        
        viewModel?.outputs.editorAreaVisible
            .asDriver()
            .drive(editorArea.rx.visible)
            .disposed(by: rx_disposeBag)
        
        
        // display Next button when view model will notify about it
        viewModel?.outputs.nextButtonVisible
            .asDriver()
            .drive(nextButton.rx.visible)
            .disposed(by: rx_disposeBag)

        viewModel?.outputs.nextButtonEnable
            .asDriver()
            .drive(onNext: { [unowned self] enabled in
                self.nextButton.alpha = enabled ? 1.0 : 0.6
                self.nextButton.isEnabled = enabled
            }).disposed(by: rx_disposeBag)


        // display Next button when view model will notify about it
        viewModel?.outputs.unsubscribeButtonVisible
            .asDriver()
            .drive(unsubscribeButton.rx.visible)
            .disposed(by: rx_disposeBag)
        viewModel?.outputs.unsubscribeButtonVisible
            .asDriver()
            .drive(unsubscribeButton.rx.visible)
            .disposed(by: rx_disposeBag)
        
        
        // EMAIL SUBSCRIPTION:
        applyFormInputStyle(emailField)
        emailField.text = nil
        emailField.keyboardType = .emailAddress
        emailField.keyboardAppearance = .dark
        emailField.attributedPlaceholder = UIFont.attributedEmailAddressPlaceholder(Strings.stay_updated_email_address(), color: Color.text)
        emailField.typingAttributes = UIFont.attributedEmailAddress(color: Color.text)
        emailField.defaultTextAttributes = UIFont.attributedEmailAddress(color: Color.text)
        emailField.addTarget(self,
                             action: #selector(emailChanged(_:)),
                             for: [.editingDidEndOnExit, .editingChanged])
        
        viewModel?.outputs.resetEmailEditor
            .asDriver()
            .map { nil } // clear email field
            .drive(emailField.rx.text)
            .disposed(by: rx_disposeBag)

        viewModel?.outputs.descriptionText
            .asDriver()
            .map { UIFont.attributedRenameValidationText($0) }
            .drive(descriptionText.rx.attributedText)
            .disposed(by: rx_disposeBag)
        
        viewModel?.outputs.descriptionIcon
            .asDriver()
            .drive(markIcon.rx.image)
            .disposed(by: rx_disposeBag)
        
        //UNSUBSCRIBE:
        viewModel?.outputs.subscriptionEmail
            .asDriver()
            .drive(currentMailLabel.rx.text)
            .disposed(by: rx_disposeBag)
        
        successInfo.attributedText = UIFont.attributedText(Strings.stay_updated_email_confirmation(), color: Color.text)
    }
    
    @objc internal func emailChanged(_ textField: UITextField) {
        emailField.typingAttributes = UIFont.attributedEmailAddress(color: Color.text)
        viewModel?.inputs.emailChanged(name: textField.text ?? "")
    }
    
    @IBAction func onNext(_ sender: UIButton) {
        viewModel?.inputs.nextButtonTapped()
    }
    
    @IBAction func onSkip(_ sender: UIButton) {
        viewModel?.inputs.skipButtonTapped()
    }
    
    @IBAction func unsubscribe(_ sender: Any) {
        viewModel?.inputs.unsubscribeButtonTapped()
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        emailField.endEditing(false)
    }
}

// MARK: - UITextViewDelegate
extension StayUpdatedViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        let safariViewController = SafariViewController.with(contentType: .privacyPolicy)
        safariViewController.transitioningDelegate = self
        present(safariViewController, animated: true)
        return true
    }
}

// MARK: - Footer - privacy policy
private extension StayUpdatedViewController {
    private func setupPrivacyPolicyString() {
        let plainText = UIFont.attributedPrivacyPolicy(Strings.stay_updated_footer_v1(), color: Color.text)
        let urlPrivacyPolicy = getPrivacyPolicyString()
        
        let finalPrivacyPolicy = NSMutableAttributedString(attributedString: plainText)
        finalPrivacyPolicy.append(urlPrivacyPolicy)
        
        privacyPolicyText.attributedText = finalPrivacyPolicy
        privacyPolicyText.textAlignment = .center
        privacyPolicyText.delegate = self
    }
    
    private func getPrivacyPolicyString() -> NSAttributedString {
        let link = Strings.stay_updated_privacy_policy()
        let urlString = NSMutableAttributedString(attributedString: UIFont.attributedPrivacyPolicy(link, color: Color.text))
        
        urlString.addAttribute(.link, value: String(), range: NSRange(location: Int(), length: link.count) )
        urlString.addAttribute(.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: Int(), length: link.count) )
        
        return urlString
    }
}
