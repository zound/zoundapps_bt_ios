//
//  AutoOffTimerViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 11/12/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift
import GUMA

private struct Config {
    struct PickerComponent {
        static let hours = 0
        static let minutes = 1
    }
    struct Alpha {
        static let min: CGFloat = 0.0
        static let disabled: CGFloat = 0.3
        static let max: CGFloat = 1.0
    }
    struct Picker {
        static let rowHeight: CGFloat = 100
        static let numberOfComponents = 2
    }
    static let colon = ":"
    static let actionButtonEnableDelay: TimeInterval = 2
    static let timePickerFormat = "%02d"
}

class AutoOffTimerViewController: BaseViewController {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var remainingTimeLabel: UILabel!
    @IBOutlet weak var remainingTimeColonLabel: BlinkingLabel!
    @IBOutlet weak var timePickerView: UIPickerView!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var minutesLabel: UILabel!
    @IBOutlet weak var onHoursLabel: UILabel!
    @IBOutlet weak var onMinutesLabel: UILabel!

    var viewModel: AutoOffTimerViewModel?
    private let disposeBag = DisposeBag()

    private var currentAutoOffTime: AutoOffTime?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: UIFont.attributedTitle(Strings.device_settings_menu_item_auto_off_timer_uc()))
        setupActionButton()
        setupDescriptionLabel()
        setupHoursAndMinutesLabels()
        setupRemainingTimeColonLabel()
        setupTimePickerView()
        let outputAutoOffTimeInit = viewModel?.outputs.outputAutoOffTimeInit.asObservable().observeOn(MainScheduler.instance)
        outputAutoOffTimeInit?.subscribe({ [weak self] outputAutoOffTimeInit in
            guard let strongSelf = self, let outputAutoOffTime = outputAutoOffTimeInit.element else { return }
            strongSelf.currentAutoOffTime = outputAutoOffTime
            strongSelf.updateRelatedUIComponents(initial: true)
            strongSelf.removeLoadingView(animated: false)
            strongSelf.setActionButton(enabled: strongSelf.currentAutoOffTime! == .off ? false : true)
        }).disposed(by: disposeBag)
        let outputAutoOffTime = viewModel?.outputs.outputAutoOffTime.asObservable().observeOn(MainScheduler.instance)
        outputAutoOffTime?.subscribe({ [weak self] outputAutoOffTimeElement in
            guard let strongSelf = self, let outputAutoOffTime = outputAutoOffTimeElement.element else { return }
            guard strongSelf.currentAutoOffTime != outputAutoOffTime else { return }
            strongSelf.currentAutoOffTime = outputAutoOffTime
            strongSelf.updateRelatedUIComponents()
        }).disposed(by: disposeBag)
        viewModel?.inputs.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if currentAutoOffTime == nil {
            addLoadingView(animated: true)
        }
        viewModel?.inputs.viewDidAppear()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel?.inputs.viewWillDisappear()
    }
    deinit {
        print(#function, String(describing: self))
    }
    @objc private func actionButtonTouchedUpInside() {
        let hours = AutoOffTime.timePickerHourComponentItems[timePickerView.selectedRow(inComponent: Config.PickerComponent.hours)]
        let minutes = AutoOffTime.timePickerMinuteComponentItems[timePickerView.selectedRow(inComponent: Config.PickerComponent.minutes)]
        guard let autoOffTime = AutoOffTime(hours: hours, minutes: minutes) else {
            return
        }
        setActionButton(enabled: false, animated: true, completion: { [weak self] _ in
            if case .some(.off) = self?.currentAutoOffTime {
                if  autoOffTime != .off {
                    self?.currentAutoOffTime = autoOffTime
                    self?.updateRelatedUIComponents()
                    self?.viewModel?.inputAutoOffTime.accept(autoOffTime)
                }
            } else {
                self?.currentAutoOffTime = .off
                self?.updateRelatedUIComponents()
                self?.viewModel?.inputAutoOffTime.accept(.off)

            }
            self?.setActionButton(enabled: autoOffTime != .off, animated: true, delay: Config.actionButtonEnableDelay)
        })
    }
}

private extension AutoOffTimerViewController {
    func setupActionButton() {
        actionButton.backgroundColor = Color.primaryButton
        actionButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(.invisible), for: .normal)
        actionButton.addTarget(self, action: #selector(actionButtonTouchedUpInside), for: .touchUpInside)
        actionButton.alpha = .zero
    }
    func setActionButton(enabled: Bool, animated: Bool = false, delay: TimeInterval = .zero, completion: ((Bool) -> Void)? = nil) {
        actionButton.isEnabled = enabled
        UIView.animate(withDuration: animated ? .t010 : .zero, delay: delay, options: .curveEaseInOut, animations: { [weak self] in
            self?.actionButton.alpha = enabled ? Config.Alpha.max : Config.Alpha.disabled
        }, completion: completion)
    }
    func setupDescriptionLabel() {
        descriptionLabel.attributedText = UIFont.attributedText(.invisible, color: .white)
    }
    func setupRemainingTimeColonLabel() {
        remainingTimeColonLabel.attributedText = UIFont.attributedH1(Config.colon)
    }
    func setupHoursAndMinutesLabels() {
        hoursLabel.attributedText = UIFont.attributedSmallerText(Strings.auto_off_timer_screen_hours(), color: UIColor.mediumGrey)
        onHoursLabel.attributedText = UIFont.attributedSmallerText(Strings.auto_off_timer_screen_hours(), color: UIColor.mediumGrey)
        minutesLabel.attributedText = UIFont.attributedSmallerText(Strings.auto_off_timer_screen_minutes(), color: UIColor.mediumGrey)
        onMinutesLabel.attributedText = UIFont.attributedSmallerText(Strings.auto_off_timer_screen_minutes(), color: UIColor.mediumGrey)
        [hoursLabel, onHoursLabel, minutesLabel, onMinutesLabel].forEach {
            $0!.alpha = .zero
            view.sendSubview(toBack: $0!)
        }
    }
    func setupTimePickerView() {
        timePickerView.delegate = self
        timePickerView.dataSource = self
        view.bringSubview(toFront: timePickerView)
        setTimePicker(visible: false, animated: false)
    }
    func updateDescriptionLabel(text: String, animated: Bool = false) {
        if animated {
            descriptionLabel.animateTextTransition(with: UILabel.TextTransitionParameters(
                attributedText: UIFont.attributedText(text, color: .white),
                textAligment: .center,
                lineBreakMode: .byTruncatingTail,
                animationOptions: [.curveEaseInOut, .transitionCrossDissolve]
            ))
        } else {
            descriptionLabel.attributedText = UIFont.attributedText(text, color: .white)
        }
    }
    func updateRemainingTimeLabel(text: NSAttributedString, animated: Bool = false) {
        func startBlinkIfPossible() {
            if text.string != .invisible {
                remainingTimeColonLabel.startBlink()
            }
        }
        if animated {
            remainingTimeColonLabel.stopBlink()
            remainingTimeLabel.animateTextTransition(with: UILabel.TextTransitionParameters(
                attributedText: text,
                textAligment: .center,
                lineBreakMode: .byTruncatingTail,
                animationOptions: [.curveEaseInOut, .transitionFlipFromTop]
            ), completion: { _ in
                startBlinkIfPossible()
            })
        } else {
            remainingTimeLabel.attributedText = text
            startBlinkIfPossible()
        }
    }
    func setTimePicker(visible: Bool, animated: Bool, completion: (() -> Void)? = nil) {
        timePickerView.isUserInteractionEnabled = false
        let animations: () -> Void = { [weak self] in
            self?.timePickerView.alpha = visible ? Config.Alpha.max : Config.Alpha.min
            self?.hoursLabel.alpha = visible ? Config.Alpha.max : Config.Alpha.min
            self?.minutesLabel.alpha = visible ? Config.Alpha.max : Config.Alpha.min
        }
        let completionBlock: (Bool) -> Void = { [weak self] _ in
            self?.timePickerView.isUserInteractionEnabled = true
            completion?()
        }
        UIView.animate(withDuration: animated ? .t025 : .zero, delay: animated ? .t010 : .zero,
                       options: .curveEaseInOut, animations: animations, completion: completionBlock)
    }
}

private extension AutoOffTimerViewController {
    func updateRelatedUIComponents(initial: Bool = false) {
        guard let currentAutoOffTime = currentAutoOffTime else {
            return
        }
        updateDescriptionLabel(text: currentAutoOffTime.descriptionText, animated: true)
        remainingTimeLabel.alpha = currentAutoOffTime.isRemainingTimeHidden ? Config.Alpha.min : Config.Alpha.max
        remainingTimeColonLabel.alpha = currentAutoOffTime.isRemainingTimeHidden ? Config.Alpha.min : Config.Alpha.max
        onHoursLabel.alpha = currentAutoOffTime.isRemainingTimeHidden ? Config.Alpha.min : Config.Alpha.max
        onMinutesLabel.alpha = currentAutoOffTime.isRemainingTimeHidden ? Config.Alpha.min : Config.Alpha.max
        updateRemainingTimeLabel(text: currentAutoOffTime.remainingTimeText, animated: !initial)
        switch currentAutoOffTime {
        case .off:
            actionButton.setAttributedTitleAnimated(UIFont.attributedPrimaryButtonLabel(Strings.auto_off_timer_button_start_uc()), for: .normal)
            setTimePicker(visible: true, animated: true)
        case .on:
            actionButton.setAttributedTitleAnimated(UIFont.attributedPrimaryButtonLabel(Strings.auto_off_timer_button_stop_uc()), for: .normal)
            setTimePicker(visible: false, animated: false)
        }
    }
}

extension AutoOffTimerViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return Config.Picker.numberOfComponents
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return component == .zero ? AutoOffTime.timePickerHourComponentItems.count : AutoOffTime.timePickerMinuteComponentItems.count
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        pickerView.subviews[1].backgroundColor = .clear
        pickerView.subviews[2].backgroundColor = .clear
        let pickerLabel = UILabel()
        let text = AutoOffTime.formattedForTimePicker(item: AutoOffTime.timePickerComponents[component][row])
        pickerLabel.attributedText = UIFont.attributedH0(text)
        pickerLabel.textAlignment = .center
        return pickerLabel
    }

    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return Config.Picker.rowHeight
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let hours = AutoOffTime.timePickerHourComponentItems[timePickerView.selectedRow(inComponent: Config.PickerComponent.hours)]
        let minutes = AutoOffTime.timePickerMinuteComponentItems[timePickerView.selectedRow(inComponent: Config.PickerComponent.minutes)]
        if let autoOffTime = AutoOffTime(hours: hours, minutes: minutes), autoOffTime.totalMinutes > .zero {
            setActionButton(enabled: true, animated: true)
        } else {
            setActionButton(enabled: false, animated: true)
        }
    }
}

private extension AutoOffTime {
    static let timePickerHourComponentItems: [Int] = [0, 1, 2]
    static let timePickerMinuteComponentItems: [Int] = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55]
    static let timePickerComponents = [
        timePickerHourComponentItems, timePickerMinuteComponentItems
    ]
    static func formattedForTimePicker(item: Int) -> String {
        return String(format: Config.timePickerFormat, item)
    }
    var descriptionText: String {
        switch self {
        case .off:
            return Strings.auto_off_timer_screen_subtitle_off()
        case .on:
            return Strings.auto_off_timer_screen_subtitle_on()
        }
    }
    var remainingTimeText: NSAttributedString {
        switch self {
        case .off:
            return UIFont.attributedH0(.invisible)
        case .on:
            return UIFont.attributedH0(
                AutoOffTime.formattedForTimePicker(item: hours) +
                .invisible +
                AutoOffTime.formattedForTimePicker(item: minutes)
            )
        }
    }
    var isRemainingTimeHidden: Bool {
        return self == .off
    }
    var isTimePickerHidden: Bool {
        return !isRemainingTimeHidden
    }
}
