//
//  PairingDoneViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 03/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

class PairingDoneViewController: BaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var deviceImageView: UIImageView!
    @IBOutlet weak var doneButton: UIButton!

    private lazy var innerShaddowView: InnerShaddowView = {
        let innerShaddowView = InnerShaddowView()
        innerShaddowView.backgroundColor = .black
        return innerShaddowView
    }()

    var viewModel: PairingDoneViewModelType!
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        setupConstraints()
        titleLabel.resizableAttributedFont(text: UIFont.attributedLargeTitle(Strings.ota_screen_finished_title_uc()))
        doneButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.appwide_done_uc()), for: .normal)
        viewModel.outputs.deviceImage
            .asDriver()
            .drive(deviceImageView.rx.image)
            .disposed(by: disposeBag)
        let setupLabelAction = setup(subtitleLabel: subtitleLabel)
        viewModel.outputs.deviceName
            .asDriver()
            .drive(onNext: setupLabelAction)
            .disposed(by: disposeBag)
        addLoadingView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        removeLoadingView(animated: true)
    }
    @IBAction func onDone(_ sender: UIButton) {
        viewModel.inputs.doneTapped()
    }
}

private extension PairingDoneViewController {
    private func setupConstraints() {
        [innerShaddowView].forEach {
            view.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        view.sendSubview(toBack: innerShaddowView)
        NSLayoutConstraint.activate([
            innerShaddowView.topAnchor.constraint(equalTo: view.topAnchor),
            innerShaddowView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            innerShaddowView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            innerShaddowView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

private func setup(subtitleLabel: UILabel) -> (String) -> Void {
    return { deviceName in
        let subtitleComponents = Strings.pairing_screen_finished_subtitle().components(separatedBy: stringComponentSeparator)
        guard let firstComponentString = subtitleComponents[optional: 0],
            let secondComponentString = subtitleComponents[optional: 1] else {
                subtitleLabel.attributedText = NSAttributedString()
                return
        }
        let subtitleString = [
                firstComponentString,
                deviceName,
                secondComponentString
            ]
            .reduce(String(), +)
        subtitleLabel.resizableAttributedFont(text: UIFont.attributedText(subtitleString, color: Color.text), numberOfLines: 0)
    }
}
