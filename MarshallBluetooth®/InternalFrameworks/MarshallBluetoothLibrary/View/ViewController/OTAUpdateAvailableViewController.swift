//
//  OTAUpdateAvailableViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 14.09.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift

class OTAUpdateAvailableViewController: BaseViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    
    internal var viewModel: OTAUpdateAvailableViewModelType?
    internal let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.resizableAttributedFont(text: UIFont.attributedLargeTitle(Strings.ota_screen_notification_title_uc()), numberOfLines: 2)
        subtitleLabel.attributedText = UIFont.attributedText(Strings.ota_screen_notification_subtitle(), color: Color.text)
        continueButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.appwide_done_uc()), for: .normal)
        
        continueButton.rx.tap
            .subscribe(onNext: { [viewModel] _ in
                viewModel?.inputs.continueTapped() })
            .disposed(by: disposeBag)
    }
    
    deinit {
        print(#function, String(describing: self))
    }
}
