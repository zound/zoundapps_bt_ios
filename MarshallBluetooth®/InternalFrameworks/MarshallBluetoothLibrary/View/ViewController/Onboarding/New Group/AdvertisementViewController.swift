//
//  AdvertisementViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 10/04/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

class AdvertisementViewController: UIViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var squaredPageIndicator: SquaredPageIndicatorView!
    @IBOutlet weak var skipButton: UIButton!
    var advertisementPagesViewController: UIPageViewController!
    private var advertisementPages = [UIViewController]()
    var currentIndex: Int?
    private var pendingIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create the page container
        advertisementPagesViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        advertisementPagesViewController.delegate = self
        advertisementPagesViewController.dataSource = self

        advertisementPagesViewController.view.backgroundColor = Color.background
        
        restartAction(sender: self)
        addChildViewController(advertisementPagesViewController)
        view.addSubview(advertisementPagesViewController.view)
        
        advertisementPagesViewController.view.translatesAutoresizingMaskIntoConstraints = false
        
        applyConstraints(parent: view, child: advertisementPagesViewController.view)
        self.advertisementPagesViewController.didMove(toParentViewController: self)

        // Configure page control
        squaredPageIndicator.configure(numPages: advertisementPages.count)
        view.bringSubview(toFront: squaredPageIndicator)
        
        // Configure progress view
        view.bringSubview(toFront: progressView)

        // Configure status label
        view.bringSubview(toFront: statusLabel)
        
        view.bringSubview(toFront: skipButton)
    }
    func configure(withAdvertisementControllers controllers: [UIViewController]) {
        advertisementPages.append(contentsOf: controllers)
    }
    func restartAction(sender: AnyObject) {
        advertisementPagesViewController.setViewControllers([advertisementPages[0]], direction: .forward, animated: true, completion: nil)
    }
    private func applyConstraints(parent: UIView, child: UIView) {
        NSLayoutConstraint.activate([
            child.topAnchor.constraint(equalTo: parent.topAnchor),
            child.leftAnchor.constraint(equalTo: parent.leftAnchor),
            child.rightAnchor.constraint(equalTo: parent.rightAnchor),
            child.bottomAnchor.constraint(equalTo: parent.bottomAnchor)
        ])
    }
    // MARK - UIPageViewController delegates
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        currentIndex = advertisementPages.index(of: viewController)
        
        guard let currentIndex = currentIndex else {
            fatalError("No advertisement page found for UIViewController: \(viewController)")
        }
        if currentIndex == 0 { return nil }
        
        let previousIndex = abs((currentIndex - 1) % advertisementPages.count)
        return advertisementPages[previousIndex]
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        currentIndex = advertisementPages.index(of: viewController)
        
        guard let currentIndex = currentIndex else {
            fatalError("No advertisement page found for UIViewController: \(viewController)")
        }
        if currentIndex == advertisementPages.count - 1 {
            return nil
        }
        let nextIndex = abs((currentIndex + 1) % advertisementPages.count)
        return advertisementPages[nextIndex]
    }
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        
        guard let firstPendingViewController = pendingViewControllers.first else {
            return
        }
        pendingIndex = advertisementPages.index(of: firstPendingViewController)
        guard let pendingIndex = pendingIndex else {
            return
        }
        squaredPageIndicator.currentPage = pendingIndex
    }
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {

        guard let finalViewController = advertisementPagesViewController.viewControllers?.last else { return }
        guard let finalIndex = advertisementPages.index(of: finalViewController) else { return }
        
        self.currentIndex = finalIndex
        squaredPageIndicator.currentPage = finalIndex
    }
}
