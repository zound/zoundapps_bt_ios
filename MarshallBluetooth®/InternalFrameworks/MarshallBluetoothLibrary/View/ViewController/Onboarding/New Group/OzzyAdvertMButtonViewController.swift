//
//  OzzyAdvertMButtonViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 08/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit

/// Customize advertisement view.
/// No view model because of no inputs/outputs (static screen with no interaction with user)
class OzzyAdvertMButtonViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var advertImageView: UIImageView!
    
    override func viewDidLoad() {
        titleLabel.resizableAttributedFont(text: UIFont.attributedLargeTitle(Strings.ozzy_onboarding_mbutton_title_uc()), numberOfLines: 0)
        subtitleLabel.resizableAttributedFont(text: UIFont.attributedText(Strings.ozzy_onboarding_mbutton_subtitle(), color: Color.text), numberOfLines: 0)
        subtitleLabel.textAlignment = .center
        view.backgroundColor = .black
    }
}
