//
//  JoplinAdvertisementViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 08/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

final class JoplinAdvertisementViewController: AdvertisementViewController {
    var viewModel: JoplinAdvertisementViewModelType!

    override func viewDidLoad() {
        super.viewDidLoad()
        skipButton.isHidden = true
        progressView.progressTintColor = Color.progressBarTint
        progressView.trackTintColor = Color.otaProgressUnfilled
        NSLayoutConstraint.activate([progressView.heightAnchor.constraint(equalToConstant: 2.0)])
        progressView.progress = 0.0
        statusLabel.attributedText = UIFont.otaProgressLabel(Strings.ota_screen_downloading_firmware())
        
        viewModel.outputs.setupProgress
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [progressView, statusLabel] progressInfo in
                progressView?.progress = Float(Float(progressInfo.entry.maxValue) / 100.0)
                statusLabel?.attributedText = UIFont.otaProgressLabel(progressInfo.entry.label) })
            .disposed(by: rx_disposeBag)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.inputs.viewAppeared()
    }
}
