//
//  OzzyAdvertEqualizerViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 08/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit

/// Customize advertisement view.
/// No view model because of no inputs/outputs (static screen with no interaction with user)
class OzzyAdvertEqualizerViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var advertImageView: UIImageView!

    private lazy var innerShaddowView: InnerShaddowView = {
        let innerShaddowView = InnerShaddowView()
        innerShaddowView.backgroundColor = .black
        return innerShaddowView
    }()
    
    override func viewDidLoad() {
        setupConstraints()
        titleLabel.resizableAttributedFont(text: UIFont.attributedLargeTitle(Strings.ozzy_onboarding_eq_title_uc()), numberOfLines: 0)
        subtitleLabel.resizableAttributedFont(text: UIFont.attributedText(Strings.ozzy_onboarding_eq_subtitle(), color: Color.text), numberOfLines: 0)
        subtitleLabel.textAlignment = .center
        view.backgroundColor = .black

    }
    private func setupConstraints() {
        [innerShaddowView].forEach {
            view.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        view.sendSubview(toBack: innerShaddowView)
        NSLayoutConstraint.activate([
            innerShaddowView.topAnchor.constraint(equalTo: view.topAnchor),
            innerShaddowView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            innerShaddowView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            innerShaddowView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

class InnerShaddowView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        addInnerShadow()
    }
    private func addInnerShadow() {
        let innerShadow = CALayer()
        innerShadow.frame = bounds
        innerShadow.shadowPath = UIBezierPath(rect: innerShadow.bounds).cgPath
        innerShadow.masksToBounds = true
        innerShadow.shadowColor = Color.background.cgColor
        innerShadow.shadowOffset = .zero
        innerShadow.shadowOpacity = 1
        innerShadow.shadowRadius = 60
        layer.addSublayer(innerShadow)
    }
}
