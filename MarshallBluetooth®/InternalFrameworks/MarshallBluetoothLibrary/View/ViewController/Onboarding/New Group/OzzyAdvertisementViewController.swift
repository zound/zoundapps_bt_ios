//
//  OzzyAdvertisementViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 08/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

private struct Config {
    struct Alpha {
        static let max: CGFloat = 1.0
    }
}

class OzzyAdvertisementViewController: UIViewController {
    @IBOutlet weak var squaredPageIndicator: SquaredPageIndicatorView!
    @IBOutlet weak var actionButton: UIButton!

    var advertisementPagesViewController: UIPageViewController!
    private var advertisementPages = [UIViewController]()
    var currentIndex: Int?
    private var pendingIndex: Int?

    var viewModel: OzzyAdvertisementViewModelType!
    override func viewDidLoad() {
        super.viewDidLoad()
        configureAdvertisementPagesViewController()
        configureSquaredPageIndicator()
        configureActionButton()
    }
    @objc func actionButtonTouchedUpInside(_ sender: UIButton) {
        viewModel?.inputs.skipTapped()
    }
    func configure(withAdvertisementControllers controllers: [UIViewController]) {
        advertisementPages.append(contentsOf: controllers)
    }
}

private extension OzzyAdvertisementViewController {
    func configureActionButton() {
        (actionButton.isEnabled, actionButton.alpha) = (false, .zero)
        view.bringSubview(toFront: actionButton)
        actionButton.backgroundColor = Color.primaryButton
        actionButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.appwide_continue_uc()), for: .normal)
        actionButton.addTarget(self, action: #selector(actionButtonTouchedUpInside), for: .touchUpInside)
    }
    func configureAdvertisementPagesViewController() {
        advertisementPagesViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        advertisementPagesViewController.delegate = self
        advertisementPagesViewController.dataSource = self
        advertisementPagesViewController.view.backgroundColor = .black
        restartAction(sender: self)
        addChildViewController(advertisementPagesViewController)
        view.addSubview(advertisementPagesViewController.view)
        advertisementPagesViewController.view.translatesAutoresizingMaskIntoConstraints = false
        applyConstraints(parent: view, child: advertisementPagesViewController.view)
        self.advertisementPagesViewController.didMove(toParentViewController: self)
    }
    func configureSquaredPageIndicator() {
        squaredPageIndicator.configure(numPages: advertisementPages.count)
        view.bringSubview(toFront: squaredPageIndicator)
    }
    func restartAction(sender: AnyObject) {
        advertisementPagesViewController.setViewControllers([advertisementPages[0]], direction: .forward, animated: true, completion: nil)
    }
    func applyConstraints(parent: UIView, child: UIView) {
        NSLayoutConstraint.activate([
            child.topAnchor.constraint(equalTo: parent.topAnchor),
            child.leftAnchor.constraint(equalTo: parent.leftAnchor),
            child.rightAnchor.constraint(equalTo: parent.rightAnchor),
            child.bottomAnchor.constraint(equalTo: parent.bottomAnchor)
        ])
    }
    func showActionButton() {
        squaredPageIndicator.isHidden = true
        UIView.animate(withDuration: .t010, delay: .zero, options: .curveEaseInOut, animations: { [weak self] in
            self?.actionButton.alpha = Config.Alpha.max
        }, completion: { [weak self] _ in
            self?.actionButton.isEnabled = true
        })
    }
    func removeAdvertisementPagesViewControllerSwipeGesture() {
        for view in advertisementPagesViewController.view.subviews {
            if let subview = view as? UIScrollView {
                subview.isScrollEnabled = false
            }
        }
    }
}

extension OzzyAdvertisementViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        currentIndex = advertisementPages.index(of: viewController)
        guard let currentIndex = currentIndex else {
            fatalError("No advertisement page found for UIViewController: \(viewController)")
        }
        if currentIndex == .zero { return nil }
        let previousIndex = abs((currentIndex - 1) % advertisementPages.count)
        return advertisementPages[previousIndex]
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        currentIndex = advertisementPages.index(of: viewController)
        guard let currentIndex = currentIndex else {
            fatalError("No advertisement page found for UIViewController: \(viewController)")
        }
        if currentIndex == advertisementPages.count - 1 {
            return nil
        }
        let nextIndex = abs((currentIndex + 1) % advertisementPages.count)
        return advertisementPages[nextIndex]
    }
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        guard let firstPendingViewController = pendingViewControllers.first else {
            return
        }
        pendingIndex = advertisementPages.index(of: firstPendingViewController)
        guard let pendingIndex = pendingIndex else {
            return
        }
        squaredPageIndicator.currentPage = pendingIndex
    }
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let finalViewController = advertisementPagesViewController.viewControllers?.last else { return }
        guard let finalIndex = advertisementPages.index(of: finalViewController) else { return }
        self.currentIndex = finalIndex
        squaredPageIndicator.currentPage = finalIndex
        if currentIndex == advertisementPages.count - 1 {
            removeAdvertisementPagesViewControllerSwipeGesture()
            showActionButton()
        }
    }
}
