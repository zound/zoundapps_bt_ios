//
//  OnboardingMButtonViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 10/02/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift
import GUMA

private struct Config {
    static let imageMargin: CGFloat = 70
    static let numberOfRowsInSection = 1
    static let heightForHeaderInSection = CGFloat(3)
    static let checkmarkRightInset = CGFloat(10)
}

class OnboardingMButtonViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    @IBOutlet weak var imageViewTopConstraint: NSLayoutConstraint!
    
    var viewModel: OnboardingMButtonViewModelType!
    private let disposeBag = DisposeBag()

    private var selectedMButtonMode: MButtonMode?
    private let modes: [MButtonMode] = MButtonMode.allCases

    override func viewDidLoad() {
        super.viewDidLoad()
        let navBarConfiguration = NavigationBarConfiguration(isBackButtonHidden: false, isTranslucent: true, navigationBarTheme: .dark)
        setupNavigationBar(title: UIFont.attributedTitle(Strings.device_settings_menu_item_m_button_uc()), configuration: navBarConfiguration)
        setupDescriptionLabel()
        setupImageViewTopConstraint()
        setupTableView()
        setupActionButton()
        let outputMButtonModeInit = viewModel?.outputs.outputMButtonModeInit.asObservable().observeOn(MainScheduler.instance)
        outputMButtonModeInit?.subscribe({ [weak self] outputMButtonModeInit in
            guard let strongSelf = self, let outputMButtonMode = outputMButtonModeInit.element else { return }
            strongSelf.setSelected(mode: outputMButtonMode)
            strongSelf.removeLoadingView(animated: false)
        }).disposed(by: disposeBag)
        let outputMButtonMode = viewModel?.outputs.outputMButtonMode.asObservable().observeOn(MainScheduler.instance)
        outputMButtonMode?.subscribe({ [weak self] outputMButtonModeElement in
            guard let strongSelf = self, let outputMButtonMode = outputMButtonModeElement.element else { return }
            guard strongSelf.selectedMButtonMode != outputMButtonMode else { return }
            strongSelf.setSelected(mode: outputMButtonMode)
        }).disposed(by: disposeBag)
        viewModel?.inputs.viewDidLoad()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if selectedMButtonMode == nil {
            addLoadingView(animated: false)
        }
    }
    deinit {
        print(#function, String(describing: self))
    }
    @objc private func actionButtonTouchedUpInside() {
        viewModel?.inputs.actionButtonTouchedUpInside()
    }
}

private extension OnboardingMButtonViewController {
    func setupDescriptionLabel() {
        descriptionLabel.attributedText = UIFont.attributedText(Strings.m_button_screen_subtitle(), color: .white)
    }
    func setupImageViewTopConstraint() {
        imageViewTopConstraint.constant = -(navigationController?.navigationBar.frame.height ?? .zero) - Config.imageMargin
    }
    func setupTableView() {
        tableView.register(SelectableTableViewCell.self, forCellReuseIdentifier: SelectableTableViewCell.defaultReusableId)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorInset = UIEdgeInsets(top: 10, left: CGFloat(), bottom: CGFloat(), right: .greatestFiniteMagnitude)
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.isScrollEnabled = false
    }
    func setupActionButton() {
        actionButton.backgroundColor = Color.primaryButton
        actionButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.appwide_next_uc()), for: .normal)
        actionButton.addTarget(self, action: #selector(actionButtonTouchedUpInside), for: .touchUpInside)
    }
    func setSelected(mode: MButtonMode) {
        guard let index = modes.index(of: mode) else {
            return
        }
        selectedMButtonMode = mode
        tableView.selectRow(at: IndexPath(row: index, section: .zero), animated: true, scrollPosition: .none)
    }
}

extension OnboardingMButtonViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return Config.numberOfRowsInSection
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modes.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Config.heightForHeaderInSection
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView().with(backgroundColor: .clear)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = SelectableTableViewCell.defaultReusableId
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? SelectableTableViewCell else {
            return UITableViewCell()
        }
        print(modes, indexPath.row, modes[indexPath.row])
        cell.textLabel?.attributedText = UIFont.attributedLabelText(modes[indexPath.row].title)
        cell.detailTextLabel?.attributedText = UIFont.attributedLabelSubText(modes[indexPath.row].subtitle)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedMButtonMode = modes[indexPath.row]
        viewModel?.inputs.inputMButtonMode.accept(selectedMButtonMode!)
    }
}
