//
//  AboutThisSpeakerViewController.swift
//  MarshallBluetooth®
//
//  Created by Bartosz Dolewski on 21.05.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import GUMA

private struct Config {
    static let buttonAreaExpandedHeight = CGFloat(110)
    struct ButtonAreaAnimation {
        static let duration: TimeInterval = .seconds(0.3)
        static let curve: UIViewAnimationCurve = .easeInOut
    }
}

class AboutThisSpeakerViewController: BaseViewController, ButtonAreaAnimating {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var speakerNameLabel: UILabel!
    
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var speakerModelLabel: UILabel!
    
    @IBOutlet weak var systemFwLabel: UILabel!
    @IBOutlet weak var speakerSystemFwLabel: UILabel!
    
    @IBOutlet weak var updateButton: UIButton!
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var buttonAreaHeightConstraint: NSLayoutConstraint!
    
    internal var viewModel: AboutThisSpeakerViewModelType?
    private var disposeBag = DisposeBag()
    
    lazy var buttonAreaHeight: NSLayoutConstraint = {
        return buttonAreaHeightConstraint
    }()
    
    lazy var buttonAreaBaseView: UIView = {
        return view
    }()
    
    lazy var buttonAreaHeightAnimator: UIViewPropertyAnimator = {
        return getDefautButtonAreaHeightAnimator()
    }()
    
    lazy var buttonAreaState: ButtonAreaState = .disabled
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar(title: UIFont.attributedTitle(viewModel?.inputs.hadrwareType.title ?? String()))
        
        nameLabel.attributedText = UIFont.attributedLabel(Strings.about_this_speaker_screen_name())
        modelLabel.attributedText = UIFont.attributedLabel(Strings.about_this_speaker_screen_model())
        systemFwLabel.attributedText = UIFont.attributedLabel(Strings.about_this_speaker_screen_system())
        
        // get fresh data each this view loaded
        viewModel?.inputs.requestHardwareInfo()
        
        zip([ viewModel?.outputs.speakerName,
              viewModel?.outputs.speakerModel,
              viewModel?.outputs.speakerFw],
            [ speakerNameLabel,
              speakerModelLabel,
              speakerSystemFwLabel]
                as [UILabel]).forEach { output, label in
                output?
                    .asDriver()
                    .map { UIFont.attributedInfoText(" " + $0) }
                    .drive(label.rx.attributedText)
                    .disposed(by: disposeBag)
            }
        
        viewModel?.outputs.dataFetched
            .asObservable()
            .observeOn(MainScheduler.instance)
            .skipWhile { $0 == false }
            .subscribe(onNext: { [weak self] _ in
                    self?.removeLoadingView()
            })
            .disposed(by: disposeBag)

        viewModel?.outputs.updateAvailable
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] updateButtonVisible in
                self?.setButtonArea(enabled: updateButtonVisible)
            })
            .disposed(by: disposeBag)
        
        setupUpdateButton()
    
        progressView.isHidden = true
        statusLabel.isHidden = true
    
        progressView.progressTintColor = Color.progressBarTint
        progressView.trackTintColor = Color.otaProgressUnfilled
        progressView.heightAnchor.constraint(equalToConstant: 2.0)
        progressView.progress = 0.0
    
        statusLabel.attributedText = UIFont.otaProgressLabel(Strings.ota_screen_downloading_firmware())
        
        viewModel?.outputs.setupProgress
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] progressInfo in
                guard let strongSelf = self else { return }
                strongSelf.progressView.progress = Float(Float(progressInfo.entry.maxValue) / 100.0)
                strongSelf.statusLabel?.attributedText = UIFont.otaProgressLabel(progressInfo.entry.label) })
            .disposed(by: rx_disposeBag)
    
        viewModel?.outputs.blockNavigation
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] flag in
                guard let strongSelf = self else { return }
                var configuration = NavigationBarConfiguration()
                configuration.isBackButtonHidden = flag
                strongSelf.setupNavigationBar(title: UIFont.attributedTitle(strongSelf.viewModel?.inputs.hadrwareType.title ?? String()),
                                   configuration: configuration)
                if !flag {
                    strongSelf.hideUpdateControls()
                }
            })
            .disposed(by: disposeBag)
    }

    func hideUpdateControls() {
        [updateButton, progressView, statusLabel].forEach {
            $0?.isHidden = true
        }
    }

    @IBAction func updateButtonTouchedUpInside(_ sender: UIButton) {
        
        updateButton.isHidden = true
        progressView.isHidden = false
        statusLabel.isHidden = false
        
        viewModel?.inputs.startUpdate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let updateInProgress = viewModel?.outputs.updateInProgress, updateInProgress == true else {
            addLoadingView(animated: false)
            return
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let updateInProgress = viewModel?.outputs.updateInProgress, updateInProgress == false else {
            viewModel?.inputs.aboutViewDidAppear()
            return
        }
    }
}

private extension AboutThisSpeakerViewController {
    func setupUpdateButton() {
        updateButton.backgroundColor = Color.primaryButton
        updateButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.appwide_update_speaker_uc()), for: .normal)
    }
}

private extension HardwareType {
    var title: String {
        switch self {
        case .speaker:
            return Strings.device_settings_menu_item_about_speaker_uc()
        case .headset:
            return Strings.device_settings_menu_item_about_headphone_uc()
        }
    }
}

