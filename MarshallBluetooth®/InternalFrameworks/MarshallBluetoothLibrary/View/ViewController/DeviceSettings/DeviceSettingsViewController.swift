//
//  DeviceSettingsViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 08.05.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class DeviceSettingsViewController: UIViewController {
    
    @IBOutlet weak var deviceNameLabel: UILabel!
    @IBOutlet weak var deviceImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topMargin: NSLayoutConstraint!

    internal var viewModel: DeviceSettingsViewModelType?
    private var disposeBag = DisposeBag()
    let smallScreen = UIScreen.main.bounds.size.width < 375

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: UIFont.attributedTitle(Strings.device_settings_menu_title_settings_uc()))
        
        view.backgroundColor = UIColor.blackOne

        tableView.register(nib: Nib.DeviceSettingsCell)
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        if smallScreen {
            tableView.isScrollEnabled  = true
            topMargin.constant = 0
        }

        // get device name
        viewModel?.outputs.deviceName
            .asDriver()
            .map { UIFont.attributedTitle($0) }
            .drive(deviceNameLabel.rx.attributedText)
            .disposed(by: disposeBag)

        viewModel?.outputs.deviceImage
            .asDriver()
            .drive(deviceImageView.rx.image)
            .disposed(by: disposeBag)
        
        viewModel?.outputs.dataSource
            .asObservable()
            .subscribe(onNext: { [weak self] _ in
                self?.tableView.reloadData()
            }).disposed(by: disposeBag)
        
        viewModel?.outputs.dataSource
            .bind(to: tableView.rx.items(cellIdentifier: DeviceSettingsCell.defaultReusableId,
                                         cellType: DeviceSettingsCell.self))
            { (_, model, cell) in
                                
                let cellViewModel = DeviceSettingsCellViewModel()
                cell.configure(with: cellViewModel)
                
                cellViewModel.configure(enabled: model.enable)
                cellViewModel.configure(name: model.name)
                cellViewModel.configure(updateAvailable: model.updateNotification)
                
            }.disposed(by: disposeBag)
        
        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                self?.viewModel?.inputs.menuItemTapped(index: indexPath.row)
            }).disposed(by: disposeBag)
        
        // refresh device name each time view did loaded
        viewModel?.inputs.refreshName()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel?.inputs.viewWillAppear()
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel?.inputs.viewWillDisappear()
    }
    
    deinit {
        print(#function, String(describing: self))
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if isMovingFromParentViewController {
            viewModel?.outputs.closed.accept(true)
        }
    }
    
    @objc func onClose(_ sender: UIButton) {
        self.viewModel?.inputs.closeTapped()
    }

}
