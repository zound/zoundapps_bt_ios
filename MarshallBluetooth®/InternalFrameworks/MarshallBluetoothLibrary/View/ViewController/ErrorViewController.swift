//
//  ErrorScreenViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 12.07.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift

public class ErrorViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var actionsStackView: UIStackView!
    
    internal var viewModel: ErrorViewModelType?
    
    let disposeBag = DisposeBag()
    
    deinit {
        print(#function, String(describing: self))
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        actionsStackView.spacing = 17
        actionsStackView.translatesAutoresizingMaskIntoConstraints = false
        
        setupDescription()
        setupActions()
    }
}

private extension ErrorViewController {
    private func setupActions() {
        viewModel?.outputs.errorInfo.actions.forEach { action in
            let button = UIButton()
            button.translatesAutoresizingMaskIntoConstraints = false
            switch action.type {
            case .skip:
                button.setAttributedTitle(UIFont.attributedSkipButtonLabel(action.label), for: .normal)
                actionsStackView.addArrangedSubview(button)
            default:
                button.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(action.label), for: .normal)
                button.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
                button.backgroundColor = UIColor.darkBeige
                actionsStackView.addArrangedSubview(button)
            }

            button.rx.tap
                .subscribe(onNext: { _ in
                    action.action(action.type)
                })
                .disposed(by: disposeBag)
        }
    }
    
    private func setupDescription() {
        let title = viewModel?.outputs.errorInfo.title ?? ""
        let subtitle = viewModel?.outputs.errorInfo.subtitle ?? ""
        
        titleLabel.resizableAttributedFont(text: UIFont.attributedMediumTitle(title))
        subtitleLabel.attributedText = UIFont.attributedText(subtitle, color: Color.text)
    }
}


