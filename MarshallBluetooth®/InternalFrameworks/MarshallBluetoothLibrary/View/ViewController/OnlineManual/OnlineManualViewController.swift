//
//  OnlineManualViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 10/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import WebKit

class OnlineManualViewController: BaseViewController {

    private var webView: WKWebView!

    var request: URLRequest?

    override func loadView() {
        let webViewConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webViewConfiguration)
        webView.scrollView.delegate = self
        webView.addGestureRecognizer(DisableDoubleTapGestureRecognizer())
        view = webView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        var navigationBarConfiguration = NavigationBarConfiguration()
        navigationBarConfiguration.navigationBarTheme = .light
        setupNavigationBar(title: UIFont.attributedTitle(Strings.main_menu_item_online_manual_uc(), color: Color.textInversed), configuration: navigationBarConfiguration)

        if let request = request {
            webView.load(request)
        }
    }

}

extension OnlineManualViewController: UIScrollViewDelegate {

    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }

}

extension OnlineManualViewController: NavigationBarThemeChangeable {

    var navigationBarTheme: NavigationBarTheme {
        return .light
    }

}

class DisableDoubleTapGestureRecognizer: UITapGestureRecognizer, UIGestureRecognizerDelegate {

    init() {
        super.init(target: nil, action: nil)
        numberOfTapsRequired = 2
        delegate = self
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

}
