//
//  SafariViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 01/10/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import SafariServices

private struct StaticUrl {
    static let foss = "https://www.marshallheadphones.com/E40"
    static let shop = "http://www.marshallheadphones.com"
    static let website = "https://www.marshallheadphones.com"
    static let support = "https://www.marshallheadphones.com/marshall-bluetooth-app-contact"
}



class SafariViewController: SFSafariViewController {

    enum ContentType {
        case privacyPolicy
        case foss
        case shop
        case website
        case support
    }

    static func with(contentType: ContentType) -> SafariViewController {
        switch contentType {
        case .privacyPolicy:
            let url = PrivacyPolicyUrlBuilder()
                .add(Environment().language).url
            return SafariViewController(url)
        case .foss:
            return SafariViewController(URL(string: StaticUrl.foss)!)
        case .shop:
            return SafariViewController(URL(string: StaticUrl.shop)!)
        case .website:
            return SafariViewController(URL(string: StaticUrl.website)!)
        case .support:
            return SafariViewController(URL(string: StaticUrl.support)!)
        }
    }

    convenience init(_ url: URL) {
        if #available(iOS 11.0, *) {
            let configuration = Configuration()
            configuration.barCollapsingEnabled = false
            self.init(url: url, configuration: configuration)
        } else {
            self.init(url: url)
        }
        preferredBarTintColor = Color.safariBarTintColor
        preferredControlTintColor = Color.safariControlTintColor
        if #available(iOS 11.0, *) {
            dismissButtonStyle = .close
        }
    }

}
