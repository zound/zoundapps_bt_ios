//
//  RootViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 13/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import Cartography

public class RootViewController: UIViewController {

    internal var viewModel: RootViewModelType!
    
    func showChildViewController(_ viewController: UIViewController?, animated: Bool) {
        
        if let vc = self.displayedViewController {
            vc.willMove(toParentViewController: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParentViewController()
        }
        if let newVC = viewController {
            self.addChildViewController(newVC)
            newVC.view.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(newVC.view)
            constrain(newVC.view) { view in
                view.edges == view.superview!.edges
            }
            newVC.didMove(toParentViewController: self)
            self.displayedViewController = newVC
            self.setNeedsStatusBarAppearanceUpdate()
            
        }
    }
    
    private var displayedViewController: UIViewController?
}

