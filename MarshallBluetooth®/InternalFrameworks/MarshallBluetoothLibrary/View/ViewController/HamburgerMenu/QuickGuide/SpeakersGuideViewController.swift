//
//  SpeakersGuideViewController.swift
//  MarshallBluetooth®
//
//  Created by Paprota Przemyslaw on 26/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

class SpeakersGuideViewController: CommonGuideViewController {

    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var paragraph0: UILabel!
    @IBOutlet weak var header1: UILabel!
    @IBOutlet weak var paragraph1: UILabel!
    @IBOutlet weak var header2: UILabel!
    @IBOutlet weak var paragraph2: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle(title: Strings.device_settings_menu_item_couple_uc())
        paragraph0.attributedText = paragraphText(text: Strings.quick_guide_couple_subtitle())
        paragraph1.attributedText = paragraphText(text: Strings.quick_guide_couple_ambient_subtitle())
        paragraph2.attributedText = paragraphText(text: Strings.quick_guide_couple_stereo_subtitle())
        header1.attributedText = headerText(text: Strings.quick_guide_couple_ambient_title_uc())
        header2.attributedText = headerText(text: Strings.quick_guide_couple_stereo_title_uc())
    }

}
