//
//  BluetoothGuideViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Paprota Przemyslaw on 26/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

class BluetoothGuideViewController: CommonGuideViewController {

    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var point1: UILabel!
    @IBOutlet weak var point2: UILabel!
    @IBOutlet weak var point3: UILabel!
    @IBOutlet weak var point4: UILabel!
    @IBOutlet weak var pointText1: UILabel!
    @IBOutlet weak var pointText2: UILabel!
    @IBOutlet weak var pointText3: UILabel!
    @IBOutlet weak var pointText4: UILabel!
    @IBOutlet weak var image: UIImageView!

    @IBOutlet weak var imageTopLayoutConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle(title: Strings.main_menu_item_bluetooth_pairing_uc())
        point1.attributedText = pointText(no: 1)
        point2.attributedText = pointText(no: 2)
        point3.attributedText = pointText(no: 3)
        point4.attributedText = pointText(no: 4)
        pointText1.attributedText = pointText(label: Strings.quick_guide_bluetooth_pairing_part1())
        pointText2.attributedText = pointText(label: Strings.quick_guide_bluetooth_pairing_part2())
        pointText3.attributedText = pointText(label: Strings.quick_guide_bluetooth_pairing_part3())
        pointText4.attributedText = pointText(label: Strings.quick_guide_bluetooth_pairing_part4())

        imageTopLayoutConstraint.constant = -(navigationController?.navigationBar.frame.height ?? CGFloat()) * 2
    }
}
