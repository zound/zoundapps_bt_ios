//
//  CommonGuideViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Paprota Przemyslaw on 17/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

class CommonGuideViewController: UIViewController {
    
    var viewModel: CommonGuideViewModelType?

    private var disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.blackOne
    }
    
    public func pointText(label: String) -> NSAttributedString {
        return UIFont.attributedBodyText(label)
    }
    
    public func pointText(no: Int) -> NSAttributedString {
        return UIFont.attributedBodyText(String(no))
    }
    
    public func paragraphText(text: String) -> NSAttributedString {
        return UIFont.attributedBodyText(text)
    }
    
    public func headerText(text: String) -> NSAttributedString {
        return UIFont.attributedH4(text)
    }
    
    public func setTitle(title: String) {
        var navigationBarConfiguration = NavigationBarConfiguration()
        navigationBarConfiguration.isTranslucent = true
        setupNavigationBar(title: UIFont.attributedTitle(title), configuration: navigationBarConfiguration)
    }

    override func viewDidDisappear(_ animated: Bool) {
        if isMovingFromParentViewController {
            viewModel?.inputs.close()
        }
    }
    
}
