//
//  GenericGuideViewController.swift
//  MarshallBluetooth®
//
//  Created by Paprota Przemyslaw on 19/03/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit

extension GenericGuideViewControllerViewModel.Image {
    func image() -> UIImage {
        switch self {
        case .quick_guide_1:
            return UIImage.quick_guide_1
        case .quick_guide_2:
            return UIImage.quick_guide_2
        case .quick_guide_4:
            return UIImage.quick_guide_4
        case .quick_guide_5:
            return UIImage.quick_guide_5
        }
    }
}

private struct Constant {
    static let leftMarginForPointsLabel: CGFloat = 28
    static let topMarginForPointsLabel: CGFloat = 18
}
class GenericGuideViewController: UIViewController {
    @IBOutlet weak var listAteaTopMargin: NSLayoutConstraint!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var listArea: UIView!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageTopLayoutConstraint: NSLayoutConstraint!
    var viewModel: GenericGuideViewControllerViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let viewModel = viewModel else {
            return
        }
        view.backgroundColor = UIColor.blackOne
        setTitle(title: viewModel.title)
        mainImage.image = viewModel.image.image()
        if let text = viewModel.text {
            label.attributedText = attributedText(text)
            label.numberOfLines = 0
            label.textAlignment = .center
        } else {
            label.text = nil
            label.isHidden = true
            listAteaTopMargin.constant = 0
        }
        add(items: viewModel.list)
        imageTopLayoutConstraint.constant = 0
        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.never
        }
    }
    func add(items: [GenericGuideViewControllerViewModel.ListItem]) {
        var lastLabel : UILabel? = nil
        for item in items {
            lastLabel = add(item: item, lastLabel: lastLabel)
        }
        if let lastLabel = lastLabel  {
            NSLayoutConstraint.activate([ lastLabel.bottomAnchor.constraint(equalTo: listArea.bottomAnchor, constant: 0), ])
        }
    }
    func add(item: GenericGuideViewControllerViewModel.ListItem, lastLabel: UILabel?)-> UILabel {
        let point = UILabel()
        let text = UILabel()
        point.attributedText = attributedText(item.pointText)
        text.attributedText = attributedText(item.label)
        text.numberOfLines = 0
        [point, text].forEach {
            listArea.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        let top: NSLayoutConstraint
        if let lastLabel = lastLabel {
            top = point.topAnchor.constraint(equalTo: lastLabel.bottomAnchor, constant: Constant.topMarginForPointsLabel)
        } else {
            top = point.topAnchor.constraint(equalTo: listArea.topAnchor, constant: 0)
        }
        point.setContentHuggingPriority(UILayoutPriority.required, for: .horizontal)
        NSLayoutConstraint.activate([
            top,
            point.leadingAnchor.constraint(equalTo: listArea.leadingAnchor, constant: 0),
            text.topAnchor.constraint(equalTo: point.topAnchor, constant: 0),

            text.trailingAnchor.constraint(equalTo: listArea.trailingAnchor, constant: 0),
            text.leadingAnchor.constraint(equalTo: listArea.leadingAnchor, constant: Constant.leftMarginForPointsLabel),])
        return text
    }
    public func attributedText(_ text: String?) -> NSAttributedString {
        return UIFont.attributedBodyText(text ?? " ")
    }
    public func setTitle(title: String) {
        var navigationBarConfiguration = NavigationBarConfiguration()
        navigationBarConfiguration.isTranslucent = true
        setupNavigationBar(title: UIFont.attributedTitle(title), configuration: navigationBarConfiguration)
    }

}
