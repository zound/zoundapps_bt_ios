//
//  RenameViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 08.06.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RenameViewController: BaseViewController {
    
    @IBOutlet weak var speakerName: UILabel!
    @IBOutlet weak var speakerImage: UIImageView!
    @IBOutlet weak var renameTextField: UITextField!
    @IBOutlet weak var renameButton: UIButton!
    @IBOutlet weak var liveValidationLabel: UILabel!
    @IBOutlet weak var renameLine: UIImageView!
    @IBOutlet weak var dynamicConstraint: NSLayoutConstraint!
    @IBOutlet weak var warningImage: UIImageView!
    
    internal var viewModel: RenameViewModelType?
    private var disposeBag = DisposeBag()

    private var liveValidationResult: LiveValidationResult?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(resignFirstResponder(_:))))
        view.backgroundColor = UIColor.blackOne
        
        let renameTitle = viewModel?.outputs.isSpeaker ?? true
                        ? Strings.device_settings_menu_item_rename_uc()
                        : Strings.device_settings_menu_item_rename_headphone_uc()
  
        setupNavigationBar(title: UIFont.attributedTitle(renameTitle))
        createRenameInput()
        
        renameButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.appwide_done_uc()), for: .normal)
        
        setupKeyboard()
        setupDeviceName()
        setupDeviceImage()
        setupRenameControls()
        
        adjustOpacity()
    }
    
    @objc func resignFirstResponder(_ sender: UITapGestureRecognizer) {
        renameTextField.resignFirstResponder()
    }
    
    @objc internal func rename(_ textField: UITextField) {
        self.viewModel?.inputs.rename(with: textField.text ?? "")
    }
    
    @IBAction func onRename(_ sender: UIButton) {
        self.viewModel?.inputs.renameButtonTapped()
    }
    
    private func adjustOpacity() {
        // change opacity for active/inactive elements
        // Non-active fields are 50% and active fields are no opacity
        renameTextField.rx.controlEvent(.editingDidBegin)
            .asDriver()
            .drive(onNext: { [weak self] _ in
                UIView.animate(withDuration: TimeInterval.seconds(0.5), animations: {
                    self?.renameLine.alpha = 1.0
                    self?.renameTextField.alpha = 1.0 })
            })
            .disposed(by: disposeBag)
    }
    
    private func adjustConstraints(info: KeyboardInfo) {
        let animations: () -> Void = {
            switch info.notificationName {
            case .UIKeyboardWillShow:
                self.dynamicConstraint.constant = CGFloat()
                self.speakerName.alpha = CGFloat()
                if self.renameTextField.text?.isEmpty == true {
                    self.renameTextField.text = self.speakerName.text
                    self.viewModel?.inputs.rename(with: self.renameTextField.text ?? String())
                }
            case .UIKeyboardWillHide:
                self.dynamicConstraint.constant = 60
                if .some(self.renameTextField.text) == .some(self.speakerName.text) {
                    self.renameTextField.text = nil
                    self.viewModel?.inputs.rename(with: String())
                }
                if self.renameTextField.text?.isEmpty == true || .some(self.renameTextField.text) == .some(self.speakerName.text) {
                    self.speakerName.alpha = 1
                }
            default:
                break
            }
            self.view.layoutIfNeeded()
        }
        UIView.animate(withDuration: info.duration, delay: TimeInterval(), options: info.options, animations: animations)
    }
    
    private func adjustLiveValidationLabel(result: LiveValidationResult) -> NSAttributedString {
        liveValidationResult = result
        
        switch result {
        case .above:
            return UIFont.attributedRenameValidationText(Strings.rename_screen_name_too_long())
        case .exact:
            return UIFont.attributedRenameValidationText(Strings.rename_screen_reached_limit())
        case .under(let left):
            return UIFont.attributedRenameValidationText(String(format: Strings.rename_screen_characters_left(), left))
        }
    }
}

// MARK: - UITextFieldDelegate
extension RenameViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // range <= 0 means typing, otherwise (range.length > 0) is deleting, which is always allowed
        // assume we're typing - if not - then it's OK to pass
        guard range.length <= 0 else {
            return true
        }
        
        guard let result = self.liveValidationResult else {
            return true
        }
        
        return result != .above && result != .exact
    }
}

// MARK: - Creation and set-up of views/controls
private extension RenameViewController {
    private func createRenameInput() {
        applyFormInputStyle(renameTextField)
        renameTextField.text = nil
        renameTextField.keyboardType = .default
        renameTextField.keyboardAppearance = .dark
        renameTextField.delegate = self
        
        renameTextField.addTarget(self,
                                  action: #selector(rename(_:)),
                                  for: [.editingDidEndOnExit, .editingChanged])
    }
    
    private func setupKeyboard() {
        Keyboard.info
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] info in
                self?.adjustConstraints(info: info)
            })
            .disposed(by: rx_disposeBag)
    }
    
    private func setupDeviceName() {
        // get device name
        viewModel?.outputs.displayDeviceName
            .asDriver()
            .map { UIFont.attributedTitle($0) }
            .drive(speakerName.rx.attributedText)
            .disposed(by: disposeBag)
    }
    
    private func setupDeviceImage() {
        viewModel?.outputs.deviceImage
            .asDriver()
            .drive(speakerImage.rx.image)
            .disposed(by: disposeBag)
    }
    
    private func setupRenameControls() {
        guard let viewModel = viewModel else { return }
        
        // set the "Rename" button visibility
        viewModel.outputs.renameButtonVisible
            .asDriver()
            .drive(renameButton.rx.visible)
            .disposed(by: disposeBag)
        
        // set the wrong device's name visibility (typed by user)
        viewModel.outputs.wrongNameIndicatorVisible
            .asDriver()
            .drive(warningImage.rx.visible)
            .disposed(by: disposeBag)
        
        viewModel.outputs.liveValidationResult
            .asObservable()
            .observeOn(MainScheduler.instance)
            .map { [weak self] result in
                return self?.adjustLiveValidationLabel(result: result) }
            .bind(to: liveValidationLabel.rx.attributedText)
            .disposed(by: disposeBag)
        
        // initial configuration for live validation label (first display)
        liveValidationLabel.attributedText = adjustLiveValidationLabel(result: .under(left: viewModel.outputs.nameLimit ))
    }
}
