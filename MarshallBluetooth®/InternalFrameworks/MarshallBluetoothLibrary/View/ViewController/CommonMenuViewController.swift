//
//  CommonMenuViewController
//  MarshallBluetooth®
//
//  Created by Paprota Przemyslaw on 09/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import Foundation
import RxSwift
import RxCocoa

class CommonMenuViewController: UIViewController {
    
    @IBOutlet weak var bottomLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    internal var viewModel: CommonMenuViewModelType?
    
    private var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.blackOne
        tableView.register(nib: Nib.CommonMenuCell)
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        
        viewModel?.outputs.title
            .asDriver()
            .drive(onNext: {[weak self] title in
                self?.setupNavigationBar(title: UIFont.attributedTitle(title))
            })
            .disposed(by: disposeBag)
        
        viewModel?.outputs.dataSource
            .asObservable()
            .subscribe(onNext: { [weak self] _ in
                self?.tableView.reloadData()
            }).disposed(by: disposeBag)
        
        viewModel?.outputs.dataSource
            .bind(to: tableView.rx.items(cellIdentifier: CommonMenuCell.defaultReusableId,
                                         cellType: CommonMenuCell.self))
            { (_, model, cell) in
                let cellViewModel = CommonMenuCellViewModel()
                cell.configureWith(viewModel: cellViewModel)
                cellViewModel.configure(name: model.label)
            }.disposed(by: disposeBag)
        
        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                self?.viewModel?.inputs.menuItemTapped(index: indexPath.row)
            }).disposed(by: disposeBag)
        
        viewModel?.outputs.bottomLabel
            .asDriver()
            .drive(bottomLabel.rx.text)
            .disposed(by: disposeBag)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if isMovingFromParentViewController {
            viewModel?.inputs.close()
        }
    }
    
}

extension CommonMenuViewController: NavigationBarThemeChangeable {}
