//
//  SavingDataViewController.swift
//  MarshallBluetooth®
//
//  Created by Dolewski Bartosz A (Ext) on 20.03.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

class SaveDataViewController: UIViewController {
    
    @IBOutlet weak var savingIndicator: UIImageView!
    @IBOutlet weak var savingLabel: UILabel!
    
    internal var viewModel: SaveDataViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Color.background
        
        savingLabel.attributedText = UIFont.attributedCircularProgressLabel(Strings.spinner_screen_savings())
        savingLabel.adjustsFontSizeToFitWidth = true
        
        // since there is no real saving data ongoing, keep the animation for (arbitrary value) 4 seconds
        let mockedProgressBarTime = 4.0
        Timer.scheduledTimer(withTimeInterval: mockedProgressBarTime, repeats: false) { (timer) in
            self.viewModel.viewCompleted.accept(true)
        }
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        savingIndicator.rotate()
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        savingIndicator.stopRotation()
    }
    
}
