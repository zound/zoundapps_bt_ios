//
//  InAppTermsAndConditionsViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 23/09/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import WebKit

class InAppTermsAndConditionsViewController: BaseViewController, HtmlStringProviding {

    private var webView: WKWebView!

    override func loadView() {
        let webViewConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webViewConfiguration)
        webView.scrollView.delegate = self
        webView.isOpaque = false
        webView.addGestureRecognizer(DisableDoubleTapGestureRecognizer())
        view = webView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: UIFont.attributedTitle(Strings.ios_terms_and_conditions_screen_title_uc()))
        loadHtml()
    }

}

extension InAppTermsAndConditionsViewController: UIScrollViewDelegate {

    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }

}

private extension InAppTermsAndConditionsViewController {
    func loadHtml() {
        webView.loadHTMLString(htmlString(for: .termsAndConditions), baseURL: URL(fileURLWithPath: Bundle.framework.bundlePath))
    }
}
