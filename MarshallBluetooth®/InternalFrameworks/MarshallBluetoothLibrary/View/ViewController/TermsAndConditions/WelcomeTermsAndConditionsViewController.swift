//
//  WelcomeTermsAndConditionsViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 20/09/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import WebKit

class WelcomeTermsAndConditionsViewController: BaseViewController, HtmlStringProviding {

    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var webViewContainerView: UIView!

    private var webView: WKWebView!

    var viewModel: TermsAndConditionsViewModelType?

    @IBAction func buttonTouchedUpInside(_ sender: UIButton) {
        viewModel?.inputs.backButtonTapped()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupExitButton()
        setupHeaderLabel()
        setupWebView()
        loadHtml()
    }

}

private extension WelcomeTermsAndConditionsViewController {

    func setupExitButton() {
        exitButton.setImage(UIImage.failIcon, for: .normal)
        exitButton.tintColor = UIColor.white
    }

    func setupHeaderLabel() {
        headerLabel.attributedText = UIFont.attributedH1(Strings.ios_terms_and_conditions_screen_title_uc())
    }

    func setupWebView() {
        let webViewConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: webViewContainerView.frame, configuration: webViewConfiguration)
        webView.scrollView.delegate = self
        webView.isOpaque = false
        webView.addGestureRecognizer(DisableDoubleTapGestureRecognizer())
        webViewContainerView.backgroundColor = UIColor.blackOne
        webViewContainerView.addSubview(webView)
        webView.setConstrainsOnBordersToBeEqual(with: webViewContainerView)
    }

    func loadHtml() {
        webView.loadHTMLString(htmlString(for: .termsAndConditions), baseURL: URL(fileURLWithPath: Bundle.framework.bundlePath))
    }

}

extension WelcomeTermsAndConditionsViewController: UIScrollViewDelegate {

    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }

}
