//
//  ContactScreenViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Paprota Przemyslaw on 13/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

class ContactScreenViewController: UIViewController {
    
    @IBOutlet weak var info: UILabel!
    @IBOutlet weak var supportButton: UIButton!
    @IBOutlet weak var websiteButton: UIButton!
    
    var model: ContactScreenViewModelType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.blackOne
        
        setupNavigationBar(title: titleText)
        info.attributedText = infoText
        
        supportButton.setAttributedTitle(supportButtonText, for: .normal)
        websiteButton.setAttributedTitle(websiteButtonText, for: .normal)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if isMovingFromParentViewController {
            model?.inputs.close()
        }
    }
    
    @IBAction func goToWebsite(_ sender: Any) {
        present(SafariViewController.with(contentType: .website), animated: true)
    }

    @IBAction func contactSupport(_ sender: Any) {
        present(SafariViewController.with(contentType: .support), animated: true)
    }
}

private extension ContactScreenViewController {
    private var titleText: NSAttributedString {
        return UIFont.attributedTitle(Strings.main_menu_item_contact_uc())
    }
    
    private var infoText: NSAttributedString {
        return UIFont.attributedInfoText(Strings.contact_screen_subtitle())
    }
    
    private var supportButtonText: NSAttributedString {
        return UIFont.attributedPrimaryButtonLabel(Strings.contact_screen_contact_support_uc())
    }
    
    private var websiteButtonText: NSAttributedString {
        return UIFont.attributedPrimaryButtonLabel(Strings.appwide_go_to_website_uc())
    }
}
