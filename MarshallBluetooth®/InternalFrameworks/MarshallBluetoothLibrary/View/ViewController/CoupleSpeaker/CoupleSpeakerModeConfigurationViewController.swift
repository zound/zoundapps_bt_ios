//
//  CoupleSpeakerModeConfigurationViewController.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 11/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Config {
    static let numberOfRowsInSection = 1
    static let heightForHeaderInSection = CGFloat(3)
}

private extension CoupleSpeakerStereoChannel {
    var oppositeChannel: CoupleSpeakerStereoChannel {
        switch self {
        case .left:
            return .right
        case .right:
            return .left
        }
    }
}

private extension CoupleSpeakerModeConfiguration {
    var tableViewCellIdentifier: String {
        switch self {
        case .standard:
            return CoupleSpeakerListTableViewCell.defaultReusableId
        case .stereo:
            return CoupleSpeakerModeConfigurationTableViewCell.defaultReusableId
        }
    }
    var nib: Nib {
        switch self {
        case .standard:
            return Nib.CoupleSpeakerListTableViewCell
        case .stereo:
            return Nib.CoupleSpeakerModeConfigurationTableViewCell
        }
    }
    var attributedText: NSAttributedString {
        switch self {
        case .standard:
            return UIFont.attributedText(Strings.couple_screen_select_mode_ambient(), color: .white)
        case .stereo:
            return UIFont.attributedText(Strings.couple_screen_title_stereo(), color: .white)
        }
    }
}

class CoupleSpeakerModeConfigurationViewController: BaseViewController {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var speakersTableView: UITableView!
    @IBOutlet weak var actionButton: UIButton!

    var viewModel: CoupleSpeakerModeConfigurationViewModelType?

    var coupledSpeakers: [CoupleSpeakerListItem] = []

    private var coupledSpeakersModeConfiguration: CoupleSpeakerModeConfiguration {
        guard let coupledSpeaker = coupledSpeakers.first, let modeConfiguration = coupledSpeaker.modeConfiguration else {
            return .standard
        }
        return modeConfiguration
    }

    private var masterCellTableViewCell: CoupleSpeakerModeConfigurationTableViewCell?
    private var slaveCellTableViewCell: CoupleSpeakerModeConfigurationTableViewCell?

    @IBAction func actionButtonTouchedUpInside(_ sender: UIButton) {
        viewModel?.inputs.done()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: UIFont.attributedTitle(Strings.device_settings_menu_item_couple_uc()))
        setupDescriptionLabel()
        setupTableView()
        setupActionButton()
    }

}

private extension CoupleSpeakerModeConfigurationViewController {

    func setupDescriptionLabel() {
        descriptionLabel.attributedText = coupledSpeakersModeConfiguration.attributedText
    }

    func setupTableView() {
        speakersTableView.register(nib: coupledSpeakersModeConfiguration.nib)
        speakersTableView.delegate = self
        speakersTableView.dataSource = self
        speakersTableView.separatorInset = UIEdgeInsets(top: CGFloat(), left: CGFloat(), bottom: CGFloat(), right: .greatestFiniteMagnitude)
        speakersTableView.showsHorizontalScrollIndicator = false
        speakersTableView.showsVerticalScrollIndicator = false
        speakersTableView.isScrollEnabled = false
        speakersTableView.allowsSelection = false
    }

    private func setupActionButton() {
        actionButton.backgroundColor = Color.primaryButton
        actionButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.appwide_done_uc()), for: .normal)
    }

}

extension CoupleSpeakerModeConfigurationViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return coupledSpeakers.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Config.numberOfRowsInSection
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Config.heightForHeaderInSection
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView().with(backgroundColor: .clear)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch coupledSpeakersModeConfiguration {
        case .standard:
            return configuredCoupleSpeakerListTableViewCell(tableView, indexPath: indexPath)
        case .stereo:
            return configuredCoupleSpeakerModeConfigurationTableViewCell(tableView, indexPath: indexPath)
        }
    }

}

private extension CoupleSpeakerModeConfigurationViewController {

    func configuredCoupleSpeakerListTableViewCell(_ tableView: UITableView, indexPath: IndexPath) -> CoupleSpeakerListTableViewCell {
        guard let modeConfiguration = coupledSpeakers[indexPath.section].modeConfiguration else {
            return CoupleSpeakerListTableViewCell()
        }
        let identifier = modeConfiguration.tableViewCellIdentifier
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? CoupleSpeakerListTableViewCell else {
            return CoupleSpeakerListTableViewCell()
        }
        cell.nameLabel.attributedText = UIFont.attributedH3(coupledSpeakers[indexPath.section].name)
        cell.speakerImageView.image = coupledSpeakers[indexPath.section].image
        if let modeConfigurationText = coupledSpeakers[indexPath.section].modeConfiguration?.text {
            cell.modeLabel.attributedText = UIFont.attributedSmallText(modeConfigurationText)
        }
        return cell
    }

    func configuredCoupleSpeakerModeConfigurationTableViewCell(_ tableView: UITableView, indexPath: IndexPath) -> CoupleSpeakerModeConfigurationTableViewCell {
        guard let modeConfiguration = coupledSpeakers[indexPath.section].modeConfiguration else {
            return CoupleSpeakerModeConfigurationTableViewCell()
        }
        let identifier = modeConfiguration.tableViewCellIdentifier
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? CoupleSpeakerModeConfigurationTableViewCell else {
            return CoupleSpeakerModeConfigurationTableViewCell()
        }
        cell.nameLabel.attributedText = UIFont.attributedH3(coupledSpeakers[indexPath.section].name)
        cell.speakerImageView.image = coupledSpeakers[indexPath.section].image
        cell.delegate = self
        if indexPath.section == Int() {
            (cell.actor, cell.channel) = (.master, .left)
            masterCellTableViewCell = cell
        } else {
            (cell.actor, cell.channel) = (.slave, .right)
            slaveCellTableViewCell = cell
        }
        return cell
    }

}

extension CoupleSpeakerModeConfigurationViewController: CoupleSpeakerModeConfigurationTableViewCellDelegate {
    func didSelect(channel: CoupleSpeakerStereoChannel, from actor: CoupleSpeakerActor) {
        switch actor {
        case .master:
            slaveCellTableViewCell?.channel = channel.oppositeChannel
            viewModel?.inputs.setMasterAs(channel)
        case .slave:
            masterCellTableViewCell?.channel = channel.oppositeChannel
            viewModel?.inputs.setMasterAs(channel.oppositeChannel)
        }
    }
}
