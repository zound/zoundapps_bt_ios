//
//  CoupleSpeakerHeadsUpViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 17/09/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

class CoupleSpeakerHeadsUpViewController: BaseViewController {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!

    var viewModel: CoupleSpeakerHeadsUpViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        var navigationBarConfiguration = NavigationBarConfiguration()
        navigationBarConfiguration.isBackButtonHidden = true
        setupNavigationBar(title: NSAttributedString(), configuration: navigationBarConfiguration)
        setupHeaderLabel()
        setupDescriptionLabel()
        setupActionButton()
    }

    @IBAction func actionButtonTouchedUpInside(_ sender: Any) {
        viewModel?.inputs.done()
    }

}

private extension CoupleSpeakerHeadsUpViewController {
    private func setupActionButton() {
        actionButton.backgroundColor = Color.primaryButton
        actionButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.couple_screen_heads_up_button()), for: .normal)
    }
    private func setupHeaderLabel() {
        headerLabel.attributedText = UIFont.attributedH2(Strings.couple_screen_heads_up_title_uc())
    }
    private func setupDescriptionLabel() {
        let descriptionComponents = Strings.couple_screen_heads_up_subtitle().components(separatedBy: stringComponentSeparator)
        guard let firstComponentString = descriptionComponents[optional: 0],
              let secondComponentString = descriptionComponents[optional: 1],
              let thirdComponentString = descriptionComponents[optional: 2],
              let pairNames = viewModel?.outputs.pairNames else {
            descriptionLabel.attributedText = NSAttributedString()
            return
        }
        let desctiptionLabelText = [
            UIFont.attributedBodyText(firstComponentString),
            UIFont.attributedBodyText(pairNames.masterName),
            UIFont.attributedBodyText(secondComponentString),
            UIFont.attributedBodyText(pairNames.slaveName),
            UIFont.attributedBodyText(thirdComponentString)
        ].reduce(NSAttributedString(), +)
        descriptionLabel.attributedText = desctiptionLabelText
    }
}

private func + (lhs: NSAttributedString, rhs: NSAttributedString) -> NSAttributedString {
    let result = NSMutableAttributedString()
    result.append(lhs)
    result.append(rhs)
    return result
}
