//
//  CoupleSpeakerListViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 08/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

private struct Config {
    static let numberOfRowsInSection = 1
    static let heightForHeaderInSection = CGFloat(3)
    struct SpeakerFadeAnimation {
        static let duration: TimeInterval = .seconds(0.3)
        static let options: UIViewAnimationOptions = .transitionCrossDissolve
    }
}

enum CoupleSpeakerListInitialState {
    case unselected, coupled
}

private enum ButtonType {
    case next, unpair
}

private enum CoupleSpeakerListState {
    case unselected
    case selected(atIndex: Int)
    case coupled
}

extension CoupleSpeakerListState: Equatable {
    public static func ==(lhs: CoupleSpeakerListState, rhs: CoupleSpeakerListState) -> Bool {
        switch (lhs, rhs) {
        case (.unselected, .unselected), (.coupled, .coupled):
            return true
        case (.selected(let lhsIndex), .selected(let rhsIndex)):
            return lhsIndex == rhsIndex
        default:
            return false
        }
    }
}

private extension CoupleSpeakerListInitialState {
    var coupleSpeakerListState: CoupleSpeakerListState {
        switch self {
        case .unselected:
            return .unselected
        case .coupled:
            return .coupled
        }
    }
}

private extension CoupleSpeakerListState {
    var buttonType: ButtonType {
        switch self {
        case .unselected, .selected:
            return .next
        case .coupled:
            return .unpair
        }
    }
    var buttonAreaEnabled: Bool {
        switch self {
        case .unselected:
            return false
        case .selected, .coupled:
            return true
        }
    }
    var initialButtonAreaState: ButtonAreaState {
        switch self {
        case .unselected:
            return .disabled
        case .selected, .coupled:
            return .enabled
        }
    }
    var tableViewScrollingEnabled: Bool {
        switch self {
        case .unselected, .selected:
            return true
        case .coupled:
            return false
        }
    }
}

struct CoupleSpeakerListItem {
    let name: String
    let image: UIImage
    let modeConfiguration: CoupleSpeakerModeConfiguration?
}

enum CoupleSpeakerModeConfiguration {
    case standard
    case stereo(CoupleSpeakerStereoChannel)
}

extension CoupleSpeakerModeConfiguration {
    var text: String {
        switch self {
        case .stereo(.left):
            return Strings.speaker_status_left_uc()
        case .stereo(.right):
            return Strings.speaker_status_right_uc()
        case .standard:
            return Strings.speaker_status_left_and_right()
        }
    }
}

class CoupleSpeakerListViewController: BaseViewController, ButtonAreaAnimating {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var speakersTableView: UITableView!
    @IBOutlet weak var actionButton: UIButton!
    
    @IBOutlet weak var buttonAreaHeightConstraint: NSLayoutConstraint!
    
    var buttonAreaState: ButtonAreaState = CoupleSpeakerListState.unselected.initialButtonAreaState
    lazy var buttonAreaHeightAnimator: UIViewPropertyAnimator = {
        return getDefautButtonAreaHeightAnimator()
    }()
    lazy var buttonAreaBaseView: UIView = {
        return view
    }()
    lazy var buttonAreaHeight: NSLayoutConstraint = {
        return buttonAreaHeightConstraint
    }()
    
    private var initialState: CoupleSpeakerListInitialState? {
        didSet {
            guard let initialState = initialState else { return }
            state = initialState.coupleSpeakerListState
            updateTableViewBehaviour()
            buttonAreaState = initialState.coupleSpeakerListState.initialButtonAreaState
            setupActionButton(type: state.buttonType)
            setupDescriptionLabel(type: state.buttonType)
            
            setButtonArea(enabled: state.buttonAreaEnabled)
            removeLoadingView(animated: true)
            speakersTableView.reloadData()
        }
    }
    
    private var state: CoupleSpeakerListState = .unselected {
        didSet {
            setButtonArea(enabled: state.buttonAreaEnabled, animated: true)
        }
    }
    
    var viewModel: CoupleSpeakerListViewModelType?
    
    private var deviceList: [CoupleSpeakerListItem] = []
    
    private let disposeBag = DisposeBag()
    
    @IBAction func actionButtonTouchedUpInside(_ sender: UIButton) {
        switch state {
        case .selected(atIndex: let selectedItemIndex):
            viewModel?.inputs.next(selectedItemIndex: selectedItemIndex)
        case .coupled:
            viewModel?.inputs.unpair()
        default:
            break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: UIFont.attributedTitle(Strings.device_settings_menu_item_couple_uc()))
        setupDescriptionLabel(type: .next)
        setupTableView()
        setupActionButton(type: .next)
        setButtonArea(enabled: false)
        
        let outputCoupleSpeakerListInit = viewModel?.outputs.outputCoupleSpeakerListInit
            .asObservable()
            .observeOn(MainScheduler.instance)
        outputCoupleSpeakerListInit?.subscribe({ [weak self] outputCoupleSpeakerListInitElement in
            guard let outputCoupleSpeakerListInit = outputCoupleSpeakerListInitElement.element else { return }
            self?.deviceList = outputCoupleSpeakerListInit.deviceList
            self?.initialState = outputCoupleSpeakerListInit.initialState
        }).disposed(by: disposeBag)
        
        let outputCoupleSpeakerListItem = viewModel?.outputs.outputCoupleSpeakerListItem
            .asObservable()
            .observeOn(MainScheduler.instance)
        outputCoupleSpeakerListItem?.subscribe({ [weak self] outputCoupleSpeakerListItemElement in
            guard let outputCoupleSpeakerListItem = outputCoupleSpeakerListItemElement.element, let strongSelf = self else { return }
            strongSelf.deviceList.append(outputCoupleSpeakerListItem)
            UIView.transition(with: strongSelf.speakersTableView,
                              duration: Config.SpeakerFadeAnimation.duration,
                              options: Config.SpeakerFadeAnimation.options,
                              animations: { strongSelf.speakersTableView.reloadData() })
        }).disposed(by: disposeBag)
        
        viewModel?.inputs.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if initialState == nil {
            addLoadingView(animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParentViewController {
            viewModel?.inputs.viewWillDisappearMovingFromParent()
        }
    }
    
}

private extension CoupleSpeakerListViewController {
    
    func setupDescriptionLabel(type: ButtonType) {
        switch type {
        case .next:
            descriptionLabel.attributedText = UIFont.attributedText(Strings.couple_screen_select_speaker_subtitle(), color: .white)
        case .unpair:
            descriptionLabel.attributedText = UIFont.attributedText(Strings.ios_couple_screen_decouple_subtitle(), color: .white)
        }
    }
    
    func setupTableView() {
        speakersTableView.register(nib: Nib.CoupleSpeakerListTableViewCell)
        speakersTableView.delegate = self
        speakersTableView.dataSource = self
        speakersTableView.separatorInset = UIEdgeInsets(top: CGFloat(), left: CGFloat(), bottom: CGFloat(), right: .greatestFiniteMagnitude)
        speakersTableView.showsHorizontalScrollIndicator = false
        speakersTableView.showsVerticalScrollIndicator = false
    }
    
    func updateTableViewBehaviour() {
        speakersTableView.isScrollEnabled = state.tableViewScrollingEnabled
    }
    
    private func setupActionButton(type: ButtonType) {
        actionButton.backgroundColor = Color.primaryButton
        switch type {
        case .next:
            actionButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.appwide_next_uc()), for: .normal)
        case .unpair:
            actionButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.ios_couple_screen_decouple_uc()), for: .normal)
        }
    }
}

extension CoupleSpeakerListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return deviceList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Config.numberOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Config.heightForHeaderInSection
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView().with(backgroundColor: .clear)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = CoupleSpeakerListTableViewCell.defaultReusableId
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? CoupleSpeakerListTableViewCell else {
            return UITableViewCell()
        }
        
        cell.speakerImageView.image = deviceList[indexPath.section].image
        cell.nameLabel.attributedText = UIFont.attributedH3(deviceList[indexPath.section].name)
        if let modeConfigurationText = deviceList[indexPath.section].modeConfiguration?.text {
            cell.modeLabel.attributedText = UIFont.attributedSmallText(modeConfigurationText)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let coupleSpeakerListTableViewCell = cell as? CoupleSpeakerListTableViewCell, (indexPath.section == Int() || state == .coupled) {
            coupleSpeakerListTableViewCell.permanentlySelected = true
            coupleSpeakerListTableViewCell.setSelected(true, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard state != .coupled else { return }
        guard indexPath.section > Int() else {
            state = .unselected
            return
        }
        state = .selected(atIndex: indexPath.section)
    }
    
}
