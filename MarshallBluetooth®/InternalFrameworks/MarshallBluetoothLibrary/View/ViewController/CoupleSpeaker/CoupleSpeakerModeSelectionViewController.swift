//
//  CoupleSpeakerModeSelectionViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 11/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Config {
    static let numberOfRowsInSection = 1
    static let heightForHeaderInSection = CGFloat(3)
    struct SubDescriptionFadeAnimation {
        static let duration: TimeInterval = .seconds(0.15)
        static let options: UIViewAnimationOptions = [.curveEaseInOut, .transitionCrossDissolve]
    }
}

enum CoupleSpeakerMode {
    case standard, stereo

    var defaultCoupleSpeakerModeConfiguration: CoupleSpeakerModeConfiguration {
        switch self {
        case .standard:
            return .standard
        case .stereo:
            return .stereo(.left)
        }
    }
    var defaultOppositeCoupleSpeakerModeConfiguration: CoupleSpeakerModeConfiguration {
        switch self {
        case .standard:
            return .standard
        case .stereo:
            return .stereo(.right)
        }
    }
}

private extension CoupleSpeakerMode {
    var text: String {
        switch self {
        case .standard:
            return Strings.couple_screen_select_mode_ambient()
        case .stereo:
            return Strings.couple_screen_select_mode_stereo()
        }
    }
    var subDescriptionText: String {
        switch self {
        case .standard:
            return Strings.couple_screen_select_mode_ambient_desc()
        case .stereo:
            return Strings.couple_screen_select_mode_stereo_desc()
        }
    }
}

struct CoupleSpeakerModeItem {
    let mode: CoupleSpeakerMode
    let enabled: Bool
}

class CoupleSpeakerModeSelectionViewController: BaseViewController, ButtonAreaAnimating {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var subDescriptionLabel: UILabel!
    @IBOutlet weak var modeTableView: UITableView!
    @IBOutlet weak var actionButton: UIButton!

    @IBOutlet weak var buttonAreaHeightConstraint: NSLayoutConstraint!

    internal var buttonAreaState: ButtonAreaState = .disabled
    lazy var buttonAreaHeightAnimator: UIViewPropertyAnimator = {
        return getDefautButtonAreaHeightAnimator()
    }()
    lazy var buttonAreaBaseView: UIView = {
        return view
    }()
    lazy var buttonAreaHeight: NSLayoutConstraint = {
        return buttonAreaHeightConstraint
    }()

    var selectedMode: CoupleSpeakerMode? {
        didSet {
            updateSubDescriptionLabel(for: selectedMode!)
        }
    }
    var coupleSpeakersModes: [CoupleSpeakerModeItem] = [
        CoupleSpeakerModeItem(mode: .standard, enabled: true),
        CoupleSpeakerModeItem(mode: .stereo, enabled: false)
    ]

    var viewModel: CoupleSpeakerModeSelectionViewModelType?
    
    @IBAction func actionButtonTouchedUpInside(_ sender: UIButton) {
        guard let selectedMode = selectedMode else { return }
        viewModel?.inputs.next(mode: selectedMode)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: UIFont.attributedTitle(Strings.device_settings_menu_item_couple_uc()))
        setupDescriptionLabel()
        setupSubDescriptionLabel()
        setupTableView()
        setupActionButton()
        setButtonArea(enabled: false)
    }

}

private extension CoupleSpeakerModeSelectionViewController {

    func setupDescriptionLabel() {
        descriptionLabel.attributedText = UIFont.attributedText(Strings.couple_screen_select_mode_subtitle(), color: .white)
    }

    func setupSubDescriptionLabel() {
        subDescriptionLabel.attributedText = UIFont.attributedSmallText(Strings.couple_screen_select_mode_ambient_and_stereo_desc())
    }

    func updateSubDescriptionLabel(for selectedMode: CoupleSpeakerMode) {
        guard subDescriptionLabel.attributedText?.string != selectedMode.subDescriptionText else {
            return
        }
        let fadeOutAnimations = {
            self.subDescriptionLabel.attributedText = NSAttributedString()
        }
        let fadeInAnimations = {
            self.subDescriptionLabel.attributedText = UIFont.attributedSmallText(selectedMode.subDescriptionText)
        }
        UIView.transition(with: subDescriptionLabel,
                          duration: Config.SubDescriptionFadeAnimation.duration,
                          options: Config.SubDescriptionFadeAnimation.options,
                          animations: fadeOutAnimations) { _ in
            UIView.transition(with: self.subDescriptionLabel,
                              duration: Config.SubDescriptionFadeAnimation.duration,
                              options: Config.SubDescriptionFadeAnimation.options,
                              animations: fadeInAnimations)
        }
    }

    func setupTableView() {
        modeTableView.register(nib: Nib.CoupleSpeakerModeSelectionTableViewCell)
        modeTableView.delegate = self
        modeTableView.dataSource = self
        modeTableView.separatorInset = UIEdgeInsets(top: CGFloat(), left: CGFloat(), bottom: CGFloat(), right: .greatestFiniteMagnitude)
        modeTableView.showsHorizontalScrollIndicator = false
        modeTableView.showsVerticalScrollIndicator = false
        modeTableView.isScrollEnabled = false
    }

    private func setupActionButton() {
        actionButton.backgroundColor = Color.primaryButton
        actionButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.appwide_next_uc()), for: .normal)
    }

}

extension CoupleSpeakerModeSelectionViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return coupleSpeakersModes.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Config.numberOfRowsInSection
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Config.heightForHeaderInSection
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView().with(backgroundColor: .clear)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = CoupleSpeakerModeSelectionTableViewCell.defaultReusableId
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? CoupleSpeakerModeSelectionTableViewCell else {
            return UITableViewCell()
        }
        cell.modeLabel.attributedText = UIFont.attributedMenuLabel(coupleSpeakersModes[indexPath.section].mode.text)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedMode = coupleSpeakersModes[indexPath.section].mode
        setButtonArea(enabled: true, animated: true)
    }

}
