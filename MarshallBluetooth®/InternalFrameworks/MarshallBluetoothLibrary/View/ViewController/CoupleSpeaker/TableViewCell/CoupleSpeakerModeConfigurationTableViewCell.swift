//
//  CoupleSpeakerModeConfigurationTableViewCell.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 11/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

private struct Config {
    static let segmentedControlBorderWidth = CGFloat(1)
    static let segmentWidth = CGFloat(80)
    struct Segment {
        static let left = 0
        static let right = 1
    }
}

enum CoupleSpeakerActor {
    case master, slave
}

private extension CoupleSpeakerStereoChannel {
    init?(segmentedControlIndex: Int) {
        switch segmentedControlIndex {
        case Config.Segment.left:
            self = .left
        case Config.Segment.right:
            self = .right
        default:
            return nil
        }
    }
    var segmentedControlIndex: Int {
        switch self {
        case .left:
            return Config.Segment.left
        case .right:
            return Config.Segment.right
        }
    }
    var attributedText: NSAttributedString {
        switch self {
        case .left:
            return UIFont.attributedSmallButton(Strings.speaker_status_left_uc())
        case .right:
            return UIFont.attributedSmallButton(Strings.speaker_status_right_uc())
        }
    }
}

protocol CoupleSpeakerModeConfigurationTableViewCellDelegate: class {
    func didSelect(channel: CoupleSpeakerStereoChannel, from actor: CoupleSpeakerActor)
}

class CoupleSpeakerModeConfigurationTableViewCell: UITableViewCell {

    weak var delegate: CoupleSpeakerModeConfigurationTableViewCellDelegate?

    var actor: CoupleSpeakerActor = .master
    var channel: CoupleSpeakerStereoChannel = .left {
        didSet {
            channelSelectionSegmentedControl.selectedSegmentIndex = channel.segmentedControlIndex
        }
    }

    @IBOutlet weak var speakerImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet private weak var channelSelectionSegmentedControl: UISegmentedControl!

    @IBAction func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        guard let channel = CoupleSpeakerStereoChannel(segmentedControlIndex: sender.selectedSegmentIndex) else {
            return
        }
        delegate?.didSelect(channel: channel, from: actor)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        setupSegmentedControl()
    }

}

private extension CoupleSpeakerModeConfigurationTableViewCell {
    func setupSegmentedControl() {
        channelSelectionSegmentedControl.tintColor = Color.segmentedControl
        channelSelectionSegmentedControl.setTitle(CoupleSpeakerStereoChannel.left.attributedText.string, forSegmentAt: Config.Segment.left)
        channelSelectionSegmentedControl.setTitle(CoupleSpeakerStereoChannel.right.attributedText.string, forSegmentAt: Config.Segment.right)
        let attributes = CoupleSpeakerStereoChannel.left.attributedText.attributes(at: Int(), effectiveRange: nil)
        channelSelectionSegmentedControl.setTitleTextAttributes(attributes, for: .selected)
        channelSelectionSegmentedControl.setTitleTextAttributes(attributes.filter {
            $0.key != NSAttributedStringKey.foregroundColor
        }, for: .normal)
        [Config.Segment.left, Config.Segment.right].forEach {
            channelSelectionSegmentedControl.setWidth(Config.segmentWidth, forSegmentAt: $0)
        }
    }
}
