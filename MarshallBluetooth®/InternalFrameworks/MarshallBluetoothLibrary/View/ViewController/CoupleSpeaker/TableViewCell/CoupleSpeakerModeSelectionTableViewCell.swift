//
//  CoupleSpeakerModeSelectionTableViewCell.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 14/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

class CoupleSpeakerModeSelectionTableViewCell: MonoSelectableTableViewCell {

    @IBOutlet weak var modeLabel: UILabel!

}
