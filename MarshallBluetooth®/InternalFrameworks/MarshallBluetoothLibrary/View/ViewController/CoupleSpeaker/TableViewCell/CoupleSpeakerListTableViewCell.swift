//
//  CoupleSpeakerListTableViewCell.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 11/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

class CoupleSpeakerListTableViewCell: MonoSelectableTableViewCell {

    @IBOutlet weak var speakerImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var modeLabel: UILabel!
}


