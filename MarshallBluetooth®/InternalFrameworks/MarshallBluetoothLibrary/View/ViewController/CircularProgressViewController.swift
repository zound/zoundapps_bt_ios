import Foundation
import UIKit

public final class CircularProgressViewController: UIViewController {
    @IBOutlet weak var progressIndicatorImage: UIImageView!
    @IBOutlet weak var progressLabel: UILabel!
    
    internal var viewModel: CircularProgressViewModel?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        progressLabel.attributedText = UIFont.attributedCircularProgressLabel(Strings.spinner_screen_searching_speakers_headphones())
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        progressIndicatorImage.rotate()
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel?.inputs.viewDidAppear()
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        progressIndicatorImage.stopRotation()
    }

}
