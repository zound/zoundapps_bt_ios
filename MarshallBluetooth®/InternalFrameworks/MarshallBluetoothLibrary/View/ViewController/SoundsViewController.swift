//
//  SoundsViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 31.07.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

class SoundsViewController: BaseViewController {
    
    @IBOutlet weak var powerLabel: UILabel!
    @IBOutlet weak var mediaLabel: UILabel!
    
    @IBOutlet weak var powerSwitch: UISwitch!
    @IBOutlet weak var mediaSwitch: UISwitch!
    
    var viewModel: SoundsViewModelType?
    
    private let disposeBag = DisposeBag()
    
    deinit {
        print(#function, String(describing: self))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: UIFont.attributedTitle(Strings.device_settings_menu_item_sounds_uc()))
        
        setupLabels()
        setupSwitches()
        setupDataFetched()
        viewModel?.inputs.requestSoundsControlStatus()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addLoadingView(animated: true)
    }
    
    @IBAction func powerSwitched(_ sender: UISwitch) {
        viewModel?.inputs.turnPower(on: sender.isOn)
    }
    
    @IBAction func mediaSwitched(_ sender: UISwitch) {
        viewModel?.inputs.turnMedia(on: sender.isOn)
    }
}

private extension SoundsViewController {
    func setupDataFetched() {
        viewModel?.outputs.dataFetched
            .asObservable()
            .observeOn(MainScheduler.instance)
            .skipWhile { $0 == false }
            .subscribe(onNext: { [weak self] _ in
                self?.removeLoadingView()
            })
            .disposed(by: disposeBag)
    }
    
    func setupLabels() {
        powerLabel.attributedText = UIFont.attributedText(Strings.sounds_screen_power(), color: .white)
        mediaLabel.attributedText = UIFont.attributedText(Strings.sounds_screen_media_control(), color: .white)

        viewModel?.outputs.isMediaSwitchEnable
            .asDriver(onErrorJustReturn: false)
            .drive(mediaLabel.rx.visible)
            .disposed(by: disposeBag)
    }
    
    func setupSwitches() {
        powerSwitch.onTintColor = Color.switcher
        powerSwitch.tintColor = Color.switcher
        
        mediaSwitch.onTintColor = Color.switcher
        mediaSwitch.tintColor = Color.switcher

        viewModel?.outputs.isMediaSwitchEnable
            .asDriver(onErrorJustReturn: false)
            .drive(mediaSwitch.rx.visible)
            .disposed(by: disposeBag)

        viewModel?.outputs.power
            .asDriver(onErrorJustReturn: false)
            .drive(powerSwitch.rx.isOn)
            .disposed(by: disposeBag)
        
        viewModel?.outputs.media
            .asDriver(onErrorJustReturn: false)
            .drive(mediaSwitch.rx.isOn)
            .disposed(by: disposeBag)
    }
}
