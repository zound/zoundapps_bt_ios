//
//  BluetoothPlayerSourceViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 18.09.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

private struct Config {
    static let blankTitle = ""
    static let blankAlbum = ""
    static let blankArtist = ""
    static let blankAlbumArtist = ""
    static let separatorAlbumArtist = " - "
    
    struct SliderAnimation {
        static let duration: TimeInterval = 0.4
        static let options: UIViewAnimationOptions = .curveEaseInOut
    }
}

class BluetoothPlayerSourceViewController: UIViewController {
    @IBOutlet weak var activateButton: UIButton!
    @IBOutlet weak var sourceIconImageView: UIImageView!
    @IBOutlet weak var speakerImageView: UIImageView!
    
    @IBOutlet weak var audioSourceView: UIView!
    @IBOutlet weak var playerView: UIView!
    
    @IBOutlet weak var volumeSlider: UISlider!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var playPauseButton: UIButton!
    
    @IBOutlet weak var trackLabel: UILabel!
    @IBOutlet weak var albumArtistLabel: UILabel!
    
    @IBOutlet weak var audioScrubberSlider: UISlider!
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var durationTimeLabel: UILabel!
    
    @IBOutlet weak var artworkImageView: UIImageView!
    
    var audioSourceViewModel: AudioSourceViewModelType?
    var bluetoothPlayerViewModel: BluetoothPlayerViewModelType?
    
    typealias Dependencies = AudioSourceViewModelDependency & BluetoothPlayerViewModelDependency
    private let throttleTime = TimeInterval.seconds(0.5)
    
    private var disposeBag = DisposeBag()
    
    var volume: Int = Int() {
        didSet {
            guard oldValue != volume else { return }
            audioSourceViewModel?.inputs.change(volume: volume)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = Color.background
        playerView.isHidden = true
        
        volumeSlider.minimumValue = Float(audioSourceViewModel?.outputs.platformTraits.volumeMin ?? 0)
        volumeSlider.maximumValue = Float(audioSourceViewModel?.outputs.platformTraits.volumeMax ?? 1)
        volumeSlider.addTarget(self, action: #selector(onTouchUpInside(_:)), for: .touchUpInside)

        setupAudioSourceAndPlayerViews()
        setupActivateButton()
        setupVolumeSlider()
        setupPlaybackControls()
        // no setup for audio scrubber since there is no design for it
        // setupScrubber()
        setupTrackInfo()
    }
    
    @IBAction func onVolumeChanged(_ sender: UISlider) {
        volume = Int(sender.value)
    }

    @objc func onTouchUpInside(_ sender: UISlider!) {
        bluetoothPlayerViewModel?.inputs.logVolumeChangeAnalyticsEvnet(with: volume)
    }
    
    deinit {
        print(#function, String(describing: self))
    }
}

extension BluetoothPlayerSourceViewController {
    private func setupAudioSourceAndPlayerViews() {
        let activation = audioSourceViewModel?.outputs.activateVisible
            .asDriver(onErrorJustReturn: false)
            .asSharedSequence()
        
        activation?
            .asDriver()
            .drive(activateButton.rx.visible)
            .disposed(by: disposeBag)
        
        activation?
            .asDriver()
            .drive(audioSourceView.rx.visible)
            .disposed(by: disposeBag)
        
        activation?
            .asDriver()
            .drive(playerView.rx.isHidden)
            .disposed(by: disposeBag)
        
        audioSourceViewModel?.outputs.speakerImage
            .asDriver()
            .drive(speakerImageView.rx.image)
            .disposed(by: disposeBag)
        
        audioSourceViewModel?.outputs.iconImage
            .asDriver()
            .drive(sourceIconImageView.rx.image)
            .disposed(by: disposeBag)
        
        audioSourceViewModel?.outputs.volume
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] volume in
                self?.updateSlider(to: volume, animated: false)
            }).disposed(by: rx_disposeBag)
        
        audioSourceViewModel?.outputs.monitoredVolume
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] volume in
                self?.updateSlider(to: volume, animated: true)
            }).disposed(by: rx_disposeBag)
    }
}

// MARK: - Setup sub-views for AudioSource part
extension BluetoothPlayerSourceViewController {
    private func setupActivateButton() {
        activateButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.appwide_activate_uc()), for: .normal)
        
        activateButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.audioSourceViewModel?.inputs.activate() })
            .disposed(by: disposeBag)
    }
}

// MARK: - Setup sub-views for Player part
extension BluetoothPlayerSourceViewController {
    private func setupVolumeSlider() {
        volumeSlider.minimumTrackTintColor = Color.volumeSliderMin
        volumeSlider.maximumTrackTintColor = Color.volumeSliderMax
        volumeSlider.setThumbImage(UIImage.sliderThumb, for: .normal)
    }
    
    private func setupScrubber() {
        audioScrubberSlider.minimumTrackTintColor = Color.volumeSliderMin
        audioScrubberSlider.maximumTrackTintColor = Color.volumeSliderMax
        audioScrubberSlider.setThumbImage(UIImage.scrubberThumb, for: .normal)
        
        let scrubberVisible = bluetoothPlayerViewModel?.outputs.scrubberVisible
            .asDriver(onErrorJustReturn: false)
            .asSharedSequence()
        
        scrubberVisible?
            .drive(audioScrubberSlider.rx.visible)
            .disposed(by: disposeBag)
        
        scrubberVisible?
            .drive(durationTimeLabel.rx.visible)
            .disposed(by: disposeBag)
        
        scrubberVisible?
            .drive(currentTimeLabel.rx.visible)
            .disposed(by: disposeBag)
    }
}

// MARK: - Playback Control
extension BluetoothPlayerSourceViewController {
    
    private func setupPlaybackControls() {
        playPauseButton.rx.tap
            .throttle(throttleTime, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.bluetoothPlayerViewModel?.inputs.playPause() })
            .disposed(by: disposeBag)
        
        nextButton.rx.tap
            .throttle(throttleTime, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.bluetoothPlayerViewModel?.inputs.next() })
            .disposed(by: disposeBag)
        
        previousButton.rx.tap
            .throttle(throttleTime, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.bluetoothPlayerViewModel?.inputs.previous() })
            .disposed(by: disposeBag)
        
        bluetoothPlayerViewModel?.outputs.isPlaying
            .asObservable()
            .subscribeOn(MainScheduler.instance)
            .map { isPlaying in
                return isPlaying ? UIImage.pauseButton : UIImage.playButton }
            .subscribe(playPauseButton.rx.image())
            .disposed(by: disposeBag)
    }
}

// MARK: - Track Info
extension BluetoothPlayerSourceViewController {
    private func setupTrackInfo() {
        guard let viewModel = bluetoothPlayerViewModel else { return }
        
        viewModel.outputs.title
            .asDriver(onErrorJustReturn: Config.blankTitle)
            .map { UIFont.attributedTitle($0) }
            .drive(trackLabel.rx.attributedText)
            .disposed(by: disposeBag)
        
        let album = viewModel.outputs.album
            .asObservable()
            .catchErrorJustReturn(Config.blankAlbum)
            .map { UIFont.attributedAlbumLabel($0) }
        
        let artist = viewModel.outputs.artist
            .asObservable()
            .catchErrorJustReturn(Config.blankArtist)
            .map { UIFont.attributedArtistLabel( $0.isEmpty ? $0 : Config.separatorAlbumArtist + $0) }
        
        Observable.combineLatest(album, artist)
            .asObservable()
            .map { album, artist in
                let final = NSMutableAttributedString(attributedString: album)
                final.append(artist)
                return final
            }.observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] text in
                self.albumArtistLabel.attributedText = text
            }).disposed(by: disposeBag)
    }
}

// MARK: - Volume bar setup
extension BluetoothPlayerSourceViewController {
    private func updateSlider(to volume: Int, animated: Bool) {
        volumeSlider.isUserInteractionEnabled = false
        
        let animations: () -> Void = { [weak self] in
            self?.volumeSlider.setValue(Float(volume), animated: true)
        }
        
        let completionBlock: (Bool) -> Void = { [weak self] _ in
            self?.volumeSlider.isUserInteractionEnabled = true
        }
        
        UIView.animate(withDuration: animated ? Config.SliderAnimation.duration : TimeInterval(),
                       delay: TimeInterval(),
                       options: Config.SliderAnimation.options,
                       animations: animations,
                       completion: completionBlock)
    }
}
