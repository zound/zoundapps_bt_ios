//
//  AnalogAudioSourceViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 18.09.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

private struct Config {
    struct SliderAnimation {
        static let duration: TimeInterval = 0.4
        static let options: UIViewAnimationOptions = .curveEaseInOut
    }
}

class AnalogAudioSourceViewController: UIViewController {
    
    @IBOutlet weak var activateButton: UIButton!
    @IBOutlet weak var speakerImageView: UIImageView!
    @IBOutlet weak var sourceIconView: UIView!
    @IBOutlet weak var sourceIconImageView: UIImageView!
    @IBOutlet weak var volumeSlider: UISlider!
    
    internal var viewModel: AudioSourceViewModelType?
    private var disposeBag = DisposeBag()
    
    var volume: Int = Int() {
        didSet {
            guard oldValue != volume else { return }
            viewModel?.inputs.change(volume: volume)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = Color.background
        
        volumeSlider.minimumValue = Float(viewModel?.outputs.platformTraits.volumeMin ?? 0)
        volumeSlider.maximumValue = Float(viewModel?.outputs.platformTraits.volumeMax ?? 1)
        volumeSlider.addTarget(self, action: #selector(onTouchUpInside(_:)), for: .touchUpInside)
        
        setupVolumeBar()
        setupActivateButton()
        bindModel()
    }
    
    @IBAction func onVolumeChange(_ sender: UISlider) {
        volume = Int(sender.value)
    }

    @objc func onTouchUpInside(_ sender: UISlider!) {
        viewModel?.inputs.logVolumeChangeAnalyticsEvent(with: volume)
    }
    
    deinit {
        print(#function, String(describing: self))
    }
}

// MARK: - Setup visuals
extension AnalogAudioSourceViewController {
    private func setupVolumeBar() {
        volumeSlider.isHidden = true
        volumeSlider.minimumTrackTintColor = Color.volumeSliderMin
        volumeSlider.maximumTrackTintColor = Color.volumeSliderMax
        volumeSlider.setThumbImage(UIImage.sliderThumb, for: .normal)
    }
    
    private func setupActivateButton() {
        activateButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.appwide_activate_uc()), for: .normal)
    }
}

extension AnalogAudioSourceViewController {
    private func bindModel() {
        viewModel?.outputs.activateVisible
            .asDriver(onErrorJustReturn: true)
            .drive(activateButton.rx.visible)
            .disposed(by: disposeBag)
        
        viewModel?.outputs.speakerImage
            .asDriver()
            .drive(speakerImageView.rx.image)
            .disposed(by: disposeBag)
        
        viewModel?.outputs.sourceIconVisible
            .asDriver(onErrorJustReturn: true)
            .drive(sourceIconView.rx.visible)
            .disposed(by: disposeBag)
        
        viewModel?.outputs.iconImage
            .asDriver()
            .drive(sourceIconImageView.rx.image)
            .disposed(by: disposeBag)
        
        viewModel?.outputs.volumeBarVisible
            .asDriver(onErrorJustReturn: false)
            .drive(volumeSlider.rx.visible)
            .disposed(by: disposeBag)
        
        viewModel?.outputs.volume
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] volume in
                self?.updateSlider(to: volume, animated: false)
            }).disposed(by: rx_disposeBag)
        
        viewModel?.outputs.monitoredVolume
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] volume in
                self?.updateSlider(to: volume, animated: true)
            }).disposed(by: rx_disposeBag)
        
        activateButton.rx.tap
            .subscribe(onNext: { [unowned self] _ in
                self.viewModel?.inputs.activate()
            })
            .disposed(by: disposeBag)
    }
}

extension AnalogAudioSourceViewController {
    private func updateSlider(to volume: Int, animated: Bool) {
        volumeSlider.isUserInteractionEnabled = false
        
        let animations: () -> Void = { [weak self] in
            self?.volumeSlider.setValue(Float(volume), animated: true)
        }
        
        let completionBlock: (Bool) -> Void = { [weak self] _ in
            self?.volumeSlider.isUserInteractionEnabled = true
        }
        
        UIView.animate(withDuration: animated ? Config.SliderAnimation.duration : TimeInterval(),
                       delay: TimeInterval(),
                       options: Config.SliderAnimation.options,
                       animations: animations,
                       completion: completionBlock)
    }
}
