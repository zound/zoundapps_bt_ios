//
//  AudioSourcesFlowLayout.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 25.06.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

open class AudioSourcesFlowLayout: UICollectionViewFlowLayout {
    
    @IBInspectable open var sideItemScale: CGFloat = 0.6
    @IBInspectable open var sideItemAlpha: CGFloat = 0.6
    @IBInspectable open var sideItemShift: CGFloat = 0.0
    
    open var spacingSize = CGFloat(40.0)
    
    override open func prepare() {
        super.prepare()
        
        setupCollectionView()
        updateLayout()
    }
    
    override open func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    override open func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let superAttributes = super.layoutAttributesForElements(in: rect),
            let attributes = NSArray(array: superAttributes, copyItems: true) as? [UICollectionViewLayoutAttributes]
            else { return nil }
        
        return attributes.map { self.transformLayoutAttributes($0) }
    }
    
    override open func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        guard let collectionView = collectionView ,
            !collectionView.isPagingEnabled,
            let layoutAttributes = layoutAttributesForElements(in: collectionView.bounds)
            else {
                return super.targetContentOffset(forProposedContentOffset: proposedContentOffset)
        }
        
        let midSide = collectionView.bounds.size.width.half
        let proposedContentOffsetCenterOrigin = proposedContentOffset.x + midSide
        
        var targetContentOffset: CGPoint
        let closest = layoutAttributes.sorted {
            abs($0.center.x - proposedContentOffsetCenterOrigin) < abs($1.center.x - proposedContentOffsetCenterOrigin)
            }.first ?? UICollectionViewLayoutAttributes()
        
        targetContentOffset = CGPoint(x: floor(closest.center.x - midSide), y: proposedContentOffset.y)
        
        return targetContentOffset
    }
}

private extension AudioSourcesFlowLayout {
    private func setupCollectionView() {
        guard let collectionView = collectionView else { return }
        collectionView.decelerationRate = UIScrollViewDecelerationRateFast
    }
    
    private func updateLayout() {
        guard let collectionView = collectionView else { return }
        
        let collectionSize = collectionView.bounds.size
        let itemWidth = itemSize.width
        let itemHeight = itemSize.height
        
        let xInset = (collectionSize.width - itemWidth).half
        let yInset = (collectionSize.height - itemHeight).half
        sectionInset = UIEdgeInsetsMake(yInset, xInset, yInset, xInset)
        
        let side = itemWidth
        let scaledItemOffset = (side - side * sideItemScale).half
        minimumLineSpacing = spacingSize - scaledItemOffset
    }
    
    private func transformLayoutAttributes(_ attributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        guard let collectionView = collectionView else { return attributes }
        
        let collectionCenter = collectionView.bounds.size.width.half
        let offset = collectionView.contentOffset.x
        let normalizedCenter = attributes.center.x - offset
        
        let maxDistance = itemSize.width + minimumLineSpacing
        let distance = min(abs(collectionCenter - normalizedCenter), maxDistance)
        let ratio = (maxDistance - distance)/maxDistance
        
        let alpha = ratio * (1 - sideItemAlpha) + sideItemAlpha
        let scale = ratio * (1 - sideItemScale) + sideItemScale
        let shift = (1 - ratio) * sideItemShift
        
        attributes.alpha = alpha
        attributes.transform3D = CATransform3DScale(CATransform3DIdentity, scale, scale, 1)
        attributes.zIndex = Int(alpha * 10)
        attributes.center.y = attributes.center.y + shift
        
        return attributes
    }
}


