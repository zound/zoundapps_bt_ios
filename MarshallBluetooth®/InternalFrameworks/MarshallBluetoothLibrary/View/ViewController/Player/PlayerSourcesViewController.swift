//
//  PlayerSourcesViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 25.06.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import GUMA

private struct Config {
    struct ScrollViewAnimation {
        static let duration: TimeInterval = 0.33
    }
}

protocol AudioSourceViewModelDependency {
    var audioSourceViewModel: AudioSourceViewModelType { get }
}

protocol BluetoothPlayerViewModelDependency {
    var bluetoothPlayerViewModel: BluetoothPlayerViewModelType { get }
}

struct BluetoothPlayerDependency: AudioSourceViewModelDependency, BluetoothPlayerViewModelDependency  {
    let audioSourceViewModel: AudioSourceViewModelType
    let bluetoothPlayerViewModel: BluetoothPlayerViewModelType
}

class PlayerSourcesViewController: UIViewController {
    
    @IBOutlet weak var sourceControlCollectionView: UICollectionView!
    @IBOutlet weak var sourcePlayersScrollView: UIScrollView!
    
    @IBOutlet weak var playerSourceLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    
    internal var viewModel: PlayerSourcesViewModelType?
    private var sourceViews: [UIView] = []
    private var sourceStackView = UIStackView(frame: .zero)
    private var disposeBag = DisposeBag()
    
    deinit {
        print(#function, String(describing: self))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.blackOne
        
        sourceControlCollectionView.register(nib: Nib.AudioSourceCell)
        sourcePlayersScrollView.isPagingEnabled = true
        sourcePlayersScrollView.isScrollEnabled = false
        
        setupCloseButton()
        setupPlayerSourceLabel()
        setupTopCollectionView()
        setupLayouts()
        setupScrollViewPages()
        
        viewModel?.inputs.viewLoaded()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel?.inputs.viewWillAppear()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel?.inputs.viewWillDisappear()
    }
}

// MARK: - Setting up general layout
private extension PlayerSourcesViewController {
    func setupCloseButton() {
        closeButton.rx.tap
            .subscribe(onNext: { [unowned self] _ in
                self.viewModel?.inputs.close()
            }).disposed(by: disposeBag)
    }
    
    func setupPlayerSourceLabel() {
        viewModel?.outputs.sourceName
            .asObservable()
            .observeOn(MainScheduler.instance)
            .map { UIFont.attributedTitle($0) }
            .bind(to: playerSourceLabel.rx.attributedText)
            .disposed(by: disposeBag)
    }
}

// MARK: - Setting up Top part of Player
private extension PlayerSourcesViewController {
    func setupLayouts() {
        let topLayout = self.sourceControlCollectionView.collectionViewLayout as! AudioSourcesFlowLayout
        topLayout.spacingSize = 30.0
        topLayout.sideItemAlpha = 0.72
        topLayout.sideItemScale = 1.0
        topLayout.scrollDirection = .horizontal
    }
    
    func setupTopCollectionView() {
        viewModel?.outputs.dataSource
            .bind(to: sourceControlCollectionView.rx.items(cellIdentifier: AudioSourceCell.defaultReusableId,
                                                           cellType: AudioSourceCell.self))
            { [unowned self] (_, model, cell) in
                
                // create view model for cell
                guard let device = self.viewModel?.outputs.device() else { return }
                let cellViewModel = AudioSourceCellViewModel(device: device,
                                                             type: model.type,
                                                             name: model.name)
                cell.configureWith(value: cellViewModel)
                cell.imageView.image = model.image
            }.disposed(by: disposeBag)
        
        viewModel?.outputs.sourceIndex
            .map { IndexPath(item: $0, section: 0) }
            .subscribeNext(weak: self, PlayerSourcesViewController.scrollSource)
            .disposed(by: disposeBag)
        
        
        sourceControlCollectionView.rx.itemSelected
            .distinctUntilChanged(==)
            .subscribe(onNext: { [unowned self] indexPath in
                self.viewModel?.inputs.select(sourceIdx: indexPath.row)
            }).disposed(by: disposeBag)
        
        sourceControlCollectionView.rx.didEndDecelerating
            .asObservable()
            .subscribe(onNext: { [unowned self] _ in
                let x = self.sourceControlCollectionView.contentOffset.x + (self.sourceControlCollectionView.frame.width.half)
                let y = self.sourceControlCollectionView.contentOffset.y + (self.sourceControlCollectionView.frame.height.half)
                let center = CGPoint(x: x, y: y)
                
                guard let indexPath = self.sourceControlCollectionView.indexPathForItem(at: center) else {
                    return
                }
                self.viewModel?.inputs.select(sourceIdx: indexPath.row)
            }).disposed(by: disposeBag)
    }
}

// MARK: - Bottom part of Player
private extension PlayerSourcesViewController {
    func setupScrollViewPages() {
        guard let viewModel = viewModel else { return }
        let device = viewModel.outputs.device()
        let supportedAudioSources = viewModel.outputs.dataSource.value.compactMap {$0.type}
        
        supportedAudioSources.forEach { audioSource in
            let childVC = audioSource == .bluetooth ? setupBluetoothPlayerSourceViewController(with: device) : setupAnalogSourceViewController(with: device, for: audioSource)
            sourceViews.append(childVC.view)
        }
        
        sourceStackView.translatesAutoresizingMaskIntoConstraints = false
        sourceStackView.distribution = .fillEqually
        sourceStackView.spacing = 0.0
        
        sourcePlayersScrollView.addSubview(sourceStackView)
        NSLayoutConstraint.activate([
            sourceStackView.topAnchor.constraint(equalTo: sourcePlayersScrollView.topAnchor),
            sourceStackView.bottomAnchor.constraint(equalTo: sourcePlayersScrollView.bottomAnchor),
            sourceStackView.leadingAnchor.constraint(equalTo: sourcePlayersScrollView.leadingAnchor),
            sourceStackView.trailingAnchor.constraint(equalTo: sourcePlayersScrollView.trailingAnchor)
            ])
        
        sourceViews.forEach { view in
            view.translatesAutoresizingMaskIntoConstraints = false
            sourceStackView.addArrangedSubview(view)
            view.widthAnchor.constraint(equalTo: sourcePlayersScrollView.widthAnchor).isActive = true
            view.heightAnchor.constraint(equalTo: sourcePlayersScrollView.heightAnchor).isActive = true
        }
    }
    
    func setupBluetoothPlayerSourceViewController(with device: DeviceType) -> BluetoothPlayerSourceViewController {
        let childVC = UIViewController.instantiate(BluetoothPlayerSourceViewController.self)
        let bluetoothPlayerSourceVM = BluetoothPlayerViewModel(device: device, dependencies: viewModel!.outputs.analyticsServiceDependency)
        let audioSourceVM = AudioSourceViewModel(device: device, audioSource: .bluetooth, dependencies: viewModel!.outputs.analyticsServiceDependency)
        
        childVC.bluetoothPlayerViewModel = bluetoothPlayerSourceVM
        childVC.audioSourceViewModel = audioSourceVM
        
        addChildViewController(childVC)
        childVC.didMove(toParentViewController: self)
        
        return childVC
    }
    
    func setupAnalogSourceViewController(with device: DeviceType, for type: AudioSource) -> AnalogAudioSourceViewController {
        let childVC = UIViewController.instantiate(AnalogAudioSourceViewController.self)
        let audioSourceVM = AudioSourceViewModel(device: device, audioSource: type, dependencies: viewModel!.outputs.analyticsServiceDependency)
        
        childVC.viewModel = audioSourceVM
        
        addChildViewController(childVC)
        childVC.didMove(toParentViewController: self)
        
        return childVC
    }
}

// MARK: - Configure scrolling
private extension PlayerSourcesViewController {
    
    func scrollSource(_ toItemAtIndexPath: IndexPath) {
        guard let selectedSourceItem = sourceControlCollectionView.cellForItem(at: toItemAtIndexPath) as? AudioSourceCell else {
            return
        }
        guard let attributes = sourceControlCollectionView.layoutAttributesForItem(at: toItemAtIndexPath) else { return }
        selectedSourceItem.apply(attributes)
        
        sourceControlCollectionView.scrollToItem(at: toItemAtIndexPath, at: .centeredHorizontally, animated: true)
        scrollBottomPart(to: toItemAtIndexPath.item)
    }
    
    func scrollBottomPart(to page: Int) {
        let pageWidth = sourcePlayersScrollView.bounds.width
        UIView.animate(withDuration: Config.ScrollViewAnimation.duration) { [weak self] in
            self?.sourcePlayersScrollView.contentOffset.x = pageWidth * CGFloat(page)
        }
    }
}
