//
//  ForgetSpeakerViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 17/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift
import GUMA

class ForgetSpeakerViewController: BaseViewController {

    @IBOutlet weak var speakerImageView: UIImageView!
    @IBOutlet weak var speakerNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var forgetSpeakerButton: UIButton!

    var viewModel: ForgetSpeakerViewModelType?

    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: UIFont.attributedTitle(viewModel?.inputs.hardwareType.title ?? String()))

        setupDescriptionLabel()
        setupForgetSpeakerButton()

        viewModel?.outputs.speakerData
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] speakerData in
                self?.setupSpeakerImageAndName(with: speakerData)
            }).disposed(by: disposeBag)

        viewModel?.inputs.viewDidLoad()
    }

    @IBAction func forgetSpeakerTouchedUpInside(_ sender: UIButton) {
        viewModel?.inputs.didForgetSpeaker()
    }

}

private extension ForgetSpeakerViewController {

    func setupSpeakerImageAndName(with speakerData: SpeakerData) {
        speakerImageView.image = speakerData.image.mediumImage
        speakerNameLabel.attributedText = UIFont.attributedTitle(speakerData.name)
    }

    func setupDescriptionLabel() {
        descriptionLabel.attributedText = UIFont.attributedText(viewModel?.inputs.hardwareType.description ?? String(), color: .white)
    }


    private func setupForgetSpeakerButton() {
        forgetSpeakerButton.backgroundColor = Color.primaryButton
        forgetSpeakerButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(viewModel?.inputs.hardwareType.title ?? String()), for: .normal)
    }
}

private extension HardwareType {
    var title: String {
        switch self {
        case .speaker:
            return Strings.device_settings_menu_item_forget_uc()
        case .headset:
            return Strings.device_settings_menu_item_forget_headphone_uc()
        }
    }
    var description: String {
        switch self {
        case .speaker:
            return Strings.forget_screen_subtitle()
        case .headset:
            return Strings.forget_headphone_screen_subtitle()
        }
    }
}
