//
//  ActiveNoiceCancellingViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 03/12/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift
import GUMA

private struct Config {
    struct SliderAnimation {
        static let duration: TimeInterval = 0.4
        static let options: UIViewAnimationOptions = .curveEaseInOut
    }
    struct Slider {
        static let minimumValue = 1
        static let maximumValue = 10
        static let enabledAlpha: CGFloat = 1
        static let disabledAlpha: CGFloat = 0.3
    }
    struct Level {
        static let postfix = "0%"
    }
}

class ActiveNoiceCancellingViewController: BaseViewController {

    @IBOutlet weak var underlineSegmentedControlContainerView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var levelSlider: UISlider!

    private var underlineSegmentedControl: UnderlineSegmentedControl?

    var viewModel: ActiveNoiceCancellingViewModel?
    private let disposeBag = DisposeBag()

    private var currentActiveNoiceCancellingSettings: ActiveNoiseCancellingSettings?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: UIFont.attributedTitle(Strings.device_settings_menu_item_anc_uc()))
        configureUnderlineSegmentedControl()
        configureVerticalSlider()
        let outputActiveNoiceCancellingSettingsInit = viewModel?.outputs.outputActiveNoiceCancellingSettingsInit.asObservable().observeOn(MainScheduler.instance)
        outputActiveNoiceCancellingSettingsInit?.subscribe({ [weak self] outputActiveNoiceCancellingSettingsInit in
            guard let strongSelf = self, let outputActiveNoiceCancellingSettings = outputActiveNoiceCancellingSettingsInit.element else { return }
            strongSelf.currentActiveNoiceCancellingSettings = outputActiveNoiceCancellingSettings
            strongSelf.underlineSegmentedControl?.selectedSegmentIndex = outputActiveNoiceCancellingSettings.mode.index
            strongSelf.updateRelatedUIComponents(initial: true)
            strongSelf.removeLoadingView(animated: false)
        }).disposed(by: disposeBag)
        let outputActiveNoiceCancellingSettings = viewModel?.outputs.outputActiveNoiceCancellingSettings.asObservable().observeOn(MainScheduler.instance)
        outputActiveNoiceCancellingSettings?.subscribe({ [weak self] outputActiveNoiceCancellingSettingsElement in
            guard let strongSelf = self, let outputActiveNoiceCancellingSettings = outputActiveNoiceCancellingSettingsElement.element else { return }
            strongSelf.currentActiveNoiceCancellingSettings = outputActiveNoiceCancellingSettings
            strongSelf.underlineSegmentedControl?.selectedSegmentIndex = outputActiveNoiceCancellingSettings.mode.index
            strongSelf.updateRelatedUIComponents()
        }).disposed(by: disposeBag)
        let outputActiveNoiceCancellingMode = viewModel?.outputs.outputActiveNoiceCancellingMode.asObservable().observeOn(MainScheduler.instance)
        outputActiveNoiceCancellingMode?.subscribe({ [weak self] outputActiveNoiceCancellingModeElement in
            guard let strongSelf = self, let outputActiveNoiceCancellingMode = outputActiveNoiceCancellingModeElement.element else { return }
            guard let currentActiveNoiceCancellingSettings = strongSelf.currentActiveNoiceCancellingSettings,
                outputActiveNoiceCancellingMode != currentActiveNoiceCancellingSettings.mode else { return }
            strongSelf.currentActiveNoiceCancellingSettings = strongSelf.currentActiveNoiceCancellingSettings?.changing(mode: outputActiveNoiceCancellingMode)
            strongSelf.underlineSegmentedControl?.selectedSegmentIndex = outputActiveNoiceCancellingMode.index
            strongSelf.updateRelatedUIComponents()
        }).disposed(by: disposeBag)
        viewModel?.inputs.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if currentActiveNoiceCancellingSettings == nil {
            addLoadingView(animated: true)
        }
        viewModel?.inputs.viewDidAppear()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel?.inputs.viewWillDisappear()
    }
    deinit {
        print(#function, String(describing: self))
    }
}

private extension ActiveNoiceCancellingViewController {
    func updateRelatedUIComponents(initial: Bool = false) {
        guard let currentActiveNoiceCancellingSettings = currentActiveNoiceCancellingSettings else {
            return
        }
        switch currentActiveNoiceCancellingSettings.mode {
        case .playBackOnly:
            updateLevelLabel(text: currentActiveNoiceCancellingSettings.levelText, animated: !initial)
            updateLevelSlider(to: .zero, animated: true) { [weak self] in
                self?.setUserInteractionForSlider(enabled: false)
                self?.applyDescriptionLabel(text: Strings.anc_settings_screen_off_subtitle(), animated: !initial)
            }
            setSlider(enabledWithAlpha: false)
        case .activeNoiseCancelling:
            updateLevelLabel(text: currentActiveNoiceCancellingSettings.levelText, animated: !initial)
            updateLevelSlider(to: currentActiveNoiceCancellingSettings.activeNoiseCancellingLevel, animated: true) { [weak self] in
                self?.setUserInteractionForSlider(enabled: true)
                self?.setSlider(enabledWithAlpha: true)
                self?.applyDescriptionLabel(text: Strings.anc_settings_screen_on_subtitle(), animated: !initial)
            }
        case .monitoring:
            updateLevelLabel(text: currentActiveNoiceCancellingSettings.levelText, animated: !initial)
            updateLevelSlider(to: currentActiveNoiceCancellingSettings.monitoringLevel, animated: true) { [weak self] in
                self?.setUserInteractionForSlider(enabled: true)
                self?.setSlider(enabledWithAlpha: true)
                self?.applyDescriptionLabel(text: Strings.anc_settings_screen_monitoring_subtitle(), animated: !initial)
            }
        }
    }
}

private extension ActiveNoiceCancellingViewController {
    func applyDescriptionLabel(text: String, animated: Bool = false) {
        if animated {
            descriptionLabel.animateTextTransition(with: UILabel.TextTransitionParameters(
                attributedText: UIFont.attributedText(text, color: .white),
                textAligment: .center,
                lineBreakMode: .byTruncatingTail,
                animationOptions: [.curveEaseInOut, .transitionCrossDissolve]
            ))
        } else {
            descriptionLabel.attributedText = UIFont.attributedText(text, color: .white)
        }
    }
    func updateLevelLabel(text: NSAttributedString, animated: Bool = false) {
        if animated {
            levelLabel.animateTextTransition(with: UILabel.TextTransitionParameters(
                attributedText: text,
                textAligment: .center,
                lineBreakMode: .byTruncatingTail,
                animationOptions: [.curveEaseInOut, .transitionFlipFromTop]
            ))
        } else {
            levelLabel.attributedText = text
        }
    }
    func configureUnderlineSegmentedControl() {
        underlineSegmentedControl = UnderlineSegmentedControl(segments: [
            UIFont.attributedTitle(Strings.anc_settings_screen_on_uc()),
            UIFont.attributedTitle(Strings.anc_settings_screen_monitoring_uc()),
            UIFont.attributedTitle(Strings.anc_settings_screen_off_uc())
        ])
        underlineSegmentedControl?.addTarget(self, action: #selector(onValueChange(_:)), for: .valueChanged)
        underlineSegmentedControlContainerView.addSubview(underlineSegmentedControl!)
        underlineSegmentedControl!.translatesAutoresizingMaskIntoConstraints = false
        underlineSegmentedControl!.topAnchor.constraint(equalTo: underlineSegmentedControlContainerView.topAnchor).isActive = true
        underlineSegmentedControl!.leadingAnchor.constraint(equalTo: underlineSegmentedControlContainerView.leadingAnchor).isActive = true
        underlineSegmentedControl!.trailingAnchor.constraint(equalTo: underlineSegmentedControlContainerView.trailingAnchor).isActive = true
    }
    func configureVerticalSlider() {
        levelSlider.minimumValue = Float(Config.Slider.minimumValue)
        levelSlider.maximumValue = Float(Config.Slider.maximumValue)
        levelSlider.addTarget(self, action: #selector(onSliderChange(_:)), for: .valueChanged)
        levelSlider.minimumTrackTintColor = Color.volumeSliderMin
        levelSlider.maximumTrackTintColor = Color.volumeSliderMax
    }
    func updateLevelSlider(to value: Int, animated: Bool, completion: (() -> Void)? = nil) {
        levelSlider.isUserInteractionEnabled = false
        let animations: () -> Void = { [weak self] in
            self?.levelSlider.setValue(Float(value), animated: true)
        }
        let completionBlock: (Bool) -> Void = { [weak self] _ in
            self?.levelSlider.isUserInteractionEnabled = true
            completion?()
        }
        UIView.animate(withDuration: animated ? Config.SliderAnimation.duration : TimeInterval(),
                       delay: TimeInterval(),
                       options: Config.SliderAnimation.options,
                       animations: animations,
                       completion: completionBlock)
    }
    func setSlider(enabledWithAlpha: Bool) {
        levelSlider.alpha = enabledWithAlpha ? Config.Slider.enabledAlpha : Config.Slider.disabledAlpha
    }
    func setUserInteractionForSlider(enabled: Bool) {
        levelSlider.isUserInteractionEnabled = enabled
    }
}

extension ActiveNoiceCancellingViewController {
    @objc func onSliderChange(_ sender: UISlider!) {
        guard let activeNoiceCancellingSettings = currentActiveNoiceCancellingSettings else {
            return
        }
        let value = Int(levelSlider.value)
        switch activeNoiceCancellingSettings.mode {
        case .activeNoiseCancelling where value != activeNoiceCancellingSettings.activeNoiseCancellingLevel:
            currentActiveNoiceCancellingSettings = currentActiveNoiceCancellingSettings?.changing(activeNoiseCancellingLevel: value)
            updateLevelLabel(text: currentActiveNoiceCancellingSettings!.levelText, animated: false)
            viewModel?.inputs.inputActiveNoiceCancellingLevel.accept(value)
        case .monitoring where value != activeNoiceCancellingSettings.monitoringLevel:
            currentActiveNoiceCancellingSettings = currentActiveNoiceCancellingSettings?.changing(monitoringLevel: value)
            updateLevelLabel(text: currentActiveNoiceCancellingSettings!.levelText, animated: false)
            viewModel?.inputs.inputMonitoringLevel.accept(value)
        default:
            break
        }
    }
    @objc func onValueChange(_ sender: UnderlineSegmentedControl!) {
        guard let activeNoiceCancellingMode = ActiveNoiseCancellingMode.mode(for: sender.selectedSegmentIndex) else {
            return
        }
        viewModel?.inputActiveNoiceCancellingMode.accept(activeNoiceCancellingMode)
        currentActiveNoiceCancellingSettings = currentActiveNoiceCancellingSettings?.changing(mode: activeNoiceCancellingMode)
        updateRelatedUIComponents()
    }
}

private extension ActiveNoiseCancellingMode {
    private struct Index {
        static let playBackOnly = 2
        static let activeNoiseCancelling = 0
        static let monitoring = 1
    }
    var index: Int {
        switch self {
        case .playBackOnly:
            return Index.playBackOnly
        case .activeNoiseCancelling:
            return Index.activeNoiseCancelling
        case .monitoring:
            return Index.monitoring
        }
    }
    static func mode(for index: Int) -> ActiveNoiseCancellingMode? {
        switch index {
        case Index.playBackOnly:
            return .playBackOnly
        case Index.activeNoiseCancelling:
            return .activeNoiseCancelling
        case Index.monitoring:
            return .monitoring
        default:
            return nil
        }
    }
}

private extension ActiveNoiseCancellingSettings {
    var levelText: NSAttributedString {
        switch mode {
        case .playBackOnly:
            return UIFont.attributedH0(Strings.anc_settings_screen_off_uc(), color: UIColor.mediumGrey)
        case .activeNoiseCancelling:
            return UIFont.attributedH0(String(activeNoiseCancellingLevel) + Config.Level.postfix)
        case .monitoring:
            return UIFont.attributedH0(String(monitoringLevel) + Config.Level.postfix)
        }
    }
}
