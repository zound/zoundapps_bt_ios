//
//  HamburgerMenuViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 20.04.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

class HamburgerMenuViewController: UIViewController {
    
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var helpButton: UIButton!
    @IBOutlet weak var shopButton: UIButton!
    @IBOutlet weak var aboutButton: UIButton!
    
    internal var viewModel: HamburgerMenuViewModelType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = Color.backgroundForMainMenu
        
        settingsButton.setAttributedTitle(UIFont.attributedMainMenuText(Strings.main_menu_item_settings()), for: .normal)
        helpButton.setAttributedTitle(UIFont.attributedMainMenuText(Strings.main_menu_item_help()), for: .normal)
        shopButton.setAttributedTitle(UIFont.attributedMainMenuText(Strings.main_menu_item_shop()), for: .normal)
        aboutButton.setAttributedTitle(UIFont.attributedMainMenuText(Strings.appwide_about()), for: .normal)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swiped))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
    }

    @objc func swiped(_ recognizer: UISwipeGestureRecognizer) {
        if recognizer.state == .recognized {
           viewModel?.inputs.requestClose()
        }
    }
    
    @IBAction func onSettings(_ sender: UIButton) {
        viewModel?.inputs.settingsTapped()
    }
    
    @IBAction func onHelp(_ sender: UIButton) {
        viewModel?.inputs.helpTapped()
    }
    
    @IBAction func onShop(_ sender: UIButton) {
        viewModel?.inputs.shopTapped()
    }
    
    @IBAction func onAbout(_ sender: UIButton) {
        viewModel?.inputs.aboutTapped()
    }

}
