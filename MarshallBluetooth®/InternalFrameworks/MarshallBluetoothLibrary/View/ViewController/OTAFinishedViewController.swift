//
//  OTAFinishedViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 06.04.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

class OTAFinishedViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var speakerImageView: UIImageView!
    @IBOutlet weak var doneButton: UIButton!
    
    internal var viewModel: OTAFinishedViewModelType!
    internal let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        
        titleLabel.resizableAttributedFont(text: UIFont.attributedLargeTitle(Strings.ota_screen_finished_title_uc()))
        subtitleLabel.resizableAttributedFont(text: UIFont.attributedText(Strings.ota_screen_finished_subtitle(), color: Color.text), numberOfLines: 0)
        doneButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.appwide_done_uc()), for: .normal)
        
        viewModel.outputs.speakerImage
            .asDriver()
            .drive(speakerImageView.rx.image)
            .disposed(by: disposeBag)
    }
    
    @IBAction func onDone(_ sender: UIButton) {
        viewModel.inputs.doneTapped()
    }
}
