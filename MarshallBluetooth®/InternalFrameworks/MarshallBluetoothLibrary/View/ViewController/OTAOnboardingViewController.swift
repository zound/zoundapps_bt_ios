//
//  OTAOnboardingViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 08/10/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

class OTAOnboardingViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var point1Label: UILabel!
    @IBOutlet weak var point2Label: UILabel!
    @IBOutlet weak var point3Label: UILabel!
    @IBOutlet weak var point4Label: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    internal var viewModel: OTAOnboardingViewModelType?
    internal let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        view.backgroundColor = Color.background
        
        titleLabel.resizableAttributedFont(text: UIFont.attributedLargeTitle(Strings.main_menu_item_bluetooth_pairing_uc()), numberOfLines: 2)
        point1Label.resizableAttributedFont(text: UIFont.attributedText(Strings.ios_ota_onboarding_point1(), color: Color.text), numberOfLines: 0)
        point2Label.resizableAttributedFont(text: UIFont.attributedText(Strings.ios_ota_onboarding_point2(), color: Color.text), numberOfLines: 0)
        point3Label.resizableAttributedFont(text: UIFont.attributedText(Strings.ios_ota_onboarding_point3(), color: Color.text), numberOfLines: 0)
        point4Label.resizableAttributedFont(text: UIFont.attributedText(Strings.ios_ota_onboarding_point4(), color: Color.text), numberOfLines: 0)
        nextButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.appwide_next_uc()), for: .normal)
        
        nextButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.viewModel?.inputs.nextTapped() })
            .disposed(by: disposeBag)
    }
}
