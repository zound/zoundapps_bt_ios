//
//  WelcomeViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 18/09/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

class WelcomeViewController: BaseViewController {

    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var checkbox: Checkbox!
    @IBOutlet weak var termsAndConditionTextView: UITextView!
    @IBOutlet weak var actionButton: UIButton!

    private var gradientLayer = CAGradientLayer()

    var viewModel: WelcomeViewModelType?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupHeaderLabel()
        setupDescriptionLabel()
        setupActionButton()
        setupImageView()
        setupTermsAndConditionTextView()
        hideContent()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addGradientLayer()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showContent()
    }

    @IBAction func actionButtonTouchedUpInside(_ sender: UIButton) {
        if checkbox.checked {
            viewModel?.inputs.userAcceptedTerms()
        } else {
            presentAlertController()
        }
    }

}

private extension WelcomeViewController {
    func addGradientLayer() {
        gradientLayer.frame = UIScreen.main.bounds
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.gradientGrey.cgColor]
        gradientView.layer.addSublayer(gradientLayer)
        view.sendSubview(toBack: gradientView)
    }
    private func setupActionButton() {
        actionButton.backgroundColor = Color.primaryButton
        actionButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.appwide_next_uc()), for: .normal)
    }
    func setupHeaderLabel() {
        headerLabel.attributedText = UIFont.attributedH1(Strings.welcome_screen_title_uc())
    }
    func setupDescriptionLabel() {
        descriptionLabel.attributedText = UIFont.attributedBodyText(Strings.welcome_screen_subtitle_v1())
    }
    func setupImageView() {
        imageView.image = UIImage.welcomeSpeakers
    }
    func hideContent() {
        imageView.transform = CGAffineTransform(translationX: CGFloat(), y: 20).concatenating(CGAffineTransform(scaleX: 0.8, y: 0.8))
        [headerLabel, descriptionLabel, imageView, gradientView, actionButton, checkbox, termsAndConditionTextView].forEach {
            $0?.alpha = CGFloat()
        }
    }
    func showContent() {
        guard viewModel?.outputs.shouldAnimateIntro == true else {
            imageView.transform = .identity
            [headerLabel, descriptionLabel, imageView, gradientView, actionButton, checkbox, termsAndConditionTextView].forEach {
                $0?.alpha = 1
            }
            return
        }
        viewModel?.inputs.introAnimated()
        let gradientAnimations: () -> Void = { [weak self] in
            self?.gradientView.alpha = 1
        }
        let imageViewAnimations: () -> Void = { [weak self] in
            self?.imageView.transform = .identity
            self?.imageView.alpha = 1
        }
        let textAnimation: () -> Void = { [weak self] in
            self?.headerLabel.alpha = 1
            self?.descriptionLabel.alpha = 1
        }
        let actionAreaAnimations: () -> Void = { [weak self] in
            self?.checkbox.alpha = 1
            self?.termsAndConditionTextView.alpha = 1
            self?.actionButton.alpha = 1
        }
        UIView.animate(withDuration: 1.0, delay: 0.4, options: .curveEaseIn, animations: gradientAnimations)
        UIView.animate(withDuration: 2.0, delay: 0.6, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: imageViewAnimations)
        UIView.animate(withDuration: 1.0, delay: 1.4, options: .curveEaseOut, animations: textAnimation)
        UIView.animate(withDuration: 1.0, delay: 1.4, options: .curveEaseOut, animations: actionAreaAnimations)
    }
    func setupTermsAndConditionTextView() {
        guard let firstTextComponent = Strings.welcome_screen_agree().components(separatedBy: stringComponentSeparator).first else {
            return
        }
        let secondTextComponent = Strings.welcome_screen_terms_and_conditions()
        let termsAndContitionsAttributedString = NSMutableAttributedString(attributedString: UIFont.attributedSmallText(firstTextComponent + secondTextComponent))
        let range = NSRange(location: firstTextComponent.count, length: secondTextComponent.count)
        termsAndContitionsAttributedString.addAttribute(.link, value: String(), range: range)
        termsAndConditionTextView.attributedText = termsAndContitionsAttributedString
        termsAndConditionTextView.linkTextAttributes = [NSAttributedStringKey.underlineStyle.rawValue: NSUnderlineStyle.styleSingle.rawValue]
        termsAndConditionTextView.tintColor = UIColor.white
        termsAndConditionTextView.isEditable = false
        termsAndConditionTextView.isScrollEnabled = false
        termsAndConditionTextView.delegate = self
    }
    func presentAlertController() {
        let title = Strings.ios_terms_and_conditions_screen_title_uc().components(separatedBy: stringComponentSeparator).first ?? String()
        let message = Strings.ios_terms_and_conditions_screen_alert_message()
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: Strings.ios_appwide_ok_uc(), style: .default))
        present(alertController, animated: true, completion: nil)
    }
}

extension WelcomeViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        viewModel?.inputs.showTermsAndConditions()
        return false
    }
}
