//
//  OTAInfoViewController.swift
//  MarshallBluetoothLibrary
//

import UIKit
import RxSwift

struct OTAInfo {
    let title: String
    let subtitle: String
}

class OTAInfoViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let otaTitle = otaTitle, let otaSubtitle = otaSubtitle else {
            fatalError("Could not initialize \(type(of: self).description()) with \(String(describing: self.otaTitle)), subtitle: \(String(describing: self.otaSubtitle))")
        }
        titleLabel.attributedText = UIFont.attributedH1(otaTitle)
        subtitleLabel.attributedText = UIFont.attributedH2(otaSubtitle)
        titleLabel.adjustsFontSizeToFitWidth = true
    }
    
    func configure(with otaInfo: OTAInfo) {
        self.otaTitle = otaInfo.title
        self.otaSubtitle = otaInfo.subtitle
    }

    private var otaTitle: String?
    private var otaSubtitle: String?
}
