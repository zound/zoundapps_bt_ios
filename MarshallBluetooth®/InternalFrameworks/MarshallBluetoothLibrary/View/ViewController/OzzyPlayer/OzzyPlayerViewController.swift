//
//  OzzyPlayerViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 20/02/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

private struct Config {
    struct Animation {
        static let duration: TimeInterval = 0.4
        static let options: UIViewAnimationOptions = .curveEaseInOut
    }
    static let separatorAlbumArtist = " - "
    static let volumeDivisor = 2
    static let throttleTime = TimeInterval.seconds(0.5)
}

class OzzyPlayerViewController: BaseViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var trackLabel: UILabel!
    @IBOutlet weak var albumArtistLabel: UILabel!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var volumeSlider: UISlider!

    @IBOutlet weak var auxCoverView: UIView!
    @IBOutlet weak var auxImageView: UIImageView!
    @IBOutlet weak var auxCircleImageView: UIImageView!
    @IBOutlet weak var auxDescriptionLabel: UILabel!

    var viewModel: OzzyPlayerViewModel?
    private let disposeBag = DisposeBag()

    var volume: Int = Int() {
        didSet {
            guard oldValue != volume else { return }
            viewModel?.inputs.inputVolume.accept(volume * Config.volumeDivisor)
        }
    }
    @IBAction func closeButtonTouchedUpInside(_ sender: UIButton) {
        viewModel?.inputs.close()
    }
    @IBAction func volumeSliderValueChanged(_ sender: UISlider) {
        volume = Int(sender.value)
    }
    @IBAction func previousButtonTouchedUpInside(_ sender: UIButton) {
        viewModel?.inputs.previusButtonTouchedUpInside()
    }
    @IBAction func playPauseButtonTouchedUpInside(_ sender: UIButton) {
        viewModel?.inputs.playPauseButtonTouchedUpInside()
    }
    @IBAction func nextButtonTouchedUpInside(_ sender: UIButton) {
        viewModel?.inputs.nextButtonTouchedUpInside()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTitleLabel()
        setupVolumeSlider()
        setupAuxViews()

        let outputVolumeInit = viewModel?.outputs.outputVolumeInit.asObservable().observeOn(MainScheduler.instance)
        outputVolumeInit?.subscribe({ [weak self] outputVolumeInit in
            guard let strongSelf = self, let volume = outputVolumeInit.element else { return }
            strongSelf.updateSlider(to: volume / Config.volumeDivisor, animated: false)
        }).disposed(by: disposeBag)
        let outputVolume = viewModel?.outputs.outputVolume.asObservable().observeOn(MainScheduler.instance)
        outputVolume?.subscribe({ [weak self] outputVolumeElement in
            guard let strongSelf = self, let volume = outputVolumeElement.element else { return }
            strongSelf.updateSlider(to: volume / Config.volumeDivisor, animated: true)
        }).disposed(by: disposeBag)

        let outputAudioControlStatus = viewModel?.outputs.outputAudioControlStatus.asObservable().observeOn(MainScheduler.instance)
        outputAudioControlStatus?.subscribe({ [weak self] outputAudioControlStatusElement in
            guard let strongSelf = self, let outputAudioControlStatus = outputAudioControlStatusElement.element else { return }
            strongSelf.playPauseButton.setImage(outputAudioControlStatus == .isPlaying ? UIImage.pauseButton : UIImage.playButton, for: .normal)
        }).disposed(by: disposeBag)

        let outputAudioNowPlaying = viewModel?.outputs.outputAudioNowPlaying.asObservable().observeOn(MainScheduler.instance)
        outputAudioNowPlaying?.subscribe({ [weak self] outputAudioNowPlayingElement in
            guard let strongSelf = self, let outputAudioNowPlaying = outputAudioNowPlayingElement.element else { return }
            strongSelf.update(audioNowPlaying: outputAudioNowPlaying)
        }).disposed(by: disposeBag)

        let outputAuxVisible = viewModel?.outputs.outputAuxVisible.asObservable().observeOn(MainScheduler.instance)
        outputAuxVisible?.subscribe({ [weak self] outputAuxVisibleElement in
            guard let strongSelf = self, let outputAuxVisible = outputAuxVisibleElement.element else { return }
            strongSelf.update(auxVisible: outputAuxVisible)
        }).disposed(by: disposeBag)

        viewModel?.viewDidLoad()
    }
}

private extension OzzyPlayerViewController {
    func setupTitleLabel() {
        titleLabel.attributedText = UIFont.attributedTitle(Strings.player_audio_uc())
    }
    func setupVolumeSlider() {
        volumeSlider.minimumTrackTintColor = Color.volumeSliderMin
        volumeSlider.maximumTrackTintColor = Color.volumeSliderMax
        volumeSlider.setThumbImage(UIImage.sliderThumb, for: .normal)
        volumeSlider.minimumValue = Float((viewModel?.outputs.platformTraits.volumeMin ?? .zero) / Config.volumeDivisor)
        volumeSlider.maximumValue = Float((viewModel?.outputs.platformTraits.volumeMax ?? .zero) / Config.volumeDivisor)
    }
    func setupAuxViews() {
        auxCoverView.backgroundColor = Color.background
        (auxImageView.isHidden, auxCircleImageView.isHidden, auxDescriptionLabel.isHidden) = (true, true, true)
        auxDescriptionLabel.attributedText = UIFont.attributedLabelText(Strings.player_audio_source_aux_activated())
    }
}

private extension OzzyPlayerViewController {
    func updateSlider(to volume: Int, animated: Bool) {
        volumeSlider.isUserInteractionEnabled = false
        let animations: () -> Void = { [weak self] in
            self?.volumeSlider.setValue(Float(volume), animated: true)
        }
        let completionBlock: (Bool) -> Void = { [weak self] _ in
            self?.volumeSlider.isUserInteractionEnabled = true
        }
        UIView.animate(withDuration: animated ? Config.Animation.duration : TimeInterval(),
                       delay: TimeInterval(),
                       options: Config.Animation.options,
                       animations: animations,
                       completion: completionBlock)
    }
    func update(audioNowPlaying: AudioNowPlaying) {
        trackLabel.attributedText = UIFont.attributedTitle(audioNowPlaying.title)
        let mutableAttributedString = NSMutableAttributedString(attributedString: UIFont.attributedAlbumLabel(audioNowPlaying.album))
        mutableAttributedString.append(UIFont.attributedArtistLabel( audioNowPlaying.artist == .invisible ? audioNowPlaying.artist : Config.separatorAlbumArtist + audioNowPlaying.artist))
        albumArtistLabel.attributedText = mutableAttributedString
    }
    func update(auxVisible: Bool) {
        if !auxVisible {
            (auxImageView.isHidden, auxCircleImageView.isHidden, auxDescriptionLabel.isHidden) = (true, true, true)
        }
        UIView.animate(withDuration: Config.Animation.duration, delay: .zero, options: .curveEaseInOut, animations: { [weak self] in
            self?.auxCoverView?.alpha = auxVisible ? 1 : .zero
        }, completion: { [weak self] _ in
            guard let strongSelf = self else { return }
            strongSelf.auxImageView.isHidden = !auxVisible
            strongSelf.auxDescriptionLabel.isHidden = !auxVisible
            strongSelf.auxCircleImageView.isHidden = !auxVisible
        })
    }
}
