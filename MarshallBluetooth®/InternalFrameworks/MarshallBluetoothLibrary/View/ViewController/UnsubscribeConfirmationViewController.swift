//
//  UnsubscribeConfirmationViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Paprota Przemyslaw on 25/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

class UnsubscribeConfirmationViewController: BaseViewController {
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    internal var viewModel: UnsubscribeConfirmationViewModelType?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.resizableAttributedFont(text: UIFont.attributedLargeTitle(Strings.stay_updated_confirm_title_uc()))
        subtitleLabel.resizableAttributedFont(text: UIFont.attributedText(Strings.stay_updated_confirm_subtitle_v1(), color: Color.text), numberOfLines: 0)
        
        okButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.stay_updated_confirm_yes_uc()), for: .normal)
        cancelButton.setAttributedTitle(UIFont.attributedNegativeButtonLabel(Strings.appwide_cancel_uc()), for: .normal)
    }
    
    @IBAction func cancel(_ sender: Any) {
        viewModel?.inputs.cancel()
    }
    
    @IBAction func confirm(_ sender: Any) {
        viewModel?.inputs.confirm()
    }
}
