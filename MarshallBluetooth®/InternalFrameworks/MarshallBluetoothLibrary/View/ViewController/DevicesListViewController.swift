//
//  DevicesListViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 16/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import Cartography
import RxSwift
import GUMA

enum DeviceListActionType {
    case add(IndexPath)
    case move(IndexPath, IndexPath)
    case update(IndexPath)
    case remove(IndexPath)
    case empty
}

final class DevicesListViewController: BaseViewController {
    @IBOutlet weak var rootContainerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleImageView: UIImageView!
    
    weak var viewModel: DevicesListViewModelType!
    
    var footerHeight: CGFloat = 60
    let footerLineHeight: CGFloat = 1.5
    var footerLineMargin: CGFloat = 26
    var footerContainerView: UIView!
    var footerLineView: UIView!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        disableVolumeView()
        setupNavigationBar(title: UIFont.attributedTitle(Strings.welcome_screen_title_uc()))
        
        tableView.register(HomeSpeakerCell.self, forCellReuseIdentifier: HomeSpeakerCell.id)
        tableView.register(nib: Nib.NoDevicesFoundCell)
        tableView.rowHeight = 180.0
        //tableView.estimatedRowHeight = 185.0
        
        tableView.dataSource = self

        let separator = UIView()
        
        separator.backgroundColor = UIColor(white: 0.0, alpha: 1.0)
        footerLineView = separator
        let containerView = UIView(frame: CGRect(x: 0, y: 0,width: tableView.bounds.width,height: footerHeight))
        containerView.addSubview(separator)
        separator.translatesAutoresizingMaskIntoConstraints = false
        constrain(separator) { view in
            view.width == view.superview!.width - footerLineMargin * 2 ~ LayoutPriority(999)
            view.height == footerLineHeight
            view.width <= 600
            view.center == view.superview!.center
        }
        containerView.backgroundColor = UIColor.clear
        footerContainerView = containerView
        viewModel.outputs.appMode
            .observeOn(MainScheduler.instance)
            .subscribeNext(weak: self, DevicesListViewController.appModeChanged)
            .disposed(by: rx_disposeBag)
        
        viewModel.outputs.dataReloadRequested
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.tableView.reloadData()
            })
            .disposed(by: rx_disposeBag)
        
        viewModel.outputs.coupledIndicationRequested
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] pair in
                self?.indicateCoupled(pair: pair)
            })
            .disposed(by: rx_disposeBag)
        
        viewModel.outputs.currentCellStatus
            .asDriver()
            .drive(onNext: { [weak self] status in
                self?.update(info: status)
            }
        )
        .disposed(by: rx_disposeBag)
        
        viewModel.outputs.listActionTrigger
            .observeOn(MainScheduler.instance)
            .flatMap { [unowned self] listEntry -> Single<Bool> in
                return self.action(for: listEntry)
            }
            .subscribe()
            .disposed(by: rx_disposeBag)
        
        viewModel.inputs.viewLoaded()
    }
    private func update(info: CellModelUpdateInfo) {
        guard let toUpdate = tableView.cellForRow(at: info.indexPath) as? HomeSpeakerCell else { return }
        toUpdate.currentStatus = info.updated
    }
    private func indicateCoupled(pair: (master: IndexPath, slave: IndexPath)) {
        guard let masterCell = tableView.cellForRow(at: pair.master) as? HomeSpeakerCell,
            let slaveCell = tableView.cellForRow(at: pair.slave) as? HomeSpeakerCell else {
                return
        }
        [masterCell, slaveCell].forEach {
            $0.indicateCouple()
        }
    }
    override public func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        super.viewWillAppear(animated)
        viewModel.inputs.viewWillAppear()
    }
    override public func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        super.viewWillDisappear(animated)
        viewModel.inputs.viewWillDisappear()
    }
    @IBAction func menuRequested(_ sender: Any) {
        viewModel.inputs.requestMenu()
    }
    deinit {
        tableView.removeAllPullToRefresh()
    }
    private var dataSnapshot: [HomeDeviceCellViewModelType] = []
}

extension DevicesListViewController: UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = dataSnapshot.count
        return count > 0 ? count : 1
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard dataSnapshot.count > 0 else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: NoDevicesFoundCell.defaultReusableId) as? NoDevicesFoundCell else {
                fatalError("Failed to initialize cell \(NoDevicesFoundCell.defaultReusableId) (for \(self)")
            }
            cell.selectionStyle = .none
            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HomeSpeakerCell.id) as? HomeSpeakerCell else {
            fatalError("Failed to initialize cell \(HomeSpeakerCell.id) for \(self)")
        }
        let entry = dataSnapshot[indexPath.row]
        cell.selectionStyle = .none
        cell.configureWith(value: entry)
        cell.currentStatus = entry.outputs.currentStatus.value
        return cell
    }
}
private extension DevicesListViewController {
    private func appModeChanged(_ mode: AppMode) {
        for (index, _) in viewModel.outputs.devices.value.enumerated() {
            guard let cell = tableView.cellForRow(at: IndexPath(item: index, section: Int())) as? HomeSpeakerCell else { return }
            switch mode {
            case .background:
                cell.viewModel?.inputs.viewWillResignActive()
            case .foreground:
                cell.viewModel?.inputs.viewDidBecameActive()
            }
        }
    }
    private func configureCATransaction(action: () -> Void, completion: @escaping () -> Void) {
        CATransaction.begin()
        CATransaction.setCompletionBlock {
            completion()
        }
        tableView.beginUpdates()
        action()
        tableView.endUpdates()
        CATransaction.commit()
    }
    
    func action(for entry: DeviceListActionEntry) -> Single<Bool> {
        return Single<Bool>.create(subscribe: { [weak self] event -> Disposable in
            guard let strongSelf = self else { return Disposables.create() }
            strongSelf.dataSnapshot = entry.models
            switch entry.type {
            case let .add(indexPath):
                strongSelf.configureCATransaction(
                    action: { [weak self] in
                        guard let strongSelf = self else {
                            return
                        }
                        if entry.models.count == 1 {
                            strongSelf.tableView.deleteRows(at: [indexPath], with: .none)
                        }
                        strongSelf.tableView.insertRows(at: [indexPath], with: .none)
                        strongSelf.tableView.reloadData()
                    },
                    completion: { [weak self] in
                        self?.viewModel.outputs.concluded.accept(entry)
                        event(.success(true))
                    }
                )
            case let .move(fromIndexPath, toIndexPath):
                strongSelf.configureCATransaction(
                    action: { [weak self] in
                        guard let strongSelf = self else {
                            return
                        }
                        strongSelf.tableView.reloadData()
                    },
                    completion: {
                        self?.viewModel.outputs.concluded.accept(entry)
                        event(.success(true))
                    }
                )
                
            case let .remove(indexPath):
                strongSelf.configureCATransaction(
                    action: { [weak self] in
                        guard let strongSelf = self else {
                            return
                        }
                        if entry.models.count == 0 {
                            strongSelf.tableView.insertRows(at: [IndexPath(item: .zero, section: .zero)], with: .none)
                        }
                        strongSelf.tableView.deleteRows(at: [indexPath], with: .none)
                        strongSelf.tableView.reloadData()
                    },
                    completion: {
                        self?.viewModel.outputs.concluded.accept(entry)
                        event(.success(true))
                    }
                )
            default: break
            }
            return Disposables.create()
        })
    }
}
