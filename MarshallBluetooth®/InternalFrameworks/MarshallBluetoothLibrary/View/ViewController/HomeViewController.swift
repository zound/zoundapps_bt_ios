//
//  HomeViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 16/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import Cartography
import RxSwift
import Zound

public final class HomeViewController: RootViewController, UIGestureRecognizerDelegate {
    
    var displayedViewController:UIViewController?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        view.addGestureRecognizer(edgePan)
    }
    
    var forcedHiddenStatusBar: Bool = false {
        didSet {
            UIView.animate(withDuration: 0.15, animations: { [weak self] in
                self?.setNeedsStatusBarAppearanceUpdate()
            })
        }
    }

    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            viewModel.inputs.displayMenu()
        }
    }
}
