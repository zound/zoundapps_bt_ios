//
//  EqPresetsAdvertViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 11.04.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

/// Remote Control advertisement view.
/// No view model because of no inputs/outputs (static screen with no interaction with user)
class EqPresetsAdvertViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func viewDidLoad() {
        titleLabel.resizableAttributedFont(text: UIFont.attributedLargeTitle(Strings.ota_screen_eq_presets_title_uc()))
        subtitleLabel.resizableAttributedFont(text: UIFont.attributedText(Strings.ota_screen_eq_presets_subtitle(), color: Color.text), numberOfLines: 0)
        subtitleLabel.textAlignment = .center
    }
}
