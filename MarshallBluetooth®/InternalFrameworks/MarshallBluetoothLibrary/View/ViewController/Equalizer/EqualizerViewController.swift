//
//  EqualizerViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 10/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift
import GUMA

private struct Config {
    struct SliderAnimation {
        static let duration: TimeInterval = 0.4
        static let options: UIViewAnimationOptions = .curveEaseInOut
    }
    struct DropDownList {
        static let bottomMargin = CGFloat(30)
    }
}

class EqualizerViewController: BaseViewController {

    @IBOutlet private weak var descriptionLabel: UILabel!

    @IBOutlet weak var dropDownList: DropDownList!

    @IBOutlet private weak var bassVerticalSlider: VerticalSlider!
    @IBOutlet private weak var lowVerticalSlider: VerticalSlider!
    @IBOutlet private weak var midVerticalSlider: VerticalSlider!
    @IBOutlet private weak var upperVerticalSlider: VerticalSlider!
    @IBOutlet private weak var highVerticalSlider: VerticalSlider!

    @IBOutlet private weak var bassLabel: UILabel!
    @IBOutlet private weak var lowLabel: UILabel!
    @IBOutlet private weak var midLabel: UILabel!
    @IBOutlet private weak var upperLabel: UILabel!
    @IBOutlet private weak var highLabel: UILabel!

    var viewModel: EqualizerViewModel?
    private let disposeBag = DisposeBag()

    private var verticalSliders: [VerticalSlider] {
        return [bassVerticalSlider, lowVerticalSlider, midVerticalSlider, upperVerticalSlider, highVerticalSlider]
    }

    private var currentCustomGraphicalEqualizer: GraphicalEqualizer = GraphicalEqualizer.Preset.customDefault
    private var currentGraphicalEqualizerPreset: GraphicalEqualizerPreset? {
        didSet {
            guard currentGraphicalEqualizerPreset != oldValue else { return }
            guard let preset = currentGraphicalEqualizerPreset else {
                updateInterfaceToCunrrentPreset(animated: true)
                return
            }
            updateSegmentedControlToCurrentPreset()
            if case .custom(let graphicalEqualizer) = preset {
                currentCustomGraphicalEqualizer = graphicalEqualizer
            }
            viewModel?.inputs.inputGraphicalEqualizer.accept(preset.graphicalEqualizer)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: UIFont.attributedTitle(Strings.device_settings_menu_item_equaliser_uc()))
        applyTextForDescriptionLabel()
        applyTextForFrequencyLabels()
        configureVerticalSlidersTargetActions()
        configureDropDownList()

        let outputGraphicalEqualizerInit = viewModel?.outputs.outputGraphicalEqualizerInit.asObservable().observeOn(MainScheduler.instance)
        outputGraphicalEqualizerInit?.subscribe({ [weak self] outputGraphicalEqualizerInitElement in
            guard let strongSelf = self, let outputGraphicalEqualizerInit = outputGraphicalEqualizerInitElement.element else { return }
            strongSelf.currentGraphicalEqualizerPreset = GraphicalEqualizerPreset(graphicalEqualizer: outputGraphicalEqualizerInit.graphicalEqualizer)
            strongSelf.currentCustomGraphicalEqualizer = outputGraphicalEqualizerInit.customGraphicalEqualizer
            strongSelf.updateSliders(to: strongSelf.currentGraphicalEqualizerPreset!, animated: false)
            strongSelf.removeLoadingView(animated: false)
        }).disposed(by: disposeBag)

        let outputGraphicalEqualizer = viewModel?.outputs.outputGraphicalEqualizer.asObservable().observeOn(MainScheduler.instance)
        outputGraphicalEqualizer?.subscribe({ [weak self] outputGraphicalEqualizerElement in
            guard let strongSelf = self, let outputGraphicalEqualizer = outputGraphicalEqualizerElement.element else { return }
            strongSelf.currentGraphicalEqualizerPreset = GraphicalEqualizerPreset(graphicalEqualizer: outputGraphicalEqualizer)
            strongSelf.updateSliders(to: strongSelf.currentGraphicalEqualizerPreset!, animated: true)
        }).disposed(by: disposeBag)

        viewModel?.inputs.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(appWillResignActive), name: .UIApplicationWillResignActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: .UIApplicationDidBecomeActive, object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setDropDownListHeight()
        if currentGraphicalEqualizerPreset == nil {
            addLoadingView(animated: true)
        }
        viewModel?.inputs.viewDidAppear()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel?.inputs.viewWillResignActive()
    }

    @objc func appDidBecomeActive(_ notification: Notification) {
        viewModel?.inputs.viewDidBecameActive()
    }

    @objc func appWillResignActive(_ notification: Notification) {
        viewModel?.inputs.viewWillResignActive()
    }

    deinit {
        print(#function, String(describing: self))
    }

}

private extension EqualizerViewController {

    func applyTextForDescriptionLabel() {
        descriptionLabel.attributedText = UIFont.attributedText(Strings.equaliser_screen_subtitle(), color: .white)
    }

    func applyTextForFrequencyLabels() {
        zip([bassLabel, lowLabel, midLabel, upperLabel, highLabel],
            [Strings.equaliser_screen_preset_bass(),
             Strings.equaliser_screen_preset_low(),
             Strings.equaliser_screen_preset_mid(),
             Strings.equaliser_screen_preset_upper(),
             Strings.equaliser_screen_preset_high()]
        ).forEach {
            $0.0?.attributedText = UIFont.attributedSkip($0.1)
        }
    }

    var optionsTextForDropDownList: [NSAttributedString] {
        return [
            UIFont.attributedDropdownList(Strings.equaliser_screen_preset_flat_uc()),
            UIFont.attributedDropdownList(Strings.equaliser_screen_preset_custom_uc()),
            UIFont.attributedDropdownList(Strings.equaliser_screen_preset_rock_uc()),
            UIFont.attributedDropdownList(Strings.equaliser_screen_preset_metal_uc()),
            UIFont.attributedDropdownList(Strings.equaliser_screen_preset_pop_uc()),
            UIFont.attributedDropdownList(Strings.equaliser_screen_preset_hip_hop_uc()),
            UIFont.attributedDropdownList(Strings.equaliser_screen_preset_electronic_uc()),
            UIFont.attributedDropdownList(Strings.equaliser_screen_preset_jazz_uc())
        ]
    }

}

extension EqualizerViewController: DropDownListDelegate {

    func didSelect(index: Int) {
        guard let preset = GraphicalEqualizerPreset.predefinedPreset(for: index) else {
            updateSliders(to: .custom(currentCustomGraphicalEqualizer), animated: true) { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.currentGraphicalEqualizerPreset = .custom(strongSelf.currentCustomGraphicalEqualizer)
            }
            return
        }
        updateSliders(to: preset, animated: true) { [weak self] in
            self?.currentGraphicalEqualizerPreset = preset
        }
    }

}

private extension EqualizerViewController {

    func configureDropDownList() {
        dropDownList.setup(options: optionsTextForDropDownList)
        dropDownList.delegate = self
    }

    func setDropDownListHeight() {
        let dropDownListHeight = view.frame.size.height - (dropDownList.frame.origin.y + dropDownList.frame.size.height + Config.DropDownList.bottomMargin)
        let rowHeight = (dropDownListHeight / CGFloat(optionsTextForDropDownList.count)).rounded(.down)
        dropDownList.set(rowHeight: rowHeight)
    }

    func configureVerticalSlidersTargetActions() {
        verticalSliders.forEach {
            $0.maximumValue = Float(GraphicalEqualizer.max)
            $0.addTarget(self, action: #selector(onAnySliderChange(_:)), for: .valueChanged)
        }
    }

    @objc func onAnySliderChange(_ sender: UISlider!) {
        let graphicalEqualizer = GraphicalEqualizer(bass: UInt8(bassVerticalSlider.value),
                                                    low: UInt8(lowVerticalSlider.value),
                                                    mid: UInt8(midVerticalSlider.value),
                                                    upper: UInt8(upperVerticalSlider.value),
                                                    high: UInt8(highVerticalSlider.value))
        currentGraphicalEqualizerPreset = GraphicalEqualizerPreset(graphicalEqualizer: graphicalEqualizer)
    }

    func updateSliders(to preset: GraphicalEqualizerPreset, animated: Bool, completion: (() -> Void)? = nil) {
        setUserInteractionForSliders(enabled: false)
        let animations = { [weak self] in
            self?.bassVerticalSlider.value = Float(preset.graphicalEqualizer.bass)
            self?.lowVerticalSlider.value = Float(preset.graphicalEqualizer.low)
            self?.midVerticalSlider.value = Float(preset.graphicalEqualizer.mid)
            self?.upperVerticalSlider.value = Float(preset.graphicalEqualizer.upper)
            self?.highVerticalSlider.value = Float(preset.graphicalEqualizer.high)
        }
        let completionBlock: (Bool) -> Void = { [weak self] _ in
            self?.setUserInteractionForSliders(enabled: true)
            completion?()
        }
        UIView.animate(withDuration: animated ? Config.SliderAnimation.duration : TimeInterval(),
                       delay: TimeInterval(),
                       options: Config.SliderAnimation.options,
                       animations: animations,
                       completion: completionBlock)
    }

    func setUserInteractionForSliders(enabled: Bool) {
        verticalSliders.forEach {
            $0.isUserInteractionEnabled = enabled
        }
    }

    func updateSegmentedControlToCurrentPreset() {
        if let preset = currentGraphicalEqualizerPreset {
            dropDownList.selectedIndex = preset.index
            dropDownList.isEnabled = true
        } else {
            dropDownList.isEnabled = false
        }
    }

    func updateInterfaceToCunrrentPreset(animated: Bool) {
        if let currentPreset = currentGraphicalEqualizerPreset {
            updateSliders(to: currentPreset, animated: animated)
        }
        updateSegmentedControlToCurrentPreset()
    }

}

fileprivate extension GraphicalEqualizerPreset {

    private struct Index {
        static let flat = 0
        static let custom = 1
        static let rock = 2
        static let metal = 3
        static let pop = 4
        static let hipHop = 5
        static let electronic = 6
        static let jazz = 7
    }

    var index: Int {
        switch self {
        case .flat:
            return Index.flat
        case .rock:
            return Index.rock
        case .metal:
            return Index.metal
        case .pop:
            return Index.pop
        case .hipHop:
            return Index.hipHop
        case .electronic:
            return Index.electronic
        case .jazz:
            return Index.jazz
        case .custom:
            return Index.custom
        }
    }

    static func predefinedPreset(for index: Int) -> GraphicalEqualizerPreset? {
        switch index {
        case Index.flat:
            return .flat
        case Index.rock:
            return .rock
        case Index.metal:
            return .metal
        case Index.pop:
            return .pop
        case Index.hipHop:
            return .hipHop
        case Index.electronic:
            return .electronic
        case Index.jazz:
            return .jazz
        default:
            return nil
        }
    }

}
