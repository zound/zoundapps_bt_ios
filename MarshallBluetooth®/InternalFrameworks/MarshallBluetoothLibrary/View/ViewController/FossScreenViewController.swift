//
//  FossScreenViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Paprota Przemyslaw on 13/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

class FossScreenViewController: UIViewController {


    @IBOutlet weak var info: UILabel!

    @IBOutlet weak var websiteButton: UIButton!

    var viewModel: FossScreenViewModelType?

    @IBAction func goToWebsite(_ sender: Any) {
        present(SafariViewController.with(contentType: .foss), animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.blackOne
        setupNavigationBar(title: title())
        info.attributedText = infoText()
        websiteButton.setAttributedTitle(websiteButtonTitle(), for: .normal)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if isMovingFromParentViewController {
            viewModel?.inputs.close()
        }
    }

    private func title() -> NSAttributedString {
        return UIFont.attributedTitle(Strings.main_menu_item_foss_uc())
    }

    private func infoText() -> NSAttributedString {
        return UIFont.attributedInfoText(Strings.foss_subtitle())
    }

    private func websiteButtonTitle() -> NSAttributedString {
        return UIFont.attributedPrimaryButtonLabel(Strings.appwide_go_to_website_uc())
    }
}
