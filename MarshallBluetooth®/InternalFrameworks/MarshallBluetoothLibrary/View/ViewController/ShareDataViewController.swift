//
//  ShareDataViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 20.03.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ShareDataViewController: UIViewController {
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var dataCollectionLabel: UILabel!
    @IBOutlet weak var dataCollectionSwitch: UISwitch!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var paragraph: UILabel!
    @IBOutlet weak var point1: UILabel!
    @IBOutlet weak var point2: UILabel!
    @IBOutlet weak var point3: UILabel!
    
    var viewModel: ShareDataViewModelType?
    
    private var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Color.background
        
        setupTitleSubtitle()
        setupParagraphPoints()
        setupButtons()
        setupData()
    }
    
    @IBAction func onSwitch(_ sender: UISwitch) {
        viewModel?.inputs.saveDataSwitched()
    }
    
    @IBAction func onSkip(_ sender: Any) {
        viewModel?.inputs.skipButtonTapped()
    }
    
    @IBAction func onNext(_ sender: UIButton) {
        viewModel?.inputs.nextButtonTapped()
    }
}

private extension ShareDataViewController {
    func setupTitleSubtitle() {
        titleLabel.resizableAttributedFont(text: UIFont.attributedLargeTitle(Strings.share_data_screen_title_uc()))
        subtitleLabel.resizableAttributedFont(text: UIFont.attributedText(Strings.share_data_screen_subtitle(), color: Color.text), numberOfLines: 0)
    }
    
    func setupParagraphPoints() {
        paragraph.attributedText = UIFont.attributedMediumText(Strings.share_data_screen_content_title(), color: Color.text)

        point1.attributedText = UIFont.attributedMediumText(Strings.share_data_screen_content_part_1(), color: Color.text)
        point2.attributedText = UIFont.attributedMediumText(Strings.share_data_screen_content_part_2(), color: Color.text)
        point3.attributedText = UIFont.attributedMediumText(Strings.share_data_screen_content_part_3(), color: Color.text)
    }
    
    func setupButtons() {
        skipButton.setAttributedTitle(UIFont.attributedSecondaryButtonLabel(Strings.appwide_skip_uc()), for: .normal)
        
        viewModel?.outputs.skipVisible
            .asDriver()
            .drive(skipButton.rx.visible)
            .disposed(by: disposeBag)
        
        viewModel?.outputs.completionText
            .asDriver()
            .map { UIFont.attributedPrimaryButtonLabel($0) }
            .drive(nextButton.rx.attributedTitle())
            .disposed(by: disposeBag)
    }
    
    func setupData() {
        dataCollectionLabel.resizableAttributedFont(text: UIFont.attributedTextForShareDataConfirm(Strings.share_data_screen_i_want_to_share()))

        dataCollectionSwitch.onTintColor = Color.switcher
        dataCollectionSwitch.tintColor = Color.switcher

        viewModel?.outputs.shareDataEnabled
            .asDriver()
            .drive(dataCollectionSwitch.rx.isOn)
            .disposed(by: disposeBag)
    }
}
