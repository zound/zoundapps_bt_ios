//
//  GoogleAssistantViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Szatkowski Michal on 06/03/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

class GoogleAssistantViewController: BaseViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var bubbleImageView: UIView!
    @IBOutlet weak var bubbleText: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    let cloud = InformationCloud()
    private lazy var innerShaddowView: InnerShaddowView = {
        let innerShaddowView = InnerShaddowView()
        innerShaddowView.backgroundColor = .black
        return innerShaddowView
    }()
    var viewModel: GoogleAssistantViewModel?
    let disposeBag = DisposeBag()
    override func viewDidLoad() {
        setupConstraints()
        titleLabel.resizableAttributedFont(text: UIFont.attributedLargeTitle(Strings.google_assistant_screen_title_uc()))
        subtitleLabel.attributedText = UIFont.attributedText(Strings.google_assistant_screen_header(), color: Color.text)
        bodyLabel.attributedText = UIFont.attributedText(Strings.google_assistant_screen_body(), color: Color.text)
        bubbleText.attributedText = UIFont.attributedText(Strings.google_assistant_screen_cloud(), color: UIColor.blackOne)
        doneButton.setAttributedTitle(UIFont.attributedPrimaryButtonLabel(Strings.appwide_done_uc()), for: .normal)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    @IBAction func onDone(_ sender: UIButton) {
        viewModel?.inputs.doneTapped()
    }
}
private extension GoogleAssistantViewController {
    private func setupConstraints() {
        [innerShaddowView].forEach {
            view.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        cloud.translatesAutoresizingMaskIntoConstraints = false
        bubbleImageView.insertSubview(cloud, at: 0)
        view.sendSubview(toBack: innerShaddowView)
        NSLayoutConstraint.activate([
            innerShaddowView.topAnchor.constraint(equalTo: view.topAnchor),
            innerShaddowView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            innerShaddowView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            innerShaddowView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            cloud.topAnchor.constraint(equalTo: bubbleImageView.topAnchor),
            cloud.leadingAnchor.constraint(equalTo: bubbleImageView.leadingAnchor),
            cloud.trailingAnchor.constraint(equalTo: bubbleImageView.trailingAnchor),
            cloud.bottomAnchor.constraint(equalTo: bubbleImageView.bottomAnchor)
        ])
    }
}

