//
//  PairingCoverView.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 10/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit

extension Dimension {
    static let headerBigBottomMargin: CGFloat = 32
    static let edgeMargin: CGFloat = 16
}

class PairingCoverView: UIView {
    var headerLabelAttributedText: NSAttributedString = NSAttributedString() {
        didSet {
            headerLabel.attributedText = headerLabelAttributedText
            headerLabel.textAlignment = .center
        }
    }
    private lazy var headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.numberOfLines = 2
        return headerLabel
    }()
    private lazy var descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.attributedText = UIFont.attributedText(Strings.pairing_waiting_description(), color: Color.primaryButtonText)
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = .zero
        return descriptionLabel
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupConstraints()
    }
    private func setupConstraints() {
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(containerView)
        [headerLabel, descriptionLabel].forEach {
            containerView.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Dimension.edgeMargin),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Dimension.edgeMargin),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor),
            headerLabel.topAnchor.constraint(equalTo: containerView.topAnchor),
            headerLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            headerLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            descriptionLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            descriptionLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            descriptionLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        ])
    }
}
