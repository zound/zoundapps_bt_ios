//
//  DevicesListRefreshView.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 16/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

class DevicesListRefreshView: UIView {
    @IBOutlet weak var refreshIndicator: UIImageView!
}

