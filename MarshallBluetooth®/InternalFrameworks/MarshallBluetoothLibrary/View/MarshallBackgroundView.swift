import UIKit

/// Simple view for drawing gradients and borders.
internal class MarshallBackgroundView: UIImageView {
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = Color.background
        self.contentMode = .scaleToFill
    }
}
