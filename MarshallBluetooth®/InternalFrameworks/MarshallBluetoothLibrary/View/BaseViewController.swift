//
//  BaseViewController.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 24/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import MediaPlayer

private struct Config {
    struct LoadingView {
        static let animationDuration = TimeInterval.seconds(0.3)
        static let delayDuration = TimeInterval.seconds(0.5)
        static let alpha = CGFloat(1.0)
    }
}

class BaseViewController: UIViewController {
    private var loadingView: LoadingView?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAppearance()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isMovingFromParentViewController, let baseNavigationController = navigationController as? BaseNavigationController {
            baseNavigationController.updateNavigationBarThemeOnTopViewController()
        }
    }
    var isVisible: Bool {
        return viewIfLoaded?.window != nil
    }
    private func setupAppearance() {
        view.backgroundColor = Color.background
    }
    deinit {
        print(#function, String(describing: self))
    }
}

extension BaseViewController {
    func disableVolumeView() {
        let volumeView = MPVolumeView(frame: .zero)
        volumeView.clipsToBounds = true
        volumeView.isUserInteractionEnabled = false
        view.addSubview(volumeView)
    }
    func addLoadingView(animated: Bool = false) {
        guard loadingView == nil else { return }
        loadingView = LoadingView(frame: view.bounds)
        view.addSubview(loadingView!)
        view.bringSubview(toFront: loadingView!)
        let duration = animated ? Config.LoadingView.animationDuration : TimeInterval()
        UIView.animate(withDuration: duration, delay: TimeInterval(), options: .curveEaseInOut, animations: { [weak self] in
            self?.loadingView?.alpha = Config.LoadingView.alpha
        })
    }
    func removeLoadingView(animated: Bool = false) {
        guard loadingView != nil else { return }
        DispatchQueue.main.asyncAfter(deadline: .now() + Config.LoadingView.delayDuration) { [weak self] in
            self?.loadingView?.fadeOutRotatingImageView { _ in
                let duration = animated ? Config.LoadingView.animationDuration : TimeInterval()
                UIView.animate(withDuration: duration, delay: .zero, options: .curveEaseInOut, animations: { [weak self] in
                    self?.loadingView?.alpha = CGFloat()
                }, completion: { [weak self] _ in
                    self?.loadingView?.removeFromSuperview()
                    self?.loadingView = nil
                })
            }
        }
    }
}
