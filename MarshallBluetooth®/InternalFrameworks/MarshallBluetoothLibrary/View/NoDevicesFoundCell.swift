//
//  NoDevicesFoundCell.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 18/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

class NoDevicesFoundCell: UITableViewCell {
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var infoDescription: UILabel!

    internal override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
    
        infoLabel.resizableAttributedFont(text: UIFont.attributedMediumTitle(Strings.error_no_devices_found_title_uc()))
        infoDescription.attributedText = UIFont.attributedBodyText(Strings.error_no_devices_found_short_subtitle())
        infoDescription.isHidden = true
    }
}
