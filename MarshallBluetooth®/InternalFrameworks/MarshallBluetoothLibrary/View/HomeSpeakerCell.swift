//
//  HomeSpeakerCell.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 15/10/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//
import UIKit
import RxSwift
import RxCocoa
import Zound
import GUMA

private struct Const {
    struct SliderAnimation {
        static let duration: TimeInterval = 0.4
        static let options: UIViewAnimationOptions = .curveEaseInOut
    }
    static let edgeMargin: CGFloat = 16.0
    static let speakerImageWidth: CGFloat = 120.0
    static let speakerImageHeight: CGFloat = 75.0
    static let connectedCheckmarkWidth: CGFloat = 10.0
    static let connectedCheckmarkHeight: CGFloat = 7.0
    static let buttonBackgroundColor = UIColor(red: 174/255, green: 145/255, blue: 91/255, alpha: 1.0)
    static let connectButtonInsets = UIEdgeInsets(top: 2.0, left: 2.0, bottom: 2.0, right: 2.0)
    static let connectButtonHeight: CGFloat = 18.0
    static let statusInterItemSpacing: CGFloat = 4.0
    static let playIndicatorViewHeight: CGFloat = 14.0
    static let playIndicatorViewWidth: CGFloat = 11.0
    static let showSettingsButtonHeight: CGFloat = 45
    static let showSettingsButtonWidth: CGFloat = 60
    static let volumeIconHeight: CGFloat = 19
    static let volumeIconWidth: CGFloat = 15
    static let volumeBlockHeight: CGFloat = 20
}

struct Currents {
    var volume = 0
    var monitoredVolume = 0
    var deviceImage = UIImage()
    var speakerName = String()
    var batteryStatus = BatteryStatus.unknown
    var monitoredBatteryStatus = BatteryStatus.unknown

    mutating func update(with status: DeviceCellEntryData) {
        self.deviceImage = status.deviceImage
        self.volume = status.volume
        self.monitoredVolume = status.monitoredVolume
        self.speakerName = status.friendlyName
        self.batteryStatus = status.batteryStatus
        self.monitoredBatteryStatus = status.monitoredBatteryStatus
    }
}

precedencegroup SingleTypeComposition {
    associativity: left
}
infix operator <>: SingleTypeComposition
func <> <A: AnyObject>(f: @escaping (A) -> Void, g: @escaping (A) -> Void) -> (A) -> Void {
    return { a in
        f(a)
        g(a)
    }
}
func autolayoutStyle<V: UIView>(_ view: V) -> Void {
    view.translatesAutoresizingMaskIntoConstraints = false
}
func fullyContained<V: UIView>(in parent: V) -> (UIView) -> Void {
    return {
        NSLayoutConstraint.activate([
            $0.topAnchor.constraint(equalTo: parent.topAnchor),
            $0.bottomAnchor.constraint(equalTo: parent.bottomAnchor),
            $0.leftAnchor.constraint(equalTo: parent.leftAnchor),
            $0.rightAnchor.constraint(equalTo: parent.rightAnchor)
        ])
    }
}
func borderStyle(color: UIColor, width: CGFloat) -> (UIView) -> Void {
    return { view in
        view.layer.borderColor = color.cgColor
        view.layer.borderWidth = width
    }
}
let featuresHorizontalStackViewStyle: (UIStackView) -> Void =
autolayoutStyle
    <> {
        $0.axis = .horizontal
        $0.alignment = .center
        $0.distribution = .equalCentering
        $0.spacing = 5.0
    }
final class HomeSpeakerCell: UITableViewCell, ConfigurableCell {
    static let id = String(describing: HomeSpeakerCell.self)
    lazy var deviceImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .center
        return imageView
    }()
    lazy var deviceStatusContainerView: UIView = {
        return UIView(frame: .zero)
    }()
    lazy var speakerNameLabel: UILabel = {
        return UILabel(frame: .zero)
    }()
    lazy var statusNameLabel: UILabel = {
       return UILabel()
    }()
    lazy var auxModeLabel: UILabel = {
       return UILabel()
    }()
    lazy var showSettingsButton: UIButton = {
        let button = UIButton()
        button.contentMode = .center
        button.setImage(UIImage.deviceSettingsIcon, for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.addTarget(self, action: #selector(showSettingsTriggered(_:)), for: .touchUpInside)
        return button
    }()
    lazy var showPlayerButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(showPlayerTriggered(_:)), for: .touchUpInside)
        return button
    }()
    /// Dynamic cell parts
    lazy var connectedCheckmark: UIImageView = {
        return UIImageView(image: UIImage.checkmarkMarshall)
    }()
    lazy var connectionStatusLabel: UILabel = {
        return UILabel(frame: .zero)
    }()
    lazy var connectButton: UIButton = {
        let button =  UIButton(frame: .zero)
        button.setAttributedTitle(UIFont.attributedSecondaryButtonLabel(Strings.appwide_connect_uc()), for: .normal)
        button.backgroundColor = Const.buttonBackgroundColor
        button.contentEdgeInsets = Const.connectButtonInsets
        button.addTarget(self, action: #selector(connectDevice(_:)), for: .touchUpInside)
        return button
    }()
    lazy var decoupleButton: UIButton = {
        let button =  UIButton(frame: .zero)
        button.setAttributedTitle(UIFont.attributedSecondaryButtonLabel(Strings.ios_couple_screen_decouple_uc()), for: .normal)
        button.backgroundColor = Const.buttonBackgroundColor
        button.contentEdgeInsets = Const.connectButtonInsets
        button.addTarget(self, action: #selector(decouple(_:)), for: .touchUpInside)
        return button
    }()
    lazy var playIndicatorView: PlayIndicatorView = {
        let playIndicatorView = PlayIndicatorView()
        playIndicatorView.frame = CGRect(
            origin: .zero,
            size: CGSize(width: Const.playIndicatorViewWidth,
                         height: Const.playIndicatorViewHeight))
        playIndicatorView.setNeedsLayout()
        return playIndicatorView
    }()
    lazy var connectingIndicatorView: UIActivityIndicatorView = {
        let indicatorView = UIActivityIndicatorView(activityIndicatorStyle: .white)
        return indicatorView
    }()
    lazy var volumeIconView: UIImageView = {
        return UIImageView(image: UIImage.volumeIcon)
    }()
    lazy var volumeSlider: UISlider = {
        let volumeSlider = UISlider(frame: .zero)
        volumeSlider.setThumbImage(UIImage.volumeThumb, for: .normal)
        volumeSlider.addTarget(self, action: #selector(onVolumeSliderChange(_:)), for: .valueChanged)
        volumeSlider.addTarget(self, action: #selector(onVolumeSliderTouchUpInside(_:)), for: .touchUpInside)
        volumeSlider.minimumTrackTintColor = Color.volumeSliderMin
        volumeSlider.maximumTrackTintColor = Color.volumeSliderMax
        guard let viewModel = viewModel else {
            return volumeSlider
        }
        volumeSlider.minimumValue = Float(viewModel.outputs.platformTraits.volumeMin)
        volumeSlider.maximumValue = Float(viewModel.outputs.platformTraits.volumeMax)
        return volumeSlider
    }()
    lazy var batteryStatusView: DevicesListBatteryStatusView = {
        return DevicesListBatteryStatusView(frame: .zero)
    }()
    private lazy var buttonSegmentsControl: ButtonSegmentsControl = {
        let buttonSegmentsControl = ButtonSegmentsControl(buttonSegments: ButtonSegmentsControl.buttonSegments(for: viewModel?.outputs.hardwareType))
        buttonSegmentsControl.addTarget(self, action: #selector(onButtonSegmentsControlTouchUpInside(_:)), for: .touchUpInside)
        buttonSegmentsControl.set(enabled: false)
        return buttonSegmentsControl
    }()
    @objc func onVolumeSliderChange(_ sender: UISlider!) {
        volume = Int(sender.value)
    }
    @objc func onVolumeSliderTouchUpInside(_ sender: UISlider!) {
        viewModel?.inputs.logVolumeChangeAnalyticsEvent(with: volume)
    }
    @objc func onButtonSegmentsControlTouchUpInside(_ sender: ButtonSegmentsControl!) {
        guard let index = sender.lastTouchedUpInsideTag else { return }
        viewModel?.inputs.buttonSegmentTouchedUpInside(for: ButtonSegmentsControl.feature(for: index, and: viewModel?.outputs.hardwareType))
    }
    @objc func connectDevice(_ sender: Any) {
        guard currentStatus.connected == true else {
            viewModel?.inputs.connectDevice()
            return
        }
    }
    @objc func showPlayerTriggered(_ sender: UIButton) {
        viewModel?.inputs.showPlayer()
    }
    @objc func showSettingsTriggered(_ sender: Any) {
        viewModel?.inputs.showSettings()
    }
    @objc func decouple(_ sender: Any) {
        viewModel?.inputs.decouple()
    }
    var viewModel: HomeDeviceCellViewModelType?
    
    var volume: Int = Int() {
        didSet {
            guard oldValue != volume else { return }
            viewModel?.inputs.change(volume: volume)
        }
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        self.currentStatus = DeviceCellEntryData()
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
    }
    required init?(coder: NSCoder) {
        fatalError()
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        contentView.subviews.forEach { $0.removeFromSuperview() }
        initialSetup()
    }
    var currentStatus: DeviceCellEntryData {
        didSet {
            self.update(status: currentStatus)
        }
    }
    private func update(status: DeviceCellEntryData) {
        if currents.speakerName != status.friendlyName {
            speakerNameLabel.attributedText = UIFont.attributedListDeviceNameLabel(status.friendlyName, connected: true)
        }
        if currents.deviceImage != status.deviceImage {
            deviceImageView.image = status.deviceImage
        }
        if currents.volume != status.volume {
            self.updateSlider(to: status.volume, animated: false)
        }
        if currents.monitoredVolume != status.monitoredVolume {
            self.updateSlider(to: status.monitoredVolume, animated: true)
        }
        connectButton.isHidden = !status.visibility.connectivityButton
        connectButton.setAttributedTitle(UIFont.attributedSecondaryButtonLabel(status.connectivityButtonLabelText), for: .normal)
        connectedCheckmark.isHidden = !status.visibility.connectedCheckmark
        connectionStatusLabel.isHidden = !status.visibility.statusLabel
        connectionStatusLabel.attributedText = UIFont.attributedListDeviceConnectionStatus(status.statusLabelText, connected: status.connected)
        auxModeLabel.isHidden = !status.visibility.auxMode
        auxModeLabel.attributedText = UIFont.attributedListDeviceConnectionStatus(Strings.speaker_status_aux(), connected: status.connected)
        setupVolumeBlock(status.visibility.mediaPart)
        playIndicatorView.isHidden = !status.visibility.mediaPart
        status.visibility.mediaPart ? playIndicatorView.startAnimation() : playIndicatorView.stopRotation()
        decoupleButton.isHidden = !status.visibility.decoupleButton
        showSettingsButton.imageView?.image = status.updateAvailable ? UIImage.deviceSettingsIconUpdate : UIImage.deviceSettingsIcon
        batteryStatusView.isHidden = !status.visibility.batteryBlock
        batteryStatusView.batteryStatus = status.batteryStatus
        setupConnecting(status.visibility.deviceConnectingActivityIndicator)
        currents.update(with: status)
        buttonSegmentsControl.set(enabled: status.connected)
    }
    internal func configureWith(value: HomeDeviceCellViewModelType) {
        viewModel = value
        setupContainerViewContstraints()
        setupConstraints()
    }
    private func setupContainerViewContstraints() {
        let statusIndicatorStackView = UIStackView(arrangedSubviews: [
            batteryStatusView,
            connectedCheckmark,
            connectionStatusLabel,
            auxModeLabel,
            connectButton,
            decoupleButton,
            playIndicatorView,
            connectingIndicatorView
        ])
        featuresHorizontalStackViewStyle(statusIndicatorStackView)
        statusIndicatorStackView.layoutMargins = UIEdgeInsets.zero
        
        [speakerNameLabel, statusIndicatorStackView, showSettingsButton].forEach {
            autolayoutStyle($0)
            deviceStatusContainerView.addSubview($0)
        }
        NSLayoutConstraint.activate([
            speakerNameLabel.topAnchor.constraint(equalTo: deviceStatusContainerView.topAnchor, constant: Const.edgeMargin),
            speakerNameLabel.leadingAnchor.constraint(equalTo: deviceStatusContainerView.leadingAnchor),
            speakerNameLabel.rightAnchor.constraint(equalTo: showSettingsButton.leftAnchor, constant: -Const.edgeMargin),
            connectedCheckmark.widthAnchor.constraint(equalToConstant: Const.connectedCheckmarkWidth),
            connectedCheckmark.heightAnchor.constraint(equalToConstant: Const.connectedCheckmarkHeight),
            statusIndicatorStackView.topAnchor.constraint(equalTo: speakerNameLabel.bottomAnchor, constant: Const.statusInterItemSpacing),
            statusIndicatorStackView.leadingAnchor.constraint(equalTo: deviceStatusContainerView.leadingAnchor),
            statusIndicatorStackView.bottomAnchor.constraint(equalTo: deviceStatusContainerView.bottomAnchor, constant: -Const.edgeMargin),
            statusIndicatorStackView.heightAnchor.constraint(equalToConstant: Const.connectButtonHeight),
            playIndicatorView.heightAnchor.constraint(equalToConstant: Const.playIndicatorViewHeight),
            playIndicatorView.widthAnchor.constraint(equalToConstant: Const.playIndicatorViewWidth),
            showSettingsButton.centerYAnchor.constraint(equalTo: speakerNameLabel.centerYAnchor),
            showSettingsButton.rightAnchor.constraint(equalTo: deviceStatusContainerView.rightAnchor),
            showSettingsButton.widthAnchor.constraint(equalToConstant: Const.showSettingsButtonWidth),
            showSettingsButton.heightAnchor.constraint(equalToConstant: Const.showSettingsButtonHeight)
        ])
    }
    private func setupConstraints() {
        let volumeIndicatorStackView = UIStackView(arrangedSubviews: [volumeIconView, volumeSlider])
        featuresHorizontalStackViewStyle(volumeIndicatorStackView)
        volumeIndicatorStackView.distribution = .fillProportionally
        volumeIndicatorStackView.spacing = Const.edgeMargin
        volumeIndicatorStackView.layoutMargins = UIEdgeInsets.zero
        volumeBlockHiddenConstraint = volumeIndicatorStackView.heightAnchor.constraint(equalToConstant: 0)
        volumeBlockHiddenConstraint.priority = .required
        volumeBlockVisibleConstraint = volumeIndicatorStackView.heightAnchor.constraint(equalToConstant: Const.volumeBlockHeight)
        var volumeBlockConstraint = NSLayoutConstraint()
        volumeBlockConstraint = currentStatus.visibility.mediaPart ?
            volumeBlockVisibleConstraint : volumeBlockHiddenConstraint
        setupVolumeBlock(currentStatus.visibility.mediaPart)
        
        [deviceImageView, showPlayerButton, deviceStatusContainerView, volumeIndicatorStackView, buttonSegmentsControl].forEach {
            autolayoutStyle($0)
            contentView.addSubview($0)
        }
        let volumeIconViewHeightConstraint = volumeIconView.heightAnchor.constraint(equalToConstant: Const.volumeIconHeight)
        volumeIconViewHeightConstraint.priority = .defaultHigh
        let deviceViewHeightConstranit = deviceImageView.heightAnchor.constraint(equalToConstant: Const.speakerImageHeight)
        deviceViewHeightConstranit.priority = .defaultHigh

        NSLayoutConstraint.activate([
            deviceImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Const.edgeMargin),
            deviceImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Const.edgeMargin),
            deviceImageView.widthAnchor.constraint(equalToConstant: Const.speakerImageWidth),
            deviceViewHeightConstranit,
            showPlayerButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Const.edgeMargin),
            showPlayerButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Const.edgeMargin),
            showPlayerButton.widthAnchor.constraint(equalToConstant: Const.speakerImageWidth),
            showPlayerButton.heightAnchor.constraint(equalToConstant: Const.speakerImageHeight),
            deviceStatusContainerView.centerYAnchor.constraint(equalTo: deviceImageView.centerYAnchor),
            deviceStatusContainerView.leftAnchor.constraint(equalTo: deviceImageView.rightAnchor),
            deviceStatusContainerView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -Const.edgeMargin),
            volumeIndicatorStackView.topAnchor.constraint(equalTo: deviceImageView.bottomAnchor, constant: Const.edgeMargin),
            volumeIndicatorStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Const.edgeMargin * 2),
            volumeIndicatorStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Const.edgeMargin * 2),
            volumeIconView.widthAnchor.constraint(equalToConstant: Const.volumeIconWidth),
            volumeIconViewHeightConstraint,
            buttonSegmentsControl.topAnchor.constraint(equalTo: volumeIndicatorStackView.bottomAnchor, constant: Const.edgeMargin / 2),
            buttonSegmentsControl.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Const.edgeMargin),
            buttonSegmentsControl.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Const.edgeMargin),
            buttonSegmentsControl.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    private var currents = Currents()
    private var volumeBlockHiddenConstraint = NSLayoutConstraint()
    private var volumeBlockVisibleConstraint = NSLayoutConstraint()
    private var disposeBag = DisposeBag()
}
private extension HomeSpeakerCell {
    func updateSlider(to volume: Int, animated: Bool) {
        volumeSlider.isUserInteractionEnabled = false
        let animations: () -> Void = { [weak self] in
            self?.volumeSlider.setValue(Float(volume), animated: true)
        }
        let completionBlock: (Bool) -> Void = { [weak self] _ in
            self?.volumeSlider.isUserInteractionEnabled = true
        }
        UIView.animate(withDuration: animated ? Const.SliderAnimation.duration : TimeInterval(),
                       delay: TimeInterval(),
                       options: Const.SliderAnimation.options,
                       animations: animations,
                       completion: completionBlock)
    }
    func setupConnecting(_ inProgress: Bool) {
        connectingIndicatorView.isHidden = !inProgress
        inProgress ? connectingIndicatorView.startAnimating() : connectingIndicatorView.stopAnimating()
    }
    func setupVolumeBlock(_ active: Bool) {
        guard active else {
            volumeIconView.isHidden = true
            volumeSlider.isHidden = true
            NSLayoutConstraint.deactivate([volumeBlockVisibleConstraint])
            NSLayoutConstraint.activate([volumeBlockHiddenConstraint])
            return
        }
        volumeIconView.isHidden = false
        volumeSlider.isHidden = false
        NSLayoutConstraint.deactivate([volumeBlockHiddenConstraint])
        NSLayoutConstraint.activate([volumeBlockVisibleConstraint])

    }
    func initialSetup() {
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        if let _ = viewModel {
            viewModel = nil
        }
        disposeBag = DisposeBag()
        setupVolumeBlock(false)
        showSettingsButton.imageView?.image = UIImage.deviceSettingsIcon
    }
}
private struct LabelAnimationConfig {
    static let duration = TimeInterval.seconds(0.4)
    static let options: UIViewAnimationOptions = [.curveEaseInOut, .transitionCrossDissolve]
}
extension HomeSpeakerCell {
    func indicateCouple() {
        func updateNameLabel(text: String, color: UIColor, completion: @escaping (Bool) -> Void) {
            let hideAnimation: () -> Void = { [weak self] in self?.speakerNameLabel.attributedText = NSAttributedString() }
            let showAnimation: () -> Void = { [weak self] in self?.speakerNameLabel.attributedText = UIFont.attributedH3(text, color: color) }
            UIView.transition(with: speakerNameLabel,
                              duration: LabelAnimationConfig.duration,
                              options: LabelAnimationConfig.options,
                              animations: hideAnimation) { [weak self] _ in
                guard let strongSelf = self else { return }
                UIView.transition(with: strongSelf.speakerNameLabel,
                                  duration: LabelAnimationConfig.duration,
                                  options: LabelAnimationConfig.options,
                                  animations: showAnimation,
                                  completion: completion)
            }
        }
        guard let text = speakerNameLabel.text else { return }
        updateNameLabel(text: text, color: UIColor.darkBeige) { _ in
                updateNameLabel(text: text, color: UIColor.white, completion: { _ in })
        }
    }
}

private extension ButtonSegmentsControl {
    static func buttonSegments(for hardwareType: HardwareType?) -> [ButtonSegment] {
        switch hardwareType {
        case .some(.headset):
            return [
                ButtonSegment(image: UIImage.ButtonSegment.eq, attributedString: UIFont.attributedSmallerText("EQ", color: UIColor.mediumGrey2)),
                ButtonSegment(image: UIImage.ButtonSegment.anc, attributedString: UIFont.attributedSmallerText("ANC", color: UIColor.mediumGrey2))
            ]
        case .some(.speaker):
            return [] /*[
                ButtonSegment(image: UIImage.ButtonSegment.eq, attributedString: UIFont.attributedSmallerText("EQ", color: UIColor.mediumGrey2)),
                ButtonSegment(image: UIImage.ButtonSegment.couple, attributedString: UIFont.attributedSmallerText("COUPLE", color: UIColor.mediumGrey2))
            ]*/
        case .none:
            return []
        }
    }
    static func feature(for index: Int, and hadrwareType: HardwareType?) -> Feature? {
        switch (index, hadrwareType) {
        case (0, .some(.headset)):
            return .hasEqualizerSettings
        case (1, .some(.headset)):
            return .hasActiveNoiceCancellingSettings
        case (0, .some(.speaker)):
            return .hasGraphicalEqualizer
        case (1, .some(.speaker)):
            return .hasTrueWireless
        default:
            return nil
        }
    }
}
