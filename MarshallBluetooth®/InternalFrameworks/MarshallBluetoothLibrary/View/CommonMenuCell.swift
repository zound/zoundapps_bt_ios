//
//  CommonMenuCell.swift
//  MarshallBluetooth®
//
//  Created by Paprota Przemyslaw on 10/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

class CommonMenuCell: UITableViewCell {

    var viewModel: CommonMenuCellViewModel?

    private var disposeBag = DisposeBag()

    internal override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        self.textLabel?.lineBreakMode = .byWordWrapping
        self.textLabel?.numberOfLines = 0
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        textLabel?.textColor = highlighted ? Color.textGrayedOut : Color.text
    }

    internal func configureWith(viewModel: CommonMenuCellViewModel) {
        self.viewModel = viewModel

        if let viewModel = self.viewModel {

            Observable.combineLatest(viewModel.outputs.name, viewModel.outputs.enabled)
                .asObservable()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { [weak self] (name, enabled) in
                    let text = enabled ? UIFont.attributedSubMenuText(name) : UIFont.attributeDisabledSubMenuText(name)
                    self?.textLabel?.attributedText = text
                }).disposed(by: disposeBag)

            let enabled = viewModel.outputs.enabled.share()

            enabled
                .observeOn(MainScheduler.instance)
                .bind(to: self.rx.isUserInteractionEnabled)
                .disposed(by: disposeBag)
            
            enabled
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { [weak self] enabled in
                    self?.textLabel?.isEnabled = enabled
                }).disposed(by: disposeBag)
            
            enabled
                .observeOn(MainScheduler.instance)
                .map { enabled -> UIImageView in
                    // change accessory view (arrow on the right side)
                    let arrow = UIImageView(image: UIImage.forwardArrow)
                    arrow.image = arrow.image?.withRenderingMode(.alwaysTemplate)
                    arrow.tintColor = enabled ? Color.text : Color.textGrayedOut
                    return arrow
                }.subscribe(onNext: { [weak self] arrow in
                    self?.accessoryView = arrow
                }).disposed(by: disposeBag)
        }
    }
}
