//
//  InformationCloud.swift
//  MarshallBluetooth®
//
//  Created by Paprota Przemyslaw on 09/03/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit

class InformationCloud: UIView {

    let trinangleSize: CGFloat = 12
    let fillColor = UIColor.warmGrey.cgColor
    override func layoutSubviews() {
        super.layoutSubviews()
        addInnerShadow()
    }
    private func addInnerShadow() {
        let rectWidth = frame.width - trinangleSize
        let roundedRect = CAShapeLayer()
        roundedRect.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: rectWidth, height: frame.height), cornerRadius: 10).cgPath
        roundedRect.fillColor = fillColor
        layer.addSublayer(roundedRect)
        let edge = trinangleSize * 4.0 / sqrt(3)
        let A = CGPoint(x: rectWidth + trinangleSize, y: frame.height / 2.0)
        let B = CGPoint(x: rectWidth, y: (frame.height - edge) / 2.0)
        let C = CGPoint(x: rectWidth, y: (frame.height + edge) / 2.0)
        let triangle = CAShapeLayer()
        let path = UIBezierPath()
        path.move(to: A)
        path.addLine(to: B)
        path.addLine(to: C)
        path.addLine(to: A)
        path.close()
        path.fill()
        triangle.path = path.cgPath
        triangle.fillColor = fillColor
        layer.addSublayer(triangle)
    }

}

