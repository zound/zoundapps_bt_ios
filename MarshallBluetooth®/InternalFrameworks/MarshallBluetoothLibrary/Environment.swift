//
//  Environment.swift
//  MarshallBluetoothLibrary
//

// It's heavily inspired by the Kickstarter iOS app
// https://github.com/kickstarter/ios-oss



///Groups global objects and properties used for various tasks
public struct Environment {
    /// speaker model names
    public static let acton = "ACTON II"
    public static let monitor = "MONITOR II A.N.C."
    public static let stanmore = "STANMORE II"
    public static let woburn = "WOBURN II"
    
    /// The user's current language, which determines which localized strings bundle to load
    public let language: Language
    
    public init(language: Language = Language(languageStrings: Locale.preferredLanguages) ?? Language.en) {
        self.language = language
    }
    
}
