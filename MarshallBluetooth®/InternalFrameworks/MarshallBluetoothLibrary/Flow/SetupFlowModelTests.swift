//
//  SetupFlowModelTests.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 25/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

@testable import MarshallBluetoothLibrary
import XCTest
import GUMA

class SetupProgressTest: XCTestCase {
    
    func testProperConnectionProgress() {
        let testElement: SetupProgressInfo = .initializingConnection
        XCTAssertTrue(testElement.entry.maxValue == 0)
        let testElement2: SetupProgressInfo = .connected
        XCTAssertTrue(testElement2.entry.maxValue == 20)
    }
    
    func testProperDownloadProgress() {
        let downloadOTAUpdateInfo = OTAUpdateInfo(deviceID: "", status: .downloadingFirmware(0.2, nil), error: nil)
        let testElement = SetupProgressInfo(otaStatus: downloadOTAUpdateInfo)
        XCTAssertTrue(testElement.entry.maxValue == 26)
        
        let downloadOTAUpdateInfo2 = OTAUpdateInfo(deviceID: "", status: .downloadingFirmware(0.8, nil), error: nil)
        let testElement2 = SetupProgressInfo(otaStatus: downloadOTAUpdateInfo2)
        XCTAssertTrue(testElement2.entry.maxValue == 44)
        
        let downloadOTAUpdateInfo3 = OTAUpdateInfo(deviceID: "", status: .downloadingFirmware(1.0, nil), error: nil)
        let testElement3 = SetupProgressInfo(otaStatus: downloadOTAUpdateInfo3)
        XCTAssertTrue(testElement3.entry.maxValue == 50)
    }
    
    func testProperDeviceUploadProgress() {
        
        let uploadToDeviceInfo = OTAUpdateInfo(deviceID: "", status: .uploadingToDevice(0.2), error: nil)
        let testElement = SetupProgressInfo(otaStatus: uploadToDeviceInfo)
        XCTAssertTrue(testElement.entry.maxValue == 56)
        
        let uploadToDeviceInfo2 = OTAUpdateInfo(deviceID: "", status: .uploadingToDevice(0.8), error: nil)
        let testElement2 = SetupProgressInfo(otaStatus: uploadToDeviceInfo2)
        XCTAssertTrue(testElement2.entry.maxValue == 74)
        
        let uploadToDeviceInfo3 = OTAUpdateInfo(deviceID: "", status: .uploadingToDevice(1.0), error: nil)
        let testElement3 = SetupProgressInfo(otaStatus: uploadToDeviceInfo3)
        XCTAssertTrue(testElement3.entry.maxValue == 80)
    }
}
