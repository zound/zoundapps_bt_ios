//
//  SetupFlowModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 30/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA
import RCER

public enum SetupState {
    case displayInfoScreen
    case displayAdvertisement
    case showOnboardingInfo
    case doneSetup
    case completed
    
    /// Manages flow of the screens while setup
    ///
    /// Switching of the screens is related to the progress of the setup.
    /// Screens with advertisements are automatically replaced with *Setup finished* screen after the successful setup notification
    ///
    /// - Returns: Next setup screen to be displayed based on current one
    func next() -> SetupState {
        switch self {
        case .displayInfoScreen: return .displayAdvertisement
        case .displayAdvertisement: return .showOnboardingInfo
        case .showOnboardingInfo: return .doneSetup
        case .doneSetup: return .completed
        default: return .displayAdvertisement
        }
    }
}

/// Inputs of setup flow model
protocol SetupFlowModelInputs {
    func perform()
}

/// Outputs of setup flow model
protocol SetupFlowModelOutputs {
    /// Pushes view controller into navigation bar
    var setViewController: BehaviorRelay<UIViewController?> { get }
    var setupProgress: BehaviorRelay<SetupProgressInfo> { get }
}

protocol SetupFlowModelType {
    var inputs: SetupFlowModelInputs { get }
    var outputs: SetupFlowModelOutputs { get }
}

/// Implementation of setup flow model
final class SetupFlowModel: BaseContextualErrorReporter<OTAStatus, OTAError>,
                            FlowModel,
                            SetupFlowModelInputs,
                            SetupFlowModelOutputs,
                            SetupFlowModelType  {
    
    /// Id that identificates this flow among other in apps
    static var id: FlowId = .deviceSetup
    
    public var setViewController = BehaviorRelay<UIViewController?>.init(value: nil)
    public var pushViewController = PublishSubject<UIViewController>()
    
    var submodels = [FlowModel]()
    var setupProgress = BehaviorRelay<SetupProgressInfo>.init(value: .initializingConnection)
    
    var popViewController = PublishSubject<Void>()
    
    var presentViewController = PublishSubject<UIViewController>()
    var dismissViewController = PublishSubject<UIViewController>()
    
    var preferences: DefaultStorage = DefaultStorage.shared
    
    var completed: PublishSubject<FlowId> = PublishSubject<FlowId>()
    
    var inputs: SetupFlowModelInputs { return self }
    var outputs: SetupFlowModelOutputs { return self }
    
    typealias Dependencies = OTAServiceDependency &
                             DeviceAdapterServiceDependency &
                             AnalyticsServiceDependency &
                             AppModeDependency
    
    init(dependencies: Dependencies, device: DeviceType) {
        self.dependencies = dependencies
        self.lastKnownOTAStatus = .completed
        self.deviceID = device.id
        self.deviceImage = device.image.largeImage
        self.platform = device.platform
        
        super.init()
        
        current
            .observeOn(MainScheduler.instance)
            .subscribeNext(weak: self, SetupFlowModel.handleState)
            .disposed(by: disposeBag)
    }
    
    func perform() {
        current.accept(setupState)
    }
    
    deinit {
        print("🚾 \(self) deinited 🚾")
    }
    
    func handleError(_ error: AnyError<OTAError>) {
        if error.id == .otherError {
            dismissError { [weak self] in
                self?.showError(parentViewController: self?.setViewController.value, error.id)
            }
            return
        }

        dismissError { [weak self] in
            self?.showError(parentViewController: self?.setViewController.value, error.id)
        }
    }
    
    public override func viewModel(for errorType: OTAError) -> ErrorViewModel? {
        switch errorType {
        case .internetNotAvailable:
            let errorViewModel = ErrorViewModel(errorInfo: NoInternetOTAErrorInfo(), dependencies: dependencies)
            errorViewModel.outputs.errorInfo.action(type: .tryAgain) { [weak self] _ in
                self?.dismissError { }
            }
            errorViewModel.outputs.errorInfo.action(type: .skip) { [weak self] _ in
                self?.dismissError { }
            }
            return errorViewModel
        case .bluetoothOff:
            let errorViewModel = ErrorViewModel(errorInfo: BluetoothErrorInfo(), dependencies: dependencies)
            errorViewModel.outputs.errorInfo.action(type: .gotoSettings) { _ in
                AppEnvironment.goToSettings()
            }
            return errorViewModel
        case .deviceDisconnected:
            let errorViewModel = ErrorViewModel(errorInfo: NoDevicesFoundErrorInfo(), dependencies: dependencies)
            errorViewModel.outputs.errorInfo.action(type: .tryAgain) { [weak self] _ in
                self?.dismissError { }
            }
            errorViewModel.outputs.errorInfo.action(type: .skip) { [weak self] _ in
                self?.dismissError { }
            }
            return errorViewModel
        case .otherError:
            let errorViewModel = ErrorViewModel(errorInfo: SomethingWentWrongErrorInfo(), dependencies: dependencies)
            errorViewModel.outputs.errorInfo.action(type: .tryAgain) { [weak self] _ in
                self?.dismissError { }
            }
            return errorViewModel
            
        default: return nil
        }
    }

    private let dependencies: Dependencies
    private var setupState: SetupState = .displayInfoScreen
    private var current = PublishRelay<SetupState>.init()
    private var disposeBag = DisposeBag()
    private let deviceID: String
    private let deviceImage: UIImage
    private let platform: Platform
    private var lastKnownOTAStatus: OTAStatus = .completed
    private var connectionDisposeBag = DisposeBag()
}

private extension SetupFlowModel {
    func handleState(_ state: SetupState) {
        switch state {
        case .displayInfoScreen:
            showUpdateInfoScreen()
        case .displayAdvertisement:
            showAdvertisement(deviceID: deviceID)
        case .showOnboardingInfo:
            showOnboardingInfo()
        case .doneSetup:
            self.showSpeakerSetupDone()
        case .completed:
            self.completed.onNext(SetupFlowModel.id)
        }
    }
    func showUpdateInfoScreen() {
        let otaUpdateAvailableViewController = UIViewController.instantiate(OTAUpdateAvailableViewController.self)
        let otaUpdateAvailableViewModel: OTAUpdateAvailableViewModelType = OTAUpdateAvailableViewModel()
        
        otaUpdateAvailableViewModel.outputs.continued
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.setupState = strongSelf.setupState.next()
                strongSelf.current.accept(strongSelf.setupState)
            })
            .disposed(by: disposeBag)
        
        otaUpdateAvailableViewController.viewModel = otaUpdateAvailableViewModel
        setViewController.accept(otaUpdateAvailableViewController)
    }
    func showAdvertisement(deviceID: String) {
        let advertisementController = UIViewController.instantiate(JoplinAdvertisementViewController.self)
        advertisementController.configure(withAdvertisementControllers: AdvertisementPage.viewControllers(for: platform))
        let advertisementViewModel: JoplinAdvertisementViewModelType = JoplinAdvertisementViewModel(progress: setupProgress.asObservable())
        advertisementController.viewModel = advertisementViewModel
        
        dependencies.analyticsService.inputs.log(.appForcedOtaStarted)
        
        advertisementViewModel.outputs.viewReady
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.performOTA()
            })
            .disposed(by: disposeBag)
        
        dependencies.otaService.outputs.rediscovered
            .subscribe(onNext: { [weak self] device in
                self?.dismissError { }
            })
            .disposed(by: disposeBag)

        setViewController.accept(advertisementController)
    }
    func showOnboardingInfo() {
        let pairingInfoViewController = UIViewController.instantiate(OTAOnboardingViewController.self)
        let pairingInfoViewModel: OTAOnboardingViewModelType = OTAOnboardingViewModel()
        
        pairingInfoViewModel.outputs.viewCompleted
            .observeOn(MainScheduler.instance)
            .subscribe(
                onCompleted: { [weak self] in
                    guard let strongSelf = self else { return }
                    strongSelf.setupState = strongSelf.setupState.next()
                    strongSelf.current.accept(strongSelf.setupState)
                }
            )
            .disposed(by: disposeBag)
        
        pairingInfoViewController.viewModel = pairingInfoViewModel
        setViewController.accept(pairingInfoViewController)
    }
    func showSpeakerSetupDone() {
        
        let otaDoneViewController = UIViewController.instantiate(OTAFinishedViewController.self)
        let otaDoneViewModel = OTAFinishedViewModel(image: self.deviceImage)
        
        otaDoneViewModel.outputs.done
            .asDriver()
            .drive(onNext: { [weak self] success in
                guard success else { return }
                guard let strongSelf = self else { return }
                strongSelf.setupState = strongSelf.setupState.next()
                strongSelf.current.accept(strongSelf.setupState)
            })
            .disposed(by: disposeBag)
        
        otaDoneViewController.viewModel = otaDoneViewModel
        setViewController.accept(otaDoneViewController)
    }
    func performOTA() {
        
        let initialState = OTAUpdateInfo(deviceID: deviceID, status: .connecting, error: nil)
        setupProgress.accept(SetupProgressInfo(otaStatus: initialState))
        lastKnownOTAStatus = .connecting
        
        connectionDisposeBag = DisposeBag()
        dependencies.otaService.inputs.resetProgress()
        
        /// Prepare to process OTA progress report from OTA service
        Observable.combineLatest(
                Observable.just(deviceID),
                dependencies.otaService.outputs.updateProgressInfo
            )
            .observeOn(MainScheduler.instance)
            .flatMap(strongify(self, Observable.empty(), SetupFlowModel.extractOTAUpdateInfo))
            .flatMap(strongify(self, Observable.empty(), SetupFlowModel.prepareProgressInfo))
            .subscribe(weak: self,
                       onNext: SetupFlowModel.handleSetupProgressInfo,
                       onError: SetupFlowModel.handleOTAError
            )
            .disposed(by: connectionDisposeBag)
        
        dependencies.otaService.inputs.startUpdate(deviceID: deviceID, interfaceError: dependencies.deviceAdapterService.outputs.error)
    }
    
    func restartOTA() {
        connectionDisposeBag = DisposeBag()
    }
    
    func extractOTAUpdateInfo(_ deviceID: String, _ allUpdatesInfo: [OTAUpdateInfo]) -> Observable<OTAUpdateInfo> {
        guard let info = allUpdatesInfo.first(where: { $0.deviceID == deviceID }) else {
            return Observable.empty()
        }
        return Observable.just(info)
    }
    
    func prepareProgressInfo(_ info: OTAUpdateInfo) -> Observable<SetupProgressInfo> {
        return Observable.just(SetupProgressInfo.init(otaStatus: info))
    }
    
    func handleSetupProgressInfo(_ info: SetupProgressInfo) {

        self.setupProgress.accept(info)
        
        switch info {
        case .otaProgress(let status):
            switch status {
            case .connecting:
                if case .connecting = lastKnownOTAStatus {
                    return
                }
                lastKnownOTAStatus = status
            case .checkingFirmware:
                if case .checkingFirmware = lastKnownOTAStatus {
                    return
                }
                lastKnownOTAStatus = status
            case .downloadingFirmware:
                if case .downloadingFirmware = lastKnownOTAStatus {
                    return
                }
                lastKnownOTAStatus = status
            case .uploadingToDevice:
                if case .uploadingToDevice = lastKnownOTAStatus {
                    return
                }
                lastKnownOTAStatus = status
            case .uploadCompleted:
                if case .uploadCompleted = lastKnownOTAStatus {
                    return
                }
                lastKnownOTAStatus = status
            
            default: break
        }
        default: break
        }
        
        guard case .otaProgress(let status) = info, case .completed = status else {
            return
        }
        dependencies.analyticsService.inputs.log(AnalyticsEvent.appOtaCompleted)
        setupState = setupState.next()
        current.accept(setupState)
    }
    
    func handleOTAError(_ error: Error) {
        guard let error = error as? AnyError<OTAError> else {
            dependencies.analyticsService.inputs.log(.appOtaFailed(.otherError))
            handleError(AnyError<OTAError>.init(id: OTAError.otherError))
            return
        }
        switch error.id {
        case .bluetoothOff:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.bluetoothOff))
        case .deviceDisconnected:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.deviceDisconnected))
        case .deviceInOTANotFoundInManagedDevicesList:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.deviceInOTANotFoundInManagedDevicesList))
        case .emptyLocalOrRemoteFirmwareVersion:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.emptyLocalOrRemoteFirmwareVersion))
        case .errorParsingRemoteURL:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.errorParsingRemoteURL))
        case .failedToCalculateMD5Checksum:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.failedToCalculateMD5Checksum))
        case .failedToCreateUnzippedFolder:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.failedToCreateUnzippedFolder))
        case .failedToUnzipFirmwareFiles:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.failedToUnzipFirmwareFiles))
        case .internetNotAvailable:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.internetNotAvailable))
        case .md5checksumMismatch:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.md5checksumMismatch))
        case .otherError:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.otherError))
        case .unableToCreateDownloadURL:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.unableToCreateDownloadURL))
        case .unknownDeviceType:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.unknownDeviceType))
        case .unknownRemoteDownloadURL:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.unknownRemoteDownloadURL))
        case .unknownRemoteFwVersion:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.unknownRemoteFwVersion))
        case .unknownRemoteVersion:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.unknownRemoteVersion))
        case .urlSessionError(_):
            dependencies.analyticsService.inputs.log(.appOtaFailed(.urlSessionError))
        }
        handleError(error)
    }
}
