//
//  DeviceSettingsFlowModel.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 16/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA
import RCER

protocol DeviceSettingsFlowModelInput {
    func pushDeviceSettings()
}

protocol DeviceSettingsFlowModelOutput {
    var popToDeviceSettingsViewController: PublishSubject<Void> { get }
    var popOutsideDeviceSettingsViewController: PublishSubject<Void> { get }
    var setupProgress: BehaviorRelay<SetupProgressInfo> { get }
}

protocol DeviceSettingsFlowModelType {
    var inputs: DeviceSettingsFlowModelInput { get }
    var outputs: DeviceSettingsFlowModelOutput { get }
}

final class DeviceSettingsFlowModel: BaseContextualErrorReporter<OTAStatus, OTAError>,
                                     FlowModel {
    typealias Dependencies = DeviceAdapterServiceDependency &
                             DeviceServiceDependency &
                             AnalyticsServiceDependency &
                             OTAServiceDependency &
                             AppModeDependency
    
    static var id: FlowId = .deviceSettings
    var submodels: [FlowModel] = []
    var setViewController = BehaviorRelay<UIViewController?>.init(value: nil)
    var pushViewController = PublishSubject<UIViewController>()
    var popViewController = PublishSubject<Void>()
    var presentViewController = PublishSubject<UIViewController>()
    var dismissViewController = PublishSubject<UIViewController>()
    var popToDeviceSettingsViewController =  PublishSubject<Void>()
    var popOutsideDeviceSettingsViewController = PublishSubject<Void>()
    var preferences: DefaultStorage
    var completed: PublishSubject<FlowId> = PublishSubject<FlowId>()
    var errorParentViewController: UIViewController?

    private let dependencies: Dependencies

    private var currentDeviceSettingsItem = PublishSubject<DeviceSettingsItem>()
    private let disposeBag = DisposeBag()
    private var lastKnownOTAStatus: OTAStatus = .completed
    private var connectionDisposeBag = DisposeBag()
    private var aboutThisSpeakerDisposeBag = DisposeBag()
    
    private var activeDevice: BehaviorRelay<DeviceType>
    
    var setupProgress = BehaviorRelay<SetupProgressInfo>.init(value: .initializingConnection)
    
    init(activeDevice: BehaviorRelay<DeviceType>, dependencies: Dependencies, preferences: DefaultStorage = DefaultStorage.shared) {
        self.activeDevice = activeDevice
        self.dependencies = dependencies
        self.preferences = preferences
        
        super.init()
        
        currentDeviceSettingsItem
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] deviceSettingsItem in
                switch deviceSettingsItem {
                case .aboutThisSpeaker:
                    self?.showAboutThisSpeaker()
                case .renameSpeaker:
                    self?.showRenameSpeaker()
                case .coupleSpeaker:
                    self?.showCoupleSpeaker()
                case .forgetSpeaker:
                    self?.showForgetSpeaker()
                case .equaliser:
                    self?.showEqualizer()
                case .equaliserSettings:
                    self?.showEqualizerSettings()
                case .activeNoiceCancellingSettings:
                    self?.showActiveNoiseCancellingSettings()
                case .autoOffTimer:
                    self?.showAutoOffTimer()
                case .mButton:
                    self?.showMButton()
                case .light:
                    self?.showLight()
                case .sounds:
                    self?.showSounds()
                }
            }).disposed(by: disposeBag)
    }

    deinit {
        print("🚾 \(self) deinited 🚾")
    }

    func handleError(_ error: AnyError<OTAError>) {
        if error.id == .otherError {
            dismissError { [weak self] in
                guard let strongSelf = self else { return }
                self?.showError(parentViewController: strongSelf.errorParentViewController, error.id)
            }
            return
        }
        
        dismissError { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.showError(parentViewController: strongSelf.errorParentViewController, error.id)
        }
    }
    
    public override func viewModel(for errorType: OTAError) -> ErrorViewModel? {
        switch errorType {
        case .internetNotAvailable:
            let errorViewModel = ErrorViewModel(errorInfo: NoInternetOTAErrorInfo(), dependencies: dependencies)
            errorViewModel.outputs.errorInfo.action(type: .tryAgain) { [weak self] _ in
                self?.dismissError { }
            }
            errorViewModel.outputs.errorInfo.action(type: .skip) { [weak self] _ in
                self?.dismissError { }
            }
            return errorViewModel
        case .bluetoothOff:
            let errorViewModel = ErrorViewModel(errorInfo: BluetoothErrorInfo(), dependencies: dependencies)
            errorViewModel.outputs.errorInfo.action(type: .gotoSettings) { _ in
                AppEnvironment.goToSettings()
            }
            return errorViewModel
        case .deviceDisconnected:
            let errorViewModel = ErrorViewModel(errorInfo: NoDevicesFoundErrorInfo(), dependencies: dependencies)
            errorViewModel.outputs.errorInfo.action(type: .tryAgain) { [weak self] _ in
                self?.dismissError {  }
            }
            errorViewModel.outputs.errorInfo.action(type: .skip) { [weak self] _ in
                self?.dismissError { }
            }
            return errorViewModel
        case .otherError:
            let errorViewModel = ErrorViewModel(errorInfo: SomethingWentWrongErrorInfo(), dependencies: dependencies)
            errorViewModel.outputs.errorInfo.action(type: .tryAgain) { [weak self] _ in
                self?.dismissError { }
            }
            return errorViewModel
            
        default: return nil
        }
    }
}

private extension DeviceSettingsFlowModel {
    func showAboutThisSpeaker() {
        
        aboutThisSpeakerDisposeBag = DisposeBag()
        
        let viewController = UIViewController.instantiate(AboutThisSpeakerViewController.self)
        errorParentViewController = viewController
        
        let viewModel: AboutThisSpeakerViewModelType = AboutThisSpeakerViewModel(activeDevice: activeDevice, otaService: dependencies.otaService, progress: setupProgress.asObservable())
        viewController.viewModel = viewModel
        
        viewModel.outputs.active.asObservable()
            .observeOn(MainScheduler.instance)
            .skipWhile { $0 }
            .subscribe { [weak self] _ in
                self?.popViewController.onNext(()) }
            .disposed(by: aboutThisSpeakerDisposeBag)
        
        pushViewController.onNext(viewController)
    
        dependencies.otaService.outputs.rediscovered
            .subscribe(onNext: { [weak self] device in
                self?.dismissError { [weak self] in
                    self?.performOTA()
                }
            })
            .disposed(by: aboutThisSpeakerDisposeBag)
    
        viewModel.outputs.updateTrigger
            .subscribe(onNext: { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.dependencies.analyticsService.inputs.log(.appOtaStarted)
                strongSelf.performOTA()
            })
            .disposed(by: aboutThisSpeakerDisposeBag)
        
        viewModel.outputs.viewAppeared.subscribe(onNext: { [weak self] in
            self?.performOTA()
        })
        .disposed(by: aboutThisSpeakerDisposeBag)
        
        
        setupProgress
            .skipWhile { info in
                guard case .otaProgress(let status) = info, case .completed = status else {
                    return true
                }
                return false
            }
            .takeOne()
            .subscribe(onNext: { [weak self] _ in
                guard let strongSelf = self else { return }
                viewModel.inputs.updateCompleted()
                strongSelf.errorParentViewController = nil
            })
            .disposed(by: aboutThisSpeakerDisposeBag)
    }
    
    func showRenameSpeaker() {
        let viewController = UIViewController.instantiate(RenameViewController.self)
        let viewModel = RenameViewModel(device: activeDevice.value, dependencies: dependencies)
        viewController.viewModel = viewModel
        
        viewModel.outputs.active.asObservable()
            .observeOn(MainScheduler.instance)
            .skipOne()
            .skipWhile { $0 }
            .subscribe { [weak self] _ in
                self?.popViewController.onNext(()) }
            .disposed(by: disposeBag)
        
        pushViewController.onNext(viewController)
    }
    
    func showCoupleSpeaker() {
        let coupleSpeakerFlowModel = CoupleSpeakerFlowModel(device: activeDevice.value, dependencies: dependencies)

        coupleSpeakerFlowModel.pushViewController
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] viewController in
                self?.pushViewController.onNext(viewController)
            }).disposed(by: disposeBag)

        coupleSpeakerFlowModel.popViewController
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.popViewController.onNext(())
            }).disposed(by: disposeBag)

        coupleSpeakerFlowModel.completed
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] flowId in
                if self.preferences.lastConfiguredCouplePairs.isEmpty {
                    self.popToDeviceSettingsViewController.onNext(())
                } else {
                    self.popOutsideDeviceSettingsViewController.onNext(())
                    self.completed.onNext(DeviceSettingsFlowModel.id)
                }
                self.submodels.remove(itemsWith: flowId)
            }).disposed(by: disposeBag)

        coupleSpeakerFlowModel.aborted
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] flowId in
                self.submodels.remove(itemsWith: flowId)
            }).disposed(by: disposeBag)

        do {
            try guardSubmodules(contains: CoupleSpeakerFlowModel.id)
            submodels.append(coupleSpeakerFlowModel)
            
            coupleSpeakerFlowModel.inputs.pushCoupleSpeaker()
        } catch {
            print("Flow guard submodule failed")
        }
    }
    
    func showForgetSpeaker() {
        let viewModel = ForgetSpeakerViewModel(device: activeDevice.value)
        let viewController = UIViewController.instantiate(ForgetSpeakerViewController.self)
        viewController.viewModel = viewModel

        viewModel.outputs.speakerForgotten
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe({ [weak self] _ in
                guard let deviceToForget = self?.activeDevice.value else { return }
                self?.dependencies.deviceAdapterService.inputs.forget(device: deviceToForget)
                self?.popOutsideDeviceSettingsViewController.onNext(())
                self?.completed.onNext(DeviceSettingsFlowModel.id)
            }).disposed(by: disposeBag)

        pushViewController.onNext(viewController)
    }
    
    func showEqualizer() {
        let viewModel = EqualizerViewModel(device: activeDevice.value, dependencies: dependencies, preferences: preferences)
        let viewController = UIViewController.instantiate(EqualizerViewController.self)
        viewController.viewModel = viewModel

        viewModel.outputs.active.asObservable()
            .observeOn(MainScheduler.instance)
            .skipOne()
            .skipWhile { $0 }
            .subscribe { [weak self] _ in
                self?.popViewController.onNext(()) }
            .disposed(by: disposeBag)

        pushViewController.onNext(viewController)
    }

    func showEqualizerSettings() {
        let viewModel = EqualizerSettingsViewModel(device: activeDevice.value, dependencies: dependencies, preferences: preferences)
        let viewController = UIViewController.instantiate(EqualizerSettingsViewController.self)
        viewController.viewModel = viewModel

        viewModel.outputs.active.asObservable()
            .observeOn(MainScheduler.instance)
            .skipOne()
            .skipWhile { $0 }
            .subscribe { [weak self] _ in
                self?.popViewController.onNext(()) }
            .disposed(by: disposeBag)

        pushViewController.onNext(viewController)
    }

    func showActiveNoiseCancellingSettings() {
        let viewModel = ActiveNoiceCancellingViewModel(device: activeDevice.value, dependencies: dependencies)
        let viewController = UIViewController.instantiate(ActiveNoiceCancellingViewController.self)
        viewController.viewModel = viewModel

        viewModel.outputs.active.asObservable()
            .observeOn(MainScheduler.instance)
            .skipOne()
            .skipWhile { $0 }
            .subscribe { [weak self] _ in
                self?.popViewController.onNext(()) }
            .disposed(by: disposeBag)

        pushViewController.onNext(viewController)
    }

    func showAutoOffTimer() {
        let viewModel = AutoOffTimerViewModel(device: activeDevice.value, dependencies: dependencies)
        let viewController = UIViewController.instantiate(AutoOffTimerViewController.self)
        viewController.viewModel = viewModel

        viewModel.outputs.active.asObservable()
            .observeOn(MainScheduler.instance)
            .skipOne()
            .skipWhile { $0 }
            .subscribe { [weak self] _ in
                self?.popViewController.onNext(()) }
            .disposed(by: disposeBag)

        pushViewController.onNext(viewController)
    }

    func showMButton() {
        let viewModel = MButtonViewModel(device: activeDevice.value, dependencies: dependencies)
        let viewController = UIViewController.instantiate(MButtonViewController.self)
        viewController.viewModel = viewModel

        viewModel.outputs.active.asObservable()
            .observeOn(MainScheduler.instance)
            .skipOne()
            .skipWhile { $0 }
            .subscribe { [weak self] _ in
                self?.popViewController.onNext(()) }
            .disposed(by: disposeBag)

        viewModel.outputs.googleAssistantScreenRequested
            .subscribe(onNext: { [weak self] in
                self?.showGoogleAssistantScreen()
            })
            .disposed(by: disposeBag)

        pushViewController.onNext(viewController)
    }

    func showGoogleAssistantScreen() {
        let viewController = UIViewController.instantiate(GoogleAssistantViewController.self)
        let viewModel = GoogleAssistantViewModel()
        viewModel.outputs.done
            .subscribe(onNext: { [weak self] in
                self?.popViewController.onNext(())
            })
            .disposed(by: disposeBag)
        viewController.viewModel = viewModel
        pushViewController.onNext(viewController)
    }
    
    func showLight() {
        let viewController = UIViewController.instantiate(LightViewController.self)
        let viewModel = LightViewModel(device: activeDevice.value, dependencies: dependencies)
        viewController.viewModel = viewModel
        
        viewModel.outputs.active.asObservable()
            .observeOn(MainScheduler.instance)
            .skipWhile { $0 }
            .subscribe { [weak self] _ in
                self?.popViewController.onNext(()) }
            .disposed(by: disposeBag)
        
        pushViewController.onNext(viewController)
    }
    
    func showSounds() {
        let viewController = UIViewController.instantiate(SoundsViewController.self)
        let viewModel = SoundsViewModel(device: activeDevice.value)
        viewController.viewModel = viewModel
        
        viewModel.outputs.active
            .asObservable()
            .observeOn(MainScheduler.instance)
            .skipWhile { $0 }
            .subscribe { [weak self] _ in
                self?.popViewController.onNext(()) }
            .disposed(by: disposeBag)
        
        pushViewController.onNext(viewController)
    }
    
    func showDeviceSettings() {
        let deviceSettingsViewController = UIViewController.instantiate(DeviceSettingsViewController.self)
        let deviceSettingsViewModel = DeviceSettingsViewModel(activeDevice: activeDevice, otaService: dependencies.otaService)
        deviceSettingsViewController.viewModel = deviceSettingsViewModel

        deviceSettingsViewModel.outputs.closed
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.completed.onNext(DeviceSettingsFlowModel.id)
            }).disposed(by: disposeBag)

        zip([
            deviceSettingsViewModel.outputs.about,
            deviceSettingsViewModel.outputs.rename,
            deviceSettingsViewModel.outputs.couple,
            deviceSettingsViewModel.outputs.forget,
            deviceSettingsViewModel.outputs.eq,
            deviceSettingsViewModel.outputs.eqSettings,
            deviceSettingsViewModel.outputs.ancSettings,
            deviceSettingsViewModel.outputs.autoOffTimer,
            deviceSettingsViewModel.outputs.mButton,
            deviceSettingsViewModel.outputs.light,
            deviceSettingsViewModel.outputs.sounds
        ], [
            .aboutThisSpeaker,
            .renameSpeaker,
            .coupleSpeaker,
            .forgetSpeaker,
            .equaliser,
            .equaliserSettings,
            .activeNoiceCancellingSettings,
            .autoOffTimer,
            .mButton,
            .light,
            .sounds
        ] as [DeviceSettingsItem]).forEach { outputDeviceSettingsItem, deviceSettingsItem in
            outputDeviceSettingsItem
                .asObservable()
                .observeOn(MainScheduler.instance)
                .subscribe({ [weak self] _ in
                    self?.currentDeviceSettingsItem.onNext(deviceSettingsItem)
                }).disposed(by: disposeBag)
        }

        pushViewController.onNext(deviceSettingsViewController)
    }

    private func performOTA() {
        
        let deviceID = activeDevice.value.id
        let initialState = OTAUpdateInfo(deviceID: deviceID, status: .connecting, error: nil)
        setupProgress.accept(SetupProgressInfo(otaStatus: initialState))
        lastKnownOTAStatus = .connecting
        
        connectionDisposeBag = DisposeBag()
        dependencies.otaService.inputs.resetProgress()
        
        /// Prepare to process OTA progress report from OTA service
        Observable.combineLatest(
            Observable.just(deviceID),
            dependencies.otaService.outputs.updateProgressInfo
            )
            .observeOn(MainScheduler.instance)
            .flatMap(strongify(self, Observable.empty(), DeviceSettingsFlowModel.extractOTAUpdateInfo))
            .flatMap(strongify(self, Observable.empty(), DeviceSettingsFlowModel.prepareProgressInfo))
            .subscribe(weak: self,
                       onNext: DeviceSettingsFlowModel.handleSetupProgressInfo,
                       onError: DeviceSettingsFlowModel.handleOTAError
            )
            .disposed(by: connectionDisposeBag)
        
        dependencies.otaService.inputs.startUpdate(deviceID: deviceID, interfaceError: dependencies.deviceAdapterService.outputs.error)
    }
    
    private func extractOTAUpdateInfo(_ deviceID: String, _ allUpdatesInfo: [OTAUpdateInfo]) -> Observable<OTAUpdateInfo> {
        guard let info = allUpdatesInfo.first(where: { $0.deviceID == deviceID }) else {
            return Observable.empty()
        }
        return Observable.just(info)
    }
    
    private func prepareProgressInfo(_ info: OTAUpdateInfo) -> Observable<SetupProgressInfo> {
        return Observable.just(SetupProgressInfo.init(otaStatus: info))
    }
    
    private func handleSetupProgressInfo(_ info: SetupProgressInfo) {
        
        self.setupProgress.accept(info)
        
        switch info {
        case .otaProgress(let status):
            switch status {
            case .connecting:
                if case .connecting = lastKnownOTAStatus {
                    return
                }
                lastKnownOTAStatus = status
            case .checkingFirmware:
                if case .checkingFirmware = lastKnownOTAStatus {
                    return
                }
                lastKnownOTAStatus = status
            case .downloadingFirmware:
                if case .downloadingFirmware = lastKnownOTAStatus {
                    return
                }
                lastKnownOTAStatus = status
            case .uploadingToDevice:
                if case .uploadingToDevice = lastKnownOTAStatus {
                    return
                }
                lastKnownOTAStatus = status
            case .uploadCompleted:
                if case .uploadCompleted = lastKnownOTAStatus {
                    return
                }
                lastKnownOTAStatus = status
                
            default: break
            }
        default: break
        }
        
        guard case .otaProgress(let status) = info, case .completed = status else {
            return
        }
        dependencies.analyticsService.inputs.log(AnalyticsEvent.appOtaCompleted)
    }
    
    private func handleOTAError(_ error: Error) {
        guard let error = error as? AnyError<OTAError> else {
            handleError(AnyError<OTAError>.init(id: OTAError.otherError))
            dependencies.analyticsService.inputs.log(AnalyticsEvent.appOtaFailed(.otherError))
            return
        }
        switch error.id {
        case .bluetoothOff:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.bluetoothOff))
        case .deviceDisconnected:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.deviceDisconnected))
        case .deviceInOTANotFoundInManagedDevicesList:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.deviceInOTANotFoundInManagedDevicesList))
        case .emptyLocalOrRemoteFirmwareVersion:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.emptyLocalOrRemoteFirmwareVersion))
        case .errorParsingRemoteURL:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.errorParsingRemoteURL))
        case .failedToCalculateMD5Checksum:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.failedToCalculateMD5Checksum))
        case .failedToCreateUnzippedFolder:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.failedToCreateUnzippedFolder))
        case .failedToUnzipFirmwareFiles:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.failedToUnzipFirmwareFiles))
        case .internetNotAvailable:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.internetNotAvailable))
        case .md5checksumMismatch:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.md5checksumMismatch))
        case .otherError:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.otherError))
        case .unableToCreateDownloadURL:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.unableToCreateDownloadURL))
        case .unknownDeviceType:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.unknownDeviceType))
        case .unknownRemoteDownloadURL:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.unknownRemoteDownloadURL))
        case .unknownRemoteFwVersion:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.unknownRemoteFwVersion))
        case .unknownRemoteVersion:
            dependencies.analyticsService.inputs.log(.appOtaFailed(.unknownRemoteVersion))
        case .urlSessionError(_):
            dependencies.analyticsService.inputs.log(.appOtaFailed(.urlSessionError))
        }
        handleError(error)
    }
    
}

extension DeviceSettingsFlowModel: DeviceSettingsFlowModelType {
    var inputs: DeviceSettingsFlowModelInput { return self }
    var outputs: DeviceSettingsFlowModelOutput { return self }
}

extension DeviceSettingsFlowModel: DeviceSettingsFlowModelInput {
    func pushDeviceSettings() {
        showDeviceSettings()
    }
}

extension DeviceSettingsFlowModel: DeviceSettingsFlowModelOutput {
}
