//
//  AppFlowModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 13/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import RxCocoa
import GUMA
import RCER
import Reachability

private struct Const {
    static let devicesSearchTimeout = RxTimeInterval.seconds(15.0)
}

public enum AppState {
    case firstRun
    case signup
    case loading
    case idle
    case startScan
    case scanning
}

public enum AppFlowError: Error {
    case bluetoothOff
    case noInternet
}

extension AppFlowError: Equatable {
    public static func == (lhs: AppFlowError, rhs: AppFlowError) -> Bool {
        switch (lhs, rhs) {
        case (.bluetoothOff, .bluetoothOff),
             (.noInternet, .noInternet ):
            return true
        default: return false
        }
    }
}

public protocol AppFlowModelInput {
    func changeStatus(state: AppState)
}

public protocol AppFlowModelOutput {
    var state: Variable<AppState> { get }
    var setViewController: BehaviorRelay<UIViewController?> { get }
    var pushViewController: PublishSubject<UIViewController> { get }
    var presentViewController: PublishSubject<UIViewController> { get }
    var dismissViewController: PublishSubject<UIViewController> { get }
}

public protocol AppFlowModelType {
    var inputs: AppFlowModelInput { get }
    var outputs: AppFlowModelOutput {get }
}

public final class AppFlowModel: BaseContextualErrorReporter<AppState, AppFlowError>,
                                 FlowModel,
                                 AppFlowModelInput,
                                 AppFlowModelOutput,
                                 AppFlowModelType {
    
    static var id: FlowId = .app
    
    public var inputs: AppFlowModelInput { return self }
    public var outputs: AppFlowModelOutput { return self}
    
    public let state: Variable<AppState> = Variable(.loading)
    
    var preferences: DefaultStorage
    
    var submodels = [FlowModel]()
    
    var completed: PublishSubject<FlowId> = PublishSubject<FlowId>()
    
    public var setViewController = BehaviorRelay<UIViewController?>.init(value: nil)
    
    public var pushViewController = PublishSubject<UIViewController>()
    var popViewController = PublishSubject<Void>()
    
    public var presentViewController = PublishSubject<UIViewController>()
    public var dismissViewController = PublishSubject<UIViewController>()
    
    var initialViewController: UIViewController? = nil
    
    public init(dependency: AppDependency, preferences: DefaultStorage = DefaultStorage.shared) {

        self.appDependencies = dependency
        self.preferences = preferences
        
        guard let reachability = Reachability() else {
            fatalError("Could not initialize Reachability")
        }
        self.errorReporter = ErrorReporter(onlineTracker: reachability)
        
        super.init()
        
        let firstRun: Variable<Bool> = Variable(!preferences.startPressed)
        let signupCompleted: Variable<Bool> = Variable(preferences.signupCompleted)
        
        Driver.combineLatest(firstRun.asDriver(), signupCompleted.asDriver())
            .drive(onNext: { [unowned self] (firstRun, signupCompleted)  in
                self.state.value = firstRun ? .firstRun
                    : signupCompleted ? .startScan : .signup
        }).disposed(by: disposeBag)

        self.state.asDriver()
            .drive(onNext: { [unowned self] state in
                self.execute(next: state,
                             previous: self.previousState)
            }).disposed(by: disposeBag)
    }
    
    public func changeStatus(state: AppState) {
        self.state.value = state
    }

    public override func configureErrorSources(for types: [AppFlowError]) {
        types.forEach { error in
            switch error {
            case .bluetoothOff:
                let source = bluetoothErrorSource()
                errorReporter.append(errorSource: source)
            default: return
            }
        }
    }
    
    public override func viewModel(for errorType: AppFlowError) -> ErrorViewModel? {
        
        switch errorType {
        case .bluetoothOff:
            let errorViewModel = ErrorViewModel(errorInfo: BluetoothErrorInfo(), dependencies: appDependencies)
            errorViewModel.outputs.errorInfo.action(type: .gotoSettings) { _ in
                AppEnvironment.goToSettings()
            }
            return errorViewModel
        default:
            return nil
        }
    }
    
    public override func handleError(_ error: AppFlowError) {
        switch error {
        case .bluetoothOff:
            showBluetoothError()
        default: break
        }
    }

    private var errorReporter: ErrorReporter<AppFlowError>
    private var deviceAdapterService: DeviceAdapterServiceType = DeviceAdapterService.shared
    private var appDependencies: AppDependency
    private var previousState: AppState = .loading
    private var disposeBag = DisposeBag()
    private var handleErrorDisposeBag = DisposeBag()
    private var waitingDisposeBag = DisposeBag()
    private var errorTriggersDisposables = [TriggerDisposableEntry]()
}

/// Helpers for AppFlowModel error handling
private extension AppFlowModel {
    
    struct TriggerDisposableEntry {
        let error: AppFlowError
        var disposeBag: DisposeBag
    }
    
    private func disposableEntry(for errorType: AppFlowError) -> TriggerDisposableEntry {
        if let existingDisposableEntryIndex = errorTriggersDisposables.index(where: { $0.error == errorType }) {
            errorTriggersDisposables.remove(at: existingDisposableEntryIndex)
        }
        let triggerDisposable = TriggerDisposableEntry(error: errorType, disposeBag: DisposeBag())
        errorTriggersDisposables.append(triggerDisposable)
        return triggerDisposable
    }
    
    private func bluetoothErrorSource() -> ErrorSource<AppFlowError> {
        
        let triggerDisposable = disposableEntry(for: .bluetoothOff)
        let trigger = BehaviorRelay<AnyError<AppFlowError>?>.init(value: nil)
        
        appDependencies.deviceAdapterService.outputs.error
            .flatMap { adapterError -> Observable<AnyError<AppFlowError>?> in
                guard let adapterError = adapterError else {
                    return Observable.just(nil)
                }
                guard adapterError == .btInterfaceDisabled else {
                    return Observable.just(nil)
                }
                return Observable.just(AnyError<AppFlowError>.init(id: .bluetoothOff))
            }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { error in trigger.accept(error) })
            .disposed(by: triggerDisposable.disposeBag)
        
        return ErrorSource<AppFlowError>.init(type: .bluetoothOff, trigger: trigger, priority: 0)
    }

    private func showBluetoothError() {
        let viewController = UIViewController.instantiate(ErrorViewController.self)
        let errorViewModel = viewModel(for: .bluetoothOff)
        viewController.viewModel = errorViewModel
        setViewController.accept(viewController)
    }

}

private extension AppFlowModel {

    private func execute(next: AppState, previous: AppState) {
        
        switch(next, previous) {
        case (.firstRun, .loading):
            showWelcome()
        case (.startScan, .loading):
            showInitialScan()
        case (.signup, .loading):
            startSignup()
        case (.startScan, .signup):
            showInitialScan()
        case (.scanning, .startScan):
            deviceAdapterService.inputs.start()
            waitForDevices()
        case (.idle, .startScan):
            previousState = .idle
            showHome()
        default: break
        }
    }

    private func showTermsAndConditions() {
        let termsAndConditions = UIViewController.instantiate(WelcomeTermsAndConditionsViewController.self)
        let termsAndConditionsViewModel = TermsAndConditionsViewModel()
        termsAndConditions.viewModel = termsAndConditionsViewModel
        presentViewController.onNext(termsAndConditions)

        weak var modalScreen = termsAndConditions
        termsAndConditionsViewModel.back
            .asDriver()
            .skipOne()
            .drive(onNext: { [unowned self] _ in
                if let modalScreen = modalScreen {
                    self.dismissViewController.onNext(modalScreen)
                }
            })
            .disposed(by: disposeBag)
    }

    private func showWelcome() {
        let viewController = UIViewController.instantiate(WelcomeViewController.self)
        let viewModel = WelcomeViewModel()
        viewController.viewModel = viewModel
        
        setViewController.accept(viewController)
        
        viewModel.outputs.termsAccepted
            .asDriver()
            .skipOne()
            .drive(onNext: { [unowned self] value in
                self.preferences.startPressed = true
                self.state.value = .signup
            })
            .disposed(by: disposeBag)

        viewModel.outputs.termsAndConditionsRequested
            .asDriver()
            .skipOne()
            .drive(onNext: { [unowned self] value in
                self.showTermsAndConditions()
            })
            .disposed(by: disposeBag)
    }
    
    private func showInitialScan() {
        let progressViewController = UIViewController.instantiate(CircularProgressViewController.self)
        let viewModel = CircularProgressViewModel()
        progressViewController.viewModel = viewModel
        
        viewModel.viewAppeared.observeOn(MainScheduler.instance).bind { [unowned self] _ in
            self.previousState = .startScan
            self.state.value = .scanning
        }.disposed(by: disposeBag)
    
        setViewController.accept(progressViewController)
    }
    
    private func startSignup() {
        let signupFlowModel = SignupFlowModel(dependencies: appDependencies)
        signupFlowModel.setViewController
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] viewController in
                self.setViewController.accept(viewController)
            })
            .disposed(by: disposeBag)

        signupFlowModel.pushViewController
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] viewController in
                self.pushViewController.onNext(viewController)
            }).disposed(by: disposeBag)

        signupFlowModel.completed
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] flowId in
                self.submodels.remove(itemsWith: flowId)
                self.preferences.signupCompleted = true
                self.previousState = .signup
                self.state.value = .startScan
            }).disposed(by: disposeBag)
        
        do {
            try guardSubmodules(contains: SignupFlowModel.id)
            submodels.append(signupFlowModel)
        } catch {
            print("Flow guard submodule failed")
        }
    }
    
    private func waitForDevices() {
        var waitForDevicesDisposeBag = DisposeBag()
        appDependencies.deviceAdapterService.outputs.adapterState
            .skipWhile { $0 != .foundFirst }
            .timeout(Const.devicesSearchTimeout, scheduler: MainScheduler.instance)
            .subscribe(
                onNext: { [weak self] adapterState in
                    switch adapterState {
                    case .foundFirst:
                        self?.state.value = .idle
                        waitForDevicesDisposeBag = DisposeBag()
                    default: break
                    }
                },
                onError: { [weak self] error in
                    self?.state.value = .idle
                    waitForDevicesDisposeBag = DisposeBag()
                }
            )
            .disposed(by: waitForDevicesDisposeBag)
        appDependencies.deviceAdapterService.inputs.refresh()
    }
    
    private func waitForBluetoothInterfaceChanges(state: AppState) {
        
        waitingDisposeBag = DisposeBag()
        
        Single<Void>.create { [weak self] singleEvent in
            guard let strongSelf = self else { return Disposables.create() }
            strongSelf.deviceAdapterService.outputs.error
                .skipWhile { $0 != nil }
                .subscribe(onNext: { _ in
                    singleEvent(.success(()))
                }).disposed(by: strongSelf.waitingDisposeBag)
            
            return Disposables.create()
        }
        .observeOn(MainScheduler.instance)
        .subscribe(onSuccess: { [weak self] _ in
            self?.showInitialScan()
            self?.waitForDevices()
        })
        .disposed(by: waitingDisposeBag)
    }
    
    private func showHome() {
        let homeFlowModel = HomeFlowModel(dependencies: appDependencies)
        
        homeFlowModel.setViewController
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] viewController in
                self.setViewController.accept(viewController)
            })
            .disposed(by: disposeBag)
        
        homeFlowModel.completed
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] flowId in
                self.submodels.remove(itemsWith: flowId)
                self.previousState = .idle
                self.state.value = .idle
            })
            .disposed(by: disposeBag)

        homeFlowModel.pushViewController
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] viewController in
                self.pushViewController.onNext(viewController)
            }).disposed(by: disposeBag)
        
        do {
            try guardSubmodules(contains: HomeFlowModel.id)
            submodels.append(homeFlowModel)
        } catch {
            print("Flow guard submodule failed")
        }
    }
}
