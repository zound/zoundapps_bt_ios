//
//  PairAction.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 05/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import GUMA
import RxSwift

typealias PairFlowDependencies = DeviceServiceDependency & AppModeDependency

func pairFlowAction(
    setViewControllerTrigger: BehaviorRelay<UIViewController?>,
    dependencies: PairFlowDependencies,
    start: StartConfigurationFlowBlock<PairingFlowModel>,
    completion: ConfigurationFlowCompletion
) -> DeviceConfigurationBlock {
    return DeviceConfigurationBlock { device in
            return performFlow(device: device,
                               setViewControllerTrigger: setViewControllerTrigger,
                               dependencies: dependencies,
                               start: start,
                               completion: completion,
                               pairingState: .unhandled)
    }
}
private func performFlow(device: DeviceType,
                         setViewControllerTrigger: BehaviorRelay<UIViewController?>,
                         dependencies: PairFlowDependencies,
                         start: StartConfigurationFlowBlock<PairingFlowModel>,
                         completion: ConfigurationFlowCompletion,
                         pairingState: DevicePairingState)
-> Single<Void> {
    return Single.create { event in
        var pairingDisposeBag = DisposeBag()
        let pairingFlowModel = PairingFlowModel(dependencies: dependencies, device: device, initialPairingState: pairingState)
        pairingFlowModel.setViewController
            .asDriver()
            .drive(onNext: { viewController in
                guard let viewController = viewController else { return }
                setViewControllerTrigger.accept(viewController)
            })
            .disposed(by: pairingDisposeBag)
        pairingFlowModel.completed
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext: { flowId in
                    completion.action(flowId, nil)
                    event(.success(()))
                    pairingDisposeBag = DisposeBag()
                }
            )
            .disposed(by: pairingDisposeBag)
        pairingFlowModel.error
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext: { (flowId, error) in
                    completion.action(flowId, error)
                    event(.error(error))
                    pairingDisposeBag = DisposeBag()
                }
            )
            .disposed(by: pairingDisposeBag)
        guard let error = start.action(pairingFlowModel) else {
            return Disposables.create()
        }
        event(.error(error))
        return Disposables.create()
    }
}
private func checkIfInPairingMode(device: DeviceType) -> Single<(DeviceType, Bool)> {
    return device.state.inputs.requestPairingMode()
        .map { [unowned device] mode in
            switch mode {
            case .enabled:
                return (device, true)
            case .disabled:
                return (device, false)
            }
        }
}
private func enablePairingModeIfNeeded(device: DeviceType, _ enable: Bool) -> Single<Void> {
    guard enable == true else {
        return Single.just(())
    }
    return device.state.inputs.activatePairingMode()
}


