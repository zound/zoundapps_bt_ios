//
//  CheckUpdateAction.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 07/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import GUMA
import RxSwift

typealias CheckUpdateDependencies = OTAServiceDependency

func checkUpdateAction(dependencies: CheckUpdateDependencies)
-> DeviceConfigurationBlock {
    return DeviceConfigurationBlock { device in
        dependencies.otaService.inputs.checkUpdateAvailable(deviceID: device.id)
            /* LINE BELOW - HACK FOR JOPLIN LITE - UNTIL OTA FOR JOPLIN LITE CLARIFIED */
            .catchErrorJustReturn(nil)
            .flatMap({ _ in return Single.just(()) })
    }
}
