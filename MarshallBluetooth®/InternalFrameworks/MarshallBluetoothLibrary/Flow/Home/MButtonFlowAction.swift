//
//  MButtonFlowAction.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 10/02/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import GUMA
import RxSwift

typealias OnboardingMButtonFlowDependencies = DeviceServiceDependency

func mButtonFlowAction(
    setViewControllerTrigger: BehaviorRelay<UIViewController?>,
    dependencies: OnboardingMButtonFlowDependencies,
    start: StartConfigurationFlowBlock<OnboardingMButtonFlowModel>,
    completion: ConfigurationFlowCompletion
) -> DeviceConfigurationBlock {
    return DeviceConfigurationBlock { device in
        Single.create { event in
            var pairingDisposeBag = DisposeBag()
            let onboardingMButtonFlowModel = OnboardingMButtonFlowModel(dependencies: dependencies, device: device)
            onboardingMButtonFlowModel.setViewController
                .asDriver()
                .drive(onNext: { viewController in
                    guard let viewController = viewController else { return }
                    setViewControllerTrigger.accept(viewController)
                })
                .disposed(by: pairingDisposeBag)
            onboardingMButtonFlowModel.completed
                .observeOn(MainScheduler.instance)
                .subscribe(
                    onNext: { flowId in
                        completion.action(flowId, nil)
                        event(.success(()))
                        pairingDisposeBag = DisposeBag()
                    }
                )
                .disposed(by: pairingDisposeBag)
            guard let error = start.action(onboardingMButtonFlowModel) else {
                return Disposables.create()
            }
            event(.error(error))
            return Disposables.create()
        }
    }
}
