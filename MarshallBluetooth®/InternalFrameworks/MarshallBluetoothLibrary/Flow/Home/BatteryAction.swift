//
//  BatteryAction.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 07/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import GUMA
import RxSwift

func batteryAction() -> DeviceConfigurationBlock {
    return DeviceConfigurationBlock { device in
        device.state.inputs.requestBatteryStatus()
        return Single.just(())
    }
}
