//
//  BLEConnectAction.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 05/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import GUMA
import RxSwift

func bleConnectAction() -> DeviceConfigurationBlock {
    return DeviceConfigurationBlock { device in
        device.bleConnect()
    }
}
