//
//  ForcedOTAAction.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 06/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import GUMA
import RxSwift

typealias ForcedOTADependencies = OTAServiceDependency &
                                  DeviceAdapterServiceDependency &
                                  AnalyticsServiceDependency &
                                  AppModeDependency
func forcedOTAAction(
    setViewControllerTrigger: BehaviorRelay<UIViewController?>,
    dependencies: ForcedOTADependencies,
    start: StartConfigurationFlowBlock<SetupFlowModel>,
    completion: ConfigurationFlowCompletion
) -> DeviceConfigurationBlock {
    return DeviceConfigurationBlock { device in
        return dependencies.otaService.inputs.checkForcedOTA(deviceID: device.id)
                .observeOn(MainScheduler.instance)
                .flatMap(checkForcedOTA(for: device))
                .flatMap(perform(device: device,
                                 setViewControllerTrigger: setViewControllerTrigger,
                                 dependencies: dependencies,
                                 start: start,
                                 completion: completion))
    }
}
private func perform(
    device: DeviceType,
    setViewControllerTrigger: BehaviorRelay<UIViewController?>,
    dependencies: ForcedOTADependencies,
    start: StartConfigurationFlowBlock<SetupFlowModel>,
    completion: ConfigurationFlowCompletion
) -> (Bool) -> Single<Void> {
    return { required  in
        guard required == true else { return Single.just(()) }
        return Single.create { event in
            var disposeBag = DisposeBag()
            let setupFlowModel = SetupFlowModel(dependencies: dependencies, device: device)
            setupFlowModel.setViewController
                .asDriver()
                .drive(onNext: { viewController in
                    guard let viewController = viewController else { return }
                    setViewControllerTrigger.accept(viewController)
                })
                .disposed(by: disposeBag)
            setupFlowModel.completed
                .observeOn(MainScheduler.instance)
                .subscribe(
                    onNext: { flowId in
                        completion.action(flowId, nil)
                        event(.success(()))
                        disposeBag = DisposeBag()
                    }
                )
                .disposed(by: disposeBag)
            guard let error = start.action(setupFlowModel) else {
                return Disposables.create()
            }
            event(.error(error))
            return Disposables.create()
        }
    }
}
private func checkForcedOTA(for device: DeviceType) -> (UpdateAvailableInfo?) -> Single<Bool> {
    return { info in
        guard let info = info else {
            return Single.error(DeviceSetupError.generalForcedOTAError)
        }
        return info.forced == true ? Single.just(true) : Single.just(false)
    }
}
