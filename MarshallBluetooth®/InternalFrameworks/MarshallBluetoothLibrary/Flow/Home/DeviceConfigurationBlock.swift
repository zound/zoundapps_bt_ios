//
//  DeviceConfigurationBlock.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 05/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import GUMA
import RxSwift

typealias ConfigurationDependencies = DeviceServiceDependency &
    OTAServiceDependency &
    DeviceAdapterServiceDependency &
    AppModeDependency &
    AnalyticsServiceDependency &
    NewsletterServiceDependency

struct StartConfigurationFlowBlock<A> {
    let action: (A) -> Error?
}
struct ConfigurationFlowCompletion {
    let action: (FlowId, Error?) -> Void
}
struct DeviceConfigurationBlock {
    let action: (DeviceType) -> Single<Void>
}
extension HomeFlowModel {
    func processConfiguration(for device: DeviceType,
                              rootViewController: UIViewController,
                              dependencies: ConfigurationDependencies) -> Single<Void> {
        return Single.create { [unowned self] event in
            var configurationDisposeBag = DisposeBag()
            let perFeatureConfigurationSteps = configurationFeatures(of: device)
            let preConfigurationActions: [DeviceConfigurationBlock] = [bleConnectAction()]
            let postConfigurationActions: [DeviceConfigurationBlock] = [
                doneAction(setViewControllerTrigger: self.setViewController,
                           completion: { [unowned self] in self.setViewController.accept(rootViewController)})
            ]
            let perFeatureConfigurationActions: [DeviceConfigurationBlock] = perFeatureConfigurationSteps
                .map(self.featureSetupAction(device: device, dependencies: dependencies, rootViewController: rootViewController))
            var configurationActions: [DeviceConfigurationBlock] = []
            configurationActions.append(contentsOf: preConfigurationActions)
            configurationActions.append(contentsOf: perFeatureConfigurationActions)
            if perFeatureConfigurationSteps.contains(where: { $0 == .hasPairing }) {
                configurationActions.append(contentsOf: postConfigurationActions)
            }
            let configurationObservables = configurationActions.map { $0.action(device).asObservable() }
            Observable.concat(configurationObservables)
                .subscribe(
                    onError: { error in
                        event(.error(error))
                        configurationDisposeBag = DisposeBag()
                    },
                    onCompleted: {
                        event(.success(()))
                        configurationDisposeBag = DisposeBag()
                    })
                .disposed(by: configurationDisposeBag)
            return Disposables.create()
        }
    }
    func featureSetupAction(device: DeviceType,
                            dependencies: ConfigurationDependencies,
                            rootViewController: UIViewController)
    -> (Feature) -> DeviceConfigurationBlock {
        return { [unowned self] feature in
            switch feature {
            case .hasForcedOTA:
                return forcedOTAAction(
                    setViewControllerTrigger: self.setViewController,
                    dependencies: dependencies,
                    start: StartConfigurationFlowBlock { [unowned self] setupFlowModel in
                        do {
                            try self.guardSubmodules(contains: type(of: setupFlowModel).id)
                            self.submodels.append(setupFlowModel)
                            setupFlowModel.inputs.perform()
                            return nil
                        } catch {
                            return FlowError.duplicatedFlow
                        }
                    },
                    completion: ConfigurationFlowCompletion { [unowned self] flowId, error in
                        self.submodels.remove(itemsWith: flowId)
                        self.setViewController.accept(rootViewController)
                    }
                )
            case .hasPairing:
                return pairFlowAction(
                    setViewControllerTrigger: self.setViewController,
                    dependencies: dependencies,
                    start: StartConfigurationFlowBlock { [unowned self] pairFlowModel in
                        do {
                            try self.guardSubmodules(contains: type(of: pairFlowModel).id)
                            self.submodels.append(pairFlowModel)
                            pairFlowModel.inputs.perform()
                            return nil
                        } catch {
                            return FlowError.duplicatedFlow
                        }
                    }, completion: ConfigurationFlowCompletion { [unowned self] flowId, error in
                        self.submodels.remove(itemsWith: flowId)
                    }
                )
            case .hasOnboarding:
                return onboardingFlowAction(
                    setViewControllerTrigger: self.setViewController,
                    dependencies: dependencies,
                    start: StartConfigurationFlowBlock { [unowned self] pairFlowModel in
                        do {
                            try self.guardSubmodules(contains: type(of: pairFlowModel).id)
                            self.submodels.append(pairFlowModel)
                            pairFlowModel.inputs.perform()
                            return nil
                        } catch {
                            return FlowError.duplicatedFlow
                        }
                    },
                    completion: ConfigurationFlowCompletion { [unowned self] flowId, error in
                        self.submodels.remove(itemsWith: flowId)
                    }
                )
            case .hasMButton:
                return mButtonFlowAction(
                    setViewControllerTrigger: self.setViewController,
                    dependencies: dependencies,
                    start: StartConfigurationFlowBlock { [unowned self] onboardingMButtonFlowModel in
                        do {
                            try self.guardSubmodules(contains: type(of: onboardingMButtonFlowModel).id)
                            self.submodels.append(onboardingMButtonFlowModel)
                            onboardingMButtonFlowModel.inputs.perform()
                            return nil
                        } catch {
                            return FlowError.duplicatedFlow
                        }
                    },
                    completion: ConfigurationFlowCompletion { [unowned self] flowId, error in
                        self.submodels.remove(itemsWith: flowId)
                    }
                )
            case .hasOTA:
                return checkUpdateAction(dependencies: dependencies)
            case .hasBatteryStatus:
                return batteryAction()
            default:
                return { DeviceConfigurationBlock { _ in Single.just(()) } }()
            }
        }
    }
}
private func configurationFeatures(of device: DeviceType) -> [Feature] {
    return device.state.outputs.perDeviceTypeFeatures
        .filter { ConfigurationFeature(feature: $0) != nil }
        .sorted { ConfigurationFeature(feature: $0)! < ConfigurationFeature(feature: $1)! }
}
