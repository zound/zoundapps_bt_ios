//
//  OnboardingFlowAction.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 08/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import GUMA
import RxSwift

typealias OnboardingFlowDependencies = DeviceServiceDependency

func onboardingFlowAction(
    setViewControllerTrigger: BehaviorRelay<UIViewController?>,
    dependencies: OnboardingFlowDependencies,
    start: StartConfigurationFlowBlock<OnboardingFlowModel>,
    completion: ConfigurationFlowCompletion
) -> DeviceConfigurationBlock {
    return DeviceConfigurationBlock { device in
        Single.create { event in
            var pairingDisposeBag = DisposeBag()
            let onboardingFlowModel = OnboardingFlowModel(dependencies: dependencies, device: device)
            onboardingFlowModel.setViewController
                .asDriver()
                .drive(onNext: { viewController in
                    guard let viewController = viewController else { return }
                    setViewControllerTrigger.accept(viewController)
                })
                .disposed(by: pairingDisposeBag)
            onboardingFlowModel.completed
                .observeOn(MainScheduler.instance)
                .subscribe(
                    onNext: { flowId in
                        completion.action(flowId, nil)
                        event(.success(()))
                        pairingDisposeBag = DisposeBag()
                    }
                )
                .disposed(by: pairingDisposeBag)
            guard let error = start.action(onboardingFlowModel) else {
                return Disposables.create()
            }
            event(.error(error))
            return Disposables.create()
        }
    }
}
