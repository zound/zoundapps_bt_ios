//
//  DoneAction.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 05/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import GUMA
import RxSwift

func doneAction(
    setViewControllerTrigger: BehaviorRelay<UIViewController?>,
    completion: @escaping () -> Void
) -> DeviceConfigurationBlock {
    return DeviceConfigurationBlock { device in
        return Single.create { event  in
            var doneDisposeBag = DisposeBag()
            let pairingDoneViewController = UIViewController.instantiate(PairingDoneViewController.self)
            let pairingDoneViewModel: PairingDoneViewModelType = PairingDoneViewModel(deviceName: device.modelName, image: device.image.largeImage)
            pairingDoneViewModel.outputs.done
                .asDriver()
                .distinctUntilChanged()
                .drive(
                    onNext: { status in
                        guard status == true else { return }
                        completion()
                        event(.success(()))
                        doneDisposeBag = DisposeBag()
                    }
                )
                .disposed(by: doneDisposeBag)
            pairingDoneViewController.viewModel = pairingDoneViewModel
            setViewControllerTrigger.accept(pairingDoneViewController)
            return Disposables.create()
        }
    }
}
