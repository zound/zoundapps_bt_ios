//
//  HelpMenuFlowModel.swift
//  MarshallBluetooth®
//
//  Created by Paprota Przemyslaw on 13/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HelpMenuFlowModel: BasicMenuFlowModel {

    let helpScreenViewModel = HelpScreenViewModel()
    
    override func start() {
        showSubMenu(viewModel: helpScreenViewModel)
    }
    
    private func showContactScreen() {
        let screen = UIViewController.instantiate(ContactScreenViewController.self)
        let model = ContactScreenViewModel()
        screen.model = model
        showNextScreen(screen)
    }
    
    func showOnlineManual() {
        showSubMenu(viewModel: OnlineManualScreenViewModel())
    }

    func showOnlineManual(for deviceModel: DeviceModel) {
        let screen = UIViewController.instantiate(OnlineManualViewController.self)
        screen.request = OnlineManualUrlRequestBuilder()
            .add(deviceModel)
            .add(Environment().language)
            .request
        showNextScreen(screen)
    }

    func showQuickGuideMenu() {
        showSubMenu(viewModel: QuickGuideMenuViewModel())
    }
    
    func showQuickGuideMenu(for deviceModel: DeviceModel) {
        let isSpeaker = deviceModel != .monitorII
        let viewModel = isSpeaker ? QuickGuideMenuSpeakersViewModel() : QuickGuideMenuHeadphonesViewModel();
        showSubMenu(viewModel: viewModel)
    }
    
    func showQuickGuideScreen(_ nextScreen: NextScreenType) {
        var viewController: UIViewController?
        switch nextScreen {
        case .bluetoothSpeakersGuide:
            viewController = UIViewController.instantiate(BluetoothGuideViewController.self)
        case .playAndPauseGuide:
            viewController = UIViewController.instantiate(PlayPauseGuideViewController.self)
        case .speakersGuide:
            viewController = UIViewController.instantiate(SpeakersGuideViewController.self)
        default:
            let guideView = UIViewController.instantiate(GenericGuideViewController.self)
            guideView.viewModel = GenericGuideViewControllerViewModel(type: nextScreen)
            viewController = guideView
        }
        showNextScreen(viewController!)
    }

    override func handleNextItem( next: NextScreenType) {
        switch next {
        case .quickGuide:
            showQuickGuideMenu()
        case .contact:
            showContactScreen()
        case .onlineManual:
            showOnlineManual()
        case .onlineManualActonII:
            showOnlineManual(for: .actonII)
        case .onlineManualMonitorII:
            showOnlineManual(for: .monitorII)
        case .onlineManualStanmoreII:
            showOnlineManual(for: .stanmoreII)
        case .onlineManualWoburnII:
            showOnlineManual(for: .woburnII)
        case .ancButtonGuide,
             .bluetoothHeadphonesGuide,
             .bluetoothSpeakersGuide,
             .controlKnobGuide,
             .mButtonGuide,
             .playAndPauseGuide,
             .speakersGuide:
            showQuickGuideScreen(next)
        case .quickGuideActonII:
            showQuickGuideMenu(for: .actonII)
        case .quickGuideMonitorII:
            showQuickGuideMenu(for: .monitorII)
        case .quickGuideStanmoreII:
            showQuickGuideMenu(for: .stanmoreII)
        case .quickGuideWoburnII:
            showQuickGuideMenu(for: .woburnII)
        default:
            break
        }
    }
    
    deinit {
        print(#function, String(describing: self))
    }
}
