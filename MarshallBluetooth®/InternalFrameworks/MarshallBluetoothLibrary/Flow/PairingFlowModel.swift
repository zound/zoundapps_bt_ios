//
//  PairingFlowModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 02/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import RxSwift
import RxCocoa
import GUMA

public enum PairingState {
    case inProgress
    case completed(PairingError?)
}
protocol PairingFlowModelInput {
    func perform()
}
protocol PairingFlowModelOutput {
    var pushViewController: PublishSubject<UIViewController> { get }
}
protocol PairingFlowModelType {
    var inputs: PairingFlowModelInput { get }
    var outputs: PairingFlowModelOutput { get }
}
final class PairingFlowModel: FlowModel, PairingFlowModelInput, PairingFlowModelOutput, PairingFlowModelType  {
    
    typealias Dependencies = DeviceServiceDependency & AppModeDependency
    
    static var id: FlowId = .pairing
    var inputs: PairingFlowModelInput { return self }
    var outputs: PairingFlowModelOutput { return self }
    var submodels = [FlowModel]()

    var setViewController = BehaviorRelay<UIViewController?>.init(value: nil)
    var pushViewController = PublishSubject<UIViewController>()
    var popViewController = PublishSubject<Void>()
    var presentViewController = PublishSubject<UIViewController>()
    var dismissViewController = PublishSubject<UIViewController>()
    
    var completed = PublishSubject<FlowId>()
    var error = PublishSubject<(FlowId, PairingError)>()
    var preferences: DefaultStorage
    let dependencies: Dependencies
    let initalState: DevicePairingState
    
    init(dependencies: Dependencies, device: DeviceType, initialPairingState: DevicePairingState, preferences: DefaultStorage = DefaultStorage.shared) {
        self.dependencies = dependencies
        self.preferences = preferences
        self.device = device
        self.initalState = initialPairingState
        current
            .observeOn(MainScheduler.instance)
            .subscribeNext(weak: self, PairingFlowModel.handle)
            .disposed(by: disposeBag)
    }
    func perform() {
        current.accept(.inProgress)
    }
    private var device: DeviceType
    private var current = PublishRelay<PairingState>()
    private var disposeBag = DisposeBag()
}

private extension PairingFlowModel {
    func handle(_ state: PairingState) {
        switch state {
        case .inProgress:
            self.showPairing()
        case let .completed(error):
            guard let error = error else {
                self.completed.onNext(PairingFlowModel.id)
                return
            }
            self.error.onNext((PairingFlowModel.id, error))
        }
    }
    private func showPairing() {
        let pairingViewController = UIViewController.instantiate(PairingViewController.self)
        let pairingViewModel: PairingViewModelType = PairingViewModel(dependencies: dependencies, device: device, initialState: self.initalState)
        pairingViewController.viewModel = pairingViewModel
        pairingViewModel.outputs.completed
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] status in
                switch status {
                case .success:
                    self?.current.accept(.completed(nil))
                    break
                case let .failure(error):
                    self?.current.accept(.completed(error))
                }
            })
            .disposed(by: disposeBag)
        setViewController.accept(pairingViewController)
    }
}
