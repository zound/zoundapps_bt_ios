//
//  BasicMenuFlowModel.swift
//  MarshallBluetooth®
//
//  Created by Paprota Przemyslaw on 13/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//


import UIKit
import RxSwift
import RxCocoa
import GUMA

/// Outputs of Hamburger Menu flow model
protocol BasicMenuFlowModelOutput {
    
    var pushViewController: PublishSubject<UIViewController> { get }
    
    var popViewController: PublishSubject<Void> { get }

    /// Those subjects may be used for presenting/dismissing sub - UIViewControllers if current Flow requires it
    var presentViewController: PublishSubject<UIViewController> { get }
    var dismissViewController: PublishSubject<UIViewController> { get }
    
}

protocol BasicMenuFlowModelInput {
    /// Pushes view controller into navigation bar
    func start()
}

/// Protocol to force input/outputs on view model
protocol BasicMenuFlowModelType {
    /// Input points of the view model
    var inputs: BasicMenuFlowModelInput { get }
    
    /// Output points of the view model
    var outputs: BasicMenuFlowModelOutput { get }
}

public class BasicMenuFlowModel: BasicMenuFlowModelInput, BasicMenuFlowModelOutput, BasicMenuFlowModelType {
    
    var outputs: BasicMenuFlowModelOutput { return self }
    var inputs: BasicMenuFlowModelInput { return self }
    var pushViewController = PublishSubject<UIViewController>()
    var popViewController = PublishSubject<Void>()
    var disposeBag = DisposeBag()
    var presentViewController = PublishSubject<UIViewController>()
    var dismissViewController = PublishSubject<UIViewController>()

    func showSubMenu(viewModel: CommonMenuViewModelType) {
        viewModel.outputs.selectedItem
            .asDriver()
            .skipOne()
            .drive(onNext:{[weak self] item in self?.handleNextItem(next: item)})
            .disposed(by: disposeBag)
        
        let screen = UIViewController.instantiate(CommonMenuViewController.self)
        screen.viewModel = viewModel
        pushViewController.onNext(screen)
    }
    
    func start() {}
    
    func handleNextItem(next: NextScreenType) {}
    
    func showNextScreen(_ screen: UIViewController) {
        pushViewController.onNext(screen)
    }

    deinit {
        print(#function, String(describing: self))
    }
}
