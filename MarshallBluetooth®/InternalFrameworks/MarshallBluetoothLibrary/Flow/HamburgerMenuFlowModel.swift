//
//  HamburgerMenuFlowModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 23.04.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import RxCocoa
import GUMA
import Zound




/// Different positions from menu to be tapped
///
/// - settings: go to Settings view
/// - help: go to Help view
/// - shop: go to Shop view
/// - about: go to About view
public enum Menu {
    case settings
    case help
    case shop
    case about
}

/// Inputs of Hamburger Menu flow model
protocol HamburgerMenuFlowModelInput {
    func menuButtonSelected()
}

/// Outputs of Hamburger Menu flow model
protocol HamburgerMenuFlowModelOutput {
    /// Pushes view controller into navigation bar
    var pushViewController: PublishSubject<UIViewController> { get }

    var popViewController: PublishSubject<Void> { get }

    /// Those subjects may be used for presenting/dismissing sub - UIViewControllers if current Flow requires it
    var presentViewController: PublishSubject<UIViewController> { get }
    var dismissViewController: PublishSubject<UIViewController> { get }

    var inProgress: PublishSubject<Bool> { get }
}

/// Protocol to force input/outputs on view model
protocol HamburgerMenuFlowModelType {
    /// Input points of the view model
    var inputs: HamburgerMenuFlowModelInput { get }
    
    /// Output points of the view model
    var outputs: HamburgerMenuFlowModelOutput { get }
}

/// Implementation of SignupFlow flow model
public final class HamburgerMenuFlowModel: HamburgerMenuFlowModelInput, HamburgerMenuFlowModelOutput, HamburgerMenuFlowModelType {
    
    /// Id that identificates this flow among other in apps
    var inputs: HamburgerMenuFlowModelInput { return self }
    var outputs: HamburgerMenuFlowModelOutput { return self }

    var completed: PublishRelay<FlowId?> = PublishRelay<FlowId?>()

    var presentViewController = PublishSubject<UIViewController>()
    var dismissViewController = PublishSubject<UIViewController>()

    private var selectedItem: BehaviorRelay<Menu?> = BehaviorRelay.init(value: nil)
    
    typealias Dependencies = DeviceAdapterServiceDependency & AnalyticsServiceDependency & NewsletterServiceDependency
    
    private var disposeBag = DisposeBag()

    var submodels = [FlowModel]()

    var preferences: DefaultStorage

    var inProgress = PublishSubject<Bool>()

    var parent: UIViewController

    var pushViewController = PublishSubject<UIViewController>()

    var popViewController = PublishSubject<Void>()

    private let menuViewController: HamburgerMenuViewController

    private let menuViewModel = HamburgerMenuViewModel()

    private let menuTransitionDelegate = MenuTransitioningDelegate()

    private var currentFlow: BasicMenuFlowModel?

    private let dependencies: Dependencies
    
    init(dependencies: Dependencies, parent: UIViewController, preferences: DefaultStorage = DefaultStorage.shared) {
        self.dependencies = dependencies
        self.preferences = preferences
        self.parent = parent
        menuViewController = UIViewController.instantiate(HamburgerMenuViewController.self)
        menuViewController.viewModel = menuViewModel
        menuViewController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        menuViewController.transitioningDelegate = menuTransitionDelegate
        
        menuViewModel.outputs.closed
            .asDriver()
            .skipOne()
            .drive(onNext: {[weak self] _ in self?.hideMenu()})
            .disposed(by: disposeBag)

        menuViewModel.outputs.help
            .asDriver()
            .skipOne()
            .drive(onNext: {[weak self] _ in self?.showHelp()})
            .disposed(by: disposeBag)

        menuViewModel.outputs.about
            .asDriver()
            .skipOne()
            .drive(onNext: {[weak self] _ in self?.showAbout()})
            .disposed(by: disposeBag)

        menuViewModel.outputs.settings
            .asDriver()
            .skipOne()
            .drive(onNext: {[weak self] _ in self?.showSettings()})
            .disposed(by: disposeBag)

        menuViewModel.outputs.shop
            .asDriver()
            .skipOne()
            .drive(onNext: {[weak self] _ in self?.showShop()})
            .disposed(by: disposeBag)
    }

    func showMenu(withCompletion completion: @escaping () -> Void = {}) {
        parent.present(menuViewController, animated: true, completion: {
            completion()
        })
    }

    func hideMenu(withCompletion completion: @escaping () -> Void = {}) {
        parent.dismiss(animated: true, completion: {
            completion()
        })
    }

    func menuButtonSelected() {
        showMenu()
    }

    private func showSubMenu(_ flow: BasicMenuFlowModel) {
        flow.pushViewController
            .subscribe(pushViewController)
            .disposed(by: disposeBag)
        flow.popViewController
            .subscribe(popViewController)
            .disposed(by: disposeBag)
        flow.presentViewController
            .subscribe(presentViewController)
            .disposed(by: disposeBag)
        flow.dismissViewController
            .subscribe(dismissViewController)
            .disposed(by: disposeBag)
        currentFlow = flow
        flow.start()
        hideMenu()
    }

    private func showSettings() {
        showSubMenu(SettingsMenuFlowModel(dependencies: dependencies))
    }

    private func showHelp() {
        showSubMenu(HelpMenuFlowModel())
    }

    private func showAbout() {
        showSubMenu(AboutMenuFlowModel())
    }

    private func showShop() {
        hideMenu { [weak self] in
            self?.parent.present(SafariViewController.with(contentType: .shop), animated: true)
        }
    }

    deinit {
        print(#function, String(describing: self))
    }
}

