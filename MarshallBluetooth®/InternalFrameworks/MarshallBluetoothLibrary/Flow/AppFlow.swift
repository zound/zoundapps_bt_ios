//
//  File.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 12/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import RxCocoa
import UIKit
import GUMA

public final class AppFlow {
    
   public init(with model: AppFlowModelType, rootViewController: RootViewController) {
        
        self.model = model
        self.rootViewController = rootViewController
        
        UIFont.registerFont(name: "RobotoCondensed-Light")
        UIFont.registerFont(name: "RobotoCondensed-Bold")
        UIFont.registerFont(name: "RobotoCondensed-Regular")

        clearDefaultStorageItemsIfNeeded()
        bindModel()
    }

    private func clearDefaultStorageItemsIfNeeded() {
        DefaultStorage.shared.lastConfiguredCouplePairs.removeAll()
    }

    private func bindModel() {
        self.model.outputs.setViewController
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] viewController in
                self.currentViewController = viewController
                self.rootViewController.showChildViewController(viewController, animated: false)
            })
            .disposed(by: disposeBag)
    
        self.model.outputs.pushViewController
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] viewController in
                guard let navigationController = self.currentViewController as? UINavigationController else { return }
                navigationController.pushViewController(viewController, animated: true)
            })
            .disposed(by: disposeBag)
        
        self.model.outputs.presentViewController
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] viewController in
                viewController.modalPresentationStyle = .fullScreen
                self.currentViewController?.present(viewController, animated: true)
            })
            .disposed(by: disposeBag)
        
        self.model.outputs.dismissViewController
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { viewController in
                viewController.dismiss(animated: true)
            })
            .disposed(by: disposeBag)
    }
    
    // Flushes all collected observables
    private func flush() {
        disposeBag = DisposeBag()
    }
    
    private var model: AppFlowModelType
    private var currentViewController: UIViewController?
    private let rootViewController: RootViewController
    private var disposeBag = DisposeBag()
}
