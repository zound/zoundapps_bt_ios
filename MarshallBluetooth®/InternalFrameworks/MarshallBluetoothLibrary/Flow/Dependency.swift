//
//  Dependency.swift
//  MarshallBluetoothLibrary
//
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import GUMA
import RxSwift

public enum AppMode {
    case foreground
    case background
}

protocol DeviceServiceDependency {
    var deviceService: DeviceServiceType { get }
}

protocol OTAServiceDependency {
    var otaService: OTAServiceType { get }
}

protocol DeviceAdapterServiceDependency {
    var deviceAdapterService: DeviceAdapterServiceType { get }
}

protocol AnalyticsServiceDependency {
    var analyticsService: AnalyticsServiceType { get }
}

protocol AppModeDependency {
    var appMode: BehaviorRelay<AppMode> { get }
}

protocol NewsletterServiceDependency {
    var newsletterService: NewsletterService { get }
}


public struct AppDependency: DeviceServiceDependency,
                             OTAServiceDependency,
                             DeviceAdapterServiceDependency,
                             AnalyticsServiceDependency,
                             AppModeDependency,
                             NewsletterServiceDependency
                            {
    let deviceService: DeviceServiceType
    let otaService: OTAServiceType
    let deviceAdapterService: DeviceAdapterServiceType
    var appMode: BehaviorRelay<AppMode>
    let analyticsService: AnalyticsServiceType
    let newsletterService: NewsletterService
    
    public init(deviceService: DeviceServiceType,
                otaService: OTAServiceType,
                deviceAdapterService: DeviceAdapterServiceType,
                appMode: BehaviorRelay<AppMode>,
                analyticsService: AnalyticsServiceType,
                newsletterService: NewsletterService) {
        self.deviceService = deviceService
        self.otaService = otaService
        self.deviceAdapterService = deviceAdapterService
        self.appMode = appMode
        self.analyticsService = analyticsService
        self.newsletterService = newsletterService
    }
}

