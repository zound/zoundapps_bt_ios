//
//  OnboardingFlowModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 08/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import RxSwift
import RxCocoa
import GUMA

public enum OnboardingState {
    case displayAdvertisement
    case completed
}
protocol OnboardingFlowModelInput {
    func perform()
}
protocol OnboardingFlowModelOutput {
    var pushViewController: PublishSubject<UIViewController> { get }
}
protocol OnboardingFlowModelType {
    var inputs: OnboardingFlowModelInput { get }
    var outputs: OnboardingFlowModelOutput { get }
}
final class OnboardingFlowModel: FlowModel, OnboardingFlowModelInput, OnboardingFlowModelOutput, OnboardingFlowModelType {
    
    typealias Dependencies = DeviceServiceDependency
    
    static var id: FlowId = .onboarding
    var inputs: OnboardingFlowModelInput { return self }
    var outputs: OnboardingFlowModelOutput { return self }
    var submodels = [FlowModel]()

    var setViewController = BehaviorRelay<UIViewController?>.init(value: nil)
    var pushViewController = PublishSubject<UIViewController>()
    var popViewController = PublishSubject<Void>()
    var presentViewController = PublishSubject<UIViewController>()
    var dismissViewController = PublishSubject<UIViewController>()
    
    var completed = PublishSubject<FlowId>()
    var preferences: DefaultStorage
    let dependencies: Dependencies
    
    init(dependencies: Dependencies, device: DeviceType, preferences: DefaultStorage = DefaultStorage.shared) {
        self.dependencies = dependencies
        self.preferences = preferences
        self.device = device
        
        current
            .observeOn(MainScheduler.instance)
            .subscribeNext(weak: self, OnboardingFlowModel.handle)
            .disposed(by: disposeBag)
    }
    func perform() {
        current.accept(.displayAdvertisement)
    }
    private var device: DeviceType
    private var current = PublishRelay<OnboardingState>()
    private var disposeBag = DisposeBag()
}

private extension OnboardingFlowModel {
    func handle(_ state: OnboardingState) {
        switch state {
        case .displayAdvertisement:
            showAdvertisement(device: device)
        case .completed:
            self.completed.onNext(OnboardingFlowModel.id)
        
        }
    }
    func showAdvertisement(device: DeviceType) {
        let advertisementController = UIViewController.instantiate(OzzyAdvertisementViewController.self)
        advertisementController.configure(withAdvertisementControllers: AdvertisementPage.ozzyViewControllers())
        let advertisementViewModel: OzzyAdvertisementViewModelType = OzzyAdvertisementViewModel(device: device)
        advertisementController.viewModel = advertisementViewModel
        advertisementViewModel.outputs.skip
            .subscribe(onNext: { [weak self] in
                self?.completed.onNext(OnboardingFlowModel.id)
            })
            .disposed(by: disposeBag)
        setViewController.accept(advertisementController)
    }
}

