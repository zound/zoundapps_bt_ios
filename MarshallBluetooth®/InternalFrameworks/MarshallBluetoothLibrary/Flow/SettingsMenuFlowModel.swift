//
//  SettingsMenuFlowModel.swift
//  MarshallBluetooth®
//
//  Created by Paprota Przemyslaw on 13/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import GUMA

class SettingsMenuFlowModel: BasicMenuFlowModel {

    typealias Dependencies = AnalyticsServiceDependency & NewsletterServiceDependency

    private weak var currentStayUpdatedModel: StayUpdatedViewModel?
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    override func start() {
        showSubMenu(viewModel: SettingsScreenViewModel())
    }

    func unsubscribeConfirmation() {
        let screen = UIViewController.instantiate(UnsubscribeConfirmationViewController.self)
        let model = UnsubscribeConfirmationViewModel()
        model.canceled
            .asDriver()
            .skipOne()
            .drive(onNext: { [weak self] _ in
                self?.dismissViewController.onNext(screen)
                self?.currentStayUpdatedModel?.inputs.unsubscribeConfirmed(isConfirmed: false)
            })
            .disposed(by: disposeBag)
        model.confirmed
            .asDriver()
            .skipOne()
            .drive(onNext: { [weak self] _ in
                self?.dismissViewController.onNext(screen)
                self?.currentStayUpdatedModel?.inputs.unsubscribeConfirmed(isConfirmed: true)
            })
            .disposed(by: disposeBag)
        screen.viewModel = model
        presentViewController.onNext(screen)

    }

    func emailSubscription() {
        let viewController = UIViewController.instantiate(StayUpdatedViewController.self)
        let viewModel = StayUpdatedViewModel(setup: false, dependencies: dependencies)
        viewController.viewModel = viewModel
        
        viewModel.viewCompleted
            .asDriver()
            .skipOne()
            .drive(onNext: { [weak self] _ in
                self?.popViewController.onNext(())
            })
            .disposed(by: disposeBag)
        
        viewModel.unsubscribeRequested
            .asDriver()
            .skipOne()
            .drive(onNext: { [weak self] _ in
                self?.unsubscribeConfirmation()
            })
            .disposed(by: disposeBag)
        
        currentStayUpdatedModel = viewModel
        showNextScreen(viewController)
    }
    
    func showAnalytics() {
        let signupShareData = UIViewController.instantiate(ShareDataViewController.self)
        let shareDataViewModel = ShareDataViewModel(wizardMode: false, dependencies: dependencies, preferences: DefaultStorage.shared)
        signupShareData.viewModel = shareDataViewModel
        
        shareDataViewModel.outputs.viewCompleted
            .asDriver()
            .skipOne()
            .drive(onNext: { [weak self] _ in
                self?.popViewController.onNext(())
            })
            .disposed(by: disposeBag)
        
        showNextScreen(signupShareData)
    }
    
    override func handleNextItem( next: NextScreenType) {
        switch next {
        case .signUp:
            emailSubscription()
        case .analytics:
            showAnalytics()
        default:
            break
        }
    }
    
    deinit {
        print(#function, String(describing: self))
    }
    
}
