//
//  SignupFlowModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 20/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import RxCocoa
import GUMA

/// Possible states for SignUp flow that represents every possible view (screen)
///
/// - stayUpdated: represents StayUpdatedViewController
/// - shareData: represents ShareDataViewController
/// - termsOfUse: represents TermsAndConditionsViewController
/// - saveData: represents SaveDataViewController
/// - completed: notify the upper flow that this SignUp flow is finished
public enum SignupState {
    case stayUpdated
    case shareData
    case saveData
    case completed
    
    /// Switches states into another one according to flow in Sketch files when user click Next button.
    /// It does not include termsOfUse state because it's a "dead-end" in flow. This case must be set up manually.
    ///
    /// - Returns: Next state according to current one
    func next() -> SignupState {
        switch self {
        case .stayUpdated: return .shareData
        case .shareData: return .saveData
        case .saveData: return .completed
        default: break
        }
        return .completed
    }
}

/// Input points of SignupFlow's flow model
protocol SignupFlowModelInput {
    
    /// Changes internal state of view controllers
    ///
    /// - Parameter state: State related to views from Sketch file
    func changeState(state: SignupState)
}

/// Output points of SignupFlow's flow model
protocol SignupFlowModelOutput {
    
    /// Pushes view controller into navigation bar
    var pushViewController: PublishSubject<UIViewController> { get }
}

protocol SignupFlowModelType {
    var inputs: SignupFlowModelInput { get }
    var outputs: SignupFlowModelOutput { get }
}

/// Implementation of SignupFlow flow model
public final class SignupFlowModel: FlowModel, SignupFlowModelInput, SignupFlowModelOutput, SignupFlowModelType  {

    typealias Dependencies = AnalyticsServiceDependency & NewsletterServiceDependency

    /// Id that identificates this flow among other in apps
    static var id: FlowId = .signup
    
    var inputs: SignupFlowModelInput { return self }
    var outputs: SignupFlowModelOutput { return self }
    
    var submodels = [FlowModel]()

    var setViewController = BehaviorRelay<UIViewController?>.init(value: nil)
    
    var pushViewController = PublishSubject<UIViewController>()
    var popViewController = PublishSubject<Void>()
    
    var presentViewController = PublishSubject<UIViewController>()
    var dismissViewController = PublishSubject<UIViewController>()

    var completed: PublishSubject<FlowId> = PublishSubject<FlowId>()
    var preferences: DefaultStorage
    let dependencies: Dependencies

    private weak var currentStayUpdatedScreen: StayUpdatedViewController?
    
    init(dependencies: Dependencies, preferences: DefaultStorage = DefaultStorage.shared) {
        self.dependencies = dependencies
        self.preferences = preferences
        
        // Submodule may be deallocated at some point, thus we use weak in this case
        current
            .asDriver()
            .drive(onNext: { [weak self] state in
                switch state {
                case .stayUpdated:
                    self?.showStayUpdated()
                case .shareData:
                    self?.showShareData()
                case .saveData:
                    self?.showSaveData()
                case .completed:
                    self?.completed.onNext(SignupFlowModel.id)
                }
            }).disposed(by: disposeBag)
    }
    
    func changeState(state: SignupState) {
        current.value = state
    }


    func unsubscribeConfirmation() {
        let screen = UIViewController.instantiate(UnsubscribeConfirmationViewController.self)
        let model = UnsubscribeConfirmationViewModel()
        model.canceled
            .asDriver()
            .skipOne()
            .drive(onNext: { [weak self] _ in
                screen.dismiss(animated: true)
                self?.currentStayUpdatedScreen?.viewModel?.inputs.unsubscribeConfirmed(isConfirmed: false)
            })
            .disposed(by: disposeBag)
        model.confirmed
            .asDriver()
            .skipOne()
            .drive(onNext: { [weak self] _ in
                screen.dismiss(animated: true)
                self?.currentStayUpdatedScreen?.viewModel?.inputs.unsubscribeConfirmed(isConfirmed: true)
            })
            .disposed(by: disposeBag)
        screen.viewModel = model
        screen.modalPresentationStyle = .fullScreen
        self.currentStayUpdatedScreen?.present(screen, animated: true)
    }
    
    private func showStayUpdated() {
        let viewController = UIViewController.instantiate(StayUpdatedViewController.self)
        let viewModel = StayUpdatedViewModel(setup: true, dependencies: dependencies)
        viewController.viewModel = viewModel
        
        let navigationController = UINavigationController()
        navigationController.navigationBar.isHidden = true
        navigationController.setViewControllers([viewController], animated: false)

        setViewController.accept(navigationController)
        
        viewModel.skipped
            .asDriver()
            .skipOne()
            .drive(onNext: { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.current.value = SignupState.stayUpdated.next()
            })
            .disposed(by: disposeBag)
        
        viewModel.viewCompleted
            .asDriver()
            .skipOne()
            .drive(onNext: { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.current.value = SignupState.stayUpdated.next()
            })
            .disposed(by: disposeBag)

        viewModel.unsubscribeRequested
            .asDriver()
            .skipOne()
            .drive(onNext: { [weak self] _ in
                self?.unsubscribeConfirmation()
            })
            .disposed(by: disposeBag)

        currentStayUpdatedScreen = viewController
    }
    
    private func showShareData() {
        let signupShareData = UIViewController.instantiate(ShareDataViewController.self)
        let shareDataViewModel = ShareDataViewModel(wizardMode: true, dependencies: dependencies, preferences: preferences)
        signupShareData.viewModel = shareDataViewModel
        pushViewController.onNext(signupShareData)

        shareDataViewModel.viewCompleted
            .asDriver()
            .skipOne()
            .drive(onNext: { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.current.value = SignupState.shareData.next()
            })
            .disposed(by: disposeBag)
    }
    
    private func showSaveData() {
        let saveData = UIViewController.instantiate(SaveDataViewController.self)
        let saveDataViewModel = SaveDataViewModel()
        saveData.viewModel = saveDataViewModel
        pushViewController.onNext(saveData)
        
        saveDataViewModel.viewCompleted
            .asDriver()
            .skipOne()
            .drive(onNext: { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.current.value = .completed
            })
            .disposed(by: disposeBag)
    }

    private var current: Variable<SignupState> = Variable(.stayUpdated)
    private var disposeBag = DisposeBag()
}

