//
//  AboutMenuFlowModel.swift
//  MarshallBluetooth®
//
//  Created by Paprota Przemyslaw on 13/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

public class AboutMenuFlowModel: BasicMenuFlowModel {

    override func start() {
        showSubMenu(viewModel: AboutScreenViewModel())
    }

    func showFoss() {
        let screen = UIViewController.instantiate(FossScreenViewController.self)
        let model = FossScreenViewModel()

        screen.viewModel = model
        showNextScreen(screen)
    }

    func showUserLicence() {
        let screen = UIViewController.instantiate(InAppTermsAndConditionsViewController.self)
        showNextScreen(screen)
    }

    override func handleNextItem(next: NextScreenType) {
        switch next {
        case .endUserLicence:
            showUserLicence()
        case .freeAndOpenSource:
            showFoss()
        default:
            break
        }
    }
    
    deinit {
        print(#function, String(describing: self))
    }
}
