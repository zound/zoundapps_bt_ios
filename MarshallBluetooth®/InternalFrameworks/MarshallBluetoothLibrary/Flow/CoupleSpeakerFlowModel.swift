//
//  CoupleSpeakerFlowModel.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 11/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

private enum CoupleSpeakerState {
    case list
    case modeSelection
    case modeConfiguration(CoupleSpeakerMode)
    case headsUp
    case completed
    case aborted

    init() {
        self = .list
    }
}

protocol CoupleSpeakerFlowModelInput {
    func pushCoupleSpeaker()
}

protocol CoupleSpeakerFlowModelOutput {
}

protocol CoupleSpeakerFlowModelType {
    var inputs: CoupleSpeakerFlowModelInput { get }
    var outputs: CoupleSpeakerFlowModelOutput { get }
}

final class CoupleSpeakerFlowModel: FlowModel {
    typealias Dependencies = DeviceServiceDependency & AnalyticsServiceDependency
    static var id: FlowId = .coupleSpeaker
    var submodels: [FlowModel] = []
    var setViewController = BehaviorRelay<UIViewController?>.init(value: nil)
    var pushViewController = PublishSubject<UIViewController>()
    var popViewController = PublishSubject<Void>()
    var presentViewController = PublishSubject<UIViewController>()
    var dismissViewController = PublishSubject<UIViewController>()
    var preferences: DefaultStorage
    var completed: PublishSubject<FlowId> = PublishSubject<FlowId>()
    var aborted: PublishSubject<FlowId> = PublishSubject<FlowId>()

    private var slaveInfo: SlaveInfo = (id: String(), couplingId: Data(), name: String(), image: UIImage())

    private let device: DeviceType
    private let dependencies: Dependencies
    private var currentState = PublishSubject<CoupleSpeakerState>()
    private let disposeBag = DisposeBag()

    init(device: DeviceType, dependencies: Dependencies, preferences: DefaultStorage = DefaultStorage.shared) {
        self.device = device
        self.dependencies = dependencies
        self.preferences = preferences
        currentState
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] coupleSpeakerState in
                switch coupleSpeakerState {
                case .list:
                    self?.showList()
                case .modeSelection:
                    self?.showModeSelection()
                case .modeConfiguration(let mode):
                    self?.showModeConfiguration(mode)
                case .headsUp:
                    self?.showHeadsUp()
                case .completed:
                    self?.complete()
                case .aborted:
                    self?.abort()
                }
            }).disposed(by: disposeBag)
    }

    deinit {
        print(#function, String(describing: self))
    }
}

private extension CoupleSpeakerFlowModel {

    func showList() {
        let viewModel = CoupleSpeakerListViewModel(device: device, dependencies: dependencies, preferences: preferences)
        let viewController = UIViewController.instantiate(CoupleSpeakerListViewController.self)
        viewController.viewModel = viewModel

        viewModel.outputs.selected
            .subscribe(onNext: { [weak self] slaveInfo in
                self?.slaveInfo = slaveInfo
                self?.currentState.onNext(.modeSelection)
            }).disposed(by: disposeBag)

        viewModel.outputs.unpaired
            .subscribe(onCompleted: { [weak self] in
                self?.currentState.onNext(.completed)
            }).disposed(by: disposeBag)

        viewModel.outputs.completed
            .subscribe(onCompleted: { [weak self] in
                self?.currentState.onNext(.completed)
            }).disposed(by: disposeBag)

        viewModel.outputs.aborted
            .subscribe(onCompleted: { [weak self] in
                self?.currentState.onNext(.aborted)
            }).disposed(by: disposeBag)

        pushViewController.onNext(viewController)
    }

    func showModeSelection() {
        let viewModel = CoupleSpeakerModeSelectionViewModel(device: device, dependencies: dependencies)
        let viewController = UIViewController.instantiate(CoupleSpeakerModeSelectionViewController.self)
        viewController.viewModel = viewModel

        viewModel.outputs.modeSelected
            .subscribe(onNext: { [weak self] mode in
                self?.currentState.onNext(.modeConfiguration(mode))
            }).disposed(by: disposeBag)

        pushViewController.onNext(viewController)
    }

    func showModeConfiguration(_ mode: CoupleSpeakerMode) {
        let viewModel = CoupleSpeakerModeConfigurationViewModel(device: device, slaveInfo: slaveInfo, coupleSpeakerMode: mode, dependencies: dependencies)
        let viewController = UIViewController.instantiate(CoupleSpeakerModeConfigurationViewController.self)
        viewController.viewModel = viewModel

        viewController.coupledSpeakers = [
            CoupleSpeakerListItem(name: device.state.outputs.name().stateObservable().value, image: device.image.smallImage, modeConfiguration: mode.defaultCoupleSpeakerModeConfiguration),
            CoupleSpeakerListItem(name: slaveInfo.name, image: slaveInfo.image, modeConfiguration: mode.defaultOppositeCoupleSpeakerModeConfiguration)
        ]

        viewModel.outputs.completed
            .subscribe(onCompleted: { [weak self] in
                self?.currentState.onNext(.headsUp)
            }).disposed(by: disposeBag)

        pushViewController.onNext(viewController)
    }

    func showHeadsUp() {
        let viewModel = CoupleSpeakerHeadsUpViewModel(dependencies: dependencies)
        let viewController = UIViewController.instantiate(CoupleSpeakerHeadsUpViewController.self)
        viewController.viewModel = viewModel

        viewModel.outputs.completed
            .subscribe(onCompleted: { [weak self] in
                self?.currentState.onNext(.completed)
            }).disposed(by: disposeBag)

        pushViewController.onNext(viewController)
    }

    func complete() {
        completed.onNext(CoupleSpeakerFlowModel.id)
    }

    func abort() {
        aborted.onNext(CoupleSpeakerFlowModel.id)
    }

}

extension CoupleSpeakerFlowModel: CoupleSpeakerFlowModelType {
    var inputs: CoupleSpeakerFlowModelInput { return self }
    var outputs: CoupleSpeakerFlowModelOutput { return self }
}

extension CoupleSpeakerFlowModel: CoupleSpeakerFlowModelInput {
    func pushCoupleSpeaker() {
        currentState.onNext(CoupleSpeakerState())
    }
}

extension CoupleSpeakerFlowModel: CoupleSpeakerFlowModelOutput {
}
