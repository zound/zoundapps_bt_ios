//
//  HomeFlowModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 25/04/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

struct SilentConnectDisposableEntry {
    var deviceID: String
    var disposeBag: DisposeBag
}

enum DeviceSetupError: Error {
    case noInstance
    case forcedOTAInProgress
    case generalForcedOTAError
}

private struct Config {
    static let trueWirelessRequestTimeout = TimeInterval.seconds(5.0)
    static let throttleTime = TimeInterval.seconds(1.0)
}

/// Different actions for Home view
///
/// - displayDeviceList: show list of discovered devices
public enum HomeState {
    case displayDeviceList
}

/// Inputs of Home flow model
protocol HomeFlowModelInputs {
}

/// Outputs of Home flow model
protocol HomeFlowModelOutputs {
    /// Sets new local root view controller
    var setViewController: BehaviorRelay<UIViewController?> { get }
    
    /// Those subjects may be used for presenting/dismissing sub - UIViewControllers if current Flow requires it
    var presentViewController: PublishSubject<UIViewController> { get }
    var dismissViewController: PublishSubject<UIViewController> { get }
}

protocol HomeFlowModelType {
    var inputs: HomeFlowModelInputs { get }
    var outputs: HomeFlowModelOutputs { get }
}

typealias ConfigurationStep = (DeviceType) -> Single<Void>

/// Implementation of Home flow model
final class HomeFlowModel: FlowModel, HomeFlowModelInputs, HomeFlowModelOutputs, HomeFlowModelType {
    
    
    /// Id that identificates this flow among other in apps
    static var id: FlowId = .home
    
    var submodels = [FlowModel]()
    
    public var setViewController = BehaviorRelay<UIViewController?>.init(value: nil)
    public var pushViewController = PublishSubject<UIViewController>()
    var popViewController = PublishSubject<Void>()
    
    var presentViewController = PublishSubject<UIViewController>()
    var dismissViewController = PublishSubject<UIViewController>()
    
    var preferences: DefaultStorage = DefaultStorage.shared
    
    var completed: PublishSubject<FlowId> = PublishSubject<FlowId>()
    
    var inputs: HomeFlowModelInputs { return self }
    var outputs: HomeFlowModelOutputs { return self }
    
    private var devicesListViewModelType: DevicesListViewModelType?
    
    typealias Dependencies = DeviceServiceDependency &
        OTAServiceDependency &
        DeviceAdapterServiceDependency &
        AppModeDependency &
        AnalyticsServiceDependency &
        NewsletterServiceDependency
    
    private let menuFlow : HamburgerMenuFlowModelType
    private var devicesBeingConfiguredIds = [String]()
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
        localRootViewController = UIViewController.instantiate(HomeViewController.self)
        navigationController = BaseNavigationController()
        navigationController.navigationBar.isHidden = true
        menuFlow = HamburgerMenuFlowModel(dependencies: dependencies, parent: localRootViewController)
        
        localRootViewController.viewModel = RootViewModel()

        localRootViewController.viewModel.outputs.menuVisible
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext:{[weak self] in
                self?.menuFlow.inputs.menuButtonSelected()
            })
            .disposed(by: disposeBag)
        
        menuFlow.outputs.presentViewController
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] viewController in
                viewController.modalPresentationStyle = .fullScreen
                self?.navigationController.present(viewController, animated: true)
            }).disposed(by: disposeBag)
        
        menuFlow.outputs.dismissViewController
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { viewController in
                viewController.dismiss(animated: true)
            }).disposed(by: disposeBag)
        
        menuFlow.outputs.pushViewController
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] viewController in
                self?.navigationController.pushViewController(viewController, animated: true)
            }).disposed(by: disposeBag)
        
        menuFlow.outputs.popViewController
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.navigationController.popViewController(animated: true)
            }).disposed(by: disposeBag)
        
        // Submodule may be deallocated at some point, thus we use weak in this case
        current
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] state in
                switch state {
                case .displayDeviceList:
                    self?.displayDevicesList()
                }
            }).disposed(by: disposeBag)
    }

    private var localRootViewController: HomeViewController
    
    private let navigationController: BaseNavigationController
    private var current = BehaviorRelay<HomeState>.init(value: .displayDeviceList)
    private let dependencies: Dependencies
    private var disposeBag = DisposeBag()
    private var displayDevicesListDisposeBag = DisposeBag()
    private var silentConnectDisposables = [SilentConnectDisposableEntry]()
    private var waitingDisposeBag = DisposeBag()
    private var deviceSettingsDisposeBag = DisposeBag()
}
    
private extension HomeFlowModel {
    
    private func showBluetoothError() {
        let viewController = UIViewController.instantiate(ErrorViewController.self)
        let errorViewModel = ErrorViewModel(errorInfo: BluetoothErrorInfo(), dependencies: dependencies)
        errorViewModel.outputs.errorInfo.action(type: .gotoSettings) { _ in
            AppEnvironment.goToSettings()
        }
        viewController.viewModel = errorViewModel
        setViewController.accept(viewController)
    }
    private func displayDevicesList() {
        
        displayDevicesListDisposeBag = DisposeBag()
        
        let devicesListViewModel = DevicesListViewModel(dependencies: self.dependencies, preferences: preferences)
        let devicesListViewController = UIViewController.instantiate(DevicesListViewController.self)
        devicesListViewController.viewModel  = devicesListViewModel
        devicesListViewModelType = devicesListViewModel
        
        navigationController.setViewControllers([devicesListViewController], animated: false)
        localRootViewController.showChildViewController(navigationController, animated: false)
        
        dependencies.deviceAdapterService.outputs.adapterState
            .skip(1)
            .distinctUntilChanged()
            .subscribe(
                onNext: { [weak self] state in
                    switch state {
                    case let .error(adapterError):
                        if adapterError == .btInterfaceDisabled {
                            self?.showBluetoothError()
                        }
                    default:
                        guard self?.setViewController.value === self?.localRootViewController else {
                            self?.setViewController.accept(self?.localRootViewController)
                            return
                        }
                    }
                }
            )
            .disposed(by: displayDevicesListDisposeBag)
        devicesListViewModel.outputs.menuRequested
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.menuFlow.inputs.menuButtonSelected()
            })
            .disposed(by: displayDevicesListDisposeBag)
        
        devicesListViewModel.outputs.settingsRequested
            .asObservable()
            .throttle(Config.throttleTime, latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] device in
                self?.showDeviceSettings(for: device.id)
            })
            .disposed(by: displayDevicesListDisposeBag)

        devicesListViewModel.outputs.featureViewRequested
        .asObservable()
        .throttle(Config.throttleTime, latest: false, scheduler: MainScheduler.instance)
        .subscribe(onNext: { [weak self] (device, feature) in
            self?.showDedicatedView(for: feature, and: device.id)
        })
        .disposed(by: displayDevicesListDisposeBag)
        
        devicesListViewModel.outputs.configureDevice
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] device in
                self?.startConfiguring(device)
            })
            .disposed(by: displayDevicesListDisposeBag)
        
        devicesListViewModel.outputs.decoupleDevices
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] device in
                self?.decouple(master: device)
            })
            .disposed(by: displayDevicesListDisposeBag)

        devicesListViewModel.outputs.playerSourcesRequested
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: showPlayer)
            .disposed(by: displayDevicesListDisposeBag)
        
        setViewController.accept(localRootViewController)
    }

    private func showDedicatedView(for feature: Feature, and deviceId: String) {
        switch feature {
        case .hasEqualizerSettings:
            showEqualizerSettings(for: deviceId)
        case .hasActiveNoiceCancellingSettings:
            showActiveNoiceCancellingSettings(for: deviceId)
        default:
            break
        }
    }

    private func showEqualizerSettings(for deviceId: String) {
        guard let device = dependencies.deviceAdapterService.outputs.managedDevices.first(where: { $0.id == deviceId }) else {
            fatalError("Requested settings for non-existing device!")
        }
        let viewModel = EqualizerSettingsViewModel(device: device, dependencies: dependencies, preferences: preferences)
        let viewController = UIViewController.instantiate(EqualizerSettingsViewController.self)
        viewController.viewModel = viewModel

        viewModel.outputs.active.asObservable()
            .observeOn(MainScheduler.instance)
            .skipOne()
            .skipWhile { $0 }
            .subscribe { [weak self] _ in
                self?.popViewController.onNext(()) }
            .disposed(by: disposeBag)

        navigationController.pushViewController(viewController, animated: true)
    }
    private func showActiveNoiceCancellingSettings(for deviceId: String) {
        guard let device = dependencies.deviceAdapterService.outputs.managedDevices.first(where: { $0.id == deviceId }) else {
            fatalError("Requested settings for non-existing device!")
        }
        let viewModel = ActiveNoiceCancellingViewModel(device: device, dependencies: dependencies)
        let viewController = UIViewController.instantiate(ActiveNoiceCancellingViewController.self)
        viewController.viewModel = viewModel

        viewModel.outputs.active.asObservable()
            .observeOn(MainScheduler.instance)
            .skipOne()
            .skipWhile { $0 }
            .subscribe { [weak self] _ in
                self?.popViewController.onNext(()) }
            .disposed(by: disposeBag)

        navigationController.pushViewController(viewController, animated: true)
    }
    
    private func showDeviceSettings(for deviceID: String) {
        
        deviceSettingsDisposeBag = DisposeBag()
        
        guard let device = dependencies.deviceService.outputs.devices.value.first(where: { $0.id == deviceID }) else {
            fatalError("Requested settings for non-existing device!")
        }
        let activeDevice = BehaviorRelay<DeviceType>(value: device)
        
        dependencies.deviceService.outputs.updated
            .filter { (device, _) in
                device.id == deviceID
            }
            .subscribe(onNext: { (device, _) in
                activeDevice.accept(device)
            })
            .disposed(by: deviceSettingsDisposeBag)
        
        let deviceSettingsFlowModel = DeviceSettingsFlowModel(activeDevice: activeDevice, dependencies: dependencies)
        
        deviceSettingsFlowModel.pushViewController
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] viewController in
                DispatchQueue.main.async {
                    self?.navigationController.pushViewController(viewController, animated: true)
                }
            }).disposed(by: deviceSettingsDisposeBag)
        
        deviceSettingsFlowModel.popViewController
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.navigationController.popViewController(animated: true)
            }).disposed(by: deviceSettingsDisposeBag)
        
        deviceSettingsFlowModel.outputs.popToDeviceSettingsViewController
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                let viewControllers = self?.navigationController.viewControllers
                if let deviceSettingsViewController = viewControllers?.first(where: { $0 is DeviceSettingsViewController }) {
                    self?.navigationController.popToViewController(deviceSettingsViewController, animated: true)
                }
            }).disposed(by: deviceSettingsDisposeBag)
        
        deviceSettingsFlowModel.outputs.popOutsideDeviceSettingsViewController
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.navigationController.popToRootViewController(animated: true)
                self?.navigationController.transitionCoordinator?.animate(alongsideTransition: nil) { _ in }
                self?.deviceSettingsDisposeBag = DisposeBag()
            }).disposed(by: deviceSettingsDisposeBag)
        
        deviceSettingsFlowModel.completed
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] flowId in
                self.submodels.remove(itemsWith: flowId)
                self.deviceSettingsDisposeBag = DisposeBag()
            }).disposed(by: deviceSettingsDisposeBag)
        
        do {
            try guardSubmodules(contains: DeviceSettingsFlowModel.id)
            submodels.append(deviceSettingsFlowModel)
            
            deviceSettingsFlowModel.inputs.pushDeviceSettings()
        } catch {
            print("Flow guard submodule failed")
        }
    }
    private func startConfiguring(_ device: DeviceType) {
        guard devicesBeingConfiguredIds.contains(device.id) == false else { return }
        dependencies.deviceAdapterService.inputs.polling(enable: false, dueTime: AdapterServiceConfig.dueTime, interval: AdapterServiceConfig.defaultPollingInterval)
        dependencies.deviceAdapterService.inputs.cancelDiscovery()
        
        devicesBeingConfiguredIds.append(device.id)
        var deviceConfigurationDisposeBag = DisposeBag()
        processConfiguration(for: device,
                             rootViewController: self.localRootViewController,
                             dependencies: dependencies)
            .subscribe(
                onSuccess: { [unowned self] in
                    self.devicesBeingConfiguredIds.removeObject(device.id)
                    self.storeDeviceInfo(device)
                    self.dependencies.deviceAdapterService.inputs.polling(enable: true, dueTime: AdapterServiceConfig.dueTime, interval: AdapterServiceConfig.defaultPollingInterval)
                    deviceConfigurationDisposeBag = DisposeBag()
                },
                onError: { [unowned self] error in
                    self.devicesBeingConfiguredIds.removeObject(device.id)
                    device.state.inputs.disconnect()
                    device.state.inputs.set(connectivityStatus: .notConfigured)
                    self.dependencies.deviceAdapterService.inputs.polling(enable: true, dueTime: AdapterServiceConfig.dueTime, interval: AdapterServiceConfig.defaultPollingInterval)
                    deviceConfigurationDisposeBag = DisposeBag()
                }
            )
            .disposed(by: deviceConfigurationDisposeBag)
    }

    private func finalizeDecouple(masterId: String) {
        preferences.couplePairs.removeValue(forKey: masterId)
    }
    private func decouple(master: DeviceType) {
        dependencies.deviceAdapterService.inputs.cancelDiscovery()
        dependencies.deviceAdapterService.inputs.polling(enable: false, dueTime: RxTimeInterval.zero, interval: RxTimeInterval.zero)
        
        var slaveToReset: DeviceType?
        
        let disposableEntry = disposableSilentConnectEntry(for: master.id)
        
        if case .wsMaster(let slaveMAC) = master.state.outputs.connectivityStatus().stateObservable().value,
            let slaveDevice = dependencies.deviceService.outputs.devices.value.first(where: { $0.mac?.hex == slaveMAC.hex }) {
            slaveToReset = slaveDevice
        }
        guard let slave = slaveToReset else {
            master.decouple()
                .timeout(Config.trueWirelessRequestTimeout, scheduler: MainScheduler.instance)
                .subscribe(
                    onSuccess: { [weak self] _ in
                        self?.finalizeDecouple(masterId: master.id)
                        self?.dependencies.deviceAdapterService.inputs.polling(enable: true, dueTime: AdapterServiceConfig.dueTime, interval: AdapterServiceConfig.defaultPollingInterval)

                    },
                    onError: { [unowned master, weak self] error in
                        master.state.inputs.set(connectivityStatus: .notConfigured)
                        self?.dependencies.deviceAdapterService.inputs.polling(enable: true, dueTime: AdapterServiceConfig.dueTime, interval: AdapterServiceConfig.defaultPollingInterval)
                    }
                )
                .disposed(by: disposableEntry.disposeBag)
            return
        }
        
        Observable.concat([master.decouple().asObservable(),
                           slave.resetSlave().asObservable()])
            .timeout(Config.trueWirelessRequestTimeout, scheduler: MainScheduler.instance)
            .subscribe(
                onNext: { [weak self] _ in
                    self?.finalizeDecouple(masterId: master.id)
                    self?.dependencies.deviceAdapterService.inputs.polling(enable: true, dueTime: AdapterServiceConfig.dueTime, interval: AdapterServiceConfig.defaultPollingInterval)
                },
                onError: { [unowned master, unowned slave, weak self] error in
                    master.state.inputs.set(connectivityStatus: .notConfigured)
                    slave.state.inputs.set(connectivityStatus: .notConfigured)
                    self?.dependencies.deviceAdapterService.inputs.polling(enable: true, dueTime: AdapterServiceConfig.dueTime, interval: AdapterServiceConfig.defaultPollingInterval)
                }
            )
            .disposed(by: disposableEntry.disposeBag)
    }
    private func showPlayer(for device: DeviceType) {
        switch device.hardwareType {
        case .speaker:
            showRegularPlayer(for: device)
        case .headset:
            showOzzyPlayer(for: device)
        }
    }

    private func showRegularPlayer(for device: DeviceType) {
        let state = device.state
        let outputs = state.outputs
        let supportedFeatures = type(of: state.outputs).supportedFeatures

        guard supportedFeatures.contains(.hasConnectivityStatus),
            outputs.connectivityStatus().stateObservable().value.connected else {
                return
        }

        let viewController = UIViewController.instantiate(PlayerSourcesViewController.self)
        let viewModel = PlayerSourcesViewModel(device: device, dependencies: dependencies)
        viewController.viewModel = viewModel

        viewModel.outputs.closed
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned viewController] in
                viewController.dismiss(animated: true, completion: nil)
            })
            .disposed(by: disposeBag)

        viewController.modalPresentationStyle = .fullScreen
        self.navigationController.present(viewController, animated: true, completion: nil)
    }
    private func showOzzyPlayer(for device: DeviceType) {
        let state = device.state
        let outputs = state.outputs
        let supportedFeatures = type(of: state.outputs).supportedFeatures

        guard supportedFeatures.contains(.hasConnectivityStatus),
            outputs.connectivityStatus().stateObservable().value.connected else {
                return
        }

        let viewController = UIViewController.instantiate(OzzyPlayerViewController.self)
        let viewModel = OzzyPlayerViewModel(device: device, dependencies: dependencies)
        viewController.viewModel = viewModel

        viewModel.outputs.closed
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned viewController] in
                viewController.dismiss(animated: true, completion: nil)
            })
            .disposed(by: disposeBag)

        viewController.modalPresentationStyle = .fullScreen
        self.navigationController.present(viewController, animated: true, completion: nil)
    }
    
    private func disposableSilentConnectEntry(for deviceID: String) -> SilentConnectDisposableEntry {
        if let existingDisposableEntryIndex = silentConnectDisposables.index(where: { $0.deviceID == deviceID }) {
            silentConnectDisposables.remove(at: existingDisposableEntryIndex)
        }
        let silentConnectDisposable = SilentConnectDisposableEntry(deviceID: deviceID, disposeBag: DisposeBag())
        silentConnectDisposables.append(silentConnectDisposable)
        return silentConnectDisposable
    }
    private func removeSilentConnectDisposableEntry(for deviceID: String) {
        if let existingDisposableEntryIndex = silentConnectDisposables.index(where: { $0.deviceID == deviceID }) {
            silentConnectDisposables.remove(at: existingDisposableEntryIndex)
        }
    }
    private func storeDeviceInfo(_ device: DeviceType) {
        
        guard let mac = device.mac else { return }
        let storageEntry = StoredDeviceInfoEntry(id: device.id, mac: mac)
        
        guard let storedDevices = preferences.storedDevicesInfo else {
            preferences.storedDevicesInfo = [storageEntry]
            return
        }
        var devices = storedDevices
        guard devices.contains(where: { $0.id == device.id }) == false else {
            return
        }
        devices.append(storageEntry)
        preferences.storedDevicesInfo = devices
    }
}
