//
//  SetupProgressInfo.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 02/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//
import GUMA

struct ProgressEntry {
    let maxValue: Int
    let label: String
}

enum SetupProgressInfo {
    case initializingConnection
    case connected
    case otaProgress(OTAStatus)
    
    static let maxConnectingPercentageProgress = 20
    static let maxDownloadingFirmwarePercentageProgress = 30
    static let maxUploadingToDevicePercentageProgress = 30
    static let maxFlashingDeviceProgress = 15
    
    static let fullOTAPercentageRange = 100 - maxConnectingPercentageProgress
    
    static let initialProgressValueForFirmwareDownload = maxConnectingPercentageProgress
    static let initialProgressValueForFirmwareDeviceUpload = maxConnectingPercentageProgress + maxDownloadingFirmwarePercentageProgress
    
    static let finalOTAState: OTAStatus = .completed
    
    var entry: ProgressEntry {
        return ProgressEntry(maxValue: percentageProgressMaxValue(for: self),
                             label: translation(for: self))
    }
    
    private func translation(for state: SetupProgressInfo) -> String {
        switch state {
        case .initializingConnection, .connected:
            return Strings.ota_screen_downloading_firmware()
        case .otaProgress(let otaState):
            switch otaState {
            case .connecting:
                return Strings.ota_screen_downloading_firmware()
            case .checkingFirmware:
                return Strings.ota_screen_downloading_firmware()
            case .downloadingFirmware:
                return Strings.ota_screen_downloading_firmware()
            case .uploadingToDevice(let progress):
                return Strings.ota_screen_state_uploading_to_device() + (progress > 0.0 ? String(format: " (%.0f %%)", progress * 100) : "")
            case .uploadCompleted(let progress):
                return Strings.ota_screen_state_updating() + (progress > 0.0 ? String(format: " (%.0f %%)", progress * 100) : "")
            case .completed:
                return Strings.ota_screen_state_completed()
            }
        }
    }
    
    private func percentageProgressMaxValue(for state: SetupProgressInfo) -> Int {
        switch state {
        case .initializingConnection:
            return 0
        case .connected: return type(of: self).maxConnectingPercentageProgress
        case .otaProgress(let otaStatus):
            return type(of: self).maxConnectingPercentageProgress + progress(forOTAStatus: otaStatus)
        }
    }
    
    private func progress(forOTAStatus status: OTAStatus) -> Int {
        switch status {
        case .connecting:
            return 0
        case .downloadingFirmware(let progress, _):
            return Int(progress * Float(type(of: self).maxDownloadingFirmwarePercentageProgress))
        case .uploadingToDevice(let progress):
            return type(of: self).maxDownloadingFirmwarePercentageProgress + Int(progress * Float(type(of: self).maxUploadingToDevicePercentageProgress))
        case .checkingFirmware:
            return 0
        case .uploadCompleted(let progress):
            return type(of: self).maxDownloadingFirmwarePercentageProgress +
                type(of: self).maxUploadingToDevicePercentageProgress + Int(progress * Float(type(of: self).maxFlashingDeviceProgress))
        case .completed:
            return type(of: self).fullOTAPercentageRange
        }
    }
}

extension SetupProgressInfo {
    init(otaStatus: OTAUpdateInfo) {
        self = .otaProgress(otaStatus.status)
    }
}

