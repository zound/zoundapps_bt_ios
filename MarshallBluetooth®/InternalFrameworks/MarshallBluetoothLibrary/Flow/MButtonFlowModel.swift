//
//  MButtonFlowModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 10/02/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import RxSwift
import RxCocoa
import GUMA

public enum OnboardingMButtonState {
    case displayOnboardingMButton
    case completed
}
protocol OnboardingMButtonFlowModelInput {
    func perform()
}
protocol OnboardingMButtonFlowModelOutput {
    var pushViewController: PublishSubject<UIViewController> { get }
}
protocol OnboardingMButtonFlowModelType {
    var inputs: OnboardingMButtonFlowModelInput { get }
    var outputs: OnboardingMButtonFlowModelOutput { get }
}
final class OnboardingMButtonFlowModel: FlowModel, OnboardingMButtonFlowModelInput, OnboardingMButtonFlowModelOutput, OnboardingMButtonFlowModelType {

    typealias Dependencies = DeviceServiceDependency

    static var id: FlowId = .onboardingMButton
    var inputs: OnboardingMButtonFlowModelInput { return self }
    var outputs: OnboardingMButtonFlowModelOutput { return self }
    var submodels = [FlowModel]()

    var setViewController = BehaviorRelay<UIViewController?>.init(value: nil)
    var pushViewController = PublishSubject<UIViewController>()
    var popViewController = PublishSubject<Void>()
    var presentViewController = PublishSubject<UIViewController>()
    var dismissViewController = PublishSubject<UIViewController>()

    var completed = PublishSubject<FlowId>()
    var preferences: DefaultStorage
    let dependencies: Dependencies

    init(dependencies: Dependencies, device: DeviceType, preferences: DefaultStorage = DefaultStorage.shared) {
        self.dependencies = dependencies
        self.preferences = preferences
        self.device = device

        current
            .observeOn(MainScheduler.instance)
            .subscribeNext(weak: self, OnboardingMButtonFlowModel.handle)
            .disposed(by: disposeBag)
    }
    func perform() {
        current.accept(.displayOnboardingMButton)
    }
    private var device: DeviceType
    private var current = PublishRelay<OnboardingMButtonState>()
    private var disposeBag = DisposeBag()
}

private extension OnboardingMButtonFlowModel {
    func handle(_ state: OnboardingMButtonState) {
        switch state {
        case .displayOnboardingMButton:
            showOnboardingMButton(device: device)
        case .completed:
            self.completed.onNext(OnboardingMButtonFlowModel.id)

        }
    }
    func showOnboardingMButton(device: DeviceType) {
        let viewController = UIViewController.instantiate(OnboardingMButtonViewController.self)
        let viewModel = OnboardingMButtonViewModel(device: device)
        viewController.viewModel = viewModel
        viewModel.outputs.finished
            .subscribe(onNext: { [weak self] in
                self?.completed.onNext(OnboardingMButtonFlowModel.id)
            })
            .disposed(by: disposeBag)
        viewModel.outputs.googleAssistantScreenRequested
            .subscribe(onNext: { [weak self] in
                self?.showGoogleAssistantScreen()
            })
            .disposed(by: disposeBag)
        let navigationController = UINavigationController()
        navigationController.setViewControllers([viewController], animated: false)
        setViewController.accept(navigationController)
    }
    func showGoogleAssistantScreen() {
        let viewController = UIViewController.instantiate(GoogleAssistantViewController.self)
        let viewModel = GoogleAssistantViewModel()
        viewModel.outputs.done
            .subscribe(onNext: { [weak self] in
                if let strongSelf = self {
                    strongSelf.showOnboardingMButton(device: strongSelf.device)
                }
            })
            .disposed(by: disposeBag)
        viewController.viewModel = viewModel
        setViewController.accept(viewController)
    }
}

