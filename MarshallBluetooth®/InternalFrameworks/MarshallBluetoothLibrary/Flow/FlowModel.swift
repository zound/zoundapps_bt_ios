//
//  FlowModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 20/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift
import GUMA

enum FlowError: Error {
    case duplicatedFlow
}

enum FlowId: String {
    case app
    case signup
    case deviceSetup
    case home
    case deviceSettings
    case coupleSpeaker
    case playerSources
    case pairing
    case onboarding
    case onboardingMButton
}

protocol FlowModel {
    static var id: FlowId { get }
    var submodels: [FlowModel] { get }
    
    /// Group UIViewController's management activities
    
    /// Sets new root UIViewController for selected Flow, managing all the sub - UIViewControllers associated with specific Flow.
    /// Previous root should start deallocating at this point
    var setViewController: BehaviorRelay<UIViewController?> { get }
    
    /// If new root UIVIewController is subclass of UINavigationController, we can use those subjects to push/pop view controllers
    var pushViewController: PublishSubject<UIViewController> { get }
    var popViewController: PublishSubject<Void> { get }
    
    /// Those subject may be used for presenting/dismissing sub - UIViewControllers if current Flow requires it
    var presentViewController: PublishSubject<UIViewController> { get }
    var dismissViewController: PublishSubject<UIViewController> { get }
    
    var preferences: DefaultStorage { get }
    var completed: PublishSubject<FlowId> { get }

    func guardSubmodules(contains flowId: FlowId) throws
}

extension FlowModel {
    func guardSubmodules(contains flowId: FlowId) throws {
        guard !submodels.contains(where: { type(of: $0).id == flowId }) else {
            print("Duplicated submodule entry: \(flowId)")
            throw FlowError.duplicatedFlow
        }
    }
}
