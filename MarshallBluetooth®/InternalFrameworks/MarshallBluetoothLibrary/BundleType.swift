//
//  BundleType.swift
//  MarshallBluetoothLibrary
//
//

// It's heavily inspired by the Kickstarter iOS app
// https://github.com/kickstarter/ios-oss


import Foundation

public protocol BundleType {
    func localizedString(forKey key: String, value: String?, table tableName: String?) -> String
    func path(forResource name: String?, ofType ext: String?) -> String?
    static func create(path: String) -> BundleType?
}

extension Bundle: BundleType {
    public static func create(path: String) -> BundleType? {
        return Bundle(path: path)
    }
}
