//
//  DeviceSettingsViewModelTests.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Bartosz Dolewski on 14.05.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA
import RCER

fileprivate struct DummyNameValidator: CustomNameValidator {
    func validate(name: String) -> CustomNameValidationResult {
        // everything is fine for this parser :)
        return .pass
    }
}

fileprivate enum Options: Int {
    case about = 0, rename, couple, forget, eq, light, sounds
}

fileprivate final class DummyConnectedDeviceState: DeviceStateType {
    
    public var inputs: DeviceStateInput { return self }
    public var outputs: DeviceStateOutput { return self }
    
    public static let supportedFeatures: [Feature] = [.hasConnectivityStatus,
                                                      .hasDeviceInfo,
                                                      .hasCustomizableName,
                                                      .hasTrueWireless,
                                                      .hasGraphicalEqualizer,
                                                      .hasLedBrightnessControl,
                                                      .hasSoundsControl]
    public let perDeviceTypeFeatures: [Feature] =    [.hasConnectivityStatus,
                                                      .hasDeviceInfo,
                                                      .hasCustomizableName,
                                                      .hasTrueWireless,
                                                      .hasGraphicalEqualizer,
                                                      .hasLedBrightnessControl,
                                                      .hasSoundsControl]

    init() {
        // initial value -> .connected
        _name = AnyState<String>(feature: .hasCustomizableName,
                                 supportsNotifications: false,
                                 initialValue: { return "Connected Joplin" })
        
        _connectivityStatus = AnyState<ConnectivityStatus>(feature: .hasConnectivityStatus,
                                                           supportsNotifications: false,
                                                           initialValue: { return .connected })


    }

    fileprivate let _name: AnyState<String>
    fileprivate let _connectivityStatus: AnyState<ConnectivityStatus>
    fileprivate let disposeBag = DisposeBag()
}

extension DummyConnectedDeviceState: DeviceStateInput {

    public func set(connectivityStatus: ConnectivityStatus) {
        _connectivityStatus.stateObservable().accept(connectivityStatus)
    }
}

extension DummyConnectedDeviceState: DeviceStateOutput {    public var activeMonitors: Set<Monitor> {
        get { return  Set<Monitor>() }
        set(newValue) {}
    }
    public func connectivityStatus() -> AnyState<ConnectivityStatus> {
        return _connectivityStatus
    }
    
    func name() -> AnyState<String> {
        return _name
    }
}

fileprivate final class DummyDisconnectedDeviceState: DeviceStateType {
    
    public var inputs: DeviceStateInput { return self }
    public var outputs: DeviceStateOutput { return self }
    
    public static let supportedFeatures: [Feature] = [.hasConnectivityStatus,
                                                      .hasDeviceInfo,
                                                      .hasCustomizableName,
                                                      .hasTrueWireless,
                                                      .hasGraphicalEqualizer,
                                                      .hasLedBrightnessControl,
                                                      .hasSoundsControl]
    public let perDeviceTypeFeatures: [Feature] =    [.hasConnectivityStatus,
                                                      .hasDeviceInfo,
                                                      .hasCustomizableName,
                                                      .hasTrueWireless,
                                                      .hasGraphicalEqualizer,
                                                      .hasLedBrightnessControl,
                                                      .hasSoundsControl]
    
    init() {
        // initial value -> disconnected
        _name = AnyState<String>(feature: .hasCustomizableName,
                                 supportsNotifications: false,
                                 initialValue: { return "Disconnected Joplin" })
        
        self._connectivityStatus = AnyState<ConnectivityStatus>(feature: .hasConnectivityStatus,
                                                                supportsNotifications: false,
                                                                initialValue: { return .disconnected })

    }

    fileprivate let _name: AnyState<String>
    fileprivate let _connectivityStatus: AnyState<ConnectivityStatus>
    fileprivate let disposeBag = DisposeBag()
}

extension DummyDisconnectedDeviceState: DeviceStateInput {
    
    public func set(connectivityStatus: ConnectivityStatus) {
        _connectivityStatus.stateObservable().accept(connectivityStatus)
    }
}

extension DummyDisconnectedDeviceState: DeviceStateOutput {
    public var activeMonitors: Set<Monitor> {
        get { return  Set<Monitor>() }
        set(newValue) {}
    }
    public func name() -> AnyState<String> {
        return _name
    }
    
    public func connectivityStatus() -> AnyState<ConnectivityStatus> {
        return _connectivityStatus
    }
}

fileprivate class DummyOtaAdapter: OTAAdapterType, OTAAdapterTypeInput, OTAAdapterTypeOutput {
    func completeUpdate(deviceID: String) { }
    func startUpdate(cachedFirmware: FirmwareCache?, interfaceError: BehaviorRelay<DeviceAdapterError?>, processMode: PublishRelay<ApplicationProcessMode>) { }
    
    func checkUpdateAvailable(deviceID: String) -> Single<UpdateAvailableInfo?> { return Single.just(nil) }
    func checkForcedOTA(deviceID: String) -> Single<UpdateAvailableInfo?> { return Single.just(nil) }
    var adapterError = BehaviorRelay<AnyError<OTAError>?>.init(value: nil)
    
    func startUpdate(requestType: OTARequestType, cachedFirmware: FirmwareCache?, interfaceError: BehaviorRelay<DeviceAdapterError?>, processMode: PublishRelay<ApplicationProcessMode>) { }
    func deviceDisconnected() { }
    func cancel() { }
    
    var inputs: OTAAdapterTypeInput { return self }
    var outputs: OTAAdapterTypeOutput { return self }
    var deviceID: String { return String(describing: self) }
    
    var updateAvailable = PublishSubject<Bool>()
    var updateProgress = PublishSubject<OTAUpdateInfo>()
}

fileprivate class DummyDevice: DeviceType {
    func bleConnect() -> Single<Void> {
        return Single.just(())
    }
    
    var mac: MAC?
    var configured = false
    var hardwareType: HardwareType = .speaker
    
    func decouple() -> Single<Void> {
        return Single.just(())
    }
    
    func resetSlave() -> Single<Void> {
        return Single.just(())
    }
    
    func initialSetup(with storedEntry: StoredDeviceInfoEntry?) -> Single<String> {
        return Single.just(String())
    }
    
    var modelName: String {
        return String()
    }

    var image: DeviceImageProviding = DeviceBlankImageProvider()
    
    var connectionTimeout: RxTimeInterval {
        return RxTimeInterval.seconds(5.0)
    }
    
    var name: BehaviorRelay<String>
    var id: String { return "TEST UUID" }
    var imageName: String { return "TEST_SPEAKER.png" }
    
    let platform = Platform(id: "joplinBT", traits: PlatformTraits())

    var state: DeviceStateType
    var otaAdapter: OTAAdapterType?

    init(name: BehaviorRelay<String>,
         state: DeviceStateType,
         otaAdapter: OTAAdapterType? = nil) {
        self.name = name
        self.state = state
        self.otaAdapter = otaAdapter
    }
}

final class DeviceSettingsViewModelTests: XCTestCase {
    var disposeBag: DisposeBag!
    var vm_connected: DeviceSettingsViewModel!
    var vm_disconnected: DeviceSettingsViewModel!
    var deviceAdapterService: DeviceAdapterServiceType!
    var deviceService: DeviceServiceType!
    var jsonOTAService: OTAServiceType!
    var activeConnectedDevice: BehaviorRelay<DeviceType>!
    var disconnectedDevice: BehaviorRelay<DeviceType>!
    
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
        
        deviceAdapterService = DeviceAdapterService.shared
        let jsonAdapter = JSONMockDeviceAdapter()
        
        deviceService = DeviceService(deviceInfoSource: DeviceListChanges(
            new: deviceAdapterService.outputs.addedDevice,
            updated: deviceAdapterService.outputs.updatedDevice,
            removed: deviceAdapterService.outputs.removedDevice))
        
        deviceAdapterService.inputs.append(adapter: jsonAdapter, mocked: true)
        deviceAdapterService.inputs.start()
        
        jsonAdapter.adapterState.accept(.ready)
        let jsonAppMode = PublishRelay<ApplicationProcessMode>()

        jsonOTAService = OTAService(deviceAdapterService: deviceAdapterService, processMode: jsonAppMode)
        
        let activeDevice = DummyDevice(name: BehaviorRelay<String>(value: String(describing: DummyDevice.self)),
                                       state: DummyConnectedDeviceState(),
                                       otaAdapter: DummyOtaAdapter())
        self.activeConnectedDevice = BehaviorRelay<DeviceType>.init(value: activeDevice)
        let disconnected = DummyDevice(name: BehaviorRelay<String>(value: String(describing: DummyDevice.self)),
                                       state: DummyDisconnectedDeviceState(),
                                       otaAdapter: DummyOtaAdapter())
        self.disconnectedDevice = BehaviorRelay<DeviceType>.init(value: disconnected)
        vm_connected = DeviceSettingsViewModel(activeDevice: self.activeConnectedDevice, otaService: jsonOTAService)
        vm_disconnected = DeviceSettingsViewModel(activeDevice: self.disconnectedDevice, otaService: jsonOTAService)
    }
    
    private func mockUpdateIsAvailable() {
        jsonOTAService.outputs.updateAvailableInfo.accept([UpdateAvailableInfo(deviceID: "TEST UUID", forced: false)])
    }
    
    private func mockUpdateIsNotAvailable() {
        jsonOTAService.outputs.updateAvailableInfo.accept([])
    }
    
    func testCloseButton() {
        let scheduler = TestScheduler(initialClock: 0)
        
        let closed = scheduler.createObserver(Bool.self)
        vm_connected.outputs.closed.asObservable().subscribe(closed).disposed(by: disposeBag)
        
        scheduler.start()
        
        // user tapped on "Close" button
        vm_connected.inputs.closeTapped()
        XCTAssertEqual([.next(0, true)], closed.events)
        
        // that's not possible in reality, but let's assume that user can tap "Close" multiple times in a row
        vm_connected.inputs.closeTapped()
        vm_connected.inputs.closeTapped()
        vm_connected.inputs.closeTapped()
        XCTAssertEqual([.next(0, true),
                        .next(0, true),
                        .next(0, true),
                        .next(0, true)], closed.events)
    }
    
    func testAboutButtonEnable() {
        let scheduler = TestScheduler(initialClock: 0)
        let enabled = scheduler.createObserver(Bool.self)
        
        vm_connected.outputs.dataSource.asObservable()
            .subscribe(onNext: { [enabled] items in
                if let item = items.first(where: { $0.type == .aboutThisSpeaker }) {
                    enabled.onNext(item.enable)
                } else {
                    XCTFail("About This Spaker is not present in Menu")
                }
            })
            .disposed(by: disposeBag)

        XCTAssertEqual([.next(0, true)], enabled.events)
    }
    
    func testAboutButtonDisable() {
        let scheduler = TestScheduler(initialClock: 0)
        let enabled = scheduler.createObserver(Bool.self)
        
        vm_disconnected.outputs.dataSource.asObservable()
            .subscribe(onNext: { [enabled] items in
                if let item = items.first(where: { $0.type == .aboutThisSpeaker }) {
                    enabled.onNext(item.enable)
                } else {
                    XCTFail("About This Spaker is not present in Menu")
                }
            })
            .disposed(by: disposeBag)
        
        XCTAssertEqual([.next(0, false)], enabled.events)
    }
    

    func testRenameButtonEnable() {
        let scheduler = TestScheduler(initialClock: 0)
        let enabled = scheduler.createObserver(Bool.self)
        
        vm_connected.outputs.dataSource.asObservable()
            .subscribe(onNext: { [enabled] items in
                if let item = items.first(where: { $0.type == .renameSpeaker }) {
                    enabled.onNext(item.enable)
                } else {
                    XCTFail("Rename is not present in Menu")
                }
            })
            .disposed(by: disposeBag)
        
        XCTAssertEqual([.next(0, true)], enabled.events)
    }
    
    func testRenameButtonDisable() {
        let scheduler = TestScheduler(initialClock: 0)
        let enabled = scheduler.createObserver(Bool.self)
        
        vm_disconnected.outputs.dataSource.asObservable()
            .subscribe(onNext: { [enabled] items in
                if let item = items.first(where: { $0.type == .renameSpeaker }) {
                    enabled.onNext(item.enable)
                } else {
                    XCTFail("Rename is not present in Menu")
                }
            })
            .disposed(by: disposeBag)
        
        XCTAssertEqual([.next(0, false)], enabled.events)
    }

    func testCoupleButtonEnable() {
        let scheduler = TestScheduler(initialClock: 0)
        let enabled = scheduler.createObserver(Bool.self)
        
        vm_connected.outputs.dataSource.asObservable()
            .subscribe(onNext: { [enabled] items in
                if let item = items.first(where: { $0.type == .coupleSpeaker }) {
                    enabled.onNext(item.enable)
                } else {
                    XCTFail("Couple is not present in Menu")
                }
            })
            .disposed(by: disposeBag)
        
        XCTAssertEqual([.next(0, true)], enabled.events)
    }
    
    func testCoupleButtonDisable() {
        let scheduler = TestScheduler(initialClock: 0)
        let enabled = scheduler.createObserver(Bool.self)
        
        vm_disconnected.outputs.dataSource.asObservable()
            .subscribe(onNext: { [enabled] items in
                if let item = items.first(where: { $0.type == .coupleSpeaker }) {
                    enabled.onNext(item.enable)
                } else {
                    XCTFail("Couple is not present in Menu")
                }
            })
            .disposed(by: disposeBag)
        
        XCTAssertEqual([.next(0, false)], enabled.events)
    }

    func testForgetButtonEnable() {
        let scheduler = TestScheduler(initialClock: 0)
        let enabled = scheduler.createObserver(Bool.self)
        
        vm_connected.outputs.dataSource.asObservable()
            .subscribe(onNext: { [enabled] items in
                if let item = items.first(where: { $0.type == .forgetSpeaker }) {
                    enabled.onNext(item.enable)
                } else {
                    XCTFail("Forget is not present in Menu")
                }
            })
            .disposed(by: disposeBag)
        
        XCTAssertEqual([.next(0, true)], enabled.events)
    }
    
    func testForgetButtonDisable() {
        let scheduler = TestScheduler(initialClock: 0)
        let enabled = scheduler.createObserver(Bool.self)
        
        vm_disconnected.outputs.dataSource.asObservable()
            .subscribe(onNext: { [enabled] items in
                if let item = items.first(where: { $0.type == .forgetSpeaker }) {
                    enabled.onNext(item.enable)
                } else {
                    XCTFail("Forget is not present in Menu")
                }
            })
            .disposed(by: disposeBag)
        
        XCTAssertEqual([.next(0, true)], enabled.events)
    }

    func testEqButtonEnable() {
        let scheduler = TestScheduler(initialClock: 0)
        let enabled = scheduler.createObserver(Bool.self)
        
        vm_connected.outputs.dataSource.asObservable()
            .subscribe(onNext: { [enabled] items in
                if let item = items.first(where: { $0.type == .equaliser }) {
                    enabled.onNext(item.enable)
                } else {
                    XCTFail("Equalizer is not present in Menu")
                }
            })
            .disposed(by: disposeBag)
        
        XCTAssertEqual([.next(0, true)], enabled.events)
    }
    
    func testEqButtonDisable() {
        let scheduler = TestScheduler(initialClock: 0)
        let enabled = scheduler.createObserver(Bool.self)
        
        vm_disconnected.outputs.dataSource.asObservable()
            .subscribe(onNext: { [enabled] items in
                if let item = items.first(where: { $0.type == .equaliser }) {
                    enabled.onNext(item.enable)
                } else {
                    XCTFail("Equalizer is not present in Menu")
                }
            })
            .disposed(by: disposeBag)
        
        XCTAssertEqual([.next(0, false)], enabled.events)
    }

    func testLightButtonEnable() {
        let scheduler = TestScheduler(initialClock: 0)
        let enabled = scheduler.createObserver(Bool.self)
        
        vm_connected.outputs.dataSource.asObservable()
            .subscribe(onNext: { [enabled] items in
                if let item = items.first(where: { $0.type == .light }) {
                    enabled.onNext(item.enable)
                } else {
                    XCTFail("Light is not present in Menu")
                }
            })
            .disposed(by: disposeBag)
        
        XCTAssertEqual([.next(0, true)], enabled.events)
    }
    
    func testLightButtonDisable() {
        let scheduler = TestScheduler(initialClock: 0)
        let enabled = scheduler.createObserver(Bool.self)
        
        vm_disconnected.outputs.dataSource.asObservable()
            .subscribe(onNext: { [enabled] items in
                if let item = items.first(where: { $0.type == .light }) {
                    enabled.onNext(item.enable)
                } else {
                    XCTFail("Light is not present in Menu")
                }
            })
            .disposed(by: disposeBag)
        
        XCTAssertEqual([.next(0, false)], enabled.events)
    }

    func testSoundsButtonEnable() {
        let scheduler = TestScheduler(initialClock: 0)
        let enabled = scheduler.createObserver(Bool.self)
        
        vm_connected.outputs.dataSource.asObservable()
            .subscribe(onNext: { [enabled] items in
                if let item = items.first(where: { $0.type == .sounds }) {
                    enabled.onNext(item.enable)
                } else {
                    XCTFail("Sounds is not present in Menu")
                }
            })
            .disposed(by: disposeBag)
        
        XCTAssertEqual([.next(0, true)], enabled.events)
    }
    
    func testSoundsButtonDisable() {
        let scheduler = TestScheduler(initialClock: 0)
        let enabled = scheduler.createObserver(Bool.self)
        
        vm_disconnected.outputs.dataSource.asObservable()
            .subscribe(onNext: { [enabled] items in
                if let item = items.first(where: { $0.type == .sounds }) {
                    enabled.onNext(item.enable)
                } else {
                    XCTFail("Sounds is not present in Menu")
                }
            })
            .disposed(by: disposeBag)
        
        XCTAssertEqual([.next(0, false)], enabled.events)
    }
    
    func testAboutTapped() {
        let scheduler = TestScheduler(initialClock: 0)
        let tapped = scheduler.createObserver(Bool.self)
        
        vm_connected.outputs.about.asObservable().subscribe(tapped).disposed(by: disposeBag)
        vm_connected.inputs.menuItemTapped(index: Options.about.rawValue)
        
        XCTAssertEqual([.next(0, true)], tapped.events)
    }
    
    func testRenameTapped() {
        let scheduler = TestScheduler(initialClock: 0)
        let tapped = scheduler.createObserver(Bool.self)
        
        vm_connected.outputs.rename.asObservable().subscribe(tapped).disposed(by: disposeBag)
        vm_connected.inputs.menuItemTapped(index: Options.rename.rawValue)
        
        XCTAssertEqual([.next(0, true)], tapped.events)
    }
    
    func testCoupleTapped() {
        let scheduler = TestScheduler(initialClock: 0)
        let tapped = scheduler.createObserver(Bool.self)
        
        vm_connected.outputs.couple.asObservable().subscribe(tapped).disposed(by: disposeBag)
        vm_connected.inputs.menuItemTapped(index: Options.couple.rawValue)
        
        XCTAssertEqual([.next(0, true)], tapped.events)
    }
    
    func testForgetTapped() {
        let scheduler = TestScheduler(initialClock: 0)
        let tapped = scheduler.createObserver(Bool.self)
        
        vm_connected.outputs.forget.asObservable().subscribe(tapped).disposed(by: disposeBag)
        vm_connected.inputs.menuItemTapped(index: Options.forget.rawValue)
        
        XCTAssertEqual([.next(0, true)], tapped.events)
    }
    
    func testEqTapped() {
        let scheduler = TestScheduler(initialClock: 0)
        let tapped = scheduler.createObserver(Bool.self)
        
        vm_connected.outputs.eq.asObservable().subscribe(tapped).disposed(by: disposeBag)
        vm_connected.inputs.menuItemTapped(index: Options.eq.rawValue)
        
        XCTAssertEqual([.next(0, true)], tapped.events)
    }
    
    func testLightTapped() {
        let scheduler = TestScheduler(initialClock: 0)
        let tapped = scheduler.createObserver(Bool.self)
        
        vm_connected.outputs.light.asObservable().subscribe(tapped).disposed(by: disposeBag)
        vm_connected.inputs.menuItemTapped(index: Options.light.rawValue)
        
        XCTAssertEqual([.next(0, true)], tapped.events)
    }
    
    func testSoundsTapped() {
        let scheduler = TestScheduler(initialClock: 0)
        let tapped = scheduler.createObserver(Bool.self)
        
        vm_connected.outputs.sounds.asObservable().subscribe(tapped).disposed(by: disposeBag)
        vm_connected.inputs.menuItemTapped(index: Options.sounds.rawValue)
        
        XCTAssertEqual([.next(0, true)], tapped.events)
    }
    
    func testUpdateIsAvailable() {
        let scheduler = TestScheduler(initialClock: 0)
        let available = scheduler.createObserver(Bool.self)
        vm_connected.outputs.updateAvailable.asObservable().subscribe(available).disposed(by: disposeBag)
        mockUpdateIsAvailable()
        
        XCTAssertEqual([.next(0, false),
                        .next(0, true) ], available.events)
    }
    
    func testUpdateIsNotAvailable() {
        let scheduler = TestScheduler(initialClock: 0)
        let available = scheduler.createObserver(Bool.self)
        
        vm_connected.outputs.updateAvailable.asObservable().subscribe(available).disposed(by: disposeBag)
        mockUpdateIsNotAvailable()
        
        XCTAssertEqual([.next(0, false),
                        .next(0, false) ], available.events)
    }
    
    func testDataSourceWithUpdate() {
        let scheduler = TestScheduler(initialClock: 0)
        let available = scheduler.createObserver(Bool.self)
        
        vm_connected.outputs.dataSource
            .asObservable()
            .subscribe(onNext: { [available] items in
                if let item = items.first(where: { $0.type == .aboutThisSpeaker }) {
                    available.onNext(item.updateNotification)
                } else {
                    XCTFail("Update notification is not present in Menu")
                }
            })
            .disposed(by: disposeBag)
        
        mockUpdateIsAvailable()
        
        XCTAssertEqual([.next(0, false),
                        .next(0, true) ], available.events)
    }
    
    func testDataSourceWithoutUpdate() {
        let scheduler = TestScheduler(initialClock: 0)
        let available = scheduler.createObserver(Bool.self)
        
        vm_connected.outputs.dataSource
            .asObservable()
            .subscribe(onNext: { [available] items in
                if let item = items.first(where: { $0.type == .aboutThisSpeaker }) {
                    available.onNext(item.updateNotification)
                } else {
                    XCTFail("Update notification is not present in Menu")
                }
            })
            .disposed(by: disposeBag)
        
        mockUpdateIsNotAvailable()
        
        XCTAssertEqual([.next(0, false),
                        .next(0, false) ], available.events)
    }
}
