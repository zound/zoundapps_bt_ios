//
//  AudioSourceViewModelTests.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Bartosz Dolewski on 25.06.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

fileprivate var mockAudioSource = AudioSource.unknown
fileprivate var mockTraits = PlatformTraits(volumeMin: 0, volumeMax: 30)
fileprivate var mockVolume = 20

fileprivate struct PlayerDummyState: DeviceStateType {
    static var supportedFeatures: [Feature] = [.hasSwitchableAudioSource]
    public let perDeviceTypeFeatures: [Feature] = [.hasSwitchableAudioSource]
    
    var inputs: DeviceStateInput { return self }
    var outputs: DeviceStateOutput { return self }
    
    fileprivate let _currentAudioSource: AnyState<AudioSource>
    fileprivate let _volumeStatus: AnyState<Int>
    
    init() {
        self._currentAudioSource = AnyState<AudioSource>(feature: .hasSwitchableAudioSource,
                                                         supportsNotifications: false,
                                                         initialValue: { return AudioSource.unknown })
        
        self._volumeStatus = AnyState<Int>(feature: .hasContinuousVolume,
                                           supportsNotifications: false,
                                           initialValue: { return Int() })
    }
    
    func supportedAudioSources() -> [AudioSource] {
        return [.bluetooth, .aux, .rca]
    }
}

fileprivate class PlayerDummyDevice: DeviceType {
    func bleConnect() -> Single<Void> {
        return Single.just(())
    }
    
    var mac: MAC?
    var configured = false
    var hardwareType: HardwareType = .speaker
    
    func decouple() -> Single<Void> {
        return Single.just(())
    }
    
    func resetSlave() -> Single<Void> {
        return Single.just(())
    }
    
    func initialSetup(with storedEntry: StoredDeviceInfoEntry?) -> Single<String> {
        return Single.just(String())
    }
    
    var modelName: String {
        return String()
    }
    var image: DeviceImageProviding = DeviceBlankImageProvider()
    
    var connectionTimeout: TimeInterval = TimeInterval.seconds(0.1)
    
    var name: BehaviorRelay<String>
    var id: String { return "TEST_ID" }
    var imageName: String { return String(describing: self) }
    
    let platform = Platform(id: "joplinBT", traits: mockTraits)
    
    var state: DeviceStateType
    var otaAdapter: OTAAdapterType?
    
    init(name: BehaviorRelay<String>,
         state: DeviceStateType,
         otaAdapter: OTAAdapterType? = nil) {
        self.name = name
        self.state = state
        self.otaAdapter = otaAdapter
    }
    
}

extension PlayerDummyState: DeviceStateInput {
    func requestCurrentAudioSource() {
        _currentAudioSource.stateObservable().accept(mockAudioSource)
    }
    
    func requestVolume() {
        _volumeStatus.stateObservable().accept(mockVolume)
    }
    
    func currentAudioSource() -> AnyState<AudioSource> {
        return _currentAudioSource
    }
    
    func activateAUX() {
        _currentAudioSource.stateObservable().accept(.aux)
    }
    
    func activateRCA() {
        _currentAudioSource.stateObservable().accept(.rca)
    }
    
    func activateBluetooth() {
        _currentAudioSource.stateObservable().accept(.bluetooth)
    }
    
    func startMonitoringAudioSources() {}
    func stopMonitoringAudioSources() {}
}

extension PlayerDummyState: DeviceStateOutput {
    public var activeMonitors: Set<Monitor> {
        get { return  Set<Monitor>() }
        set(newValue) {}
    }
    
    func volume() -> AnyState<Int> {
        return _volumeStatus
    }
    
    func monitoredVolume() -> AnyState<Int> {
        return _volumeStatus
    }
}

final class AudioSourceViewModelTests: XCTestCase {
    var disposeBag: DisposeBag!
    var vm: AudioSourceViewModelType!
    var testRelay: BehaviorRelay<SourceModelEntry>!
    var testSourceEntry: SourceModelEntry!
    var device: DeviceType!
    
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
        
        device = PlayerDummyDevice(name: BehaviorRelay<String>(value: String(describing: self)),
                                   state: PlayerDummyState(),
                                   otaAdapter: nil)
        vm = AudioSourceViewModel(device: device, audioSource: .aux, dependencies: MockedAnalyticsServiceDependency())
    }
    
    func testInitialStateNotActivatedYet() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let activateVisible = scheduler.createObserver(Bool.self)
        let sourceIconVisible = scheduler.createObserver(Bool.self)
        let volumeBarVisible = scheduler.createObserver(Bool.self)
        
        vm.outputs.activateVisible.asObservable().subscribe(activateVisible).disposed(by: disposeBag)
        vm.outputs.sourceIconVisible.asObservable().subscribe(sourceIconVisible).disposed(by: disposeBag)
        vm.outputs.volumeBarVisible.asObservable().subscribe(volumeBarVisible).disposed(by: disposeBag)
        
        mockAudioSource = .rca
        device.state.inputs.requestCurrentAudioSource()
        
        XCTAssertEqual([.next(TestTime(), true)], activateVisible.events)
        XCTAssertEqual([.next(TestTime(), true)], sourceIconVisible.events)
        XCTAssertEqual([.next(TestTime(), false)], volumeBarVisible.events)
    }
    
    func testInitialStateAlreadyActivated() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let activateVisible = scheduler.createObserver(Bool.self)
        let sourceIconVisible = scheduler.createObserver(Bool.self)
        let volumeBarVisible = scheduler.createObserver(Bool.self)
        
        vm.outputs.activateVisible.asObservable().subscribe(activateVisible).disposed(by: disposeBag)
        vm.outputs.sourceIconVisible.asObservable().subscribe(sourceIconVisible).disposed(by: disposeBag)
        vm.outputs.volumeBarVisible.asObservable().subscribe(volumeBarVisible).disposed(by: disposeBag)
        
        mockAudioSource = .aux
        device.state.inputs.requestCurrentAudioSource()
        
        XCTAssertEqual([.next(TestTime(), false)], activateVisible.events)
        XCTAssertEqual([.next(TestTime(), false)], sourceIconVisible.events)
        XCTAssertEqual([.next(TestTime(), true)], volumeBarVisible.events)
    }
    
    func testActivate() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let bluetoothActivateVisible = scheduler.createObserver(Bool.self)
        let auxActivateVisible = scheduler.createObserver(Bool.self)
        let rcaActivateVisible = scheduler.createObserver(Bool.self)
        
        let bluetoothVM = AudioSourceViewModel(device: device, audioSource: .bluetooth, dependencies: MockedAnalyticsServiceDependency())
        let auxVM = AudioSourceViewModel(device: device, audioSource: .aux, dependencies: MockedAnalyticsServiceDependency())
        let rcaVM = AudioSourceViewModel(device: device, audioSource: .rca, dependencies: MockedAnalyticsServiceDependency())
        
        bluetoothVM.outputs.activateVisible.asObservable().subscribe(bluetoothActivateVisible).disposed(by: disposeBag)
        auxVM.outputs.activateVisible.asObservable().subscribe(auxActivateVisible).disposed(by: disposeBag)
        rcaVM.outputs.activateVisible.asObservable().subscribe(rcaActivateVisible).disposed(by: disposeBag)
        
        mockAudioSource = .aux
        auxVM.inputs.activate()
        
        XCTAssertEqual([.next(TestTime(), true)], bluetoothActivateVisible.events)
        XCTAssertEqual([.next(TestTime(), false)], auxActivateVisible.events)
        XCTAssertEqual([.next(TestTime(), true)], rcaActivateVisible.events)
        
        mockAudioSource = .bluetooth
        bluetoothVM.inputs.activate()
        
        XCTAssertEqual([.next(TestTime(), true),
                        .next(TestTime(), false)], bluetoothActivateVisible.events)
        
        XCTAssertEqual([.next(TestTime(), false),
                        .next(TestTime(), true)], auxActivateVisible.events)
        
        XCTAssertEqual([.next(TestTime(), true),
                        .next(TestTime(), true)], rcaActivateVisible.events)
        
        mockAudioSource = .rca
        rcaVM.inputs.activate()

        XCTAssertEqual([.next(TestTime(), true),
                        .next(TestTime(), false),
                        .next(TestTime(), true)], bluetoothActivateVisible.events)
        
        XCTAssertEqual([.next(TestTime(), false),
                        .next(TestTime(), true),
                        .next(TestTime(), true)], auxActivateVisible.events)
        
        XCTAssertEqual([.next(TestTime(), true),
                        .next(TestTime(), true),
                        .next(TestTime(), false)], rcaActivateVisible.events)
    }
    
    func testVolume() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let volume = scheduler.createObserver(Int.self)
        
        let bluetoothVM = AudioSourceViewModel(device: device, audioSource: .bluetooth, dependencies: MockedAnalyticsServiceDependency())
        bluetoothVM.outputs.volume.asObservable().subscribe(volume).disposed(by: disposeBag)
        
        XCTAssertEqual([.next(TestTime(), 20)], volume.events)
    }
}
