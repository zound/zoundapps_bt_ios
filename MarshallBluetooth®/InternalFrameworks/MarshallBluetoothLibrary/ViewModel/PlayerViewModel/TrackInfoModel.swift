//
//  TrackInfoModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 16.08.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import GUMA
import RxSwift

private struct Config {
    static let blankData = ""
}

/// Input points of the PlaybackControlsModel
protocol TrackInfoModelInput {
    func requestTrackInfo()
}

/// Output points of the PlaybackControlsModel
protocol TrackInfoModelOutput {
    var artist: ReplaySubject<String> { get }
    var title: ReplaySubject<String> { get }
    var album: ReplaySubject<String> { get }
}

/// Protocol to force input/outputs on playback controls model
protocol TrackInfoModelType {
    /// Input points of the model
    var inputs: TrackInfoModelInput { get }
    /// Output points of the model
    var outputs: TrackInfoModelOutput { get }
}

class TrackInfoModel: TrackInfoModelInput, TrackInfoModelOutput {
    var artist = ReplaySubject<String>.create(bufferSize: 1)
    var title = ReplaySubject<String>.create(bufferSize: 1)
    var album = ReplaySubject<String>.create(bufferSize: 1)
    
    let disposeBag = DisposeBag()
    let modelDevice: DeviceType
    
    init(device: DeviceType) {
        modelDevice = device
        
        let trackInfo = modelDevice.state.outputs.trackInfo().stateObservable()
            .asObservable()
            .share()
        
        trackInfo
            .map { TrackInfoModel.correctInvalidData(input: $0.title) }
            .catchErrorJustReturn(Config.blankData)
            .bind(to: title)
            .disposed(by: disposeBag)
        
        trackInfo
            .map { TrackInfoModel.correctInvalidData(input: $0.album) }
            .catchErrorJustReturn(Config.blankData)
            .bind(to: album)
            .disposed(by: disposeBag)
        
        trackInfo
            .map {  TrackInfoModel.correctInvalidData(input: $0.artist) }
            .catchErrorJustReturn(Config.blankData)
            .bind(to: artist)
            .disposed(by: disposeBag)
    }
    
    deinit {
        print(#function, String(describing: self))
    }
    
    func requestTrackInfo() {
        modelDevice.state.inputs.requestCurrentTrackInfo()
    }
}

extension TrackInfoModel: TrackInfoModelType {
    var inputs: TrackInfoModelInput { return self }
    var outputs: TrackInfoModelOutput { return self }
}

private extension TrackInfoModel {
    class func correctInvalidData(input: String?) -> String {
        guard let input = input else { return Config.blankData }
        return input
    }
}
