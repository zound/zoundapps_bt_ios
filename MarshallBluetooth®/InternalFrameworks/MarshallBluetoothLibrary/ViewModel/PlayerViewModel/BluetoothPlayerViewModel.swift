//
//  BluetoothPlayerViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 13/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

/// Input points of Bluetooth player's view model
protocol BluetoothPlayerViewModelInput {
    /// Handle tap on Play/Pause controls
    func playPause()
    /// Handle tap on next track controls
    func next()
    /// Handle tap on previous track controls
    func previous()
    func logVolumeChangeAnalyticsEvnet(with volume: Int)
}

protocol BluetoothPlayerViewModelOutput {
    /// Notify if device is currently playing an audio track or not
    var isPlaying: PublishRelay<Bool> { get }
    var artist: ReplaySubject<String> { get }
    var album: ReplaySubject<String> { get }
    var title: ReplaySubject<String> { get }
    var scrubberVisible: ReplaySubject<Bool> { get }
}

protocol BluetoothPlayerViewModelType {
    /// Input points of the view model
    var inputs: BluetoothPlayerViewModelInput { get }
    /// Output points of the view model
    var outputs: BluetoothPlayerViewModelOutput { get }
}

final class BluetoothPlayerViewModel: BluetoothPlayerViewModelInput, BluetoothPlayerViewModelOutput {

    typealias Dependencies = AnalyticsServiceDependency
    
    let modelDevice: DeviceType
    let dependencies: Dependencies
    
    var isPlaying = PublishRelay<Bool>()
    var artist = ReplaySubject<String>.create(bufferSize: 1)
    var album = ReplaySubject<String>.create(bufferSize: 1)
    var title = ReplaySubject<String>.create(bufferSize: 1)
    var scrubberVisible = ReplaySubject<Bool>.create(bufferSize: 1)
    
    private let playPauseEvents = PublishRelay<Void>()
    private let playbackModel: PlaybackControlsModelType
    private let trackInfoModel: TrackInfoModelType
    private let audioScrubbingModel: AudioScrubbingModelType
    private let disposeBag = DisposeBag()
    
    init(device: DeviceType, dependencies: Dependencies) {
        modelDevice = device
        self.dependencies = dependencies
        playbackModel = PlaybackControlsModel(device: modelDevice)
        trackInfoModel = TrackInfoModel(device: modelDevice)
        audioScrubbingModel = AudioScrubbingModel(device: modelDevice)
        
        playbackModel.outputs.playbackState
            .asObservable()
            .subscribeNext(weak: self, BluetoothPlayerViewModel.handlePlayback)
            .disposed(by: disposeBag)
        
        let playPauseAction = playPauseEvents
            .asObservable()
            .withLatestFrom(isPlaying) { $1 } // just return boolean value about playing status
            .share()
        
        playPauseAction
            .filter { $0 == true } // react when something is playing on
            .subscribe(onNext: { [unowned self] _ in
                self.playbackModel.inputs.pause()
                self.dependencies.analyticsService.inputs.log(.appMediaButtonTouchedPause, associatedWith: self.modelDevice.modelName)
            }).disposed(by: disposeBag)
        
        playPauseAction
            .filter { $0 == false } // react when music is paused/stopped
            .subscribe(onNext: { [unowned self] _ in
                self.playbackModel.inputs.play()
                self.dependencies.analyticsService.inputs.log(.appMediaButtonTouchedPlay, associatedWith: self.modelDevice.modelName)
            }).disposed(by: disposeBag)
        
        playbackModel.inputs.requestPlaybackState()
        
        trackInfoModel.outputs.artist
            .distinctUntilChanged()
            .bind(to: artist)
            .disposed(by: disposeBag)
        
        trackInfoModel.outputs.album
            .distinctUntilChanged()
            .bind(to: album)
            .disposed(by: disposeBag)
        
        trackInfoModel.outputs.title
            .distinctUntilChanged()
            .bind(to: title)
            .disposed(by: disposeBag)
        
        trackInfoModel.inputs.requestTrackInfo()
        
        audioScrubbingModel.outputs.accessible
            .bind(to: scrubberVisible)
            .disposed(by: disposeBag)
        
        audioScrubbingModel.inputs.requestScrubbingAccessibility()
    }
    
    deinit {
        print(#function, String(describing: self))
    }
}

extension BluetoothPlayerViewModel: BluetoothPlayerViewModelType {
    var inputs: BluetoothPlayerViewModelInput { return self }
    var outputs: BluetoothPlayerViewModelOutput { return self }
}

// MARK: - Implementation of inputs
extension BluetoothPlayerViewModel {
    func playPause() {
        playPauseEvents.accept(())
    }

    func next() {
        playbackModel.inputs.next()
        dependencies.analyticsService.inputs.log(.appMediaButtonTouchedNext, associatedWith: modelDevice.modelName)
    }

    func previous() {
        playbackModel.inputs.previous()
        dependencies.analyticsService.inputs.log(.appMediaButtonTouchedPrev, associatedWith: modelDevice.modelName)
    }

    func logVolumeChangeAnalyticsEvnet(with volume: Int) {
        dependencies.analyticsService.inputs.log(.appVolumeChanged(volume), associatedWith: modelDevice.modelName)
    }
}

// MARK: - Handle playback state from device
private extension BluetoothPlayerViewModel {
    private func handlePlayback(state: PlaybackStatus) {
        switch state {
        case .playing:
            isPlaying.accept(true)
        default:
            isPlaying.accept(false)
        }
    }
}
