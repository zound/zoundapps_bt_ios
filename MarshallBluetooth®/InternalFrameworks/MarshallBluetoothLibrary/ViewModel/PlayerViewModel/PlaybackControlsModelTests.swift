//
//  PlaybackControlsModelTests.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Bartosz Dolewski on 19.07.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

/// Global configuration mock
fileprivate var mockPlaybackStatus: PlaybackStatus = .unknown

fileprivate struct PlayerDummyState: DeviceStateType {
    static var supportedFeatures: [Feature] = [.hasSwitchableAudioSource]
    let perDeviceTypeFeatures: [Feature] = [.hasSwitchableAudioSource]
    
    var mockedSource: AudioSource = .unknown
    
    var inputs: DeviceStateInput { return self }
    var outputs: DeviceStateOutput { return self }

    fileprivate let _playbackStatus: AnyState<PlaybackStatus>
    
    init() {
        self._playbackStatus = AnyState<PlaybackStatus>(feature: .hasAudioPlayback,
                                                        supportsNotifications: false,
                                                        initialValue: { return mockPlaybackStatus } )
    }
}

fileprivate class PlayerDummyDevice: DeviceType {
    func bleConnect() -> Single<Void> {
        return Single.just(())
    }
    
    var mac: MAC?
    var configured = false
    var hardwareType: HardwareType = .speaker
    
    func decouple() -> Single<Void> {
        return Single.just(())
    }
    
    func resetSlave() -> Single<Void> {
        return Single.just(())
    }
    
    func initialSetup(with storedEntry: StoredDeviceInfoEntry?) -> Single<String> {
        return Single.just(String())
    }
    
    var modelName: String {
        return String()
    }

    var name: BehaviorRelay<String>

    var image: DeviceImageProviding = DeviceBlankImageProvider()

    var connectionTimeout: TimeInterval = TimeInterval.seconds(0.1)
    
    var friendlyName: String { return String(describing: self) }
    var id: String { return String(describing: self) }
    var imageName: String { return String(describing: self) }
    
    let platform = Platform(id: "joplinBT", traits: PlatformTraits())
    
    var state: DeviceStateType
    var otaAdapter: OTAAdapterType?

    init(name: BehaviorRelay<String>,
         state: DeviceStateType,
         otaAdapter: OTAAdapterType? = nil) {
        self.name = name
        self.state = state
        self.otaAdapter = otaAdapter
    }
}

extension PlayerDummyState: DeviceStateInput {
    func requestPlaybackStatus() {
        _playbackStatus.stateObservable().accept(mockPlaybackStatus)
    }
}

extension PlayerDummyState: DeviceStateOutput {
    public var activeMonitors: Set<Monitor> {
        get { return  Set<Monitor>() }
        set(newValue) {}
    }
    
    func playbackStatus() -> AnyState<PlaybackStatus> {
        return _playbackStatus
    }
}

final class PlaybackControlsModelTests: XCTestCase {
    var disposeBag: DisposeBag!
    var model: PlaybackControlsModel!
    
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
        
        let device = PlayerDummyDevice(name: BehaviorRelay<String>(value: String(describing: self)),
                                       state: PlayerDummyState(),
                                       otaAdapter: nil)
        
        mockPlaybackStatus = .unknown
        model = PlaybackControlsModel(device: device)
    }
    
    func testInitialState() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let playbackState = scheduler.createObserver(PlaybackStatus.self)
        
        model.outputs.playbackState.asObservable().subscribe(playbackState).disposed(by: disposeBag)

        XCTAssertTrue(playbackState.events.isEmpty)
        
        model.requestPlaybackState()
        XCTAssertEqual([.next(TestTime(), .unknown)], playbackState.events)
    }
    
    func testRequest() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let playbackState = scheduler.createObserver(PlaybackStatus.self)
        
        model.outputs.playbackState.asObservable().subscribe(playbackState).disposed(by: disposeBag)
        
        mockPlaybackStatus = .playing
        model.requestPlaybackState()
        
        XCTAssertEqual([.next(TestTime(), .playing)], playbackState.events)
        
        mockPlaybackStatus = .paused
        model.requestPlaybackState()
        
        XCTAssertEqual([.next(TestTime(), .playing),
                        .next(TestTime(), .paused)], playbackState.events)
        
        mockPlaybackStatus = .playing
        model.requestPlaybackState()
        
        XCTAssertEqual([.next(TestTime(), .playing),
                        .next(TestTime(), .paused),
                        .next(TestTime(), .playing)], playbackState.events)
        
        mockPlaybackStatus = .paused
        model.requestPlaybackState()
        
        XCTAssertEqual([.next(TestTime(), .playing),
                        .next(TestTime(), .paused),
                        .next(TestTime(), .playing),
                        .next(TestTime(), .paused)], playbackState.events)
        
        mockPlaybackStatus = .stopped
        model.requestPlaybackState()
        
        XCTAssertEqual([.next(TestTime(), .playing),
                        .next(TestTime(), .paused),
                        .next(TestTime(), .playing),
                        .next(TestTime(), .paused),
                        .next(TestTime(), .stopped)], playbackState.events)
    }
    
    
}
