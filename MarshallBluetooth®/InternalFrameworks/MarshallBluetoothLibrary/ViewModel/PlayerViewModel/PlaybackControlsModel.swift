//
//  PlaybackControlsModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 17.07.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import GUMA
import RxSwift

/// Input points of the PlaybackControlsModel
protocol PlaybackControlsModelInput {
    func requestPlaybackState()
    func next()
    func previous()
    func play()
    func pause()
}

/// Output points of the PlaybackControlsModel
protocol PlaybackControlsModelOutput {
    var playbackState: PublishRelay<PlaybackStatus> { get }
}

/// Protocol to force input/outputs on playback controls model
protocol PlaybackControlsModelType {
    /// Input points of the model
    var inputs: PlaybackControlsModelInput { get }
    /// Output points of the model
    var outputs: PlaybackControlsModelOutput { get }
}

class PlaybackControlsModel: PlaybackControlsModelInput, PlaybackControlsModelOutput {
    
    func requestPlaybackState() {
        modelDevice.state.inputs.requestPlaybackStatus()
    }
    
    func next() {
        modelDevice.state.inputs.nextTrack()
    }
    
    func previous() {
        modelDevice.state.inputs.previousTrack()
    }
    
    func play() {
        modelDevice.state.inputs.play()
    }
    
    func pause() {
        modelDevice.state.inputs.pause()
    }
    
    var playbackState = PublishRelay<PlaybackStatus>()
    
    let disposeBag = DisposeBag()
    let modelDevice: DeviceType
    
    init(device: DeviceType) {
        modelDevice = device
        
        modelDevice.state.outputs.playbackStatus().stateObservable()
            .bind(to: playbackState)
            .disposed(by: disposeBag)
    }
    
    deinit {
        print(#function, String(describing: self))
    }
}

extension PlaybackControlsModel: PlaybackControlsModelType {
    var inputs: PlaybackControlsModelInput { return self }
    var outputs: PlaybackControlsModelOutput { return self }
}
