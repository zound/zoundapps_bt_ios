//
//  PlayerSourcesViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 26.06.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

struct SourceModelEntry {
    var type: AudioSource
    var image: UIImage
    var name: String
}

/// Input points of the view model for PlayerSources View
protocol PlayerSourcesViewModelInput {
    /// Close main view for player
    func close()
    /// Select audio source
    ///
    /// - Parameter source: Audio source type
    func select(sourceIdx: Int)
    func viewLoaded()
    func viewWillAppear()
    func viewWillDisappear()
    func typeForIndex(index: Int) -> AudioSource
}

/// Output points of the view model for PlayerSources View
protocol PlayerSourcesViewModelOutput {
    /// Notify that Player's main view is going to be closed
    var closed: PublishSubject<Void> { get }
    /// Notify about source's name
    var sourceName: PublishRelay<String> { get }
    /// Notify about source's index in underylig data source
    var sourceIndex: PublishRelay<Int> { get }
    /// Notify about data source for collection views
    var dataSource: BehaviorRelay<[SourceModelEntry]> { get }
    var analyticsServiceDependency: AnalyticsServiceDependency { get }
    
    /// Return device for which the player is displayed
    ///
    /// - Returns: Generic device type
    func device() -> DeviceType
    func typeForIndex(index: Int) -> AudioSource
}

/// Force input/outputs points for view model
protocol PlayerSourcesViewModelType {
    var inputs: PlayerSourcesViewModelInput { get }
    var outputs: PlayerSourcesViewModelOutput { get }
}

/// Actual implementation for Player's view model
class PlayerSourcesViewModel: PlayerSourcesViewModelInput, PlayerSourcesViewModelOutput {
    
    typealias Dependencies = AnalyticsServiceDependency

    var closed = PublishSubject<Void>.init()
    var sourceName = PublishRelay<String>.init()
    var sourceIndex = PublishRelay<Int>.init()
    var remoteIndex = PublishRelay<Int>.init()
    var dataSource = BehaviorRelay<[SourceModelEntry]>(value: [])
    var analyticsServiceDependency: AnalyticsServiceDependency {
        return dependencies
    }
    
    private var disposeBag = DisposeBag()
    private var sourceSwitchingDisposeBag = DisposeBag()
    private var modelDevice: DeviceType
    var dependencies: Dependencies
    
    init(device: DeviceType, dependencies: Dependencies) {
        modelDevice = device
        self.dependencies = dependencies
        dataSource.accept(device.state.outputs.supportedAudioSources().map { modelSourceEntry(for: $0) })
        let disconnectedEvent = modelDevice.state.outputs.connectivityStatus().stateObservable().skipWhile { $0 != .disconnected }
        disconnectedEvent.subscribe(onNext: { [weak self] _ in
            self?.close()
        })
        .disposed(by: disposeBag)
    }
    func viewLoaded() {
        modelDevice.state.outputs.currentAudioSource().stateObservable()
            .observeOn(MainScheduler.instance)
            .subscribeNext(weak: self, PlayerSourcesViewModel.remoteSourceChanged)
            .disposed(by: disposeBag)
        modelDevice.state.inputs.requestCurrentAudioSource()
        modelDevice.state.inputs.requestCurrentTrackInfo()
    }

    deinit {
        print(#function, String(describing: self))
    }
    
    func close() {
        closed.onNext(())
    }
    
    func select(sourceIdx: Int) {
        let type = typeForIndex(index: sourceIdx)
        guard let sourceObservable = dataSource.value.first(where: { $0.type == type }) else { return }
        sourceIndex.accept(sourceIdx)
        sourceName.accept(sourceObservable.name)
    }
    
    func device() -> DeviceType {
        return modelDevice
    }

    func typeForIndex(index: Int) -> AudioSource {
        guard dataSource.value.indices.contains(index) else { return .unknown}
        return dataSource.value[index].type
    }
    func viewWillAppear() {
        modelDevice.state.inputs.requestCurrentAudioSource()
        modelDevice.state.inputs.requestCurrentTrackInfo()
        startMonitors(modelDevice.hardwareType)
    }
    
    func viewWillDisappear() {
        stopMonitors(modelDevice.hardwareType)
    }
}

private extension PlayerSourcesViewModel {
    func remoteSourceChanged(_ to: AudioSource) {
        /// Enable selected source
        guard let sourceObservable = dataSource.value.first(where: { $0.type == to }) else { return }
        guard let remoteIndex = dataSource.value.index(where: { $0.type == to }) else { return }
        
        sourceIndex.accept(remoteIndex)
        sourceName.accept(sourceObservable.name)
    }
    
    func modelSourceEntry(for source: AudioSource) -> SourceModelEntry {
        switch source {
        case .bluetooth:
            return SourceModelEntry(type: .bluetooth, image: UIImage.bluetoothImage, name: Strings.player_audio_source_bluetooth())
        case .aux:
            return SourceModelEntry(type: .aux, image: UIImage.auxImage, name: Strings.player_audio_source_aux())
        case .rca:
            return SourceModelEntry(type: .rca, image: UIImage.rcaImage, name: Strings.player_audio_source_rca())
        case .unknown:
            fatalError("Unknown audio source type: \(source)")
        }
    }
    func startMonitors(_ hardware: HardwareType) {
        let monitors: [Monitor] = hardware == .headset ?
            [.currentTrack] :
            [.currentTrack]
        monitors.forEach {
            modelDevice.state.inputs.start(monitorType: $0)()
        }
    }
    func stopMonitors(_ hardware: HardwareType) {
        let monitors: [Monitor] = hardware == .headset ?
            [.currentTrack] :
            [.currentTrack]
        monitors.forEach {
            modelDevice.state.inputs.stop(monitorType: $0)()
        }
    }
}

// MARK: - Inputs and Outpts for View Model
extension PlayerSourcesViewModel: PlayerSourcesViewModelType {
    var inputs: PlayerSourcesViewModelInput { return self }
    var outputs: PlayerSourcesViewModelOutput { return self }
}
