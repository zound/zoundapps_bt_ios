//
//  AudioScrubbingModelTests.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Bartosz Dolewski on 03.09.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//


import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

fileprivate class PlayerDummyState: DeviceStateType {
    static var supportedFeatures: [Feature] = []
    public let perDeviceTypeFeatures: [Feature] = []
    
    var inputs: DeviceStateInput { return self }
    var outputs: DeviceStateOutput { return self }
    
    init() {
    }
}

fileprivate class PlayerDummyDevice: DeviceType {
    func bleConnect() -> Single<Void> {
        return Single.just(())
    }
    
    var mac: MAC?
    var configured = false
    
    var hardwareType: HardwareType = .speaker
    
    func decouple() -> Single<Void> {
        return Single.just(())
    }
    
    func resetSlave() -> Single<Void> {
        return Single.just(())
    }
    
    func initialSetup(with storedEntry: StoredDeviceInfoEntry?) -> Single<String> {
        return Single.just(String())
    }
    
    var modelName: String {
        return String()
    }
    
    var name: BehaviorRelay<String>
    
    var image: DeviceImageProviding = DeviceBlankImageProvider()
    
    
    var connectionTimeout: TimeInterval {
        return TimeInterval.seconds(5.0)
    }
    
    var friendlyName: String { return String(describing: self) }
    var id: String { return String(describing: self) }
    var imageName: String { return String(describing: self) }
    
    let platform = Platform(id: "joplinBT", traits: PlatformTraits())
    
    var state: DeviceStateType
    var otaAdapter: OTAAdapterType?
    
    init(name: BehaviorRelay<String>,
         state: DeviceStateType,
         otaAdapter: OTAAdapterType? = nil) {
        self.name = name
        self.state = state
        self.otaAdapter = otaAdapter
    }
}

extension PlayerDummyState: DeviceStateInput {

}

extension PlayerDummyState: DeviceStateOutput {
    public var activeMonitors: Set<Monitor> {
        get { return  Set<Monitor>() }
        set(newValue) {}
    }
}

final class AudioScrubbingModelTests: XCTestCase {
    var disposeBag: DisposeBag!
    var model: AudioScrubbingModelType!
    
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
        
        let device = PlayerDummyDevice(name: BehaviorRelay<String>(value: String(describing: self)),
                                       state: PlayerDummyState(),
                                       otaAdapter: nil)
        
        model = AudioScrubbingModel(device: device)
    }
    
    func testInitialState() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let accessible = scheduler.createObserver(Bool.self)
        
        model.outputs.accessible.subscribe(accessible).disposed(by: disposeBag)
        
        XCTAssertEqual([.next(TestTime(), false)], accessible.events)
    }
    
    func testAccesibility() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let accessible = scheduler.createObserver(Bool.self)
        
        model.outputs.accessible.subscribe(accessible).disposed(by: disposeBag)
        
        model.inputs.requestScrubbingAccessibility()
        model.inputs.requestScrubbingAccessibility()
        model.inputs.requestScrubbingAccessibility()
        model.inputs.requestScrubbingAccessibility()
        
        // It's always 'false' until iOS support from MediaPlayer will be implemented
        XCTAssertEqual([.next(TestTime(), false),
                        .next(TestTime(), false),
                        .next(TestTime(), false),
                        .next(TestTime(), false),
                        .next(TestTime(), false)], accessible.events)
    }
}
