//
//  AudioSourceViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 25.06.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

/// Input points of the model for Audio Source
protocol AudioSourceViewModelInput {
    /// Activate audio source
    func activate()
    func change(volume: Int)
    func logVolumeChangeAnalyticsEvent(with volume: Int)
}

/// Output points of the model for Aux Audio Source
protocol AudioSourceViewModelOutput {
    /// Notify about visibility of "Activate" button
    //var activateVisible: Observable<Bool> { get }
    var activateVisible: ReplaySubject<Bool> { get }
    
    /// Notify about visibility of AUX source icon
    //var sourceIconVisible: BehaviorRelay<Bool> { get }
    var sourceIconVisible: ReplaySubject<Bool> { get }
    
    /// Notify about visibility of volume bar
    var volumeBarVisible: ReplaySubject<Bool> { get }
    
    var speakerImage: BehaviorRelay<UIImage> { get }
    var iconImage: BehaviorRelay<UIImage> { get }
    
    var volume: ReplaySubject<Int> { get }
    var monitoredVolume: ReplaySubject<Int> { get }
    
    var platformTraits: PlatformTraits { get }
}

/// Protocol to force input/outputs on view model
protocol AudioSourceViewModelType {
    /// Input points of the view model
    var inputs: AudioSourceViewModelInput { get }
    /// Output points of the view model
    var outputs: AudioSourceViewModelOutput { get }
}

/// View model implementation
class AudioSourceViewModel: AudioSourceViewModelInput, AudioSourceViewModelOutput {

    typealias Dependencies = AnalyticsServiceDependency

    var activateVisible = ReplaySubject<Bool>.create(bufferSize: 1)
    var sourceIconVisible = ReplaySubject<Bool>.create(bufferSize: 1)
    var volumeBarVisible = ReplaySubject<Bool>.create(bufferSize: 1)
    var volume = ReplaySubject<Int>.create(bufferSize: 1)
    var monitoredVolume = ReplaySubject<Int>.create(bufferSize: 1)
    
    var platformTraits: PlatformTraits {
        return modelDevice.platform.traits
    }
    
    var speakerImage: BehaviorRelay<UIImage>
    var iconImage: BehaviorRelay<UIImage>
    
    private var disposeBag = DisposeBag()
    private var modelDevice: DeviceType
    private var audioSourceType: AudioSource
    private let dependencies: Dependencies
    
    init(device: DeviceType, audioSource: AudioSource, dependencies: Dependencies) {
        modelDevice = device
        self.dependencies = dependencies
        audioSourceType = audioSource
        
        speakerImage = BehaviorRelay<UIImage>(value: modelDevice.image.mediumImage)
        
        switch audioSourceType {
        case .bluetooth:
            iconImage = BehaviorRelay<UIImage>(value: UIImage.bluetoothLargeImage)
        case .aux:
            iconImage = BehaviorRelay<UIImage>(value: UIImage.auxLargeImage)
        case .rca:
            iconImage = BehaviorRelay<UIImage>(value: UIImage.rcaLargeImage)
        case .unknown:
            iconImage = BehaviorRelay<UIImage>(value: UIImage())
        }

        modelDevice.state.outputs.currentAudioSource().stateObservable()
            .skipOne()
            .distinctUntilChanged()
            .map { [unowned self] in
                $0 != self.audioSourceType
            }.bind(to: activateVisible)
            .disposed(by: disposeBag)
        
        activateVisible
            .bind(to: sourceIconVisible)
            .disposed(by: disposeBag)
        
        activateVisible
            .not()
            .bind(to: volumeBarVisible)
            .disposed(by: disposeBag)
        
        modelDevice.state.outputs.volume().stateObservable()
            .skipOne()
            .bind(to: volume)
            .disposed(by: disposeBag)
        
        modelDevice.state.outputs.volume().stateObservable()
            .observeOn(MainScheduler.instance)
            .skipOne()
            .takeOne()
            .subscribe({ [weak self] volumeEvent in
                guard let volume = volumeEvent.element else { return }
                self?.volume.onNext(volume)
            }).disposed(by: disposeBag)
        
        modelDevice.state.outputs.monitoredVolume().stateObservable()
            .observeOn(MainScheduler.instance)
            .skip(2)
            .subscribe({ [weak self] volumeEvent in
                guard let volume = volumeEvent.element else { return }
                self?.monitoredVolume.onNext(volume)
            }).disposed(by: disposeBag)
        
        modelDevice.state.inputs.requestVolume()
    }
    
    deinit {
        print(#function, String(describing: self))
    }
    
    func activate() {
        switch(audioSourceType) {
        case .bluetooth:
            modelDevice.state.inputs.activateBluetooth()
            dependencies.analyticsService.inputs.log(.appSourceBluetoothActivated, associatedWith: modelDevice.modelName)
        case .aux:
            modelDevice.state.inputs.activateAUX()
            dependencies.analyticsService.inputs.log(.appSourceAuxActivated, associatedWith: modelDevice.modelName)
        case .rca:
            modelDevice.state.inputs.activateRCA()
            dependencies.analyticsService.inputs.log(.appSourceRcaActivated, associatedWith: modelDevice.modelName)
        case .unknown:
            fatalError("Could not activiate unsupported source type: \(audioSourceType)")
        }
    }
    
    func change(volume: Int) {
        modelDevice.state.inputs.write(volume: volume)
    }

    func logVolumeChangeAnalyticsEvent(with volume: Int) {
        dependencies.analyticsService.inputs.log(.appVolumeChanged(volume), associatedWith: modelDevice.modelName)
    }
}

// MARK: - View model's type
extension AudioSourceViewModel: AudioSourceViewModelType {
    var inputs: AudioSourceViewModelInput { return self }
    var outputs: AudioSourceViewModelOutput { return self }
}

