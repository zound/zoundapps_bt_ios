//
//  PlayerSourcesViewModelTests.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Dolewski Bartosz A (Ext) on 04.07.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

fileprivate let mockBt = "BLUETOOTH"
fileprivate let mockAux = "AUX"
fileprivate let mockRca = "RCA"

fileprivate struct PlayerDummyState: DeviceStateType, DeviceStateOutput {
    public var activeMonitors: Set<Monitor> {
        get { return  Set<Monitor>() }
        set(newValue) {}
    }
    static var supportedFeatures: [Feature] = [.hasSwitchableAudioSource]
    public let perDeviceTypeFeatures: [Feature] = [.hasSwitchableAudioSource]
    
    var mockedSource: AudioSource = .unknown
    
    var inputs: DeviceStateInput { return self }
    var outputs: DeviceStateOutput { return self }
    
    func currentAudioSource() -> AnyState<AudioSource> {
        return _currentAudioSource
    }
    func connectivityStatus() -> AnyState<ConnectivityStatus> {
        return _currentConnectivityStatus
    }
    init() {
        self._currentAudioSource = AnyState<AudioSource>(feature: .hasSwitchableAudioSource,
                                                         supportsNotifications: false,
                                                         initialValue: { return AudioSource.unknown })
        self._currentConnectivityStatus = AnyState<ConnectivityStatus>(feature: .hasConnectivityStatus,
                                                                       supportsNotifications: false,
                                                                       initialValue: { return ConnectivityStatus.connected })
    }
    func supportedAudioSources() -> [AudioSource] {
            return [.bluetooth, .aux, .rca]
    }
    fileprivate let _currentAudioSource: AnyState<AudioSource>
    fileprivate let _currentConnectivityStatus: AnyState<ConnectivityStatus>
}

fileprivate class PlayerDummyDevice: DeviceType {
    func bleConnect() -> Single<Void> {
        return Single.just(())
    }
    
    var mac: MAC?
    var configured = false
    var hardwareType: HardwareType = .speaker
    
    func decouple() -> Single<Void> {
        return Single.just(())
    }
    
    func resetSlave() -> Single<Void> {
        return Single.just(())
    }
    func initialSetup(with storedEntry: StoredDeviceInfoEntry?) -> Single<String> {
        return Single.just(String())
    }
    
    var modelName: String {
        return String()
    }
    var image: DeviceImageProviding = DeviceBlankImageProvider()
    
    var connectionTimeout: TimeInterval = TimeInterval.seconds(0.1)
    
    var name: BehaviorRelay<String>
    var id: String { return "TEST_ID" }
    var imageName: String { return String(describing: self) }
    
    let platform = Platform(id: "joplinBT", traits: PlatformTraits())
    
    var state: DeviceStateType
    var otaAdapter: OTAAdapterType?

    init(name: BehaviorRelay<String>,
         state: DeviceStateType,
         otaAdapter: OTAAdapterType? = nil) {
        self.name = name
        self.state = state
        self.otaAdapter = otaAdapter
    }
}

extension PlayerDummyState: DeviceStateInput {
    func requestCurrentAudioSource() {
        _currentAudioSource.stateObservable().accept(mockedSource)
    }
    func activateAUX() {
        _currentAudioSource.stateObservable().accept(.aux)
    }
    
    func startMonitoringAudioSources() {}
    func stopMonitoringAudioSources() {}
}

final class PlayerSourcesViewModelTests: XCTestCase {
    var disposeBag: DisposeBag!
    var vm: PlayerSourcesViewModelType!
    
    override func setUp() {
        super.setUp()
        
        AppEnvironment.pushEnvironment(Environment(language: .en))
        
        disposeBag = DisposeBag()
        let device = PlayerDummyDevice(name: BehaviorRelay<String>(value: String(describing: self)),
                                       state: PlayerDummyState(),
                                       otaAdapter: nil)
        vm = PlayerSourcesViewModel(device: device, dependencies: MockedAnalyticsServiceDependency())
    }
    
    func testInitialState() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let closed = scheduler.createObserver(Void.self)
        let sourceName = scheduler.createObserver(String.self)
        let sourceIndex = scheduler.createObserver(Int.self)
        let dataSource = scheduler.createObserver([SourceModelEntry].self)
        
        vm.outputs.closed.asObservable().subscribe(closed).disposed(by: disposeBag)
        vm.outputs.sourceName.asObservable().subscribe(sourceName).disposed(by: disposeBag)
        vm.outputs.sourceIndex.asObservable().subscribe(sourceIndex).disposed(by: disposeBag)
        vm.outputs.dataSource.asObservable().subscribe(dataSource).disposed(by: disposeBag)
        
        XCTAssertTrue(closed.events.isEmpty)
        XCTAssertTrue(sourceName.events.isEmpty)
        XCTAssertTrue(sourceIndex.events.isEmpty)
        XCTAssertFalse(dataSource.events.isEmpty)
    }
    
    func testClose() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let closed = scheduler.createObserver(Void.self)
        
        vm.outputs.closed.asObservable().subscribe(closed).disposed(by: disposeBag)
        vm.inputs.close()
        
        XCTAssertEqual(closed.events.count, 1)
    }
    
    func testSourceName() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let sourceName = scheduler.createObserver(String.self)
        
        let bluetoothIndex = vm.outputs.dataSource.value.index(where: { $0.type == .bluetooth })!
        let auxIndex = vm.outputs.dataSource.value.index(where: { $0.type == .aux })!
        let rcaIndex = vm.outputs.dataSource.value.index(where: { $0.type == .rca })!
        
        vm.outputs.sourceName.asObservable().subscribe(sourceName).disposed(by: disposeBag)
        
        vm.inputs.select(sourceIdx: bluetoothIndex)
        XCTAssertEqual([.next(TestTime(), mockBt)], sourceName.events)
        
        vm.inputs.select(sourceIdx: auxIndex)
        XCTAssertEqual([.next(TestTime(), mockBt),
                        .next(TestTime(), mockAux)], sourceName.events)
        
        vm.inputs.select(sourceIdx: rcaIndex)
        XCTAssertEqual([.next(TestTime(), mockBt),
                        .next(TestTime(), mockAux),
                        .next(TestTime(), mockRca)], sourceName.events)
    }
    
    func testSourceIndex() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let sourceIndex = scheduler.createObserver(Int.self)
        
        let bluetoothIndex = vm.outputs.dataSource.value.index(where: { $0.type == .bluetooth })!
        let auxIndex = vm.outputs.dataSource.value.index(where: { $0.type == .aux })!
        let rcaIndex = vm.outputs.dataSource.value.index(where: { $0.type == .rca })!
        
        vm.outputs.sourceIndex.asObservable().subscribe(sourceIndex).disposed(by: disposeBag)
        
        vm.inputs.select(sourceIdx: bluetoothIndex)
        XCTAssertEqual([.next(TestTime(), 0)], sourceIndex.events)
        
        vm.inputs.select(sourceIdx: auxIndex)
        XCTAssertEqual([.next(TestTime(), 0),
                        .next(TestTime(), 1)], sourceIndex.events)
        
        vm.inputs.select(sourceIdx: rcaIndex)
        XCTAssertEqual([.next(TestTime(), 0),
                        .next(TestTime(), 1),
                        .next(TestTime(), 2)], sourceIndex.events)
    }
    
    func testReturnDevice() {
        XCTAssertTrue(vm.outputs.device().id == "TEST_ID")
    }
}
