//
//  AudioScrubbingModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 03.09.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import GUMA
import RxSwift

/// Input points of the PlaybackControlsModel
protocol AudioScrubbingModelInput {
    func requestScrubbingAccessibility()
}

/// Output points of the PlaybackControlsModel
protocol AudioScrubbingModelOutput {
    var accessible: BehaviorRelay<Bool> { get }
}

/// Protocol to force input/outputs on playback controls model
protocol AudioScrubbingModelType {
    /// Input points of the model
    var inputs: AudioScrubbingModelInput { get }
    /// Output points of the model
    var outputs: AudioScrubbingModelOutput { get }
}

class AudioScrubbingModel: AudioScrubbingModelInput, AudioScrubbingModelOutput {
    var accessible = BehaviorRelay<Bool>(value: false)
    
    private let modelDevce: DeviceType
    
    init(device: DeviceType) {
        modelDevce = device
    }
    
    func requestScrubbingAccessibility() {
        accessible.accept(false)
    }
}

extension AudioScrubbingModel: AudioScrubbingModelType {
    var inputs: AudioScrubbingModelInput { return self }
    var outputs: AudioScrubbingModelOutput { return self }
}
