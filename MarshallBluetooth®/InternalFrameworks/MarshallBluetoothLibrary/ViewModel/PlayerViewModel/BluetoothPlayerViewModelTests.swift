//
//  BluetoothPlayerViewModelTests.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Bartosz Dolewski on 19.07.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

/// Global configuration mock
fileprivate var mockPlaybackStatus: PlaybackStatus = .unknown
fileprivate var mockNoData = ""

fileprivate var initTitle = "Freedy Freeloader"
fileprivate var initArtist = "Miles Davis"
fileprivate var initAlbum = "Kind of Blue"

fileprivate var mockTrackInfo = TrackInfo(title: "Freedy Freeloader",
                                          artist: "Miles Davis",
                                          album: "Kind of Blue",
                                          number: "2",
                                          totalNumber: "5",
                                          genre: "Jazz",
                                          playingTime: "9:46")

fileprivate struct PlayerDummyState: DeviceStateType {
    static var supportedFeatures: [Feature] = [.hasSwitchableAudioSource]
    let perDeviceTypeFeatures: [Feature] = [.hasSwitchableAudioSource]
    
    var mockedSource: AudioSource = .unknown
    
    var inputs: DeviceStateInput { return self }
    var outputs: DeviceStateOutput { return self }
    
    fileprivate let _currentAudioSource: AnyState<AudioSource>
    fileprivate let _volumeStatus: AnyState<Int>
    fileprivate let _playbackStatus: AnyState<PlaybackStatus>
    fileprivate let _currentTrackInfo: AnyState<TrackInfo>
    
    init() {
        self._currentAudioSource = AnyState<AudioSource>(feature: .hasSwitchableAudioSource,
                                                         supportsNotifications: false,
                                                         initialValue: { return AudioSource.unknown })
        
        self._volumeStatus = AnyState<Int>(feature: .hasContinuousVolume,
                                           supportsNotifications: false,
                                           initialValue: { return Int() })
        
        self._playbackStatus = AnyState<PlaybackStatus>(feature: .hasAudioPlayback,
                                                        supportsNotifications: false,
                                                        initialValue: { return mockPlaybackStatus } )
        
        self._currentTrackInfo = AnyState<TrackInfo>(feature: .hasAudioPlayback,
                                                     supportsNotifications: false,
                                                     initialValue: { return TrackInfo() } )
    }
    
    func supportedAudioSources() -> [AudioSource] {
        return [.bluetooth, .aux, .rca]
    }
}

fileprivate class PlayerDummyDevice: DeviceType {
    func bleConnect() -> Single<Void> {
        return Single.just(())
    }
    
    var mac: MAC?
    var configured = false
    var hardwareType: HardwareType = .speaker
    
    func decouple() -> Single<Void> {
        return Single.just(())
    }
    
    func resetSlave() -> Single<Void> {
        return Single.just(())
    }
    
    func initialSetup(with storedEntry: StoredDeviceInfoEntry?) -> Single<String> {
        return Single.just(String())
    }
    
    var connectionTimeout: TimeInterval = TimeInterval.seconds(0.1)
    
    var modelName: String {
        return String()
    }
    var name: BehaviorRelay<String>
    
    var image: DeviceImageProviding = DeviceBlankImageProvider()
    
    
    var id: String { return String(describing: self) }
    var imageName: String { return String(describing: self) }
    
    let platform = Platform(id: "joplinBT", traits: PlatformTraits())
    
    var state: DeviceStateType
    var otaAdapter: OTAAdapterType?

    init(name: BehaviorRelay<String>,
         state: DeviceStateType,
         otaAdapter: OTAAdapterType? = nil) {
        self.name = name
        self.state = state
        self.otaAdapter = otaAdapter
    }
}

extension PlayerDummyState: DeviceStateInput {
    func requestCurrentAudioSource() {
    }
    
    func requestVolume() {
    }
    
    func requestPlaybackStatus() {
        _playbackStatus.stateObservable().accept(mockPlaybackStatus)
    }
    
    func currentAudioSource() -> AnyState<AudioSource> {
        return _currentAudioSource
    }
    
    func play() {
        mockPlaybackStatus = .playing
        _playbackStatus.stateObservable().accept(mockPlaybackStatus)
    }
    
    func pause() {
        mockPlaybackStatus = .paused
        _playbackStatus.stateObservable().accept(mockPlaybackStatus)
    }
    
    func requestCurrentTrackInfo() {
        _currentTrackInfo.stateObservable().accept(mockTrackInfo)
    }
    
    func startMonitoringAudioSources() {}
    func stopMonitoringAudioSources() {}
}

extension PlayerDummyState: DeviceStateOutput {
    public var activeMonitors: Set<Monitor> {
        get { return  Set<Monitor>() }
        set(newValue) {}
    }
    
    func volume() -> AnyState<Int> {
        return _volumeStatus
    }
    
    func playbackStatus() -> AnyState<PlaybackStatus> {
        return _playbackStatus
    }
    
    func trackInfo() -> AnyState<TrackInfo> {
        return _currentTrackInfo
    }
}

final class BluetoothPlayerViewModelTests: XCTestCase {
    var disposeBag: DisposeBag!
    var vm: BluetoothPlayerViewModelType!
    var device: DeviceType!

    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
        
        device = PlayerDummyDevice(name: BehaviorRelay<String>(value: String(describing: self)),
                                   state: PlayerDummyState(),
                                   otaAdapter: nil)

        vm = BluetoothPlayerViewModel(device: device, dependencies: MockedAnalyticsServiceDependency())
    }
    
    func testInitialState() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let isPlaying = scheduler.createObserver(Bool.self)
        let songTitle = scheduler.createObserver(String.self)
        let artist = scheduler.createObserver(String.self)
        let album = scheduler.createObserver(String.self)
        
        vm.outputs.isPlaying.asObservable().subscribe(isPlaying).disposed(by: disposeBag)
        vm.outputs.title.asObservable().subscribe(songTitle).disposed(by: disposeBag)
        vm.outputs.artist.asObservable().subscribe(artist).disposed(by: disposeBag)
        vm.outputs.album.asObservable().subscribe(album).disposed(by: disposeBag)
        
        device.state.inputs.requestPlaybackStatus()
        device.state.inputs.requestCurrentTrackInfo()
        
        XCTAssertEqual([.next(TestTime(), false)], isPlaying.events)
        
        XCTAssertEqual([.next(TestTime(), initTitle)], songTitle.events)
        XCTAssertEqual([.next(TestTime(), initArtist)], artist.events)
        XCTAssertEqual([.next(TestTime(), initAlbum)], album.events)
    }
    
    func testPlayPause() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let isPlaying = scheduler.createObserver(Bool.self)
        
        vm.outputs.isPlaying.asObservable().subscribe(isPlaying).disposed(by: disposeBag)
        device.state.inputs.requestPlaybackStatus()
        
        XCTAssertEqual([.next(TestTime(), false)], isPlaying.events)
        
        mockPlaybackStatus = .paused
        device.state.inputs.requestPlaybackStatus()
        
        vm.inputs.playPause()
        XCTAssertEqual([.next(TestTime(), false),
                        .next(TestTime(), false),
                        .next(TestTime(), true)], isPlaying.events)
        
        vm.inputs.playPause()
        XCTAssertEqual([.next(TestTime(), false),
                        .next(TestTime(), false),
                        .next(TestTime(), true),
                        .next(TestTime(), false)], isPlaying.events)
        
        vm.inputs.playPause()
        XCTAssertEqual([.next(TestTime(), false),
                        .next(TestTime(), false),
                        .next(TestTime(), true),
                        .next(TestTime(), false),
                        .next(TestTime(), true)], isPlaying.events)
        
        vm.inputs.playPause()
        XCTAssertEqual([.next(TestTime(), false),
                        .next(TestTime(), false),
                        .next(TestTime(), true),
                        .next(TestTime(), false),
                        .next(TestTime(), true),
                        .next(TestTime(), false)], isPlaying.events)
    }
    
    func testNoTrackInfo() {
        let save = mockTrackInfo
        mockTrackInfo = TrackInfo()
        
        let scheduler = TestScheduler(initialClock: TestTime())
        let songTitle = scheduler.createObserver(String.self)
        let artist = scheduler.createObserver(String.self)
        let album = scheduler.createObserver(String.self)
        
        vm.outputs.title.asObservable().subscribe(songTitle).disposed(by: disposeBag)
        vm.outputs.artist.asObservable().subscribe(artist).disposed(by: disposeBag)
        vm.outputs.album.asObservable().subscribe(album).disposed(by: disposeBag)
        
        
        device.state.inputs.requestCurrentTrackInfo()
        
        XCTAssertEqual([.next(TestTime(), initTitle),
                        .next(TestTime(), mockNoData)], songTitle.events)
        XCTAssertEqual([.next(TestTime(), initArtist),
                        .next(TestTime(), mockNoData)], artist.events)
        XCTAssertEqual([.next(TestTime(), initAlbum),
                        .next(TestTime(), mockNoData)], album.events)
        
        mockTrackInfo = save
    }
}
