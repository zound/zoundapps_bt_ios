//
//  TrackInfoModelTests.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Bartosz Dolewski on 17.08.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

/// Global configuration mocks
fileprivate var mockNoData = ""
fileprivate var mockTrackInfo = TrackInfo(title: "Freedy Freeloader",
                                          artist: "Miles Davis",
                                          album: "Kind of Blue",
                                          number: "2",
                                          totalNumber: "5",
                                          genre: "Jazz",
                                          playingTime: "9:46")

fileprivate class PlayerDummyState: DeviceStateType {
    static var supportedFeatures: [Feature] = [.hasAudioPlayback]
    public let perDeviceTypeFeatures: [Feature] = [.hasAudioPlayback]
    
    var mockedSource: AudioSource = .unknown
    
    var inputs: DeviceStateInput { return self }
    var outputs: DeviceStateOutput { return self }
    
    fileprivate let _currentTrackInfo: AnyState<TrackInfo>
    
    init() {
        self._currentTrackInfo = AnyState<TrackInfo>(feature: .hasAudioPlayback,
                                                     supportsNotifications: false,
                                                     initialValue: { return TrackInfo() } )
    }
}

fileprivate class PlayerDummyDevice: DeviceType {
    func bleConnect() -> Single<Void> {
        return Single.just(())
    }
    
    var mac: MAC?
    var configured = false
    var hardwareType: HardwareType = .speaker
    
    func decouple() -> Single<Void> {
        return Single.just(())
    }
    
    func resetSlave() -> Single<Void> {
        return Single.just(())
    }
    
    func initialSetup(with storedEntry: StoredDeviceInfoEntry?) -> Single<String> {
        return Single.just(String())
    }
    
    var modelName: String {
        return String()
    }
    
    var name: BehaviorRelay<String>
    
    var image: DeviceImageProviding = DeviceBlankImageProvider()
    
    
    var connectionTimeout: TimeInterval {
        return TimeInterval.seconds(5.0)
    }
    
    var friendlyName: String { return String(describing: self) }
    var id: String { return String(describing: self) }
    var imageName: String { return String(describing: self) }
    
    let platform = Platform(id: "joplinBT", traits: PlatformTraits())
    
    var state: DeviceStateType
    var otaAdapter: OTAAdapterType?

    init(name: BehaviorRelay<String>,
         state: DeviceStateType,
         otaAdapter: OTAAdapterType? = nil) {
        self.name = name
        self.state = state
        self.otaAdapter = otaAdapter
    }
}

extension PlayerDummyState: DeviceStateInput {
    func requestCurrentTrackInfo() {
        _currentTrackInfo.stateObservable().accept(mockTrackInfo)
    }
}

extension PlayerDummyState: DeviceStateOutput {
    public var activeMonitors: Set<Monitor> {
        get { return  Set<Monitor>() }
        set(newValue) {}
    }
    func trackInfo() -> AnyState<TrackInfo> {
        return _currentTrackInfo
    }
}

final class TrackInfoModelTests: XCTestCase {
    var disposeBag: DisposeBag!
    var model: TrackInfoModel!
    
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
        
        let device = PlayerDummyDevice(name: BehaviorRelay<String>(value: String(describing: self)),
                                        state: PlayerDummyState(),
                                        otaAdapter: nil)
        
        model = TrackInfoModel(device: device)
    }
    
    func testSongTitle() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let title = scheduler.createObserver(String.self)
        
        model.outputs.title.asObservable().subscribe(title).disposed(by: disposeBag)
        
        XCTAssertEqual([.next(TestTime(), mockNoData)], title.events)
        
        model.requestTrackInfo()
        XCTAssertEqual([.next(TestTime(), mockNoData),
                        .next(TestTime(), "Freedy Freeloader")], title.events)
    }
    
    func testArtistName() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let artist = scheduler.createObserver(String.self)
        
        model.outputs.artist.asObservable().subscribe(artist).disposed(by: disposeBag)
        
        XCTAssertTrue(artist.events.isEmpty)
        
        model.requestTrackInfo()
        XCTAssertEqual([.next(TestTime(), "Miles Davis")], artist.events)
    }
    
    func testAlbumName() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let album = scheduler.createObserver(String.self)
        
        model.outputs.album.asObservable().subscribe(album).disposed(by: disposeBag)
        
        XCTAssertTrue(album.events.isEmpty)
        
        model.requestTrackInfo()
        XCTAssertEqual([.next(TestTime(), "Kind of Blue")], album.events)
    }
}
