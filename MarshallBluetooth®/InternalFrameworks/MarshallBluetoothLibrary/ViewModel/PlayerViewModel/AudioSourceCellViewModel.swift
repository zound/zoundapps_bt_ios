//
//  AudioSourceCellViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 27.06.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import GUMA

protocol AudioSourceCellViewModelInput {
}

protocol AudioSourceCellViewModelOutput {
    var type: AudioSource { get }
}

protocol AudioSourceCellViewModelType {
    var inputs: AudioSourceCellViewModelInput { get }
    var outputs: AudioSourceCellViewModelOutput { get }
}

class AudioSourceCellViewModel: AudioSourceCellViewModelInput, AudioSourceCellViewModelOutput {
    var type: AudioSource
    
    private let modelDevice: DeviceType
    
    init(device: DeviceType, type: AudioSource, name: String) {
        self.modelDevice = device
        self.type = type
    }
}

extension AudioSourceCellViewModel: AudioSourceCellViewModelType {
    var inputs: AudioSourceCellViewModelInput { return self }
    var outputs: AudioSourceCellViewModelOutput { return self }
}
