//
//  DeviceSettingsCellViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 03.06.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift

protocol DeviceSettingsCellViewModelInputs {
    func configure(enabled: Bool)
    func configure(name: String)
    func configure(updateAvailable: Bool)
}

protocol DeviceSettingsCellViewModelOutputs {
    var name: BehaviorRelay<String> { get }
    var enabled: BehaviorRelay<Bool> { get }
    var updateNotification: BehaviorRelay<Bool> { get }
}

protocol DeviceSettingsCellViewModelType {
    var inputs: DeviceSettingsCellViewModelInputs { get }
    var outputs: DeviceSettingsCellViewModelOutputs { get }
}

class DeviceSettingsCellViewModel: DeviceSettingsCellViewModelInputs, DeviceSettingsCellViewModelOutputs {
    
    var name = BehaviorRelay<String>.init(value: "")
    var enabled = BehaviorRelay<Bool>.init(value: true)
    var updateNotification = BehaviorRelay<Bool>.init(value: false)
    
    func configure(enabled: Bool) {
        self.enabled.accept(enabled)
    }
    
    func configure(name: String) {
        self.name.accept(name)
    }
    
    func configure(updateAvailable: Bool) {
        self.updateNotification.accept(updateAvailable)
    }
}

extension DeviceSettingsCellViewModel : DeviceSettingsCellViewModelType {
    var inputs: DeviceSettingsCellViewModelInputs { return self }
    var outputs: DeviceSettingsCellViewModelOutputs { return self }
}
