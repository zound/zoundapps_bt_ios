//
//  CommonCellViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Paprota Przemyslaw on 10/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

protocol CommonMenuCellViewModelInputs {
    
    func configure(enabled: Bool)
    
    func configure(name: String)
}

protocol CommonMenuCellViewModelOutputs {
    /// Text to display
    var name: BehaviorRelay<String> { get }
    /// Enable/disable state
    var enabled: BehaviorRelay<Bool> { get }
}

protocol CommonMenuCellViewModelType {
    var inputs: CommonMenuCellViewModelInputs { get }
    var outputs: CommonMenuCellViewModelOutputs { get }
}

class CommonMenuCellViewModel: CommonMenuCellViewModelInputs, CommonMenuCellViewModelOutputs {
    
    var name = BehaviorRelay<String>.init(value: "")
    
    var enabled = BehaviorRelay<Bool>.init(value: true)
    
    func configure(enabled: Bool) {
        self.enabled.accept(enabled)
    }
    
    func configure(name: String) {
        self.name.accept(name)
    }
}

extension CommonMenuCellViewModel : CommonMenuCellViewModelType {
    var inputs: CommonMenuCellViewModelInputs { return self }
    var outputs: CommonMenuCellViewModelOutputs { return self }
}
