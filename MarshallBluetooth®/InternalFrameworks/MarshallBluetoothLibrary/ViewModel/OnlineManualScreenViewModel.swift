//
//  OnlineManualScreenViewModel.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 10/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

class OnlineManualScreenViewModel: CommonMenuViewModel {
    override init() {
        super.init()
        var  items = [CommonMenuItem]()
        items.append(CommonMenuItem(nextScreen: .onlineManualActonII, label: Environment.acton))
        items.append(CommonMenuItem(nextScreen: .onlineManualStanmoreII, label: Environment.stanmore))
        items.append(CommonMenuItem(nextScreen: .onlineManualWoburnII, label: Environment.woburn))
        items.append(CommonMenuItem(nextScreen: .onlineManualMonitorII, label: Environment.monitor))
        dataSource.accept(items)
        title.accept(Strings.main_menu_item_online_manual_uc())
    }
}
