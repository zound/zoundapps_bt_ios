//
//  OzzyAdvertisementViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 08/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

protocol OzzyAdvertisementViewModelInput {
    func viewAppeared()
    func skipTapped()
}
protocol OzzyAdvertisementViewModelOutput {
    var viewReady:PublishRelay<Void> { get }
    var advertisingFinished: PublishRelay<Void> { get }
    var skip: PublishRelay<Void> { get }
}
protocol OzzyAdvertisementViewModelType {
    var inputs: OzzyAdvertisementViewModelInput { get }
    var outputs: OzzyAdvertisementViewModelOutput { get }
}
final class OzzyAdvertisementViewModel: OzzyAdvertisementViewModelInput, OzzyAdvertisementViewModelOutput, OzzyAdvertisementViewModelType {
    var inputs: OzzyAdvertisementViewModelInput { return self }
    var outputs: OzzyAdvertisementViewModelOutput { return self }
    var viewReady = PublishRelay<Void>()
    var advertisingFinished = PublishRelay<Void>()
    var skip = PublishRelay<Void>()
    let device: DeviceType
    init(device: DeviceType) {
        self.device = device
    }
    func viewAppeared() {
        viewReady.accept(())
    }
    func skipTapped() {
        skip.accept(())
    }
}

