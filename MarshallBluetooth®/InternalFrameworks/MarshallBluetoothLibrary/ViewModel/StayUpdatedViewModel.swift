//
//  SetupViewModel.swift
//  MarshallBluetoothLibrary
//

import RxSwift
import Foundation
import GUMA

/// Implementation of e-mail validation for SignUp flow
struct EmailValidator: EmailValidating { }

/// Input points of the model of StayUpdated screen
protocol StayUpdatedViewModelInput {
    /// Change e-mail to be validated
    ///
    /// - Parameter name: e-mail
    func emailChanged(name: String)
    /// Service the tap on the Next button
    func nextButtonTapped()
    /// Service the tap on the Skip button
    func skipButtonTapped()
    /// Service the tap on the Unsubscribe button
    func unsubscribeButtonTapped()
    /// Service the tap on the Yes button  on confirmation screen
    func unsubscribeConfirmed(isConfirmed: Bool)
}

private struct StayUpdatedConstants {
    static let charactersToActivateValidator = 3
    static let invalidEmailIcon = UIImage.failIcon
    static let correctEmailIcon = UIImage.checkmarkMarshall
    static let invalidEmailText = Strings.stay_updated_email_validation_fail()
    static let correctEmailText = Strings.stay_updated_email_validation_ok()
    static let communicationErrorText = Strings.error_common()
    static let communicationErrorIcon = UIImage.warningIcon
}

/// Output points of the model of StayUpdated screen
protocol StayUpdatedViewModelOutput {
    /// Notify when StayUpdated screen should be switched to next one
    var viewCompleted: BehaviorRelay<Bool> { get }
    /// Notify when StayUpdated screen should be skipped (with entire flow)
    var skipped: BehaviorRelay<Bool> { get }
    /// Notify when e-mail validation passed and user can proceeed further in flow
    var nextButtonVisible: BehaviorRelay<Bool> { get }
    var nextButtonEnable: BehaviorRelay<Bool> { get }
    var unsubscribeButtonVisible: BehaviorRelay<Bool> { get }
    var skipButtonVisible: BehaviorRelay<Bool> { get }
    /// Notify when email is sending to server
    var indicatorVisible: BehaviorRelay<Bool> { get }
    /// Email editing is disabled during communication with server.
    var emailEditingEnable: BehaviorRelay<Bool> { get }
    /// Text information about errors and email address validation result
    var descriptionText: BehaviorRelay<String> { get }
    /// Icon indicator about errors and email address validation result
    var descriptionIcon: BehaviorRelay<UIImage?> { get }
    var subscriptionEmail: BehaviorRelay<String> { get }
    var editorAreaVisible: BehaviorRelay<Bool> { get }
    var emailAreaVisible: BehaviorRelay<Bool> { get }
    var unsubscribeRequested: BehaviorRelay<Void> { get }
    var resetEmailEditor: BehaviorRelay<Void> { get }
}

/// Protocol to force input/outputs on view model
protocol StayUpdatedViewModelType {
    
    /// Input points of the view model
    var inputs: StayUpdatedViewModelInput { get }
    
    /// Output points of the view model
    var outputs: StayUpdatedViewModelOutput { get }
}

/// View model implementation for StayUpdated screen
final class StayUpdatedViewModel: StayUpdatedViewModelInput, StayUpdatedViewModelOutput, StayUpdatedViewModelType {

    typealias Dependencies = AnalyticsServiceDependency & NewsletterServiceDependency

    var viewCompleted = BehaviorRelay<Bool>(value: false)
    var skipped = BehaviorRelay<Bool>(value: false)
    
    var nextButtonVisible = BehaviorRelay<Bool>(value: false)
    var nextButtonEnable = BehaviorRelay<Bool>(value: false)
    var unsubscribeButtonVisible = BehaviorRelay<Bool>(value: false)
    var skipButtonVisible = BehaviorRelay<Bool>(value: false)

    var indicatorVisible = BehaviorRelay<Bool>(value: false)
    var emailEditingEnable = BehaviorRelay<Bool>(value: true)
    var descriptionText = BehaviorRelay<String>(value: "")
    var descriptionIcon = BehaviorRelay<UIImage?>(value: nil)
    var subscriptionEmail = BehaviorRelay<String>(value: "")

    var editorAreaVisible = BehaviorRelay<Bool>(value: false)
    var emailAreaVisible = BehaviorRelay<Bool>(value: false)

    var resetEmailEditor = BehaviorRelay<Void>(value: ())
    var unsubscribeRequested = BehaviorRelay<Void>(value: ())

    var inputs: StayUpdatedViewModelInput { return self }
    var outputs: StayUpdatedViewModelOutput { return self }

    private var currentEmail = BehaviorRelay<String>(value: "")
    private let validator = EmailValidator()
    private let dependencies: Dependencies
    private var disposeBag = DisposeBag()
    
    init(setup: Bool, dependencies: Dependencies) {
        self.dependencies = dependencies

        skipButtonVisible.accept(setup)
        setupEmailValidation()
        updateSubscriptionState()
        dependencies.newsletterService.delegate = self
    }
    
    func setCommunicationErrorState() {
        descriptionIcon.accept(StayUpdatedConstants.communicationErrorIcon)
        descriptionText.accept(StayUpdatedConstants.communicationErrorText)
    }
    
    /// Handle Next button (send subscription)
    func nextButtonTapped() {
        sendSubscription()
    }
    
    /// Make the SignUp screen skipped by tapping the Skip button
    func skipButtonTapped() {
        dependencies.analyticsService.inputs.log(.appOnboardingStayUpdatedSkipped)
        skipped.accept(true)
    }
    
    /// Change current e-mail for validation
    ///
    /// - Parameter name: e-mail to be validated
    func emailChanged(name: String) {
        currentEmail.accept(name)
    }
    
    func unsubscribeConfirmed(isConfirmed : Bool) {
        if isConfirmed {
            sendUnsubscribe()
        } else {
            unsubscribeButtonVisible.accept(true)
        }
    }
    
    func unsubscribeButtonTapped() {
        unsubscribeButtonVisible.accept(false)
        unsubscribeRequested.accept(())
    }

}

private extension StayUpdatedViewModel {
    func setupEmailValidation() {

        // emit events when user typed enough characters to activate validator
        let validation = currentEmail
            .asObservable()
            .filter { $0.count >= StayUpdatedConstants.charactersToActivateValidator }
            .map(validator.validate)
            .share()

        // emit events when user typed not enough characters
        let notReadyForValidation = currentEmail
            .asObservable()
            .filter { $0.count < StayUpdatedConstants.charactersToActivateValidator }
            .share()

        // bind validation result with button visibility
        validation
            .bind(to: nextButtonEnable)
            .disposed(by: disposeBag)

        // validation result for icon
        validation
            .map { $0 ? StayUpdatedConstants.correctEmailIcon : StayUpdatedConstants.invalidEmailIcon }
            .bind(to: descriptionIcon)
            .disposed(by: disposeBag)

        // validation result for text
        validation
            .map { $0 ? StayUpdatedConstants.correctEmailText : StayUpdatedConstants.invalidEmailText }
            .bind(to: descriptionText)
            .disposed(by: disposeBag)

        // reset validation icon & text
        notReadyForValidation
            .map { _ in nil }
            .bind(to: descriptionIcon)
            .disposed(by: disposeBag)

        notReadyForValidation
            .map { _ in "" }
            .bind(to: descriptionText)
            .disposed(by: disposeBag)
    }
}

// MARK: - Subscription status
private extension StayUpdatedViewModel {
    private func updateSubscriptionState() {
        let email = dependencies.newsletterService.subscriptionEmail
        let subscribed = !email.isEmpty
        
        editorAreaVisible.accept(!subscribed)
        emailAreaVisible.accept(subscribed)
        unsubscribeButtonVisible.accept(subscribed)
        nextButtonVisible.accept(!subscribed)
        subscriptionEmail.accept(email)
    }
}

// MARK: - Subscribe
private extension StayUpdatedViewModel {
    /// Set failure state in case of empty result.
    /// If result is not empty the the "Stay Updated" screen completed.
    ///
    /// - Parameter result: request result (email address or empty string)
    private func handleSubscriptionResult(result: NewsletterResult) {
        indicatorVisible.accept(false)
        emailEditingEnable.accept(true)
        guard result == .ok else {
            nextButtonVisible.accept(true)
            setCommunicationErrorState()
            return
        }
        updateSubscriptionState()
        viewCompleted.accept(true)
        dependencies.analyticsService.inputs.log(.appNewsletterSignedUp)
    }
    
    /// Send email address to server.
    private func sendSubscription() {
        let email = self.currentEmail.value
        guard validator.validate(email: email) else {
            return
        }
        descriptionIcon.accept(nil)
        descriptionText.accept("")
        indicatorVisible.accept(true)
        emailEditingEnable.accept(false)
        nextButtonVisible.accept(false)
        dependencies.newsletterService.signUp(with: email)
    }
}

// MARK: - Unsubscribe
private extension StayUpdatedViewModel {
    private func handleUnsubscribeResult(result: NewsletterResult) {
        indicatorVisible.accept(false)
        guard result == .ok else {
            unsubscribeButtonVisible.accept(true)
            setCommunicationErrorState()
            return
        }
        resetEmailEditor.accept(())
        emailChanged(name: "")
        updateSubscriptionState()
        viewCompleted.accept(true)
    }
    
    private func sendUnsubscribe() {
        indicatorVisible.accept(true)
        unsubscribeButtonVisible.accept(false)
        dependencies.newsletterService.signOut()
    }
}
extension StayUpdatedViewModel: NewsletterServiceResult {
    func signUpFinishedWithResult(result: NewsletterResult) {
        handleSubscriptionResult(result: result)
    }
    func signOutFinishedWithResult(result: NewsletterResult) {
        handleUnsubscribeResult(result: result)
    }
}
