//
//  DeviceSettingsCellViewModelTests.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 25.07.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary

final class DeviceSettingsCellViewModelTests: XCTestCase {
    var disposeBag: DisposeBag!
    var viewModel: DeviceSettingsCellViewModelType!
    
    override func setUp() {
        super.setUp()
        
        disposeBag = DisposeBag()
        viewModel = DeviceSettingsCellViewModel()
    }
    
    func testConfigureEnabled() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let enabled = scheduler.createObserver(Bool.self)
        
        viewModel.outputs.enabled.asObservable().subscribe(enabled).disposed(by: disposeBag)
        
        viewModel.inputs.configure(enabled: true)
        viewModel.inputs.configure(enabled: false)
        
        XCTAssertEqual([.next(TestTime(), true),
                        .next(TestTime(), true),
                        .next(TestTime(), false) ], enabled.events)
    }
    
    func testConfigureName() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let name = scheduler.createObserver(String.self)
        
        viewModel.outputs.name.asObservable().subscribe(name).disposed(by: disposeBag)
        
        viewModel.inputs.configure(name: "AAA")
        viewModel.inputs.configure(name: "BBB")
        
        XCTAssertEqual([.next(TestTime(), ""),
                        .next(TestTime(), "AAA"),
                        .next(TestTime(), "BBB") ], name.events)
    }
    
    func testConfigureUpdateAvailable() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let update = scheduler.createObserver(Bool.self)
        
        viewModel.outputs.updateNotification.asObservable().subscribe(update).disposed(by: disposeBag)
        
        viewModel.inputs.configure(updateAvailable: true)
        viewModel.inputs.configure(updateAvailable: false)
        
        XCTAssertEqual([.next(TestTime(), false),
                        .next(TestTime(), true),
                        .next(TestTime(), false) ], update.events)
    }
}
