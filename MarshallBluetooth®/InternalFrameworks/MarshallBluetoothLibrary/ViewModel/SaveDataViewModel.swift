//
//  SaveDataViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 26.03.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift

/// Output points of the model of Saving Data screen
protocol SaveDataViewModelOutput {
    
    /// Notify when this view finished saving data
    var viewCompleted: BehaviorRelay<Bool> { get }
}

/// Output points of Save Data view model since it does not have input points (yet)
protocol SaveDataViewModelType {
    var outputs: SaveDataViewModelOutput { get }
}

/// View model implementation of Sava Data screen
final class SaveDataViewModel: SaveDataViewModelOutput, SaveDataViewModelType {
    
    var viewCompleted = BehaviorRelay<Bool>(value: false)
    var outputs: SaveDataViewModelOutput { return self }
}
