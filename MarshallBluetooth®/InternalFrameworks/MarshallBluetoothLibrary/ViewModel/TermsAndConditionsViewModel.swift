//
//  TermsAndConditionsViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 23.03.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift

protocol TermsAndConditionsViewModelInput {
    func backButtonTapped()
}

protocol TermsAndConditionsViewModelOutput {
    var back: BehaviorRelay<Bool> { get }
}

protocol TermsAndConditionsViewModelType {
    var inputs: TermsAndConditionsViewModelInput { get }
    var outputs: TermsAndConditionsViewModelOutput { get }
}

final class TermsAndConditionsViewModel: TermsAndConditionsViewModelInput, TermsAndConditionsViewModelOutput, TermsAndConditionsViewModelType {
    
    var back: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    var inputs: TermsAndConditionsViewModelInput { return self }
    var outputs: TermsAndConditionsViewModelOutput { return self }

    func backButtonTapped() {
        back.accept(true)
    }
}
