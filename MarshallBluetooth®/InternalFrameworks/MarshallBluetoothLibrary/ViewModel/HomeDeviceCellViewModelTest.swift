//
//  HomeDeviceCellViewModelTest.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Grzegorz Kiel on 04/04/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

//import XCTest
//import RxSwift
//import RxTest
//@testable import MarshallBluetoothLibrary
//import GUMA
//
///* Mocked device entry from JSON
// {"name" : "FIRST", "id" : "00:a2:c2:d5:d4:a3", "playing" : false, "connected" : false, "hasUpdate" : true},
// */
//
//fileprivate let mockConnected = "Connected"
//fileprivate let mockNotConnected = "Not connected"
//fileprivate let mockSetup = "SETUP"
//
//final class HomeDeviceCellViewModelTest: XCTestCase, HomeDeviceCellViewModelOutputs {
//    
//    var configureDevice = PublishRelay<Void>()
//    var decoupleDevices = PublishRelay<Void>()
//    var deviceImage = BehaviorRelay<UIImage>.init(value: UIImage())
//    var friendlyName = BehaviorRelay<String>.init(value: "")
//    var statusLabelVisible = BehaviorRelay<Bool>.init(value: false)
//    var deviceConnected = BehaviorRelay<Bool>.init(value: false)
//    var statusLabelText = BehaviorRelay<String>.init(value: "")
//    var mediaPartVisible = BehaviorRelay<Bool>.init(value: false)
//    var connectivityButtonVisible = BehaviorRelay<Bool>.init(value: false)
//    var connectivityButtonLabelText = BehaviorRelay<String>.init(value: mockSetup)
//    var connectedCheckmarkVisible = BehaviorRelay<Bool>.init(value: false)
//    var showPlayerTriggered = PublishRelay<String>()
//    var showDeviceSettingsTriggered = PublishRelay<String>()
//    var hideDeviceSettingsTriggered: PublishRelay<Bool> = PublishRelay<Bool>.init()
//    var deviceSetupTriggered = PublishRelay<OTARequestType>.init()
//    var forcedOTAButtonVisible = BehaviorRelay<Bool>.init(value: false)
//    var decoupleButtonVisible = BehaviorRelay<Bool>.init(value: false)
//    var deviceConnectingActivityIndicatorVisible = BehaviorRelay<Bool>.init(value: false)
//    var updateAvailable = BehaviorRelay<Bool>.init(value: false)
//    var batteryBlockVisible = BehaviorRelay<Bool>.init(value: false)
//    var batteryStatus = BehaviorRelay<BatteryStatus>.init(value: .full)
//    
//    var volume = PublishRelay<Int>()
//    var monitoredVolume = PublishRelay<Int>()
//    var platformTraits = PlatformTraits()
//    
//    var testfriendlyName: TestableObserver<String>!
//    var testStatusLabelVisible: TestableObserver<Bool>!
//    var testDeviceConnected: TestableObserver<Bool>!
//    var testStatusLabelText: TestableObserver<String>!
//    var testMediaPartVisible: TestableObserver<Bool>!
//    var testConnectivityButtonVisible: TestableObserver<Bool>!
//    var testConnectivityButtonLabelText: TestableObserver<String>!
//    var testConnectedCheckmarkVisible: TestableObserver<Bool>!
//    var testDeviceSetupTriggered: TestableObserver<OTARequestType>!
//    var testShowDeviceSettingsTriggered: TestableObserver<String>!
//    var testHideDeviceSettingsTriggered: TestableObserver<Bool>!
//    var testForcedOTAButtonVisible: TestableObserver<Bool>!
//    var testDeviceConnectingActivityIndicatorVisible: TestableObserver<Bool>!
//    var testUpdateAvailable: TestableObserver<Bool>!
//    
//    var jsonOTAService: OTAServiceType!
//    var deviceService: DeviceServiceType!
//    
//    var disposeBag: DisposeBag!
//    var devicesListVm: DevicesListViewModel!
//    var cellVm: HomeDeviceCellViewModel!
//    
//    let updateObservable = Observable<Bool>.just(false)
//    
//    var deviceAdapterService: DeviceAdapterServiceType!
//    
//    var testScheduler: TestScheduler!
//    
//    var testDevice: DeviceType!
//    
//    
//    override func setUp() {
//        
//        super.setUp()
//        
//        disposeBag = DisposeBag()
//        
//        deviceAdapterService = DeviceAdapterService.shared
//        let jsonAdapter = JSONMockDeviceAdapter()
//        
//        deviceService = DeviceService(deviceInfoSource: DeviceListChanges(
//            new: deviceAdapterService.outputs.addedDevice,
//            updated: deviceAdapterService.outputs.updatedDevice,
//            removed: deviceAdapterService.outputs.removedDevice))
//        
//        deviceAdapterService.inputs.append(adapter: jsonAdapter, mocked: true)
//        deviceAdapterService.inputs.start()
//        
//        jsonAdapter.adapterState.accept(.ready)
//        let jsonAppMode = PublishRelay<ApplicationProcessMode>()
//        
//        jsonOTAService = OTAService(devicesList: deviceService.outputs.devices, deviceService: deviceService, processMode: jsonAppMode)
//        
//        let updateAvailableInfo = jsonOTAService.outputs.updateAvailableInfo
//            .flatMap { [weak self] info -> Observable<Bool> in
//                guard let strongSelf = self else { return Observable.just(false) }
//                guard info.isEmpty else {
//                    let available = info.contains(where: { $0.deviceID == strongSelf.testDevice.id })
//                    return Observable.just(available)
//                }
//                return Observable.just(false)
//            }
//        
//        let dependency = AppDependency(deviceService: deviceService,
//                                       otaService: jsonOTAService,
//                                       deviceAdapterService: deviceAdapterService,
//                                       appMode: BehaviorRelay<AppMode>.init(value: .foreground),
//                                       analyticsService: MockedAnalyticsService(),
//                                       newsletterService: NewsletterService())
//        
//        let asyncExpectation = expectation(description: "Waiting for mocked devices list")
//        _ = Observable<Int>
//            .timer(RxTimeInterval(5), scheduler: MainScheduler.instance)
//            .subscribe(onNext: { _ in
//                asyncExpectation.fulfill()
//            }).disposed(by: disposeBag)
//        waitForExpectations(timeout: 10, handler: nil)
//        
//        devicesListVm = DevicesListViewModel(dependencies: dependency)
//        
//        self.testDevice = deviceService.outputs.devices.value.first(where: {
//            $0.id == "00:a2:c2:d5:d4:a3"
//        })!
//        self.cellVm = HomeDeviceCellViewModel(device: self.testDevice, updateInfo: updateObservable, dependencies: MockedAnalyticsServiceDependency())
//        
//        friendlyName = cellVm.outputs.friendlyName
//        statusLabelVisible = cellVm.outputs.statusLabelVisible
//        deviceConnected = cellVm.outputs.deviceConnected
//        statusLabelText = cellVm.outputs.statusLabelText
//        mediaPartVisible = cellVm.outputs.mediaPartVisible
//        connectivityButtonVisible = cellVm.outputs.connectivityButtonVisible
//        connectivityButtonLabelText = cellVm.outputs.connectivityButtonLabelText
//        connectedCheckmarkVisible = cellVm.outputs.connectedCheckmarkVisible
//        showDeviceSettingsTriggered = cellVm.outputs.showDeviceSettingsTriggered
//        hideDeviceSettingsTriggered = cellVm.outputs.hideDeviceSettingsTriggered
//        forcedOTAButtonVisible = cellVm.outputs.forcedOTAButtonVisible
//        deviceConnectingActivityIndicatorVisible = cellVm.outputs.deviceConnectingActivityIndicatorVisible
//        updateAvailable = cellVm.outputs.updateAvailable
//        
//        testScheduler = TestScheduler(initialClock: 0)
//        
//        testfriendlyName = testScheduler.createObserver(String.self)
//        testStatusLabelVisible = testScheduler.createObserver(Bool.self)
//        testDeviceConnected = testScheduler.createObserver(Bool.self)
//        testStatusLabelText = testScheduler.createObserver(String.self)
//        testMediaPartVisible = testScheduler.createObserver(Bool.self)
//        testConnectivityButtonVisible = testScheduler.createObserver(Bool.self)
//        testConnectivityButtonLabelText = testScheduler.createObserver(String.self)
//        testConnectedCheckmarkVisible = testScheduler.createObserver(Bool.self)
//        testDeviceSetupTriggered = testScheduler.createObserver(OTARequestType.self)
//        testShowDeviceSettingsTriggered = testScheduler.createObserver(String.self)
//        testHideDeviceSettingsTriggered = testScheduler.createObserver(Bool.self)
//        testUpdateAvailable = testScheduler.createObserver(Bool.self)
//        
//        friendlyName.asDriver().drive(testfriendlyName).disposed(by: disposeBag)
//        statusLabelVisible.asDriver().drive(testStatusLabelVisible).disposed(by: disposeBag)
//        deviceConnected.asDriver().drive(testDeviceConnected).disposed(by: disposeBag)
//        statusLabelText.asDriver().drive(testStatusLabelText).disposed(by: disposeBag)
//        mediaPartVisible.asDriver().drive(testMediaPartVisible).disposed(by: disposeBag)
//        connectivityButtonVisible.asDriver().drive(testConnectivityButtonVisible).disposed(by: disposeBag)
//        connectivityButtonLabelText.asDriver().drive(testConnectivityButtonLabelText).disposed(by: disposeBag)
//        connectedCheckmarkVisible.asDriver().drive(testConnectedCheckmarkVisible).disposed(by: disposeBag)
//        deviceSetupTriggered.subscribe(testDeviceSetupTriggered).disposed(by: disposeBag)
//        showDeviceSettingsTriggered.subscribe(testShowDeviceSettingsTriggered).disposed(by: disposeBag)
//        hideDeviceSettingsTriggered.subscribe(testHideDeviceSettingsTriggered).disposed(by: disposeBag)
//        updateAvailableInfo.subscribe(testUpdateAvailable).disposed(by: disposeBag)
//    }
//    
//    func testUnconfiguredDevice() {
//        AppEnvironment.pushEnvironment(Environment(language: .en))
//        testScheduler.start()
//        
//        XCTAssertEqual([.next(0,"JSON MOCK")], testfriendlyName.events)
//        XCTAssertEqual([.next(0,false)], testStatusLabelVisible.events)
//        XCTAssertEqual([.next(0,false)], testDeviceConnected.events)
//        XCTAssertEqual([.next(0,mockNotConnected)], testStatusLabelText.events)
//        XCTAssertEqual([.next(0,false)], testMediaPartVisible.events)
//        XCTAssertEqual([.next(0,false)], testConnectedCheckmarkVisible.events)
//        XCTAssertEqual([.next(0,false)], testUpdateAvailable.events)
//    }
//    
//    func testReconnectDevice() {
//        AppEnvironment.pushEnvironment(Environment(language: .en))
//        testScheduler.start()
//
//        // Device initially disconnected
//        XCTAssertEqual([.next(0,mockNotConnected)], testStatusLabelText.events)
//        XCTAssertEqual([.next(0,false)], testStatusLabelVisible.events)
//        XCTAssertEqual([.next(0,false)], testMediaPartVisible.events)
//        XCTAssertEqual([.next(0,false)], testConnectedCheckmarkVisible.events)
//
//        // Connecting device
//        connectDevice()
//
//        XCTAssertEqual([.next(0,mockNotConnected),.next(0,mockConnected)], testStatusLabelText.events)
//        XCTAssertEqual([.next(0,false), .next(0, true)], testStatusLabelVisible.events)
//        XCTAssertEqual([.next(0,false), .next(0,true)], testMediaPartVisible.events)
//        XCTAssertEqual([.next(0,false), .next(0,true)], testConnectedCheckmarkVisible.events)
//    }
//    
//    func testPlayingThenDisconnected() {
//        AppEnvironment.pushEnvironment(Environment(language: .en))
//        testScheduler.start()
//
//        connectDevice()
//        startPlaying()
//
//        XCTAssertEqual([.next(0,mockNotConnected), .next(0,mockConnected)], testStatusLabelText.events)
//        XCTAssertEqual([.next(0,false), .next(0,true)], testStatusLabelVisible.events)
//        XCTAssertEqual([.next(0,false), .next(0,true)], testMediaPartVisible.events)
//        XCTAssertEqual([.next(0,false), .next(0,true)], testConnectedCheckmarkVisible.events)
//
//        disconnect()
//
//        XCTAssertEqual([.next(0,mockNotConnected), .next(0,mockConnected), .next(0,mockNotConnected)], testStatusLabelText.events)
//        XCTAssertEqual([.next(0,false), .next(0,true), .next(0,true)], testStatusLabelVisible.events)
//        XCTAssertEqual([.next(0,false), .next(0,true), .next(0,false)], testMediaPartVisible.events)
//        XCTAssertEqual([.next(0,false), .next(0,true), .next(0,false)], testConnectedCheckmarkVisible.events)
//    }
//
//    func testUserTriggeredDeviceSettings() {
//        AppEnvironment.pushEnvironment(Environment(language: .en))
//        testScheduler.start()
//        
//        connectDevice()
//        XCTAssertEqual([], testShowDeviceSettingsTriggered.events)
//        cellVm.inputs.showSettings()
//        XCTAssertEqual([.next(0,testDevice.id)], testShowDeviceSettingsTriggered.events)
//    }
//    
//    func testUpdateIsAvailable() {
//        AppEnvironment.pushEnvironment(Environment(language: .en))
//        testScheduler.start()
//
//        connectDevice()
//        mockUpdateAvailable()
//
//        
//        XCTAssertEqual([.next(0, false),
//                        .next(0, true)], testUpdateAvailable.events)
//    }
//    
//    func testUpdateIsNotAvailable() {
//        AppEnvironment.pushEnvironment(Environment(language: .en))
//        testScheduler.start()
//        
//        connectDevice()
//        mockUpdateNotAvailable()
//        
//        XCTAssertEqual([.next(0, false),
//                        .next(0, false)], testUpdateAvailable.events)
//    }
//    
//    private func connectDevice() {
//        let connectExpectation = expectation(description: "Waiting for connection tick")
//        let connectDisposable = testDevice.state.outputs.connectivityStatus().stateObservable()
//            .skipOne()
//            .subscribe(onNext: { connectivityStatus in
//                connectExpectation.fulfill()
//            })
//        dump(testDevice.id)
//        testDevice.state.inputs.set(connectivityStatus: .connected)
//        waitForExpectations(timeout: 3, handler: nil)
//        connectDisposable.dispose()
//    }
//    
//    private func startPlaying() {
//        let playingExpectation = expectation(description: "Start playing expectation")
//        let playDisposable = testDevice.state.outputs.playbackStatus().stateObservable()
//            .asDriver()
//            .skipOne()
//            .drive(onNext: { playingInfo in
//                playingExpectation.fulfill()
//            })
//        
//        testDevice.state.inputs.requestPlaybackStatus()
//        waitForExpectations(timeout: 3, handler: nil)
//        playDisposable.dispose()
//    }
//    
//    private func stopPlaying() {
//        let stopExpectation = expectation(description: "Stop playing expectation")
//
//        let stopDisposable = testDevice.state.outputs.playbackStatus().stateObservable()
//            .asDriver()
//            .skipOne()
//            .drive(onNext: { playingInfo in
//                stopExpectation.fulfill()
//            })
//
//        testDevice.state.inputs.requestPlaybackStatus()
//        waitForExpectations(timeout: 3, handler: nil)
//        stopDisposable.dispose()
//
//    }
//    
//    private func disconnect() {
//        let disconnectExpectation = expectation(description: "Waiting for disconnection tick")
//        let disconnectDisposable = testDevice.state.outputs.connectivityStatus().stateObservable()
//            .observeOn(MainScheduler.instance)
//            .map { $0.connected }
//            .skipOne()
//            .subscribe(onNext: { connected in
//                disconnectExpectation.fulfill()
//            })
//        testDevice.state.inputs.set(connectivityStatus: .disconnected)
//        testDevice.state.inputs.requestPlaybackStatus()
//        waitForExpectations(timeout: 3, handler: nil)
//        disconnectDisposable.dispose()
//    }
//    
//    private func mockUpdateAvailable() {
//        jsonOTAService.outputs.updateAvailableInfo.accept([UpdateAvailableInfo(deviceID: "00:a2:c2:d5:d4:a3", forced: false)])
//    }
//    
//    private func mockUpdateNotAvailable() {
//        jsonOTAService.outputs.updateAvailableInfo.accept([])
//    }
//    
//    deinit {
//        print(#function, String(describing: self))
//    }
//    
//}
