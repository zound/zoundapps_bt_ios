//
//  AboutThisSpeakerViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 22.05.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift
import GUMA

/// Default label for notifying about info not available
/// Translations not enabled because this case scenario (cannot get data from speaker) is not designed yet
/// Unavailable info may be presented different way. Consider this behavior temporary

/// Input points of the model for About This Speaker view model
protocol AboutThisSpeakerViewModelInput {
    /// Fetch data from device
    func aboutViewDidAppear()
    func requestHardwareInfo()
    func startUpdate()
    func updateCompleted()
    var hadrwareType: HardwareType { get }
}

protocol AboutThisSpeakerViewModelOutput {
    /// Get device's Name
    var speakerName: BehaviorRelay<String> { get }
    /// Get device's Model name
    var speakerModel: BehaviorRelay<String> { get }
    /// Get device's Firmware version
    var speakerFw: BehaviorRelay<String> { get }
    
    var dataFetched: PublishSubject<Bool> { get }
    
    var active: PublishSubject<Bool> { get }
    var updateAvailable: Observable<Bool> { get }

    var setupProgress: Observable<SetupProgressInfo> { get }

    var updateTrigger: PublishRelay<Void> { get }
    var blockNavigation: PublishRelay<Bool> { get }

    var updateInProgress: Bool { get }
    var viewAppeared: PublishRelay<Void> { get }
}

/// Protocol to force input/outputs on view model
protocol AboutThisSpeakerViewModelType {
    /// Input points of the view model
    var inputs: AboutThisSpeakerViewModelInput { get }
    
    /// Output points of the view model
    var outputs: AboutThisSpeakerViewModelOutput { get }
}

class AboutThisSpeakerViewModel: AboutThisSpeakerViewModelOutput {
    
    // conforming protocol with outputs
    var speakerModel: BehaviorRelay<String> = BehaviorRelay<String>.init(value: "")
    var speakerFw: BehaviorRelay<String> = BehaviorRelay<String>.init(value: "")
    var speakerName: BehaviorRelay<String> = BehaviorRelay<String>.init(value: "")
    
    var dataFetched = PublishSubject<Bool>.init()
    var active = PublishSubject<Bool>.init()
    
    let updateAvailable: Observable<Bool>
    
    var setupProgress: Observable<SetupProgressInfo>
    
    var updateInProgress = false
    var viewAppeared = PublishRelay<Void>()

    private var disposeBag = DisposeBag()
    private var otaService: OTAServiceType
    private var connectivityDisposeBag = DisposeBag()
    
    var updateTrigger = PublishRelay<Void>()
    var blockNavigation = PublishRelay<Bool>()
    private var activeDevice: BehaviorRelay<DeviceType>
    
    init(activeDevice: BehaviorRelay<DeviceType>, otaService: OTAServiceType, progress: Observable<SetupProgressInfo>) {
        
        self.activeDevice = activeDevice
        self.otaService = otaService
        self.setupProgress = progress
        
        let deviceID = activeDevice.value.id
        
        self.updateAvailable = otaService.outputs.updateAvailableInfo
            .flatMap { updateAvailableInfo -> Observable<Bool> in
                guard updateAvailableInfo.contains(where: { $0.deviceID == deviceID }) else {
                    return Observable.just(false)
                }
                return Observable.just(true)
        }
        
        activeDevice
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] device in
                self?.setupDevice(device)
            })
            .disposed(by: disposeBag)
    }
    
    deinit {
        print(#function, String(describing: self))
    }
}

// MARK: - Inputs for About This Speaker's view model
extension AboutThisSpeakerViewModel: AboutThisSpeakerViewModelInput {
    func requestHardwareInfo() {
        activeDevice.value.state.inputs.requestHardwareInfo()
        activeDevice.value.state.inputs.refreshName()
    }
    func startUpdate() {
        updateInProgress = true
        updateTrigger.accept(())
        blockNavigation.accept(true)
        connectivityDisposeBag = DisposeBag()
    }
    func updateCompleted() {
        blockNavigation.accept(false)
        requestHardwareInfo()
    }
    func aboutViewDidAppear() {
        self.viewAppeared.accept(())
    }
    var hadrwareType: HardwareType {
        return activeDevice.value.hardwareType
    }
}

// MARK: - Inputs and Outputs for About This Speaker's view model
extension AboutThisSpeakerViewModel: AboutThisSpeakerViewModelType {
    var inputs: AboutThisSpeakerViewModelInput { return self }
    var outputs: AboutThisSpeakerViewModelOutput { return self }
}

private extension AboutThisSpeakerViewModel {
    func updateData(info: DeviceHardwareInfo?) {
        // check if received data is nil. Event single nil element is a "error" from UI point of view
        guard let model = info?.model, let fw = info?.firmwareVersion else {
            outputs.dataFetched.onNext(false)
            return
        }
        
        guard [model, fw].contains(where: { $0.isEmpty }) == false else {
            outputs.dataFetched.onNext(false)
            return
        }
        
        // check if non-nil received data contains empty strings. Empty strings are also "errors" from UI point of view
        zip([speakerModel, speakerFw], [model, fw]).forEach({ (output, infoString) in
            output?.accept(infoString)
        })
        
        outputs.dataFetched.onNext(true)
    }
    
    private func setupDevice(_ device: DeviceType) {
        device.state.outputs.connectivityStatus().stateObservable()
            .map { $0 == .connected }
            .bind(to: active)
            .disposed(by: connectivityDisposeBag)
        
        device.state.outputs.name().stateObservable()
            .bind(to: speakerName)
            .disposed(by: disposeBag)
        
        // set up the speaker's MAC, model name and FW version to display in Settings
        device.state.outputs.hardwareInfoState().stateObservable()
            .subscribeNext(weak: self, AboutThisSpeakerViewModel.updateData)
            .disposed(by: disposeBag)
    }
}
