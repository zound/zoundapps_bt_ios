//
//  AboutThisSpeakerViewModelTests.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Bartosz Dolewski on 23.05.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

fileprivate var mockName = "Initial Name"
fileprivate var mockModel = "STANMORE"
fileprivate var mockFw = "1234"

public final class DummyHwInfoDeviceState: DeviceStateType {
    
    public let perDeviceTypeFeatures: [Feature] = []
    
    public var inputs: DeviceStateInput { return self }
    public var outputs: DeviceStateOutput { return self }
    
    public static let supportedFeatures: [Feature] = [.hasConnectivityStatus, .hasDeviceInfo]
    
    fileprivate let disposeBag = DisposeBag()
    
    fileprivate var currentHw: DeviceHardwareInfo? = nil
    
    fileprivate let _name: AnyState<String>
    fileprivate let _hardwareInfoState: AnyState<DeviceHardwareInfo?>
    fileprivate let _connectivityStatus: AnyState<ConnectivityStatus>
    
    fileprivate let theGood: DeviceHardwareInfo? = DeviceHardwareInfo(model: "STANMORE X", firmwareVersion: "12345")
    fileprivate let theBad: DeviceHardwareInfo? = nil
    fileprivate let theUgly: DeviceHardwareInfo? = DeviceHardwareInfo(model: "", firmwareVersion: "")
    
    init() {
        self._name = AnyState<String>(feature: .hasCustomizableName,
                                      supportsNotifications: false,
                                      initialValue: { return mockName })
        
        self._connectivityStatus = AnyState<ConnectivityStatus>(feature: .hasConnectivityStatus,
                                                                supportsNotifications: false,
                                                                initialValue: { return .connected })
        
        self._hardwareInfoState = AnyState<DeviceHardwareInfo?>(feature: .hasDeviceInfo,
                                                               supportsNotifications: false,
                                                               initialValue: { return DeviceHardwareInfo(model: mockModel, firmwareVersion: mockFw) })
    
    
    }
    
    public func beGoodGuy() {
        currentHw = theGood
    }
    
    public func beBadGuy() {
        currentHw = theBad
    }
    
    public func beUglyGuy() {
        currentHw = theUgly
    }
}

extension DummyHwInfoDeviceState: DeviceStateInput {
    public func refreshName() {
        _name.stateObservable().accept(mockName)
    }
    
    public func requestHardwareInfo() {
        _hardwareInfoState.stateObservable().accept(currentHw)
    }
}

extension DummyHwInfoDeviceState: DeviceStateOutput {
    public var activeMonitors: Set<Monitor> {
        get { return  Set<Monitor>() }
        set(newValue) {}
    }
    
    public func name() -> AnyState<String> {
        return _name
    }
    
    public func connectivityStatus() -> AnyState<ConnectivityStatus> {
        return _connectivityStatus
    }
    
    public func hardwareInfoState() -> AnyState<DeviceHardwareInfo?> {
        return _hardwareInfoState
    }
}

fileprivate class AboutThisSpeakerDummyDevice: DeviceType {
    func bleConnect() -> Single<Void> {
        return Single.just(())
    }
    
    var mac: MAC?
    var configured = false
    
    var hardwareType: HardwareType = .speaker
    
    func decouple() -> Single<Void> {
        return Single.just(())
    }
    
    func resetSlave() -> Single<Void> {
        return Single.just(())
    }
    
    func initialSetup(with storedEntry: StoredDeviceInfoEntry?) -> Single<String> {
        return Single.just(String())
    }
    
    var modelName: String {
        return String()
    }

    var image: DeviceImageProviding = DeviceBlankImageProvider()
    
    
    var connectionTimeout: TimeInterval {
        return TimeInterval.seconds(5.0)
    }
    
    var id: String { return "TEST UUID" }
    var imageName: String { return "TEST_SPEAKER.png" }
    
    let platform = Platform(id: "joplinBT", traits: PlatformTraits())
    
    var state: DeviceStateType
    var otaAdapter: OTAAdapterType?

    init(name: BehaviorRelay<String>,
         state: DeviceStateType,
         otaAdapter: OTAAdapterType? = nil) {
        self.state = state
        self.otaAdapter = otaAdapter
    }
}

final class AboutThisSpeakerViewModelTests: XCTestCase {
    var disposeBag: DisposeBag!
    var vm: AboutThisSpeakerViewModel!
    
    var deviceAdapterService: DeviceAdapterServiceType!
    var deviceService: DeviceServiceType!
    var jsonOTAService: OTAServiceType!
    var activeConnectedDevice: BehaviorRelay<DeviceType>!
    var disconnectedDevice: BehaviorRelay<DeviceType>!
    var testProgressObservable = Observable<SetupProgressInfo>.just(.initializingConnection)

    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
    
        deviceAdapterService = DeviceAdapterService.shared
        let jsonAdapter = JSONMockDeviceAdapter()
        
        deviceService = DeviceService(deviceInfoSource: DeviceListChanges(
            new: deviceAdapterService.outputs.addedDevice,
            updated: deviceAdapterService.outputs.updatedDevice,
            removed: deviceAdapterService.outputs.removedDevice))
        
        deviceAdapterService.inputs.append(adapter: jsonAdapter, mocked: true)
        deviceAdapterService.inputs.start()
        
        jsonAdapter.adapterState.accept(.ready)
        let jsonAppMode = PublishRelay<ApplicationProcessMode>()
        
        jsonOTAService = OTAService(deviceAdapterService: deviceAdapterService, processMode: jsonAppMode)
    }
    
    func setUpGoodDevice() {
        let goodDeviceState = DummyHwInfoDeviceState()
        goodDeviceState.beGoodGuy()
        let goodDevice = AboutThisSpeakerDummyDevice(name: BehaviorRelay.init(value: "GOODTEST"),
                                                     state: goodDeviceState,
                                                     otaAdapter: nil)
        let goodDeviceRelay = BehaviorRelay<DeviceType>.init(value: goodDevice)
        vm = AboutThisSpeakerViewModel(activeDevice: goodDeviceRelay, otaService: jsonOTAService, progress: testProgressObservable)
    }
    
    func setUpBadDevice() {
        let badDeviceState = DummyHwInfoDeviceState()
        badDeviceState.beBadGuy()
        let badDevice = AboutThisSpeakerDummyDevice(name: BehaviorRelay.init(value: "BADTEST"),
                                                    state: badDeviceState,
                                                    otaAdapter: nil)
        let badDeviceRelay = BehaviorRelay<DeviceType>.init(value: badDevice)
        vm = AboutThisSpeakerViewModel(activeDevice: badDeviceRelay, otaService: jsonOTAService, progress: testProgressObservable)
    }
    
    func setUpUglyDevice() {
        let uglyDeviceState = DummyHwInfoDeviceState()
        uglyDeviceState.beUglyGuy()
        let uglyDevice = AboutThisSpeakerDummyDevice(name: BehaviorRelay.init(value: "UGLYTEST"),
                                                     state: uglyDeviceState,
                                                     otaAdapter: nil)
        let uglyDeviceRelay = BehaviorRelay<DeviceType>.init(value: uglyDevice)
        vm = AboutThisSpeakerViewModel(activeDevice: uglyDeviceRelay, otaService: jsonOTAService, progress: testProgressObservable)
    }
    
    func testHardwareInfoName() {
        setUpGoodDevice()
        let scheduler = TestScheduler(initialClock: TestTime())
        
        let name = scheduler.createObserver(String.self)
        let dataFetched = scheduler.createObserver(Bool.self)
        
        vm.outputs.speakerName.asObservable().subscribe(name).disposed(by: disposeBag)
        vm.outputs.dataFetched.asObservable().subscribe(dataFetched).disposed(by: disposeBag)
        
        // initial values
        XCTAssertEqual([.next(TestTime(), mockName)], name.events)
        
        // request values (actuall behavior in view model - requested by view controller)
        vm.inputs.requestHardwareInfo()
        XCTAssertEqual([.next(TestTime(), mockName),
                        .next(TestTime(), mockName)], name.events)
        
        XCTAssertEqual([.next(TestTime(), true)], dataFetched.events)
    }
    
    func testHardwareInfoModel() {
        setUpGoodDevice()
        let scheduler = TestScheduler(initialClock: TestTime())
        
        let model = scheduler.createObserver(String.self)
        let dataFetched = scheduler.createObserver(Bool.self)
        
        vm.outputs.speakerModel.asObservable().subscribe(model).disposed(by: disposeBag)
        vm.outputs.dataFetched.asObservable().subscribe(dataFetched).disposed(by: disposeBag)
        
        // initial values
        XCTAssertEqual([.next(TestTime(), mockModel)], model.events)
        
        // request values (actuall behavior in view model - requested by view controller)
        vm.inputs.requestHardwareInfo()
        XCTAssertEqual([.next(TestTime(), mockModel),
                        .next(TestTime(), "STANMORE X")], model.events)
        
        XCTAssertEqual([.next(TestTime(), true)], dataFetched.events)
    }
    
    func testHardwareInfoModelNil() {
        setUpBadDevice()
        let scheduler = TestScheduler(initialClock: TestTime())
        
        let model = scheduler.createObserver(String.self)
        let dataFetched = scheduler.createObserver(Bool.self)
        
        vm.outputs.speakerModel.asObservable().subscribe(model).disposed(by: disposeBag)
        vm.outputs.dataFetched.asObservable().subscribe(dataFetched).disposed(by: disposeBag)
        
        // initial values
        XCTAssertEqual([.next(TestTime(), mockModel)], model.events)
        
        // request values (actuall behavior in view model - requested by view controller)
        vm.inputs.requestHardwareInfo()
        XCTAssertEqual([.next(TestTime(), mockModel)], model.events)
        
        XCTAssertEqual([.next(TestTime(), false)], dataFetched.events)
    }
    
    func testHardwareInfoModelEmpty() {
        setUpUglyDevice()
        let scheduler = TestScheduler(initialClock: TestTime())
        
        let model = scheduler.createObserver(String.self)
        let dataFetched = scheduler.createObserver(Bool.self)
        
        vm.outputs.speakerModel.asObservable().subscribe(model).disposed(by: disposeBag)
        vm.outputs.dataFetched.asObservable().subscribe(dataFetched).disposed(by: disposeBag)
        
        // initial values
        XCTAssertEqual([.next(TestTime(), mockModel)], model.events)
        
        // request values (actuall behavior in view model - requested by view controller)
        vm.inputs.requestHardwareInfo()
        XCTAssertEqual([.next(TestTime(), mockModel)], model.events)
        
        XCTAssertEqual([.next(TestTime(), false)], dataFetched.events)
    }
    
    func testHardwareInfoFirwmare() {
        setUpGoodDevice()
        let scheduler = TestScheduler(initialClock: TestTime())
        
        let fw = scheduler.createObserver(String.self)
        let dataFetched = scheduler.createObserver(Bool.self)
        
        vm.outputs.speakerFw.asObservable().subscribe(fw).disposed(by: disposeBag)
        vm.outputs.dataFetched.asObservable().subscribe(dataFetched).disposed(by: disposeBag)
        
        // initial values
        XCTAssertEqual([.next(TestTime(), mockFw)], fw.events)
        
        // request values (actuall behavior in view model - requested by view controller)
        vm.inputs.requestHardwareInfo()
        XCTAssertEqual([.next(TestTime(), mockFw),
                        .next(TestTime(), "12345")], fw.events)
        
        XCTAssertEqual([.next(TestTime(), true)], dataFetched.events)
    }
    
    func testHardwareInfoFirwmareNil() {
        setUpBadDevice()
        let scheduler = TestScheduler(initialClock: TestTime())
        
        let fw = scheduler.createObserver(String.self)
        let dataFetched = scheduler.createObserver(Bool.self)
        
        vm.outputs.speakerFw.asObservable().subscribe(fw).disposed(by: disposeBag)
        vm.outputs.dataFetched.asObservable().subscribe(dataFetched).disposed(by: disposeBag)
        
        // initial values
        XCTAssertEqual([.next(TestTime(), mockFw)], fw.events)
        
        // request values (actuall behavior in view model - requested by view controller)
        vm.inputs.requestHardwareInfo()
        XCTAssertEqual([.next(TestTime(), mockFw)], fw.events)
        
        XCTAssertEqual([.next(TestTime(), false)], dataFetched.events)
    }
    
    func testHardwareInfoFirwmareEmpty() {
        setUpUglyDevice()
        let scheduler = TestScheduler(initialClock: TestTime())
        
        let fw = scheduler.createObserver(String.self)
        let dataFetched = scheduler.createObserver(Bool.self)
        
        vm.outputs.speakerFw.asObservable().subscribe(fw).disposed(by: disposeBag)
        vm.outputs.dataFetched.asObservable().subscribe(dataFetched).disposed(by: disposeBag)
        
        // initial values
        XCTAssertEqual([.next(TestTime(), mockFw)], fw.events)
        
        // request values (actuall behavior in view model - requested by view controller)
        vm.inputs.requestHardwareInfo()
        XCTAssertEqual([.next(TestTime(), mockFw)], fw.events)
        
        XCTAssertEqual([.next(TestTime(), false)], dataFetched.events)
    }
    
    // ******* test extensions used in this view model & class **********
    func testDataHexExtension() {
        let testData = Data(bytes: [0, 18, 111, 247, 96, 165])
        let finalHex = String("00126FF760A5")
        
        XCTAssertEqual(testData.hexDescription.uppercased(), finalHex)
    }
    
    func testDataHexExtensionEmpty() {
        let testData = Data(bytes: [])
        let finalHex = ""
        
        XCTAssertEqual(testData.hexDescription.uppercased(), finalHex)
    }
    
    func testMacFormatter() {
        let mac = String("00126FF760A5")
        let final = String("00:12:6F:F7:60:A5")
        
        XCTAssertEqual(mac.formatToMAC(), final)
    }
    
    func testMacFormatterTooShort() {
        let mac = String("00126FF760A")
        let final = String("00:12:6F:F7:60:A")
        
        XCTAssertEqual(mac.formatToMAC(), final)
    }
    
    func testMacFormatterEmpty() {
        let mac = String("")
        let final = String("")
        
        XCTAssertEqual(mac.formatToMAC(), final)
    }
}
