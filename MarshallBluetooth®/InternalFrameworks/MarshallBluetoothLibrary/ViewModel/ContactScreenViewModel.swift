//
//  ContactScreenViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Paprota Przemyslaw on 16/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

/// Input points of the model for Device Settings
protocol ContactScreenViewModelInput {
    func close()
}

protocol ContactScreenViewModelOutput {
    var closed: BehaviorRelay<Void> { get set }
}

/// Protocol to force input/outputs on view model
protocol ContactScreenViewModelType {
    /// Input points of the view model
    var inputs: ContactScreenViewModelInput { get }
    /// Output points of the view model
    var outputs: ContactScreenViewModelOutput { get }
}

class ContactScreenViewModel: ContactScreenViewModelType, ContactScreenViewModelInput, ContactScreenViewModelOutput {
    
    var inputs: ContactScreenViewModelInput { return self }
    var outputs: ContactScreenViewModelOutput { return self }

    var closed = BehaviorRelay<Void>(value: ())

    func close() {
        closed.accept(())
    }
    
}
