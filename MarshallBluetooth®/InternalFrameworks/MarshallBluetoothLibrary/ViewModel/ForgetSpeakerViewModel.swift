//
//  ForgetSpeakerViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 17/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import GUMA
import RxSwift

struct SpeakerData {
    var name: String
    var image: MediumDeviceImageProviding
}

protocol ForgetSpeakerViewModelInput {
    func viewDidLoad()
    func didForgetSpeaker()
    var hardwareType: HardwareType { get }
}

protocol ForgetSpeakerViewModelOutput {
    var speakerForgotten: PublishRelay<Void> { get set }
    var speakerData: PublishRelay<SpeakerData> { get set }

}

protocol ForgetSpeakerViewModelType {
    var inputs: ForgetSpeakerViewModelInput { get }
    var outputs: ForgetSpeakerViewModelOutput { get }
}

class ForgetSpeakerViewModel: ForgetSpeakerViewModelInput, ForgetSpeakerViewModelOutput, ForgetSpeakerViewModelType {

    var speakerForgotten = PublishRelay<Void>()
    var speakerData = PublishRelay<SpeakerData>()

    var inputs: ForgetSpeakerViewModelInput { return self }
    var outputs: ForgetSpeakerViewModelOutput { return self }

    private let device: DeviceType
    private var disposeBag = DisposeBag()

    init(device: DeviceType) {
        self.device = device
    }

    deinit {
        print(#function, String(describing: self))
    }

}

extension ForgetSpeakerViewModel {

    func viewDidLoad() {
        speakerData.accept(SpeakerData(name: device.state.outputs.name().stateObservable().value, image: device.image))
    }

    func didForgetSpeaker() {
        device.state.inputs.disconnect()
        speakerForgotten.accept(())
    }

    var hardwareType: HardwareType {
        return device.hardwareType
    }
}
