//
//  CoupleSpeakerModeConfigurationViewModel.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 15/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

enum CoupleSpeakerStereoChannel {
    case left, right
}

protocol CoupleSpeakerModeConfigurationViewModelInput {
    func done()
    func setMasterAs(_ channel: CoupleSpeakerStereoChannel)
}

protocol CoupleSpeakerModeConfigurationViewModelOutput {
    var completed: PublishSubject<Void> { get }
}

protocol CoupleSpeakerModeConfigurationViewModelType {
    var inputs: CoupleSpeakerModeConfigurationViewModelInput { get }
    var outputs: CoupleSpeakerModeConfigurationViewModelOutput { get }
}

class CoupleSpeakerModeConfigurationViewModel: CoupleSpeakerModeConfigurationViewModelInput,
                                               CoupleSpeakerModeConfigurationViewModelOutput,
                                               CoupleSpeakerModeConfigurationViewModelType {
    typealias Dependencies = DeviceServiceDependency

    // CoupleSpeakerModeConfigurationViewModelOutput
    var completed = PublishSubject<Void>()

    var inputs: CoupleSpeakerModeConfigurationViewModelInput { return self }
    var outputs: CoupleSpeakerModeConfigurationViewModelOutput { return self }

    private let device: DeviceType
    private let slaveInfo: SlaveInfo
    private let coupleSpeakerMode: CoupleSpeakerMode
    private let dependencies: Dependencies
    private let preferences: DefaultStorage

    private var channel = CoupleSpeakerStereoChannel.left
    private var disposeBag = DisposeBag()

    init(device: DeviceType, slaveInfo: SlaveInfo, coupleSpeakerMode: CoupleSpeakerMode, dependencies: Dependencies, preferences: DefaultStorage = DefaultStorage.shared) {
        self.device = device
        self.slaveInfo = slaveInfo
        self.coupleSpeakerMode = coupleSpeakerMode
        self.dependencies = dependencies
        self.preferences = preferences

        device.state.outputs.connectivityStatus().stateObservable()
            .map { !$0.connected }
            .subscribe(onNext: { [weak self] disconnected in
                guard disconnected else { return }
                self?.completed.onCompleted()
            }).disposed(by: disposeBag)
    }

}

extension CoupleSpeakerModeConfigurationViewModel {
    func done() {
        device.state.inputs.write(trueWirelessConnectWith: slaveInfo.couplingId, completion: { [weak self] in
            guard let strongSelf = self else { return }
            let trueWirelessChannel = strongSelf.coupleSpeakerMode == .standard ? .party : strongSelf.channel.trueWirelessChannel
            strongSelf.device.state.inputs.write(trueWirelessChannel: trueWirelessChannel)
            strongSelf.preferences.couplePairs[strongSelf.device.id] = strongSelf.slaveInfo.id
            strongSelf.preferences.lastConfiguredCouplePairs[strongSelf.device.id] = strongSelf.slaveInfo.id
            strongSelf.completed.onCompleted()
        })
    }
    func setMasterAs(_ channel: CoupleSpeakerStereoChannel) {
        self.channel = channel
    }
}

private extension CoupleSpeakerStereoChannel {
    var trueWirelessChannel: WirelessStereoChannel {
        switch self {
        case .left:
            return .left
        case .right:
            return .right
        }
    }
}
