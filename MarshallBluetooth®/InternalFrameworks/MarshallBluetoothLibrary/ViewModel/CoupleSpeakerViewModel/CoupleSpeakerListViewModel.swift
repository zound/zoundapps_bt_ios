//
//  CoupleSpeakerListViewModel.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 12/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

private struct Config {
    static let deviceInteractionTimeout: RxTimeInterval = .seconds(2)
    struct Indice {
        static let master = 0
        static let slave = 1
    }
}

struct CoupleSpeakerListInit {
    let initialState: CoupleSpeakerListInitialState
    let deviceList: [CoupleSpeakerListItem]
}

protocol CoupleSpeakerListViewModelInput {
    func viewDidLoad()
    func viewWillDisappearMovingFromParent()
    func next(selectedItemIndex: Int)
    func unpair()
}

protocol CoupleSpeakerListViewModelOutput {
    var outputCoupleSpeakerListInit: PublishRelay<CoupleSpeakerListInit> { get }
    var outputCoupleSpeakerListItem: PublishRelay<CoupleSpeakerListItem> { get }
    var completed: PublishSubject<Void> { get }
    var aborted: PublishSubject<Void> { get }
    var selected: PublishSubject<SlaveInfo> { get }
    var unpaired: PublishSubject<Void> { get }
}

protocol CoupleSpeakerListViewModelType {
    var inputs: CoupleSpeakerListViewModelInput { get }
    var outputs: CoupleSpeakerListViewModelOutput { get }
}

typealias SlaveInfo = (id: String, couplingId: Data, name: String, image: UIImage)

class CoupleSpeakerListViewModel: CoupleSpeakerListViewModelInput,
                                  CoupleSpeakerListViewModelOutput,
                                  CoupleSpeakerListViewModelType,
                                  WirelessStereoDeviceFriendlyNameProviding,
                                  WirelessStereoDeviceImageProviding {

    typealias Dependencies = DeviceServiceDependency

    // CoupleSpeakerListViewModelOutput
    var outputCoupleSpeakerListInit = PublishRelay<CoupleSpeakerListInit>()
    var outputCoupleSpeakerListItem = PublishRelay<CoupleSpeakerListItem>()
    var completed = PublishSubject<Void>()
    var aborted = PublishSubject<Void>()
    var selected = PublishSubject<SlaveInfo>()
    var unpaired = PublishSubject<Void>()

    var inputs: CoupleSpeakerListViewModelInput { return self }
    var outputs: CoupleSpeakerListViewModelOutput { return self }

    private let device: DeviceType
    private let dependencies: Dependencies
    private let preferences: DefaultStorage
    private var disposeBag = DisposeBag()

    private var slavesInfo: [SlaveInfo] = []

    init(device: DeviceType, dependencies: Dependencies, preferences: DefaultStorage) {
        self.device = device
        self.dependencies = dependencies
        self.preferences = preferences

        device.state.outputs.connectivityStatus().stateObservable()
            .map { !$0.connected }
            .subscribe(onNext: { [weak self] disconnected in
                guard disconnected else { return }
                self?.completed.onCompleted()
            }).disposed(by: disposeBag)

        device.state.outputs.trueWirelessStatus().stateObservable()
            .skipOne()
            .takeOne()
            .subscribe(onNext: { [weak self] trueWirelessStatus in
                self?.handle(trueWirelessStatus: trueWirelessStatus)
            }).disposed(by: disposeBag)
    }

}

private extension CoupleSpeakerListViewModel {

    func handle(trueWirelessStatus: WirelessStereoStatus) {
        switch trueWirelessStatus {
        case .disconnected:
            presentDevicesList(trueWirelessStatus: trueWirelessStatus)
        case .connectedAsMaster:
            presentCoupledSpeakersAsMaster(trueWirelessStatus: trueWirelessStatus)
        case .connectedAsSlave:
            presentCoupledSpeakersAsSlave(trueWirelessStatus: trueWirelessStatus)
        }
    }

    func presentCoupledSpeakersAsMaster(trueWirelessStatus: WirelessStereoStatus) {
        let name = slaveDeviceFriendlyName(device: device, deviceService: dependencies.deviceService, preferences: preferences)
        let image = slaveDeviceImage(device: device, deviceService: dependencies.deviceService, preferences: preferences) ?? device.image.smallImage
        let deviceList = [
            CoupleSpeakerListItem(name: device.state.outputs.name().stateObservable().value, image: device.image.smallImage, modeConfiguration: trueWirelessStatus.modeConfiguration),
            CoupleSpeakerListItem(name: name, image: image, modeConfiguration: trueWirelessStatus.oppositeModeConfiguration),
        ]
        outputCoupleSpeakerListInit.accept(CoupleSpeakerListInit(initialState: .coupled, deviceList: deviceList))
    }

    func presentCoupledSpeakersAsSlave(trueWirelessStatus: WirelessStereoStatus) {
        let name = masterDeviceFriendlyName(device: device, deviceService: dependencies.deviceService, preferences: preferences)
        let image = masterDeviceImage(device: device, deviceService: dependencies.deviceService, preferences: preferences) ?? device.image.smallImage
        let deviceList = [
            CoupleSpeakerListItem(name: name, image: image, modeConfiguration: trueWirelessStatus.oppositeModeConfiguration),
            CoupleSpeakerListItem(name: device.state.outputs.name().stateObservable().value, image: device.image.smallImage, modeConfiguration: trueWirelessStatus.modeConfiguration),
        ]
        outputCoupleSpeakerListInit.accept(CoupleSpeakerListInit(initialState: .coupled, deviceList: deviceList))
    }

    func presentDevicesList(trueWirelessStatus: WirelessStereoStatus) {
        let deviceList = [CoupleSpeakerListItem(name: device.state.outputs.name().stateObservable().value, image: device.image.smallImage, modeConfiguration: trueWirelessStatus.modeConfiguration)]
        outputCoupleSpeakerListInit.accept(CoupleSpeakerListInit(initialState: .unselected, deviceList: deviceList))
        let slaves = dependencies.deviceService.outputs.devices.value.filter {
            let connectivityStatus = $0.state.outputs.connectivityStatus().stateObservable().value
            return $0.id != device.id && ([.readyToConnect, .connected, .disconnected] as [ConnectivityStatus]).contains(connectivityStatus)
        }
        handle(slaves: slaves)
    }

    func handle(slaves: [DeviceType]) {
        enum ConnectedEvent {
            case connected(Bool)
            case timeout
        }
        guard let slave = slaves.first else {
            return
        }
        slave.bleConnect()
            .map { ConnectedEvent.connected(true) }
            .catchErrorJustReturn(.timeout)
            .asObservable()
            .subscribe(onNext: { [weak self] connectedEvent in
                switch connectedEvent {
                case .connected(let connected) where connected:
                    self?.requestTrueWirelessStatusWithId(for: slave, remainingSlaves: Array(slaves.dropFirst()))
                default:
                    self?.handle(slaves: Array(slaves.dropFirst()))
                }
            }).disposed(by: disposeBag)
    }

    func requestTrueWirelessStatusWithId(for slave: DeviceType, remainingSlaves: [DeviceType]) {
        enum TrueWirelessStatusWithIdEvent {
            case event(WirelessStereoStatusWithId)
            case timeout
        }
        slave.state.outputs.trueWirelessStatusWithId().stateObservable()
            .observeOn(MainScheduler.instance)
            .skipOne()
            .takeOne()
            .map { trueWirelessStatusWithId in TrueWirelessStatusWithIdEvent.event(trueWirelessStatusWithId) }
            .timeout(Config.deviceInteractionTimeout, scheduler: MainScheduler.instance)
            .catchErrorJustReturn(.timeout)
            .asObservable()
            .subscribe(onNext: { [weak self] trueWirelessStatusWithIdEvent in
                switch trueWirelessStatusWithIdEvent {
                case .event(let trueWirelessStatusWithId):
                    let slaveName = slave.state.outputs.name().stateObservable().value
                    let slaveImage = slave.image.smallImage
                    let slaveModeConfiguration = trueWirelessStatusWithId.status.modeConfiguration
                    self?.slavesInfo.append((id: slave.id, couplingId: trueWirelessStatusWithId.id, name: slaveName, image: slaveImage))
                    self?.outputCoupleSpeakerListItem.accept(CoupleSpeakerListItem(name: slaveName, image: slaveImage, modeConfiguration: slaveModeConfiguration))
                    fallthrough
                case .timeout:
                    self?.handle(slaves: remainingSlaves)
                }
            }).disposed(by: disposeBag)
        slave.state.inputs.requestTrueWirelessStatusWithId()
    }
}

extension CoupleSpeakerListViewModel {
    func viewDidLoad() {
        device.state.inputs.requestTrueWirelessStatus()
    }
    func viewWillDisappearMovingFromParent() {
        aborted.onCompleted()
    }
    func next(selectedItemIndex: Int) {
        guard let slaveInfo = slavesInfo[optional: selectedItemIndex - 1] else {
            return
        }
        self.selected.onNext(slaveInfo)
    }
    func unpair() {
        device.state.inputs.disconnectTrueWireless()
        preferences.couplePairs.removeValue(forKey: device.id)
        unpaired.onCompleted()
    }
}

private extension WirelessStereoStatus {
    var modeConfiguration: CoupleSpeakerModeConfiguration {
        switch self {
        case .connectedAsMaster(.left, oppositeDeviceMACAddress: _), .connectedAsSlave(.left, oppositeDeviceMACAddress: _):
            return .stereo(.left)
        case .connectedAsMaster(.right, oppositeDeviceMACAddress: _), .connectedAsSlave(.right, oppositeDeviceMACAddress: _):
            return .stereo(.right)
        default:
            return .standard
        }
    }
    var oppositeModeConfiguration: CoupleSpeakerModeConfiguration {
        switch modeConfiguration {
        case .standard:
            return .standard
        case .stereo(.left):
            return .stereo(.right)
        case .stereo(.right):
            return .stereo(.left)
        }
    }
}
