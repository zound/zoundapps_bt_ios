//
//  CoupleSpeakerHeadsUpViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 17/09/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import GUMA
import RxSwift

protocol CoupleSpeakerHeadsUpViewModelInput {
    func done()
}

protocol CoupleSpeakerHeadsUpViewModelOutput {
    var completed: PublishSubject<Void> { get }
    var pairNames: (masterName: String, slaveName: String) { get }
}

protocol CoupleSpeakerHeadsUpViewModelType {
    var inputs: CoupleSpeakerHeadsUpViewModelInput { get }
    var outputs: CoupleSpeakerHeadsUpViewModelOutput { get }
}

private struct Config {
    static let disconnectTimeout = TimeInterval.seconds(8.0)
    static let coupledMembers = 2
    static let masterIdx = 0
    static let slaveIdx = 1
}

class CoupleSpeakerHeadsUpViewModel: CoupleSpeakerHeadsUpViewModelInput,
                                     CoupleSpeakerHeadsUpViewModelOutput,
                                     CoupleSpeakerHeadsUpViewModelType {

    typealias Dependencies = DeviceServiceDependency

    // CoupleSpeakerHeadsUpViewModelOutput
    var completed = PublishSubject<Void>()
    var pairNames: (masterName: String, slaveName: String) {
        guard let couplePair = preferences.lastConfiguredCouplePairs.first,
            let masterName = dependencies.deviceService.outputs.devices.value.first(where: { $0.id == couplePair.key })?.state.outputs.name().stateObservable().value,
            let slaveName = dependencies.deviceService.outputs.devices.value.first(where: { $0.id == couplePair.value })?.state.outputs.name().stateObservable().value else {
                return (masterName: String(), slaveName: String())
        }
        return (masterName: masterName, slaveName: slaveName)
    }

    var inputs: CoupleSpeakerHeadsUpViewModelInput { return self }
    var outputs: CoupleSpeakerHeadsUpViewModelOutput { return self }

    private let dependencies: Dependencies
    private let preferences: DefaultStorage
    private var disconnectCoupledPairDisposeBag = DisposeBag()
    private var disconnectActionDisposeBag = DisposeBag()
    
    init(dependencies: Dependencies, preferences: DefaultStorage = DefaultStorage.shared) {
        self.dependencies = dependencies
        self.preferences = preferences
    }
}

private extension CoupleSpeakerHeadsUpViewModel {
    
    func indicateCoupledDevices() -> Single<Void> {
       
        return Single.create(subscribe: { [weak self] singleEvent in
            guard let strongSelf = self else {
                singleEvent(.success(()))
                return Disposables.create()
            }
            
            let coupledMembers = strongSelf.groupMembers()
            
            guard coupledMembers.count == Config.coupledMembers else {
                singleEvent(.success(()))
                return Disposables.create()
            }
            let master = coupledMembers[Config.masterIdx]
            let slave = coupledMembers[Config.slaveIdx]
            
            guard let masterMAC = master.mac, let slaveMAC = slave.mac else {
                singleEvent(.success(()))
                return Disposables.create()
            }
            
            master.state.inputs.set(connectivityStatus: .wsMaster(peerMAC: slaveMAC))
            slave.state.inputs.set(connectivityStatus: .wsSlave(peerMAC: masterMAC))
            
            singleEvent(.success(()))
            return Disposables.create()
        })
    }
    
    func disconnectAction(for device: DeviceType) -> Single<String> {
        return Single<String>.create(subscribe: { singleEvent in
            
            var disconnectDisposeBag = DisposeBag()
            
            device.state.inputs.set(connectivityStatus: .connecting)
            device.state.outputs.connectivityStatus().stateObservable().asObservable()
                .skipWhile { $0 != .disconnected }
                .timeout(Config.disconnectTimeout, scheduler: MainScheduler.instance)
                .take(1)
                .materialize()
                .observeOn(MainScheduler.instance)
                .subscribe(
                    onNext: { [unowned device] _ in
                        singleEvent(.success(device.id))
                        disconnectDisposeBag = DisposeBag()
                    }
                ).disposed(by: disconnectDisposeBag)
            
            device.state.inputs.disconnect()
            
            return Disposables.create()
        })

    }
    
    func groupMembers() -> [DeviceType] {
        
        guard let couplePair = self.preferences.lastConfiguredCouplePairs.first else {
            return []
        }
        let coupledMembers = [couplePair.key, couplePair.value]
            .map { [weak self] memberId in
                self?.dependencies.deviceService.outputs.devices.value.first(where: { $0.id == memberId })
            }
            .compactMap { $0 }
        guard coupledMembers.isEmpty == false else {
            return []
        }
        return coupledMembers
    
    }
    
    func disconnectSpeakersFromCouplePair() -> Single<Void> {
        
        return Single.create(subscribe: { [weak self] singleEvent in

            self?.disconnectCoupledPairDisposeBag = DisposeBag()

            guard let strongSelf = self else {
                singleEvent(.success(()))
                return Disposables.create()
            }

            let coupledMembers = strongSelf.groupMembers()
            
            guard coupledMembers.count == Config.coupledMembers else {
                singleEvent(.success(()))
                return Disposables.create()
            }
            
            let disconnectActions = coupledMembers
                .map { [weak self] device in
                    self?.disconnectAction(for: device).asObservable().materialize()
                }
                .compactMap { $0 }
            
            Observable.concat(disconnectActions)
                .observeOn(MainScheduler.instance)
                .subscribe(
                    onDisposed: {
                        singleEvent(.success(()))
                })
                .disposed(by: strongSelf.disconnectCoupledPairDisposeBag)
            
            return Disposables.create()
        })
    }
}

extension CoupleSpeakerHeadsUpViewModel {
    func done() {
        disconnectActionDisposeBag = DisposeBag()
        disconnectSpeakersFromCouplePair()
            .flatMap { [weak self] _ -> Single<Void> in
                guard let strongSelf = self else {
                    return Single.just(())
                }
                return strongSelf.indicateCoupledDevices()
            }
            .observeOn(MainScheduler.instance)
            .subscribe(
                onSuccess: { [weak self] _ in
                    self?.completed.onCompleted()
                }
            )
            .disposed(by: disconnectActionDisposeBag)
        
    }
}
