//
//  CoupleSpeakerModeSelectionViewModel.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 14/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

protocol CoupleSpeakerModeSelectionViewModelInput {
    func viewWillDisappearMovingFromParent()
    func next(mode: CoupleSpeakerMode)
}

protocol CoupleSpeakerModeSelectionViewModelOutput {
    var completed: PublishSubject<Void> { get }
    var modeSelected: PublishSubject<CoupleSpeakerMode> { get }
}

protocol CoupleSpeakerModeSelectionViewModelType {
    var inputs: CoupleSpeakerModeSelectionViewModelInput { get }
    var outputs: CoupleSpeakerModeSelectionViewModelOutput { get }
}

class CoupleSpeakerModeSelectionViewModel: CoupleSpeakerModeSelectionViewModelInput,
                                           CoupleSpeakerModeSelectionViewModelOutput,
                                           CoupleSpeakerModeSelectionViewModelType {

    typealias Dependencies = AnalyticsServiceDependency

    // CoupleSpeakerModeSelectionViewModelOutput
    var completed = PublishSubject<Void>()
    var modeSelected = PublishSubject<CoupleSpeakerMode>()

    var inputs: CoupleSpeakerModeSelectionViewModelInput { return self }
    var outputs: CoupleSpeakerModeSelectionViewModelOutput { return self }

    private let device: DeviceType
    private let dependencies: Dependencies
    private var disposeBag = DisposeBag()

    init(device: DeviceType, dependencies: Dependencies) {
        self.device = device
        self.dependencies = dependencies

        device.state.outputs.connectivityStatus().stateObservable()
            .map { !$0.connected }
            .subscribe(onNext: { [weak self] disconnected in
                guard disconnected else { return }
                self?.completed.onCompleted()
            }).disposed(by: disposeBag)
    }

}

extension CoupleSpeakerModeSelectionViewModel {
    func viewWillDisappearMovingFromParent() {
        completed.onCompleted()
    }
    func next(mode: CoupleSpeakerMode) {
        device.state.inputs.write(trueWirelessChannel: mode.defaultCoupleSpeakerModeConfiguration.trueWirelessChannel)
        switch mode.defaultCoupleSpeakerModeConfiguration {
        case .standard:
            dependencies.analyticsService.inputs.log(.appCouplingSwitchedAmbient)
        case .stereo:
            dependencies.analyticsService.inputs.log(.appCouplingSwitchedStereo)
        }
        modeSelected.onNext(mode)
    }

}

private extension CoupleSpeakerModeConfiguration {
    var trueWirelessChannel: WirelessStereoChannel {
        switch self {
        case .standard:
            return .party
        case .stereo(.left):
            return .left
        case .stereo(.right):
            return .right
        }
    }
}
