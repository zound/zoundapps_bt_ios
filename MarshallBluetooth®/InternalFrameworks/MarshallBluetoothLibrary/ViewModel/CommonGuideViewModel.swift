//
//  CommonGuideViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Paprota Przemyslaw on 17/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

/// Input points of the model for Device Settings
protocol CommonGuideViewModelInput {
    /// Notify model that screen is closed
    func close()
}

/// Output points of the model for Device Settings
protocol CommonGuideViewModelOutput {
    /// Indicates that screen is closed
    var closed: BehaviorRelay<Void> { get set }
}

/// Protocol to force input/outputs on view model
protocol CommonGuideViewModelType {
    /// Input points of the view model
    var inputs: CommonGuideViewModelInput { get }
    
    /// Output points of the view model
    var outputs: CommonGuideViewModelOutput { get }
}

class CommonGuideViewModel: CommonGuideViewModelInput, CommonGuideViewModelOutput, CommonGuideViewModelType {
    
    var inputs: CommonGuideViewModelInput { return self }
    
    var outputs: CommonGuideViewModelOutput { return self }
    
    var closed = BehaviorRelay<Void>(value: ())

    func close() {
        closed.accept(())
    }
    
}
