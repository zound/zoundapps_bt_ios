//
//  AutoOffTimerViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 11/12/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

protocol AutoOffTimerViewModelInput {
    func viewDidLoad()
    func viewDidAppear()
    func viewWillDisappear()
    var inputAutoOffTime: PublishRelay<AutoOffTime> { get set }
}

protocol AutoOffTimerViewModelOutput {
    var outputAutoOffTimeInit: PublishRelay<AutoOffTime> { get set }
    var outputAutoOffTime: PublishRelay<AutoOffTime> { get set }
    var active: BehaviorRelay<Bool> { get set }
}

protocol AutoOffTimerViewModelType {
    var inputs: AutoOffTimerViewModelInput { get }
    var outputs: AutoOffTimerViewModelOutput { get }
}

class AutoOffTimerViewModel: AutoOffTimerViewModelInput, AutoOffTimerViewModelOutput, AutoOffTimerViewModelType {
    typealias Dependencies = AnalyticsServiceDependency & AppModeDependency

    var active: BehaviorRelay<Bool> = BehaviorRelay(value: true)

    // AutoOffTimerViewModelOutput
    var outputAutoOffTimeInit = PublishRelay<AutoOffTime>()
    var outputAutoOffTime = PublishRelay<AutoOffTime>()

    // AutoOffTimerViewModelInput
    var inputAutoOffTime = PublishRelay<AutoOffTime>()

    // AutoOffTimerViewModelType
    var inputs: AutoOffTimerViewModelInput { return self }
    var outputs: AutoOffTimerViewModelOutput { return self }

    private let device: DeviceType
    private var dependencies: Dependencies
    private var disposeBag = DisposeBag()

    init(device: DeviceType, dependencies: Dependencies) {
        self.device = device
        self.dependencies = dependencies

        device.state.outputs.connectivityStatus().stateObservable()
            .asDriver()
            .map { $0.connected }
            .drive(active)
            .disposed(by: disposeBag)

        let autoOffTime = device.state.outputs.autoOffTime().stateObservable().skipOne()
        autoOffTime.takeOne().subscribe(onNext: { [weak self] autoOffTime in
            self?.outputAutoOffTimeInit.accept(autoOffTime)
        }).disposed(by: disposeBag)
        autoOffTime.skipOne().subscribe(onNext: { [weak self] autoOffTime in
            self?.outputAutoOffTime.accept(autoOffTime)
        }).disposed(by: disposeBag)

        let monitoredAutoOffTime = device.state.outputs.monitoredAutoOffTime().stateObservable().skipOne()
        monitoredAutoOffTime.subscribe(onNext: { [weak self] autoOffTime in
            self?.outputAutoOffTime.accept(autoOffTime)
        }).disposed(by: disposeBag)

        inputAutoOffTime.asObservable().observeOn(MainScheduler.instance).subscribe({ [weak self] inputAutoOffTime in
            guard let autoOffTime = inputAutoOffTime.element else { return }
            self?.device.state.inputs.write(autoOffTime: autoOffTime)
        }).disposed(by: disposeBag)

        dependencies.appMode.skipOne().observeOn(MainScheduler.instance).subscribe({ [weak self] mode in
            guard let mode = mode.element else { return }
            switch mode {
            case .background:
                self?.device.state.inputs.stopMonitoringAutoOffTime()
            case .foreground:
                self?.device.state.inputs.requestAutoOffTime()
                self?.device.state.inputs.startMonitoringAutoOffTime()
            }
        }).disposed(by: disposeBag)
    }
    deinit {
        print(#function, String(describing: self))
    }
}

extension AutoOffTimerViewModel {
    func viewDidLoad() {
        device.state.inputs.requestAutoOffTime()
    }
    func viewDidAppear() {
        device.state.inputs.startMonitoringAutoOffTime()
    }
    func viewWillDisappear() {
        device.state.inputs.stopMonitoringAutoOffTime()
    }
}
