//
//  GenericGuideViewControllerViewModel.swift
//  MarshallBluetooth®
//
//  Created by Paprota Przemyslaw on 24/03/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

struct GenericGuideViewControllerViewModel {
    struct ListItem {
        static let defaultPoint = "•"
        let pointText: String
        let label: String
        init(label: String) {
            self.pointText = ListItem.defaultPoint
            self.label = label
        }
        init(no: Int, label: String) {
            self.pointText = String(no)
            self.label = label
        }
    }
    enum Image {
        case quick_guide_1
        case quick_guide_2
        case quick_guide_4
        case quick_guide_5
    }

    var title: String
    var image: GenericGuideViewControllerViewModel.Image
    var text: String?
    var list: [GenericGuideViewControllerViewModel.ListItem]

    init(title: String, image: GenericGuideViewControllerViewModel.Image, text: String?, list: [GenericGuideViewControllerViewModel.ListItem]) {
        self.title = title
        self.image = image
        self.text = text
        self.list = list
    }

    init?(type:NextScreenType) {
        switch type {
        case .bluetoothHeadphonesGuide:
            let items = [ListItem(no:1, label:Strings.quick_guide_ozzy_bluetooth_pairing_item_1()),
                         ListItem(no:2, label:Strings.quick_guide_ozzy_bluetooth_pairing_item_2()),
                         ListItem(no:3, label:Strings.quick_guide_ozzy_bluetooth_pairing_item_3()),
                         ListItem(no:4, label:Strings.quick_guide_ozzy_bluetooth_pairing_item_4()),]
            self.init(title: Strings.main_menu_item_bluetooth_pairing_uc(),
                      image: .quick_guide_5,
                      text: nil,
                      list:items)
        case .ancButtonGuide:
            let items = [ListItem(label:Strings.quick_guide_ozzy_anc_button_bullet_1()),
                         ListItem(label:Strings.quick_guide_ozzy_anc_button_bullet_2()),]
            self.init(title: Strings.main_menu_item_anc_button_uc(),
                      image: .quick_guide_1,
                      text: Strings.quick_guide_ozzy_anc_button_label(),
                      list:items)
        case .controlKnobGuide:
            let items = [ListItem(label:Strings.quick_guide_ozzy_item_control_knob_v2_bullet_1()),
                         ListItem(label:Strings.quick_guide_ozzy_item_control_knob_v2_bullet_2()),
                         ListItem(label:Strings.quick_guide_ozzy_item_control_knob_v2_bullet_3()),
                         ListItem(label:Strings.quick_guide_ozzy_item_control_knob_v2_bullet_4()),
                         ListItem(label:Strings.quick_guide_ozzy_item_control_knob_v2_bullet_5()),
                         ListItem(label:Strings.quick_guide_ozzy_item_control_knob_v2_bullet_6()),]
            self.init(title: Strings.main_menu_item_control_knob_uc(),
                      image: .quick_guide_4,
                      text: Strings.quick_guide_ozzy_item_control_knob_v2(),
                      list:items)
        case .mButtonGuide:
            self.init(title: Strings.main_menu_item_m_button_uc(),
                      image: .quick_guide_2,
                      text: Strings.quick_guide_ozzy_item_m_button(),
                      list:[])
        default:
            return nil
        }
    }

}

