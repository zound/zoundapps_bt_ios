//
//  HomeDeviceCellViewModel.swift
//  MarshallBluetoothLibrary
//

import RxSwift
import GUMA

public struct DeviceCellElementVisibility {
    var connectivityButton = false
    var connectedCheckmark = false
    var statusLabel = false
    var mediaPart = false
    var batteryBlock = false
    var decoupleButton = false
    var deviceConnectingActivityIndicator = false
    var auxMode = false
}
public class DeviceCellEntryData {
    var visibility = DeviceCellElementVisibility()
    var friendlyName = String()
    var deviceImage = UIImage()
    var connectivityButtonLabelText = String()
    var statusLabelText = String()
    var connectivityStatus = ConnectivityStatus.notConfigured
    var batteryStatus = BatteryStatus.unknown
    var monitoredBatteryStatus = BatteryStatus.unknown
    var updateAvailable = false
    var volume = 0
    var monitoredVolume = 0
    var trackInfo = TrackInfo()
    var playbackStatus = PlaybackStatus.unknown
    var connected = false
}
private enum VisibleElement: CaseIterable {
    case statusLabel
    case mediaPart
    case connectivityButton
    case decoupleButton
    case deviceConnectingActivityIndicator
    case connectedCheckmark
    case batteryBlock
    case auxBlock
}
extension VisibleElement {
    static func visibility(for status: ConnectivityStatus) -> [VisibleElement] {
        switch status {
            case .disconnected: return [.statusLabel]
            case .notConfigured: return [.connectivityButton]
            case .readyToConnect: return [.connectivityButton]
            case .connecting: return [.statusLabel, .deviceConnectingActivityIndicator]
            case .connected: return [.connectedCheckmark, .statusLabel, .batteryBlock]
            case .wsMaster: return [.decoupleButton, .statusLabel]
            case .wsSlave: return [.statusLabel]
            case .error: return []
        }
    }
    static func path(for element: VisibleElement) -> WritableKeyPath<DeviceCellEntryData, Bool> {
        switch element {
        case .batteryBlock: return \DeviceCellEntryData.visibility.batteryBlock
        case .statusLabel: return \DeviceCellEntryData.visibility.statusLabel
        case .mediaPart: return \DeviceCellEntryData.visibility.mediaPart
        case .connectivityButton: return \DeviceCellEntryData.visibility.connectivityButton
        case .decoupleButton: return \DeviceCellEntryData.visibility.decoupleButton
        case .deviceConnectingActivityIndicator: return \DeviceCellEntryData.visibility.deviceConnectingActivityIndicator
        case .connectedCheckmark: return \DeviceCellEntryData.visibility.connectedCheckmark
        case .auxBlock: return \DeviceCellEntryData.visibility.auxMode
        }
    }
}
protocol HomeDeviceCellViewModelInputs {
    func change(volume: Int)
    func requestVolume()
    func requestPlayback()
    func showPlayer()
    func showSettings()
    func connectDevice()
    func viewDidBecameActive()
    func viewWillResignActive()
    func decouple()
    func logVolumeChangeAnalyticsEvent(with value: Int)
    func buttonSegmentTouchedUpInside(for feature: Feature?)
    func setupMonitors(enable: Bool)
}
protocol HomeDeviceCellViewModelOutputs {
    var configureDevice: PublishRelay<Void> { get }
    var decoupleDevices: PublishRelay<Void> { get }
    var showPlayerTriggered: PublishRelay<String> { get }
    var showDeviceSettingsTriggered: PublishRelay<String> { get }
    var showDedicatedFeatureViewTriggered: PublishRelay<(String, Feature)> { get }
    var platformTraits: PlatformTraits { get }
    var hardwareType: HardwareType { get }
    var currentStatus: BehaviorRelay<DeviceCellEntryData> { get }
    var statusDisposable: Disposable { get set }
    var id: String { get }
    static func supportedMonitors(hardware: HardwareType) -> [Monitor]
}
protocol HomeDeviceCellViewModelType: class {
    var inputs: HomeDeviceCellViewModelInputs { get }
    var outputs: HomeDeviceCellViewModelOutputs { get }
}
final class HomeDeviceCellViewModel: HomeDeviceCellViewModelInputs, HomeDeviceCellViewModelOutputs, HomeDeviceCellViewModelType {
    static func supportedMonitors(hardware: HardwareType) -> [Monitor] {
        switch hardware {
        case .headset:
            return [.battery, .powerCableAttached, .volume, .audioSource]
        case .speaker:
            return [.volume, .audioSource]
        }
    }
    typealias Dependencies = AnalyticsServiceDependency
    var inputs: HomeDeviceCellViewModelInputs { return self }
    var outputs: HomeDeviceCellViewModelOutputs { return self }

    var configureDevice = PublishRelay<Void>()
    var decoupleDevices = PublishRelay<Void>()
    var showPlayerTriggered = PublishRelay<String>()
    var showDeviceSettingsTriggered = PublishRelay<String>()
    var hideDeviceSettingsTriggered = PublishRelay<Bool>()
    var showDedicatedFeatureViewTriggered = PublishRelay<(String, Feature)>()
    var volume = PublishRelay<Int>()
    var monitoredVolume = PublishRelay<Int>()
    var updateAvailable = BehaviorRelay<Bool>.init(value: false)
    var batteryBlockVisible = BehaviorRelay<Bool>.init(value: true)
    var batteryStatus = BehaviorRelay<BatteryStatus>.init(value: .unknown)
    var currentStatus = BehaviorRelay<DeviceCellEntryData>.init(value: DeviceCellEntryData())
    var platformTraits: PlatformTraits {
        return modelDevice.platform.traits
    }
    var hardwareType: HardwareType {
        return modelDevice.hardwareType
    }
    var statusDisposable = Disposables.create()
    var currentValue: DeviceCellEntryData
    var id = String()
    
    init(device: DeviceType, updateInfo: Observable<Bool>, dependencies: Dependencies) {
        self.modelDevice = device
        self.dependencies = dependencies
        self.currentValue = DeviceCellEntryData()
        self.id = device.id
        currentValue.deviceImage = modelDevice.image.smallImage
        initialSetup(entry: &currentValue, with: self.modelDevice)
        var statusObservables = [Observable<DeviceCellEntryData>]()
        let nameStatusObservable = nameObservable(device: device,
                                                  nameMapper: nameMapper(entry: currentValue, device: device))
        let connectivityStatusObservable = connectivityObservable(device: device,
                                                                  connectivityMapper: connectivityMapper(entry: currentValue, device: device))
        let batteryStatusObservable = batteryObservable(device: device,
                                                        batteryMapper: batteryMapper(entry: currentValue, device: device))
        let monitoredBatteryStatusObservable = monitoredBatteryObservable(device: device,
                                                                          monitoredBatteryMapper: monitoredBatteryMapper(entry: currentValue, device: device))
        let volumeStatusObservable = volumeObservable(device: device,
                                                     volumeMapper: volumeMapper(entry: currentValue, device: device))
        let monitoredVolumeStatusObservable = monitoredVolumeObservable(device: device,
                                                                        monitoredVolumeMapper: monitoredVolumeMapper(entry: currentValue, device: device))
        let playingStatusObservable = deviceIsPlayingObservable(device: device, base: currentValue)
        let auxObservable = auxModeObservable(device: device, auxModeMapper: auxModeMapper(entry: currentValue, device: device))
        let updateStatusObservable = updateInfo
            .map { updateAvailable -> DeviceCellEntryData in
                self.currentValue.updateAvailable = updateAvailable
                return self.currentValue
            }
        switch device.hardwareType {
        case .headset:
            [nameStatusObservable,
             connectivityStatusObservable,
             batteryStatusObservable,
             monitoredBatteryStatusObservable,
             volumeStatusObservable,
             monitoredVolumeStatusObservable,
             playingStatusObservable,
             updateStatusObservable,
             auxObservable]
                .forEach { statusObservables.append($0) }
        case .speaker:
            [nameStatusObservable,
             connectivityStatusObservable,
             volumeStatusObservable,
             monitoredVolumeStatusObservable,
             playingStatusObservable,
             updateStatusObservable]
                .forEach { statusObservables.append($0) }
        }
        statusDisposable = Observable.merge(statusObservables)
            .subscribe(
                onNext: { [weak self] updated in
                    self?.currentStatus.accept(updated)
                }
            )
    }
    func change(volume: Int) {
        let state = modelDevice.state
        guard type(of: state.outputs).supportedFeatures.contains(.hasContinuousVolume) else { return }
        state.inputs.write(volume: volume)
    }
    func requestVolume() {
        let state = modelDevice.state
        guard type(of: state.outputs).supportedFeatures.contains(.hasContinuousVolume) else { return }
        state.inputs.requestVolume()
    }
    func requestPlayback() {
        let state = modelDevice.state
        guard type(of: state.outputs).supportedFeatures.contains(.hasAudioPlayback) else { return }
        state.inputs.requestPlaybackStatus()
    }
    func connectDevice() {
       self.configureDevice.accept(())
    }
    func decouple() {
        self.decoupleDevices.accept(())
    }
    func logVolumeChangeAnalyticsEvent(with volume: Int) {
        dependencies.analyticsService.inputs.log(.appVolumeChanged(volume), associatedWith: modelDevice.modelName)
    }
    func buttonSegmentTouchedUpInside(for feature: Feature?) {
        guard let feature = feature else { return }
        showDedicatedFeatureViewTriggered.accept((modelDevice.id, feature))
    }
    func showPlayer() {
        showPlayerTriggered.accept(modelDevice.id)
    }
    func showSettings() {
        showDeviceSettingsTriggered.accept(modelDevice.id)
    }
    func setupMonitors(enable: Bool) {
        guard modelDevice.state.outputs.connectivityStatus().stateObservable().value == .connected else { return }
        guard enable == true else {
            MarshallBluetoothLibrary.setupMonitors(connected: false, device: modelDevice)
            return
        }
        MarshallBluetoothLibrary.setupMonitors(connected: true, device: modelDevice)
        modelDevice.state.inputs.requestBatteryStatus()
        modelDevice.state.inputs.requestVolume()
        modelDevice.state.inputs.requestCurrentAudioSource()
        modelDevice.state.inputs.requestPlaybackStatus()
    }

    func viewWillResignActive() { }

    func viewDidBecameActive() { }

    private weak var modelDevice: DeviceType!
    private let dependencies: Dependencies
    private var disposeBag = DisposeBag()
    private weak var updateInfo: Observable<Bool>!
}

private func initialSetup(entry: inout DeviceCellEntryData, with device: DeviceType) {
    _ = nameMapper(entry: entry, device: device)(device.state.outputs.name().stateObservable().value)
    _ = connectivityMapper(entry: entry, device: device)(device.state.outputs.connectivityStatus().stateObservable().value)
    let batteryInfo = BatteryInfo(level: device.state.outputs.batteryStatus().stateObservable().value,
                           powerCable: device.state.outputs.powerCableStatus().stateObservable().value)
    _ = batteryMapper(entry: entry, device: device)(batteryInfo)
    _ = volumeMapper(entry: entry, device: device)(device.state.outputs.volume().stateObservable().value)
    var isPlayingFlags = [Bool]()
    let playbackStatus = device.state.outputs.playbackStatus().stateObservable().value == .playing
    let bluetoothAudioSource = device.state.outputs.currentAudioSource().stateObservable().value == .bluetooth
    let deviceConnected = device.state.outputs.connectivityStatus().stateObservable().value == .connected
    if deviceConnected {
        setupMonitors(connected: true, device: device)
    }
    switch device.hardwareType {
    case .headset:
        [playbackStatus, bluetoothAudioSource, deviceConnected].forEach { isPlayingFlags.append($0) }
    case .speaker:
        [playbackStatus, deviceConnected].forEach { isPlayingFlags.append($0) }
    }
    _ = isPlayingMapper(entry: entry, device: device)
}
func nameMapper(entry: DeviceCellEntryData, device: DeviceType) -> (String) -> DeviceCellEntryData {
    return { name in
        entry.friendlyName = name
        return entry
    }
}
func connectivityMapper(entry: DeviceCellEntryData, device:DeviceType) -> (ConnectivityStatus) -> DeviceCellEntryData {
    return { status in
        var copy = entry
        setupVisibility(&copy, status)
        copy.connected = false
        setupBatteryIndication(status: status, device: device, entry: &copy)
        switch status {
        case .connecting:
            copy.statusLabelText = Strings.speaker_status_connecting()
        case .notConfigured,
             .readyToConnect:
            copy.connectivityButtonLabelText = Strings.appwide_connect_uc()
        case .connected:
            setupMonitors(connected: true, device: device)
            copy.connected = true
            copy.statusLabelText = Strings.speaker_status_connected()
            if device.state.outputs.perDeviceTypeFeatures.contains(.hasContinuousVolume) { device.state.inputs.requestVolume() }
            if device.state.outputs.perDeviceTypeFeatures.contains(.hasAudioPlayback) { device.state.inputs.requestPlaybackStatus() }
        case .disconnected:
            copy.statusLabelText = Strings.speaker_status_not_connected()
        case .wsMaster:
            copy.statusLabelText = Strings.speaker_status_coupled_uc()
        case .wsSlave:
            copy.statusLabelText = Strings.speaker_status_coupled_uc()
        default:
            print("🤔🤔 Unhandled case for 📻 \(device.state.outputs.name().stateObservable().value) connectivity state: \(status)")
        }
        copy.connectivityStatus = status
        return copy
    }
}
typealias BatteryInfo = (level: BatteryStatus, powerCable: PowerCableStatus)
func batteryMapper(entry: DeviceCellEntryData, device: DeviceType) -> (BatteryInfo) -> DeviceCellEntryData {
    return { batteryInfo in
        if case .connected =  batteryInfo.powerCable {
            entry.batteryStatus = .charging
            entry.visibility.batteryBlock = true
            return entry
        }
        guard case .unknown = batteryInfo.level else {
            entry.batteryStatus = batteryInfo.level
            entry.visibility.batteryBlock = true
            return entry
        }
        entry.visibility.batteryBlock = false
        return entry
    }
}
func monitoredBatteryMapper(entry: DeviceCellEntryData, device: DeviceType) -> (BatteryInfo) -> DeviceCellEntryData {
    return { batteryInfo in
        if case .connected =  batteryInfo.powerCable {
            entry.monitoredBatteryStatus = .charging
            entry.visibility.batteryBlock = true
            return entry
        }
        guard case .unknown = batteryInfo.level else {
            entry.monitoredBatteryStatus = batteryInfo.level
            entry.visibility.batteryBlock = true
            return entry
        }
        entry.visibility.batteryBlock = false
        return entry
    }
}
func volumeMapper(entry: DeviceCellEntryData, device: DeviceType) -> (Int) -> DeviceCellEntryData {
    return { volume in
        entry.volume = volume
        return entry
    }
}
func monitoredVolumeMapper(entry: DeviceCellEntryData, device: DeviceType) -> (Int) -> DeviceCellEntryData {
    return { volume in
        entry.monitoredVolume = volume
        return entry
    }
}
func isPlayingMapper(entry: DeviceCellEntryData, device: DeviceType) -> ([Bool]) -> DeviceCellEntryData {
    return { requiredFlags in
        guard requiredFlags.contains(false) else {
            entry.visibility.mediaPart = true
            return entry
        }
        entry.visibility.mediaPart = false
        return entry
    }
}
func auxModeMapper(entry: DeviceCellEntryData, device: DeviceType) -> (AudioSource) -> DeviceCellEntryData {
    return { source in
        let connectivityStatus = device.state.outputs.connectivityStatus().stateObservable().value
        entry.visibility.auxMode = source == .aux
        entry.visibility.connectedCheckmark = !(source == .aux) && connectivityStatus == .connected
        entry.visibility.statusLabel = !(source == .aux) && connectivityStatus == .connected
        return entry
    }
}
private func nameObservable(device: DeviceType,
                            nameMapper: @escaping (String) -> DeviceCellEntryData)
    -> Observable<DeviceCellEntryData> {
    return device.state.outputs.name().stateObservable()
        .distinctUntilChanged()
        .map(nameMapper)
        .asObservable()
}
private func connectivityObservable(device: DeviceType,
                                    connectivityMapper: @escaping (ConnectivityStatus) -> DeviceCellEntryData)
    -> Observable<DeviceCellEntryData> {
    return device.state.outputs.connectivityStatus().stateObservable()
        .distinctUntilChanged()
        .map(connectivityMapper)
}
private func setupVisibility(_ base: inout DeviceCellEntryData, _ status: ConnectivityStatus) -> Void {
    VisibleElement.allCases.forEach {
        guard VisibleElement.visibility(for: status).contains($0) else {
            base[keyPath: VisibleElement.path(for: $0)] = false
            return
        }
        base[keyPath: VisibleElement.path(for: $0)] = true
    }
}
private func setupMonitors(connected: Bool, device: DeviceType) {
    let monitors = HomeDeviceCellViewModel.supportedMonitors(hardware: device.hardwareType)
    guard connected else {
        monitors.forEach { device.state.inputs.stop(monitorType: $0)() }
        return
    }
    monitors.forEach {
        device.state.inputs.stop(monitorType: $0)()
        device.state.inputs.start(monitorType: $0)()
    }
}
private func setupBatteryIndication(status: ConnectivityStatus, device: DeviceType, entry: inout DeviceCellEntryData) -> Void {
    guard device.state.outputs.perDeviceTypeFeatures.contains(.hasBatteryStatus) else {
        entry.visibility.batteryBlock = false
        return
    }
    switch status {
    case .connected:
        switch device.state.outputs.batteryStatus().stateObservable().value {
        case .unknown:
            entry.visibility.batteryBlock = false
        default:
            entry.visibility.batteryBlock = true
        }
        device.state.inputs.requestBatteryStatus()
    default:
        break
    }
}
private func batteryObservable(device: DeviceType,
                               batteryMapper: @escaping (BatteryInfo) -> DeviceCellEntryData)
    -> Observable<DeviceCellEntryData> {
    return Observable.combineLatest(
        device.state.outputs.batteryStatus().stateObservable().asObservable(),
        device.state.outputs.powerCableStatus().stateObservable().asObservable())
        .map(batteryMapper)
}
private func monitoredBatteryObservable(device: DeviceType,
                                        monitoredBatteryMapper: @escaping (BatteryInfo) -> DeviceCellEntryData)
    -> Observable<DeviceCellEntryData> {
    return Observable.combineLatest(
        device.state.outputs.monitoredBatteryStatus().stateObservable().asObservable(),
        device.state.outputs.monitoredPowerCableStatus().stateObservable().asObservable())
        .map(monitoredBatteryMapper)
}
private func volumeObservable(device: DeviceType,
                              volumeMapper: @escaping (Int) -> DeviceCellEntryData)
    -> Observable<DeviceCellEntryData> {
    return device.state.outputs.volume().stateObservable().asObservable()
        .map(volumeMapper)
}
private func monitoredVolumeObservable(device: DeviceType,
                                       monitoredVolumeMapper: @escaping (Int) -> DeviceCellEntryData)
    -> Observable<DeviceCellEntryData> {
    return device.state.outputs.monitoredVolume().stateObservable().asObservable()
        .map(monitoredVolumeMapper)
}
private func deviceIsPlayingObservable(device: DeviceType,
                                       base: DeviceCellEntryData) -> Observable<DeviceCellEntryData> {
    var deviceIsPlayingObservables = [Observable<Bool>]()
    let playbackStatus = device.state.outputs.playbackStatus().stateObservable()
        .map { $0 == .playing }
    let bluetoothAudioSource = device.state.outputs.currentAudioSource().stateObservable()
        .map { $0 == .bluetooth }
    let deviceConnected = device.state.outputs.connectivityStatus().stateObservable()
        .map { $0 == .connected }
    switch device.hardwareType {
    case .headset:
        [playbackStatus, bluetoothAudioSource, deviceConnected].forEach { deviceIsPlayingObservables.append($0) }
    case .speaker:
        [playbackStatus, deviceConnected].forEach { deviceIsPlayingObservables.append($0) }
    }
    return Observable.combineLatest(deviceIsPlayingObservables)
        .distinctUntilChanged()
        .map { playingFlags -> DeviceCellEntryData in
            guard playingFlags.contains(false) else {
                base.visibility.mediaPart = true
                return base
            }
            base.visibility.mediaPart = false
            return base
        }
}
private func auxModeObservable(device: DeviceType,
                               auxModeMapper: @escaping (AudioSource) -> DeviceCellEntryData)
    -> Observable<DeviceCellEntryData> {
    return device.state.outputs.currentAudioSource().stateObservable().asObservable()
        .map(auxModeMapper)
}
    
    




