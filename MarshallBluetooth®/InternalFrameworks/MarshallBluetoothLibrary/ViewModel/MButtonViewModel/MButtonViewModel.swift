//
//  MButtonViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 26/12/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

protocol MButtonViewModelInput {
    func viewDidLoad()
    var inputMButtonMode: PublishRelay<MButtonMode> { get set }
}

protocol MButtonViewModelOutput {
    var outputMButtonModeInit: PublishRelay<MButtonMode> { get set }
    var outputMButtonMode: PublishRelay<MButtonMode> { get set }
    var active: BehaviorRelay<Bool> { get set }
    var googleAssistantScreenRequested: PublishRelay<Void> { get }
}

protocol MButtonViewModelType {
    var inputs: MButtonViewModelInput { get }
    var outputs: MButtonViewModelOutput { get }
}

class MButtonViewModel: MButtonViewModelInput, MButtonViewModelOutput, MButtonViewModelType {


    typealias Dependencies = AnalyticsServiceDependency & AppModeDependency

    var active: BehaviorRelay<Bool> = BehaviorRelay(value: true)

    // MButtonViewModelOutput
    var outputMButtonModeInit = PublishRelay<MButtonMode>()
    var outputMButtonMode = PublishRelay<MButtonMode>()

    // MButtonViewModelInput
    var inputMButtonMode = PublishRelay<MButtonMode>()

    // MButtonViewModelType
    var inputs: MButtonViewModelInput { return self }
    var outputs: MButtonViewModelOutput { return self }

    var googleAssistantScreenRequested = PublishRelay<Void>()

    private let device: DeviceType
    private var dependencies: Dependencies
    private var disposeBag = DisposeBag()

    init(device: DeviceType, dependencies: Dependencies) {
        self.device = device
        self.dependencies = dependencies

        device.state.outputs.connectivityStatus().stateObservable()
            .asDriver()
            .map { $0.connected }
            .drive(active)
            .disposed(by: disposeBag)

        device.state.outputs.currentAudioSource().stateObservable()
            .asDriver()
            .distinctUntilChanged()
            .map { $0 != .aux }
            .drive(active)
            .disposed(by: disposeBag)

        let mButtonMode = device.state.outputs.mButtonMode().stateObservable().skipOne()
        mButtonMode.takeOne().subscribe(onNext: { [weak self] mButtonMode in
            self?.outputMButtonModeInit.accept(mButtonMode)
        }).disposed(by: disposeBag)
        mButtonMode.skipOne().subscribe(onNext: { [weak self] mButtonMode in
            self?.outputMButtonMode.accept(mButtonMode)
        }).disposed(by: disposeBag)
        inputMButtonMode.asObservable().observeOn(MainScheduler.instance).subscribe({ [weak self] inputMButtonMode in
            guard let mButtonMode = inputMButtonMode.element else { return }
            self?.device.state.inputs.write(mButtonMode: mButtonMode)
            if mButtonMode == .googleVoiceAssistant {
                self?.googleAssistantScreenRequested.accept(())
            }
        }).disposed(by: disposeBag)

        dependencies.appMode.skipOne().observeOn(MainScheduler.instance).subscribe({ [weak self] mode in
            guard let mode = mode.element else { return }
            switch mode {
            case .background:
                break
            case .foreground:
                self?.device.state.inputs.requestMButtonMode()
            }
        }).disposed(by: disposeBag)
    }
    deinit {
        print(#function, String(describing: self))
    }
}

extension MButtonViewModel {
    func viewDidLoad() {
        device.state.inputs.requestMButtonMode()
    }
}


