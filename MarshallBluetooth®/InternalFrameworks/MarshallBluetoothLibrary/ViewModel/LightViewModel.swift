//
//  LightViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 24.05.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//
import RxSwift
import GUMA

/// Input points of the model for Light View
public protocol LightViewModelInput {
    /// Change LED intensity
    ///
    /// - Parameter intensity: LED brightness
    func change(intensity: Int)
    /// Set base image for filter processing (LED "animation")
    ///
    /// - Parameter image: base image for processing
    func original(image: UIImage)
    /// Get LED info when app will go to foreground
    func viewDidBecameActive()
    func  logLightIntensityChangeAnalyticsEvent(with intensity: Int)
}

/// Output points of the model for Light View
public protocol LightViewModelOutput {
    /// Processed LED image
    var ledImage: BehaviorRelay<UIImage> { get }
    /// Led brigthness
    var ledIntensity: ReplaySubject<Int> { get }
    /// Minimum and maximum possible brightness for LED
    var ledIntensityScale: ReplaySubject<(Int, Int)> { get }
    /// Current LED brightness in percentage value
    var ledPercentage: ReplaySubject<Double> { get }
    
    var appMode: BehaviorRelay<AppMode> { get }
    var dataFetched: BehaviorRelay<Bool> { get }
    var active: PublishSubject<Bool> { get }
}

/// Protocol to force input/outputs on view model
public protocol LightViewModelType {
    /// Input points of the view model
    var inputs: LightViewModelInput { get }
    
    /// Output points of the view model
    var outputs: LightViewModelOutput { get }
}

/// Helper structure to calculate percentage value (uses default implementation)
struct LedIntensityFormatter: Percentable {}

/// Helper structure to calculate Lerp (linear interpolation) value (uses default implementation)
/// This is used for mapping Device's LED min-max scale into iOS filter's min-max scale
struct LedInterpolator: Lerpable {}

/// Helper enum to contain [min,max] scope for filter applied to image
fileprivate struct FilterScale {
    static let min = -3.5
    static let max = 2.5
}

/// View model for Light view
class LightViewModel: LightViewModelOutput {
    typealias Dependencies = AppModeDependency & AnalyticsServiceDependency
    
    var ledImage = BehaviorRelay<UIImage>.init(value: UIImage())
    var ledIntensity = ReplaySubject<Int>.create(bufferSize: 1)
    var ledIntensityScale = ReplaySubject<(Int, Int)>.create(bufferSize: 1)
    var ledPercentage = ReplaySubject<Double>.create(bufferSize: 1)
    
    var appMode: BehaviorRelay<AppMode>
    var dataFetched = BehaviorRelay<Bool>.init(value: false)
    var active = PublishSubject<Bool>.init()
    
    private let modelDevice: DeviceType
    private let formatter: Percentable
    private let interpolator: Lerpable
    private var originalImage = PublishSubject<UIImage>()
    private let dependencies: Dependencies
    
    private var disposeBag = DisposeBag()
    
    init(device: DeviceType, dependencies: Dependencies, formatter: Percentable = LedIntensityFormatter(), interpolator: Lerpable = LedInterpolator()) {
        self.modelDevice = device
        self.dependencies = dependencies
        self.appMode = dependencies.appMode
        self.formatter = formatter
        self.interpolator = interpolator

        setupConnection()
        setupLedScale()
        setupLedBrightness()
        setupImageProcessing()
   
        modelDevice.state.inputs.requestLedBrightness()
    }
    
    deinit {
        print(#function, String(describing: self))
    }
}

extension LightViewModel: LightViewModelInput {
    func change(intensity: Int) {
        outputs.ledIntensity.onNext(intensity)
        modelDevice.state.inputs.write(ledBrightness: intensity)
    }
    
    func original(image: UIImage) {
        originalImage.onNext(image)
    }
    
    func viewDidBecameActive() {
        modelDevice.state.inputs.requestLedBrightness()
    }

    func logLightIntensityChangeAnalyticsEvent(with intensity: Int) {
        dependencies.analyticsService.inputs.log(.appLightSettingChanged(intensity), associatedWith: modelDevice.modelName)
    }
}

extension LightViewModel: LightViewModelType {
    var inputs: LightViewModelInput { return self }
    var outputs: LightViewModelOutput { return self }
}

private extension LightViewModel {
    func setupConnection() {
        modelDevice.state.outputs.connectivityStatus().stateObservable()
            .map { $0.connected }
            .bind(to: active)
            .disposed(by: disposeBag)
    }
    
    func setupLedScale() {
        let ledMin = Observable.just(modelDevice.platform.traits.ledMin)
        let ledMax = Observable.just(modelDevice.platform.traits.ledMax)
        
        Observable.combineLatest(ledMin, ledMax) { ($0, $1) }
            .bind(to: ledIntensityScale)
            .disposed(by: disposeBag)
    }
    
    func setupLedBrightness() {
        modelDevice.state.outputs.ledBrightness().stateObservable()
            .skipOne()
            .takeOne()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] current in
                self?.outputs.ledIntensity.onNext(current)
                self?.dataFetched.accept(true)
            }).disposed(by: disposeBag)
    }
    
    func setupImageProcessing() {
        Observable.combineLatest(outputs.ledIntensityScale.asObservable(),
                                 outputs.ledIntensity.asObservable())
            .map { [unowned self] (scale, current) -> Double? in
                let (min, max) = scale
                return self.formatter.percentage(min: Double(min),
                                                 max: Double(max),
                                                 current: Double(current))?.trunc(toPlaces: 2)
            }.subscribe(onNext: { [unowned self] percentage in
                guard let percentage = percentage else { return }
                self.outputs.ledPercentage.onNext(percentage)
            }).disposed(by: disposeBag)
        
        Observable.combineLatest(originalImage.asObservable(),
                                 ledPercentage.asObservable())
            .map { [unowned self] (image, percentage) -> UIImage in
                return self.applyFilter(image, percentage)
            }.subscribe({ [unowned self] image in
                guard let finalImage = image.element else { return }
                self.outputs.ledImage.accept(finalImage)
            }).disposed(by: disposeBag)
    }
}

// MARK: - Image manipulation
private extension LightViewModel {
    func applyFilter(_ image: UIImage?, _ value: Double) -> UIImage {
        
        // Get the original image and set up the CIExposureAdjust filter
        guard let originalImage = image,
            let inputImage = CIImage(image: originalImage),
            let filter = CIFilter(name: "CIExposureAdjust") else {
                fatalError("Original image, input image and filter must be available. Crashing the controlling way due to abnormal conditions")
        }
        
        let originalScale = originalImage.scale
        let originalOrientation = originalImage.imageOrientation
        
        // The inputEV value on the CIFilter adjusts exposure (negative values darken, positive values brighten)
        // First wee need to interpolate the percentage value
        guard let lerpValue = self.interpolator.lerp(x: FilterScale.min, y: FilterScale.max, percentage: value) else {
            fatalError("Percentage value for interpolation needs to be [0,1]. Crashing the controlling way due to abnormal conditions")
        }
        
        filter.setValue(inputImage, forKey: kCIInputImageKey)
        filter.setValue(lerpValue, forKey: kCIInputEVKey)
        
        // Break early if the filter was not a success (.outputImage is optional in Swift)
        guard let filteredImage = filter.outputImage else {
            fatalError("Applying filter must be successfull. Crashing the controlling way due to abnormal conditions")
        }
        
        guard let outputCGImage = CIContext(options: nil).createCGImage(filteredImage, from: filteredImage.extent) else {
            fatalError("Cannot create CGImage. Crashing the controlling way due to abnormal conditions")
        }
        
        return UIImage(cgImage: outputCGImage, scale: originalScale, orientation: originalOrientation)
    }
}
