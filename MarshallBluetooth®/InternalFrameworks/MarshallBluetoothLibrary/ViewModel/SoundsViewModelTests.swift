//
//  SoundsViewModelTests.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Dolewski Bartosz A (Ext) on 02.08.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

fileprivate var connectivityMock = ConnectivityStatus.connected
fileprivate var soundsControlMock = SoundsControl.powerCue(on: true)

public final class TestSoundsDeviceState: DeviceStateType {
    
    public var inputs: DeviceStateInput { return self }
    public var outputs: DeviceStateOutput { return self }
    
    public static let supportedFeatures: [Feature] = [.hasConnectivityStatus, .hasSoundsControl]
    public let perDeviceTypeFeatures: [Feature] = [.hasConnectivityStatus, .hasSoundsControl]
    
    fileprivate let _connectivityStatus: AnyState<ConnectivityStatus>
    fileprivate let _soundsControlStatus: AnyState<SoundsControl>
    
    fileprivate let disposeBag = DisposeBag()

    init() {
        self._connectivityStatus = AnyState<ConnectivityStatus>(feature: .hasConnectivityStatus,
                                                                supportsNotifications: false,
                                                                initialValue: { return connectivityMock })
        
        self._soundsControlStatus = AnyState<SoundsControl>(feature: .hasSoundsControl,
                                                            supportsNotifications: false,
                                                            initialValue: { return soundsControlMock })
    }
}

extension TestSoundsDeviceState: DeviceStateInput {
    public func write(soundControl: SoundsControl) {
    }
    
    public func requestSoundsControlStatus() {
        _soundsControlStatus.stateObservable().accept(soundsControlMock)
    }
}

extension TestSoundsDeviceState: DeviceStateOutput {
    public var activeMonitors: Set<Monitor> {
        get { return  Set<Monitor>() }
        set(newValue) {}
    }
    public func connectivityStatus() -> AnyState<ConnectivityStatus> {
        _connectivityStatus.stateObservable().accept(connectivityMock)
        return _connectivityStatus
    }
    
    public func soundsControlStatus() -> AnyState<SoundsControl> {
        return _soundsControlStatus
    }
}

fileprivate class TestSoundsDevice: DeviceType {
    func bleConnect() -> Single<Void> {
        return Single.just(())
    }
    
    var configured = false
    var mac: MAC?
var hardwareType: HardwareType = .speaker
    
    func decouple() -> Single<Void> {
        return Single.just(())
    }
    
    func resetSlave() -> Single<Void> {
        return Single.just(())
    }
    
    func initialSetup(with storedEntry: StoredDeviceInfoEntry?) -> Single<String> {
        return Single.just(String())
    }
    
    var modelName: String {
        return String()
    }

    var image: DeviceImageProviding = DeviceBlankImageProvider()
    
    
    var connectionTimeout: TimeInterval {
        return TimeInterval.seconds(5.0)
    }
    
    var friendlyName: String { return String(describing: self) }
    var id: String { return String(describing: self) }
    var imageName: String { return String(describing: self) }
    
    let platform = Platform(id: "joplinBT", traits: PlatformTraits())
    
    var state: DeviceStateType
    var otaAdapter: OTAAdapterType?

    init(name: BehaviorRelay<String>,
         state: DeviceStateType,
         otaAdapter: OTAAdapterType? = nil) {
        self.state = state
        self.otaAdapter = otaAdapter
    }
}

final class SoundsViewModelTests: XCTestCase {
    var disposeBag: DisposeBag!
    var vm: SoundsViewModelType!
    
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
        
        let deviceState = TestSoundsDeviceState()
        let device = TestSoundsDevice(name: BehaviorRelay.init(value: "TEST"), state: deviceState, otaAdapter: nil)
        vm = SoundsViewModel(device: device)
    }
    
    func testPowerCueOn() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let power = scheduler.createObserver(Bool.self)
        
        vm.outputs.power.asObservable().subscribe(power).disposed(by: disposeBag)
        
        soundsControlMock = SoundsControl.powerCue(on: true)
        vm.inputs.requestSoundsControlStatus()
        
        XCTAssertEqual([.next(TestTime(), true)], power.events)
    }
    
    func testPowerCueOff() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let power = scheduler.createObserver(Bool.self)
        
        vm.outputs.power.asObservable().subscribe(power).disposed(by: disposeBag)
        
        soundsControlMock = SoundsControl.powerCue(on: false)
        vm.inputs.requestSoundsControlStatus()
        
        XCTAssertEqual([.next(TestTime(), false)], power.events)
    }
    
    func testMediaCueOn() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let media = scheduler.createObserver(Bool.self)
        
        vm.outputs.media.asObservable().subscribe(media).disposed(by: disposeBag)
        
        soundsControlMock = SoundsControl.mediaCue(on: true)
        vm.inputs.requestSoundsControlStatus()
        
        XCTAssertEqual([.next(TestTime(), true)], media.events)
    }
    
    func testMediaCueOff() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let media = scheduler.createObserver(Bool.self)
        
        vm.outputs.media.asObservable().subscribe(media).disposed(by: disposeBag)
        
        soundsControlMock = SoundsControl.mediaCue(on: false)
        vm.inputs.requestSoundsControlStatus()
        
        XCTAssertEqual([.next(TestTime(), false)], media.events)
    }
    
    func testNotFetchedYet() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let dataFetched = scheduler.createObserver(Bool.self)
        
        vm.outputs.dataFetched.asObservable().subscribe(dataFetched).disposed(by: disposeBag)
        
        XCTAssertTrue(dataFetched.events.isEmpty)
        
        vm.inputs.requestSoundsControlStatus()
        XCTAssertTrue(dataFetched.events.isEmpty)
    }
    
    func testFetched() {
        let scheduler = TestScheduler(initialClock: TestTime())
        let dataFetched = scheduler.createObserver(Bool.self)
        
        vm.outputs.dataFetched.asObservable().subscribe(dataFetched).disposed(by: disposeBag)
        
        XCTAssertTrue(dataFetched.events.isEmpty)
        
        soundsControlMock = SoundsControl.powerCue(on: true)
        vm.inputs.requestSoundsControlStatus()
        
        soundsControlMock = SoundsControl.mediaCue(on: true)
        vm.inputs.requestSoundsControlStatus()
        
        XCTAssertEqual([.next(TestTime(), true)], dataFetched.events)
    }
}
