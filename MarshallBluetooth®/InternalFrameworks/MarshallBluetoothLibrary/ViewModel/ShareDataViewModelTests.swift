//
//  ShareDataViewModelTests.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Bartosz Dolewski on 08.08.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

private var mockStoreAnalytics = true

private final class MockStorage: ShareAnalyticsStorageItems {
    var shareAnalyticsData = mockStoreAnalytics
}

final class ShareDataViewModelTests: XCTestCase {
    var disposeBag: DisposeBag!
    var vm: ShareDataViewModelType!
    
    private var storage: MockStorage!
    
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
        storage = MockStorage()
    }

    func setup(wizardMode: Bool) {
        vm = ShareDataViewModel(wizardMode: wizardMode, dependencies: MockedAnalyticsServiceDependency(), preferences: storage)
    }

    func testSaveDataSwitched() {
        setup(wizardMode: false)

        let scheduler = TestScheduler(initialClock: TestTime())
        let enabled = scheduler.createObserver(Bool.self)
        
        vm.outputs.shareDataEnabled.subscribe(enabled).disposed(by: disposeBag)
        XCTAssertEqual([.next(TestTime(), true)], enabled.events)
        
        vm.inputs.saveDataSwitched()
        vm.inputs.saveDataSwitched()
        vm.inputs.saveDataSwitched()
        vm.inputs.saveDataSwitched()
        vm.inputs.saveDataSwitched()
        vm.inputs.nextButtonTapped()
        
        XCTAssertEqual(storage.shareAnalyticsData, false)
        XCTAssertEqual([.next(TestTime(), true),
                        .next(TestTime(), false),
                        .next(TestTime(), true),
                        .next(TestTime(), false),
                        .next(TestTime(), true),
                        .next(TestTime(), false)], enabled.events)
        
        vm.inputs.saveDataSwitched()
        vm.inputs.nextButtonTapped()
        
        XCTAssertEqual(storage.shareAnalyticsData, true)
        XCTAssertEqual([.next(TestTime(), true),
                        .next(TestTime(), false),
                        .next(TestTime(), true),
                        .next(TestTime(), false),
                        .next(TestTime(), true),
                        .next(TestTime(), false),
                        .next(TestTime(), true)], enabled.events)
    }
    
    func testCompleteButtonTapped() {
        setup(wizardMode: false)

        let scheduler = TestScheduler(initialClock: TestTime())
        let tapped = scheduler.createObserver(Bool.self)
        
        vm.outputs.viewCompleted.subscribe(tapped).disposed(by: disposeBag)
        XCTAssertEqual([.next(TestTime(), false)], tapped.events)
        
        vm.inputs.nextButtonTapped()
        vm.inputs.nextButtonTapped()
        vm.inputs.nextButtonTapped()
        vm.inputs.nextButtonTapped()
        vm.inputs.nextButtonTapped()
        vm.inputs.nextButtonTapped()
        
        XCTAssertEqual([.next(TestTime(), false),
                        .next(TestTime(), true),
                        .next(TestTime(), true),
                        .next(TestTime(), true),
                        .next(TestTime(), true),
                        .next(TestTime(), true),
                        .next(TestTime(), true)], tapped.events)
        
    }

    func testSkipButtonVisible() {
        setup(wizardMode: true)

        let scheduler = TestScheduler(initialClock: TestTime())
        let visible = scheduler.createObserver(Bool.self)

        vm.outputs.skipVisible.subscribe(visible).disposed(by: disposeBag)

        XCTAssertEqual([.next(TestTime(), true)], visible.events)
    }

    func testSkipButtonHidden() {
        setup(wizardMode: false)

        let scheduler = TestScheduler(initialClock: TestTime())
        let visible = scheduler.createObserver(Bool.self)

        vm.outputs.skipVisible.subscribe(visible).disposed(by: disposeBag)

        XCTAssertEqual([.next(TestTime(), false)], visible.events)
    }
}
