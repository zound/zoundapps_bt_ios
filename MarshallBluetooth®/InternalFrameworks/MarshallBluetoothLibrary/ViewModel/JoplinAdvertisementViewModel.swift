//
//  AdvertisementViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 11/04/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift

protocol JoplinAdvertisementViewModelInput {
    func viewAppeared()
}

protocol JoplinAdvertisementViewModelOutput {
    var viewReady:PublishRelay<Void> { get }
    var setupProgress: Observable<SetupProgressInfo> { get }
    var updateFinished: PublishRelay<Void> { get }
}

protocol JoplinAdvertisementViewModelType {
    var inputs: JoplinAdvertisementViewModelInput { get }
    var outputs: JoplinAdvertisementViewModelOutput { get }
}

final class JoplinAdvertisementViewModel: JoplinAdvertisementViewModelInput, JoplinAdvertisementViewModelOutput, JoplinAdvertisementViewModelType {
    
    var inputs: JoplinAdvertisementViewModelInput { return self }
    var outputs: JoplinAdvertisementViewModelOutput { return self }
    
    var viewReady = PublishRelay<Void>()
    var setupProgress: Observable<SetupProgressInfo>
    var updateFinished = PublishRelay<Void>()
    
    init(progress: Observable<SetupProgressInfo>) {
        self.setupProgress = progress
    }
    
    func viewAppeared() {
        viewReady.accept(())
    }
    
    private var disposeBag = DisposeBag()
}
