//
//  Advertisement.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 11/04/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import GUMA

enum AdvertisementPage {
    case customizeSound
    case eqPresets
    case stereo
    case ozzyANC
    case ozzyMButton
    case ozzyEqualizer
    
    static func advertisements(platform: Platform) -> [AdvertisementPage] {
        switch(platform.id) {
        case "joplinBT": return [.customizeSound, .eqPresets, .stereo]
        case "joplinBTMock": return [.customizeSound, .eqPresets, .stereo]
        default:
            fatalError("Add support for platform \(platform)")
        }
    }
    static func ozzyAdvertisements() -> [AdvertisementPage] {
        return [.ozzyANC, .ozzyMButton, .ozzyEqualizer]
    }
    static func viewController(for page: AdvertisementPage) -> UIViewController {
        switch page {
        case .customizeSound: return UIViewController.instantiate(CustomizeAdvertViewController.self)
        case .eqPresets: return UIViewController.instantiate(EqPresetsAdvertViewController.self)
        case .stereo: return UIViewController.instantiate(CoupleSpeakersAdvertViewController.self)
        case .ozzyANC: return UIViewController.instantiate(OzzyAdvertANCViewController.self)
        case .ozzyMButton: return UIViewController.instantiate(OzzyAdvertMButtonViewController.self)
        case .ozzyEqualizer: return UIViewController.instantiate(OzzyAdvertEqualizerViewController.self)
        }
    }
    static func viewControllers(for platform: Platform) -> [UIViewController] {
        return AdvertisementPage.advertisements(platform: platform)
            .map { viewController(for: $0 ) }
    }
    static func ozzyViewControllers() -> [UIViewController] {
        return AdvertisementPage.ozzyAdvertisements()
            .map { viewController(for: $0 ) }
    }
}
