//
//  CircularProgressViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 23/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift

protocol CircularProgressViewModelInput {
    func viewDidAppear()
}

protocol CircularProgressViewModelOutput {
    var viewAppeared: PublishSubject<Bool> { get }
}

protocol CircualProgressViewModelType {
    var inputs: CircularProgressViewModelInput { get }
    var outputs: CircularProgressViewModelOutput { get }
}

public final class CircularProgressViewModel: CircularProgressViewModelInput, CircularProgressViewModelOutput, CircualProgressViewModelType {
    
    var inputs: CircularProgressViewModelInput { return self }
    var outputs: CircularProgressViewModelOutput { return self }
    
    func viewDidAppear() {
        viewAppeared.onNext(true)
    }

    var viewAppeared = PublishSubject<Bool>()
}
