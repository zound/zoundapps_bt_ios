//
//  EqualizerViewModelTests.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Wudarski Lukasz on 07/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

import XCTest
import RxSwift
import RxTest
import GUMA

@testable import MarshallBluetoothLibrary

private struct Config {
    static let delayAfterStartMonitoringGraphicalEqualizer = TimeInterval.seconds(1)
    static let expectedFultillmentCount = 2
    static let outputGraphicalEqualizerWaitTimeout = Double(expectedFultillmentCount * 2)
}

private struct DummyDefaultStorage: EqualizerStorageItems {
    var customDefaultGraphicalEqualizer: GraphicalEqualizer? = GraphicalEqualizer.Preset.customDefault
}

private class DummyEqualizerDeviceState: DeviceStateType {

    public let perDeviceTypeFeatures: [Feature] = []
    
    let disposeBag = DisposeBag()

    var inputs: DeviceStateInput { return self }
    var outputs: DeviceStateOutput { return self }

    fileprivate let _connectivityStatus: AnyState<ConnectivityStatus>
    fileprivate let _graphicalEqualizer: AnyState<GraphicalEqualizer>
    fileprivate let _monitoredGraphicalEqualizer: AnyState<GraphicalEqualizer>

    var configurableGraphicalEqualizer = GraphicalEqualizer()
    var configurableMonitoredGraphicalEqualizer = GraphicalEqualizer()

    init() {
        _connectivityStatus = AnyState<ConnectivityStatus>(feature: .hasConnectivityStatus, supportsNotifications: false, initialValue: { return .disconnected })
        _graphicalEqualizer = AnyState<GraphicalEqualizer>(feature: .hasGraphicalEqualizer, supportsNotifications: false, initialValue: { return GraphicalEqualizer() })
        _monitoredGraphicalEqualizer = AnyState<GraphicalEqualizer>(feature: .hasGraphicalEqualizer, supportsNotifications: false, initialValue: { return GraphicalEqualizer() })
    }

    func disconnectTest() {
        _connectivityStatus.stateObservable().accept(.disconnected)
    }
    func connectTest() {
        _connectivityStatus.stateObservable().accept(.connected)
    }
}

extension DummyEqualizerDeviceState: DeviceStateInput {
    func requestGraphicalEqualizer() {
        _graphicalEqualizer.stateObservable().accept(configurableGraphicalEqualizer)
    }
    func startMonitoringGraphicalEqualizer() {
        _ = Observable<Int>
            .timer(RxTimeInterval(Config.delayAfterStartMonitoringGraphicalEqualizer), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self._monitoredGraphicalEqualizer.stateObservable().accept(self.configurableMonitoredGraphicalEqualizer)
            }).disposed(by: disposeBag)
    }
    func stopMonitoringGraphicalEqualizer() {
        // NOTE: Not used since startMonitoringGraphicalEqualizer() emits only one event
    }
    func write(graphicalEqualizer: GraphicalEqualizer) {
        configurableGraphicalEqualizer = graphicalEqualizer
    }
}

extension DummyEqualizerDeviceState: DeviceStateOutput {
    public var activeMonitors: Set<Monitor> {
        get { return  Set<Monitor>() }
        set(newValue) {}
    }
    
    static var supportedFeatures: [Feature] { return [.hasGraphicalEqualizer] }
    func connectivityStatus() -> AnyState<ConnectivityStatus> {
        return _connectivityStatus
    }
    func graphicalEqualizer() -> AnyState<GraphicalEqualizer> {
        return _graphicalEqualizer
    }
    func monitoredGraphicalEqualizer() -> AnyState<GraphicalEqualizer> {
        return _monitoredGraphicalEqualizer
    }
}

private class EqualiserExposingDummyDevice: DeviceType {
    func bleConnect() -> Single<Void> {
        return Single.just(())
    }
    
    var mac: MAC?
    var configured = false
    var hardwareType: HardwareType = .speaker
    
    func decouple() -> Single<Void> {
        return Single.just(())
    }
    
    func resetSlave() -> Single<Void> {
        return Single.just(())
    }
    
    func initialSetup(with storedEntry: StoredDeviceInfoEntry?) -> Single<String> {
        return Single.just(String())
    }
    
    var modelName: String {
        return String()
    }
    var image: DeviceImageProviding = DeviceBlankImageProvider()
    
    var connectionTimeout: RxTimeInterval {
        return RxTimeInterval.seconds(5.0)
    }
    
    var name: BehaviorRelay<String>
    var id: String { return String(describing: self) }
    var imageName: String { return String(describing: self) }
    let platform = Platform(id: "joplinBT", traits: PlatformTraits())
    var state: DeviceStateType
    var otaAdapter: OTAAdapterType?

    init(name: BehaviorRelay<String>,
         state: DeviceStateType,
         otaAdapter: OTAAdapterType? = nil) {
        self.name = name
        self.state = state
        self.otaAdapter = otaAdapter
    }
}

final class EqualizerViewModelTests: XCTestCase {
    let disposeBag = DisposeBag()
    fileprivate var dummyEqualizerDeviceState: DummyEqualizerDeviceState!
    var viewModel: EqualizerViewModel!

    func setUpDevice() {
        dummyEqualizerDeviceState = DummyEqualizerDeviceState()
        let dummyDevice = EqualiserExposingDummyDevice(name: BehaviorRelay<String>(value: String(describing: self)),
                                                       state: dummyEqualizerDeviceState,
                                                       otaAdapter: nil)
        viewModel = EqualizerViewModel(device: dummyDevice, dependencies: MockedAnalyticsServiceDependency(), preferences: DummyDefaultStorage())
    }

    func testConnection() {
        setUpDevice()
        let testScheduler = TestScheduler(initialClock: TestTime())
        let activeObserver = testScheduler.createObserver(Bool.self)
        viewModel.outputs.active
            .asDriver()
            .drive(activeObserver)
            .disposed(by: disposeBag)
        testScheduler.start()

        dummyEqualizerDeviceState.connectTest()
        dummyEqualizerDeviceState.disconnectTest()

        XCTAssertEqual([
            .next(TestTime(), false),
            .next(TestTime(), true),
            .next(TestTime(), false)
        ], activeObserver.events)
    }

    func testOutputGraphicalEqualizerInit() {
        setUpDevice()
        let testScheduler = TestScheduler(initialClock: TestTime())
        let outputGraphicalEqualizerInitObserver = testScheduler.createObserver(GraphicalEqualizerInit.self)
        viewModel.outputs.outputGraphicalEqualizerInit
            .asObservable()
            .subscribe(outputGraphicalEqualizerInitObserver)
            .disposed(by: disposeBag)
        testScheduler.start()

        let expectedGraphicalEqualizerInit = GraphicalEqualizerInit(graphicalEqualizer: GraphicalEqualizer(),
                                                                    customGraphicalEqualizer: GraphicalEqualizer.Preset.customDefault)
        XCTAssertTrue(outputGraphicalEqualizerInitObserver.events.isEmpty)
        viewModel.inputs.viewDidLoad()
        XCTAssertEqual([.next(TestTime(), expectedGraphicalEqualizerInit)], outputGraphicalEqualizerInitObserver.events)
        viewModel.inputs.viewDidLoad()
        XCTAssertEqual([.next(TestTime(), expectedGraphicalEqualizerInit)], outputGraphicalEqualizerInitObserver.events)
    }

    func testOutputGraphicalEqualizer() {
        setUpDevice()
        let testScheduler = TestScheduler(initialClock: TestTime())
        let outputGraphicalEqualizerObserver = testScheduler.createObserver(GraphicalEqualizer.self)
        viewModel.outputs.outputGraphicalEqualizer
            .asObservable()
            .subscribe(outputGraphicalEqualizerObserver)
            .disposed(by: disposeBag)
        testScheduler.start()

        XCTAssertTrue(outputGraphicalEqualizerObserver.events.isEmpty)
        viewModel.inputs.viewDidLoad()
        XCTAssertTrue(outputGraphicalEqualizerObserver.events.isEmpty)
        viewModel.inputs.viewDidBecameActive()
        XCTAssertEqual([.next(TestTime(), GraphicalEqualizer())], outputGraphicalEqualizerObserver.events)
        viewModel.inputs.viewDidBecameActive()
        XCTAssertEqual([
            .next(TestTime(), GraphicalEqualizer()),
            .next(TestTime(), GraphicalEqualizer())
        ], outputGraphicalEqualizerObserver.events)
    }

    func testOutputGraphicalEqualizerWhenMonitoringGraphicalEqualizer() {
        setUpDevice()
        let outputGraphicalEqualizerExpectation = XCTestExpectation(description: String())
        outputGraphicalEqualizerExpectation.expectedFulfillmentCount = Config.expectedFultillmentCount
        viewModel.outputs.outputGraphicalEqualizer
            .asObservable()
            .subscribe {
                XCTAssertNotNil($0.element)
                XCTAssertEqual($0.element!, GraphicalEqualizer())
                outputGraphicalEqualizerExpectation.fulfill()
                self.viewModel.inputs.viewDidAppear()
            }
            .disposed(by: disposeBag)

        viewModel.inputs.viewDidLoad()
        viewModel.inputs.viewDidAppear()
        wait(for: [outputGraphicalEqualizerExpectation], timeout: Config.outputGraphicalEqualizerWaitTimeout)
    }

    func testInputGraphicalEqualizer() {
        setUpDevice()
        let outputGraphicalEqualizerTestScheduler = TestScheduler(initialClock: TestTime())
        let outputGraphicalEqualizerInitTestScheduler = TestScheduler(initialClock: TestTime())
        let outputGraphicalEqualizerObserver = outputGraphicalEqualizerTestScheduler.createObserver(GraphicalEqualizer.self)
        let outputGraphicalEqualizerInitObserver = outputGraphicalEqualizerInitTestScheduler.createObserver(GraphicalEqualizerInit.self)
        viewModel.outputs.outputGraphicalEqualizer
            .asObservable()
            .subscribe(outputGraphicalEqualizerObserver)
            .disposed(by: disposeBag)
        viewModel.outputs.outputGraphicalEqualizerInit
            .asObservable()
            .subscribe(outputGraphicalEqualizerInitObserver)
            .disposed(by: disposeBag)
        outputGraphicalEqualizerTestScheduler.start()
        outputGraphicalEqualizerInitTestScheduler.start()

        viewModel.inputs.inputGraphicalEqualizer.accept(GraphicalEqualizer())

        XCTAssertTrue(outputGraphicalEqualizerObserver.events.isEmpty)
        XCTAssertTrue(outputGraphicalEqualizerInitObserver.events.isEmpty)
    }

}
