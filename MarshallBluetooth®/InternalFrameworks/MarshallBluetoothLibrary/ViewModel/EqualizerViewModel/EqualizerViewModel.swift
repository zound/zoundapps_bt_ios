//
//  EqualizerViewModel.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 21/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

struct GraphicalEqualizerInit: Equatable {
    let graphicalEqualizer: GraphicalEqualizer
    let customGraphicalEqualizer: GraphicalEqualizer
}

protocol EqualizerViewModelInput {
    func viewDidLoad()
    func viewDidAppear()
    func viewDidBecameActive()
    func viewWillResignActive()
    var inputGraphicalEqualizer: PublishRelay<GraphicalEqualizer> { get set }
}

protocol EqualizerViewModelOutput {
    var outputGraphicalEqualizerInit: PublishRelay<GraphicalEqualizerInit> { get set }
    var outputGraphicalEqualizer: PublishRelay<GraphicalEqualizer> { get set }
    var active: BehaviorRelay<Bool> { get set }
}

protocol EqualizerViewModelType {
    var inputs: EqualizerViewModelInput { get }
    var outputs: EqualizerViewModelOutput { get }
}

class EqualizerViewModel: EqualizerViewModelInput, EqualizerViewModelOutput, EqualizerViewModelType {
    typealias Dependencies = AnalyticsServiceDependency

    var active: BehaviorRelay<Bool> = BehaviorRelay(value: true)

    // EqualizerViewModelOutput
    var outputGraphicalEqualizerInit = PublishRelay<GraphicalEqualizerInit>()
    var outputGraphicalEqualizer = PublishRelay<GraphicalEqualizer>()

    // EqualizerViewModelInput
    var inputGraphicalEqualizer = PublishRelay<GraphicalEqualizer>()

    // EqualizerViewModelType
    var inputs: EqualizerViewModelInput { return self }
    var outputs: EqualizerViewModelOutput { return self }

    private let device: DeviceType
    private var preferences: EqualizerStorageItems
    private var dependencies: Dependencies
    private var disposeBag = DisposeBag()

    init(device: DeviceType, dependencies: Dependencies, preferences: EqualizerStorageItems) {
        self.device = device
        self.dependencies = dependencies
        self.preferences = preferences

        device.state.outputs.connectivityStatus().stateObservable()
            .asDriver()
            .map { $0.connected }
            .drive(active)
            .disposed(by: disposeBag)

        let graphicalEqualizer = device.state.outputs.graphicalEqualizer().stateObservable().skipOne()
        graphicalEqualizer.takeOne().subscribe(onNext: { [weak self] graphicalEqualizer in
                let customDefaultGraphicalEqualizer = preferences.customDefaultGraphicalEqualizer ?? GraphicalEqualizer.Preset.customDefault
                let graphicalEqualizerInit = GraphicalEqualizerInit(graphicalEqualizer: graphicalEqualizer, customGraphicalEqualizer: customDefaultGraphicalEqualizer)
                self?.outputGraphicalEqualizerInit.accept(graphicalEqualizerInit)
        }).disposed(by: disposeBag)
        graphicalEqualizer.skipOne().subscribe(onNext: { [weak self] graphicalEqualizer in
                self?.outputGraphicalEqualizer.accept(graphicalEqualizer)
        }).disposed(by: disposeBag)

        let monitoredGraphicalEqualizer = device.state.outputs.monitoredGraphicalEqualizer().stateObservable().skipOne()
        monitoredGraphicalEqualizer.subscribe(onNext: { [weak self] graphicalEqualizer in
            self?.outputGraphicalEqualizer.accept(graphicalEqualizer)
        }).disposed(by: disposeBag)

        inputGraphicalEqualizer.asObservable().observeOn(MainScheduler.instance).subscribe({ [weak self] inputGraphicalEqualizerElement in
            guard let graphicalEqualizer = inputGraphicalEqualizerElement.element else { return }
            device.state.inputs.write(graphicalEqualizer: graphicalEqualizer)
            dependencies.analyticsService.inputs.log(GraphicalEqualizerPreset(graphicalEqualizer: graphicalEqualizer).analyticsEvent, associatedWith: device.modelName)
            self?.storeCustomGraphicalEqualizerIfNeeded(graphicalEqualizer)
        }).disposed(by: disposeBag)
    }

    deinit {
        print(#function, String(describing: self))
    }
}

extension EqualizerViewModel {

    func viewDidLoad() {
        self.device.state.inputs.requestGraphicalEqualizer()
    }

    func viewDidAppear() {
        self.device.state.inputs.startMonitoringGraphicalEqualizer()
    }

    func viewDidBecameActive() {
        self.device.state.inputs.requestGraphicalEqualizer()
        self.device.state.inputs.startMonitoringGraphicalEqualizer()
    }

    func viewWillResignActive() {
        self.device.state.inputs.stopMonitoringGraphicalEqualizer()
    }

}

private extension EqualizerViewModel {
    func storeCustomGraphicalEqualizerIfNeeded(_ graphicalEqualizer: GraphicalEqualizer) {
        if graphicalEqualizer.isCustom {
            preferences.customDefaultGraphicalEqualizer = graphicalEqualizer
        }
    }
}

extension GraphicalEqualizerPreset: AnalyticsEventProviding {
    var analyticsEvent: AnalyticsEvent {
        switch self {
        case .flat:
            return .appGraphicalEqualizerPresetChanged(.flat)
        case .rock:
            return .appGraphicalEqualizerPresetChanged(.rock)
        case .metal:
            return .appGraphicalEqualizerPresetChanged(.metal)
        case .pop:
            return .appGraphicalEqualizerPresetChanged(.pop)
        case .hipHop:
            return .appGraphicalEqualizerPresetChanged(.hipHop)
        case .electronic:
            return .appGraphicalEqualizerPresetChanged(.electronic)
        case .jazz:
            return .appGraphicalEqualizerPresetChanged(.jazz)
        case .custom:
            return .appGraphicalEqualizerPresetChanged(.custom)
        }
    }
}
