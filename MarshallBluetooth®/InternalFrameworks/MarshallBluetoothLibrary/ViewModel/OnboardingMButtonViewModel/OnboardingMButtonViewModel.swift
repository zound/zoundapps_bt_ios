//
//  OnboardingMButtonViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 10/02/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

protocol OnboardingMButtonViewModelInput {
    func viewDidLoad()
    func viewAppeared()
    func actionButtonTouchedUpInside()
    var inputMButtonMode: PublishRelay<MButtonMode> { get set }
}
protocol OnboardingMButtonViewModelOutput {
    var outputMButtonModeInit: PublishRelay<MButtonMode> { get set }
    var outputMButtonMode: PublishRelay<MButtonMode> { get set }
    var viewReady: PublishRelay<Void> { get }
    var finished: PublishRelay<Void> { get }
    var googleAssistantScreenRequested: PublishRelay<Void> { get }
}
protocol OnboardingMButtonViewModelType {
    var inputs: OnboardingMButtonViewModelInput { get }
    var outputs: OnboardingMButtonViewModelOutput { get }
}
final class OnboardingMButtonViewModel: OnboardingMButtonViewModelInput, OnboardingMButtonViewModelOutput, OnboardingMButtonViewModelType {

    var inputs: OnboardingMButtonViewModelInput { return self }
    var outputs: OnboardingMButtonViewModelOutput { return self }

    // MButtonViewModelOutput
    var outputMButtonModeInit = PublishRelay<MButtonMode>()
    var outputMButtonMode = PublishRelay<MButtonMode>()

    // MButtonViewModelInput
    var inputMButtonMode = PublishRelay<MButtonMode>()
    
    var viewReady = PublishRelay<Void>()
    var finished = PublishRelay<Void>()
    var googleAssistantScreenRequested = PublishRelay<Void>()
    var skip = PublishRelay<Void>()
    let device: DeviceType
    private var disposeBag = DisposeBag()
    init(device: DeviceType) {
        self.device = device

        let mButtonMode = device.state.outputs.mButtonMode().stateObservable().skipOne()
        mButtonMode.takeOne().subscribe(onNext: { [weak self] mButtonMode in
            self?.outputMButtonModeInit.accept(mButtonMode)
        }).disposed(by: disposeBag)
        mButtonMode.skipOne().subscribe(onNext: { [weak self] mButtonMode in
            self?.outputMButtonMode.accept(mButtonMode)
        }).disposed(by: disposeBag)
        inputMButtonMode.asObservable().observeOn(MainScheduler.instance).subscribe({ [weak self] inputMButtonMode in
            guard let mButtonMode = inputMButtonMode.element else { return }
            self?.device.state.inputs.write(mButtonMode: mButtonMode)
            if mButtonMode == .googleVoiceAssistant {
                self?.googleAssistantScreenRequested.accept(())
            }
        }).disposed(by: disposeBag)
    }
}

extension OnboardingMButtonViewModel {
    func viewDidLoad() {
        device.state.inputs.requestMButtonMode()
    }
    func viewAppeared() {
        viewReady.accept(())
    }
    func actionButtonTouchedUpInside() {
        finished.accept(())
    }
}
