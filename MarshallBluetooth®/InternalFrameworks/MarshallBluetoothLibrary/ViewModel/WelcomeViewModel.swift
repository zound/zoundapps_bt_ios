//
//  WelcomeViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 13/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

protocol WelcomeViewModelInput {
    func userAcceptedTerms()
    func showTermsAndConditions()
    func introAnimated()
}

protocol WelcomeViewModelOutput {
    // (next button typed)
    var termsAccepted : BehaviorRelay<Void> { get set }
    /// Notify when agreement option has changed
    var tacAgreementSwitchEnabled: BehaviorRelay<Bool> { get }
    /// Terms And Conditions screen was requested
    var termsAndConditionsRequested: BehaviorRelay<Void> { get }
    var shouldAnimateIntro: Bool { get }
}

protocol WelcomeViewModelType {
    var inputs: WelcomeViewModelInput { get }
    var outputs: WelcomeViewModelOutput { get }
}

public final class WelcomeViewModel: WelcomeViewModelInput, WelcomeViewModelOutput, WelcomeViewModelType {

    var inputs: WelcomeViewModelInput { return self }
    var outputs: WelcomeViewModelOutput { return self }

    let defaultStorage: DefaultStorage = DefaultStorage.shared

    var tacAgreementSwitchEnabled = BehaviorRelay<Bool>(value: true)
    var termsAccepted = BehaviorRelay<Void>(value: ())
    var termsAndConditionsRequested = BehaviorRelay<Void>(value: ())
    var shouldAnimateIntro: Bool {
        return !defaultStorage.welcomeIntroAnimated
    }

    func userAcceptedTerms() {
        self.outputs.termsAccepted.accept(())
    }

    func showTermsAndConditions() {
        self.outputs.termsAndConditionsRequested.accept(())
    }

    func introAnimated() {
        defaultStorage.welcomeIntroAnimated = true
    }

}
