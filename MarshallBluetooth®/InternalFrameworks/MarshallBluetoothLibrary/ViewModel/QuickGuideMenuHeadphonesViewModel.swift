//
//  QuickGuideMenuHeadphonesViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Szatkowski Michal on 18/02/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit

class QuickGuideMenuHeadphonesViewModel: CommonMenuViewModel {

    override init() {
        super.init()
        var  items = [CommonMenuItem]()
        items.append(CommonMenuItem(nextScreen: .bluetoothHeadphonesGuide, label: Strings.main_menu_item_bluetooth_pairing()))
        items.append(CommonMenuItem(nextScreen: .ancButtonGuide, label: Strings.main_menu_item_anc_button()))
        items.append(CommonMenuItem(nextScreen: .mButtonGuide, label: Strings.main_menu_item_m_button()))
        items.append(CommonMenuItem(nextScreen: .controlKnobGuide, label: Strings.main_menu_item_control_knob()))

        dataSource.accept(items)
        title.accept(Strings.main_menu_item_quick_guide_uc())
    }
}
