//
//  OTAFinishedViewModelTests.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 06.04.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

final class OTAFinishedViewModelTests: XCTestCase {
    var disposeBag: DisposeBag!
    var vm: OTAFinishedViewModel!
    
    override func setUp() {
        super.setUp()
        
        disposeBag = DisposeBag()
        vm = OTAFinishedViewModel(image: UIImage())
    }
    
    func testDoneButton() {
        let scheduler = TestScheduler(initialClock: 0)
        
        let done = scheduler.createObserver(Bool.self)
        vm.outputs.done.asDriver().drive(done).disposed(by: disposeBag)
        
        scheduler.start()
        
        // make sure that initial state is valid - user enter the AllFinished screen
        XCTAssertEqual([Recorded.next(0, false)], done.events)
        
        // user taps on DONE button
        vm.inputs.doneTapped()
        
        // check if model will notify flow about DONE button
        XCTAssertEqual([ Recorded.next(0, false),
                         Recorded.next(0, true)], done.events)
    }
}
