//
//  OzzyPlayerViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 20/02/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

private struct Const {
    static let currentAudioSourceDelay = 0.5
}

enum AudioControlStatus {
    case isPaused
    case isPlaying
    init(playbackStatus: PlaybackStatus) {
        switch playbackStatus {
        case .playing:
            self = .isPlaying
        default:
            self = .isPaused
        }
    }
}

struct AudioNowPlaying: Equatable {
    let title: String
    let artist: String
    let album: String
    init(trackInfo: TrackInfo) {
        self.title = trackInfo.title ?? String.invisible
        self.artist = trackInfo.artist ?? String.invisible
        self.album = trackInfo.album ?? String.invisible
    }
    init() {
        (title, artist, album) = (String.invisible, String.invisible, String.invisible)
    }
}

protocol OzzyPlayerViewModelInput {
    func close()
    func viewDidLoad()
    func nextButtonTouchedUpInside()
    func previusButtonTouchedUpInside()
    func playPauseButtonTouchedUpInside()
    var inputVolume: PublishRelay<Int> { get set }
}

protocol OzzyPlayerViewModelOutput {
    var closed: PublishSubject<Void> { get }
    var platformTraits: PlatformTraits { get }
    var outputAudioControlStatus: PublishRelay<AudioControlStatus> { get }
    var outputAudioNowPlaying: PublishRelay<AudioNowPlaying> { get }
    var outputAuxVisible: PublishRelay<Bool> { get }
    var outputVolumeInit: PublishRelay<Int> { get set }
    var outputVolume: PublishRelay<Int> { get set }
}

protocol OzzyPlayerViewModelType {
    var inputs: OzzyPlayerViewModelInput { get }
    var outputs: OzzyPlayerViewModelOutput { get }
}

class OzzyPlayerViewModel: OzzyPlayerViewModelInput, OzzyPlayerViewModelOutput {
    typealias Dependencies = AnalyticsServiceDependency & AppModeDependency

    static func supportedMonitors() -> [Monitor] {
        return [.volume, .audioSource, .currentTrack]
    }

    var inputs: OzzyPlayerViewModelInput { return self }
    var outputs: OzzyPlayerViewModelOutput { return self }

    // OzzyPlayerViewModelOutput
    var closed = PublishSubject<Void>.init()
    var platformTraits: PlatformTraits { return device.platform.traits }
    var outputAudioControlStatus = PublishRelay<AudioControlStatus>()
    var outputAudioNowPlaying = PublishRelay<AudioNowPlaying>()
    var outputAuxVisible = PublishRelay<Bool>()
    var outputVolumeInit = PublishRelay<Int>()
    var outputVolume = PublishRelay<Int>()

    // OzzyPlayerViewModelInput
    var inputVolume = PublishRelay<Int>()

    private let device: DeviceType
    private var disposeBag = DisposeBag()

    var dependencies: Dependencies

    private var currentAudioControlStatus: AudioControlStatus = .isPaused {
        didSet {
            guard oldValue != currentAudioControlStatus else { return }
            outputAudioControlStatus.accept(currentAudioControlStatus)
        }
    }
    private var currentAudioNowPlaying: AudioNowPlaying = AudioNowPlaying() {
        didSet {
            guard oldValue != currentAudioNowPlaying else { return }
            outputAudioNowPlaying.accept(currentAudioNowPlaying)
        }
    }
    private var auxVisible: Bool = true {
        didSet {
            guard oldValue != auxVisible else { return }
            outputAuxVisible.accept(auxVisible)
        }
    }

    init(device: DeviceType, dependencies: Dependencies) {
        self.device = device
        self.dependencies = dependencies

        // Connectivity
        device.state.outputs.connectivityStatus().stateObservable().observeOn(MainScheduler.instance).debug(">>")
        .subscribe({ [weak self] connectivityStatusEvent in
            guard let connectivityStatus = connectivityStatusEvent.element else { return }
            if connectivityStatus == .disconnected {
                self?.close()
            }
        }).disposed(by: disposeBag)

        // Volume
        inputVolume.asObservable().observeOn(MainScheduler.instance).subscribe({ [weak self] inputVolume in
            guard let volume = inputVolume.element else { return }
            self?.device.state.inputs.write(volume: volume)
        }).disposed(by: disposeBag)
        device.state.outputs.volume().stateObservable().observeOn(MainScheduler.instance)
            .skipOne().takeOne().subscribe({ [weak self] volumeEvent in
                guard let volume = volumeEvent.element else { return }
                self?.outputVolumeInit.accept(volume)
            }).disposed(by: disposeBag)
        device.state.outputs.monitoredVolume().stateObservable().observeOn(MainScheduler.instance)
            .skip(2).throttle(1, scheduler: MainScheduler.instance).subscribe({ [weak self] volumeEvent in
                guard let volume = volumeEvent.element else { return }
                self?.outputVolume.accept(volume)
            }).disposed(by: disposeBag)

        // Playback status
        device.state.outputs.playbackStatus().stateObservable().observeOn(MainScheduler.instance)
        .skipOne().subscribe({ [weak self] playbackStatusEvent in
            guard let playbackStatus = playbackStatusEvent.element else { return }
            self?.currentAudioControlStatus = AudioControlStatus(playbackStatus: playbackStatus)
        }).disposed(by: disposeBag)

        // Track info
        device.state.outputs.trackInfo().stateObservable().observeOn(MainScheduler.instance)
        .skipOne().subscribe({ [weak self] trackInfoEvent in
            guard let trackInfo = trackInfoEvent.element else { return }
            self?.currentAudioNowPlaying = AudioNowPlaying(trackInfo: trackInfo)
        }).disposed(by: disposeBag)

        // Current audio source
        device.state.outputs.currentAudioSource().stateObservable().observeOn(MainScheduler.instance)
            .skipOne().takeOne().subscribe({ [weak self] currentAudioSourceEvent in
            guard let currentAudioSource = currentAudioSourceEvent.element else { return }
            if self?.auxVisible == (currentAudioSource == .aux) {
                self?.outputAuxVisible.accept(currentAudioSource == .aux)
            } else {
                self?.auxVisible = currentAudioSource == .aux
            }
        }).disposed(by: disposeBag)

        device.state.outputs.currentAudioSource().stateObservable().observeOn(MainScheduler.instance)
        .skipOne().subscribe({ [weak self] currentAudioSourceEvent in
            guard let currentAudioSource = currentAudioSourceEvent.element else { return }
            self?.auxVisible = currentAudioSource == .aux
        }).disposed(by: disposeBag)

        // App mode
        dependencies.appMode.skipOne().observeOn(MainScheduler.instance).subscribe({ [weak self] mode in
            guard let mode = mode.element else { return }
            switch mode {
            case .background:
                OzzyPlayerViewModel.supportedMonitors().forEach { self?.device.state.inputs.stop(monitorType: $0)() }
            case .foreground:
                self?.viewDidLoad()
            }
        }).disposed(by: disposeBag)
    }
    deinit {
        print(#function, String(describing: self))
    }
    func close() {
        closed.onNext(())
    }
}

extension OzzyPlayerViewModel {
    func viewDidLoad() {
        device.state.inputs.requestPlaybackStatus()
        device.state.inputs.requestCurrentTrackInfo()
        device.state.inputs.requestVolume()
        DispatchQueue.main.asyncAfter(deadline: .now() + Const.currentAudioSourceDelay) { [weak self] in
            self?.device.state.inputs.requestCurrentAudioSource()
            OzzyPlayerViewModel.supportedMonitors().forEach { self?.device.state.inputs.start(monitorType: $0)() }
        }
    }
    func nextButtonTouchedUpInside() {
        device.state.inputs.nextTrack()
    }
    func previusButtonTouchedUpInside() {
        device.state.inputs.previousTrack()
    }
    func playPauseButtonTouchedUpInside() {
        switch currentAudioControlStatus {
        case .isPlaying:
            device.state.inputs.pause()
            currentAudioControlStatus = .isPaused
        case .isPaused:
            device.state.inputs.play()
            currentAudioControlStatus = .isPlaying
        }
    }
}
