//
//  RootViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 13/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift

protocol RootViewModelInput {
    func displayMenu()
}

protocol RootViewModelOutput {
    var menuVisible: BehaviorRelay<Void> { get }
}

protocol RootViewModelType {
    var inputs: RootViewModelInput { get }
    var outputs: RootViewModelOutput { get }
}

public final class RootViewModel: RootViewModelInput, RootViewModelOutput, RootViewModelType {
    var inputs: RootViewModelInput { return self }
    var outputs: RootViewModelOutput { return self }

    func displayMenu() {
       self.outputs.menuVisible.accept(())
    }
    
    internal var menuVisible = BehaviorRelay<Void>(value: ())
}
