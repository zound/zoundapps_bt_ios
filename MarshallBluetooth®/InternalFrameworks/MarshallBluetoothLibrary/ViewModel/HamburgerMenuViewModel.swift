//
//  HamburgerMenuViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 20.04.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift

/// Input points of the model of Hamburger menu screen
protocol HamburgerMenuViewModelInput {
    /// Handle tapping on Close button
    func requestClose()
    
    /// Handle tapping on Settings menu
    func settingsTapped()
    
    /// Handle tapping on Help menu
    func helpTapped()
    
    /// Handle tapping on Shop menu
    func shopTapped()
    
    /// Handle tapping on About menu
    func aboutTapped()
}

/// Input points of the model of StayUpdated screen
protocol HamburgerMenuViewModelOutput {
    /// Notify that Close button was tapped
    var closed: BehaviorRelay<Bool> { get set }
    
    /// Notify that Help menu was selected
    var settings: BehaviorRelay<Bool> { get set }
    
    /// Notify that Help menu was selected
    var help: BehaviorRelay<Bool> { get set }
    
    /// Notify that Shop menu was selected
    var shop: BehaviorRelay<Bool> { get set }
    
    /// Notify that About menu was selected
    var about: BehaviorRelay<Bool> { get set }
}

/// Protocol to force input/outputs on view model
protocol HamburgerMenuViewModelType {
    /// Input points of the view model
    var inputs: HamburgerMenuViewModelInput { get }
    
    /// Output points of the view model
    var outputs: HamburgerMenuViewModelOutput { get }
}

/// View model implementation for Hamburger Menu screen
class HamburgerMenuViewModel: HamburgerMenuViewModelInput, HamburgerMenuViewModelOutput, HamburgerMenuViewModelType {
    
    var inputs: HamburgerMenuViewModelInput { return self }
    var outputs: HamburgerMenuViewModelOutput { return self }
    
    /// Notify that Close button is tapped and Hamburger View should disappear
    func requestClose() {
        outputs.closed.accept(true)
    }
    
    /// Notify that Settings button is tapped and menu should be displayed
    func settingsTapped() {
        outputs.settings.accept(true)
    }
    
    /// Notify that Help button is tapped and menu should be displayed
    func helpTapped() {
        outputs.help.accept(true)
    }
    
    /// Notify that Shop button is tapped and menu should be displayed
    func shopTapped() {
        outputs.shop.accept(true)
    }
    
    /// Notify that About button is tapped and menu should be displayed
    func aboutTapped() {
        outputs.about.accept(true)
    }
    
    internal var closed: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    internal var settings: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    internal var help: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    internal var shop: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    internal var about: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
}
