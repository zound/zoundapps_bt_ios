//
//  UnsubscribeConfirmationViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Paprota Przemyslaw on 25/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit


/// Input points of the model for Device Settings
protocol UnsubscribeConfirmationViewModelInput {

    func confirm()

    func cancel()
}

protocol UnsubscribeConfirmationViewModelOutput {
    var confirmed: BehaviorRelay<Void> { get set }
    var canceled: BehaviorRelay<Void> { get set }
}

/// Protocol to force input/outputs on view model
protocol UnsubscribeConfirmationViewModelType {
    /// Input points of the view model
    var inputs: UnsubscribeConfirmationViewModelInput { get }
    /// Output points of the view model
    var outputs: UnsubscribeConfirmationViewModelOutput { get }

}


class UnsubscribeConfirmationViewModel: UnsubscribeConfirmationViewModelType, UnsubscribeConfirmationViewModelOutput, UnsubscribeConfirmationViewModelInput {

    var inputs: UnsubscribeConfirmationViewModelInput { return self }

    var outputs: UnsubscribeConfirmationViewModelOutput { return self }

    var confirmed = BehaviorRelay<Void>(value: ())

    var canceled = BehaviorRelay<Void>(value: ())

    func confirm() {
        confirmed.accept(())
    }

    func cancel() {
        canceled.accept(())
    }
}
