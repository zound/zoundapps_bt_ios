//
//  HamburgerMenuViewModelTests.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 24.04.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

final class HamburgerMenuViewModelTest: XCTestCase {
    var disposeBag: DisposeBag!
    var vm: HamburgerMenuViewModel!
    
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
        vm = HamburgerMenuViewModel()
    }
    
    func testClose() {
        let scheduler = TestScheduler(initialClock: 0)
        
        let closed = scheduler.createObserver(Bool.self)
        vm.outputs.closed.asDriver().drive(closed).disposed(by: disposeBag)
        
        scheduler.start()
        
        // Initial state - user entered hamburger menu screen
        XCTAssertEqual([.next(0, false)], closed.events)
        
        // user tapped on "Close" button
        vm.inputs.requestClose()
        XCTAssertEqual([.next(0, false),
                        .next(0, true)], closed.events)
        
        // that's not possible in reality, but let's assume that user can tap "Close" multiple times in a row
        vm.inputs.requestClose()
        vm.inputs.requestClose()
        vm.inputs.requestClose()
        XCTAssertEqual([.next(0, false),
                        .next(0, true),
                        .next(0, true),
                        .next(0, true),
                        .next(0, true)], closed.events)
    }
    
    func testSettings() {
        let scheduler = TestScheduler(initialClock: 0)
        
        let settings = scheduler.createObserver(Bool.self)
        vm.outputs.settings.asDriver().drive(settings).disposed(by: disposeBag)
        
        scheduler.start()
        
        // Initial state - user entered hamburger menu screen
        XCTAssertEqual([.next(0, false)], settings.events)
        
        // user tapped on "Close" button
        vm.inputs.settingsTapped()
        XCTAssertEqual([.next(0, false),
                        .next(0, true)], settings.events)
        
        // that's not possible in reality, but let's assume that user can tap "Close" multiple times in a row
        vm.inputs.settingsTapped()
        vm.inputs.settingsTapped()
        vm.inputs.settingsTapped()
        XCTAssertEqual([.next(0, false),
                        .next(0, true),
                        .next(0, true),
                        .next(0, true),
                        .next(0, true)], settings.events)
    }
    
    func testHelp() {
        let scheduler = TestScheduler(initialClock: 0)
        
        let help = scheduler.createObserver(Bool.self)
        vm.outputs.help.asDriver().drive(help).disposed(by: disposeBag)
        
        scheduler.start()
        
        // Initial state - user entered hamburger menu screen
        XCTAssertEqual([.next(0, false)], help.events)
        
        // user tapped on "Close" button
        vm.inputs.helpTapped()
        XCTAssertEqual([.next(0, false),
                        .next(0, true)], help.events)
        
        // that's not possible in reality, but let's assume that user can tap "Close" multiple times in a row
        vm.inputs.helpTapped()
        vm.inputs.helpTapped()
        vm.inputs.helpTapped()
        XCTAssertEqual([.next(0, false),
                        .next(0, true),
                        .next(0, true),
                        .next(0, true),
                        .next(0, true)], help.events)
    }
    
    func testShop() {
        let scheduler = TestScheduler(initialClock: 0)
        
        let shop = scheduler.createObserver(Bool.self)
        vm.outputs.shop.asDriver().drive(shop).disposed(by: disposeBag)
        
        scheduler.start()
        
        // Initial state - user entered hamburger menu screen
        XCTAssertEqual([.next(0, false)], shop.events)
        
        // user tapped on "Close" button
        vm.inputs.shopTapped()
        XCTAssertEqual([.next(0, false),
                        .next(0, true)], shop.events)
        
        // that's not possible in reality, but let's assume that user can tap "Close" multiple times in a row
        vm.inputs.shopTapped()
        vm.inputs.shopTapped()
        vm.inputs.shopTapped()
        XCTAssertEqual([.next(0, false),
                        .next(0, true),
                        .next(0, true),
                        .next(0, true),
                        .next(0, true)], shop.events)
    }
    
    func testAbout() {
        let scheduler = TestScheduler(initialClock: 0)
        
        let about = scheduler.createObserver(Bool.self)
        vm.outputs.about.asDriver().drive(about).disposed(by: disposeBag)
        
        scheduler.start()
        
        // Initial state - user entered hamburger menu screen
        XCTAssertEqual([.next(0, false)], about.events)
        
        // user tapped on "Close" button
        vm.inputs.aboutTapped()
        XCTAssertEqual([.next(0, false),
                        .next(0, true)], about.events)
        
        // that's not possible in reality, but let's assume that user can tap "Close" multiple times in a row
        vm.inputs.aboutTapped()
        vm.inputs.aboutTapped()
        vm.inputs.aboutTapped()
        XCTAssertEqual([.next(0, false),
                        .next(0, true),
                        .next(0, true),
                        .next(0, true),
                        .next(0, true)], about.events)
    }
}
