//
//  GoogleAssistantViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Szatkowski Michal on 06/03/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import RxSwift


protocol GoogleAssistantViewModelInput {
    func doneTapped()
}
protocol GoogleAssistantViewModelOutput {
    var done: PublishRelay<Void> { get }
}
public final class GoogleAssistantViewModel: GoogleAssistantViewModelInput, GoogleAssistantViewModelOutput {
    var inputs: GoogleAssistantViewModelInput { return self }
    var outputs: GoogleAssistantViewModelOutput { return self }
    var done = PublishRelay<Void>.init()
    func doneTapped() {
        self.done.accept(())
    }
}

