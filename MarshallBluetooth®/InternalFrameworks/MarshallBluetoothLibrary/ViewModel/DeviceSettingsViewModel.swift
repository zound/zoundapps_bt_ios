//
//  DeviceSettingsViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 09.05.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

/// Default device name that should be displayed if app could not get friendly name from speaker
private let DefaultDeviceName = "JOPLIN SPEAKER"

/// Types of settings submenus present in view
///
/// - about: About This Speaker sub menu
/// - rename: Rename Speaker sub menu
/// - couple: Couple Speakers sub menu
/// - forget: Forget Speaker sub menu
/// - eq: Equalize sub menu
/// - light: Light sub menu
/// - sounds: Sounds sub menu
enum DeviceSettingsItem {
    case aboutThisSpeaker
    case renameSpeaker
    case coupleSpeaker
    case forgetSpeaker
    case equaliser
    case equaliserSettings
    case activeNoiceCancellingSettings
    case autoOffTimer
    case mButton
    case light
    case sounds
}

/// Helper struct to associate UIViews with view model
struct SettingsItem {
    /// Unique type/id of particular settings's item
    var type: DeviceSettingsItem
    /// Name displayed in view
    var name: String
    /// Tells if menu item (with arrow) should be disabled or enabled
    var enable: Bool
    /// Tells cell if should display notification about OTA update (currently true only for .aboutThisSpeaker)
    var updateNotification: Bool
}

/// Input points of the model for Device Settings
protocol DeviceSettingsViewModelInput {
    /// Handle tap on Close
    func closeTapped()
    
    /// Handle tap on settings options
    ///
    /// - Parameter index: row's index from IndexPath
    func menuItemTapped(index: Int)
    
    /// Get name from device
    func refreshName()
    func viewWillAppear()
    func viewWillDisappear()
}

/// Output points of the model for Device Settings
protocol DeviceSettingsViewModelOutput {
    /// User friendy name of device (speaker) taken from device itself via SDK
    var deviceName: BehaviorRelay<String> { get set }
    
    var deviceImage: BehaviorRelay<UIImage> { get }
    
    var updateAvailable: BehaviorRelay<Bool> { get }
    
    /// Notify that view should be closed
    var closed: PublishRelay<Bool> { get set }

    var about: PublishRelay<Bool> {get set }
    var rename: PublishRelay<Bool> { get set }
    var couple: PublishRelay<Bool> { get set }
    var forget: PublishRelay<Bool> { get set }
    var eq: PublishRelay<Bool> { get set }
    var eqSettings: PublishRelay<Bool> { get set }
    var ancSettings: PublishRelay<Bool> { get set }
    var autoOffTimer: PublishRelay<Bool> { get set }
    var mButton: PublishRelay<Bool> { get set }
    var light: PublishRelay<Bool> { get set }
    var sounds: PublishRelay<Bool> { get set }
    
    /// Notify about new data source for view (for ex. UITableView)
    var dataSource: BehaviorRelay<[SettingsItem]> { get set }
}

/// Protocol to force input/outputs on view model
protocol DeviceSettingsViewModelType {
    /// Input points of the view model
    var inputs: DeviceSettingsViewModelInput { get }
    
    /// Output points of the view model
    var outputs: DeviceSettingsViewModelOutput { get }
}

class DeviceSettingsViewModel: DeviceSettingsViewModelInput, DeviceSettingsViewModelOutput, DeviceSettingsViewModelType {
    var inputs: DeviceSettingsViewModelInput { return self }
    var outputs: DeviceSettingsViewModelOutput { return self }
    
    var deviceName = BehaviorRelay<String>.init(value: DefaultDeviceName)
    var updateAvailable = BehaviorRelay<Bool>.init(value: false)
    
    var deviceImage: BehaviorRelay<UIImage>
    
    var closed = PublishRelay<Bool>.init()
    var about = PublishRelay<Bool>.init()
    var rename = PublishRelay<Bool>.init()
    var couple = PublishRelay<Bool>.init()
    var forget = PublishRelay<Bool>.init()
    var eq = PublishRelay<Bool>.init()
    var eqSettings = PublishRelay<Bool>.init()
    var ancSettings = PublishRelay<Bool>.init()
    var autoOffTimer = PublishRelay<Bool>.init()
    var mButton = PublishRelay<Bool>.init()
    var light = PublishRelay<Bool>.init()
    var sounds = PublishRelay<Bool>.init()
    var dataSource = BehaviorRelay<[SettingsItem]>.init(value: [])
    
    private var items: [SettingsItem] = []
    
    private var disposeBag = DisposeBag()
    private var activeDevice: BehaviorRelay<DeviceType>
    
    init(activeDevice: BehaviorRelay<DeviceType>, otaService: OTAServiceType) {
        
        self.activeDevice = activeDevice
        self.deviceImage = BehaviorRelay<UIImage>(value: activeDevice.value.image.mediumImage)
        
        let updateInfoObservable = otaService.outputs.updateAvailableInfo
            .flatMap { [weak self] updateAvailableInfo -> Observable<Bool> in
                guard let strongSelf = self else { return Observable.just(false) }
                let device = strongSelf.activeDevice.value
                guard updateAvailableInfo.contains(where: { $0.deviceID == device.id }) else {
                    return Observable.just(false)
                }
                return Observable.just(true)
        }
        
        updateInfoObservable
            .observeOn(MainScheduler.instance)
            .subscribeNext(weak: self, DeviceSettingsViewModel.refreshUpdateNotification)
            .disposed(by: disposeBag)
    
        activeDevice
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] device in
                self?.setupDevice(device)
            })
            .disposed(by: disposeBag)

    }
    
    deinit {
        print(#function, String(describing: self))
    }
    
    func closeTapped() {
        closed.accept(true)
    }
    
    func menuItemTapped(index: Int) {
        guard items.indices.contains(index) else {
            fatalError("Index should never be outside bounds of <items> (<items> is constant in a lifetime of this app")
        }
        
        switch items[index].type {
        case .aboutThisSpeaker:
            about.accept(true)
        case .renameSpeaker:
            rename.accept(true)
        case .coupleSpeaker:
            couple.accept(true)
        case .forgetSpeaker:
            forget.accept(true)
        case .equaliser:
            eq.accept(true)
        case .equaliserSettings:
            eqSettings.accept(true)
        case .activeNoiceCancellingSettings:
            ancSettings.accept(true)
        case .autoOffTimer:
            autoOffTimer.accept(true)
        case .mButton:
            mButton.accept(true)
        case .light:
            light.accept(true)
        case .sounds:
            sounds.accept(true)
        }
    }
    
    func refreshName() {
        // request getting (custom) name from speaker
        activeDevice.value.state.inputs.refreshName()
    }
    
    private func refreshUpdateNotification(available: Bool) {
        // get supported features
        let supportedFeatures = activeDevice.value.state.outputs.perDeviceTypeFeatures
        
        // rebuild data source (for cells)
        // actually all it does was just update update availability for .aboutThisSpeaker
        setupMenuItems(features: supportedFeatures, updateAvailable: available)
        
        // update data source
        dataSource.accept(items)
        
        // notify if update is available or not (some UI indicators can be shown or hidden depending on that output)
        updateAvailable.accept(available)
    }
    
    private func setupMenuItems(features: [Feature], updateAvailable: Bool = false) {
        // clear current items
        items = []

        if features.contains(.hasDeviceInfo) {
            items.append(SettingsItem(type: .aboutThisSpeaker,
                                      name: Strings.appwide_about(),
                                      enable: true,
                                      updateNotification: updateAvailable ))
        }
        if features.contains(.hasMButton) {
            items.append(SettingsItem(type: .mButton,
                                      name: Strings.device_settings_menu_item_m_button(),
                                      enable: true,
                                      updateNotification: false))
        }
        if features.contains(.hasActiveNoiceCancellingSettings) {
            items.append(SettingsItem(type: .activeNoiceCancellingSettings,
                                      name: Strings.device_settings_menu_item_anc_uc(),
                                      enable: true,
                                      updateNotification: false))
        }
        if features.contains(.hasEqualizerSettings) {
            items.append(SettingsItem(type: .equaliserSettings,
                                      name: Strings.device_settings_menu_item_equaliser(),
                                      enable: true,
                                      updateNotification: false))
        }
        if features.contains(.hasAutoOffTime) {
            items.append(SettingsItem(type: .autoOffTimer,
                                      name: Strings.device_settings_menu_item_auto_off_timer(),
                                      enable: true,
                                      updateNotification: false))
        }
        if features.contains(.hasCustomizableName) {
            let renameTitle = activeDevice.value.hardwareType == .speaker
                            ? Strings.device_settings_menu_item_rename()
                            : Strings.device_settings_menu_item_rename_headphone();
            
            items.append(SettingsItem(type: .renameSpeaker,
                                      name: renameTitle,
                                      enable: true,
                                      updateNotification: false))
        }
        if features.contains(.hasTrueWireless) {
            items.append(SettingsItem(type: .coupleSpeaker,
                                      name: Strings.device_settings_menu_item_couple(),
                                      enable: true,
                                      updateNotification: false))
        }
        items.append(SettingsItem(type: .forgetSpeaker,
                                  name: activeDevice.value.hardwareType.forgetName,
                                  enable: true,
                                  updateNotification: false))
        if features.contains(.hasGraphicalEqualizer) {
            items.append(SettingsItem(type: .equaliser,
                                      name: Strings.device_settings_menu_item_equaliser(),
                                      enable: true,
                                      updateNotification: false))
        }
        if features.contains(.hasLedBrightnessControl) {
            items.append(SettingsItem(type: .light,
                                      name: Strings.device_settings_menu_item_light(),
                                      enable: true,
                                      updateNotification: false))
        }
        if features.contains(.hasSoundsControl) {
            items.append(SettingsItem(type: .sounds,
                                      name: Strings.device_settings_menu_item_sounds(),
                                      enable: true,
                                      updateNotification: false))
        }
    }
    
    private func setupDevice(_ device: DeviceType) {
        // set up the speakers name to display in Settings. Use default generic name if somehow cannot get this from speaker
        device.state.outputs.name()
            .stateObservable()
            .bind(to: deviceName)
            .disposed(by: disposeBag)
        
        deviceImage = BehaviorRelay<UIImage>(value: device.image.mediumImage)
        
        // get supported features
        let state = device.state
        let supportedFeatures = type(of: state.outputs).supportedFeatures
        let outputs = state.outputs
        
        let supportsConnectionInfo = supportedFeatures.contains(.hasConnectivityStatus)
        if supportsConnectionInfo {
            outputs.connectivityStatus().stateObservable()
                .asObservable()
                .map { $0.connected }
                .subscribe(onNext: { [weak self] connected in
                    guard let strongSelf = self else { return }
                    for i in strongSelf.items.indices {
                        // Forget Speaker should be always active, the rest of items depend on device's connectivity
                        strongSelf.items[i].enable = strongSelf.items[i].type == .forgetSpeaker ? true : connected
                    }
                    strongSelf.dataSource.accept(strongSelf.items)
                })
                .disposed(by: disposeBag)
        }

        if device.hardwareType == .headset, supportedFeatures.contains(.hasSwitchableAudioSource) {
            outputs.currentAudioSource().stateObservable()
                .asObservable()
                .map { $0 != .aux }
                .subscribe(onNext: { [weak self] notAux in
                    guard let strongSelf = self else { return }
                    guard outputs.connectivityStatus().stateObservable().value.connected else { return }
                    for i in strongSelf.items.indices {
                        strongSelf.items[i].enable = ![.equaliserSettings, .mButton].contains(strongSelf.items[i].type) ? true : notAux
                    }
                    strongSelf.dataSource.accept(strongSelf.items)
                })
                .disposed(by: disposeBag)
        }

    }
    func startMonitors(_ hardware: HardwareType) {
        let monitors: [Monitor] = hardware == .headset ?
            [.audioSource] :
            [.audioSource]
        monitors.forEach {
            activeDevice.value.state.inputs.start(monitorType: $0)()
        }
    }
    func stopMonitors(_ hardware: HardwareType) {
        let monitors: [Monitor] = hardware == .headset ?
            [.audioSource] :
            [.audioSource]
        monitors.forEach {
            activeDevice.value.state.inputs.stop(monitorType: $0)()
        }
    }
    func viewWillAppear() {
        activeDevice.value.state.inputs.requestCurrentAudioSource()
        startMonitors(activeDevice.value.hardwareType)
    }
    func viewWillDisappear() {
        stopMonitors(activeDevice.value.hardwareType)
    }
}

private extension HardwareType {
    var forgetName: String {
        switch self {
        case .speaker:
            return Strings.device_settings_menu_item_forget()
        case .headset:
            return Strings.device_settings_menu_item_forget_headphone()
        }
    }
}

