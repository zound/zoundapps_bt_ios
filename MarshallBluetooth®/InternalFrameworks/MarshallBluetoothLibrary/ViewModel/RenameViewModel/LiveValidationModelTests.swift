//
//  LiveValidationModelTests.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Bartosz Dolewski on 17.06.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

fileprivate let limit = 17
fileprivate var mockTraits = PlatformTraits()

fileprivate final class DummyDeviceState: DeviceStateType {
    
    public var inputs: DeviceStateInput { return self }
    public var outputs: DeviceStateOutput { return self }
    
    public static let supportedFeatures: [Feature] = [.hasCustomizableName]
    public let perDeviceTypeFeatures: [Feature] = [.hasCustomizableName]
    
    init() {
        _name = AnyState<String>(feature: .hasCustomizableName,
                                 supportsNotifications: false,
                                 initialValue: { return "" })
    }
    
    fileprivate let _name: AnyState<String>
    fileprivate let disposeBag = DisposeBag()
}

extension DummyDeviceState: DeviceStateInput {
    public func refreshName() {
        _name.stateObservable().accept("")
    }
}

extension DummyDeviceState: DeviceStateOutput {
    public var activeMonitors: Set<Monitor> {
        get { return  Set<Monitor>() }
        set(newValue) {}
    }
    
    public func name() -> AnyState<String> {
        return _name
    }
}

fileprivate class DummyDevice: DeviceType {
    func bleConnect() -> Single<Void> {
        return Single.just(())
    }
    
    var mac: MAC?
    var configured = false
    var hardwareType: HardwareType = .speaker
    
    func decouple() -> Single<Void> {
        return Single.just(())
    }
    
    func resetSlave() -> Single<Void> {
        return Single.just(())
    }

    
    func initialSetup(with storedEntry: StoredDeviceInfoEntry?) -> Single<String> {
        return Single.just(String())
    }
    
    var modelName: String {
        return String()
    }
    var image: DeviceImageProviding = DeviceBlankImageProvider()

    var connectionTimeout: RxTimeInterval {
        return RxTimeInterval.seconds(5.0)
    }
    
    var name: BehaviorRelay<String>
    var id: String { return String(describing: DummyDevice.self) }
    var imageName: String { return String(describing: DummyDevice.self) }
    
    let platform = Platform(id: "joplinBT", traits: mockTraits)
    
    var state: DeviceStateType
    var otaAdapter: OTAAdapterType?

    init(name: BehaviorRelay<String>,
         state: DeviceStateType,
         otaAdapter: OTAAdapterType? = nil) {
        self.name = name
        self.state = state
        self.otaAdapter = otaAdapter
    }
}

final class LiveValidationModelTests: XCTestCase {
    var disposeBag: DisposeBag!
    var vm: LiveValidationModelType!
    
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
    }
    
    func setupMockDevice() {
        mockTraits = PlatformTraits(volumeMin: 10,
                                    volumeMax: 11,
                                    connectionTimeout: 12,
                                    nameLengthLimit: 17,
                                    nameValidator: StockNameValidator() )
        
        let mockConnectedDevice = DummyDevice(name: BehaviorRelay<String>(value: String(describing: DummyDevice.self)),
                                              state: DummyDeviceState(),
                                              otaAdapter: nil)
        vm = LiveValidationModel(device: mockConnectedDevice)
    }
    
    func testInit() {
        setupMockDevice()
        
        let testScheduler = TestScheduler(initialClock: TestTime())
        let result = testScheduler.createObserver(LiveValidationResult.self)
        
        vm.outputs.result.subscribe(result).disposed(by: disposeBag)
        XCTAssertTrue(result.events.isEmpty)
    }
    
    func testUnderLimit() {
        setupMockDevice()
        
        let testScheduler = TestScheduler(initialClock: TestTime())
        let result = testScheduler.createObserver(LiveValidationResult.self)
        let testInput = String(repeating: "X", count: limit - 6)
        let expected = limit - testInput.utf8.count
        
        vm.outputs.result.subscribe(result).disposed(by: disposeBag)
        vm.inputs.validate(name: testInput)
        
        XCTAssertEqual([.next(TestTime(), .under(left: expected))], result.events)
    }
    
    func testMaxLimit() {
        setupMockDevice()
        
        let testScheduler = TestScheduler(initialClock: TestTime())
        let result = testScheduler.createObserver(LiveValidationResult.self)
        let testInput = String(repeating: "X", count: limit)
        
        vm.outputs.result.subscribe(result).disposed(by: disposeBag)
        vm.inputs.validate(name: testInput)
        
        XCTAssertEqual([.next(TestTime(), .exact)], result.events)
    }
    
    func testAboveLimit() {
        setupMockDevice()
        
        let testScheduler = TestScheduler(initialClock: TestTime())
        let result = testScheduler.createObserver(LiveValidationResult.self)
        let testInput = String(repeating: "X", count: limit + 1)
        
        vm.outputs.result.subscribe(result).disposed(by: disposeBag)
        vm.inputs.validate(name: testInput)
        
        XCTAssertEqual([.next(TestTime(), .above)], result.events)
    }
    
    func testEmptyInput() {
        setupMockDevice()
        
        let testScheduler = TestScheduler(initialClock: TestTime())
        let result = testScheduler.createObserver(LiveValidationResult.self)
        let testInput = ""
        
        vm.outputs.result.subscribe(result).disposed(by: disposeBag)
        vm.inputs.validate(name: testInput)
        
        XCTAssertEqual([.next(TestTime(), .under(left: limit))], result.events)
    }
    
    func testLiveValidationResultsComparison() {
        var lhs = LiveValidationResult.above
        var rhs = LiveValidationResult.above
        XCTAssertEqual(lhs, rhs)
        
        lhs = .exact
        rhs = .exact
        XCTAssertEqual(lhs, rhs)
        
        lhs = .under(left: 10)
        rhs = .under(left: 10)
        XCTAssertEqual(lhs, rhs)
        
        
        lhs = .under(left: 10)
        rhs = .under(left: 11)
        XCTAssertNotEqual(lhs, rhs)
        
        lhs = .under(left: 10)
        rhs = .exact
        XCTAssertNotEqual(lhs, rhs)
        
        lhs = .above
        rhs = .exact
        XCTAssertNotEqual(lhs, rhs)
    }
}
