//
//  CustomNameValidationModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 17.06.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift
import GUMA

protocol CustomNameValidationModelInput {
    func validate(name: String)
}

protocol CustomNameValidationModelOutput {
    var result: PublishSubject<CustomNameValidationResult> { get }
}

protocol CustomNameValidationModelType {
    var inputs: CustomNameValidationModelInput { get }
    var outputs: CustomNameValidationModelOutput { get }
}

class CustomNameValidationModel: CustomNameValidationModelInput, CustomNameValidationModelOutput {

    var result = PublishSubject<CustomNameValidationResult>.init()
    
    private var currentName = PublishSubject<String>.init()
    
    private let modelDevice: DeviceType
    private var disposeBag = DisposeBag()
    
    init(device: DeviceType) {
        modelDevice = device
        
        let parser = Observable.just(modelDevice.platform.traits.nameValidator)
        Observable.combineLatest(currentName, parser)
            .map{ $1.validate(name: $0) }
            .bind(to: result)
            .disposed(by: disposeBag)
    }
    
    func validate(name: String) {
        currentName.onNext(name)
    }
}

extension CustomNameValidationModel: CustomNameValidationModelType {
    var inputs: CustomNameValidationModelInput { return self }
    var outputs: CustomNameValidationModelOutput { return self }
}
