//
//  RenameViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 08.06.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift
import GUMA

/// Default device name that should be displayed if app could not get friendly name from speaker
private let DefaultDeviceName = "JOPLIN SPEAKER"

/// Input points of the model for Rename View model
protocol RenameViewModelInput {
    /// Change speaker's name (to be validated)
    ///
    /// - Parameter name: new speaker name
    func rename(with: String)
    
    /// Handle the tap on the Rename button
    func renameButtonTapped()
    
    /// Get name from device
    func refreshName()
}

/// Output points of the model for Rename View model
protocol RenameViewModelOutput {
    /// User friendy name of device (speaker) taken from device itself via SDK
    var displayDeviceName: BehaviorRelay<String> { get }
    
    var deviceImage: BehaviorRelay<UIImage> { get }
    
    /// Notify when rename button can be visible
    var renameButtonVisible: BehaviorRelay<Bool> { get }
    
    var liveValidationResult: PublishSubject<LiveValidationResult> { get }
    
    /// Notify if view is displayed (active)
    var active: BehaviorRelay<Bool> { get }
    
    /// Notify when name validation failed and user neeeds to re-enter proper one
    var wrongNameIndicatorVisible: BehaviorRelay<Bool> { get }
    
    var nameLimit: Int { get }
    
    var isSpeaker: Bool { get }
}

/// Protocol to force input/outputs on view model
protocol RenameViewModelType {
    /// Input points of the view model
    var inputs: RenameViewModelInput { get }
    
    /// Output points of the view model
    var outputs: RenameViewModelOutput { get }
}

/// View model implementation
class RenameViewModel: RenameViewModelInput, RenameViewModelOutput {
    typealias Dependencies = AnalyticsServiceDependency
    
    var displayDeviceName = BehaviorRelay<String>.init(value: DefaultDeviceName)
    var renameButtonVisible = BehaviorRelay<Bool>.init(value: false)
    var liveValidationResult = PublishSubject<LiveValidationResult>.init()
    var active = BehaviorRelay<Bool>.init(value: true)
    var wrongNameIndicatorVisible = BehaviorRelay<Bool>.init(value: false)
    var deviceImage: BehaviorRelay<UIImage>
    
    var isSpeaker: Bool {
        return modelDevice.hardwareType == .speaker
    }
    
    var nameLimit: Int {
            return modelDevice.platform.traits.nameLengthLimit
    }
    
    private let modelDevice: DeviceType
    private let dependencies: Dependencies
    private let liveValidation: LiveValidationModelType
    private let customNameValidation: CustomNameValidationModelType
    private var currentName = BehaviorRelay<String>.init(value: "")
    
    private var disposeBag = DisposeBag()
    
    init(device: DeviceType, dependencies: Dependencies) {
        modelDevice = device
        self.dependencies = dependencies
        liveValidation = LiveValidationModel(device: modelDevice)
        customNameValidation = CustomNameValidationModel(device: modelDevice)
        
        deviceImage = BehaviorRelay<UIImage>(value: modelDevice.image.mediumImage)
        
        // set up the speakers name to display in Settings. Use default generic name if somehow cannot get this from speaker
        modelDevice.state.outputs.name()
            .stateObservable()
            .bind(to: displayDeviceName)
            .disposed(by: disposeBag)
        
        setUpCustomNameValidation()
        setUpLiveValidation()
    }
    
    deinit {
        print(#function, String(describing: self))
    }
    
    func rename(with: String) {
        currentName.accept(with)
    }
    
    func renameButtonTapped() {
        let newName = currentName.value.trimmingCharacters(in: .whitespacesAndNewlines)
        dependencies.analyticsService.inputs.log(.appSpeakerNameChanged, associatedWith: modelDevice.modelName)
        modelDevice.state.inputs.rename(with: newName)
        active.accept(false)
        
        refreshName()
    }
    
    func refreshName() {
        // request getting (custom) name from speaker
        modelDevice.state.inputs.refreshName()
    }
    
    private func visible(rename: Bool, warning: Bool) {
        renameButtonVisible.accept(rename)
        wrongNameIndicatorVisible.accept(warning)
    }
    
    private func setUpLiveValidation() {
        currentName
            .subscribe(onNext: { [weak self] name in
                self?.liveValidation.inputs.validate(name: name)
            })
            .disposed(by: disposeBag)
        
        liveValidation.outputs.result
            .bind(to: liveValidationResult)
            .disposed(by: disposeBag)
    }
    
    private func setUpCustomNameValidation() {
        currentName
            .subscribe(onNext: { [weak self] name in
                self?.customNameValidation.inputs.validate(name: name)
            })
            .disposed(by: disposeBag)
        
        customNameValidation.outputs.result
            .subscribe(onNext: {[weak self] result in
                switch result {
                case .pass:
                    self?.visible(rename: true, warning: false)
                case .empty:
                    self?.visible(rename: false, warning: false)
                case .whiteCharactersOnly:
                    self?.visible(rename: false, warning: true)
                case .tooLong:
                    self?.visible(rename: false, warning: true)
                }
            })
            .disposed(by: disposeBag)
    }
}

// MARK: - Inputs and Outputs for RenameViewModel
extension RenameViewModel: RenameViewModelType {
    var inputs: RenameViewModelInput { return self }
    var outputs: RenameViewModelOutput { return self }
}
