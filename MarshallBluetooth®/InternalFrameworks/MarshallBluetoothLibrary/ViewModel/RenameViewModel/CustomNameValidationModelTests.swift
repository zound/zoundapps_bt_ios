//
//  CustomNameValidationModelTests.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Bartosz Dolewski on 17.06.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

fileprivate let limit = 17
fileprivate var mockTraits = PlatformTraits()

fileprivate struct AlwaysPassValidator: CustomNameValidator {
    func validate(name: String) -> CustomNameValidationResult {
        // everything is fine for this parser
        return .pass
    }
}

fileprivate struct AlwaysEmptyValidator: CustomNameValidator {
    func validate(name: String) -> CustomNameValidationResult {
        // everything is bad for this parser
        return .empty
    }
}

fileprivate struct AlwaysWhiteCharactesrValidator: CustomNameValidator {
    func validate(name: String) -> CustomNameValidationResult {
        // everything contains white characters for this parser
        return .whiteCharactersOnly
    }
}

fileprivate struct AlwaysTooLongValidator: CustomNameValidator {
    func validate(name: String) -> CustomNameValidationResult {
        // everything is too long for this parser
        return .tooLong
    }
}

fileprivate struct GenericBluetoothNameValidator: CustomNameValidator {
    func validate(name: String) -> CustomNameValidationResult {
        
        // empty name is obviously not acceptable
        guard name.isEmpty == false else {
            return .empty
        }
        
        // name that contains only white characters (for example only spaces) is not acceptable
        guard name.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == false else {
            return .whiteCharactersOnly
        }
        
        return name.utf8.count <= limit ? .pass : .tooLong
    }
}
fileprivate final class DummyDeviceState: DeviceStateType {
    
    public var inputs: DeviceStateInput { return self }
    public var outputs: DeviceStateOutput { return self }
    
    public static let supportedFeatures: [Feature] = [.hasCustomizableName]
    public let perDeviceTypeFeatures: [Feature] = [.hasCustomizableName]
    
    init() {
        self._name = AnyState<String>(feature: .hasCustomizableName,
                                      supportsNotifications: false,
                                      initialValue: { return "" })
    }
    
    fileprivate let _name: AnyState<String>
    fileprivate let disposeBag = DisposeBag()
}

extension DummyDeviceState: DeviceStateInput {
    public func refreshName() {
    }
}

extension DummyDeviceState: DeviceStateOutput {
    var activeMonitors: Set<Monitor> {
        get { return  Set<Monitor>() }
        set(newValue) {}
    }
    
    public func name() -> AnyState<String> {
        return _name
    }
}

fileprivate class DummyDevice: DeviceType {
    func bleConnect() -> Single<Void> {
        return Single.just(())
    }
    
    var mac: MAC?
    var configured = false
    
    var hardwareType: HardwareType = .speaker
    
    func decouple() -> Single<Void> {
        return Single.just(())
    }
    
    func resetSlave() -> Single<Void> {
        return Single.just(())
    }

    
    func initialSetup(with storedEntry: StoredDeviceInfoEntry?) -> Single<String> {
        return Single.just(String())
    }
    
    var modelName: String {
        return String()
    }
    var image: DeviceImageProviding = DeviceBlankImageProvider()
    
    var connectionTimeout: RxTimeInterval {
        return RxTimeInterval.seconds(5.0)
    }
    
    var name: BehaviorRelay<String>
    var id: String { return String(describing: DummyDevice.self) }
    var imageName: String { return String(describing: DummyDevice.self) }
    
    let platform = Platform(id: "joplinBT", traits: mockTraits)
    
    var state: DeviceStateType
    var otaAdapter: OTAAdapterType?

    init(name: BehaviorRelay<String>,
         state: DeviceStateType,
         otaAdapter: OTAAdapterType? = nil) {
        self.name = name
        self.state = state
        self.otaAdapter = otaAdapter
    }
}

final class CustomNameValidationModelTests: XCTestCase {
    var disposeBag: DisposeBag!
    var vm: CustomNameValidationModelType!
    
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
    }
    
    func setupValidator(parser: CustomNameValidator) {
        mockTraits = PlatformTraits(volumeMin: 10,
                                    volumeMax: 11,
                                    connectionTimeout: 12,
                                    nameLengthLimit: 17,
                                    nameValidator: parser )
        
        let mockDevice = DummyDevice(name: BehaviorRelay<String>(value: String(describing: DummyDevice.self)),
                                     state: DummyDeviceState(),
                                     otaAdapter: nil)
        vm = CustomNameValidationModel(device: mockDevice)
    }
    
    func testInit() {
        setupValidator(parser: AlwaysPassValidator())
        
        let testScheduler = TestScheduler(initialClock: TestTime())
        let result = testScheduler.createObserver(CustomNameValidationResult.self)
        
        vm.outputs.result.subscribe(result).disposed(by: disposeBag)
        XCTAssertTrue(result.events.isEmpty)
    }
    
    func testAlwaysPass() {
        setupValidator(parser: AlwaysPassValidator())
        
        let testScheduler = TestScheduler(initialClock: TestTime())
        let testInput = "X"
        let result = testScheduler.createObserver(CustomNameValidationResult.self)
        
        vm.outputs.result.subscribe(result).disposed(by: disposeBag)
        vm.inputs.validate(name: testInput)
        XCTAssertEqual([.next(TestTime(), .pass)], result.events)
    }
    
    func testAlwaysEmpty() {
        setupValidator(parser: AlwaysEmptyValidator())
        
        let testScheduler = TestScheduler(initialClock: TestTime())
        let testInput = "X"
        let result = testScheduler.createObserver(CustomNameValidationResult.self)
        
        vm.outputs.result.subscribe(result).disposed(by: disposeBag)
        vm.inputs.validate(name: testInput)
        XCTAssertEqual([.next(TestTime(), .empty)], result.events)
    }
    
    func testAlwaysWhiteCharacters() {
        setupValidator(parser: AlwaysWhiteCharactesrValidator())
        
        let testScheduler = TestScheduler(initialClock: TestTime())
        let testInput = "X"
        let result = testScheduler.createObserver(CustomNameValidationResult.self)
        
        vm.outputs.result.subscribe(result).disposed(by: disposeBag)
        vm.inputs.validate(name: testInput)
        XCTAssertEqual([.next(TestTime(), .whiteCharactersOnly)], result.events)
    }
    
    func testAlwaysTooLong() {
        setupValidator(parser: AlwaysTooLongValidator())
        
        let testScheduler = TestScheduler(initialClock: TestTime())
        let testInput = "X"
        let result = testScheduler.createObserver(CustomNameValidationResult.self)
        
        vm.outputs.result.subscribe(result).disposed(by: disposeBag)
        vm.inputs.validate(name: testInput)
        XCTAssertEqual([.next(TestTime(), .tooLong)], result.events)
    }
    
    func testPass() {
        setupValidator(parser: GenericBluetoothNameValidator())
        
        let testScheduler = TestScheduler(initialClock: TestTime())
        let testInput = String("ABC")
        let result = testScheduler.createObserver(CustomNameValidationResult.self)
        
        vm.outputs.result.subscribe(result).disposed(by: disposeBag)
        vm.inputs.validate(name: testInput)
        XCTAssertEqual([.next(TestTime(), .pass)], result.events)
    }
    
    func testEmpty() {
        setupValidator(parser: GenericBluetoothNameValidator())
        
        let testScheduler = TestScheduler(initialClock: TestTime())
        let testInput = String("")
        let result = testScheduler.createObserver(CustomNameValidationResult.self)
        
        vm.outputs.result.subscribe(result).disposed(by: disposeBag)
        vm.inputs.validate(name: testInput)
        XCTAssertEqual([.next(TestTime(), .empty)], result.events)
    }

    func testWhiteCharacters() {
        setupValidator(parser: GenericBluetoothNameValidator())
        
        let testScheduler = TestScheduler(initialClock: TestTime())
        let testInput = String("    ")
        let result = testScheduler.createObserver(CustomNameValidationResult.self)
        
        vm.outputs.result.subscribe(result).disposed(by: disposeBag)
        vm.inputs.validate(name: testInput)
        XCTAssertEqual([.next(TestTime(), .whiteCharactersOnly)], result.events)
    }
    
    func testTooLong() {
        setupValidator(parser: GenericBluetoothNameValidator())
        
        let testScheduler = TestScheduler(initialClock: TestTime())
        let testInput = String(repeating: "🍔", count: 10)
        let result = testScheduler.createObserver(CustomNameValidationResult.self)
        
        vm.outputs.result.subscribe(result).disposed(by: disposeBag)
        vm.inputs.validate(name: testInput)
        XCTAssertEqual([.next(TestTime(), .tooLong)], result.events)
    }
}
