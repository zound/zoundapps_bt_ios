//
//  LiveVadlidationModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 17.06.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift
import GUMA

enum LiveValidationResult {
    case under(left: Int)
    case exact
    case above
}

protocol LiveValidationModelInput {
    func validate(name: String)
}

protocol LiveValidationModelOutput {
    var result: PublishSubject<LiveValidationResult> { get }
}

protocol LiveValidationModelType {
    var inputs: LiveValidationModelInput { get }
    var outputs: LiveValidationModelOutput { get }
}

class LiveValidationModel: LiveValidationModelInput, LiveValidationModelOutput {

    var result = PublishSubject<LiveValidationResult>.init()
    
    private var currentName = PublishSubject<String>.init()
    
    private let modelDevice: DeviceType
    private var disposeBag = DisposeBag()
    
    init(device: DeviceType) {
        modelDevice = device
        let lengthLimit = Observable.just(modelDevice.platform.traits.nameLengthLimit)
        
        Observable.combineLatest(lengthLimit, currentName)
            .map { $0 - $1.utf8.count }
            .map {
                switch $0 {
                case Int.min..<0: return .above
                case 0: return .exact
                default: return .under(left: $0)
                }
            }
            .bind(to: result)
            .disposed(by: disposeBag)
    }
    
    func validate(name: String) {
        currentName.onNext(name)
    }
}

extension LiveValidationModel: LiveValidationModelType {
    var inputs: LiveValidationModelInput { return self }
    var outputs: LiveValidationModelOutput { return self }
}

extension LiveValidationResult: Equatable {
    public static func ==(lhs: LiveValidationResult, rhs: LiveValidationResult) -> Bool {
        switch (lhs, rhs) {
        case (.exact, .exact), (.above, .above): return true
        case let (.under(left: a), .under(left: b)): return a == b
        default: return false
        }
    }
}
