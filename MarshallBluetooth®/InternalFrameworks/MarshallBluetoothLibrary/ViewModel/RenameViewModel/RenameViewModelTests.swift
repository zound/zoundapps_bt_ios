//
//  RenameViewModelTests.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Dolewski Bartosz A (Ext) on 12.06.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

fileprivate let limit = 17
fileprivate var mockTraits = PlatformTraits(volumeMin: 10,
                                            volumeMax: 11,
                                            connectionTimeout: 12,
                                            nameLengthLimit: 17,
                                            nameValidator: AlwaysPassValidator() )

fileprivate let initialName = "Initial Name"
fileprivate let refreshedName = "Refreshed Name"

fileprivate struct AlwaysPassValidator: CustomNameValidator {
    func validate(name: String) -> CustomNameValidationResult {
        // everything is fine for this parser
        return .pass
    }
}

fileprivate struct AlwaysEmptyValidator: CustomNameValidator {
    func validate(name: String) -> CustomNameValidationResult {
        // everything is bad for this parser
        return .empty
    }
}

fileprivate struct AlwaysWhiteCharactesrValidator: CustomNameValidator {
    func validate(name: String) -> CustomNameValidationResult {
        // everything is bad for this parser
        return .whiteCharactersOnly
    }
}

fileprivate struct AlwaysTooLongValidator: CustomNameValidator {
    func validate(name: String) -> CustomNameValidationResult {
        // everything is bad for this parser
        return .tooLong
    }
}

// **** Mocked Device state for: connected, initialized with correct data ********
fileprivate final class DummyDeviceState: DeviceStateType {
    
    public var inputs: DeviceStateInput { return self }
    public var outputs: DeviceStateOutput { return self }
    
    public static let supportedFeatures: [Feature] = [.hasConnectivityStatus, .hasCustomizableName]
    public let perDeviceTypeFeatures: [Feature] = [.hasConnectivityStatus, .hasCustomizableName]
    
    init(replyWithCustomName: Bool = true) {
        self.replyWithCustomName = replyWithCustomName
        
        self._name = AnyState<String>(feature: .hasCustomizableName,
                                      supportsNotifications: false,
                                      initialValue: { return initialName })
        self._connectedState = AnyState<Bool>(feature: .hasConnectivityStatus, supportsNotifications: false, initialValue: { return true })
    }
    
    fileprivate let _name: AnyState<String>
    fileprivate let _connectedState: AnyState<Bool>
    fileprivate var replyWithCustomName = true
    fileprivate let disposeBag = DisposeBag()
}

extension DummyDeviceState: DeviceStateInput {
    
    public func setConnectedState(flag: Bool) {
        _connectedState.stateObservable().accept(flag)
    }
    
    public func refreshName() {
        if(replyWithCustomName) {
            _name.stateObservable().accept(refreshedName)
        }
    }
    
    public func rename(with: String) {
    }
}

extension DummyDeviceState: DeviceStateOutput {
    public var activeMonitors: Set<Monitor> {
        get { return  Set<Monitor>() }
        set(newValue) {}
    }
    public func connectedState() -> AnyState<Bool> {
        return _connectedState
    }
    
    public func name() -> AnyState<String> {
        return _name
    }
}

// **** Mocked Device state for: connected, initialized with nil data ********
fileprivate final class DummyNilDeviceState: DeviceStateType {
    
    public var inputs: DeviceStateInput { return self }
    public var outputs: DeviceStateOutput { return self }
    
    public static let supportedFeatures: [Feature] = [.hasConnectivityStatus, .hasCustomizableName]
    public let perDeviceTypeFeatures: [Feature] = [.hasConnectivityStatus, .hasCustomizableName]
    
    init(replyWithCustomName: Bool = true) {
        self.replyWithCustomName = replyWithCustomName
        
        self._connectedState = AnyState<Bool>(feature: .hasConnectivityStatus,
                                              supportsNotifications: false,
                                              initialValue: { return true })
        
        self._name = AnyState<String>(feature: .hasCustomizableName,
                                      supportsNotifications: false,
                                      initialValue: { return initialName })
    }
    
    fileprivate let _name: AnyState<String>
    fileprivate let _connectedState: AnyState<Bool>
    fileprivate var replyWithCustomName = true
    fileprivate let disposeBag = DisposeBag()
}

extension DummyNilDeviceState: DeviceStateInput {
    
    public func setConnectedState(flag: Bool) {
        _connectedState.stateObservable().accept(flag)
    }
    
    public func refreshName() {
        if(replyWithCustomName) {
            _name.stateObservable().accept(refreshedName)
        }
    }
    
    public func rename(with: String) {
    }
}

extension DummyNilDeviceState: DeviceStateOutput {
    public var activeMonitors: Set<Monitor> {
        get { return  Set<Monitor>() }
        set(newValue) {}
    }
    
    
    public func connectedState() -> AnyState<Bool> {
        return _connectedState
    }
    
    public func name() -> AnyState<String> {
        return _name
    }
}

fileprivate class DummyDevice: DeviceType {
    func bleConnect() -> Single<Void> {
        return Single.just(())
    }
    
    var mac: MAC?
    var configured = false
    var hardwareType: HardwareType = .speaker
    
    func decouple() -> Single<Void> {
        return Single.just(())
    }
    
    func resetSlave() -> Single<Void> {
        return Single.just(())
    }
    
    func initialSetup(with storedEntry: StoredDeviceInfoEntry?) -> Single<String> {
        return Single.just(String())
    }
    
    var modelName: String {
        return String()
    }
    var image: DeviceImageProviding = DeviceBlankImageProvider()
    
    var connectionTimeout: RxTimeInterval {
        return RxTimeInterval.seconds(5.0)
    }
    
    var name: BehaviorRelay<String>
    var id: String { return String(describing: DummyDevice.self) }
    var imageName: String { return String(describing: DummyDevice.self) }
    
    let platform = Platform(id: "joplinBT", traits: mockTraits)
    
    var state: DeviceStateType
    var otaAdapter: OTAAdapterType?

    init(name: BehaviorRelay<String>,
         state: DeviceStateType,
         otaAdapter: OTAAdapterType? = nil) {
        self.name = name
        self.state = state
        self.otaAdapter = otaAdapter
    }
}

final class RenameViewModelTests: XCTestCase {
    var disposeBag: DisposeBag!
    var vm_connected: RenameViewModelType!
    
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
    }
    
    func setUpWithCustomNameResponse() {
        let mockConnectedDevice = DummyDevice(name: BehaviorRelay<String>(value: String(describing: DummyDevice.self)),
                                              state: DummyDeviceState(replyWithCustomName: true),
                                              otaAdapter: nil)
        vm_connected = RenameViewModel(device: mockConnectedDevice, dependencies: MockedAnalyticsServiceDependency())
    }
    
    func setUpWithNoCustomNameResponse() {
        let mockConnectedDevice = DummyDevice(name: BehaviorRelay<String>(value: String(describing: DummyDevice.self)),
                                              state: DummyDeviceState(replyWithCustomName: false),
                                              otaAdapter: nil)
        vm_connected = RenameViewModel(device: mockConnectedDevice, dependencies: MockedAnalyticsServiceDependency())
    }
    
    func testReadingDeviceNameCustomNameResponse() {
        setUpWithCustomNameResponse()
        
        let testScheduler = TestScheduler(initialClock: TestTime())
        let deviceName = testScheduler.createObserver(String.self)
        
        vm_connected.outputs.displayDeviceName.asObservable().subscribe(deviceName).disposed(by: disposeBag)
        XCTAssertEqual([.next(TestTime(), initialName)], deviceName.events)
        
        vm_connected.inputs.refreshName()
        XCTAssertEqual([.next(TestTime(), initialName),
                        .next(TestTime(), refreshedName)], deviceName.events)
        
        vm_connected.inputs.refreshName()
        XCTAssertEqual([.next(TestTime(), initialName),
                        .next(TestTime(), refreshedName),
                        .next(TestTime(), refreshedName)], deviceName.events)
    }
    
    func testReadingDeviceNameCustomNameNotAvailable() {
        setUpWithNoCustomNameResponse()
        
        let testScheduler = TestScheduler(initialClock: TestTime())
        let deviceNameNoCustom = testScheduler.createObserver(String.self)
        let deviceNameWithCustom = testScheduler.createObserver(String.self)
        
        vm_connected.outputs.displayDeviceName.asObservable().subscribe(deviceNameNoCustom).disposed(by: disposeBag)
        XCTAssertEqual([.next(TestTime(), initialName)], deviceNameNoCustom.events)
        
        vm_connected.inputs.refreshName()
        XCTAssertEqual([.next(TestTime(), initialName)], deviceNameNoCustom.events)
        
        vm_connected.inputs.refreshName()
        XCTAssertEqual([.next(TestTime(), initialName)], deviceNameNoCustom.events)
        
        setUpWithCustomNameResponse()
        vm_connected.outputs.displayDeviceName.asObservable().subscribe(deviceNameWithCustom).disposed(by: disposeBag)
        vm_connected.inputs.refreshName()
        XCTAssertEqual([.next(TestTime(), initialName),
                        .next(TestTime(), refreshedName)], deviceNameWithCustom.events)
    }
    
    func testReadingCustomNameFromDeviceWithNilInitialData() {
        setUpWithNoCustomNameResponse()
        
        let testScheduler = TestScheduler(initialClock: TestTime())
        let deviceName = testScheduler.createObserver(String.self)
        
        let mockConnectedDevice = DummyDevice(name: BehaviorRelay<String>(value: String(describing: DummyDevice.self)),
                                              state: DummyNilDeviceState(replyWithCustomName: true),
                                              otaAdapter: nil)
        
        vm_connected = RenameViewModel(device: mockConnectedDevice, dependencies: MockedAnalyticsServiceDependency())
        
        vm_connected.outputs.displayDeviceName.asObservable().subscribe(deviceName).disposed(by: disposeBag)
        
        XCTAssertEqual([.next(TestTime(), initialName)], deviceName.events)
    }
    
    func testRenameWarningVisibilityInit() {
        let testScheduler = TestScheduler(initialClock: TestTime())
        let renameVisible = testScheduler.createObserver(Bool.self)
        let warningVisible = testScheduler.createObserver(Bool.self)
        
        let mockConnectedDevice = DummyDevice(name: BehaviorRelay<String>(value: String(describing: DummyDevice.self)),
                                              state: DummyDeviceState(replyWithCustomName: true),
                                              otaAdapter: nil)
        
        vm_connected = RenameViewModel(device: mockConnectedDevice, dependencies: MockedAnalyticsServiceDependency())
        
        vm_connected.outputs.renameButtonVisible.asObservable().subscribe(renameVisible).disposed(by: disposeBag)
        vm_connected.outputs.wrongNameIndicatorVisible.asObservable().subscribe(warningVisible).disposed(by: disposeBag)
        
        // users enters the screen, none of indicators (rename button, warning triangle) should be visible
        XCTAssertEqual([.next(TestTime(), false)], renameVisible.events)
        XCTAssertEqual([.next(TestTime(), false)], warningVisible.events)
    }
    
    func testRenameWarningVisibilityPass() {
        let testScheduler = TestScheduler(initialClock: TestTime())
        let renameVisible = testScheduler.createObserver(Bool.self)
        let warningVisible = testScheduler.createObserver(Bool.self)
        
        mockTraits = PlatformTraits(volumeMin: 10,
                                    volumeMax: 11,
                                    connectionTimeout: 12,
                                    nameLengthLimit: 17,
                                    nameValidator: AlwaysPassValidator())
        
        let mockConnectedDevice = DummyDevice(name: BehaviorRelay<String>(value: String(describing: DummyDevice.self)),
                                              state: DummyDeviceState(replyWithCustomName: true),
                                              otaAdapter: nil)
        
        vm_connected = RenameViewModel(device: mockConnectedDevice, dependencies: MockedAnalyticsServiceDependency())
        
        vm_connected.outputs.renameButtonVisible.asObservable().subscribe(renameVisible).disposed(by: disposeBag)
        vm_connected.outputs.wrongNameIndicatorVisible.asObservable().subscribe(warningVisible).disposed(by: disposeBag)
        
        vm_connected.inputs.rename(with: "NEW NAME")
        
        // casuall parsing user's new name
        XCTAssertEqual([.next(TestTime(), false),
                        .next(TestTime(), true)], renameVisible.events)
        XCTAssertEqual([.next(TestTime(), false),
                        .next(TestTime(), false)], warningVisible.events)
    }
    
    func testRenameWarningVisibilityFailEmpty() {
        let testScheduler = TestScheduler(initialClock: TestTime())
        let renameVisible = testScheduler.createObserver(Bool.self)
        let warningVisible = testScheduler.createObserver(Bool.self)
        
        mockTraits = PlatformTraits(volumeMin: 10,
                                    volumeMax: 11,
                                    connectionTimeout: 12,
                                    nameLengthLimit: 17,
                                    nameValidator: AlwaysEmptyValidator() )
        
        let mockConnectedDevice = DummyDevice(name: BehaviorRelay<String>(value: String(describing: DummyDevice.self)),
                                              state: DummyDeviceState(replyWithCustomName: true),
                                              otaAdapter: nil)
        
        vm_connected = RenameViewModel(device: mockConnectedDevice, dependencies: MockedAnalyticsServiceDependency())
        
        vm_connected.outputs.renameButtonVisible.asObservable().subscribe(renameVisible).disposed(by: disposeBag)
        vm_connected.outputs.wrongNameIndicatorVisible.asObservable().subscribe(warningVisible).disposed(by: disposeBag)
        
        vm_connected.inputs.rename(with: "NEW NAME")
        
        // user removes all text in input field
        XCTAssertEqual([.next(TestTime(), false),
                        .next(TestTime(), false)], renameVisible.events)
        XCTAssertEqual([.next(TestTime(), false),
                        .next(TestTime(), false)], warningVisible.events)
    }
    
    func testRenameWarningVisibilityFailWhiteCharactetrs() {
        let testScheduler = TestScheduler(initialClock: TestTime())
        let renameVisible = testScheduler.createObserver(Bool.self)
        let warningVisible = testScheduler.createObserver(Bool.self)
        
        mockTraits = PlatformTraits(volumeMin: 10,
                                    volumeMax: 11,
                                    connectionTimeout: 12,
                                    nameLengthLimit: 17,
                                    nameValidator: AlwaysWhiteCharactesrValidator() )
        
        let mockConnectedDevice = DummyDevice(name: BehaviorRelay<String>(value: String(describing: DummyDevice.self)),
                                              state: DummyDeviceState(replyWithCustomName: true),
                                              otaAdapter: nil)
        
        vm_connected = RenameViewModel(device: mockConnectedDevice, dependencies: MockedAnalyticsServiceDependency())
        
        vm_connected.outputs.renameButtonVisible.asObservable().subscribe(renameVisible).disposed(by: disposeBag)
        vm_connected.outputs.wrongNameIndicatorVisible.asObservable().subscribe(warningVisible).disposed(by: disposeBag)
        
        vm_connected.inputs.rename(with: "NEW NAME")
        
        // user put only white spaces into the name
        XCTAssertEqual([.next(TestTime(), false),
                        .next(TestTime(), false)], renameVisible.events)
        XCTAssertEqual([.next(TestTime(), false),
                        .next(TestTime(), true)], warningVisible.events)
    }
    
    func testRenameWarningVisibilityFailTooLong() {
        let testScheduler = TestScheduler(initialClock: TestTime())
        let renameVisible = testScheduler.createObserver(Bool.self)
        let warningVisible = testScheduler.createObserver(Bool.self)
        
        mockTraits = PlatformTraits(volumeMin: 10,
                                    volumeMax: 11,
                                    connectionTimeout: 12,
                                    nameLengthLimit: 17,
                                    nameValidator: AlwaysTooLongValidator() )
        
        let mockConnectedDevice = DummyDevice(name: BehaviorRelay<String>(value: String(describing: DummyDevice.self)),
                                              state: DummyDeviceState(replyWithCustomName: true),
                                              otaAdapter: nil)
        
        vm_connected = RenameViewModel(device: mockConnectedDevice, dependencies: MockedAnalyticsServiceDependency())
        
        vm_connected.outputs.renameButtonVisible.asObservable().subscribe(renameVisible).disposed(by: disposeBag)
        vm_connected.outputs.wrongNameIndicatorVisible.asObservable().subscribe(warningVisible).disposed(by: disposeBag)
        
        vm_connected.inputs.rename(with: "NEW NAME")
        
        // casual validation - name is just too big
        XCTAssertEqual([.next(TestTime(), false),
                        .next(TestTime(), false)], renameVisible.events)
        XCTAssertEqual([.next(TestTime(), false),
                        .next(TestTime(), true)], warningVisible.events)
    }
    
    func testActive() {
        setUpWithCustomNameResponse()
        let testScheduler = TestScheduler(initialClock: TestTime())
        let active = testScheduler.createObserver(Bool.self)
        
        vm_connected.outputs.active.asObservable().subscribe(active).disposed(by: disposeBag)
        
        XCTAssertEqual([.next(TestTime(), true)], active.events)
        
        vm_connected.inputs.renameButtonTapped()
        XCTAssertEqual([.next(TestTime(), true),
                        .next(TestTime(), false)], active.events)
    }
}
