//
//  ActiveNoiceCancellingViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 03/12/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

protocol ActiveNoiceCancellingViewModelInput {
    func viewDidLoad()
    func viewDidAppear()
    func viewWillDisappear()
    var inputActiveNoiceCancellingMode: PublishRelay<ActiveNoiseCancellingMode> { get set }
    var inputActiveNoiceCancellingLevel: PublishRelay<Int> { get set }
    var inputMonitoringLevel: PublishRelay<Int> { get set }
}

protocol ActiveNoiceCancellingViewModelOutput {
    var outputActiveNoiceCancellingSettingsInit: PublishRelay<ActiveNoiseCancellingSettings> { get set }
    var outputActiveNoiceCancellingSettings: PublishRelay<ActiveNoiseCancellingSettings> { get set }
    var outputActiveNoiceCancellingMode: PublishRelay<ActiveNoiseCancellingMode> { get set }
    var active: BehaviorRelay<Bool> { get set }
}

protocol ActiveNoiceCancellingViewModelType {
    var inputs: ActiveNoiceCancellingViewModelInput { get }
    var outputs: ActiveNoiceCancellingViewModelOutput { get }
}

class ActiveNoiceCancellingViewModel: ActiveNoiceCancellingViewModelInput, ActiveNoiceCancellingViewModelOutput, ActiveNoiceCancellingViewModelType {
    typealias Dependencies = AnalyticsServiceDependency & AppModeDependency

    var active: BehaviorRelay<Bool> = BehaviorRelay(value: true)

    // ActiveNoiceCancellingViewModelOutput
    var outputActiveNoiceCancellingSettingsInit = PublishRelay<ActiveNoiseCancellingSettings>()
    var outputActiveNoiceCancellingSettings = PublishRelay<ActiveNoiseCancellingSettings>()
    var outputActiveNoiceCancellingMode = PublishRelay<ActiveNoiseCancellingMode>()

    // ActiveNoiseCancellingViewModelInput
    var inputActiveNoiceCancellingMode = PublishRelay<ActiveNoiseCancellingMode>()
    var inputActiveNoiceCancellingLevel = PublishRelay<Int>()
    var inputMonitoringLevel = PublishRelay<Int>()

    // ActiveNoiseCancellingViewModelType
    var inputs: ActiveNoiceCancellingViewModelInput { return self }
    var outputs: ActiveNoiceCancellingViewModelOutput { return self }

    private let device: DeviceType
    private var dependencies: Dependencies
    private var disposeBag = DisposeBag()

    init(device: DeviceType, dependencies: Dependencies) {
        self.device = device
        self.dependencies = dependencies

        device.state.outputs.connectivityStatus().stateObservable()
            .asDriver()
            .map { $0.connected }
            .drive(active)
            .disposed(by: disposeBag)

        let activeNoiseCancellingSettings = device.state.outputs.activeNoiseCancellingSettings().stateObservable().skipOne()
        activeNoiseCancellingSettings.takeOne().subscribe(onNext: { [weak self] activeNoiseCancellingSettings in
            self?.outputActiveNoiceCancellingSettingsInit.accept(activeNoiseCancellingSettings)
        }).disposed(by: disposeBag)
        activeNoiseCancellingSettings.skipOne().subscribe(onNext: { [weak self] activeNoiseCancellingSettings in
            self?.outputActiveNoiceCancellingSettings.accept(activeNoiseCancellingSettings)
        }).disposed(by: disposeBag)

        let monitoredActiveNoiseCancellingMode = device.state.outputs.monitoredActiveNoiseCancellingMode().stateObservable().skipOne()
        monitoredActiveNoiseCancellingMode.subscribe(onNext: { [weak self] activeNoiseCancellingMode in
            self?.outputActiveNoiceCancellingMode.accept(activeNoiseCancellingMode)
        }).disposed(by: disposeBag)

        inputActiveNoiceCancellingMode.asObservable().observeOn(MainScheduler.instance).subscribe({ [weak self] inputActiveNoiceCancellingMode in
            guard let activeNoiceCancellingMode = inputActiveNoiceCancellingMode.element else { return }
            self?.device.state.inputs.write(activeNoiseCancellingMode: activeNoiceCancellingMode)
        }).disposed(by: disposeBag)
        inputActiveNoiceCancellingLevel.asObservable().observeOn(MainScheduler.instance).subscribe({ [weak self] inputActiveNoiceCancellingLevel in
            guard let activeNoiceCancellingLevel = inputActiveNoiceCancellingLevel.element else { return }
            self?.device.state.inputs.write(activeNoiseCancellingLevel: activeNoiceCancellingLevel)
        }).disposed(by: disposeBag)
        inputMonitoringLevel.asObservable().observeOn(MainScheduler.instance).subscribe({ [weak self] inputMonitoringLevel in
            guard let monitoringLevel = inputMonitoringLevel.element else { return }
            self?.device.state.inputs.write(monitoringLevel: monitoringLevel)
        }).disposed(by: disposeBag)
        dependencies.appMode.skipOne().observeOn(MainScheduler.instance).subscribe({ [weak self] mode in
            guard let mode = mode.element else { return }
            switch mode {
            case .background:
                self?.device.state.inputs.stopMonitoringActiveNoiseCancellingMode()
            case .foreground:
                self?.device.state.inputs.requestActiveNoiseCancellingSettings()
                self?.device.state.inputs.startMonitoringActiveNoiseCancellingMode()
            }
        }).disposed(by: disposeBag)

    }
    deinit {
        print(#function, String(describing: self))
    }
}

extension ActiveNoiceCancellingViewModel {
    func viewDidLoad() {
        device.state.inputs.requestActiveNoiseCancellingSettings()
    }
    func viewDidAppear() {
        device.state.inputs.startMonitoringActiveNoiseCancellingMode()
    }
    func viewWillDisappear() {
        device.state.inputs.stopMonitoringActiveNoiseCancellingMode()
    }
}
