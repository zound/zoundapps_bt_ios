//
//  PairingViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 10/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

public enum PairingError : Error {
    case bleDisconnect
    case acessoryDisconnect
    case pickerCancelled
}
enum DevicePairingState {
    case pairing
    case completed
    case error(PairingError)
    case foregrounded
    case backgrounded
    case unhandled
}
protocol PairingViewModelInput: class {
    func viewDidAppear()
    func viewDidLoad()
    func pairingFadeOutCompleted()
}
protocol PairingViewModelOutput {
    var viewAppeared: PublishRelay<Void> { get }
    var viewLoaded: PublishRelay<Void> { get }
    var pairingState: BehaviorRelay<DevicePairingState> { get }
    var completed: PublishRelay<Result<DevicePairingState, PairingError>> { get }
    var pairingImage: UIImage? { get }
}
protocol PairingViewModelType {
    var inputs: PairingViewModelInput { get }
    var outputs: PairingViewModelOutput { get }
}
final public class PairingViewModel: PairingViewModelInput, PairingViewModelOutput, PairingViewModelType {
    typealias Dependencies = DeviceServiceDependency & AppModeDependency
    var inputs: PairingViewModelInput { return self }
    var outputs: PairingViewModelOutput { return self }
    var pairingState = BehaviorRelay<DevicePairingState>.init(value: .unhandled)
    var completed = PublishRelay<Result<DevicePairingState, PairingError>>()
    var pairingImage: UIImage?
    var initialState: DevicePairingState
    
    init(dependencies: Dependencies, device: DeviceType, initialState: DevicePairingState) {
        self.dependencies = dependencies
        self.device = device
        self.pairingImage = device.image.pairingSetupImage
        self.initialState = initialState
    }
    func viewDidAppear() {
        viewAppeared.accept(())
    }
    func viewDidLoad() {
        viewLoaded.accept(())
        let deviceDisconnectedTrigger = device.state.outputs.connectivityStatus().stateObservable()
            .skipWhile({ $0 != .disconnected })
            .takeOne()
            .map { _ in return DevicePairingState.error(PairingError.bleDisconnect) }
        let accessoryDisconnectedTrigger = device.state.outputs.accessoryPairingStatus().stateObservable()
            .skipWhile({ $0 != .disconnected })
            .takeOne()
            .map { _ in return DevicePairingState.error(PairingError.acessoryDisconnect) }
        let pickerCancelledTrigger = device.state.outputs.accessoryPairingStatus().stateObservable()
            .skipWhile({ $0 != .cancelled })
            .takeOne()
            .map { _ in return DevicePairingState.error(PairingError.pickerCancelled) }
        let appMode = dependencies.appMode
            .distinctUntilChanged()
            .map { appMode -> DevicePairingState  in
                switch appMode {
                case .background: return DevicePairingState.backgrounded
                case .foreground: return DevicePairingState.foregrounded
                }
            }
        let accessoryConnectedTrigger = device.state.outputs.accessoryPairingStatus().stateObservable()
            .skipWhile({ $0 != .connected })
            .takeOne()
            .map { _ in return DevicePairingState.completed }
        
        let state = Observable.merge(
            deviceDisconnectedTrigger,
            accessoryDisconnectedTrigger,
            pickerCancelledTrigger,
            appMode,
            accessoryConnectedTrigger
        )
        .startWith(initialState)
        let handleStateAction = handle(pairingStateTrigger: pairingState,
                                       completedTrigger: completed,
                                       device: device)
        state
            .subscribe(onNext: handleStateAction).disposed(by: disposeBag)
    }
    func pairingFadeOutCompleted() {
        completed.accept(Result.success(.completed))
    }
    var viewAppeared = PublishRelay<Void>()
    var viewLoaded = PublishRelay<Void>()

    private var device: DeviceType
    private var dependencies: Dependencies
    private var disposeBag = DisposeBag()
}

private func handle(pairingStateTrigger: BehaviorRelay<DevicePairingState>,
                    completedTrigger: PublishRelay<Result<DevicePairingState, PairingError>>,
                    device: DeviceType)
-> (DevicePairingState) -> Void {
    return { state in
        switch state {
        case .foregrounded:
            device.state.inputs.showAccessoryPicker()
        case let .error(error):
            completedTrigger.accept(.failure(error))
        case .completed:
            pairingStateTrigger.accept(state)
        default: break
        }
    }
}
