//
//  EqualizerSettingsViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 21/11/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

protocol EqualizerSettingsViewModelInput {
    func viewDidLoad()
    func viewDidAppear()
    func viewDidBecameActive()
    func viewWillResignActive()
    var inputEqualizerButtonStep: PublishRelay<EqualizerButtonStep> { get set }
    var inputEqualizerButtonStep2Preset: PublishRelay<EqualizerButtonStepPreset> { get set }
    var inputEqualizerButtonStep3Preset: PublishRelay<EqualizerButtonStepPreset> { get set }
    var inputCustomPresetGraphicalEqualizer: PublishRelay<GraphicalEqualizer> { get set }
}

protocol EqualizerSettingsViewModelOutput {
    var outputEqualizerSettingsInit: PublishRelay<EqualizerSettings> { get set }
    var outputEqualizerSettings: PublishRelay<EqualizerSettings> { get set }
    var outputEqualizerButtonStep: PublishRelay<EqualizerButtonStep> { get set }
    var active: BehaviorRelay<Bool> { get set }
}

protocol EqualizerSettingsViewModelType {
    var inputs: EqualizerSettingsViewModelInput { get }
    var outputs: EqualizerSettingsViewModelOutput { get }
}

class EqualizerSettingsViewModel: EqualizerSettingsViewModelInput, EqualizerSettingsViewModelOutput, EqualizerSettingsViewModelType {
    typealias Dependencies = AnalyticsServiceDependency

    var active: BehaviorRelay<Bool> = BehaviorRelay(value: true)

    // EqualizerSettingsViewModelOutput
    var outputEqualizerSettingsInit = PublishRelay<EqualizerSettings>()
    var outputEqualizerSettings = PublishRelay<EqualizerSettings>()
    var outputEqualizerButtonStep = PublishRelay<EqualizerButtonStep>()

    // EqualizerSettingsViewModelInput
    var inputEqualizerButtonStep = PublishRelay<EqualizerButtonStep>()
    var inputEqualizerButtonStep2Preset = PublishRelay<EqualizerButtonStepPreset>()
    var inputEqualizerButtonStep3Preset = PublishRelay<EqualizerButtonStepPreset>()
    var inputCustomPresetGraphicalEqualizer = PublishRelay<GraphicalEqualizer>()

    // EqualizerSettingsViewModelType
    var inputs: EqualizerSettingsViewModelInput { return self }
    var outputs: EqualizerSettingsViewModelOutput { return self }

    private let device: DeviceType
    private var preferences: EqualizerStorageItems
    private var dependencies: Dependencies
    private var disposeBag = DisposeBag()

    init(device: DeviceType, dependencies: Dependencies, preferences: EqualizerStorageItems) {
        self.device = device
        self.dependencies = dependencies
        self.preferences = preferences

        device.state.outputs.connectivityStatus().stateObservable()
            .asDriver()
            .map { $0.connected }
            .drive(active)
            .disposed(by: disposeBag)

        device.state.outputs.currentAudioSource().stateObservable()
            .asDriver()
            .distinctUntilChanged()
            .map { $0 != .aux }
            .drive(active)
            .disposed(by: disposeBag)

        let equalizerSettings = device.state.outputs.equalizerSettings().stateObservable().skipOne()
        equalizerSettings.takeOne().subscribe(onNext: { [weak self] equalizerSettings in
            self?.outputEqualizerSettingsInit.accept(equalizerSettings)
        }).disposed(by: disposeBag)
        equalizerSettings.skipOne().subscribe(onNext: { [weak self] equalizerSettings in
            self?.outputEqualizerSettings.accept(equalizerSettings)
        }).disposed(by: disposeBag)

        let monitoredEqualizerButtonStep = device.state.outputs.monitoredEqualizerButtonStep().stateObservable().skipOne()
        monitoredEqualizerButtonStep.subscribe(onNext: { [weak self] equalizerButtonStep in
            self?.outputEqualizerButtonStep.accept(equalizerButtonStep)
        }).disposed(by: disposeBag)

        inputEqualizerButtonStep.asObservable().observeOn(MainScheduler.instance).subscribe({ [weak self] inputEqualizerButtonStep in
            guard let equalizerButtonStep = inputEqualizerButtonStep.element else { return }
            self?.device.state.inputs.write(equalizerButtonStep: equalizerButtonStep)
        }).disposed(by: disposeBag)
        inputEqualizerButtonStep2Preset.asObservable().observeOn(MainScheduler.instance).subscribe({ [weak self] inputEqualizerButtonStep2Preset in
            guard let equalizerButtonStep2Preset = inputEqualizerButtonStep2Preset.element else { return }
            self?.device.state.inputs.write(step2Preset: equalizerButtonStep2Preset)
        }).disposed(by: disposeBag)
        inputEqualizerButtonStep3Preset.asObservable().observeOn(MainScheduler.instance).subscribe({ [weak self] inputEqualizerButtonStep3Preset in
            guard let equalizerButtonStep3Preset = inputEqualizerButtonStep3Preset.element else { return }
            self?.device.state.inputs.write(step3Preset: equalizerButtonStep3Preset)
        }).disposed(by: disposeBag)
        inputCustomPresetGraphicalEqualizer.asObservable().observeOn(MainScheduler.instance).subscribe({ [weak self] inputCustomPresetGraphicalEqualizer in
            guard let customPresetGraphicalEqualizer = inputCustomPresetGraphicalEqualizer.element else { return }
            self?.device.state.inputs.write(customPresetGraphicalEqualizer: customPresetGraphicalEqualizer)
        }).disposed(by: disposeBag)
    }
    deinit {
        print(#function, String(describing: self))
    }
}

extension EqualizerSettingsViewModel {
    func viewDidLoad() {
        device.state.inputs.requestEqualizerSettings()
    }
    func viewDidAppear() {
        device.state.inputs.startMonitoringEqualizerButtonStep()
    }
    func viewDidBecameActive() {
        device.state.inputs.requestEqualizerSettings()
        device.state.inputs.startMonitoringEqualizerButtonStep()
    }
    func viewWillResignActive() {
        device.state.inputs.stopMonitoringEqualizerButtonStep()
    }
}
