//
//  OTAFinishedViewController.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 06.04.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift

/// Input points of the model of AllFinished screen
protocol OTAFinishedViewModelInput {
    
    /// Support of the tap on DONE button
    func doneTapped()
}

/// Output points of the model of AllFinished screen
protocol OTAFinishedViewModelOutput {
    
    /// Notify flow when DONE button is tapped
    var done: BehaviorRelay<Bool> { get set }
    var speakerImage: BehaviorRelay<UIImage> { get }
}

/// Protocol to force input/outputs on view model
protocol OTAFinishedViewModelType {
    
    /// Input points of the view model
    var inputs: OTAFinishedViewModelInput { get }
    
    /// Output points of the view model
    var outputs: OTAFinishedViewModelOutput { get }
}

/// View model implementation for AllFinished screen
public final class OTAFinishedViewModel: OTAFinishedViewModelInput, OTAFinishedViewModelOutput, OTAFinishedViewModelType {
    
    var inputs: OTAFinishedViewModelInput { return self }
    var outputs: OTAFinishedViewModelOutput { return self }
    
    init(image: UIImage) {
        done = BehaviorRelay.init(value: false)
        speakerImage = BehaviorRelay.init(value: image)
    }
    
    /// Make the screen disappear when DONE button is tapped
    func doneTapped() {
        self.done.accept(true)
    }
    
    internal var done: BehaviorRelay<Bool>
    internal var speakerImage: BehaviorRelay<UIImage>
}
