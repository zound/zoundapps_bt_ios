//
//  CommonSettingsViewModel.swift
//  MarshallBluetooth®
//
//  Created by Paprota Przemyslaw on 10/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit
import RxSwift

enum NextScreenType {
    case unknown
    case signUp
    case analytics
    case quickGuide
    case quickGuideActonII
    case quickGuideMonitorII
    case quickGuideStanmoreII
    case quickGuideWoburnII
    case onlineManual
    case onlineManualActonII
    case onlineManualMonitorII
    case onlineManualStanmoreII
    case onlineManualWoburnII
    case contact
    case endUserLicence
    case freeAndOpenSource
    case ancButtonGuide
    case bluetoothSpeakersGuide
    case bluetoothHeadphonesGuide
    case controlKnobGuide
    case mButtonGuide
    case speakersGuide
    case playAndPauseGuide  
}

/// Item to display on the menu
struct CommonMenuItem {
    var nextScreen: NextScreenType
    var label: String

}

/// Input points of the model for Device Settings
protocol CommonMenuViewModelInput {

    /// Handle tap on settings options
    ///
    /// - Parameter index: row's index from IndexPath
    func menuItemTapped(index: Int)

    /// Notify model that screen is closed.
    func close()

}

/// Output points of the model for Device Settings
protocol CommonMenuViewModelOutput {
    /// Screen title
    var title: BehaviorRelay<String> { get set }

    /// Additional text which is displayed on the screen
    var bottomLabel: BehaviorRelay<String> { get set }

    /// Notify about new data source for view (for ex. UITableView)
    var dataSource: BehaviorRelay<[CommonMenuItem]> { get set }

    /// Item from datasource which is selected by user
    var selectedItem: BehaviorRelay<NextScreenType> { get set }

    // Indicates that screen is closed
    var closed: BehaviorRelay<Void> { get set }
}

/// Protocol to force input/outputs on view model
protocol CommonMenuViewModelType {
    /// Input points of the view model
    var inputs: CommonMenuViewModelInput { get }

    /// Output points of the view model
    var outputs: CommonMenuViewModelOutput { get }
}

class CommonMenuViewModel: CommonMenuViewModelInput,CommonMenuViewModelOutput, CommonMenuViewModelType {

    var inputs: CommonMenuViewModelInput { return self }

    var outputs: CommonMenuViewModelOutput { return self }

    var title: BehaviorRelay<String> = BehaviorRelay<String>(value: "")

    var bottomLabel: BehaviorRelay<String> = BehaviorRelay<String>(value: "")

    var dataSource: BehaviorRelay<[CommonMenuItem]> = BehaviorRelay<[CommonMenuItem]>(value: [])

    var selectedItem: BehaviorRelay<NextScreenType> = BehaviorRelay<NextScreenType>(value: .unknown)

    var closed = BehaviorRelay<Void>(value: ())

    func menuItemTapped(index: Int) {
        let item = dataSource.value[index]
        selectedItem.accept(item.nextScreen)
    }

    func close() {
        closed.accept(())
    }

}
