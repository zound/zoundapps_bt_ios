//
//  StayUpdatedViewModelTests.swift
//  MarshallBluetoothLibrary
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

fileprivate let mockCorrectIcon = UIImage.checkmarkMarshall
fileprivate let mockIncorrectIcon = UIImage.failIcon
fileprivate let mockWarningIcon = UIImage.warningIcon
fileprivate let mockNoIcon: UIImage? = nil

private final class MockStorage: SubscriptionStorageItems{
    var subscriptionEmail = ""
}
private final class NewsletterServiceMock:  NewsletterService {
    var email = ""
    override var subscriptionEmail : String {
        get { return email}
        set { email = newValue}
    }
}
typealias TestDependencies = AnalyticsServiceDependency & NewsletterServiceDependency
class TestDependecy: TestDependencies {
    var analyticsService: AnalyticsServiceType
    var newsletterService: NewsletterService
    init(analyticsService: MockedAnalyticsService, newsletterService: NewsletterService) {
        self.analyticsService = analyticsService
        self.newsletterService = newsletterService
    }
}
final class StayUpdatedViewModelTests: XCTestCase {
    var disposeBag: DisposeBag!
    var vm: StayUpdatedViewModel!
    
    override func setUp() {
       let dependencies = TestDependecy(analyticsService: MockedAnalyticsService(), newsletterService: NewsletterServiceMock())
        super.setUp()
        disposeBag = DisposeBag()
        vm = StayUpdatedViewModel(setup: true, dependencies: dependencies)
    }
    
    func testUIEmailValidation() {
        let scheduler = TestScheduler(initialClock: 0)
        let nextButtonEnable = scheduler.createObserver(Bool.self)
        vm.outputs.nextButtonEnable.asDriver().drive(nextButtonEnable).disposed(by: disposeBag)

        let skipped = scheduler.createObserver(Bool.self)
        vm.outputs.skipped.asDriver().drive(skipped).disposed(by: disposeBag)
        
        let completed = scheduler.createObserver(Bool.self)
        vm.outputs.viewCompleted.asDriver().drive(completed).disposed(by: disposeBag)
        
        let icon = scheduler.createObserver(UIImage?.self)
        vm.outputs.descriptionIcon.asDriver().drive(icon).disposed(by: disposeBag)
        
        scheduler.start()
        
        // Initial state - user entered signup screen
        XCTAssertEqual([.next(0, false)], nextButtonEnable.events)
        XCTAssertEqual([.next(0, false)], completed.events)
        XCTAssertEqual([.next(0, false)], skipped.events)
        XCTAssertEqual([.next(0, mockNoIcon)], icon.events)
        
        // Email shorter than 3 characters
        vm.inputs.emailChanged(name: "fo")
        
        // Wrong email typed twice
        vm.inputs.emailChanged(name: "foo")
        vm.inputs.emailChanged(name: "foo@bar")

        // Good email typed in
        vm.inputs.emailChanged(name: "foo@bar.pl")

        // Wrong email typed again
        vm.inputs.emailChanged(name: "foo@bar")

        // User reduced email length to 2 chars
        vm.inputs.emailChanged(name: "fo")

        XCTAssertEqual([.next(0, mockNoIcon),
                        .next(0, mockNoIcon),
                        .next(0, mockIncorrectIcon),
                        .next(0, mockIncorrectIcon),
                        .next(0, mockCorrectIcon),
                        .next(0, mockIncorrectIcon),
                        .next(0, mockNoIcon)], icon.events)

        XCTAssertEqual([.next(0,false),
                        .next(0,false),
                        .next(0,false),
                        .next(0,true),
                        .next(0,false)], nextButtonEnable.events)

        // User clicked Next
        vm.inputs.nextButtonTapped()
        XCTAssertEqual([.next(0,false)], completed.events)
        
        // User clicked Skip
        vm.inputs.skipButtonTapped()
        XCTAssertEqual([.next(0,false), .next(0,true)], skipped.events)
    }
    
    func testUISendEmail() {
        let scheduler = TestScheduler(initialClock: 0)
        let nextButtonEnable = scheduler.createObserver(Bool.self)
        vm.outputs.nextButtonEnable.asDriver().drive(nextButtonEnable).disposed(by: disposeBag)

        let nextButtonVisible = scheduler.createObserver(Bool.self)
        vm.outputs.nextButtonVisible.asDriver().drive(nextButtonVisible).disposed(by: disposeBag)
        
        let indicatorVisible = scheduler.createObserver(Bool.self)
        vm.outputs.indicatorVisible.asDriver().drive(indicatorVisible).disposed(by: disposeBag)
        
        scheduler.start()
        
        // Good email typed in
        vm.inputs.emailChanged(name: "foo@bar.pl")
        
        // User clicked Next
        vm.inputs.nextButtonTapped()
        
        XCTAssertEqual([.next(0,false),
                        .next(0,true)], nextButtonEnable.events)
        XCTAssertEqual([.next(0,true),
                        .next(0,false)], nextButtonVisible.events)
        XCTAssertEqual([.next(0,false),
                        .next(0,true)], indicatorVisible.events)
    }
}
