//
//  QuickGuideMenuSpeakersViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Szatkowski Michal on 18/02/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import UIKit

class QuickGuideMenuSpeakersViewModel: CommonMenuViewModel {
    
    override init() {
        super.init()
        var  items = [CommonMenuItem]()
        items.append(CommonMenuItem(nextScreen: .bluetoothSpeakersGuide, label: Strings.main_menu_item_bluetooth_pairing()))
        items.append(CommonMenuItem(nextScreen: .speakersGuide, label: Strings.device_settings_menu_item_couple()))
        items.append(CommonMenuItem(nextScreen: .playAndPauseGuide, label: Strings.main_menu_item_play_pause_button()))
        
        dataSource.accept(items)
        title.accept(Strings.main_menu_item_quick_guide_uc())
    }
}
