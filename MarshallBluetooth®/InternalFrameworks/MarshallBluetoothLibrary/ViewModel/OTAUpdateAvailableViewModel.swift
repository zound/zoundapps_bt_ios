//
//  OTAUpdateAvailableViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 14.09.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//


import RxSwift

/// Input points of the model of UpdateAvailable screen
protocol OTAUpdateAvailableViewModelInput {
    
    /// Handle taps on CONTINUE button
    func continueTapped()
}

/// Output points of the model of UpdateAvailable screen
protocol OTAUpdateAvailableViewModelOutput {
    
    /// Notify flow when CONTINUE button is tapped
    var continued: PublishSubject<Void> { get set }
}

/// Protocol to force input/outputs on view model
protocol OTAUpdateAvailableViewModelType {
    
    /// Input points of the view model
    var inputs: OTAUpdateAvailableViewModelInput { get }
    
    /// Output points of the view model
    var outputs: OTAUpdateAvailableViewModelOutput { get }
}

/// View model implementation for AllFinished screen
public final class OTAUpdateAvailableViewModel: OTAUpdateAvailableViewModelInput, OTAUpdateAvailableViewModelOutput {
    
    var continued = PublishSubject<Void>.init()
        
    /// Make the screen disappear when DONE button is tapped
    func continueTapped() {
        continued.onNext(())
    }
}

extension OTAUpdateAvailableViewModel: OTAUpdateAvailableViewModelType {
    var inputs: OTAUpdateAvailableViewModelInput { return self }
    var outputs: OTAUpdateAvailableViewModelOutput { return self }
}
