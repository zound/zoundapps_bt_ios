//
//  HelpScreenViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Paprota Przemyslaw on 11/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

class HelpScreenViewModel: CommonMenuViewModel {
    override init() {
        super.init()
        var  items = [CommonMenuItem]()
        items.append(CommonMenuItem(nextScreen: .quickGuide, label: quickGuideText))
        items.append(CommonMenuItem(nextScreen: .onlineManual, label: onlineManualText))
        items.append(CommonMenuItem(nextScreen: .contact, label: contactText))
        dataSource.accept(items)
        title.accept(titleText)
    }
}

private extension HelpScreenViewModel {
    private var quickGuideText: String {
        return  Strings.main_menu_item_quick_guide()
    }
    
    private var onlineManualText: String {
        return  Strings.main_menu_item_online_manual()
    }
    private var contactText: String{
        return  Strings.main_menu_item_contact()
    }
    
    private var titleText: String {
        return Strings.main_menu_item_help_uc()
    }
}
