//
//  DevicesListViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 15/03/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//
import RxSwift
import GUMA

typealias IndexPathPair = (masterIndexPath: IndexPath, slaveIndexPath: IndexPath)
typealias CellModelUpdateInfo = (indexPath: IndexPath, updated: DeviceCellEntryData)

private struct Const {
    static let cellStatusObservableActivationDelaySeconds = 1.0
}
enum DevicesListActionType {
    case add(IndexPath)
    case update(IndexPath)
    case move(IndexPath, IndexPath)
    case delete(IndexPath)
}
struct DeviceListActionEntry {
    let type: DeviceListActionType
    let models: [HomeDeviceCellViewModel]
}
protocol DevicesListViewModelInput {
    func refresh()
    func startConfiguringDevice(device: DeviceType)
    func decoupleDevices(master: DeviceType)
    func requestMenu()
    func requestDataReload()
    func requestDeviceSettings(device: DeviceType)
    func requestDedicatedView(for feature: Feature, and device: DeviceType)
    func requestPlayerSources(device: DeviceType)
    func viewLoaded()
    func viewWillAppear()
    func viewWillDisappear()
}

protocol DevicesListViewModelOutput {
    var devices: BehaviorRelay<[DeviceType]> { get }
    var updateInfo: BehaviorRelay<[UpdateAvailableInfo]> { get }
    var menuRequested: PublishRelay<Void> { get }
    var dataReloadRequested: PublishRelay<Void> { get }
    var configureDevice: PublishRelay<DeviceType> { get }
    var decoupleDevices: PublishRelay<DeviceType> { get }
    var coupledIndicationRequested: PublishRelay<IndexPathPair> { get }
    var currentCellStatus: BehaviorRelay<CellModelUpdateInfo> { get }
    var concluded: PublishRelay<DeviceListActionEntry> { get }
    
    var settingsRequested: PublishRelay<DeviceType> { get }
    var featureViewRequested: PublishRelay<(DeviceType, Feature)> { get }
    var playerSourcesRequested: PublishRelay<DeviceType> { get }
//    var refreshInProgress: BehaviorRelay<Bool> { get }
    var appMode: BehaviorRelay<AppMode> { get }
    var listActionTrigger: PublishRelay<DeviceListActionEntry> { get }
    
    var moved: PublishRelay<(IndexPath, IndexPath)> { get }
    var added: PublishRelay<IndexPath> { get }
    var removed: PublishRelay<IndexPath> { get }
    var updated: PublishRelay<IndexPath> { get }
    var cellModels: [DeviceModelEntry] { get }
}

protocol DevicesListViewModelType: AnyObject {
    var inputs: DevicesListViewModelInput { get }
    var outputs: DevicesListViewModelOutput { get }
}
private struct ConnectivityStatusDisposable {
    let deviceId: String
    let disposable: Disposable
}

class DeviceModelEntry {
    var indexPath: IndexPath
    let cellModel: HomeDeviceCellViewModel
    let disposable: Disposable
    var updatedStatusDisposable: Disposable
    init(indexPath: IndexPath,
         model: HomeDeviceCellViewModel,
         modelDisposable: Disposable,
         updatedStatusDisposable: Disposable) {
        self.indexPath = indexPath
        self.cellModel = model
        self.disposable = modelDisposable
        self.updatedStatusDisposable = updatedStatusDisposable
    }
}

enum DeviceListOperation {
    case add
    case remove
    case changeConnectivityStatus
}

public final class DevicesListViewModel: DevicesListViewModelInput, DevicesListViewModelOutput, DevicesListViewModelType {
    typealias Dependencies = DeviceServiceDependency &
                             OTAServiceDependency &
                             DeviceAdapterServiceDependency &
                             AppModeDependency &
                             AnalyticsServiceDependency
    
    var inputs: DevicesListViewModelInput { return self }
    var outputs: DevicesListViewModelOutput { return self }

    /// Outputs
    var updateInfo: BehaviorRelay<[UpdateAvailableInfo]>
    var menuRequested = PublishRelay<Void>()
    var dataReloadRequested = PublishRelay<Void>()
    var configureDevice = PublishRelay<DeviceType>()
    var decoupleDevices = PublishRelay<DeviceType>()
    var coupledIndicationRequested = PublishRelay<IndexPathPair>()
    var settingsRequested = PublishRelay<DeviceType>()
    var featureViewRequested = PublishRelay<(DeviceType, Feature)>()
    var playerSourcesRequested = PublishRelay<DeviceType>()
    var refreshInProgress = BehaviorRelay<Bool>.init(value: false)
    var listActionTrigger = PublishRelay<DeviceListActionEntry>()
    var concluded = PublishRelay<DeviceListActionEntry>()
    var appMode: BehaviorRelay<AppMode>
    
    var moved = PublishRelay<(IndexPath, IndexPath)>()
    var added = PublishRelay<IndexPath>()
    var removed = PublishRelay<IndexPath>()
    var updated = PublishRelay<IndexPath>()
    var changed = PublishRelay<DeviceType>()
    
    var devices: BehaviorRelay<[DeviceType]>
    var cellModels = [DeviceModelEntry]()
    let currentCellStatus = BehaviorRelay<CellModelUpdateInfo>
        .init(value: CellModelUpdateInfo(indexPath: IndexPath(),
                                         updated: DeviceCellEntryData()))
    
    init(dependencies: Dependencies, preferences: DefaultStorage = DefaultStorage.shared) {
        self.dependencies = dependencies
        self.preferences = preferences
        self.appMode = dependencies.appMode

        self._moved = dependencies.deviceService.outputs.moved
        self._added = dependencies.deviceService.outputs.added
        self._removed = dependencies.deviceService.outputs.removed
        self._updated = dependencies.deviceService.outputs.updated
        
        self.devices = dependencies.deviceService.outputs.devices
        self.updateInfo = dependencies.otaService.outputs.updateAvailableInfo
    }

    /// Inputs
    func startConfiguringDevice(device: DeviceType) {
        guard let activeDevice = dependencies.deviceAdapterService.outputs.managedDevices.first(where: { $0.id == device.id }) else { return }
        outputs.configureDevice.accept(activeDevice)
    }
    func decoupleDevices(master: DeviceType) {
        guard let activeMaster = dependencies.deviceAdapterService.outputs.managedDevices.first(where: { $0.id == master.id }) else { return }
        outputs.decoupleDevices.accept(activeMaster)
    }
    func refresh() {
        dependencies.deviceAdapterService.inputs.refresh()
    }
    func requestMenu() {
        outputs.menuRequested.accept(())
    }
    func requestDataReload() {
        outputs.dataReloadRequested.accept(())
        requestCoupledIndicationIfNeeded()
    }
    func requestDeviceSettings(device: DeviceType) {
        guard let activeDevice = dependencies.deviceAdapterService.outputs.managedDevices.first(where: { $0.id == device.id }) else { return }
        outputs.settingsRequested.accept(activeDevice)
    }
    func requestDedicatedView(for feature: Feature, and device: DeviceType) {
        outputs.featureViewRequested.accept((device, feature))
    }
    func requestPlayerSources(device: DeviceType) {
        guard let activeDevice = dependencies.deviceAdapterService.outputs.managedDevices.first(where: { $0.id == device.id }) else { return }
        outputs.playerSourcesRequested.accept(activeDevice)
    }
    func viewLoaded() {
        let discoveredDevicesObservables = dependencies.deviceAdapterService.outputs.managedDevices.map { Observable.just($0) }
        let discovered = Observable.concat(discoveredDevicesObservables)
            .map { return (DeviceListOperation.add, $0) }
        let added = dependencies.deviceAdapterService.outputs.addedDevice.map { return (DeviceListOperation.add, $0) }
        let removed = dependencies.deviceAdapterService.outputs.removedDevice.map {
            return (DeviceListOperation.remove, $0 )
        }
        let changedConnectivityStatus = changed.asObservable().map { return (DeviceListOperation.changeConnectivityStatus, $0 )}
        Observable.merge(discovered, added, removed, changedConnectivityStatus)
            .observeOn(MainScheduler.instance)
            .map { [unowned self] operationInfo -> DeviceListActionEntry in
                let (actionType, device) = operationInfo
                return listActionEntry(type: actionType, device: device, cellModels: &self.cellModels, generator: self.cellViewModel, cellUpdateTrigger: self.currentCellStatus)
            }
            .map { [unowned self] listAction in
                return (listAction, self.listActionTrigger, self.concluded)
            }
            .flatMap(perform)
            .subscribe()
            .disposed(by: disposeBag)
    }
    func viewWillAppear() {
        dependencies.deviceAdapterService.inputs.polling(enable: true, dueTime: RxTimeInterval.seconds(0), interval: AdapterServiceConfig.defaultPollingInterval)
        cellModels.forEach { $0.cellModel.inputs.setupMonitors(enable: true) }
    }
    func viewWillDisappear() {
        dependencies.deviceAdapterService.inputs.polling(enable: false, dueTime: RxTimeInterval.seconds(0), interval: AdapterServiceConfig.defaultPollingInterval)
        cellModels.forEach { $0.cellModel.inputs.setupMonitors(enable: false) }
        dependencies.deviceAdapterService.inputs.cancelDiscovery()
    }
    private var refreshDisposeBag = DisposeBag()
    private var cellModelsDisposeBag = DisposeBag()
    private let disposeBag = DisposeBag()
    private let dependencies: Dependencies
    private let preferences: DefaultStorage

    private var _moved: PublishRelay<(IndexPath, IndexPath)>
    private var _added: PublishRelay<(DeviceType, IndexPath)>
    private var _removed: PublishRelay<IndexPath>
    private var _updated: PublishRelay<(DeviceType, IndexPath)>

    private var cellModelsDisposables = [DisposableEntry]()
    private var connectivityStatusDisposables = [ConnectivityStatusDisposable]()
}
private extension DevicesListViewModel {
    func requestCoupledIndicationIfNeeded() {
        if let masterIndex = devices.value.index(where: { $0.id == preferences.lastConfiguredCouplePairs.first?.key }),
            let slaveIndex = devices.value.index(where: { $0.id == preferences.lastConfiguredCouplePairs.first?.value }) {
            preferences.lastConfiguredCouplePairs.removeAll()
            let pair = (
                masterIndexPath: IndexPath(item: masterIndex, section: Int()),
                slaveIndexPath: IndexPath(item: slaveIndex, section: Int())
            )
            coupledIndicationRequested.accept(pair)
        }
    }
    private func cellViewModel(for device: DeviceType) -> HomeDeviceCellViewModel {
        let perDeviceUpdateInfoObservable = updateInfo
            .flatMap { [weak device] updateAvailableInfo -> Observable<Bool> in
                
                guard let device = device else { return Observable.just(false) }
                
                guard updateAvailableInfo.contains(where: { $0.deviceID == device.id }) else {
                    return Observable.just(false)
                }
                return Observable.just(true)
            }
    
        let cellModel = HomeDeviceCellViewModel(device: device, updateInfo: perDeviceUpdateInfoObservable, dependencies: dependencies)
        let disposable = disposableEntry(for: device.id)
        
        cellModel.outputs.configureDevice
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self, weak device] in
                guard let device = device else { return }
                self?.startConfiguringDevice(device: device)
            })
            .disposed(by: disposable.disposeBag)
        
        cellModel.outputs.decoupleDevices
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self, weak device] in
                guard let device = device else { return }
                self?.decoupleDevices(master: device)
            })
            .disposed(by: disposable.disposeBag)
        
        cellModel.outputs.showDeviceSettingsTriggered
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self, weak device] deviceID in
                guard let device = device else { return }
                guard device.id == deviceID else { return }
                self?.requestDeviceSettings(device: device)
            })
            .disposed(by: disposable.disposeBag)

        cellModel.outputs.showDedicatedFeatureViewTriggered
        .asObservable()
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { [weak self, weak device] (deviceID, feature) in
            guard let device = device else { return }
            guard device.id == deviceID else { return }
            self?.requestDedicatedView(for: feature, and: device)
        })
        .disposed(by: disposable.disposeBag)

        cellModel.outputs.showPlayerTriggered
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self, weak device] deviceID in
                guard let device = device else { return }
                guard device.id == deviceID else { return }
                self?.requestPlayerSources(device: device)
            })
            .disposed(by: disposable.disposeBag)
        return cellModel
    }

    private func disposableEntry(for deviceID: String) -> DisposableEntry {
        if let existingDisposableEntryIndex = cellModelsDisposables.index(where: { $0.id == deviceID }) {
            cellModelsDisposables.remove(at: existingDisposableEntryIndex)
        }
        let cellDisposable = DisposableEntry(id: deviceID, disposeBag: DisposeBag())
        cellModelsDisposables.append(cellDisposable)
        return cellDisposable
    }
    
}

private func connectivitySort(lhs: DeviceType, rhs: DeviceType) -> Bool {
    let lhsConnectivityStatus = lhs.state.outputs.connectivityStatus().stateObservable().value
    let rhsConnectivityStatus = rhs.state.outputs.connectivityStatus().stateObservable().value
    return compare(lhsStatus: lhsConnectivityStatus, rhsStatus: rhsConnectivityStatus)
}
private func connectivitySort(lhs: DeviceModelEntry, rhs: DeviceModelEntry) -> Bool {
    let lhsConnectivityStatus = lhs.cellModel.currentValue.connectivityStatus
    let rhsConnectivityStatus = rhs.cellModel.currentValue.connectivityStatus
    return compare(lhsStatus: lhsConnectivityStatus, rhsStatus: rhsConnectivityStatus)
}
private func compare(lhsStatus: ConnectivityStatus, rhsStatus: ConnectivityStatus) -> Bool {
    switch (lhsStatus, rhsStatus) {
    case (.connected, .connected):
        return false
    case (.connected, _):
        return true
    default: return false
    }
}
private func modelEntry(idx: Int,
                        device: DeviceType,
                        cellModelGenerator: (DeviceType) -> HomeDeviceCellViewModel,
                        cellUpdateTrigger: BehaviorRelay<CellModelUpdateInfo>)
    -> DeviceModelEntry {
    let indexPath = IndexPath(item: idx, section: .zero)
    let viewModel = cellModelGenerator(device)
    let disposable = viewModel.statusDisposable
    let updatedStatusDisposable = viewModel.outputs.currentStatus
        .observeOn(MainScheduler.instance)
        .delay(RxTimeInterval.seconds(Const.cellStatusObservableActivationDelaySeconds), scheduler: MainScheduler.instance)
        .subscribe(onNext: { updated in
            cellUpdateTrigger.accept(CellModelUpdateInfo(indexPath: indexPath, updated: updated))
        })
    return DeviceModelEntry(indexPath: indexPath,
                            model: viewModel,
                            modelDisposable: disposable,
                            updatedStatusDisposable: updatedStatusDisposable)
}
private func listActionEntry(type: DeviceListOperation,
                             device: DeviceType,
                             cellModels: inout [DeviceModelEntry],
                             generator: @escaping (DeviceType) -> HomeDeviceCellViewModel,
                             cellUpdateTrigger: BehaviorRelay<CellModelUpdateInfo>
                             ) -> DeviceListActionEntry {
    if type == .add {
        let entry = modelEntry(idx: .zero,
                               device: device,
                               cellModelGenerator: generator,
                               cellUpdateTrigger: cellUpdateTrigger)
        cellModels.append(entry)
        guard cellModels.count > 1 else {
            let indexPath = IndexPath(item: .zero, section: .zero)
            let listAction = DeviceListActionEntry(
                type: .add(indexPath),
                models: cellModels.map { $0.cellModel }
            )
            return listAction
        }
        cellModels.forEach {
            $0.updatedStatusDisposable.dispose()
        }
        cellModels = cellModels
            .sorted(by: connectivitySort)
            .enumerated().map { offset, entry -> DeviceModelEntry in
                entry.updatedStatusDisposable.dispose()
                let newIndexPath = IndexPath(item: offset, section: .zero)
                entry[keyPath: \.indexPath] = newIndexPath
                entry[keyPath: \.updatedStatusDisposable] = entry.cellModel.currentStatus
                    .observeOn(MainScheduler.instance)
                    .delay(RxTimeInterval.seconds(Const.cellStatusObservableActivationDelaySeconds), scheduler: MainScheduler.instance)
                    .subscribe(onNext: { updated in
                        cellUpdateTrigger.accept(CellModelUpdateInfo(indexPath: newIndexPath, updated: updated))
                    })
                return entry
            }
        if let newIndex = cellModels.firstIndex(where: { $0.cellModel.id == device.id  }) {
            let newIndexPath = IndexPath(item: newIndex, section: .zero)
            let listAction = DeviceListActionEntry(
                type: .add(newIndexPath),
                models: cellModels.map { $0.cellModel }
            )
            return listAction
        }
    }
    else if type == .remove {
        guard let idx = cellModels.firstIndex(where: { $0.cellModel.id == device.id }) else { return DeviceListActionEntry(type: .empty, models: cellModels.map { $0.cellModel }) }
        let removed = cellModels.remove(at: idx)
        removed.updatedStatusDisposable.dispose()
        removed.disposable.dispose()
        let indexPathToRemove = IndexPath(item: idx, section: .zero)
        guard cellModels.count > 0 else {
            return .init(type: .remove(indexPathToRemove), models: [])
        }
        cellModels = cellModels
            .sorted(by: connectivitySort)
            .enumerated().map { offset, entry -> DeviceModelEntry in
                entry.updatedStatusDisposable.dispose()
                let newIndexPath = IndexPath(item: offset, section: .zero)
                entry[keyPath: \.indexPath] = newIndexPath
                entry[keyPath: \.updatedStatusDisposable] = entry.cellModel.currentStatus
                    .observeOn(MainScheduler.instance)
                    .delay(RxTimeInterval.seconds(Const.cellStatusObservableActivationDelaySeconds), scheduler: MainScheduler.instance)
                    .subscribe(onNext: { updated in
                        cellUpdateTrigger.accept(CellModelUpdateInfo(indexPath: newIndexPath, updated: updated))
                    })
                return entry
            }
        return .init(type: .remove(indexPathToRemove), models: cellModels.map { $0.cellModel })
    }
    return DeviceListActionEntry(type: .empty, models: [])
}
private func perform(operation : DeviceListActionEntry,
                     trigger: PublishRelay<DeviceListActionEntry>,
                     responseChannel: PublishRelay<DeviceListActionEntry>
) -> Single<Void> {
    return Single.create { event in
        var localDisposeBag = DisposeBag()
        responseChannel.subscribe(onNext: { _ in
            event(.success(()))
            localDisposeBag = DisposeBag()
        })
        .disposed(by: localDisposeBag)
        trigger.accept(operation)
        return Disposables.create()
    }
}

