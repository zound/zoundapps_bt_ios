//
//  LightViewModel.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Bartosz Dolewski on 29.05.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

fileprivate let mockMin = 35
fileprivate let mockMax = 70

fileprivate let mockGood = 50
fileprivate let mockBad = 9000

fileprivate var mockTraits = PlatformTraits(volumeMin: 10,
                                            volumeMax: 11,
                                            ledMin: 35,
                                            ledMax: 70,
                                            connectionTimeout: 12,
                                            nameLengthLimit: 17,
                                            nameValidator: StockNameValidator())

fileprivate struct MockedDependency: AppModeDependency, AnalyticsServiceDependency {
    var appMode = BehaviorRelay<AppMode>(value: .foreground)
    var analyticsService: AnalyticsServiceType = MockedAnalyticsService()
}

fileprivate extension UIImage {
    static func from(color: UIColor, width: CGFloat, height: CGFloat) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: width, height: height)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}

fileprivate final class DummyLedBrightnessState: DeviceStateType {
    
    public var inputs: DeviceStateInput { return self }
    public var outputs: DeviceStateOutput { return self }
    
    public static let supportedFeatures: [Feature] = [.hasConnectivityStatus, .hasLedBrightnessControl]
    
    fileprivate let _ledBrightness: AnyState<Int>
    fileprivate let _connectivityStatus: AnyState<ConnectivityStatus>
    
    fileprivate let disposeBag = DisposeBag()

    fileprivate var currentLed = Int()
    
    public let perDeviceTypeFeatures: [Feature] = []
    
    init() {
        self._connectivityStatus = AnyState<ConnectivityStatus>(feature: .hasConnectivityStatus, supportsNotifications: false, initialValue: { return .connected })
        self._ledBrightness = AnyState<Int>(feature: .hasLedBrightnessControl,
                                                              supportsNotifications: false,
                                                              initialValue: { return  Int() })
    }
    
    public func setupGood() {
        currentLed = mockGood
    }
    
    public func setupBad() {
        currentLed = mockBad
    }
}

extension DummyLedBrightnessState: DeviceStateInput {
    public func requestLedBrightness() {
        guard (Int(mockMin)...Int(mockMax)).contains(currentLed) == true else { return }
        _ledBrightness.stateObservable().accept(currentLed)
    }
    
    public func write(ledBrightness: Int) {
        _ledBrightness.stateObservable().accept(Int(ledBrightness))
    }
}

extension DummyLedBrightnessState: DeviceStateOutput {
    public var activeMonitors: Set<Monitor> {
        get { return  Set<Monitor>() }
        set(newValue) {}
    }
    
    public func connectivityStatus() -> AnyState<ConnectivityStatus> {
        return _connectivityStatus
    }
    
    public func ledBrightness() -> AnyState<Int> {
        return _ledBrightness
    }
}

fileprivate class LightDummyDevice: DeviceType {
    func bleConnect() -> Single<Void> {
        return Single.just(())
    }
    
    var mac: MAC?
    var configured = false
    
    var hardwareType: HardwareType = .speaker
    
    func decouple() -> Single<Void> {
        return Single.just(())
    }
    
    func resetSlave() -> Single<Void> {
        return Single.just(())
    }
    
    func initialSetup(with storedEntry: StoredDeviceInfoEntry?) -> Single<String> {
        return Single.just(String())
    }
    
    var modelName: String {
        return String()
    }

    var image: DeviceImageProviding = DeviceBlankImageProvider()
    

    var connectionTimeout: RxTimeInterval {
        return RxTimeInterval.seconds(5.0)
    }
    
    var name: BehaviorRelay<String>
    var id: String { return String(describing: LightDummyDevice.self) }
    var imageName: String { return String(describing: LightDummyDevice.self) }
    
    let platform = Platform(id: "joplinBT", traits: mockTraits)
    
    var state: DeviceStateType
    var otaAdapter: OTAAdapterType?

    init(name: BehaviorRelay<String>,
         state: DeviceStateType,
         otaAdapter: OTAAdapterType? = nil) {
        self.name = name
        self.state = state
        self.otaAdapter = otaAdapter
    }
}

final class LightViewModelTests: XCTestCase {
    var disposeBag: DisposeBag!
    var vm: LightViewModelType!
    
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
    }
    
    func setUpGoodDevice() {
        let mockState = DummyLedBrightnessState()
        mockState.setupGood()
        let mockDevice = LightDummyDevice(name: BehaviorRelay<String>(value: String(describing: LightDummyDevice.self)),
                                          state: mockState,
                                          otaAdapter: nil)
        vm = LightViewModel(device: mockDevice, dependencies: MockedDependency())
    }
    
    func setUpBadDevice() {
        let mockState = DummyLedBrightnessState()
        mockState.setupBad()
        let mockDevice = LightDummyDevice(name: BehaviorRelay<String>(value: String(describing: LightDummyDevice.self)),
                                          state: mockState,
                                          otaAdapter: nil)
        vm = LightViewModel(device: mockDevice, dependencies: MockedDependency())
    }
    
    func testReadingCorrectCurrentBrightness() {
        setUpGoodDevice()
        
        let scheduler = TestScheduler(initialClock: TestTime())
        let brightness = scheduler.createObserver(Int.self)
        let dataFeched = scheduler.createObserver(Bool.self)
        
        vm.outputs.ledIntensity.asObservable().subscribe(brightness).disposed(by: disposeBag)
        vm.outputs.dataFetched.asObservable().subscribe(dataFeched).disposed(by: disposeBag)
        
        setUpGoodDevice()
        
        XCTAssertEqual([.next(TestTime(), 50)], brightness.events)
        XCTAssertEqual([.next(TestTime(), true)], dataFeched.events)
    }
    
    func testReadingCurrentBrightnessOutsideScale() {
        setUpBadDevice()
        
        let scheduler = TestScheduler(initialClock: TestTime())
        let brightness = scheduler.createObserver(Int.self)
        let dataFeched = scheduler.createObserver(Bool.self)
        
        vm.outputs.ledIntensity.asObservable().subscribe(brightness).disposed(by: disposeBag)
        vm.outputs.dataFetched.asObservable().subscribe(dataFeched).disposed(by: disposeBag)
        
        XCTAssertTrue(brightness.events.isEmpty)
        XCTAssertEqual([.next(TestTime(), false)], dataFeched.events)
    }
    
    func testWritingCurrentBrightness() {
        setUpGoodDevice()
        
        let scheduler = TestScheduler(initialClock: TestTime())
        let brightness = scheduler.createObserver(Int.self)
        
        vm.outputs.ledIntensity.asObservable().subscribe(brightness).disposed(by: disposeBag)
        
        // initial value (taken from device)
        XCTAssertEqual([.next(TestTime(), 50)], brightness.events)
        
        // write new LED intensity
        vm.inputs.change(intensity: 60)
        XCTAssertEqual([.next(TestTime(), 50),
                        .next(TestTime(), 60)], brightness.events)
    }
    
    func testReadingCurrentBrightnessAsPercentageValue() {
        setUpGoodDevice()
        
        let scheduler = TestScheduler(initialClock: TestTime())
        let dataFeched = scheduler.createObserver(Bool.self)
        let percentage = scheduler.createObserver(Double.self)
        
        vm.outputs.ledPercentage.asObservable().subscribe(percentage).disposed(by: disposeBag)
        vm.outputs.dataFetched.asObservable().subscribe(dataFeched).disposed(by: disposeBag)
        
        XCTAssertEqual([.next(TestTime(), 0.42)], percentage.events)
        
        vm.inputs.change(intensity: mockMin)
        XCTAssertEqual([.next(TestTime(), 0.42),
                        .next(TestTime(), 0.0)], percentage.events)
        
        vm.inputs.change(intensity: mockMax)
        XCTAssertEqual([.next(TestTime(), 0.42),
                        .next(TestTime(), 0.0),
                        .next(TestTime(), 1.0)], percentage.events)
        
        XCTAssertEqual([.next(TestTime(), true)], dataFeched.events)
    }
    
    func testProcessingImage() {
        setUpGoodDevice()
        
        let scheduler = TestScheduler(initialClock: TestTime())
        let dataFeched = scheduler.createObserver(Bool.self)
        let percentage = scheduler.createObserver(Double.self)
        let image = scheduler.createObserver(UIImage.self)
        let fakeImage = UIImage.from(color: .red, width: 400, height: 400)
        
        vm.outputs.ledPercentage.asObservable().subscribe(percentage).disposed(by: disposeBag)
        vm.outputs.dataFetched.asObservable().subscribe(dataFeched).disposed(by: disposeBag)
        vm.outputs.ledImage.subscribe(image).disposed(by: disposeBag)
        
        XCTAssertEqual([.next(TestTime(), 0.42)], percentage.events)
        
        vm.inputs.change(intensity: mockMin)
        vm.inputs.change(intensity: mockMax)
        vm.inputs.original(image: fakeImage)
        
        XCTAssertEqual(image.events.count, 2)
        
        let processedImageScale = image.events[1].value.element?.scale
        let processedImageSize = image.events[1].value.element?.size
        
        XCTAssertEqual(processedImageScale, fakeImage.scale)
        XCTAssertEqual(processedImageSize, fakeImage.size)
        
        XCTAssertEqual([.next(TestTime(), true)], dataFeched.events)
    }
}
