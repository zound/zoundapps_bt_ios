//
//  CommonMenuViewModelTests.swift
//  MarshallBluetoothLibraryTests
//
//  Created by Paprota Przemyslaw on 18/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary

class CommonMenuViewModelTests: XCTestCase {

    var disposeBag: DisposeBag!
    var vm: CommonMenuViewModel!

    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
        vm = CommonMenuViewModel()
    }

    func testCancel() {
        let scheduler = TestScheduler(initialClock: 0)
        let closed = scheduler.createObserver(Void.self)
        vm.outputs.closed
            .asDriver()
            .drive(closed)
            .disposed(by: disposeBag)

        scheduler.start()
        XCTAssertEqual(1, closed.events.count)
        vm.inputs.close()
        XCTAssertEqual(2, closed.events.count)
    }
    
    
}
