//
//  FossScreenViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Paprota Przemyslaw on 16/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

/// Input points of the model for Device Settings
protocol FossScreenViewModelInput {
    func close()
}

protocol FossScreenViewModelOutput {
    var closed: BehaviorRelay<Void> { get set }
}

/// Protocol to force input/outputs on view model
protocol FossScreenViewModelType {
    /// Input points of the view model
    var inputs: FossScreenViewModelInput { get }
    /// Output points of the view model
    var outputs: FossScreenViewModelOutput { get }
    
}

class FossScreenViewModel: FossScreenViewModelType, FossScreenViewModelInput, FossScreenViewModelOutput {
    
    var inputs: FossScreenViewModelInput { return self }
    var outputs: FossScreenViewModelOutput { return self }

    var closed = BehaviorRelay<Void>(value: ())

    func close() {
        closed.accept(())
    }
    
}
