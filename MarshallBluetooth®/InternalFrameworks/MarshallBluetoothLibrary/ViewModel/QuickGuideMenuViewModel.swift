//
//  QuickGuideMenuViewModel.swift
//  MarshallBluetooth®
//
//  Created by Paprota Przemyslaw on 17/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

class QuickGuideMenuViewModel: CommonMenuViewModel {
    override init() {
        super.init()
        var  items = [CommonMenuItem]()
        items.append(CommonMenuItem(nextScreen: .quickGuideActonII, label: Environment.acton))
        items.append(CommonMenuItem(nextScreen: .quickGuideStanmoreII, label: Environment.stanmore))
        items.append(CommonMenuItem(nextScreen: .quickGuideWoburnII, label: Environment.woburn))
        items.append(CommonMenuItem(nextScreen: .quickGuideMonitorII, label: Environment.monitor))
        dataSource.accept(items)
        title.accept(titleText)
    }

    private var titleText: String {
        return Strings.main_menu_item_quick_guide_uc()
    }
}
