//
//  ErrorViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 20/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift

public enum ActionType {
    case tryAgain
    case gotoSettings
    case skip
}

public class BaseErrorAction {
    var label: String
    var type: ActionType
    var action: (ActionType) -> Void = { _ in }

    init(label: String, type: ActionType) {
        self.label = label
        self.type = type
    }
}

public final class TryAgainAction: BaseErrorAction {
    init() {
        super.init(label: Strings.appwide_try_again_uc(), type: .tryAgain)
    }
}

public final class GotoSettingsAction: BaseErrorAction {
    init() {
        super.init(label: Strings.appwide_go_to_settings_uc(), type: .gotoSettings)
    }
}

public final class SkipAction: BaseErrorAction {
    init() {
        super.init(label: Strings.appwide_skip_uc(), type: .skip)
    }
}

public class BaseErrorInfo {
    public let title: String
    public let subtitle: String
    public var actions: [BaseErrorAction]
    
    init(title: String, subtitle: String, actions: [BaseErrorAction]) {
        self.title = title
        self.subtitle = subtitle
        self.actions = actions
    }

    func action(type: ActionType, _ action: @escaping (ActionType) -> Void ) {
        self.actions.first { $0.type == type }?.action = action
    }
}

public final class BluetoothErrorInfo: BaseErrorInfo {
    init() {
        super.init(title: Strings.error_no_bluetooth_title_uc(),
                   subtitle: Strings.error_no_bluetooth_subtitle(),
                   actions: [GotoSettingsAction()])
    }
}

public final class NoDevicesFoundErrorInfo: BaseErrorInfo {
    init() {
        super.init(title: Strings.error_ota_undefined(),
                   subtitle: Strings.error_ota_undefined_subtitle(),
                   actions: [TryAgainAction()])
    }
}

public final class NoInternetOTAErrorInfo: BaseErrorInfo {
    init() {
        super.init(title: Strings.error_common(),
                   subtitle: Strings.error_ota_downloading_description(),
                   actions: [TryAgainAction()])
    }
}

public final class OTAErrorInfo: BaseErrorInfo {
    init() {
        super.init(title: Strings.error_firmware_update_title_uc(),
                   subtitle: Strings.error_firmware_update_subtitle(),
                   actions: [TryAgainAction(), SkipAction()])
    }
}

public final class SomethingWentWrongErrorInfo: BaseErrorInfo {
    init() {
        super.init(title: Strings.error_ota_undefined(),
                   subtitle: Strings.error_ota_undefined_subtitle(),
                   actions: [TryAgainAction()])
    }
}

public final class DownloadingErrorInfo: BaseErrorInfo {
    init() {
        super.init(title: Strings.error_firmware_update_title_uc(),
                   subtitle: Strings.error_firmware_update_subtitle(),
                   actions: [TryAgainAction(), SkipAction()])
    }
}

public final class FlashingErrorInfo: BaseErrorInfo {
    init() {
        super.init(title: Strings.error_firmware_update_title_uc(),
                   subtitle: Strings.error_firmware_update_subtitle(),
                   actions: [TryAgainAction(), SkipAction()])
    }
}
    
public final class UpdateFailedErrorInfo: BaseErrorInfo {
    init() {
        super.init(title: Strings.error_firmware_update_title_uc(),
                   subtitle: Strings.error_firmware_update_subtitle(),
                   actions: [TryAgainAction(), SkipAction()])
    }
}

protocol ErrorViewModelOutput {
    var errorInfo: BaseErrorInfo { get }
}

protocol ErrorViewModelType {
    var outputs: ErrorViewModelOutput { get }
}

public final class ErrorViewModel: ErrorViewModelOutput {

    typealias Dependencies = AnalyticsServiceDependency
    
    var errorInfo: BaseErrorInfo
    private let dependencies: Dependencies
    
    init(errorInfo: BaseErrorInfo, dependencies: Dependencies) {
        self.errorInfo = errorInfo
        self.dependencies = dependencies
        logAnalyticsEventIfNeeded()
    }
    
    deinit {
        print(#function, String(describing: self))
    }
    
    private let disposeBag = DisposeBag()
}

extension ErrorViewModel: ErrorViewModelType {
    var outputs: ErrorViewModelOutput { return self }
}

private extension ErrorViewModel {
    func logAnalyticsEventIfNeeded() {
        switch errorInfo {
        case is BluetoothErrorInfo:
            dependencies.analyticsService.inputs.log(.appErrorOccurredBluetooth)
        case is NoInternetOTAErrorInfo:
            dependencies.analyticsService.inputs.log(.appErrorOccurredNetwork)
        case is FlashingErrorInfo:
            dependencies.analyticsService.inputs.log(.appErrorOccurredOtaFlashing)
        case is DownloadingErrorInfo:
            dependencies.analyticsService.inputs.log(.appErrorOccurredOtaDownload)
        case is UpdateFailedErrorInfo:
            dependencies.analyticsService.inputs.log(.appErrorOccurredOtaUndefined)
        default:
            break
        }
    }
}
