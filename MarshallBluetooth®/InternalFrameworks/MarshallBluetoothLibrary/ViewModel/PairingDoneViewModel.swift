//
//  PairingDoneViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 03/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import RxSwift

protocol PairingDoneViewModelInput {
    func doneTapped()
}
protocol PairingDoneViewModelOutput {
    var done: BehaviorRelay<Bool> { get set }
    var deviceImage: BehaviorRelay<UIImage> { get }
    var deviceName: BehaviorRelay<String> { get }
}
protocol PairingDoneViewModelType {
    var inputs: PairingDoneViewModelInput { get }
    var outputs: PairingDoneViewModelOutput { get }
}
public final class PairingDoneViewModel: PairingDoneViewModelInput, PairingDoneViewModelOutput, PairingDoneViewModelType {
    
    var inputs: PairingDoneViewModelInput { return self }
    var outputs: PairingDoneViewModelOutput { return self }
    
    init(deviceName: String, image: UIImage) {
        self.done = BehaviorRelay.init(value: false)
        self.deviceName = BehaviorRelay.init(value: deviceName)
        self.deviceImage = BehaviorRelay.init(value: image)
    }
    func doneTapped() {
        self.done.accept(true)
    }
    
    var done: BehaviorRelay<Bool>
    var deviceImage: BehaviorRelay<UIImage>
    var deviceName: BehaviorRelay<String>
}
