//
//  OTAOnboardingViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 08/10/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift

protocol OTAOnboardingViewModelInput {
    func nextTapped()
}

protocol OTAOnboardingViewModelOutput {
    var viewCompleted: PublishSubject<Void> { get set }
}

protocol OTAOnboardingViewModelType {
    /// Input points of the view model
    var inputs: OTAOnboardingViewModelInput { get }
    
    /// Output points of the view model
    var outputs: OTAOnboardingViewModelOutput { get }
}

public final class OTAOnboardingViewModel: OTAOnboardingViewModelInput, OTAOnboardingViewModelOutput {
    var viewCompleted = PublishSubject<Void>.init()
    
    func nextTapped() {
        viewCompleted.onCompleted()
    }
}

extension OTAOnboardingViewModel: OTAOnboardingViewModelType {
    var inputs: OTAOnboardingViewModelInput { return self }
    var outputs: OTAOnboardingViewModelOutput { return self }
}
