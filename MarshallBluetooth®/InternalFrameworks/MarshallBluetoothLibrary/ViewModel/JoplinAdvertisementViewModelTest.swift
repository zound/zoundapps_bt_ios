//
//  AdvertisementViewModelTest.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 16/04/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

/* Mocked device entry from JSON
 {"name" : "FIRST", "id" : "00:a2:c2:d5:d4:a3", "playing" : false, "connected" : false, "hasUpdate" : true},
 */

final class AdvertisementViewModelTest: XCTestCase, JoplinAdvertisementViewModelOutput {
    
    var setupProgress: Observable<SetupProgressInfo> = Observable.just(.initializingConnection)
    
    var viewReady = PublishRelay<Void>.init()
    var updateFinished = PublishRelay<Void>.init()

    var testViewReady: TestableObserver<Void>!
    var testSetupProgress: TestableObserver<SetupProgressInfo>!
    var testUpdateFinished: TestableObserver<Void>!

    var disposeBag: DisposeBag!
    var testScheduler: TestScheduler!
    
    var advertisementVm: JoplinAdvertisementViewModel!
    
    override func setUp() {
        
        super.setUp()
        
        disposeBag = DisposeBag()
        
        let progressInfoOTAStates = OTAStatus.all().map(SetupProgressInfo.otaProgress)
        let allStates: [SetupProgressInfo] = [.initializingConnection, .connected] + progressInfoOTAStates
        
        let setupProgressObservable = Observable<SetupProgressInfo>.from(allStates)
        advertisementVm = JoplinAdvertisementViewModel(progress: setupProgressObservable)
        
        viewReady = advertisementVm.outputs.viewReady
        setupProgress = advertisementVm.outputs.setupProgress
        updateFinished = advertisementVm.outputs.updateFinished
        
        let updateInfoExpectation = expectation(description: "Waiting for update info tick")
        _ = Observable<Int>
                .timer(RxTimeInterval(5), scheduler: MainScheduler.instance)
                .subscribe(onNext: { _ in
                    updateInfoExpectation.fulfill()
                }).disposed(by: disposeBag)
        
        waitForExpectations(timeout: 15, handler: nil)
    
        testScheduler = TestScheduler(initialClock: 0)
        
        testViewReady = testScheduler.createObserver(Void.self)
        testSetupProgress = testScheduler.createObserver(SetupProgressInfo.self)
        testUpdateFinished = testScheduler.createObserver(Void.self)
    
        viewReady.subscribe(testViewReady).disposed(by: disposeBag)
        setupProgress.subscribe(testSetupProgress).disposed(by: disposeBag)
        updateFinished.subscribe(testUpdateFinished).disposed(by: disposeBag)
    }

    func testInitialProgressEntry() {
        testScheduler.start()
        
        let initialExpectation = expectation(description: "Waiting for initial value")
        setupProgress.subscribe(onNext: { progressInfo in
            if case .initializingConnection = progressInfo {
                XCTAssertEqual(progressInfo.entry.maxValue, 0)
            }
            if case .connected = progressInfo {
                XCTAssertEqual(progressInfo.entry.maxValue, SetupProgressInfo.maxConnectingPercentageProgress)
                initialExpectation.fulfill()
            }
        }).disposed(by: disposeBag)
        waitForExpectations(timeout: 5, handler: nil)
    }

    func testFinalProgressEntry() {
        testScheduler.start()
        
        let finalExpectation = expectation(description: "Waiting for final value")
        setupProgress.subscribe(onNext: { progressInfo in
            guard case .otaProgress(let otaStatus) = progressInfo else { return }
            guard case .completed = otaStatus else { return }
                
            XCTAssertEqual(progressInfo.entry.maxValue, 100)
            finalExpectation.fulfill()
        }).disposed(by: disposeBag)
        waitForExpectations(timeout: 5, handler: nil)
    }
}
