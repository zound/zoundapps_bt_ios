//
//  AboutScreenViewModel.swift
//  MarshallBluetooth®
//
//  Created by Paprota Przemyslaw on 11/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

class AboutScreenViewModel: CommonMenuViewModel {
    override init() {
        super.init()
        var  items = [CommonMenuItem]()
        let endUserLicence = CommonMenuItem(nextScreen: .endUserLicence, label: endUserLicenceLabel())
        let freeAndOpenSource = CommonMenuItem(nextScreen: .freeAndOpenSource, label: freeAndOpenSourceLabel())
        items.append(endUserLicence)
        items.append(freeAndOpenSource)
        dataSource.accept(items)
        title.accept(title())
        bottomLabel.accept(bottomText())
    }

    private func endUserLicenceLabel() -> String {
        return Strings.main_menu_item_terms_and_conditions()
    }

    private func freeAndOpenSourceLabel() -> String {
        return Strings.main_menu_item_foss()
    }

    private func title() -> String {
        return Strings.appwide_about_uc()
    }

    private var version: String {
        return (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String) ?? Strings.appwide_unknown()
    }
    
    private var build: String {
        return (Bundle.main.infoDictionary?["CFBundleVersion"] as? String) ?? Strings.appwide_unknown()
    }
    
    private func bottomText() -> String {
        let text = Strings.main_menu_about_name() + "\n" + Strings.main_menu_about_version()
        return text + " " + version + " (" + build + ")" 
    }
}

