//
//  SettingsScreenViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Paprota Przemyslaw on 11/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

class SettingsScreenViewModel: CommonMenuViewModel {
    override init() {
        super.init()
        var  items = [CommonMenuItem]()
        
        items.append(CommonMenuItem(nextScreen: .signUp, label: signUpText))
        items.append(CommonMenuItem(nextScreen: .analytics, label: analyticsText))
        
        dataSource.accept(items)
        title.accept(titleText)
    }
    
    private var signUpText: String {
        return Strings.main_menu_item_email_subscription()
    }
    
    private var analyticsText: String {
        return Strings.main_menu_item_analytics()
    }
    
    private var titleText: String {
        return Strings.main_menu_item_settings_uc()
    }
}
