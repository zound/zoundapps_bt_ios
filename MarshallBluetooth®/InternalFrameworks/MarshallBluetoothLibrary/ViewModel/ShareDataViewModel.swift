//
//  ShareDataViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 22.03.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA

/// Input points of the model of ShareData screen
protocol ShareDataViewModelInput {

    /// Switch the sharing data option
    func saveDataSwitched()

    /// Service the tap on the Next button
    func nextButtonTapped()

    /// Service the tap on the Skip button
    func skipButtonTapped()
}

/// Output points of the model of ShareData screen
protocol ShareDataViewModelOutput {
    
    /// Notify when sharing data option has changed
    var shareDataEnabled: BehaviorRelay<Bool> { get }
    
    /// Notify when Share Data view is going to leave screen
    var viewCompleted: BehaviorRelay<Bool> { get }
    
    /// Text which is displayed on "next" button ("NEXT" or "DONE")
    var completionText: BehaviorRelay<String> { get }

    /// Notify when Skip button should be visible or not
    var skipVisible: BehaviorRelay<Bool> { get }
}

/// Protocol to force input/outputs on view model
protocol ShareDataViewModelType {
    
    /// Input points of the view model
    var inputs: ShareDataViewModelInput { get }
    
    /// Output points of the view model
    var outputs: ShareDataViewModelOutput { get }
}

/// View model implementation for StayUpdated screen
final class ShareDataViewModel: ShareDataViewModelInput, ShareDataViewModelOutput {
    
    typealias Dependencies = AnalyticsServiceDependency
    
    var shareDataEnabled: BehaviorRelay<Bool>
    var viewCompleted = BehaviorRelay(value: false)
    var completionText: BehaviorRelay<String>
    var skipVisible: BehaviorRelay<Bool>

    private var preferences: ShareAnalyticsStorageItems
    private let dependencies: Dependencies
    private let disposeBag = DisposeBag()
    
    init(wizardMode: Bool, dependencies: Dependencies, preferences: ShareAnalyticsStorageItems) {
        self.preferences = preferences
        self.dependencies = dependencies
        let label = wizardMode ? Strings.appwide_next_uc() : Strings.appwide_done_uc()
        completionText = BehaviorRelay(value: label)

        skipVisible = BehaviorRelay(value: wizardMode)

        shareDataEnabled = BehaviorRelay(value: preferences.shareAnalyticsData)

        viewCompleted
            .skipOne()
            .withLatestFrom(shareDataEnabled) { $1 }
            .subscribe(onNext: { [unowned self] save in
                self.dependencies.analyticsService.inputs.setAnalyticsCollection(enabled: save)
                self.preferences.shareAnalyticsData = save
            }).disposed(by: disposeBag)
    }
    
    deinit {
        print(#function, String(describing: self))
    }
    
    /// Change the state of sharing data
    func saveDataSwitched() {
        let enabled = !shareDataEnabled.value
        dependencies.analyticsService.inputs.log(.appShareDataSwitched(enabled ? .on : .off))
        shareDataEnabled.accept(enabled)
    }
    
    /// Notify the view model to switch this view into next one in SignUp flow
    func skipButtonTapped() {
        dependencies.analyticsService.inputs.log(.appOnboardingShareDataSkipped)
        viewCompleted.accept(true)
    }
    /// Notify the view model to switch this view into next one in SignUp flow
    func nextButtonTapped() {
        // user has ended with the data sharing configuration, move on to the next view
        viewCompleted.accept(true)
    }
}

// MARK: - Inputs and Outputs for ShareDataViewModel
extension ShareDataViewModel: ShareDataViewModelType {
    var inputs: ShareDataViewModelInput { return self }
    var outputs: ShareDataViewModelOutput { return self }
}
