//
//  SoundsViewModel.swift
//  MarshallBluetoothLibrary
//
//  Created by Bartosz Dolewski on 31.07.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift
import GUMA

/// Input points of the model for About This Speaker view model
protocol SoundsViewModelInput {
    func requestSoundsControlStatus()
    func turnPower(on: Bool)
    func turnMedia(on: Bool)
}

protocol SoundsViewModelOutput {
    var active: PublishSubject<Bool> { get }
    var dataFetched: PublishSubject<Bool> { get }
    var power: PublishRelay<Bool> { get }
    var media: PublishRelay<Bool> {get }
    var isMediaSwitchEnable: PublishRelay<Bool> {get }
}

/// Protocol to force input/outputs on view model
protocol SoundsViewModelType {
    /// Input points of the view model
    var inputs: SoundsViewModelInput { get }
    
    /// Output points of the view model
    var outputs: SoundsViewModelOutput { get }
}

class SoundsViewModel: SoundsViewModelInput, SoundsViewModelOutput {
    
    var active = PublishSubject<Bool>()
    var dataFetched = PublishSubject<Bool>()
    var power = PublishRelay<Bool>()
    var media = PublishRelay<Bool>()
    var isMediaSwitchEnable = PublishRelay<Bool>()
    
    private let modelDevice: DeviceType
    private var disposeBag = DisposeBag()
    
    init(device: DeviceType) {
        modelDevice = device
        
        modelDevice.state.outputs.connectivityStatus().stateObservable()
            .map { $0.connected }
            .bind(to: active)
            .disposed(by: disposeBag)
        
        let powerDataFetched = modelDevice.state.outputs.soundsControlStatus().stateObservable()
            .skipOne()
            .filter { $0.isPowerCue }
            .share()
        
        let mediaDataFetched = modelDevice.state.outputs.soundsControlStatus().stateObservable()
            .skipOne()
            .filter { $0.isMediaCue }
            .share()
        
        powerDataFetched
            .map { $0.isOn }
            .bind(to: power)
            .disposed(by: disposeBag)
        
        mediaDataFetched
            .map { $0.isOn }
            .bind(to: media)
            .disposed(by: disposeBag)
        
        Observable.combineLatest(powerDataFetched, mediaDataFetched) { _,_ in return true }
            .bind(to: dataFetched)
            .disposed(by: disposeBag)
    }
    
    deinit {
        print(#function, String(describing: self))
    }
    
    func requestSoundsControlStatus() {
        modelDevice.state.inputs.requestSoundsControlStatus()
        isMediaSwitchEnable.accept(modelDevice.hardwareType == .headset)
    }
    
    func turnPower(on: Bool) {
        modelDevice.state.inputs.write(soundControl: .powerCue(on: on))
    }
    
    func turnMedia(on: Bool) {
        modelDevice.state.inputs.write(soundControl: .mediaCue(on: on))
    }
}

extension SoundsViewModel: SoundsViewModelType {
    var inputs: SoundsViewModelInput { return self }
    var outputs: SoundsViewModelOutput { return self }
}
