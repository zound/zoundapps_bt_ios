//
//  AutoOffTime.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 11/12/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

private struct Const {
    static let minutesInAnHour = 60
}

public enum AutoOffTime: Equatable {
    case off
    case on(minutes: Int)
}

public extension AutoOffTime {
    init?(minutes: Int) {
        if minutes == .zero {
            self = .off
        } else if minutes > .zero {
            self = .on(minutes: minutes)
        } else {
            return nil
        }
    }
    init?(hours: Int, minutes: Int) {
        if hours == .zero, minutes == .zero {
            self = .off
        } else if (hours > .zero && minutes > .zero) || (hours > .zero && minutes == .zero) || (hours == .zero && minutes > .zero) {
            self = .on(minutes: hours * Const.minutesInAnHour + minutes)
        } else {
            return nil
        }
    }
    var hours: Int {
        if case .on(let minutes) = self {
            return Int(floor(Float(minutes)/Float(Const.minutesInAnHour)))
        } else {
            return .zero
        }
    }
    var minutes: Int {
        if case .on(let minutes) = self {
            return minutes % Const.minutesInAnHour
        } else {
            return .zero
        }
    }
    var totalMinutes: Int {
        if case .on(let minutes) = self {
            return minutes
        } else {
            return .zero
        }
    }
}
