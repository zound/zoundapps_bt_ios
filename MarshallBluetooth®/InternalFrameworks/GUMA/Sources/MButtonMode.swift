//
//  MButtonMode.swift
//  GUMA
//
//  Created by Wudarski Lukasz on 28/12/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

public enum MButtonMode: CaseIterable {
    case equalizerSettingsControl
    case googleVoiceAssistant
    case nativeVoiceAssistant
}
