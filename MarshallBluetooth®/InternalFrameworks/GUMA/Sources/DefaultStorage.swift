//
//  DefaultStorage.swift
//  GUMA
//
//  Created by Grzegorz Kiel on 05/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

@objc public final class DefaultStorage: NSObject {
    
    @objc public static let shared = DefaultStorage()
    
    @objc public dynamic var startPressed: Bool = false
    @objc public dynamic var signupCompleted: Bool = false
    @objc public dynamic var welcomeIntroAnimated: Bool = false
    @objc public dynamic var newsletterSubscription: Bool = false
    @objc public dynamic var newsletterSubscriptionEmail: String?
    @objc public dynamic var newsletterAccountID: String?
    @objc public dynamic var subscriptionEmail: String = ""
    @objc public dynamic var shareAnalyticsData: Bool = true
    @objc public dynamic var couplePairs: [MasterId: SlaveId] = [:]
    @objc public dynamic var lastConfiguredCouplePairs: [MasterId: SlaveId] = [:]
    
    override init() {
        super.init()
        for c in Mirror.init(reflecting: self).children {
            guard let key = c.label else {
                continue
            }
            self.addObserver(self, forKeyPath: key, options: .new, context: nil)
            guard let value = UserDefaults.standard.value(forKey: key) else {
                continue
            }
            self.setValue(value, forKey: key)
        }
    }
    
    deinit {
        for c in Mirror(reflecting: self).children {
            guard let key = c.label else {
                continue
            }
            self.removeObserver(self, forKeyPath: key)
        }
    }
    
    @objc public override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        for c in Mirror.init(reflecting: self).children {
            guard let key = c.label else {
                continue
            }
            if key == keyPath {
                UserDefaults.standard.set(change?[NSKeyValueChangeKey.newKey], forKey: key)
            }
        }
    }
    
}

extension DefaultStorage {
    
    private func set<T: Encodable>(_ value: T?, forKey key: String = #function) {
        if let value = value {
            do {
                let data = try PropertyListEncoder().encode(value)
                UserDefaults.standard.set(data, forKey: key)
            } catch {}
        } else {
            UserDefaults.standard.removeObject(forKey: key)
        }
    }
    
    private func get<T: Decodable>(_ type: T.Type, forKey key: String = #function) -> T? {
        if let data = UserDefaults.standard.data(forKey: key) {
            return try? PropertyListDecoder().decode(type, from: data) as T
        } else {
            return nil
        }
    }
    
}

extension DefaultStorage: DefaultStorageItems {
    
    public var customDefaultGraphicalEqualizer: GraphicalEqualizer? {
        get { return get(GraphicalEqualizer.self) }
        set { set(newValue) }
    }
    
    public var storedDevicesInfo: [StoredDeviceInfoEntry]? {
        get { return get([StoredDeviceInfoEntry].self) }
        set { set(newValue) }
    }
    
}
