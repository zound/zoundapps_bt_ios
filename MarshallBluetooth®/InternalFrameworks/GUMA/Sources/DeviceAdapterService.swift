//
//  DeviceAdapterService.swift
//  GUMA
//
//  Created by Grzegorz Kiel on 22/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift

public struct AdapterServiceConfig {
    static public let defaultPollingInterval = RxTimeInterval.seconds(5)
    static public let dueTime = RxTimeInterval.seconds(0)
}

public enum AdapterState {
    case idle
    case scanning
    case foundFirst
    case noDeviceFound
    case processingDiscovered
    case error(DeviceAdapterError)
}
extension AdapterState: Equatable {
    public static func == (lhs: AdapterState, rhs: AdapterState) -> Bool {
        switch(lhs, rhs) {
        case (.idle, .idle),
             (.scanning, .scanning),
             (.noDeviceFound, .noDeviceFound),
             (.foundFirst, .foundFirst),
             (.processingDiscovered, .processingDiscovered),
             (.error, .error):
            return true
        default:
            return false
        }
    }
}
public enum DeviceAdapterError: Swift.Error {
    case btInterfaceDisabled
}
public protocol DeviceAdapterServiceInput {
    func start()
    func append(adapter: DeviceAdapterType, mocked: Bool)
    func refresh()
    func forget(device: DeviceType)
    func polling(enable: Bool, dueTime: RxTimeInterval, interval: RxTimeInterval)
    func cancelDiscovery()
}
public protocol DeviceAdapterServiceOutput {
    var error: BehaviorRelay<DeviceAdapterError?> { get }
    var scanInProgress: BehaviorRelay<Bool> { get }
    var addedDevice: PublishRelay<DeviceType> { get }
    var updatedDevice: PublishRelay<DeviceType> { get }
    var removedDevice: PublishRelay<DeviceType> { get }
    var configurationInProgress: Bool { get }
    var adapterState: BehaviorRelay<AdapterState> { get }
    var managedDevices: [DeviceType] { get }
}
public protocol DeviceAdapterServiceType: class {
    var inputs: DeviceAdapterServiceInput { get }
    var outputs: DeviceAdapterServiceOutput { get }
}
public final class DeviceAdapterService: DeviceAdapterServiceInput, DeviceAdapterServiceOutput, DeviceAdapterServiceType {
    public let addedDevice = PublishRelay<DeviceType>()
    public let updatedDevice = PublishRelay<DeviceType>()
    public let removedDevice = PublishRelay<DeviceType>()
    public static let shared = DeviceAdapterService()
    public var inputs: DeviceAdapterServiceInput { return self }
    public var outputs: DeviceAdapterServiceOutput { return self }
    public let error = BehaviorRelay<DeviceAdapterError?>(value: nil)
    public let scanInProgress = BehaviorRelay<Bool>.init(value: false)
    public let adapterState = BehaviorRelay<AdapterState>.init(value: .idle)
    public var managedDevices: [DeviceType] = []
    public func start() {
        managedDevices.removeAll()
        guard started == false else { return }
        guard supportedAdapters.count > 0 else {
            fatalError("No discovery adapters found")
        }
        let isScanning = checkScan(for: supportedAdapters)
        let isProcessingDiscovered = checkPreprocessing(for: supportedAdapters)
        let bluetoothError = error.flatMap { error -> Observable<AdapterState> in
            switch error {
            case .some(.btInterfaceDisabled):
                return Observable.just(.error(DeviceAdapterError.btInterfaceDisabled))
            case .none:
                return Observable.just(.idle)
            }
        }
        let waitingForFirstDevice = addedDevice.flatMap { _ -> Observable<AdapterState> in
            return Observable.just(.foundFirst)
        }
        .take(1)
        
        Observable.merge(
            waitingForFirstDevice,
            isScanning,
            isProcessingDiscovered,
            bluetoothError
        )
        .distinctUntilChanged()
        .subscribe(
            onNext: { [unowned self] state in
                self.adapterState.accept(state)
            }
        )
        .disposed(by: disposeBag)
        started = true
    }
    public func refresh() {
        supportedAdapters.forEach { [unowned self] adapter in
            if adapter.configurationInProgress() == false {
                self.handleAdapterState(adapter, mocked: self.mocked)
            }
        }
    }
    public func append(adapter: DeviceAdapterType, mocked: Bool = false) {
        if let index = supportedAdapters.index(where: { type(of: $0).platform == type(of: adapter).platform }) {
            supportedAdapters.remove(at: index)
        }
        self.mocked = mocked
        supportedAdapters.append(adapter)
        
        adapter.discovered
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] device in
                self.handleDiscovered(device: device)
            })
            .disposed(by: disposeBag)
        
        adapter.removed
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] device in
                self.handleRemoved(device: device)
            }).disposed(by: disposeBag)
        
        adapter.adapterState
            .subscribe(onNext: { [unowned adapter, unowned self] _ in
                self.handleAdapterState(adapter, mocked: mocked)
            })
            .disposed(by: disposeBag)
    }
    public func forget(device toForget: DeviceType) {
        self.polling(enable: false)
        self.cancelDiscovery()
        guard let managingAdapter = supportedAdapters.first(where: { $0.managesDevice(deviceID: toForget.id) }) else {
            return
        }
        managingAdapter.forget(device: toForget)
        self.polling(enable: true)
    }
    public var configurationInProgress: Bool {
        return discoveryInProgress
    }
    public func polling(enable: Bool, dueTime: RxTimeInterval = AdapterServiceConfig.dueTime, interval: RxTimeInterval = AdapterServiceConfig.defaultPollingInterval) {
        guard enable == true else {
            pollingDisposeBag = DisposeBag()
            return
        }
        Observable<Int>.timer(dueTime, period: interval, scheduler: MainScheduler.instance)
            .subscribe(
                onNext:
                    { [weak self] _ in
                        self?.refresh()
                    }
            )
            .disposed(by: pollingDisposeBag)
    }
    public func cancelDiscovery() {
        discoveryDisposable?.dispose()
    }
    private init() { }
    private var started = false
    private var mocked = false
    private var disposeBag = DisposeBag()
    private var supportedAdapters:[DeviceAdapterType] = []
    private var defaultScanTimeout: TimeInterval = TimeInterval.seconds(2)
    private var discoveryInProgress: Bool = false
    private var pollingDisposeBag = DisposeBag()
    private var discoveryDisposable: Disposable?
    private var forcedCancel = false
}
private extension DeviceAdapterService {
    private func handleAdapterState(_ adapter: DeviceAdapterType, mocked: Bool) {
        guard started else { return }
        switch adapter.adapterState.value {
        case .ready:
            error.accept(nil)
            guard discoveryInProgress == false else { return }
            discoveryInProgress = true
            adapter.startScan(mocked: mocked)
            discoveryDisposable = Observable<Int>
                .timer(RxTimeInterval(defaultScanTimeout), scheduler: MainScheduler.instance)
                .observeOn(MainScheduler.instance)
                .flatMap{ _ -> Observable<Void> in
                    adapter.stopScan()
                    return adapter.setupDevices().asObservable()
                }
                .subscribe(
                    onNext: { [weak self] _ in
                        self?.discoveryInProgress = false
                    },
                    onDisposed: { [weak self] in
                        self?.discoveryInProgress = false
                    }
                )
        case .btDisabled:
            error.accept(.btInterfaceDisabled)
        default:
            error.accept(nil)
            break
        }
    }
    private func handleDiscovered(device: DeviceType) {
        let currentDevicesList = managedDevices
        guard let _ = currentDevicesList
            .first(where: { $0.id == device.id}) else {
            managedDevices = currentDevicesList.appending(device)
            addedDevice.accept(device)
            return
        }
        updatedDevice.accept(device)
    }
    private func handleRemoved(device: DeviceType) {
        managedDevices = managedDevices.removing(device)
        removedDevice.accept(device)
    }
}
private func checkScan(for adapters: [DeviceAdapterType]) -> Observable<AdapterState> {
    let allScanInProgress = adapters.map { $0.scanInProgress }
    return Observable.combineLatest(allScanInProgress)
        .distinctUntilChanged()
        .flatMap { allScansInProgress -> Observable<AdapterState> in
            guard allScansInProgress.filter({ $0 == true }).isEmpty else {
                return Observable.just(.scanning)
            }
            return Observable.just(.idle)
        }
}
private func checkPreprocessing(for adapters: [DeviceAdapterType]) -> Observable<AdapterState> {
    let allProcessing = adapters.map { $0.preprocessingDiscoveredInProgress }
    return Observable.combineLatest(allProcessing)
        .distinctUntilChanged()
        .flatMap { allProcessingInProgress -> Observable<AdapterState> in
            guard allProcessingInProgress.filter({ $0 == true }).isEmpty else {
                return Observable.just(.processingDiscovered)
            }
            return Observable.just(.idle)
        }
}


