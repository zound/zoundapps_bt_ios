//
//  PlatformTraits.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 13/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

/// Protocol for validating device name
public protocol CustomNameValidator {
    /// Speaker's name validation
    ///
    /// - Parameter SpeakerName: new speaker name to be validated
    /// - Returns: true if speaker name is valid, false otherwise
    func validate(name: String) -> CustomNameValidationResult
}

public struct StockNameValidator: CustomNameValidator {
    public init() {}
    public func validate(name: String) -> CustomNameValidationResult { return .pass }
}

public enum CustomNameValidationResult {
    case pass
    case empty
    case whiteCharactersOnly
    case tooLong
}

public struct PlatformTraits {
    public var volumeMin: Int
    public var volumeMax: Int
    public var ledMin: Int
    public var ledMax: Int
    public var nameLengthLimit: Int
    public var nameValidator: CustomNameValidator
    public var connectionTimeout: TimeInterval

    public init(volumeMin: Int = Int(),
                volumeMax: Int = Int(),
                ledMin: Int = Int(),
                ledMax: Int = Int(),
                connectionTimeout: TimeInterval = TimeInterval.seconds(0),
                nameLengthLimit: Int = Int(),
                nameValidator: CustomNameValidator = StockNameValidator()) {
        self.volumeMin = volumeMin
        self.volumeMax = volumeMax
        self.ledMin = ledMin
        self.ledMax = ledMax
        self.connectionTimeout = connectionTimeout
        self.nameLengthLimit = nameLengthLimit
        self.nameValidator = nameValidator
    }
}
