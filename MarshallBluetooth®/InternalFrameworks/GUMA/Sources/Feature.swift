//
//  Feature.swift
//  GUMA
//
//  Created by Grzegorz Kiel on 04/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

public enum Feature {
    case hasDeviceInfo
    case hasContinuousVolume
    case hasGraphicalEqualizer
    case hasEqualizerSettings
    case hasActiveNoiceCancellingSettings
    case hasAutoOffTime
    case hasMButton
    case hasLedBrightnessControl
    case hasConnectivityStatus
    case hasCustomizableName
    case hasTrueWireless
    case hasSoundsControl
    case hasBatteryStatus
    case hasOTA
    case hasForcedOTA
    case hasSwitchableAudioSource
    case hasAudioPlayback
    case hasPairing
    case hasOnboarding
    case hasSoftwarePairingModeTrigger
}

public enum ConfigurationFeature: Int, CaseIterable, Comparable {
    public static func < (lhs: ConfigurationFeature, rhs: ConfigurationFeature) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
    case hasForcedOTA
    case hasPairing
    case hasOnboarding
    case hasMButton
    case hasOTA
    case hasBatteryStatus
    public init?(feature: Feature) {
        switch feature {
        case .hasForcedOTA:
            self = .hasForcedOTA
        case .hasPairing:
            self = .hasPairing
        case .hasOnboarding:
            self = .hasOnboarding
        case .hasMButton:
            self = .hasMButton
        case .hasOTA:
            self = .hasOTA
        case .hasBatteryStatus:
            self = .hasBatteryStatus
        default:
            return nil
        }
    }
}
