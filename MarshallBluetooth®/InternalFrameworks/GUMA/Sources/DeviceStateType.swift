//
//  DeviceStateType.swift
//  GUMA
//
//  Created by Grzegorz Kiel on 04/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift

public typealias NormalizedVolume = Float
public typealias Volume = Int
public typealias VolumeSteps = Int

public enum Monitor {
    case battery
    case powerCableAttached
    case volume
    case graphicalEqualizer
    case stepEqualizer
    case activeNoiseCancelling
    case autoOff
    case trueWireless
    case audioSource
    case currentTrack
    case pairingMode
}

public struct DeviceHardwareInfo {
    public let model: String
    public let firmwareVersion: String
    public init(model: String, firmwareVersion: String) {
        self.model = model
        self.firmwareVersion = firmwareVersion
    }
}
public struct WirelessStereoStatusWithId {
    public let status: WirelessStereoStatus
    public let id: Data
    public init() {
        status = WirelessStereoStatus()
        id = Data()
    }
    public init(status: WirelessStereoStatus, id: Data) {
        self.status = status
        self.id = id
    }
}
public enum ConnectivityStatus {
    case disconnected
    case notConfigured
    case readyToConnect
    case connecting
    case connected
    case wsMaster(peerMAC: MAC)
    case wsSlave(peerMAC: MAC)
    case error(Error?)
    
    public var connected: Bool {
        if case .connected = self { return true }
        return false
    }
}
public enum AccessoryPairingStatus {
    case idle
    case waitingForConfirmation
    case connected
    case disconnected
    case cancelled
}
public enum PowerCableStatus {
    case connected
    case disconnected
    case unknown
}
extension ConnectivityStatus: Equatable {
    public static func ==(lhs: ConnectivityStatus, rhs: ConnectivityStatus) -> Bool {
        switch (lhs, rhs) {
        case (.disconnected, .disconnected),
             (.notConfigured, .notConfigured),
             (.readyToConnect, .readyToConnect),
             (.connecting, .connecting),
             (.connected, .connected):
            return true
        case (.wsMaster(let lmac), .wsMaster(let rmac)):
            return lmac.hex == rmac.hex
        case (.wsSlave(let lmac), .wsSlave(let rmac)):
            return lmac.hex == rmac.hex
        case (.error(let lhsError), .error(let rhsError)):
            guard let lhsError = lhsError, let rhsError = rhsError else { return false }
            return lhsError.localizedDescription == rhsError.localizedDescription
        default:
            return false
        }
    }
}
public enum BatteryStatus {
    case unknown
    case charging
    case full
    case l10(Int)
    case l20(Int)
    case l30(Int)
    case l40(Int)
    case l50(Int)
    case l60(Int)
    case l70(Int)
    case l80(Int)
    case l90(Int)
}
public enum AudioSource {
    case bluetooth
    case aux
    case rca
    case unknown
}
extension AudioSource: Equatable {
    public static func == (lhs: AudioSource, rhs: AudioSource) -> Bool {
        switch (lhs, rhs) {
        case (.aux, .aux), (.rca, .rca), (.bluetooth, .bluetooth), (.unknown, .unknown): return true
        default: return false
        }
    }
}

/// Possible states of playback
///
/// - playing: Device is playing an audio track
/// - paused: Device paused playing an audio track
/// - stopped: Device stopped playing an audio track
/// - unknown: Cannot determine the playback status
public enum PlaybackStatus {
    case playing
    case paused
    case stopped
    case unknown
}

// MARK: - Helper extension (for example for distincting changes)
extension PlaybackStatus: Equatable {
    public static func == (lhs: PlaybackStatus, rhs: PlaybackStatus) -> Bool {
        switch (lhs, rhs) {
        case (.playing, .playing), (.paused, .paused), (.stopped, .stopped), (.unknown, .unknown): return true
        default: return false
        }
    }
}

public struct TrackInfo {
    public let title: String?
    public let artist: String?
    public let album: String?
    public let number: String?
    public let totalNumber: String?
    public let genre: String?
    public let playingTime: String?
    
    public init() {
        self.init(title: nil, artist: nil, album: nil, number: nil, totalNumber: nil, genre: nil, playingTime: nil)
    }
    
    public init(title: String?, artist: String?, album: String?, number: String?, totalNumber: String?, genre: String?, playingTime: String?) {
        self.title = title
        self.artist = artist
        self.album = album
        self.number = number
        self.totalNumber = totalNumber
        self.genre = genre
        self.playingTime = playingTime
    }
    
    public init(title: Any?, artist: Any?, album: Any?, number: Any?, totalNumber: Any?, genre: Any?, playingTime: Any?) {
        self.title = title as? String
        self.artist = artist as? String
        self.album = album as? String
        self.number = number as? String
        self.totalNumber = totalNumber as? String
        self.genre = genre as? String
        self.playingTime = playingTime as? String
    }
}

public enum SoundsControl {
    case powerCue(on: Bool)
    case mediaCue(on: Bool)
    
    public var isOn: Bool {
        switch self {
        case .powerCue(let on): return on
        case .mediaCue(let on): return on
        }
    }
    
    public var isPowerCue: Bool {
        switch self {
        case .powerCue(_): return true
        default: return false
        }
    }
    
    public var isMediaCue: Bool {
        switch self {
        case .mediaCue(_): return true
        default: return false
        }
    }
}

public protocol DeviceStateInput {
    func connect()
    func disconnect()
    func requestVolume()
    func startMonitoringVolume()
    func stopMonitoringVolume()
    func write(volume: Int)
    func requestHardwareInfo()
    func requestBatteryStatus()
    func startMonitoringBatteryStatus()
    func stopMonitoringBatteryStatus()
    func requestGraphicalEqualizer()
    func startMonitoringGraphicalEqualizer()
    func stopMonitoringGraphicalEqualizer()
    func write(graphicalEqualizer: GraphicalEqualizer)
    func requestEqualizerSettings()
    func startMonitoringEqualizerButtonStep()
    func stopMonitoringEqualizerButtonStep()
    func write(equalizerButtonStep: EqualizerButtonStep)
    func write(step2Preset: EqualizerButtonStepPreset)
    func write(step3Preset: EqualizerButtonStepPreset)
    func write(customPresetGraphicalEqualizer: GraphicalEqualizer)
    func requestActiveNoiseCancellingSettings()
    func startMonitoringActiveNoiseCancellingMode()
    func stopMonitoringActiveNoiseCancellingMode()
    func write(activeNoiseCancellingMode: ActiveNoiseCancellingMode)
    func write(activeNoiseCancellingLevel: Int)
    func write(monitoringLevel: Int)
    func requestAutoOffTime()
    func startMonitoringAutoOffTime()
    func stopMonitoringAutoOffTime()
    func write(autoOffTime: AutoOffTime)
    func requestMButtonMode()
    func write(mButtonMode: MButtonMode)
    func requestTrueWirelessStatus()
    func requestTrueWirelessStatusWithId()
    func startMonitoringTrueWirelessStatus()
    func stopMonitoringTrueWirelessStatus()
    func disconnectTrueWireless()
    func write(trueWirelessConnectWith slaveId: Data, completion: (() -> Void)?)
    func write(trueWirelessChannel: WirelessStereoChannel)
    func requestLedBrightness()
    func write(ledBrightness: Int)
    func requestSoundsControlStatus()
    func write(soundControl: SoundsControl)
    func set(connectivityStatus: ConnectivityStatus)
    func set(volume: Int)
    func requestPlayingInfoState()
    func refreshName()
    func rename(with: String)
    func forceOTA()
    func requestCurrentAudioSource()
    func startMonitoringAudioSources()
    func stopMonitoringAudioSources()
    func activateBluetooth()
    func activateAUX()
    func activateRCA()
    func requestPlaybackStatus()
    func play()
    func pause()
    func nextTrack()
    func previousTrack()
    func requestCurrentTrackInfo()
    func startMonitoringCurrentTrackInfo()
    func stopMonitoringCurrentTrackInfo()
    func activatePairingMode() -> Single<Void>
    func startMonitoringPairingMode()
    func stopMonitoringPairingMode()
    func requestPairingMode() -> Single<PairingMode>
    func showAccessoryPicker()
    func requestPowerCableStatus()
    func startMonitoringPowerCableStatus()
    func stopMonitoringPowerCableStatus()
    func append(monitor: Monitor)
    func remove(monitor: Monitor)
}

public protocol DeviceStateOutput {
    static var supportedFeatures: [Feature] { get }
    var perDeviceTypeFeatures: [Feature] { get }
    var activeMonitors: Set<Monitor> { get set }
    func name() -> AnyState<String>
    func volume() -> AnyState<Int>
    func monitoredVolume() -> AnyState<Int>
    func batteryStatus() -> AnyState<BatteryStatus>
    func monitoredBatteryStatus() -> AnyState<BatteryStatus>
    func hardwareInfoState() -> AnyState<DeviceHardwareInfo?>
    func graphicalEqualizer() -> AnyState<GraphicalEqualizer>
    func monitoredGraphicalEqualizer() -> AnyState<GraphicalEqualizer>
    func equalizerSettings() -> AnyState<EqualizerSettings>
    func monitoredEqualizerButtonStep() -> AnyState<EqualizerButtonStep>
    func activeNoiseCancellingSettings() -> AnyState<ActiveNoiseCancellingSettings>
    func monitoredActiveNoiseCancellingMode() -> AnyState<ActiveNoiseCancellingMode>
    func autoOffTime() -> AnyState<AutoOffTime>
    func monitoredAutoOffTime() -> AnyState<AutoOffTime>
    func mButtonMode() -> AnyState<MButtonMode>
    func trueWirelessStatus() -> AnyState<WirelessStereoStatus>
    func trueWirelessStatusWithId() -> AnyState<WirelessStereoStatusWithId>
    func monitoredTrueWirelessStatus() -> AnyState<WirelessStereoStatus?>
    func ledBrightness() -> AnyState<Int>
    func soundsControlStatus() -> AnyState<SoundsControl>
    func connectivityStatus() -> AnyState<ConnectivityStatus>
    func currentAudioSource() -> AnyState<AudioSource>
    func supportedAudioSources() -> [AudioSource]
    func playbackStatus() -> AnyState<PlaybackStatus>
    func trackInfo() -> AnyState<TrackInfo>
    func pairingMode() -> AnyState<PairingMode>
    func monitoredPairingMode() -> AnyState<PairingMode>
    func accessoryPairingStatus() -> AnyState<AccessoryPairingStatus>
    func powerCableStatus() -> AnyState<PowerCableStatus>
    func monitoredPowerCableStatus() -> AnyState<PowerCableStatus>
}

public protocol DeviceStateType {
    var inputs: DeviceStateInput { get }
    var outputs: DeviceStateOutput { get }
}

private let commonFatalErrorMessage = "Implement in concrete protocol adopter or unsupported feature"

public extension DeviceStateInput {
    func connect() { fatalError(commonFatalErrorMessage) }
    func disconnect() { fatalError(commonFatalErrorMessage) }
    func requestVolume() { fatalError(commonFatalErrorMessage) }
    func startMonitoringVolume() { fatalError(commonFatalErrorMessage) }
    func stopMonitoringVolume() { fatalError(commonFatalErrorMessage) }
    func write(volume: Int) { fatalError(commonFatalErrorMessage) }
    func requestHardwareInfo() { fatalError(commonFatalErrorMessage) }
    func requestBatteryStatus() { fatalError(commonFatalErrorMessage) }
    func requestGraphicalEqualizer() { fatalError(commonFatalErrorMessage) }
    func startMonitoringGraphicalEqualizer() { fatalError(commonFatalErrorMessage) }
    func stopMonitoringGraphicalEqualizer() { fatalError(commonFatalErrorMessage) }
    func write(graphicalEqualizer: GraphicalEqualizer) { fatalError(commonFatalErrorMessage) }
    func requestEqualizerSettings() { fatalError(commonFatalErrorMessage) }
    func startMonitoringEqualizerButtonStep() { fatalError(commonFatalErrorMessage) }
    func stopMonitoringEqualizerButtonStep() { fatalError(commonFatalErrorMessage) }
    func write(equalizerButtonStep: EqualizerButtonStep) { fatalError(commonFatalErrorMessage) }
    func write(step2Preset: EqualizerButtonStepPreset) { fatalError(commonFatalErrorMessage) }
    func write(step3Preset: EqualizerButtonStepPreset) { fatalError(commonFatalErrorMessage) }
    func write(customPresetGraphicalEqualizer: GraphicalEqualizer) { fatalError(commonFatalErrorMessage) }
    func requestActiveNoiseCancellingSettings() { fatalError(commonFatalErrorMessage) }
    func startMonitoringActiveNoiseCancellingMode() { fatalError(commonFatalErrorMessage) }
    func stopMonitoringActiveNoiseCancellingMode() { fatalError(commonFatalErrorMessage) }
    func write(activeNoiseCancellingMode: ActiveNoiseCancellingMode) { fatalError(commonFatalErrorMessage) }
    func write(activeNoiseCancellingLevel: Int) { fatalError(commonFatalErrorMessage) }
    func write(monitoringLevel: Int) { fatalError(commonFatalErrorMessage) }
    func requestAutoOffTime() { fatalError(commonFatalErrorMessage) }
    func startMonitoringAutoOffTime() { fatalError(commonFatalErrorMessage) }
    func stopMonitoringAutoOffTime() { fatalError(commonFatalErrorMessage) }
    func write(autoOffTime: AutoOffTime) { fatalError(commonFatalErrorMessage) }
    func requestMButtonMode() { fatalError(commonFatalErrorMessage) }
    func write(mButtonMode: MButtonMode) { fatalError(commonFatalErrorMessage) }
    func requestTrueWirelessStatus() { fatalError(commonFatalErrorMessage) }
    func requestTrueWirelessStatusWithId() { fatalError(commonFatalErrorMessage) }
    func startMonitoringTrueWirelessStatus() { fatalError(commonFatalErrorMessage) }
    func stopMonitoringTrueWirelessStatus() { fatalError(commonFatalErrorMessage) }
    func disconnectTrueWireless() { fatalError(commonFatalErrorMessage) }
    func write(trueWirelessConnectWith slaveId: Data, completion: (() -> Void)?) { fatalError(commonFatalErrorMessage) }
    func write(trueWirelessChannel: WirelessStereoChannel) { fatalError(commonFatalErrorMessage) }
    func requestLedBrightness() { fatalError(commonFatalErrorMessage) }
    func write(ledBrightness: Int) { fatalError(commonFatalErrorMessage) }
    func requestPlayingInfoState() { fatalError(commonFatalErrorMessage) }
    func set(connectivityStatus: ConnectivityStatus) { fatalError(commonFatalErrorMessage) }
    func set(volume: Int) { fatalError(commonFatalErrorMessage) }
    func refreshName() { fatalError(commonFatalErrorMessage) }
    func rename(with: String) { fatalError(commonFatalErrorMessage) }
    func forceOTA() { fatalError(commonFatalErrorMessage) }
    func requestCurrentAudioSource() { fatalError(commonFatalErrorMessage) }
    func startMonitoringAudioSources() { fatalError(commonFatalErrorMessage) }
    func stopMonitoringAudioSources() { fatalError(commonFatalErrorMessage) }
    func activateBluetooth() { fatalError(commonFatalErrorMessage) }
    func activateAUX() { fatalError(commonFatalErrorMessage) }
    func activateRCA() { fatalError(commonFatalErrorMessage) }
    func requestPlaybackStatus() { fatalError(commonFatalErrorMessage) }
    func play() { fatalError(commonFatalErrorMessage) }
    func pause() { fatalError(commonFatalErrorMessage) }
    func nextTrack() { fatalError(commonFatalErrorMessage) }
    func previousTrack() { fatalError(commonFatalErrorMessage) }
    func requestCurrentTrackInfo() { fatalError(commonFatalErrorMessage) }
    func startMonitoringCurrentTrackInfo() { fatalError(commonFatalErrorMessage) }
    func stopMonitoringCurrentTrackInfo() { fatalError(commonFatalErrorMessage) }
    func requestSoundsControlStatus() { fatalError(commonFatalErrorMessage) }
    func write(soundControl: SoundsControl) { fatalError(commonFatalErrorMessage) }
    func activatePairingMode() -> Single<Void> { fatalError(commonFatalErrorMessage) }
    func startMonitoringPairingMode() { fatalError(commonFatalErrorMessage) }
    func stopMonitoringPairingMode() { fatalError(commonFatalErrorMessage) }
    func requestPairingMode() -> Single<PairingMode> { fatalError(commonFatalErrorMessage) }
    func showAccessoryPicker() { fatalError(commonFatalErrorMessage) }
    func startMonitoringBatteryStatus() { fatalError(commonFatalErrorMessage) }
    func stopMonitoringBatteryStatus() { fatalError(commonFatalErrorMessage) }
    func requestPowerCableStatus() { fatalError(commonFatalErrorMessage) }
    func startMonitoringPowerCableStatus() { fatalError(commonFatalErrorMessage) }
    func stopMonitoringPowerCableStatus() { fatalError(commonFatalErrorMessage) }
    func append(monitor: Monitor) { fatalError(commonFatalErrorMessage) }
    func remove(monitor: Monitor) { fatalError(commonFatalErrorMessage) }
}

public extension DeviceStateOutput {
    func name() -> AnyState<String> { fatalError(commonFatalErrorMessage) }
    func volume() -> AnyState<Int> { fatalError(commonFatalErrorMessage) }
    func monitoredVolume() -> AnyState<Int> { fatalError(commonFatalErrorMessage) }
    func hardwareInfoState() -> AnyState<DeviceHardwareInfo?> { fatalError(commonFatalErrorMessage) }
    func batteryStatus() -> AnyState<BatteryStatus> { fatalError(commonFatalErrorMessage) }
    func graphicalEqualizer() -> AnyState<GraphicalEqualizer> { fatalError(commonFatalErrorMessage) }
    func monitoredGraphicalEqualizer() -> AnyState<GraphicalEqualizer> { fatalError(commonFatalErrorMessage) }
    func equalizerSettings() -> AnyState<EqualizerSettings> { fatalError(commonFatalErrorMessage) }
    func monitoredEqualizerButtonStep() -> AnyState<EqualizerButtonStep> { fatalError(commonFatalErrorMessage) }
    func activeNoiseCancellingSettings() -> AnyState<ActiveNoiseCancellingSettings> { fatalError(commonFatalErrorMessage) }
    func monitoredActiveNoiseCancellingMode() -> AnyState<ActiveNoiseCancellingMode> { fatalError(commonFatalErrorMessage) }
    func autoOffTime() -> AnyState<AutoOffTime> { fatalError(commonFatalErrorMessage) }
    func monitoredAutoOffTime() -> AnyState<AutoOffTime> { fatalError(commonFatalErrorMessage) }
    func mButtonMode() -> AnyState<MButtonMode> { fatalError(commonFatalErrorMessage) }
    func trueWirelessStatus() -> AnyState<WirelessStereoStatus> { fatalError(commonFatalErrorMessage) }
    func trueWirelessStatusWithId() -> AnyState<WirelessStereoStatusWithId> { fatalError(commonFatalErrorMessage) }
    func monitoredTrueWirelessStatus() -> AnyState<WirelessStereoStatus?> { fatalError(commonFatalErrorMessage) }
    func ledBrightness() -> AnyState<Int> { fatalError(commonFatalErrorMessage) }
    func soundsControlStatus() -> AnyState<SoundsControl> { fatalError(commonFatalErrorMessage) }
    func connectivityStatus() -> AnyState<ConnectivityStatus> { fatalError(commonFatalErrorMessage) }
    func currentAudioSource() -> AnyState<AudioSource> { fatalError(commonFatalErrorMessage) }
    func supportedAudioSources() -> [AudioSource] { fatalError(commonFatalErrorMessage) }
    func playbackStatus() -> AnyState<PlaybackStatus> { fatalError(commonFatalErrorMessage) }
    func trackInfo() -> AnyState<TrackInfo> { fatalError(commonFatalErrorMessage) }
    func pairingMode() -> AnyState<PairingMode> { fatalError(commonFatalErrorMessage) }
    func monitoredPairingMode() -> AnyState<PairingMode> { fatalError(commonFatalErrorMessage) }
    func accessoryPairingStatus() -> AnyState<AccessoryPairingStatus> { fatalError(commonFatalErrorMessage) }
    func monitoredBatteryStatus() -> AnyState<BatteryStatus> { fatalError(commonFatalErrorMessage) }
    func powerCableStatus() -> AnyState<PowerCableStatus> { fatalError(commonFatalErrorMessage) }
    func monitoredPowerCableStatus() -> AnyState<PowerCableStatus> { fatalError(commonFatalErrorMessage) }
}
public extension DeviceStateInput {
    func start(monitorType: Monitor) -> () -> Void {
        switch monitorType {
        case .battery:
            return startMonitoringBatteryStatus
        case .powerCableAttached:
            return startMonitoringPowerCableStatus
        case .volume:
            return startMonitoringVolume
        case .graphicalEqualizer:
            return startMonitoringGraphicalEqualizer
        case .stepEqualizer:
            return startMonitoringEqualizerButtonStep
        case .activeNoiseCancelling:
            return startMonitoringActiveNoiseCancellingMode
        case .autoOff:
            return startMonitoringAutoOffTime
        case .trueWireless:
            return startMonitoringTrueWirelessStatus
        case .audioSource:
            return startMonitoringAudioSources
        case .currentTrack:
            return startMonitoringCurrentTrackInfo
        case .pairingMode:
            return startMonitoringPairingMode
        }
    }
    func stop(monitorType: Monitor) -> () -> Void {
        switch monitorType {
        case .battery:
            return stopMonitoringBatteryStatus
        case .powerCableAttached:
            return stopMonitoringPowerCableStatus
        case .volume:
            return stopMonitoringVolume
        case .graphicalEqualizer:
            return stopMonitoringGraphicalEqualizer
        case .stepEqualizer:
            return stopMonitoringEqualizerButtonStep
        case .activeNoiseCancelling:
            return stopMonitoringActiveNoiseCancellingMode
        case .autoOff:
            return stopMonitoringAutoOffTime
        case .trueWireless:
            return stopMonitoringTrueWirelessStatus
        case .audioSource:
            return stopMonitoringAudioSources
        case .currentTrack:
            return stopMonitoringCurrentTrackInfo
        case .pairingMode:
            return stopMonitoringPairingMode
        }
    }
}
