//
//  OTAServiceType.swift
//  GUMA
//
//  Created by Grzegorz Kiel on 05/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift

public struct UpdateAvailableInfo {
    public let deviceID: String
    public let forced: Bool

    public init(deviceID: String, forced: Bool) {
        self.deviceID = deviceID
        self.forced = forced
    }
}

public final class FirmwareCache {
    public let blob: Data
    public let version: String
    
    public init(blob: Data, version: String) {
        self.blob = blob
        self.version = version
    }
}

public enum ApplicationProcessMode {
    case foreground
    case background
}

public func ==(lhs: UpdateAvailableInfo, rhs: UpdateAvailableInfo) -> Bool {
    return lhs.deviceID == rhs.deviceID
}

public protocol OTAServiceInput {
    /// Starts update for specific device
    func startUpdate(deviceID: String, interfaceError: BehaviorRelay<DeviceAdapterError?>)
    /// Checks if update available for selected device
    func checkUpdateAvailable(deviceID: String) -> Single<UpdateAvailableInfo?>
    /// Checks if selected device needs forced OTA
    func checkForcedOTA(deviceID: String) -> Single<UpdateAvailableInfo?>
    /// Triggered if device in OTA disconnects as a result of an error
    func deviceDisconnected(deviceID: String)
    /// Resets progress observable
    func resetProgress()
    func completeUpdate(deviceID: String)
}

public protocol OTAServiceOutput {
    /// devices for which update is available
    var updateAvailableInfo: BehaviorRelay<[UpdateAvailableInfo]> { get }
    /// Status of ongoing OTA for currently updating devices
    var updateProgressInfo: PublishSubject<[OTAUpdateInfo]> { get }
    /// ID's of devices currently in OTA
    var idsOfDevicesInOTA: [String] { get }
    /// Error triggered while in OTA
    var otaError: PublishRelay<OTAError> { get }
    /// Triggered when disconnected device in OTA rediscovers
    var rediscovered: PublishRelay<DeviceType> { get }
}

public protocol OTAServiceType {
    var inputs: OTAServiceInput { get }
    var outputs: OTAServiceOutput { get }
}

/**
 Service that manages OTA updates, for devices supporting it.
 */
public final class OTAService: OTAServiceInput, OTAServiceOutput, OTAServiceType {
    public var inputs: OTAServiceInput { return self }
    public var outputs: OTAServiceOutput { return self }
    
    /// info about updates available for specific devices
    public var updateAvailableInfo = BehaviorRelay<[UpdateAvailableInfo]>.init(value: [])
    /// Ids of devices currently in OTA
    public var idsOfDevicesInOTA: [String] = []
    /// Status of ongoing OTA for currently updating devices
    public var updateProgressInfo = PublishSubject<[OTAUpdateInfo]>()
    /// Error triggered while in OTA
    public var otaError = PublishRelay<OTAError>()
    /// Triggered when disconnected device in OTA rediscovers
    public var rediscovered = PublishRelay<DeviceType>()
    
    public init(deviceAdapterService: DeviceAdapterServiceType,
                interfaceError: BehaviorRelay<DeviceAdapterError?> = BehaviorRelay<DeviceAdapterError?>.init(value: nil),
                processMode: PublishRelay<ApplicationProcessMode>) {
        self.deviceAdapterService = deviceAdapterService
        self.interfaceError = interfaceError
        self.processMode = processMode
        
        self.deviceAdapterService.outputs.updatedDevice
            .filter { [unowned self] device in
                self.idsOfDevicesInOTA.contains(device.id)
            }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] device in
                self.deviceRediscovered(device)
            })
            .disposed(by: disposeBag)
        
        self.deviceAdapterService.outputs.addedDevice
            .subscribe(onNext: { [unowned self] newDevice in
                guard newDevice.configured == true else { return }
                var localDisposeBag = DisposeBag()
                self.checkUpdateAvailable(deviceID: newDevice.id)
                    .subscribe(
                        onSuccess: { _ in localDisposeBag = DisposeBag() },
                        onError: { error in localDisposeBag = DisposeBag() }
                    )
                    .disposed(by: localDisposeBag)
            })
            .disposed(by: disposeBag)
    }
    
    public func resetProgress() {
        updateProgressInfo = PublishSubject<[OTAUpdateInfo]>()
    }
    
    /**
     Starts OTA update for selected device
     
     ### How it works ###
     `OTAService` looks for OTA adapter associated with device requesting the update.
     
     Then it registers itself as an observer for OTA progress and propagates received values further using `updateProgressInfo` property
     
     ### Note ###
     It's assumed that only one device may be updating at specified time. Please consider adding more advanded `otaProgressDisposeBag` management for simultaneous updates
     
     - Parameter deviceID: ID of the device we want to update.
     - Parameter requestType: indicates if OTA was forced or triggered by user
     */
    public func startUpdate(deviceID: String,
                            interfaceError: BehaviorRelay<DeviceAdapterError?> = BehaviorRelay<DeviceAdapterError?>.init(value: nil)) {

        otaProgressDisposeBag = DisposeBag()
        
        let adapter = otaAdapter(for: deviceID)

        adapter.outputs.updateProgress.asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext: { [unowned self] updateInfo in
                    self.handle(updateInfo: updateInfo, for: deviceID)
                },
                onError: { [unowned self] error in
                    self.handleError(error)
                }
            )
            .disposed(by: otaProgressDisposeBag)

        if idsOfDevicesInOTA.contains(deviceID) == false {
            idsOfDevicesInOTA.append(deviceID)
        }
        adapter.inputs.startUpdate(cachedFirmware: firmwareCaches[deviceID],
                                   interfaceError: interfaceError,
                                   processMode: processMode)
    }
    public func completeUpdate(deviceID: String) {
        let adapter = otaAdapter(for: deviceID)
        adapter.inputs.completeUpdate(deviceID: deviceID)
    }
    public func checkUpdateAvailable(deviceID: String) -> Single<UpdateAvailableInfo?> {
        return otaAdapter(for: deviceID).inputs.checkUpdateAvailable(deviceID: deviceID)
            .do(onSuccess: { [weak self] updateInfo in
                guard let strongSelf = self else { return }
                var info = strongSelf.updateAvailableInfo.value
                guard let updateInfo = updateInfo else {
                    info = info.filter { $0.deviceID != deviceID }
                    strongSelf.updateAvailableInfo.accept(info)
                    return
                }
                info.append(updateInfo)
                strongSelf.updateAvailableInfo.accept(info)
        })
    }
    
    public func checkForcedOTA(deviceID: String) -> Single<UpdateAvailableInfo?> {
        return otaAdapter(for: deviceID).inputs.checkForcedOTA(deviceID: deviceID)
    }

    public func deviceDisconnected(deviceID: String) {
        otaAdapter(for: deviceID).inputs.deviceDisconnected()
        firmwareCaches.removeValue(forKey: deviceID)
        otaProgressDisposeBag = DisposeBag()
    }

    private var updateStatusInfo: [OTAUpdateInfo] = []
    private let disposeBag = DisposeBag()
    private var otaProgressDisposeBag = DisposeBag()
    private var otaAdapterInProgress: OTAAdapterType?
    private weak var deviceAdapterService: DeviceAdapterServiceType!
    private var firmwareCaches: [String: FirmwareCache] = [:]
    private var interfaceError: BehaviorRelay<DeviceAdapterError?>
    private var processMode: PublishRelay<ApplicationProcessMode>
}

private extension OTAService {

    func otaAdapter(for deviceID: String) -> OTAAdapterType {
        guard let otaAdapter = deviceAdapterService.outputs.managedDevices
            .first(where: { $0.id == deviceID })?.otaAdapter else {
                fatalError("Non exisiting ota adapter for device with id: \(deviceID)")
        }
        return otaAdapter
    }

    /**
     Device reconnected while being in OTA

     ### Explanation ###
     While doing OTA it's possible for device to reconnect. This may be associated with OTA procedure, it may also indicate that some kind of error appeared while in OTA
     
     - Parameter deviceID: ID of the device that reconnected.
    */
    func deviceRediscovered(_ device: DeviceType) {
        rediscovered.accept(device)
        startUpdate(deviceID: device.id)
    }
    func handle(updateInfo: OTAUpdateInfo, for deviceID: String) {
        var statusInfo = self.updateStatusInfo.filter({ $0.deviceID != deviceID })
        statusInfo.append(updateInfo)
        self.updateStatusInfo = statusInfo
        self.updateProgressInfo.onNext(self.updateStatusInfo)
        if case .downloadingFirmware(_, let cache) = updateInfo.status {
            firmwareCaches[deviceID] = cache
        }
        if case .completed = updateInfo.status {
            idsOfDevicesInOTA = idsOfDevicesInOTA.filter { $0 != deviceID }
            firmwareCaches.removeValue(forKey: deviceID)
            var updatesAvailable = updateAvailableInfo.value
            updatesAvailable = updatesAvailable.filter { $0.deviceID != deviceID }
            updateAvailableInfo.accept(updatesAvailable)
            completeUpdate(deviceID: deviceID)
        }
    }
    
    func handleError(_ error: Error) {
        self.updateProgressInfo.onError(error)
    }
}

