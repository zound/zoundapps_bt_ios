//
//  DeviceServiceType.swift
//  GUMA
//
//  Created by Grzegorz Kiel on 05/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift

public struct DisposableEntry {
    public let id: String
    public var disposeBag: DisposeBag
    
    public init(id: String, disposeBag: DisposeBag) {
        self.id = id
        self.disposeBag = disposeBag
    }
}

public struct DeviceListChanges {
    public let new: PublishRelay<DeviceType>
    public let updated: PublishRelay<DeviceType>
    public let removed: PublishRelay<DeviceType>
    
    public init(new: PublishRelay<DeviceType>,
                updated: PublishRelay<DeviceType>,
                removed: PublishRelay<DeviceType>) {
        self.new = new
        self.updated = updated
        self.removed = removed
    }
}
public protocol DeviceServiceInputs {
    func startBroadcasting()
}
public protocol DeviceServiceOutputs {
    var devices: BehaviorRelay<[DeviceType]> { get }
    var moved: PublishRelay<(IndexPath, IndexPath)> { get }
    var added: PublishRelay<(DeviceType, IndexPath)> { get }
    var removed: PublishRelay<IndexPath> { get }
    var updated: PublishRelay<(DeviceType, IndexPath)> { get }
}

public protocol DeviceServiceType: AnyObject {
    var outputs: DeviceServiceOutputs { get }
    var inputs: DeviceServiceInputs { get }
}

public final class DeviceService: DeviceServiceOutputs, DeviceServiceInputs, DeviceServiceType {
    public var devices = BehaviorRelay<[DeviceType]>.init(value: [])
    public var moved = PublishRelay<(IndexPath, IndexPath)>()
    public var added = PublishRelay<(DeviceType, IndexPath)>()
    public var removed = PublishRelay<IndexPath>()
    public var updated = PublishRelay<(DeviceType,IndexPath)>()
    
    public var outputs: DeviceServiceOutputs { return self }
    public var inputs: DeviceServiceInputs { return self }
    
    public init(deviceInfoSource: DeviceListChanges) {
        self.deviceInfoSource = deviceInfoSource
    
        /// Handle new devices
        self.deviceInfoSource.new
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] device in
                self.add(device)
            })
            .disposed(by: disposeBag)
    
        /// Handle re-connected devices
        self.deviceInfoSource.updated
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] device in
                self.update(device)
            })
            .disposed(by: disposeBag)
    
        /// Handle devices user wants to forget
        self.deviceInfoSource.removed
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] device in
                self.remove(device)
            })
            .disposed(by: disposeBag)
    }
    public func startBroadcasting() {
        broadcasting = true
    }
    private var broadcasting = false
    private let deviceInfoSource: DeviceListChanges
    private var disposeBag = DisposeBag()
    private var connectivityStateDisposables: [DisposableEntry] = []
}

private extension DeviceService {
    
    func sortSingle(lhs: DeviceType, rhs: DeviceType) -> Bool {
        let lhsConnectivityStatus = lhs.state.outputs.connectivityStatus().stateObservable().value
        let rhsConnectivityStatus = rhs.state.outputs.connectivityStatus().stateObservable().value

        switch (lhsConnectivityStatus, rhsConnectivityStatus) {
        case (.connected, .connected):
            return false
        case (.connected, _):
            return true
        default: return false
        }
    }
    func trackConnectivityStatusChange(for device: DeviceType) {
        /// Look for connectivity status changes and re-sort devices list if needed
        let disposable = disposableEntry(for: device.id)
        device.state.outputs.connectivityStatus().stateObservable()
            .distinctUntilChanged()
            .skipWhile { $0 != .connected && $0 != .disconnected }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] connectivityStatus in
                self.connectivityStatusChanged(device.id, connectivityStatus)
            })
            .disposed(by: disposable.disposeBag)
        connectivityStateDisposables.append(disposable)
    }
    func add(_ device: DeviceType) {
        var existingDevices = devices.value
        existingDevices.append(device)
        existingDevices.sort(by: sortSingle)
        guard let insertedAt = existingDevices.index(where: { $0.id == device.id }) else {
            fatalError("Could not find index for newly inserted device element! \(device.id)")
        }
        trackConnectivityStatusChange(for: device)
        devices.accept(existingDevices)
        guard broadcasting else { return }
        added.accept((device, IndexPath(item: insertedAt, section: 0)))
    }
    
    func update(_ device: DeviceType) {
        var existingDevices = devices.value
        guard let existingAt = existingDevices.index(where: { $0.id == device.id }) else {
            fatalError("Could not find index for newly inserted device element! \(device.id)")
        }
        existingDevices.remove(at: existingAt)
        existingDevices.insert(device, at: existingAt)
        devices.accept(existingDevices)
        trackConnectivityStatusChange(for: device)
        guard broadcasting else { return }
        updated.accept((device, IndexPath(item: existingAt, section: 0)))
    }
   
    func remove(_ device: DeviceType) {
        var existingDevices = devices.value
        guard let existingAt = existingDevices.index(where: { $0.id == device.id }) else {
            return
        }
        existingDevices.remove(at: existingAt)
        if let existingDisposableEntryIndex = connectivityStateDisposables.index(where: { $0.id == device.id }) {
            connectivityStateDisposables.remove(at: existingDisposableEntryIndex)
        }
        guard broadcasting else { return }
        devices.accept(existingDevices)
        removed.accept(IndexPath(item: existingAt, section: 0))
    }

    func disposableEntry(for deviceID: String) -> DisposableEntry {
        if let existingDisposableEntryIndex = connectivityStateDisposables.index(where: { $0.id == deviceID }) {
            connectivityStateDisposables.remove(at: existingDisposableEntryIndex)
        }
        let connectivityDisposableEntry = DisposableEntry(id: deviceID, disposeBag: DisposeBag())
        return connectivityDisposableEntry
    }
    
    func connectivityStatusChanged(_ id: String, _ status: ConnectivityStatus) {
        guard devices.value.count > 1  && broadcasting else { return }
        var existingDevices = devices.value
        guard let previousPositionIndex = existingDevices.index(where: { $0.id == id }) else {
            return
        }
        existingDevices.sort(by: sortSingle)
        guard let newPositionIndex = existingDevices.index(where: { $0.id == id }) else {
            fatalError("Could not find element with id :\(id)")
        }
        devices.accept(existingDevices)
        moved.accept((IndexPath(item: previousPositionIndex, section: 0),
                      IndexPath(item: newPositionIndex, section: 0)))
    }
}

