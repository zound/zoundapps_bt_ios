//
//  ActiveNoiceCancellingSettings.swift
//  MarshallBluetoothLibrary
//
//  Created by Wudarski Lukasz on 03/12/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

public enum ActiveNoiseCancellingMode: Int {
    case playBackOnly
    case activeNoiseCancelling
    case monitoring
}

public struct ActiveNoiseCancellingSettings: Equatable {
    public let mode: ActiveNoiseCancellingMode
    public let activeNoiseCancellingLevel: Int
    public let monitoringLevel: Int
    public init(mode: ActiveNoiseCancellingMode, activeNoiseCancellingLevel: Int, monitoringLevel: Int) {
        self.mode = mode
        self.activeNoiseCancellingLevel = activeNoiseCancellingLevel
        self.monitoringLevel = monitoringLevel
    }
    public init() {
        (self.mode, self.activeNoiseCancellingLevel, self.monitoringLevel) = (.playBackOnly, .zero, .zero)
    }
    public func changing(mode: ActiveNoiseCancellingMode) -> ActiveNoiseCancellingSettings {
        return ActiveNoiseCancellingSettings(mode: mode, activeNoiseCancellingLevel: activeNoiseCancellingLevel, monitoringLevel: monitoringLevel)
    }
    public func changing(activeNoiseCancellingLevel: Int) -> ActiveNoiseCancellingSettings {
        return ActiveNoiseCancellingSettings(mode: mode, activeNoiseCancellingLevel: activeNoiseCancellingLevel, monitoringLevel: monitoringLevel)
    }
    public func changing(monitoringLevel: Int) -> ActiveNoiseCancellingSettings {
        return ActiveNoiseCancellingSettings(mode: mode, activeNoiseCancellingLevel: activeNoiseCancellingLevel, monitoringLevel: monitoringLevel)
    }
}
