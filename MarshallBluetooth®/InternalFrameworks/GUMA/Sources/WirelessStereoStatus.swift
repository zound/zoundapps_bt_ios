//
//  WirelessStereoStatus.swift
//  GUMA
//
//  Created by Wudarski Lukasz on 19/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

public enum WirelessStereoChannel {
    case party, left, right
    
    init() {
        self = .party
    }
}

public enum WirelessStereoStatus {
    case connectedAsMaster(WirelessStereoChannel, oppositeDeviceMACAddress: Data)
    case connectedAsSlave(WirelessStereoChannel, oppositeDeviceMACAddress: Data)
    case disconnected
    
    public init() {
        self = .disconnected
    }
}

public typealias MasterId = String
public typealias SlaveId = String
public typealias WirelessStereoPair = (masterId: MasterId, slaveId: SlaveId)

public enum BasicWirelessStereoStatus {
    case connectedAsMaster(slaveId: SlaveId)
    case connectedAsSlave
    case disconnected
}
