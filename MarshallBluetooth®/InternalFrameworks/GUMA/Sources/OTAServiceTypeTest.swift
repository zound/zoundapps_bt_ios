//
//  OTAServiceTypeTest.swift
//  GUMA
//
//  Created by Grzegorz Kiel on 05/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import MarshallBluetoothLibrary
import GUMA

/* Mocked device entry from JSON with OTA update available
 {"name" : "FIRST", "id" : "00:a2:c2:d5:d4:a3", "playing" : false, "connected" : false, "hasUpdate" : true},
 */

extension OTAStatus {
    func testDescription() -> String {
        switch self {
        case .checkingFirmware: return "testCheckingFirmware"
        case .downloadingFirmware(_): return "testDownloadingFirmware"
        case .uploadingToDevice(_): return "testUploadingToDevice"
        case .uploadCompleted: return "testUploadCompleted"
        case .completed: return "testCompleted"
        }
    }
}

final class OTAServiceTypeTests: XCTestCase, OTAServiceOutput {
    
    var devicesToUpdate = BehaviorRelay<[UpdateAvailableInfo]>.init(value: [])
    var updateProgressInfo = PublishRelay<[OTAUpdateInfo]>()
    var otaUIRequest = PublishRelay<String>()
    
    var testDevicesToUpdate: TestableObserver<[UpdateAvailableInfo]>!
    var testUpdateProgressInfo: TestableObserver<[OTAUpdateInfo]>!
    var testOTAUIRequest: TestableObserver<String>!
    
    var jsonOTAService: OTAServiceType!
    
    var disposeBag: DisposeBag!
    
    var deviceAdapterService: DeviceAdapterServiceType!
    
    var testScheduler: TestScheduler!
    
    override func setUp() {
        
        super.setUp()
        
        disposeBag = DisposeBag()
        
        deviceAdapterService = DeviceAdapterService.shared
        deviceAdapterService.outputs.managedDevices.value.removeAll()
        
        let jsonAdapter = JSONMockDeviceAdapter()
        deviceAdapterService.inputs.append(adapter: jsonAdapter, mocked: true)
        deviceAdapterService.inputs.start()
        jsonAdapter.adapterState.accept(.ready)
        
        jsonOTAService = OTAService(devicesList: deviceAdapterService.outputs.managedDevices, immediateUpdateCheck: true)
        
        testScheduler = TestScheduler(initialClock: 0)
        
        testDevicesToUpdate = testScheduler.createObserver([UpdateAvailableInfo].self)
        testUpdateProgressInfo = testScheduler.createObserver([OTAUpdateInfo].self)
        testOTAUIRequest = testScheduler.createObserver(String.self)
        
        devicesToUpdate = jsonOTAService.outputs.devicesToUpdate
        updateProgressInfo = jsonOTAService.outputs.updateProgressInfo
        otaUIRequest = jsonOTAService.outputs.otaUIRequest
        
        let updateInfoExpectation = expectation(description: "Waiting for update info tick")
        
        _ = Observable<Int>
            .timer(RxTimeInterval(5), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                updateInfoExpectation.fulfill()
            }).disposed(by: disposeBag)
        
        waitForExpectations(timeout: 10, handler: nil)
        
        devicesToUpdate.asDriver().drive(testDevicesToUpdate).disposed(by: disposeBag)
        updateProgressInfo.asObservable().subscribe(testUpdateProgressInfo).disposed(by: disposeBag)
        otaUIRequest.asObservable().subscribe(testOTAUIRequest).disposed(by: disposeBag)
    }
    
    func testRequestUIIfTriggered() {
        testScheduler.start()
        
        self.jsonOTAService.inputs.requestUIForOTA(deviceID: "00:a2:c2:d5:d4:a3")
        XCTAssertEqual([Recorded.next(0, "00:a2:c2:d5:d4:a3")], testOTAUIRequest.events)
    }
    
    func testOTAProgress() {
        testScheduler.start()
        
        var otaStatus: [String] = []
        
        var skipNotification: Bool = false
        
        let otaProgressExpectation = expectation(description: "Waiting for OTA progress to finish")
        
        let expectedUniqueProgressEvents: [String] = OTAStatus.all().map { $0.testDescription() }
        
        jsonOTAService.outputs.updateProgressInfo
            .asObservable().subscribe(onNext: { updateInfo in
                let otaStatusInfo = updateInfo.first!
                let testDescription = otaStatusInfo.status.testDescription()
                if !otaStatus.contains(testDescription) {
                    otaStatus.append(testDescription)
                }
                if testDescription == "testCompleted" {
                    guard !skipNotification else { return }
                    XCTAssertTrue(expectedUniqueProgressEvents.joined() ==  otaStatus.joined())
                    otaProgressExpectation.fulfill()
                    skipNotification = true
                }
            }).disposed(by: disposeBag)
        
        jsonOTAService.inputs.startUpdate(deviceID: "00:a2:c2:d5:d4:a3", requestType: .normal)
        
        waitForExpectations(timeout: 10, handler: nil)
        
        
    }
    
}
