//
//  GraphicalEqualizerPreset.swift
//  GUMA
//
//  Created by Wudarski Lukasz on 11/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

public enum GraphicalEqualizerPreset: Equatable {

    case flat
    case rock
    case metal
    case pop
    case hipHop
    case electronic
    case jazz
    case custom(GraphicalEqualizer)

    static let `default`: GraphicalEqualizerPreset = .flat

    public static func == (lhs: GraphicalEqualizerPreset, rhs: GraphicalEqualizerPreset) -> Bool {
        return lhs.graphicalEqualizer == rhs.graphicalEqualizer
    }

    public var graphicalEqualizer: GraphicalEqualizer {
        switch self {
        case .flat:
            return GraphicalEqualizer.Preset.flat
        case .rock:
            return GraphicalEqualizer.Preset.rock
        case .metal:
            return GraphicalEqualizer.Preset.metal
        case .pop:
            return GraphicalEqualizer.Preset.pop
        case .hipHop:
            return GraphicalEqualizer.Preset.hipHop
        case .electronic:
            return GraphicalEqualizer.Preset.electronic
        case .jazz:
            return GraphicalEqualizer.Preset.jazz
        case .custom(let graphicalEqualizer):
            return graphicalEqualizer
        }
    }

    public init(graphicalEqualizer: GraphicalEqualizer) {
        switch graphicalEqualizer {
        case GraphicalEqualizer.Preset.flat:
            self = .flat
        case GraphicalEqualizer.Preset.rock:
            self = .rock
        case GraphicalEqualizer.Preset.metal:
            self = .metal
        case GraphicalEqualizer.Preset.pop:
            self = .pop
        case GraphicalEqualizer.Preset.hipHop:
            self = .hipHop
        case GraphicalEqualizer.Preset.electronic:
            self = .electronic
        case GraphicalEqualizer.Preset.jazz:
            self = .jazz
        default:
            self = .custom(graphicalEqualizer)
        }
    }

}

