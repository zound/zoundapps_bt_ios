//
//  WirelessStereoDataTypes.swift
//  GUMA
//
//  Created by Grzegorz Kiel on 03/02/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import Foundation

public struct WirelessStereoPairEntry {
    public var master: DeviceType?
    public var slave: DeviceType?
    public init() {}
}
public extension WirelessStereoPairEntry {
    mutating func add(device: DeviceType) {
        switch device.state.outputs.trueWirelessStatus().stateObservable().value {
        case let .connectedAsMaster(_, peerMAC):
            addEntry(master: device, slaveMAC: MAC(data: peerMAC))
        case let .connectedAsSlave(_, peerMAC):
            addEntry(slave: device, masterMAC: MAC(data: peerMAC))
        default: break
        }
    }
    private mutating func addEntry(master: DeviceType, slaveMAC: MAC) {
        guard slave == nil else {
            if slave?.mac?.hex == slaveMAC.hex {
                self.master = master
            }
            return
        }
        self.master = master
    }
    private mutating func addEntry(slave: DeviceType, masterMAC: MAC) {
        guard master == nil else {
            if master?.mac?.hex == masterMAC.hex {
                self.slave = slave
            }
            return
        }
        self.slave = slave
    }
    func verified() -> Bool {
        return master != nil && slave != nil
    }
    func existing() -> DeviceType? {
        guard self.master != nil else {
            return slave
        }
        return master
    }
    func pairExists(for device: DeviceType) -> Bool {
        let deviceConnectivityState = device.state.outputs.connectivityStatus().stateObservable().value
        switch deviceConnectivityState {
        case let .wsMaster(peerMAC):
            guard let slave = self.slave else { return false }
            if slave.mac?.hex == peerMAC.hex { return true }
        case let .wsSlave(peerMAC):
            guard let master = self.master else { return false }
            if master.mac?.hex == peerMAC.hex { return true }
        default: break
        }
        return false
    }
}

