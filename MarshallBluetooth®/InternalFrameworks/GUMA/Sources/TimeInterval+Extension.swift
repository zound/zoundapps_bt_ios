//
//  TimeInterval+Extension.swift
//  GUMA
//
//  Created by Bartosz Dolewski on 18.05.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift

// MARK: - Extension for TimeInterval to use bigger units than seconds and get better "unit system"
// Add further units if needed
public extension TimeInterval {
    
    /// Time interval for 1 second (to force unit-safety)
    public static func seconds(_ value: TimeInterval) -> RxTimeInterval {
        return RxTimeInterval(value)
    }
    
    /// Time interval for 1 minute (in seconds)
    static func minutes(_ value: TimeInterval) -> RxTimeInterval {
        return RxTimeInterval(value * seconds(60))
    }
    
    /// Time interaval for 1 hour (in seconds)
    static func hours(_ value: TimeInterval) -> RxTimeInterval {
        return RxTimeInterval(value * minutes(60))
    }
    
    /// Time interval for 24hours (working day) in seconds
    static func days(_ value: TimeInterval) -> RxTimeInterval {
        return RxTimeInterval(value * hours(24))
    }
    
    /// Print formatted interval
    ///
    /// - Returns: Time interval formatted to string aligned with current system locale settings
    func formatted() -> String {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .positional // Use the appropriate positioning according to current locale
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.zeroFormattingBehavior = [.pad] // Padding with zeros appropriate to current locale
        return formatter.string(from: self) ?? ""
    }
}
