//
//  GraphicalEqualizer.swift
//  GUMA
//
//  Created by Wudarski Lukasz on 11/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

public struct GraphicalEqualizer: Equatable, Codable {
    
    private enum GraphicalEqualizerDictionaryKey: String {
        case treble, upper, bass, low, mid
    }
    
    public static let max: UInt8 = 10
    
    public let bass: UInt8
    public let low: UInt8
    public let mid: UInt8
    public let upper: UInt8
    public let high: UInt8
    
    public struct Preset {
        public static let flat = GraphicalEqualizer(bass: 5, low: 5, mid: 5, upper: 5, high: 5)
        public static let rock = GraphicalEqualizer(bass: 8, low: 6, mid: 3, upper: 5, high: 7)
        public static let metal = GraphicalEqualizer(bass: 8, low: 3, mid: 5, upper: 7, high: 8)
        public static let pop = GraphicalEqualizer(bass: 6, low: 7, mid: 8, upper: 4, high: 5)
        public static let hipHop = GraphicalEqualizer(bass: 8, low: 7, mid: 6, upper: 5, high: 5)
        public static let electronic = GraphicalEqualizer(bass: 7, low: 4, mid: 4, upper: 7, high: 6)
        public static let jazz = GraphicalEqualizer(bass: 4, low: 7, mid: 5, upper: 4, high: 5)
        public static let customDefault = GraphicalEqualizer(bass: 10, low: 10, mid: 10, upper: 10, high: 10)
        public static let unknown = GraphicalEqualizer(bass: 0, low: 0, mid: 0, upper: 0, high: 0)
    }
    
    public var isCustom: Bool {
        return ![Preset.flat, Preset.rock, Preset.metal, Preset.pop, Preset.hipHop, Preset.electronic, Preset.jazz].contains(self)
    }
    
    public init() {
        (self.bass, self.low, self.mid, self.upper, self.high) = (UInt8.min, UInt8.min, UInt8.min, UInt8.min, UInt8.min)
    }
    
    public init(bass: UInt8, low: UInt8, mid: UInt8, upper: UInt8, high: UInt8) {
        self.bass = min(bass, GraphicalEqualizer.max)
        self.low = min(low, GraphicalEqualizer.max)
        self.mid = min(mid, GraphicalEqualizer.max)
        self.upper = min(upper, GraphicalEqualizer.max)
        self.high = min(high, GraphicalEqualizer.max)
    }
    
    public init?(graphicalEqualizerDictionary: [AnyHashable: Any]) {
        guard let graphicalEqualizerDictionaryUnified = graphicalEqualizerDictionary as? [String: UInt8] else {
            return nil
        }
        guard let bass = graphicalEqualizerDictionaryUnified[GraphicalEqualizerDictionaryKey.bass.rawValue],
            let low = graphicalEqualizerDictionaryUnified[GraphicalEqualizerDictionaryKey.low.rawValue],
            let mid = graphicalEqualizerDictionaryUnified[GraphicalEqualizerDictionaryKey.mid.rawValue],
            let upper = graphicalEqualizerDictionaryUnified[GraphicalEqualizerDictionaryKey.upper.rawValue],
            let high = graphicalEqualizerDictionaryUnified[GraphicalEqualizerDictionaryKey.treble.rawValue] else {
                return nil
        }
        self.init(bass: bass, low: low, mid: mid, upper: upper, high: high)
    }
    
}
