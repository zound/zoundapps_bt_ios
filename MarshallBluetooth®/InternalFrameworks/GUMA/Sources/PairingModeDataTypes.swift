//
//  PairingModeDataTypes.swift
//  GUMA
//
//  Created by Grzegorz Kiel on 23/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import Foundation

public enum PairingMode {
    case enabled
    case disabled
}

public enum PairingModeError: Error {
    case unknownState
}

