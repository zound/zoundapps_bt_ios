//
//  Version.swift
//  GUMA
//
//  Inspired by Bartosz Dolewski, who was inspired by Apple suggested approach on 12/09/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

public struct Version {
    let major: Int
    let minor: Int
    let patch: Int
    
    
    public init(version: String) {
        let parts = version.components(separatedBy: ".")
        self.init(major: parts.indices.contains(0) ? parts[0] : "0",
                  minor: parts.indices.contains(1) ? parts[1] : "0",
                  patch: parts.indices.contains(2) ? parts[2] : "0")
    }
    
    public init(major: String, minor: String, patch: String) {
        self.major = Int(major) ?? 0
        self.minor = Int(minor) ?? 0
        self.patch = Int(patch) ?? 0
    }
}

extension Version: Comparable {
    public static func == (lhs: Version, rhs: Version) -> Bool {
        return lhs.major == rhs.major && lhs.minor == rhs.minor && lhs.patch == rhs.patch
    }
    
    public static func < (lhs: Version, rhs: Version) -> Bool {
        if lhs.major != rhs.major {
            return lhs.major < rhs.major
        } else if lhs.minor != rhs.minor {
            return lhs.minor < rhs.minor
        } else {
            return lhs.patch < rhs.patch
        }
    }
}
