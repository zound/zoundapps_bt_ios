//
//  DeviceType.swift
//  GUMA
//
//  Created by Grzegorz Kiel on 04/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift

private let hexStringFormat = "%02X"
private let hex8BitSeparator = ":"

extension Collection where Element == UInt8 {
    var hexStringWith8BitSeparator: String {
        return map { String(format: hexStringFormat, $0) }.joined(separator: hex8BitSeparator)
    }
    subscript (optional index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

public struct MAC: Codable {
    public let hex: String
}

public extension MAC {
    init(data: Data) {
        self.hex = data.hexStringWith8BitSeparator
    }
    static func==(lhs: MAC, rhs: MAC) -> Bool {
        return lhs.hex == rhs.hex
    }
}

public struct StoredDeviceInfoEntry: Codable {
    public let id: String
    public let mac: MAC

    public init(id: String, mac: MAC) {
        self.id = id
        self.mac = mac
    }
}

public struct Platform {
    public var id: String
    public let traits: PlatformTraits
    
    public init(id: String, traits: PlatformTraits) {
        self.id = id
        self.traits = traits
    }
}

public func ==(lhs: Platform, rhs: Platform) -> Bool  {
    return lhs.id == rhs.id
}
public enum HardwareType {
    case speaker
    case headset
}
public protocol DeviceType: AnyObject {
    var platform: Platform { get }
    var id: String { get }
    var modelName: String { get }
    var image: DeviceImageProviding { get set }
    var state: DeviceStateType { get }
    var otaAdapter: OTAAdapterType? { get }
    var connectionTimeout: TimeInterval { get }
    var mac: MAC? { get set }
    var configured: Bool { get }
    var hardwareType: HardwareType { get }
    
    func bleConnect() -> Single<Void>
    func initialSetup(with storedEntry: StoredDeviceInfoEntry?) -> Single<String>
    func decouple() -> Single<Void>
    func resetSlave() -> Single<Void>
}

public extension DeviceType {
    func bleConnect() -> Single<Void> { fatalError("function \(#function) not implemented") }
    func initialSetup(with storedEntry: StoredDeviceInfoEntry?) -> Single<String> { fatalError("function \(#function) not implemented") }
    func decouple() -> Single<Void> { fatalError("function \(#function) not implemented") }
    func resetSlave() -> Single<Void> {fatalError("function \(#function) not implemented") }
}
