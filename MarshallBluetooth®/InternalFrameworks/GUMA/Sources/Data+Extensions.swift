//
//  Data+Extensions.swift
//  GUMA
//
//  Created by Bartosz Dolewski on 23.05.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

public extension Data {
    public var hexDescription: String {
        return reduce("") { $0 + String(format: "%02x", $1) }
    }
}
