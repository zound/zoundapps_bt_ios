//
//  WirelessStereoDeviceFriendlyNameProviding.swift
//  GUMA
//
//  Created by Wudarski Lukasz on 27/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

public protocol WirelessStereoDeviceFriendlyNameProviding {
    func slaveDeviceFriendlyName(device: DeviceType, deviceService: DeviceServiceType, preferences: DefaultStorage) -> String
    func masterDeviceFriendlyName(device: DeviceType, deviceService: DeviceServiceType, preferences: DefaultStorage) -> String
}

public extension WirelessStereoDeviceFriendlyNameProviding {
    
    func slaveDeviceFriendlyName(device: DeviceType, deviceService: DeviceServiceType, preferences: DefaultStorage) -> String {
        return deviceService.outputs.devices.value
            .first { $0.id == preferences.couplePairs[device.id] }
            .map { $0.state.outputs.name().stateObservable().value } ?? String()
    }
    
    func masterDeviceFriendlyName(device: DeviceType, deviceService: DeviceServiceType, preferences: DefaultStorage) -> String {
        return deviceService.outputs.devices.value
            .first { $0.id == preferences.couplePairs.keys(for: device.id).first }
            .map { $0.state.outputs.name().stateObservable().value } ?? String()
    }
}
