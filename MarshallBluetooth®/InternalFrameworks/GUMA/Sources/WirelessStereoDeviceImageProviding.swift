//
//  WirelessStereoDeviceImageProviding.swift
//  GUMA
//
//  Created by Dolewski Bartosz A (Ext) on 29.08.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

public protocol WirelessStereoDeviceImageProviding {
    func slaveDeviceImage(device: DeviceType, deviceService: DeviceServiceType, preferences: DefaultStorage) -> UIImage?
    func masterDeviceImage(device: DeviceType, deviceService: DeviceServiceType, preferences: DefaultStorage) -> UIImage?
}

public extension WirelessStereoDeviceImageProviding {
    
    func slaveDeviceImage(device: DeviceType, deviceService: DeviceServiceType, preferences: DefaultStorage) -> UIImage? {
        return deviceService.outputs.devices.value
            .first { $0.id == preferences.couplePairs[device.id] }
            .map { $0.image.smallImage }
    }
    
    func masterDeviceImage(device: DeviceType, deviceService: DeviceServiceType, preferences: DefaultStorage) -> UIImage? {
        return deviceService.outputs.devices.value
            .first { $0.id == preferences.couplePairs.keys(for: device.id).first }
            .map { $0.image.smallImage }
    }
}
