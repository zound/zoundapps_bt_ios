//
//  EqualizerSettings.swift
//  GUMA
//
//  Created by Wudarski Lukasz on 23/11/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation

public enum EqualizerButtonStep: Int {
    case step1
    case step2
    case step3
}

public enum EqualizerButtonStepPreset: Int {
    case flat
    case custom
    case rock
    case metal
    case pop
    case hipHop
    case electronic
    case jazz

    static let `default`: EqualizerButtonStepPreset = .flat

    public var graphicalEqualizer: GraphicalEqualizer? {
        switch self {
        case .flat:
            return GraphicalEqualizer.Preset.flat
        case .rock:
            return GraphicalEqualizer.Preset.rock
        case .metal:
            return GraphicalEqualizer.Preset.metal
        case .pop:
            return GraphicalEqualizer.Preset.pop
        case .hipHop:
            return GraphicalEqualizer.Preset.hipHop
        case .electronic:
            return GraphicalEqualizer.Preset.electronic
        case .jazz:
            return GraphicalEqualizer.Preset.jazz
        case .custom:
            return nil
        }
    }

    public init(graphicalEqualizer: GraphicalEqualizer) {
        switch graphicalEqualizer {
        case GraphicalEqualizer.Preset.flat:
            self = .flat
        case GraphicalEqualizer.Preset.rock:
            self = .rock
        case GraphicalEqualizer.Preset.metal:
            self = .metal
        case GraphicalEqualizer.Preset.pop:
            self = .pop
        case GraphicalEqualizer.Preset.hipHop:
            self = .hipHop
        case GraphicalEqualizer.Preset.electronic:
            self = .electronic
        case GraphicalEqualizer.Preset.jazz:
            self = .jazz
        default:
            self = .custom
        }
    }
}

public struct EqualizerSettings: Equatable {
    public let step: EqualizerButtonStep
    public let step2Preset: EqualizerButtonStepPreset
    public let step3Preset: EqualizerButtonStepPreset
    public let customGraphicalEqualizer: GraphicalEqualizer
    public init(step: EqualizerButtonStep, step2Preset: EqualizerButtonStepPreset, step3Preset: EqualizerButtonStepPreset, customGraphicalEqualizer: GraphicalEqualizer) {
        self.step = step
        self.step2Preset = step2Preset
        self.step3Preset = step3Preset
        self.customGraphicalEqualizer = customGraphicalEqualizer
    }
    public init(limitedEqualizerSettings: LimitedEqualizerSettings, customGraphicalEqualizer: GraphicalEqualizer) {
        self.step = limitedEqualizerSettings.step
        self.step2Preset = limitedEqualizerSettings.step2Preset
        self.step3Preset = limitedEqualizerSettings.step3Preset
        self.customGraphicalEqualizer = customGraphicalEqualizer
    }
    public init() {
        (self.step, self.step2Preset, self.step3Preset, self.customGraphicalEqualizer) = (.step1, .flat, .flat, GraphicalEqualizer())
    }
    public func changing(step: EqualizerButtonStep) -> EqualizerSettings {
        return EqualizerSettings(step: step, step2Preset: step2Preset, step3Preset: step3Preset, customGraphicalEqualizer: customGraphicalEqualizer)
    }
    public func changing(step2Preset: EqualizerButtonStepPreset) -> EqualizerSettings {
        return EqualizerSettings(step: step, step2Preset: step2Preset, step3Preset: step3Preset, customGraphicalEqualizer: customGraphicalEqualizer)
    }
    public func changing(step3Preset: EqualizerButtonStepPreset) -> EqualizerSettings {
        return EqualizerSettings(step: step, step2Preset: step2Preset, step3Preset: step3Preset, customGraphicalEqualizer: customGraphicalEqualizer)
    }
    public func changing(customGraphicalEqualizer: GraphicalEqualizer) -> EqualizerSettings {
        return EqualizerSettings(step: step, step2Preset: step2Preset, step3Preset: step3Preset, customGraphicalEqualizer: customGraphicalEqualizer)
    }
}

public struct LimitedEqualizerSettings {
    public let step: EqualizerButtonStep
    public let step2Preset: EqualizerButtonStepPreset
    public let step3Preset: EqualizerButtonStepPreset
    public init(step: EqualizerButtonStep, step2Preset: EqualizerButtonStepPreset, step3Preset: EqualizerButtonStepPreset) {
        self.step = step
        self.step2Preset = step2Preset
        self.step3Preset = step3Preset
    }
    public init() {
        (self.step, self.step2Preset, self.step3Preset) = (.step1, .flat, .flat)
    }
}
