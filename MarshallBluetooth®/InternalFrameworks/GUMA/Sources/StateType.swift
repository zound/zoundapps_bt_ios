//
//  StateType.swift
//  GUMA
//
//  Created by Grzegorz Kiel on 04/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift


public protocol StateType {
    associatedtype ObservedValue
    
    var notificationSupported: Bool { get }
    
    func associatedFeature() -> Feature
    func stateObservable() -> BehaviorRelay<ObservedValue>
}

public struct AnyState<T>: StateType {
    public var notificationSupported: Bool
    public typealias ObservedValue = T
    
    public init(feature: Feature, supportsNotifications: Bool, initialValue: () -> T) {
        self.feature = feature
        self.notificationSupported = supportsNotifications
        switch feature {
        default:
            self.observable = BehaviorRelay<T>.init(value: initialValue())
        }
    }
    
    public func associatedFeature() -> Feature {
        return feature
    }
    
    public func stateObservable() -> BehaviorRelay<T> {
        return observable
    }
    
    private let observable: BehaviorRelay<T>
    private let feature: Feature
}

