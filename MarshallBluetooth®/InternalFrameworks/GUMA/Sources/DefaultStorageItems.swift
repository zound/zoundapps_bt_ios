//
//  DefaultStorageItems.swift
//  MarshallBluetooth®
//
//  Created by Grzegorz Kiel on 05/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

public typealias DefaultStorageItems = GeneralStorageItems &
                                       EqualizerStorageItems &
                                       CoupleSpeakerStorageItems &
                                       SubscriptionStorageItems &
                                       ShareAnalyticsStorageItems &
                                       StoredDevicesInfoItems

public protocol GeneralStorageItems {
    var startPressed: Bool { set get }
    var signupCompleted: Bool { set get }
}

public protocol EqualizerStorageItems {
    var customDefaultGraphicalEqualizer: GraphicalEqualizer? { set get }
}

public protocol CoupleSpeakerStorageItems {
    var couplePairs: [MasterId: SlaveId] { set get }
    var lastConfiguredCouplePairs: [MasterId: SlaveId] { set get }
}

public protocol SubscriptionStorageItems {
    var subscriptionEmail: String { set get }
}

public protocol ShareAnalyticsStorageItems {
    var shareAnalyticsData: Bool { set get }
}

public protocol StoredDevicesInfoItems {
    var storedDevicesInfo: [StoredDeviceInfoEntry]? { set get }
}
