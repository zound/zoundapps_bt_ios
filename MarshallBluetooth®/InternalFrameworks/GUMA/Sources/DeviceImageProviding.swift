//
//  DeviceImageProviding.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 24/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import UIKit

public typealias DeviceImageProviding = SmallDeviceImageProviding & MediumDeviceImageProviding & LargeDeviceImageProviding & PairingSetupDeviceImageProviding

public protocol SmallDeviceImageProviding {
    var smallImage: UIImage { get }
}
public protocol MediumDeviceImageProviding {
    var mediumImage: UIImage { get }
}
public protocol LargeDeviceImageProviding {
    var largeImage: UIImage { get }
}
public protocol PairingSetupDeviceImageProviding {
    var pairingSetupImage: UIImage { get }
}
