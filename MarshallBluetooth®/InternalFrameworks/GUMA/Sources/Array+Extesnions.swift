//
//  Array+Extesnions.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 27/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

public extension Array where Element == DeviceType {

    public func appending(_ newDevice: DeviceType) -> [DeviceType] {
        var devices = self
        devices.append(newDevice)
        return devices
    }

    public func appending(_ newDevices: [DeviceType]) -> [DeviceType] {
        var devices = self
        devices.append(contentsOf: newDevices)
        return devices
    }

    public func removing(_ device: DeviceType) -> [DeviceType] {
        guard let index = index(where: { $0.id == device.id }) else {
            return self
        }
        var devices = self
        devices.remove(at: index)
        return devices
    }

    public func contains(device: DeviceType) -> Bool {
        return contains(where: { $0.id == device.id })
    }

}
