//
//  DeviceAdapterType.swift
//  GUMA
//
//  Created by Grzegorz Kiel on 22/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift

public enum DeviceAdapterState {
    case initializing
    case btDisabled
    case ready
}

public protocol DeviceAdapterType: AnyObject {
    func startScan(mocked: Bool)
    func stopScan()
    func managesDevice(deviceID: String) -> Bool
    func forget(device: DeviceType)
    func configurationInProgress() -> Bool
    func setupDevices() -> Single<Void>
    static var platform: Platform { get }
    var discovered: PublishRelay<DeviceType> { get }
    var removed: PublishRelay<DeviceType> { get }
    /// Property that determines if adapter is ready to detect devices.
    /// It may take some time for adapter to initialize and we need to take that into account
    var adapterState: BehaviorRelay<DeviceAdapterState> { get }
    var scanInProgress: BehaviorRelay<Bool> { get }
    var preprocessingDiscoveredInProgress: BehaviorRelay<Bool> { get }
}
