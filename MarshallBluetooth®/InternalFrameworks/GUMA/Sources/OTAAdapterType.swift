//
//  OTAAdapterType.swift
//  GUMA
//
//  Created by Grzegorz Kiel on 05/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import RCER

public enum OTARequestType {
    case none
    case normal
    case forced
}

public enum OTAStatus {
    case connecting
    case checkingFirmware
    case downloadingFirmware(Float, FirmwareCache?)
    case uploadingToDevice(Float)
    case uploadCompleted(Float)
    case completed
    
    public static func all() -> [OTAStatus] {
        return [.connecting,
                .checkingFirmware,
                .downloadingFirmware(0.0, nil),
                .uploadingToDevice(0.0),
                .uploadCompleted(0.0),
                .completed]
    }
}

extension OTAStatus: Equatable {
    public static func == (lhs: OTAStatus, rhs: OTAStatus) -> Bool {
        switch (lhs, rhs) {
        case (.connecting, .connecting),
             (.checkingFirmware, checkingFirmware),
             (.downloadingFirmware, .downloadingFirmware),
             (.uploadingToDevice, .uploadingToDevice),
             (.uploadCompleted, .uploadCompleted),
             (.completed, .completed):
            return true
        default:
            return false
        }
    }
}

public enum OTAError: Error {
    case unknownDeviceType
    case bluetoothOff
    case internetNotAvailable
    case deviceDisconnected
    case errorParsingRemoteURL
    case unknownRemoteFwVersion
    case unknownRemoteDownloadURL
    case emptyLocalOrRemoteFirmwareVersion
    case deviceInOTANotFoundInManagedDevicesList
    case unableToCreateDownloadURL
    case failedToCreateUnzippedFolder
    case failedToUnzipFirmwareFiles
    case failedToCalculateMD5Checksum
    case md5checksumMismatch
    case urlSessionError(Error)
    case unknownRemoteVersion
    case otherError
}

extension OTAError: Equatable {
    public static func == (lhs: OTAError, rhs: OTAError) -> Bool {
        switch (lhs, rhs) {
        case (.unknownDeviceType, .unknownDeviceType),
             (.bluetoothOff, .bluetoothOff),
             (.internetNotAvailable, .internetNotAvailable),
             (.deviceDisconnected, .deviceDisconnected),
             (.unknownRemoteFwVersion, .unknownRemoteFwVersion),
             (.unknownRemoteDownloadURL, unknownRemoteDownloadURL),
             (.emptyLocalOrRemoteFirmwareVersion, .emptyLocalOrRemoteFirmwareVersion),
             (.deviceInOTANotFoundInManagedDevicesList, .deviceInOTANotFoundInManagedDevicesList),
             (.unableToCreateDownloadURL, .unableToCreateDownloadURL),
             (.failedToCreateUnzippedFolder, .failedToCreateUnzippedFolder),
             (.failedToUnzipFirmwareFiles, .failedToUnzipFirmwareFiles),
             (.failedToCalculateMD5Checksum, .failedToCalculateMD5Checksum),
             (.md5checksumMismatch, .md5checksumMismatch),
             (.urlSessionError, .urlSessionError),
             (.unknownRemoteVersion, .unknownRemoteVersion),
             (.otherError, .otherError): return true
        default: return false
        }
    }
}

public struct OTAUpdateInfo {
    public var deviceID: String
    public var status: OTAStatus
    public var error: OTAError?

    public init(deviceID: String, status: OTAStatus, error: OTAError?) {
        self.deviceID = deviceID
        self.status = status
        self.error = error
    }
}

public protocol OTAAdapterTypeInput {
    func startUpdate(cachedFirmware: FirmwareCache?,
                     interfaceError: BehaviorRelay<DeviceAdapterError?>,
                     processMode: PublishRelay<ApplicationProcessMode>)
    func completeUpdate(deviceID: String)
    func deviceDisconnected()
    func checkUpdateAvailable(deviceID: String) -> Single<UpdateAvailableInfo?>
    func checkForcedOTA(deviceID: String) -> Single<UpdateAvailableInfo?>
    func cancel()
}

public protocol OTAAdapterTypeOutput {
    var updateAvailable: PublishSubject<Bool> { get }
    var updateProgress: PublishSubject<OTAUpdateInfo> { get }
}

/**
 Defines OTA Adapter requirements, separated in two groups: `inputs` and `outputs`, that every conforming OTA adapter must implement.
 
 `inputs`:
 * `startUpdate()` - starts OTA update
 * `deviceDisconnected()` - device being in OTA disconnected. It can be expected, can be also result of an error
 * `cancel()` - skip updating and clean all responsible data

 
 `outputs`:
 * `updateAvailable` - it OTA update for this device is available, event containing id of the device is triggered.
 * `updateProgress` - triggers events containing current status of the OTA update for this adapter. Please check `OTAUpdateInfo` structure and `OTAStatus` enum for further details.
 
 - SeeAlso: OTAUpdateInfo, OTAStatus
 */
public protocol OTAAdapterType: AnyObject {
    var inputs: OTAAdapterTypeInput { get }
    var outputs: OTAAdapterTypeOutput { get }
    var deviceID: String { get }
    var adapterError: BehaviorRelay<AnyError<OTAError>?> { get }
}
