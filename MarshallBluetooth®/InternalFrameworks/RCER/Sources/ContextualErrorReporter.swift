//
//  ContextualErrorReporter.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 19/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

public protocol ContextualErrorReporter: AnyObject {
    associatedtype Context
    associatedtype ErrorType
    associatedtype ErrorViewControllerType
    associatedtype ErrorViewModelType
    
    var errorViewController: ErrorViewControllerType? { get set }
    
    func configureErrorSources(for types: [ErrorType])
    func setupErrorActions(for viewController: ErrorViewControllerType,
                           with context: Context,
                           error type: ErrorType)
    func handleError(_ error: ErrorType)
    func viewModel(for errorType: ErrorType) -> ErrorViewModelType?
    func errorViewController(for errorType: ErrorType) -> ErrorViewControllerType?
    func showError(parentViewController: UIViewController?, _ error: ErrorType)
    func dismissError(completion: @escaping () -> Void)
}


