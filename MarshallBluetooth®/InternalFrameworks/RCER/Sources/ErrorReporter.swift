//
//  ErrorReporter.swift
//  MarshallBluetooth®
//
//  Created by Grzegorz Kiel on 23/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

//
//  ErrorReporter.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 17/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public struct AnyError<T: Equatable> {
    public var id: T

    public init(id: T) {
        self.id = id
    }
}

extension AnyError: Error {}

extension AnyError: Equatable {
    public static func==(lhs: AnyError<T>, rhs: AnyError<T>) -> Bool {
        return lhs.id == rhs.id
    }
}

final public class ErrorSource<T: Equatable> {
    let type: T
    let disposeBag = DisposeBag()
    /// The lower the number the higher the priority
    var priority: Int
    
    public func trigger() -> BehaviorRelay<AnyError<T>?> {
        return _trigger
    }
    
    public init(type: T, trigger: BehaviorRelay<AnyError<T>?>, priority: Int = 0) {
        self.type = type
        self._trigger = trigger
        self.priority = priority
    }
    
    private let _trigger: BehaviorRelay<AnyError<T>?>
}

public protocol ErrorReporterInput {
    associatedtype ErrorType: Equatable
    
    func append(errorSource: ErrorSource<ErrorType>)
    func reset()
    func remove(sourceForType: ErrorType)
}

public protocol ErrorReporterOutput {
    associatedtype ErrorType: Equatable
    var currentError: BehaviorRelay<AnyError<ErrorType>?> { get }
    var tracker: InternetConnectionStatusTracker { get }
}

public final class ErrorReporter<T: Equatable> {
    public var currentError = BehaviorRelay<AnyError<T>?>(value: nil)
    public let tracker: InternetConnectionStatusTracker
    
    public init(onlineTracker: InternetConnectionStatusTracker) {
        self.tracker = onlineTracker
        self.tracker.start()
        self.currentError = BehaviorRelay<AnyError<T>?>(value: nil)
    }
    
    private func contains(_ source: ErrorSource<T>) -> Bool {
        return errorSources.contains(where: { $0.type == source.type })
    }
    
    private func source(for type: T) -> ErrorSource<T>? {
        return errorSources.first {  $0.type == type }
    }
    
    private func handleError(_ type: T) {
        guard let errorSource = source(for: type) else {
            fatalError("No source found for error type \(type)")
        }
        
        guard let topPrioritySource = errorSources
            .sorted(by: { $0.priority < $1.priority })
            .first(where: { $0.trigger().value != nil } ) else {
            currentError.accept(errorSource.trigger().value)
            return
        }
        currentError.accept(topPrioritySource.trigger().value)
    }
    
    deinit {
        tracker.stop()
    }
    
    private var errorSources = [ErrorSource<T>]()
}

extension ErrorReporter: ErrorReporterInput, ErrorReporterOutput {

    public func append(errorSource: ErrorSource<T>) {
        guard contains(errorSource) == false else { return }
        errorSources.append(errorSource)
        errorSource.trigger()
            .subscribe(onNext: { [weak self, unowned errorSource] error in
                guard let error = error else {
                    self?.handleError(errorSource.type)
                    return
                }
                self?.handleError(error.id)
            }).disposed(by: errorSource.disposeBag)
    }
    
    public func reset() {
        errorSources.forEach {
            if currentError.value?.id == $0.type {
                currentError.accept(nil)
            }
        }
        currentError.accept(nil)
        errorSources.removeAll()
    }

    public func remove(sourceForType type: T) {
        errorSources = errorSources.filter { $0.type != type }
        guard let topPrioritySource = errorSources
            .sorted(by: { $0.priority < $1.priority })
            .first(where: { $0.trigger().value != nil } ) else {
                currentError.accept(nil)
                return
        }
        currentError.accept(topPrioritySource.trigger().value)
    }
}
