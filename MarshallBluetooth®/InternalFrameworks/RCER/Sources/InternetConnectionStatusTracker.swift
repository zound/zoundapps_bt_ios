//
//  InternetConnectionStatusTracker.swift
//  MarshallBluetoothLibrary
//
//  Created by Grzegorz Kiel on 19/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Reachability
import RxSwift

public protocol InternetConnectionStatusTracker {
    func start()
    func stop()
    func online() -> Observable<Bool>
}

extension Reachability: InternetConnectionStatusTracker {
    public func start() {
        try? startNotifier()
    }
    
    public func stop() {
        stopNotifier()
    }
    
    public func online() -> Observable<Bool> {
        let initialConnection = self.connection
        return rx.reachable.startWith(initialConnection == .none ? false : true)
    }
}
