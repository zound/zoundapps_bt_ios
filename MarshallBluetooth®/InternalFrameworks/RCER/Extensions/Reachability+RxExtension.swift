//
//  Reachability+RxExtension.swift
//  MarshallBluetoothLibrary
//
//  Created by Dolewski Bartosz A (Ext) on 12.07.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import Reachability
import RxCocoa
import RxSwift

extension Reachability: ReactiveCompatible { }

// MARK: - Rx Reachability to support static instances
public extension Reactive where Base: Reachability {
    
    public static var reachabilityChanged: Observable<Reachability> {
        return NotificationCenter.default.rx.notification(Notification.Name.reachabilityChanged)
            .flatMap { notification -> Observable<Reachability> in
                guard let reachability = notification.object as? Reachability else { return .empty() }
                return .just(reachability) }
    }
    
    public static var status: Observable<Reachability.Connection> {
        return reachabilityChanged
            .map { $0.connection }
    }
    
    public static var reachable: Observable<Bool> {
        return reachabilityChanged
            .map { $0.connection != .none }
    }
    
    public static var connected: Observable<Void> {
        return reachable
            .filter { $0 == true }
            .map { _ in Void() }
    }
    
    public static var disconnected: Observable<Void> {
        return reachable
            .filter { $0 == false }
            .map { _ in Void() }
    }
}

// MARK: - Rx Reachability to support local instances
public extension Reactive where Base: Reachability {
    
    public var reachabilityChanged: Observable<Reachability> {
        return NotificationCenter.default.rx.notification(Notification.Name.reachabilityChanged, object: base)
            .flatMap { notification -> Observable<Reachability> in
                guard let reachability = notification.object as? Reachability else { return .empty() }
                return .just(reachability) }
    }
    
    public var status: Observable<Reachability.Connection> {
        return reachabilityChanged
            .map { $0.connection }
    }
    
    public var reachable: Observable<Bool> {
        return reachabilityChanged
            .map { $0.connection != .none }
    }
    
    public var connected: Observable<Void> {
        return reachable
            .filter { $0 }
            .map { _ in Void() }
    }
    
    public var disconnected: Observable<Void> {
        return reachable
            .filter { !$0 }
            .map { _ in Void() }
    }
}
