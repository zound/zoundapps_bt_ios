//
//  JoplinPlatform.h
//  JoplinPlatform
//
//  Created by Grzegorz Kiel on 05/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for JoplinPlatform.
FOUNDATION_EXPORT double JoplinPlatformVersionNumber;

//! Project version string for JoplinPlatform.
FOUNDATION_EXPORT const unsigned char JoplinPlatformVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <JoplinPlatform/PublicHeader.h>
#import "RxCocoa/RxCocoa.h"

