//
//  TymphanyDeviceAdapter.swift
//  JoplinPlatform
//
//  Created by Grzegorz Kiel on 22/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import TAPlatformSwift
import GUMA

public enum TymphanyConnectMode {
    case discovery
    case setup
    case decouple
    case checkFirmware
    case colorRequest
    case ota
    case idle
}
private struct Config {
    static let volumeMin = Int()
    static let volumeMax = 30
    static let ledMin = 35
    static let ledMax = 70
    static let connectionTimeout = TimeInterval.seconds(10)
    static let nameLengthLimit = 17
    static let nameValidator = JoplinCustomNameValidator()
}

struct JoplinCustomNameValidator: CustomNameValidator {
    
    func validate(name: String) -> CustomNameValidationResult {
        
        // empty name is obviously not acceptable
        guard name.isEmpty == false else {
            return .empty
        }
        
        // name that contains only white characters (for example only spaces) is not acceptable
        guard name.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == false else {
            return .whiteCharactersOnly
        }
        
        let limit = TymphanyDeviceAdapter.platform.traits.nameLengthLimit
        return name.utf8.count <= limit ? .pass : .tooLong
    }
}

public class TymphanyDeviceAdapter: DeviceAdapterType {
    
    public func configurationInProgress() -> Bool {
        return systemService.configurationInProgress()
    }
    
    public func set(mode: TymphanyConnectMode, for deviceID: String) {
        systemService.mode(mode, for: deviceID)
    }
    
    public var discovered = PublishRelay<DeviceType>()
    public var removed = PublishRelay<DeviceType>()
    
    public var adapterState = BehaviorRelay<DeviceAdapterState>.init(value: .initializing)
    public var scanInProgress = BehaviorRelay<Bool>.init(value: false)
    public var preprocessingDiscoveredInProgress = BehaviorRelay<Bool>.init(value: false)
    
    public static var platform = Platform(id: "joplinBT",
                                          traits: PlatformTraits(volumeMin: Config.volumeMin,
                                                                 volumeMax: Config.volumeMax,
                                                                 ledMin: Config.ledMin,
                                                                 ledMax: Config.ledMax,
                                                                 connectionTimeout: Config.connectionTimeout,
                                                                 nameLengthLimit: Config.nameLengthLimit,
                                                                 nameValidator: Config.nameValidator ))
    public static let shared = TymphanyDeviceAdapter()
    
    public func startScan(mocked: Bool = false) {
        scanInProgress.accept(true)
        systemService.globalMode(.discovery)
        systemService.service.scanForSystems()
    }
    public func stopScan() {
        systemService.service.stopScanningForSystems()
        systemService.globalMode(.idle)
        scanInProgress.accept(false)
    }
    public func reset() {
        internalDevicesList.accept([])
    }

    public func managesDevice(deviceID: String) -> Bool {
        return internalDevicesList.value.contains(where: { $0.id == deviceID })
    }
    
    public func forget(device: DeviceType) {
        let currentList = internalDevicesList.value
        internalDevicesList.accept(currentList.removing(device))
        removed.accept(device)
        systemService.forget(device)
    }
    
    public var globalConnectMode: TymphanyConnectMode? {
        didSet {
            systemService.globalMode(globalConnectMode)
        }
    }
    
    /// Initialize discovered devices - get additional info (speaker color, mac address, tws paired status)
    public func setupDevices() -> Single<Void> {
        preprocessingDiscoveredInProgress.accept(true)
        return systemService.processDiscoveredDevices()
            .flatMap { [unowned self] in
                self.preprocessingDiscoveredInProgress.accept(false)
                return Single.just(())
            }
    }

    private init(storage: DefaultStorage = DefaultStorage.shared) {
        
        self.playControlAdapter = TymphanyPlayControlServiceAdapter(managedDevices: internalDevicesList)
        self.playControlAdapter.start()
        
        self.dspServiceAdapter = TymphanyDigitalSignalProcessingServiceAdapter()
        self.dspServiceAdapter.start()
        
        self.otaAdapter = TymphanyFirmwareOverTheAirUpdateServiceAdapter()
        self.otaAdapter.inputs.start()
        
        self.systemService = TymphanySystemServiceAdapter(managedDevices: internalDevicesList,
                                                          playControlServiceAdapter: self.playControlAdapter,
                                                          dspServiceAdapter: self.dspServiceAdapter,
                                                          firmwareOTAAdapter: self.otaAdapter,
                                                          platform: type(of: self).platform,
                                                          storage:  storage)
        
        self.systemService.adapterState
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [adapterState] state in
                adapterState.accept(state)
            }).disposed(by: disposeBag)
        
        self.systemService.discoveredInfoObservable
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] device in
                self.discovered.accept(device)
            }).disposed(by: disposeBag)
    }
    private let disposeBag = DisposeBag()
    private let systemService: TymphanySystemServiceAdapter
    private let playControlAdapter: TymphanyPlayControlServiceAdapter
    private let dspServiceAdapter: TymphanyDigitalSignalProcessingServiceAdapter
    private let otaAdapter: TymphanyFirmwareOverTheAirUpdateServiceAdapterType
    private var internalDevicesList = BehaviorRelay<[DeviceType]>(value: [])
}
