//
//  DataTypes.swift
//  JoplinPlatform
//
//  Created by Grzegorz Kiel on 21/01/2020.
//  Copyright © 2020 ZoundIndustries. All rights reserved.
//

import Foundation

public typealias ResultBluetoothClassicPairingStatus = Result<BluetoothClassicPairingStatus, DataError>
public typealias ResultBluetoothClassicPairingMode = Result<BluetoothClassicPairingMode, DataError>

public enum DataError: Error {
    case featureNotSupportedForThisTypeOfDevice
    case unsuccessfulWrite
    case unsuccessfulRead
    case wrongDataFormat

    case accessoryDisconnected
    case accessoryProtocolStringNotSupported
    case accessoryPreviousSessionOpen
    case accessoryDataCorrupted
}

public struct AccessoryInfo {
    public let mac: String
    public let connected: Bool
}

public enum BluetoothClassicPairingStatus: UInt8 {
    case notPaired
    case paired
}

public enum BluetoothClassicPairingMode: UInt8 {
    case notInPairingMode
    case inPairingMode
}
