//
//  TymphanyDeviceState.swift
//  JoplinPlatform
//
//  Created by Grzegorz Kiel on 07/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import GUMA
import TAPlatformSwift

private struct Config {
    static let minLedBrightness = 35
    static let maxLedBrightness = 70
    static let maxNameLength = 17
    static let volumeSteps: VolumeSteps = 32
    static let activatePairingTimeout = RxTimeInterval.seconds(5.0)
    static let pickerToAppearDelay = RxTimeInterval.seconds(3.0)
}
protocol SilentConnectionInputHandler {
    func connect() -> Single<ConnectivityStatus>
    func disconnect() -> Single<Void>
}
protocol SilentConnectionOutputHandler {
    func status() -> AnyState<ConnectivityStatus>
}
public final class TymphanyDeviceState: DeviceStateType {
    
    public var inputs: DeviceStateInput { return self }
    public var outputs: DeviceStateOutput { return self }
    var silentInput: SilentConnectionInputHandler { return self }
    var silentOutput: SilentConnectionOutputHandler { return self }
    var hw: TAPSSystem {
        get {
            return _underlyingHw
        }
        set {
            _underlyingHw = newValue
        }
    }
    public static let supportedFeatures: [Feature] = [.hasContinuousVolume,
                                                      .hasDeviceInfo,
                                                      .hasGraphicalEqualizer,
                                                      .hasLedBrightnessControl,
                                                      .hasConnectivityStatus,
                                                      .hasCustomizableName,
                                                      .hasTrueWireless,
                                                      .hasOTA,
                                                      .hasForcedOTA,
                                                      .hasSwitchableAudioSource,
                                                      .hasAudioPlayback,
                                                      .hasSoundsControl,
                                                      .hasPairing,
                                                      .hasSoftwarePairingModeTrigger]
    public let perDeviceTypeFeatures: [Feature]
    init(underlyingHardware: TAPSSystem,
         systemService: TymphanySystemServiceAdapter,
         playControlService: TymphanyPlayControlServiceAdapter,
         dspControlService: TymphanyDigitalSignalProcessingServiceAdapter,
         connectivityStatus: ConnectivityStatus) {
        
        self._name = AnyState<String>(feature: .hasCustomizableName,
            supportsNotifications: false, initialValue: { return String() })
        self._volume = AnyState<Int>(feature: .hasContinuousVolume,
            supportsNotifications: false, initialValue: { return Int() })
        self._monitoredVolume = AnyState<Int>(feature: .hasContinuousVolume,
            supportsNotifications: false, initialValue: { return Int() })
        self._hardwareInfoState = AnyState<DeviceHardwareInfo?>(feature: .hasDeviceInfo,
            supportsNotifications: false,initialValue: { () -> DeviceHardwareInfo? in return nil })
        self._batteryStatus = AnyState<BatteryStatus>(feature: .hasBatteryStatus,
            supportsNotifications: false, initialValue: { .unknown })
        self._monitoredBatteryStatus = AnyState<BatteryStatus>(feature: .hasBatteryStatus,
            supportsNotifications: false, initialValue: { .unknown })
        self._graphicalEqualizer = AnyState<GraphicalEqualizer>(feature: .hasGraphicalEqualizer,
            supportsNotifications: false, initialValue: { return GraphicalEqualizer() })
        self._monitoredGraphicalEqualizer = AnyState<GraphicalEqualizer>(feature: .hasGraphicalEqualizer,
            supportsNotifications: false, initialValue: { return GraphicalEqualizer() })
        self._equalizerSettings = AnyState<EqualizerSettings>(feature: .hasEqualizerSettings,
            supportsNotifications: false, initialValue: { return EqualizerSettings() })
        self._monitoredEqualizerButtonStep = AnyState<EqualizerButtonStep>(feature: .hasEqualizerSettings,
            supportsNotifications: false, initialValue: { return .step1 })
        self._activeNoiseCancellingSettings = AnyState<ActiveNoiseCancellingSettings>(feature: .hasActiveNoiceCancellingSettings,
            supportsNotifications: false, initialValue: { return ActiveNoiseCancellingSettings() })
        self._monitoredActiveNoiseCancellingMode = AnyState<ActiveNoiseCancellingMode>(feature: .hasActiveNoiceCancellingSettings,
            supportsNotifications: false, initialValue: { return .playBackOnly })
        self._activeNoiseCancellingLevel = AnyState<Int>(feature: .hasActiveNoiceCancellingSettings,
            supportsNotifications: false, initialValue: { return Int() })
        self._monitoringLevel = AnyState<Int>(feature: .hasActiveNoiceCancellingSettings,
            supportsNotifications: false, initialValue: { return Int() })
        self._autoOffTime = AnyState<AutoOffTime>(feature: .hasAutoOffTime,
            supportsNotifications: false, initialValue: { return .off })
        self._monitoredAutoOffTime = AnyState<AutoOffTime>(feature: .hasAutoOffTime,
            supportsNotifications: false, initialValue: { return .off })
        self._mButtonMode = AnyState<MButtonMode>(feature: .hasMButton,
            supportsNotifications: false, initialValue: { return .equalizerSettingsControl })
        self._trueWirelessStatus = AnyState<WirelessStereoStatus>(feature: .hasTrueWireless,
            supportsNotifications: false, initialValue: { return WirelessStereoStatus() })
        self._trueWirelessStatusWithId = AnyState<WirelessStereoStatusWithId>(feature: .hasTrueWireless,
            supportsNotifications: false, initialValue: { return WirelessStereoStatusWithId() })
        self._monitoredTrueWirelessStatus = AnyState<WirelessStereoStatus?>(feature: .hasTrueWireless,
            supportsNotifications: false, initialValue: { return WirelessStereoStatus() })
        self._ledBrightness = AnyState<Int>(feature: .hasLedBrightnessControl,
            supportsNotifications: false, initialValue: { return  Int() })
        self._soundsControlStatus = AnyState<SoundsControl>(feature: .hasSoundsControl,
            supportsNotifications: false, initialValue: { return .powerCue(on: false) })
        self._connectivityStatus = AnyState<ConnectivityStatus>(feature: .hasConnectivityStatus,
            supportsNotifications: false, initialValue: { return connectivityStatus })
        self._connectMode = AnyState<TymphanyConnectMode>(feature: .hasConnectivityStatus,
            supportsNotifications: false, initialValue: { return .idle })
        self._currentAudioSource = AnyState<AudioSource>(feature: .hasSwitchableAudioSource,
            supportsNotifications: false, initialValue: { return AudioSource.unknown })
        self._currentPlaybackStatus = AnyState<PlaybackStatus>(feature: .hasAudioPlayback,
            supportsNotifications: false, initialValue: { return PlaybackStatus.unknown })
        self._currentTrackInfo = AnyState<TrackInfo>(feature: .hasAudioPlayback,
            supportsNotifications: false, initialValue: { return TrackInfo() })
        self._silentConnectivityStatus = AnyState<ConnectivityStatus>(feature: .hasConnectivityStatus,
            supportsNotifications: false, initialValue: { .disconnected })
        self._pairingMode = AnyState<PairingMode>(feature: .hasSoftwarePairingModeTrigger,
            supportsNotifications: false, initialValue: { return .disabled})
        self._monitoredPairingMode = AnyState<PairingMode>(feature: .hasSoftwarePairingModeTrigger,
            supportsNotifications: false, initialValue: { return .disabled})
        self._accessoryPickerState = AnyState<AccessoryPairingStatus>(feature: .hasPairing,
            supportsNotifications: false, initialValue: { return .idle })
        self._powerCableStatus = AnyState<PowerCableStatus>(feature: .hasBatteryStatus,
            supportsNotifications: false, initialValue: { .unknown })
        self._monitoredPowerCableStatus = AnyState<PowerCableStatus>(feature: .hasBatteryStatus,
            supportsNotifications: false, initialValue: { .unknown })

        
        self._underlyingHw = underlyingHardware
        self._systemService = systemService
        self._playControlService = playControlService
        self._dspControlService = dspControlService
    
        guard let type = underlyingHardware.type else {
            self.perDeviceTypeFeatures = []
            return
        }
        switch type {
        case .actonII, .stanmoreII, .woburnII:
            self.perDeviceTypeFeatures = TymphanyDeviceState.supportedFeatures.filter { $0 != .hasPairing && $0 != .hasSoftwarePairingModeTrigger }
        case .actonIILite, .stanmoreIILite, .woburnIILite:
            self.perDeviceTypeFeatures = TymphanyDeviceState.supportedFeatures.filter { $0 != .hasForcedOTA && $0 != .hasPairing && $0 != .hasSoftwarePairingModeTrigger }
        case .ozzy, .ozzyAnc:
            self.perDeviceTypeFeatures = [.hasContinuousVolume,
                                          .hasDeviceInfo,
                                          .hasEqualizerSettings,
                                          .hasActiveNoiceCancellingSettings,
                                          .hasAutoOffTime,
                                          .hasMButton,
                                          .hasCustomizableName,
                                          .hasConnectivityStatus,
                                          .hasAudioPlayback,
                                          .hasBatteryStatus,
                                          .hasOTA,
                                          .hasPairing,
                                          .hasOnboarding,
                                          .hasSoftwarePairingModeTrigger,
                                          .hasSoundsControl
                                        ]
        }
    }
    fileprivate let _name: AnyState<String>
    fileprivate let _volume: AnyState<Int>
    fileprivate let _monitoredVolume: AnyState<Int>
    fileprivate let _batteryStatus: AnyState<BatteryStatus>
    fileprivate let _monitoredBatteryStatus: AnyState<BatteryStatus>
    fileprivate let _hardwareInfoState: AnyState<DeviceHardwareInfo?>
    fileprivate let _graphicalEqualizer: AnyState<GraphicalEqualizer>
    fileprivate let _monitoredGraphicalEqualizer: AnyState<GraphicalEqualizer>
    fileprivate let _equalizerSettings: AnyState<EqualizerSettings>
    fileprivate let _monitoredEqualizerButtonStep: AnyState<EqualizerButtonStep>
    fileprivate let _activeNoiseCancellingSettings: AnyState<ActiveNoiseCancellingSettings>
    fileprivate let _monitoredActiveNoiseCancellingMode: AnyState<ActiveNoiseCancellingMode>
    fileprivate let _activeNoiseCancellingLevel: AnyState<Int>
    fileprivate let _monitoringLevel: AnyState<Int>
    fileprivate let _autoOffTime: AnyState<AutoOffTime>
    fileprivate let _monitoredAutoOffTime: AnyState<AutoOffTime>
    fileprivate let _mButtonMode: AnyState<MButtonMode>
    fileprivate let _trueWirelessStatus: AnyState<WirelessStereoStatus>
    fileprivate let _trueWirelessStatusWithId: AnyState<WirelessStereoStatusWithId>
    fileprivate let _monitoredTrueWirelessStatus: AnyState<WirelessStereoStatus?>
    fileprivate let _connectivityStatus: AnyState<ConnectivityStatus>
    fileprivate let _connectMode: AnyState<TymphanyConnectMode>
    fileprivate let _ledBrightness: AnyState<Int>
    fileprivate let _soundsControlStatus: AnyState<SoundsControl>
    fileprivate let _currentAudioSource: AnyState<AudioSource>
    fileprivate let _currentPlaybackStatus: AnyState<PlaybackStatus>
    fileprivate let _currentTrackInfo: AnyState<TrackInfo>
    fileprivate let _silentConnectivityStatus: AnyState<ConnectivityStatus>
    fileprivate let _pairingMode: AnyState<PairingMode>
    fileprivate let _monitoredPairingMode: AnyState<PairingMode>
    fileprivate let _accessoryPickerState: AnyState<AccessoryPairingStatus>
    fileprivate let _powerCableStatus: AnyState<PowerCableStatus>
    fileprivate let _monitoredPowerCableStatus: AnyState<PowerCableStatus>
    fileprivate var _activeMonitors = Set<Monitor>()
    
    fileprivate let disposeBag = DisposeBag()
    fileprivate var connectivityDisposeBag = DisposeBag()
    
    private var _underlyingHw: TAPSSystem
    private weak var _systemService: TymphanySystemServiceAdapter!
    private weak var _playControlService: TymphanyPlayControlServiceAdapter!
    private weak var _dspControlService: TymphanyDigitalSignalProcessingServiceAdapter!

    private var _disconnectObservableDisposable: Disposable?
    private var _ozzyPairingDisposable: Disposable?
}

extension TymphanyDeviceState: DeviceStateInput {
    public func connect() {
        // NOTE: This is temporary solution until author of the silent connect is back
        let _: Single<ConnectivityStatus> = connect()
    }
    public func requestPowerCableStatus() {
        var disposeBag = DisposeBag()
        _systemService.requestPowerCableStatus(for: _underlyingHw)
            .observeOn(MainScheduler.instance)
            .subscribe(
                onSuccess: { [weak self] powerCableStatus in
                    self?.outputs.powerCableStatus().stateObservable().accept(powerCableStatus)
                    disposeBag = DisposeBag()
                },
                onError: { _ in
                    disposeBag = DisposeBag()
                }
            )
            .disposed(by: disposeBag)
    }
    public func startMonitoringPowerCableStatus() {
        _systemService.startMonitoringPowerCableConnectionStatus(of: _underlyingHw)
    }
    public func stopMonitoringPowerCableStatus() {
        _systemService.stopMonitoringPowerCableConnectionStatus(of: _underlyingHw)
    }
    public func activatePairingMode() -> Single<Void> {
        self.accessoryPairingStatus().stateObservable().accept(.waitingForConfirmation)
        return _systemService.write(pairingMode: .enabled, for: _underlyingHw)
    }
    public func startMonitoringPairingMode() {
        _systemService.startMonitoringPairingMode(of: _underlyingHw)
    }
    public func stopMonitoringPairingMode() {
        _systemService.stopMonitoringPairingMode(of: _underlyingHw)
    }
    public func requestVolume() {
        _playControlService.requestVolume(of: _underlyingHw)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] volume in
                self?._volume.stateObservable().accept(volume)
            })
            .disposed(by: disposeBag)
    }
    public func write(volume: Int) {
        _playControlService.write(volume: volume, of: _underlyingHw)
    }
    public func startMonitoringVolume() {
        _playControlService.startMonitoringVolume(of: _underlyingHw)
    }
    public func stopMonitoringVolume() {
        _playControlService.stopMonitoringVolume(of: _underlyingHw)
    }
    public func requestBatteryStatus() {
        _systemService.batteryInfo(for: _underlyingHw)
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { [weak self] batteryStatus in
                self?._batteryStatus.stateObservable().accept(batteryStatus)
            })
            .disposed(by: disposeBag)
    }
    public func startMonitoringBatteryStatus() {
        _systemService.startMonitoringBatteryStatus(of: _underlyingHw)
    }
    public func stopMonitoringBatteryStatus() {
        _systemService.stopMonitoringBatterStatus(of: _underlyingHw)
    }
    public func requestHardwareInfo() {
        _systemService.requestHardwareInfo(for: _underlyingHw)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] deviceHardwareInfo in
                self?._hardwareInfoState.stateObservable().accept(deviceHardwareInfo)
            })
            .disposed(by: disposeBag)
    }
    public func requestGraphicalEqualizer() {
        _dspControlService.requestGraphicalEqualizer(for: _underlyingHw)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [_graphicalEqualizer] graphicalEqualizer in
                _graphicalEqualizer.stateObservable().accept(graphicalEqualizer)
            }).disposed(by: disposeBag)
    }
    public func startMonitoringGraphicalEqualizer() {
        _dspControlService.startMonitoringGraphicalEqualizer(of: _underlyingHw)
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [_monitoredGraphicalEqualizer] graphicalEqualizer in
                _monitoredGraphicalEqualizer.stateObservable().accept(graphicalEqualizer)
            }).disposed(by: disposeBag)
    }
    public func stopMonitoringGraphicalEqualizer() {
        _dspControlService.stopMonitoringGraphicalEqualizer(of: _underlyingHw)
    }
    public func write(graphicalEqualizer: GraphicalEqualizer) {
        _dspControlService.write(graphicalEqualizer: graphicalEqualizer, for: _underlyingHw)
    }
    public func requestEqualizerSettings() {
        requestEqualizerSettings(of: _underlyingHw)
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { [_equalizerSettings] equalizerSettings in
            _equalizerSettings.stateObservable().accept(equalizerSettings)
        })
        .disposed(by: disposeBag)
    }
    public func startMonitoringEqualizerButtonStep() {
        _dspControlService.startMonitoringEqualizerButtonStep(of: _underlyingHw)
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [_monitoredEqualizerButtonStep] equalizerButtonStep in
                _monitoredEqualizerButtonStep.stateObservable().accept(equalizerButtonStep)
            }).disposed(by: disposeBag)
    }
    public func stopMonitoringEqualizerButtonStep() {
        _dspControlService.stopMonitoringEqualizerButtonStep(of: _underlyingHw)
    }
    public func write(equalizerButtonStep: EqualizerButtonStep) {
        _dspControlService.write(equalizerButtonStep: equalizerButtonStep, for: _underlyingHw)
    }
    public func write(step2Preset: EqualizerButtonStepPreset) {
        _dspControlService.write(step2Preset: step2Preset, for: _underlyingHw)
    }
    public func write(step3Preset: EqualizerButtonStepPreset) {
        _dspControlService.write(step3Preset: step3Preset, for: _underlyingHw)
    }
    public func write(customPresetGraphicalEqualizer: GraphicalEqualizer) {
        _dspControlService.write(customPresetGraphicalEqualizer: customPresetGraphicalEqualizer, for: _underlyingHw)
    }
    public func requestActiveNoiseCancellingSettings() {
        requestActiveNoiseCancellingSettings(of: _underlyingHw)
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { [_activeNoiseCancellingSettings] activeNoiseCancellingSettings in
            _activeNoiseCancellingSettings.stateObservable().accept(activeNoiseCancellingSettings)
        })
        .disposed(by: disposeBag)
    }
    public func startMonitoringActiveNoiseCancellingMode() {
        _playControlService.startMonitoringActiveNoiseCancellingMode(of: _underlyingHw)
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [_monitoredActiveNoiseCancellingMode] activeNoiseCancellingMode in
                _monitoredActiveNoiseCancellingMode.stateObservable().accept(activeNoiseCancellingMode)
            }).disposed(by: disposeBag)
    }
    public func stopMonitoringActiveNoiseCancellingMode() {
        _playControlService.stopMonitoringActiveNoiseCancellingMode(of: _underlyingHw)
    }
    public func write(activeNoiseCancellingMode: ActiveNoiseCancellingMode) {
        _playControlService.write(activeNoiseCancellingMode: activeNoiseCancellingMode, for: _underlyingHw)
    }
    public func write(activeNoiseCancellingLevel: Int) {
        _playControlService.write(activeNoiseCancellingLevel: activeNoiseCancellingLevel, for: _underlyingHw)
    }
    public func write(monitoringLevel: Int) {
        _playControlService.write(monitorLevel: monitoringLevel, for: _underlyingHw)
    }
    public func requestAutoOffTime() {
        _systemService.requestAutoOffTime(for: _underlyingHw)
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { [_autoOffTime] autoOffTime in
            _autoOffTime.stateObservable().accept(autoOffTime)
        }).disposed(by: disposeBag)
    }
    public func startMonitoringAutoOffTime() {
        _systemService.startMonitoringAutoOffTime(of: _underlyingHw)
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [_monitoredAutoOffTime] autoOffTime in
                _monitoredAutoOffTime.stateObservable().accept(autoOffTime)
            }).disposed(by: disposeBag)
    }
    public func stopMonitoringAutoOffTime() {
        _systemService.stopMonitoringAutoOffTime(of: _underlyingHw)
    }
    public func write(autoOffTime: AutoOffTime) {
        _systemService.write(autoOffTime: autoOffTime, for: _underlyingHw)
    }

    public func requestMButtonMode() {
        _playControlService.requestMButtonMode(for: _underlyingHw)
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { [_mButtonMode] mButtonMode in
            _mButtonMode.stateObservable().accept(mButtonMode)
        }).disposed(by: disposeBag)
    }
    public func write(mButtonMode: MButtonMode) {
        _playControlService.write(mButtonMode: mButtonMode, for: _underlyingHw)
    }
    public func requestTrueWirelessStatus() {
        _playControlService.requestTrueWirelessStatus(of: _underlyingHw)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [_trueWirelessStatus] trueWielessStatus in
                _trueWirelessStatus.stateObservable().accept(trueWielessStatus)
            })
            .disposed(by: disposeBag)
    }
    public func requestTrueWirelessStatusWithId() {
        requestTrueWirelessStatusWithId(of: _underlyingHw)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [_trueWirelessStatusWithId] trueWielessStatuswithId in
                _trueWirelessStatusWithId.stateObservable().accept(trueWielessStatuswithId)
            })
            .disposed(by: disposeBag)
    }
    public func startMonitoringTrueWirelessStatus() {
        _playControlService.startMonitoringTrueWirelessStatus(of: _underlyingHw)
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [_monitoredTrueWirelessStatus] monitoredTrueWirelessStatus in
                _monitoredTrueWirelessStatus.stateObservable().accept(monitoredTrueWirelessStatus)
            }).disposed(by: disposeBag)
    }
    public func stopMonitoringTrueWirelessStatus() {
        _playControlService.stopMonitoringTrueWirelessStatus(of: _underlyingHw)
    }
    public func disconnectTrueWireless() {
        _playControlService.disconnectTrueWireless(of: _underlyingHw)
    }
    public func write(trueWirelessConnectWith slaveId: Data, completion: (() -> Void)? = nil) {
        _playControlService.write(trueWirelessConnectWith: slaveId, of: _underlyingHw, completion: completion)
    }
    public func write(trueWirelessChannel: WirelessStereoChannel) {
        _playControlService.write(trueWirelessChannel: trueWirelessChannel, of: _underlyingHw)
    }
    public func requestLedBrightness() {
        _systemService.requestBrightnessInfo(for: _underlyingHw)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [_ledBrightness] currentLevel in
                guard (Config.minLedBrightness...Config.maxLedBrightness).contains(currentLevel) == true else { return }
                _ledBrightness.stateObservable().accept(currentLevel)
            })
            .disposed(by: disposeBag)
    }
    public func requestSoundsControlStatus() {
        _playControlService.requestSoundsControl(of: _underlyingHw)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [_soundsControlStatus] status in
                _soundsControlStatus.stateObservable().accept(status) })
            .disposed(by: disposeBag)
    }
    public func write(ledBrightness: Int) {
        _systemService.write(ledBrightness: ledBrightness, for: _underlyingHw)
    }
    public func write(soundControl: SoundsControl) {
        switch soundControl {
        case .powerCue(let on):
            _playControlService.turn(powerCue: on, of: _underlyingHw)
        case .mediaCue(let on):
            _playControlService.turn(mediaCue: on, of: _underlyingHw)
        }
    }
    public func set(connectivityStatus: ConnectivityStatus) {
        _connectivityStatus.stateObservable().accept(connectivityStatus)
    }
    public func set(connectMode: TymphanyConnectMode) {
        _connectMode.stateObservable().accept(connectMode)
    }
    public func set(volume: Int) {
        _monitoredVolume.stateObservable().accept(volume)
    }
    public func refreshName() {
        _systemService.requestCustomName(of: _underlyingHw)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [_name] name in
                _name.stateObservable().accept(name)
            }).disposed(by: disposeBag)
    }
    public func rename(with: String) {
        _systemService.write(name: with, for: _underlyingHw)
    }
    public func requestCurrentAudioSource() {
        _playControlService.currentAudioSource(of: _underlyingHw)
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [_currentAudioSource] source in
                _currentAudioSource.stateObservable().accept(source)
            }).disposed(by: disposeBag)
    }
    public func startMonitoringAudioSources() {
        _playControlService.startMonitoringAudioSources(of: _underlyingHw)
    }
    public func stopMonitoringAudioSources() {
        _playControlService.stopMonitoringAudioSources(of: _underlyingHw)
    }
    public func activateAUX() {
        _playControlService.activateAUX(of: _underlyingHw) {
            self._currentAudioSource.stateObservable().accept(.aux)
        }
    }
    public func activateRCA() {
        _playControlService.activateRCA(of: _underlyingHw) {
            self._currentAudioSource.stateObservable().accept(.rca)
        }
    }
    public func activateBluetooth() {
        _playControlService.activateBluetooth(of: _underlyingHw) {
            self._currentAudioSource.stateObservable().accept(.bluetooth)
        }
    }
    public func requestPlaybackStatus() {
        _playControlService.playbackStatus(of: _underlyingHw)
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [_currentPlaybackStatus] status in
                _currentPlaybackStatus.stateObservable().accept(status)
            }).disposed(by: disposeBag)
    }
    public func play() {
        _playControlService.play(on: _underlyingHw)
    }
    public func pause() {
        _playControlService.pause(on: _underlyingHw)
    }
    public func nextTrack() {
        _playControlService.nextTrack(on: _underlyingHw)
    }
    public func previousTrack() {
        _playControlService.previousTrack(on: _underlyingHw)
    }
    public func requestCurrentTrackInfo() {
        _playControlService.currentTrackInfo(of: _underlyingHw)
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [_currentTrackInfo] info in
                _currentTrackInfo.stateObservable().accept(info)
            }).disposed(by: disposeBag)
    }
    public func startMonitoringCurrentTrackInfo() {
        _playControlService.startMonitoringCurrentTrackInfo(of: _underlyingHw)
    }
    public func stopMonitoringCurrentTrackInfo() {
        _playControlService.stopMonitoringCurrentTrackInfo(of: _underlyingHw)
    }
    public func requestPairingMode() -> Single<PairingMode> {
        return _systemService.requestPairingMode(for: _underlyingHw)
    }
    public func showAccessoryPicker() {
        switch _underlyingHw.type {
        case .some(.ozzy(_)), .some(.ozzyAnc(_)):
            _ozzyPairingDisposable?.dispose()
            let successHandler = activatePairingModeSuccessHandler(systemService: _systemService)
            let failureHandler = activatePairingModeFailureHanlder(state: self)
            _ozzyPairingDisposable = activatePairingMode()
                .delaySubscription(RxTimeInterval.seconds(Config.pickerToAppearDelay), scheduler: MainScheduler.instance)
                .timeout(Config.activatePairingTimeout, scheduler: MainScheduler.instance)
                .subscribe(
                    onSuccess: successHandler,
                    onError: failureHandler
                )
        default:
            return _systemService.externalAccessoryService.showAccessoryPicker()
        }
    }
    public func append(monitor: Monitor) {
        _activeMonitors.insert(monitor)
    }
    public func remove(monitor: Monitor) {
        _activeMonitors.remove(monitor)
    }
    func watchForDisconnect(_ flag: Bool) {
        _disconnectObservableDisposable?.dispose()
        guard flag == true else { return }
        _disconnectObservableDisposable = silentOutput.status().stateObservable()
            .filter { $0 == .disconnected }
            .subscribe(onNext: { [weak self] status in
                guard let strongSelf = self else { return }
                let twsStatus = strongSelf.trueWirelessStatus().stateObservable().value
                guard case .disconnected = twsStatus else {
                    switch twsStatus {
                    case let .connectedAsMaster(_, pairedMACData):
                        strongSelf.inputs.set(connectivityStatus: .wsMaster(peerMAC: MAC(data: pairedMACData)))
                    case let .connectedAsSlave(_, pairedMACData):
                        strongSelf.inputs.set(connectivityStatus: .wsSlave(peerMAC: MAC(data: pairedMACData)))
                    default: return
                    }
                    return
                }
                strongSelf.inputs.set(connectivityStatus: status)
            })
    }
}
private extension TymphanyDeviceState {
    func trueWirelessStatusWithId(statusEvent: Event<WirelessStereoStatus>, idEvent: Event<Data>) -> Observable<WirelessStereoStatusWithId> {
        let status = statusEvent.element ?? WirelessStereoStatus()
        let id = idEvent.element ?? Data()
        return Observable.just(WirelessStereoStatusWithId(status: status, id: id))
    }
    func requestTrueWirelessStatusWithId(of system: TAPSSystem) -> Observable<WirelessStereoStatusWithId> {
        return Observable.combineLatest(
            _playControlService.requestTrueWirelessStatus(of: _underlyingHw).materialize(),
            _systemService.requestMACAddress(of: _underlyingHw).materialize()
        )
        .flatMap { [weak self] (statusEvent, idEvent) -> Observable<WirelessStereoStatusWithId> in
            guard let strongSelf = self else { return Observable.empty() }
            return strongSelf.trueWirelessStatusWithId(statusEvent: statusEvent, idEvent: idEvent)
        }
    }
    func equalizerSettings(limitedEqualizerSettingsEvent: Event<LimitedEqualizerSettings>, customPresetGraphicalEqualizerEvent: Event<GraphicalEqualizer>) -> Observable<EqualizerSettings> {
        let limitedEqualizerSettings = limitedEqualizerSettingsEvent.element ?? LimitedEqualizerSettings()
        let customPresetGraphicalEqualizer = customPresetGraphicalEqualizerEvent.element ?? GraphicalEqualizer()
        return Observable.just(EqualizerSettings(limitedEqualizerSettings: limitedEqualizerSettings, customGraphicalEqualizer: customPresetGraphicalEqualizer))
    }
    func requestEqualizerSettings(of system: TAPSSystem) -> Observable<EqualizerSettings> {
        return Observable.combineLatest(
            _dspControlService.requestLimitedEqualizerSettings(for: _underlyingHw).materialize(),
            _dspControlService.requestCustomPresetGraphicalEqualizer(for: _underlyingHw).materialize()
        )
        .flatMap { [weak self] (limitedEqualizerSettingsEvent, customPresetGraphicalEqualizerEvent) -> Observable<EqualizerSettings> in
            guard let strongSelf = self else { return Observable.empty() }
            return strongSelf.equalizerSettings(limitedEqualizerSettingsEvent: limitedEqualizerSettingsEvent, customPresetGraphicalEqualizerEvent: customPresetGraphicalEqualizerEvent)
        }
    }
    func activeNoiseCancellingSettings(modeEvent: Event<ActiveNoiseCancellingMode>, activeNoiceCancellingLevelEvent: Event<Int>, monitorLevelEvent: Event<Int>) -> Observable<ActiveNoiseCancellingSettings> {
        let activeNoiseCancellingMode = modeEvent.element ?? .playBackOnly
        let activeNoiceCancellingLevel = activeNoiceCancellingLevelEvent.element ?? .zero
        let monitorLevel = monitorLevelEvent.element ?? .zero
        return Observable.just(ActiveNoiseCancellingSettings(mode: activeNoiseCancellingMode,
                                                             activeNoiseCancellingLevel: activeNoiceCancellingLevel,
                                                             monitoringLevel: monitorLevel))
    }
    func requestActiveNoiseCancellingSettings(of system: TAPSSystem) -> Observable<ActiveNoiseCancellingSettings> {
        return Observable.combineLatest(
            _playControlService.requestActiveNoiseCancellingMode(for: _underlyingHw).materialize(),
            _playControlService.requestActiveNoiseCancellingLevel(for: _underlyingHw).materialize(),
            _playControlService.requestMonitorLevel(for: _underlyingHw).materialize()
        )
        .flatMap { [weak self] (activeNoiseCancellingMode, activeNoiceCancellingLevel, monitorLevel) -> Observable<ActiveNoiseCancellingSettings> in
            guard let strongSelf = self else { return Observable.empty() }
            return strongSelf.activeNoiseCancellingSettings(modeEvent: activeNoiseCancellingMode,
                                                            activeNoiceCancellingLevelEvent: activeNoiceCancellingLevel,
                                                            monitorLevelEvent: monitorLevel)
        }
    }
}
extension TymphanyDeviceState: DeviceStateOutput {
    public var activeMonitors: Set<Monitor> {
        get {
            return _activeMonitors
        }
        set(newValue) {
            _activeMonitors = newValue
        }
    }
    public func monitoredBatteryStatus() -> AnyState<BatteryStatus> {
        return _monitoredBatteryStatus
    }
    public func powerCableStatus() -> AnyState<PowerCableStatus> {
        return _powerCableStatus
    }
    public func monitoredPowerCableStatus() -> AnyState<PowerCableStatus> {
        return _monitoredPowerCableStatus
    }
    public func pairingMode() -> AnyState<PairingMode> {
        return _pairingMode
    }
    public func monitoredPairingMode() -> AnyState<PairingMode> {
        return _monitoredPairingMode
    }
    public func name() -> AnyState<String> {
        return _name
    }
    public func volume() -> AnyState<Int> {
        return _volume
    }
    public func monitoredVolume() -> AnyState<Int> {
        return _monitoredVolume
    }
    public func hardwareInfoState() -> AnyState<DeviceHardwareInfo?> {
        return _hardwareInfoState
    }
    public func batteryStatus() -> AnyState<BatteryStatus> {
        return _batteryStatus
    }
    public func graphicalEqualizer() -> AnyState<GraphicalEqualizer> {
        return _graphicalEqualizer
    }
    public func monitoredGraphicalEqualizer() -> AnyState<GraphicalEqualizer> {
        return _monitoredGraphicalEqualizer
    }
    public func equalizerSettings() -> AnyState<EqualizerSettings> {
        return _equalizerSettings
    }
    public func monitoredEqualizerButtonStep() -> AnyState<EqualizerButtonStep> {
        return _monitoredEqualizerButtonStep
    }
    public func activeNoiseCancellingSettings() -> AnyState<ActiveNoiseCancellingSettings> {
        return _activeNoiseCancellingSettings
    }
    public func monitoredActiveNoiseCancellingMode() -> AnyState<ActiveNoiseCancellingMode> {
        return _monitoredActiveNoiseCancellingMode
    }
    public func activeNoiseCancellingLevel() -> AnyState<Int> {
        return _activeNoiseCancellingLevel
    }
    public func monitoringLevel() -> AnyState<Int> {
        return _monitoringLevel
    }
    public func autoOffTime() -> AnyState<AutoOffTime> {
        return _autoOffTime
    }
    public func monitoredAutoOffTime() -> AnyState<AutoOffTime> {
        return _monitoredAutoOffTime
    }
    public func mButtonMode() -> AnyState<MButtonMode> {
        return _mButtonMode
    }
    public func trueWirelessStatus() -> AnyState<WirelessStereoStatus> {
        return _trueWirelessStatus
    }
    public func trueWirelessStatusWithId() -> AnyState<WirelessStereoStatusWithId> {
        return _trueWirelessStatusWithId
    }
    public func monitoredTrueWirelessStatus() -> AnyState<WirelessStereoStatus?> {
        return _monitoredTrueWirelessStatus
    }
    public func ledBrightness() -> AnyState<Int> {
        return _ledBrightness
    }
    public func soundsControlStatus() -> AnyState<SoundsControl> {
        return _soundsControlStatus
    }
    public func connectivityStatus() -> AnyState<ConnectivityStatus> {
        return _connectivityStatus
    }
    public func connectMode() -> AnyState<TymphanyConnectMode> {
        return _connectMode
    }
    public func currentAudioSource() -> AnyState<AudioSource> {
        return _currentAudioSource
    }
    public func supportedAudioSources() -> [AudioSource] {
        switch _underlyingHw.type {
        case .some(.actonII), .some(.actonIILite), .some(.ozzy), .some(.ozzyAnc):
            return [.bluetooth, .aux]
        case .some(.woburnII), .some(.woburnIILite), .some(.stanmoreII), .some(.stanmoreIILite):
            return [.bluetooth, .aux, .rca]
        default:
            fatalError("Unkown hardware type : \(_underlyingHw.type!)")
        }
    }
    public func playbackStatus() -> AnyState<PlaybackStatus> {
        return _currentPlaybackStatus
    }
    public func trackInfo() -> AnyState<TrackInfo> {
        return _currentTrackInfo
    }
    public func accessoryPairingStatus() -> AnyState<AccessoryPairingStatus> {
        return _accessoryPickerState
    }
    public func disconnect() {
        _systemService.service.disconnect(_underlyingHw)
    }
    func wait(for status: ConnectivityStatus) -> Single<ConnectivityStatus> {
        return Single.create { [weak self] event in
            guard let strongSelf = self else { return Disposables.create() }
            var statusDisposeBag = DisposeBag()
            strongSelf.silentOutput.status().stateObservable()
                .skipWhile { $0 != status }
                .timeout(strongSelf._systemService.platform.traits.connectionTimeout, scheduler: MainScheduler.instance)
                .take(1)
                .subscribe(
                    onNext: { status in
                        event(.success(status))
                        statusDisposeBag = DisposeBag()
                    },
                    onError: { error in
                        event(.error(error))
                        statusDisposeBag = DisposeBag()
                    }
                )
                .disposed(by: statusDisposeBag)
            switch status {
            case .connected: strongSelf._systemService.service.connect(strongSelf._underlyingHw)
            case .disconnected: strongSelf._systemService.service.disconnect(strongSelf._underlyingHw)
            default: fatalError("No wait state defined for \(status)")
            }
            return Disposables.create()
        }
    }
}

extension TymphanyDeviceState: SilentConnectionInputHandler {
    func connect() -> Single<ConnectivityStatus> {
        return wait(for: .connected)
    }
    func disconnect() -> Single<Void> {
        return wait(for: .disconnected).flatMap { _ in return Single.just(()) }
    }
}
extension TymphanyDeviceState: SilentConnectionOutputHandler {
    func status() -> AnyState<ConnectivityStatus> {
        return _silentConnectivityStatus
    }
}
private func activatePairingModeSuccessHandler(systemService: TymphanySystemServiceAdapter) -> () -> Void {
    return {
        systemService.externalAccessoryService.showAccessoryPicker()
    }
}
private func activatePairingModeFailureHanlder(state: TymphanyDeviceState) -> (Error) -> Void {
    return { _ in
        var disconnectedDisposeBag = DisposeBag()
        state.wait(for: .disconnected)
            .do(
                onSuccess: { [weak state] _ in
                    state?.set(connectivityStatus: .disconnected) },
                onError: { [weak state] _ in
                    state?.set(connectivityStatus: .disconnected) }
            )
            .subscribe(
                onSuccess: { _ in
                    disconnectedDisposeBag = DisposeBag()
                },
                onError: { _ in
                    disconnectedDisposeBag = DisposeBag()
            })
            .disposed(by: disconnectedDisposeBag)
    }
}
