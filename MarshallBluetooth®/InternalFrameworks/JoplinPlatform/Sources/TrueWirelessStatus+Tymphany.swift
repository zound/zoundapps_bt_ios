//
//  TrueWirelessStatus+Tymphany.swift
//  JoplinPlatform
//
//  Created by Wudarski Lukasz on 29/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import TAPlatformSwift
import GUMA

extension WirelessStereoChannel {
    var tapsTrueWirelessChannel: TAPSPlayControlServiceTrueWirelessChannel {
        switch self {
        case .party:
            return .party
        case .left:
            return .masterAsLeft
        case .right:
            return .masterAsRight
        }
    }
}

extension WirelessStereoStatus {
    init(config: TAPSPlayControlServiceTrueWirelessConfig) {
        let oppositeDeviceMACAddress = config.oppositeDeviceMACAddress ?? Data()
        switch (config.status, config.channel) {
        case (.connectedAsMaster, .party):
            self = .connectedAsMaster(.party, oppositeDeviceMACAddress: oppositeDeviceMACAddress)
        case (.connectedAsMaster, .masterAsLeft):
            self = .connectedAsMaster(.left, oppositeDeviceMACAddress: oppositeDeviceMACAddress)
        case (.connectedAsMaster, .masterAsRight):
            self = .connectedAsMaster(.right, oppositeDeviceMACAddress: oppositeDeviceMACAddress)
        case (.connectedAsSlave, .party):
            self = .connectedAsSlave(.party, oppositeDeviceMACAddress: oppositeDeviceMACAddress)
        case (.connectedAsSlave, .masterAsLeft):
            self = .connectedAsSlave(.right, oppositeDeviceMACAddress: oppositeDeviceMACAddress)
        case (.connectedAsSlave, .masterAsRight):
            self = .connectedAsSlave(.left, oppositeDeviceMACAddress: oppositeDeviceMACAddress)
        default:
            self = .disconnected
        }
    }
}
