//
//  TymphanyFirmwareDownloader.swift
//  JoplinPlatform
//
//  Created by Grzegorz Kiel on 24/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import ZIPFoundation
import IDZSwiftCommonCrypto
import GUMA

enum TymphanyDownloaderStatus {
    case idle
    case downloading(Float)
    case unzipping
    case md5checksumChecking
    case ready(Data, TymphanyFirmwareVersion)
    case error(OTAError)
}

protocol TymphanyFirmwareDownloaderInput {
    func startDownload()
}

protocol TymphanyFirmwareDownloaderOutput {
    func status() -> BehaviorRelay<TymphanyDownloaderStatus>
    var info: TymphanyOTAInfo { get }
}

protocol TymphanyFirmwareDownloaderType {
    var inputs: TymphanyFirmwareDownloaderInput { get }
    var outputs: TymphanyFirmwareDownloaderOutput { get }
}

extension TymphanyFirmwareDownloader: TymphanyFirmwareDownloaderType {
    var inputs: TymphanyFirmwareDownloaderInput { return self }
    var outputs: TymphanyFirmwareDownloaderOutput { return self }
}

final class TymphanyFirmwareDownloader: NSObject,
    TymphanyFirmwareDownloaderInput,
TymphanyFirmwareDownloaderOutput {
    let info: TymphanyOTAInfo
    
    init(info: TymphanyOTAInfo) {
        self.info = info
    }
    
    func startDownload() {
        
        /// Check if file already downloaded
        guard let cachedFilename = info.remoteInfo.cachedBinFilename(),
            FileManager.default.fileExists(atPath: cachedFirmwareFileURL(cachedFilename).path) else {
                downloadFirmware()
                return
        }
        /// Load cached file
        do {
            let firmwareData = try Data(contentsOf: cachedFirmwareFileURL(cachedFilename))
            guard let latestVersion = info.remoteInfo.latestVersion else {
                downloadFirmware()
                return
            }
            _status.accept(.ready(firmwareData, latestVersion))
        } catch {
            downloadFirmware()
        }
    }
    
    func status() -> BehaviorRelay<TymphanyDownloaderStatus> {
        return _status
    }
    
    fileprivate let _status = BehaviorRelay<TymphanyDownloaderStatus>.init(value: .idle)
}

extension TymphanyFirmwareDownloader: URLSessionDelegate {
    public func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        guard let _ = error else { return }
        _status.accept(.error(.internetNotAvailable))
    }
}

extension TymphanyFirmwareDownloader: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        firmwareDownloaded(location: location)
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let progress = Float(totalBytesWritten)/Float(totalBytesExpectedToWrite)
        _status.accept(.downloading(progress))
    }
}

fileprivate extension TymphanyFirmwareDownloader {
    
    func downloadFirmware() {
        guard let url = URL(string: info.remoteInfo.downloadURL) else {
            _status.accept(.error(OTAError.unableToCreateDownloadURL))
            return
        }
        let sessionConfiguration = URLSessionConfiguration.background(withIdentifier: url.absoluteString)
        let session = URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: nil)
        let task = session.downloadTask(with: url)
        task.resume()
    }
    
    func firmwareDownloaded(location: URL) {
        
        /// try to unzip the file in the provided location
        guard let unzippedFolder = info.remoteInfo.cachedDirectory(),
            let md5Filename = info.remoteInfo.cachedMD5Filename(),
            let binFilename = info.remoteInfo.cachedBinFilename() else {
                _status.accept(.error(OTAError.failedToCreateUnzippedFolder))
                return
        }
        
        do {
            let unzippedDirectory = cachedFirmwareFileURL(unzippedFolder)
            try FileManager.default.unzipItem(at: location, to: unzippedDirectory)
            
            let md5 = try String(contentsOf: cachedFirmwareFileURL(md5Filename))
            let bin = try Data(contentsOf: cachedFirmwareFileURL(binFilename))
            
            guard let md5digest = Digest(algorithm: .md5).update(data: bin)?.final() else {
                _status.accept(.error(OTAError.failedToCalculateMD5Checksum))
                return
            }
            let binaryMD5 = hexString(fromArray: md5digest)
            guard let correctMD5 = md5.split(separator: "\n").first else {
                _status.accept(.error(OTAError.failedToCalculateMD5Checksum))
                return
            }
            
            /// Checking MD5 sum
            guard correctMD5 == binaryMD5 else {
                _status.accept(.error(OTAError.md5checksumMismatch))
                try FileManager.default.removeItem(at: unzippedDirectory)
                return
            }
            guard let latestVersion = info.remoteInfo.latestVersion else {
                _status.accept(.error(OTAError.unknownRemoteVersion))
                return
            }
            _status.accept(.ready(bin, latestVersion))
        } catch {
            _status.accept(.error(OTAError.failedToUnzipFirmwareFiles))
        }
    }
    
    func cachedFirmwareFileURL(_ fileName: String) -> URL {
        return FileManager.default.urls(for: .cachesDirectory, in: .allDomainsMask)
            .first!
            .appendingPathComponent(fileName)
    }
}
