//
//  TymphanyPlayControlServiceAdapter.swift
//  JoplinPlatform
//
//  Created by Grzegorz Kiel on 05/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import TAPlatformSwift
import GUMA

final class TymphanyPlayControlServiceAdapter {
    var service: TAPSPlayControlService!
    var managedDevices = BehaviorRelay<[DeviceType]>(value: [])
    private var monitoredTrueWirelessStatus: PublishRelay<WirelessStereoStatus>?
    private var monitoredAudioSource: PublishRelay<AudioSource>?
    private var monitoredActiveNoiseCancellingMode: PublishRelay<ActiveNoiseCancellingMode>?
    private var volumeBuffers: [String: Int] = [:]
    private var monitoredVolumeBuffers: [String: Int] = [:] // TODO: Remove once fixed in SDK
    init(managedDevices: BehaviorRelay<[DeviceType]>) {
        self.managedDevices = managedDevices
    }
    func start() {
        let config: [String: Any] = [
            TAPSSystemKey.serviceInfo: TAPSResource.qcc3008ServiceInfo["serviceConfig"]!,
            TAPSSystemKey.connectionDuration: Int(TimeInterval.seconds(5))
        ]
        self.service = TAPSPlayControlService(type: .ble, config: config, delegate: self).start()
    }
}

extension TymphanyPlayControlServiceAdapter {
    func requestTrueWirelessStatus(of system: TAPSSystem) -> Observable<WirelessStereoStatus> {
        return Observable.create { [unowned self] observer -> Disposable in
            self.service.trueWirelessConfig(of: system) { config in
                observer.onNext(WirelessStereoStatus(config: config))
            }
            return Disposables.create()
        }
    }
    func write(trueWirelessConnectWith slaveId: Data, of system: TAPSSystem, completion: (() -> Void)? = nil) {
        service.pairingTrueWireless(to: slaveId, of: system, completion: { _ in
            completion?()
        })
    }
    func write(trueWirelessChannel: WirelessStereoChannel, of system: TAPSSystem) {
        service.switch(trueWirelessChannel: trueWirelessChannel.tapsTrueWirelessChannel, of: system, completion: { _ in })
    }
    func disconnectTrueWireless(of system: TAPSSystem) {
        service.disconnectTrueWireless(of: system, completion: { _ in })
    }
    func startMonitoringTrueWirelessStatus(of system: TAPSSystem) -> PublishRelay<WirelessStereoStatus> {
        monitoredTrueWirelessStatus = PublishRelay<WirelessStereoStatus>()
        service.startMonitoringTrueWirelessConfig(of: system)
        return monitoredTrueWirelessStatus!
    }
    func stopMonitoringTrueWirelessStatus(of system: TAPSSystem) {
        service.stopMonitoringTrueWirelessConfig(of: system)
        monitoredTrueWirelessStatus = nil
    }
}

extension TymphanyPlayControlServiceAdapter {
    func requestVolume(of system: TAPSSystem) -> Observable<Int> {
        return Observable.create { [unowned self] observer -> Disposable in
            self.service.volume(of: system) { result in
                switch result {
                case .success(let value):
                    observer.onNext(value)
                case .error(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    func write(volume: Int, of system: TAPSSystem) {
        if volumeBuffers.contains(where: { $0.key == system.tapSystem.uuidString }) {
            volumeBuffers[system.tapSystem.uuidString] = volume
        } else {
            volumeBuffers[system.tapSystem.uuidString] = volume
            service.write(volume: volume, of: system) { [weak self] _ in
                guard let currentBufferedVolume = self?.volumeBuffers[system.tapSystem.uuidString], currentBufferedVolume != volume else {
                    self?.volumeBuffers.removeValue(forKey: system.tapSystem.uuidString)
                    return
                }
                self?.volumeBuffers.removeValue(forKey: system.tapSystem.uuidString)
                self?.write(volume: currentBufferedVolume, of: system)
            }
        }
    }
    func startMonitoringVolume(of system: TAPSSystem) {
        service.startMonitoringVolume(of: system)
    }
    func stopMonitoringVolume(of system: TAPSSystem) {
        service.stopMonitoringVolume(of: system)
    }
}

// MARK: - Handling audio sources
extension TymphanyPlayControlServiceAdapter {
    func currentAudioSource(of system: TAPSSystem) -> Single<AudioSource> {
        return Single<AudioSource>.create { [unowned self] single in
            self.service.audioSource(of: system) { source in
                single(.success(source.converted))
            }
            return Disposables.create()
        }
    }
    func activateAUX(of system: TAPSSystem, completion: @escaping () -> Void) {
        service.auxSource(of: system) { _ in
            completion()
        }
    }
    func activateRCA(of system: TAPSSystem, completion: @escaping () -> Void) {
        service.rcaSource(of: system) { _ in
            completion()
        }
    }
    func activateBluetooth(of system: TAPSSystem, completion: @escaping () -> Void) {
        service.bluetoothSource(of: system) { _ in
            completion()
        }
    }
    func startMonitoringAudioSources(of system: TAPSSystem) {
        service.startMonitoringAudioControlAndStatus(of: system)
    }
    func stopMonitoringAudioSources(of system: TAPSSystem) {
        service.stopMonitoringAudioControlAndStatus(of: system)
    }
}

// MARK: - Sounds control (power, media cues)
extension TymphanyPlayControlServiceAdapter {
    func requestSoundsControl(of system: TAPSSystem) -> Observable<SoundsControl> {
        return Observable.merge(powerCue(of: system).asObservable(), mediaCue(of: system).asObservable())
    }
    func powerCue(of system: TAPSSystem) -> Single<SoundsControl> {
        return Single<SoundsControl>.create { [unowned self] single in
            self.service.powerCue(of: system) { cue in
                single(.success(.powerCue(on: cue.converted)))
            }
            return Disposables.create()
        }
    }
    func mediaCue(of system: TAPSSystem) -> Single<SoundsControl> {
        return Single<SoundsControl>.create { [unowned self] single in
            self.service.mediaCue(of: system) { cue in
                single(.success(.mediaCue(on: cue.converted)))
            }
            return Disposables.create()
        }
    }
    func turn(powerCue: Bool, of system: TAPSSystem) {
        service.turn(powerCue: powerCue ? .on : .off, of: system, completion: { _ in })
    }
    func turn(mediaCue: Bool, of system: TAPSSystem) {
        service.turn(mediaCue: mediaCue ? .on : .off, of: system, completion: { _ in })
    }
}

extension TymphanyPlayControlServiceAdapter {
    func playbackStatus(of system: TAPSSystem) -> Observable<PlaybackStatus> {
        return Observable<PlaybackStatus>.create { [unowned self] observer in
            self.service.playStatus(of: system) { status in
                observer.onNext(status.converted)
            }
            return Disposables.create()
        }
    }
    func play(on system: TAPSSystem) {
        service.play(of: system, completion: { _ in })
    }
    func pause(on system: TAPSSystem) {
        service.pause(of: system, completion: { _ in })
    }
    func nextTrack(on system: TAPSSystem) {
        service.next(of: system, completion: { _ in })
    }
    func previousTrack(on system: TAPSSystem) {
        service.previous(of: system, completion: { _ in })
    }
    func currentTrackInfo(of system: TAPSSystem) -> Observable<TrackInfo> {
        return Observable<TrackInfo>.create { [unowned self] observer in
            self.service.currentTrackInfo(of: system) { result in
                switch result {
                case .success(let info):
                    let trackInfo = TrackInfo(title: info[TAPSPlayControlKey.title],
                                              artist: info[TAPSPlayControlKey.artist],
                                              album: info[TAPSPlayControlKey.album],
                                              number: info[TAPSPlayControlKey.number],
                                              totalNumber: info[TAPSPlayControlKey.totalNumber],
                                              genre: info[TAPSPlayControlKey.genre],
                                              playingTime: info[TAPSPlayControlKey.playingTime])
                    observer.onNext(trackInfo)
                case .error(let details):
                    observer.onError(details)
                }
            }
            return Disposables.create()
        }
    }
    func startMonitoringCurrentTrackInfo(of system: TAPSSystem) {
        service.startMonitoringCurrentTrackInfo(of: system)
    }
    func stopMonitoringCurrentTrackInfo(of system: TAPSSystem) {
        service.stopMonitoringCurrentTrackInfo(of: system)
    }
}

extension TymphanyPlayControlServiceAdapter {
    func requestActiveNoiseCancellingMode(for system: TAPSSystem) -> Observable<ActiveNoiseCancellingMode> {
        return Observable.create { [unowned self] observer in
            self.service.ancMode(of: system) { tapsANCMode in
                guard let activeNoiseCancellingMode = ActiveNoiseCancellingMode(rawValue: tapsANCMode.rawValue) else {
                    observer.onError(NSError())
                    return
                }
                observer.onNext(activeNoiseCancellingMode)
            }
            return Disposables.create()
        }
    }
    func startMonitoringActiveNoiseCancellingMode(of system: TAPSSystem) -> PublishRelay<ActiveNoiseCancellingMode> {
        monitoredActiveNoiseCancellingMode = PublishRelay<ActiveNoiseCancellingMode>()
        service.startMonitoringAncMode(of: system)
        return monitoredActiveNoiseCancellingMode!
    }
    func stopMonitoringActiveNoiseCancellingMode(of system: TAPSSystem) {
        service.stopMonitoringAncMode(of: system)
        monitoredActiveNoiseCancellingMode = nil
    }
    func write(activeNoiseCancellingMode: ActiveNoiseCancellingMode, for system: TAPSSystem) {
        guard let ancMode = TAPSPlayControlServiceANCMode(rawValue: activeNoiseCancellingMode.rawValue) else {
            return
        }
        service.turn(ancMode: ancMode, of: system, completion: { _ in })
    }
    func requestActiveNoiseCancellingLevel(for system: TAPSSystem) -> Observable<Int> {
        return Observable.create { [unowned self] observer in
            self.service.ancLevel(of: system) { tapsANCLevel in
                observer.onNext(tapsANCLevel)
            }
            return Disposables.create()
        }
    }
    func write(activeNoiseCancellingLevel: Int, for system: TAPSSystem) {
        service.write(ancLevel: activeNoiseCancellingLevel, of: system, completion: { _ in })
    }
    func requestMonitorLevel(for system: TAPSSystem) -> Observable<Int> {
        return Observable.create { [unowned self] observer in
            self.service.monitorLevel(of: system) { tapsMonitorLevel in
                observer.onNext(tapsMonitorLevel)
            }
            return Disposables.create()
        }
    }
    func write(monitorLevel: Int, for system: TAPSSystem) {
        service.write(monitorLevel: monitorLevel, of: system, completion: { _ in })
    }
}

extension TymphanyPlayControlServiceAdapter {
    func requestMButtonMode(for system: TAPSSystem) -> Observable<MButtonMode> {
        return Observable.create { [unowned self] observer in
            self.service.customButtonMode(of: system, completion: { tapsCustomButtonMode in
                if let mButtonMode = MButtonMode(customButtonMode: tapsCustomButtonMode) {
                    observer.onNext(mButtonMode)
                } else {
                    observer.onError(NSError())
                }
            })
            return Disposables.create()
        }
    }
    func write(mButtonMode: MButtonMode, for system: TAPSSystem) {
        service.switch(customButtonMode: mButtonMode.customButtonMode, of: system, completion: { _ in })
    }
}


extension TymphanyPlayControlServiceAdapter: TAPSPlayControlServiceDelegate {
    func playControlService(_ system: TAPSSystem, didUpdate volume: Int) {
        guard let device = managedDevices.value.first(where: { $0.id == system.tapSystem.uuidString }) else { return }
        guard monitoredVolumeBuffers[system.tapSystem.uuidString] == nil || monitoredVolumeBuffers[system.tapSystem.uuidString]! != volume else { return }
        volumeBuffers.removeValue(forKey: system.tapSystem.uuidString)
        monitoredVolumeBuffers[system.tapSystem.uuidString] = volume
        device.state.inputs.set(volume: volume)
    }
    func playControlService(_ system: TAPSSystem, didUpdate audioSource: TAPSPlayControlServiceAudioSource) {
        managedDevices.value
            .first { $0.id == system.tapSystem.uuidString }?
            .state.outputs.currentAudioSource().stateObservable()
            .accept(audioSource.converted)
    }
    func playControlService(_ system: TAPSSystem, didUpdate playStatus: TAPSPlayControlServicePlayStatus) {
        managedDevices.value
            .first { $0.id == system.tapSystem.uuidString }?
            .state.outputs.playbackStatus().stateObservable()
            .accept(playStatus.converted)
    }
    func playControlService(_ system: TAPSSystem, didUpdate ancMode: TAPSPlayControlServiceANCMode) {
        guard let activeNoiseCancellingMode = ActiveNoiseCancellingMode(rawValue: ancMode.rawValue) else {
            return
        }
        monitoredActiveNoiseCancellingMode?.accept(activeNoiseCancellingMode)
    }
    func playControlService(_ system: TAPSSystem, didUpdate anc: TAPSPlayControlServiceANC) {
        print("⚠️ name: \(system.name), playControlService(_ system: TAPSSystem, didUpdate anc: TAPSPlayControlServiceANC) - new unimplemented delegate call - take a closer look.")
    }
    
    func playControlService(_ system: TAPSSystem, didUpdatePower cue: TAPSPlayControlServiceCue) {
        print("⚠️ playControlService(_ system: TAPSSystem, didUpdatePower cue: TAPSPlayControlServiceCue) - new unimplemented delegate call - take a closer look.")
    }
    
    func playControlService(_ system: TAPSSystem, didUpdateMedia cue: TAPSPlayControlServiceCue) {
        print("⚠️ playControlService(_ system: TAPSSystem, didUpdateMedia cue: TAPSPlayControlServiceCue) - new unimplemented delegate call - take a closer look.")
    }
    
    func playControlService(_ system: TAPSSystem, didUpdate currentTrackInfo: [AnyHashable: Any]) {
        managedDevices.value
            .first { $0.id == system.tapSystem.uuidString }?
            .state.outputs.trackInfo().stateObservable()
            .accept(TrackInfo(title: currentTrackInfo[TAPSPlayControlKey.title],
                              artist: currentTrackInfo[TAPSPlayControlKey.artist],
                              album: currentTrackInfo[TAPSPlayControlKey.album],
                              number: currentTrackInfo[TAPSPlayControlKey.number],
                              totalNumber: currentTrackInfo[TAPSPlayControlKey.totalNumber],
                              genre: currentTrackInfo[TAPSPlayControlKey.genre],
                              playingTime: currentTrackInfo[TAPSPlayControlKey.playingTime]))
    }

    func playControlService(_ system: TAPSSystem, didUpdate trueWirelessConfig: TAPSPlayControlServiceTrueWirelessConfig) {
        monitoredTrueWirelessStatus?.accept(WirelessStereoStatus(config: trueWirelessConfig))
    }
    
}

// MARK: - Helper extension to Tymphany enum
private extension TAPSPlayControlServiceAudioSource {
    
    /// Mapping function to translate Tymphany's enum into GUMA
    var converted: AudioSource {
        switch self {
        case .auxIn: return .aux
        case .bluetooth: return .bluetooth
        case .RCA: return .rca
        default: return .unknown
        }
    }
}

// MARK: - Helper extension to Tymphany's playback status
private extension TAPSPlayControlServicePlayStatus {
    
    /// Mapping function to translate Tymphany's enum into GUMA
    var converted: PlaybackStatus {
        switch self {
        case .play: return .playing
        case .pause: return .paused
        case .stopped: return .stopped
        default: return .unknown
        }
    }
}

private extension TAPSPlayControlServiceCue {
    var converted: Bool {
        switch self {
        case .on: return true
        default: return false
        }
    }
}

private extension MButtonMode {
    init?(customButtonMode: TAPSPlayControlServiceCustomButtonMode) {
        switch customButtonMode {
        case .eqControl:
            self = .equalizerSettingsControl
        case .googleVoiceAssistant:
            self = .googleVoiceAssistant
        case .nativeVoiceAssistant:
            self = .nativeVoiceAssistant
        case .alexa, .unknown:
            return nil
        }
    }
    var customButtonMode: TAPSPlayControlServiceCustomButtonMode {
        switch self {
        case .equalizerSettingsControl:
            return .eqControl
        case .googleVoiceAssistant:
            return .googleVoiceAssistant
        case .nativeVoiceAssistant:
            return .nativeVoiceAssistant
        }
    }
}
