//
//  TymphanyFirmwareOverTheAirUpdateServiceAdapter.swift
//  JoplinPlatform
//
//  Created by Grzegorz Kiel on 05/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import TAPlatformSwift
import GUMA

struct TymphanyOTAUpdateStatus {
    let system: TAPSSystem?
    let status: TAPSFirmwareOTAStatus
}

protocol TymphanyFirmwareOverTheAirUpdateServiceAdapterInputs {
    func start()
    func startMonitoringOTA(system: TAPSSystem)
    func stopMonitoringOTA(system: TAPSSystem)
    func pauseUpdateOTA(system: TAPSSystem)
    func uploadFirmwareToDevice(system: TAPSSystem, version: String, binary: Data)
    func checkStatusOTA(system: TAPSSystem, completion: @escaping (TAPSFirmwareOTAStatus, String?) -> Void)
}

protocol TymphanyFirmwareOverTheAirUpdateServiceAdapterOutputs {
    var updateStatus: PublishRelay<TymphanyOTAUpdateStatus> { get }
}

protocol TymphanyFirmwareOverTheAirUpdateServiceAdapterType: AnyObject {
    var inputs: TymphanyFirmwareOverTheAirUpdateServiceAdapterInputs { get }
    var outputs: TymphanyFirmwareOverTheAirUpdateServiceAdapterOutputs { get }
}

final class TymphanyFirmwareOverTheAirUpdateServiceAdapter: TymphanyFirmwareOverTheAirUpdateServiceAdapterInputs,
    TymphanyFirmwareOverTheAirUpdateServiceAdapterOutputs,
    TymphanyFirmwareOverTheAirUpdateServiceAdapterType
{
    var updateStatus = PublishRelay<TymphanyOTAUpdateStatus>()
    
    private var service: TAPSFirmwareOverTheAirUpdateService!
    
    var inputs: TymphanyFirmwareOverTheAirUpdateServiceAdapterInputs { return self }
    var outputs: TymphanyFirmwareOverTheAirUpdateServiceAdapterOutputs { return self }
    
    func start() {
        let config: [String: Any] = [
            TAPSSystemKey.serviceInfo: TAPSResource.qcc3008ServiceInfo["serviceConfig"]!,
            TAPSSystemKey.connectionDuration: Int(TimeInterval.seconds(5))
        ]
        self.service = TAPSFirmwareOverTheAirUpdateService(type: .ble, config: config, delegate: self).start()
    }
    
    func checkStatusOTA(system: TAPSSystem, completion: @escaping (TAPSFirmwareOTAStatus, String?) -> Void) {
        service.checkFirmwareOTAStatus(of: system, completion: completion)
    }
    
    func pauseUpdateOTA(system: TAPSSystem) {
        service.pauseUpdate(of: system)
    }
    
    func startMonitoringOTA(system: TAPSSystem) {
        service.startMonitoringStatus(of: system)
    }
    
    func stopMonitoringOTA(system: TAPSSystem) {
        service.stopMonitoringStatus(of: system)
    }
    
    func uploadFirmwareToDevice(system: TAPSSystem, version: String, binary: Data) {
        service.startUpdate(of: system, to: version, using: binary)
    }
}

extension TymphanyFirmwareOverTheAirUpdateServiceAdapter: TAPSFirmwareOverTheAirUpdateServiceDelegate {
    func firmwareOverTheAirUpdateService(_ system: TAPSSystem, didUpdate status: TAPSFirmwareOTAStatus) {
        print("🔄 firmwareOverTheAirUpdateService status: \(status)")
        let status = TymphanyOTAUpdateStatus(system: system, status: status)
        updateStatus.accept(status)
    }
}
