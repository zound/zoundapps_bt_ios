//
//  TymphanyDigitalSignalProcessingServiceAdapter.swift
//  JoplinPlatform
//
//  Created by Grzegorz Kiel on 05/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import TAPlatformSwift
import GUMA

final class TymphanyDigitalSignalProcessingServiceAdapter {
    private var service: TAPSDigitalSignalProcessingService!
    private var monitoredGraphicalEqualizer: PublishRelay<GraphicalEqualizer>?
    private var monitoredEqualizerButtonStep: PublishRelay<EqualizerButtonStep>?
    private var graphicalEqualizerBuffers: [String: GraphicalEqualizer] = [:]
    private var customPresetGraphicalEqualizerBuffers: [String: GraphicalEqualizer] = [:]
    func start() {
        let config: [String: Any] = [
            TAPSSystemKey.serviceInfo: TAPSResource.qcc3008ServiceInfo["serviceConfig"]!,
            TAPSSystemKey.connectionDuration: Int(TimeInterval.seconds(5))
        ]
        self.service = TAPSDigitalSignalProcessingService(type: .ble, config: config, delegate: self).start()
    }
}

extension TymphanyDigitalSignalProcessingServiceAdapter {
    func requestGraphicalEqualizer(for system: TAPSSystem) -> Observable<GraphicalEqualizer> {
        return Observable.create { [unowned self] observer in
            self.service.graphicalEqualizer(of: system) { result in
                switch result {
                case .success(let graphicalEqualizerDictionary):
                    guard let graphicalEqualizer = GraphicalEqualizer(graphicalEqualizerDictionary: graphicalEqualizerDictionary) else {
                        observer.onError(NSError())
                        return
                    }
                    observer.onNext(graphicalEqualizer)
                case .error(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    func startMonitoringGraphicalEqualizer(of system: TAPSSystem) -> PublishRelay<GraphicalEqualizer> {
        monitoredGraphicalEqualizer = PublishRelay<GraphicalEqualizer>()
        service.startMonitoringGraphicalEqualizer(of: system)
        return monitoredGraphicalEqualizer!
    }
    func stopMonitoringGraphicalEqualizer(of system: TAPSSystem) {
        service.stopMonitoringGraphicalEqualizer(of: system)
        monitoredGraphicalEqualizer = nil
    }
    func write(graphicalEqualizer: GraphicalEqualizer, for system: TAPSSystem) {
        if graphicalEqualizerBuffers.contains(where: { $0.key == system.tapSystem.uuidString }) {
            graphicalEqualizerBuffers[system.tapSystem.uuidString] = graphicalEqualizer
        } else {
            graphicalEqualizerBuffers[system.tapSystem.uuidString] = graphicalEqualizer
            let tapsGraphicalEqualizer: TAPSGraphicalEqualizer = (bass: Int(graphicalEqualizer.bass),
                                                                  low: Int(graphicalEqualizer.low),
                                                                  mid: Int(graphicalEqualizer.mid),
                                                                  upper: Int(graphicalEqualizer.upper),
                                                                  high: Int(graphicalEqualizer.high))
            service.write(graphicalEqualizer: tapsGraphicalEqualizer, for: system, completion: { [weak self] _ in
                guard let currentBufferedGraphicalEqualizer = self?.graphicalEqualizerBuffers[system.tapSystem.uuidString],
                          currentBufferedGraphicalEqualizer != graphicalEqualizer else {
                            self?.graphicalEqualizerBuffers.removeValue(forKey: system.tapSystem.uuidString)
                    return
                }
                self?.graphicalEqualizerBuffers.removeValue(forKey: system.tapSystem.uuidString)
                self?.write(graphicalEqualizer: currentBufferedGraphicalEqualizer, for: system)
            })
        }
    }
}

extension TymphanyDigitalSignalProcessingServiceAdapter {
    func requestLimitedEqualizerSettings(for system: TAPSSystem) -> Observable<LimitedEqualizerSettings> {
        return Observable.create { [unowned self] observer in
            self.service.equalizerSetting(of: system) { tapsEqualizerSettings in
                guard let step = EqualizerButtonStep(rawValue: tapsEqualizerSettings.step.rawValue),
                      let step2Preset = EqualizerButtonStepPreset(rawValue: tapsEqualizerSettings.step2Preset.rawValue),
                      let step3Preset = EqualizerButtonStepPreset(rawValue: tapsEqualizerSettings.step3Preset.rawValue) else {
                    observer.onError(NSError())
                    return
                }
                observer.onNext(LimitedEqualizerSettings(step: step, step2Preset: step2Preset, step3Preset: step3Preset))
            }
            return Disposables.create()
        }
    }
    func requestCustomPresetGraphicalEqualizer(for system: TAPSSystem) -> Observable<GraphicalEqualizer> {
        return Observable.create { [unowned self] observer in
            self.service.customPresetGraphicalEqualizer(of: system) { result in
                switch result {
                case .success(let graphicalEqualizerDictionary):
                    guard let graphicalEqualizer = GraphicalEqualizer(graphicalEqualizerDictionary: graphicalEqualizerDictionary) else {
                        observer.onError(NSError())
                        return
                    }
                    observer.onNext(graphicalEqualizer)
                case .error(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    func write(customPresetGraphicalEqualizer: GraphicalEqualizer, for system: TAPSSystem) {
        if customPresetGraphicalEqualizerBuffers.contains(where: { $0.key == system.tapSystem.uuidString }) {
            customPresetGraphicalEqualizerBuffers[system.tapSystem.uuidString] = customPresetGraphicalEqualizer
        } else {
            customPresetGraphicalEqualizerBuffers[system.tapSystem.uuidString] = customPresetGraphicalEqualizer
            let tapsGraphicalEqualizer: TAPSGraphicalEqualizer = (bass: Int(customPresetGraphicalEqualizer.bass),
                                                                  low: Int(customPresetGraphicalEqualizer.low),
                                                                  mid: Int(customPresetGraphicalEqualizer.mid),
                                                                  upper: Int(customPresetGraphicalEqualizer.upper),
                                                                  high: Int(customPresetGraphicalEqualizer.high))
            service.write(customPresetGraphicalEqualizer: tapsGraphicalEqualizer, for: system, completion: { [weak self] _ in
                guard let currentBufferedGraphicalEqualizer = self?.customPresetGraphicalEqualizerBuffers[system.tapSystem.uuidString],
                          currentBufferedGraphicalEqualizer != customPresetGraphicalEqualizer else {
                            self?.customPresetGraphicalEqualizerBuffers.removeValue(forKey: system.tapSystem.uuidString)
                    return
                }
                self?.customPresetGraphicalEqualizerBuffers.removeValue(forKey: system.tapSystem.uuidString)
                self?.write(customPresetGraphicalEqualizer: currentBufferedGraphicalEqualizer, for: system)
            })
        }
    }
    func startMonitoringEqualizerButtonStep(of system: TAPSSystem) -> PublishRelay<EqualizerButtonStep> {
        monitoredEqualizerButtonStep = PublishRelay<EqualizerButtonStep>()
        service.startMonitoringEqualizerButtonStep(of: system)
        return monitoredEqualizerButtonStep!
    }
    func stopMonitoringEqualizerButtonStep(of system: TAPSSystem) {
        service.stopMonitoringEqualizerButtonStep(of: system)
        monitoredEqualizerButtonStep = nil
    }
    func write(equalizerButtonStep: EqualizerButtonStep, for system: TAPSSystem) {
        guard let tapsEqualizerButtonStep = TAPSDigitalSignalProcessingServiceEQButtonStep(rawValue: equalizerButtonStep.rawValue) else {
            return
        }
        service.switchTo(equalizerButtonStep: tapsEqualizerButtonStep, for: system) { _ in }
    }
    func write(step2Preset: EqualizerButtonStepPreset, for system: TAPSSystem) {
        guard let tapsStep2Preset = TAPSDigitalSignalProcessingServiceEQPreset(rawValue: step2Preset.rawValue) else {
            return
        }
        service.write(step2Preset: tapsStep2Preset, for: system) { _ in }
    }
    func write(step3Preset: EqualizerButtonStepPreset, for system: TAPSSystem) {
        guard let tapsStep3Preset = TAPSDigitalSignalProcessingServiceEQPreset(rawValue: step3Preset.rawValue) else {
            return
        }
        service.write(step3Preset: tapsStep3Preset, for: system) { _ in }
    }
}

extension TymphanyDigitalSignalProcessingServiceAdapter: TAPSDigitalSignalProcessingServiceDelegate {
    func dspService(_ system: TAPSSystem, didUpdate equalizer: [AnyHashable: Any]) {
        print("⚠️ dspService(_ system: TAPSSystem, didUpdate equalizer: [AnyHashable: Any]) - new unimplemented delegate call - take a closer look.")
    }
    func dspService(_ system: TAPSSystem, didUpdateTone equalizer: [AnyHashable: Any]) {
        print("⚠️ dspService(_ system: TAPSSystem, didUpdateTone equalizer: [AnyHashable: Any]) - new unimplemented delegate call - take a closer look.")
    }
    func dspService(_ system: TAPSSystem, didUpdateGraphical equalizer: [AnyHashable: Any]) {
        guard let graphicalEqualizer = GraphicalEqualizer(graphicalEqualizerDictionary: equalizer) else {
            return
        }
        graphicalEqualizerBuffers.removeValue(forKey: system.tapSystem.uuidString)
        monitoredGraphicalEqualizer?.accept(graphicalEqualizer)
    }
    func dspService(_ system: TAPSSystem, didUpdate equalizerButtonStep: TAPSDigitalSignalProcessingServiceEQButtonStep) {
        guard let step = EqualizerButtonStep(rawValue: equalizerButtonStep.rawValue) else {
            return
        }
        monitoredEqualizerButtonStep?.accept(step)
    }
}
