//
//  TymphanySystemServiceAdapter.swift
//  JoplinPlatform
//
//  Created by Grzegorz Kiel on 05/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import TAPlatformSwift
import GUMA
import ExternalAccessory

private struct Config {
    static let disconnectTimeout = TimeInterval.seconds(3.0)
}
private struct BroadcastEntry {
    let devices: [DeviceType]
    let toBeBroadcasted: DeviceType?
}

extension BatteryStatus {
    init(stringValue: String) {
        guard let intValue = Int(stringValue) else {
            self = .unknown
            return
        }
        switch(intValue) {
        case 0..<20:
            self = .l10(intValue)
        case 20..<30:
            self = .l20(intValue)
        case 30..<40:
            self = .l30(intValue)
        case 40..<50:
            self = .l40(intValue)
        case 50..<60:
            self = .l50(intValue)
        case 60..<70:
            self = .l60(intValue)
        case 70..<80:
            self = .l70(intValue)
        case 80..<90:
            self = .l80(intValue)
        case 90..<100:
            self = .l90(intValue)
        case 100:
            self = .full
        default:
            self = .unknown
        }
    }
}
final class TymphanySystemServiceAdapter {
    let service: TAPSSystemService
    var managedDevices = BehaviorRelay<[DeviceType]>(value: [])
    let adapterState = PublishSubject<DeviceAdapterState>()
    let platform: Platform
    let discoveredInfoObservable = PublishRelay<DeviceType>()
    var externalAccessoryService = ExternalAccessoryService()
    
    private var ledBuffers: [String: Int] = [:]
    private var monitoredAutoOffTime: PublishRelay<AutoOffTime>?
    
    init(managedDevices: BehaviorRelay<[DeviceType]>,
         playControlServiceAdapter: TymphanyPlayControlServiceAdapter,
         dspServiceAdapter: TymphanyDigitalSignalProcessingServiceAdapter,
         firmwareOTAAdapter: TymphanyFirmwareOverTheAirUpdateServiceAdapterType,
         platform: Platform,
         storage: DefaultStorage
        ) {
        let config: [String: Any] = [
            TAPSSystemKey.serviceInfo: TAPSResource.qcc3008ServiceInfo["serviceConfig"]!,
            TAPSSystemKey.connectionDuration: Int(TimeInterval.seconds(5))
        ]
        self.playControlAdapter = playControlServiceAdapter
        self.dspControlAdapter = dspServiceAdapter
        self.otaAdapter = firmwareOTAAdapter
        self.managedDevices = managedDevices
        self.service = TAPSSystemService(type: .ble, config: config)
        self.platform = platform
        self._storage = storage
        self.service.delegate = self
        self.externalAccessoryService.delegate = self
        self.externalAccessoryService.setAccessoriesMonitoring(enabled: true)
    }
    func forget(_ device: DeviceType) {
        if managedDevices.value.contains(device: device) {
            managedDevices.accept(managedDevices.value.removing(device))
        }
        if let internalIndex = _internalDevices.firstIndex(where: { $0.id == device.id }) {
            _internalDevices.remove(at: internalIndex)
        }
        guard let storedDevicesInfo = _storage.storedDevicesInfo else { return }
        var toBeModified = storedDevicesInfo
        toBeModified = toBeModified.filter { $0.id != device.id }
        _storage.storedDevicesInfo = toBeModified
    }
    func mode(_ connectMode: TymphanyConnectMode, for deviceID: String) {
        connectModeInfo[deviceID] = connectMode
    }
    
    func globalMode(_ connectMode: TymphanyConnectMode?) {
        _globalConnectMode = connectMode
    }
    func processDiscoveredDevices() -> Single<Void> {
        var setupDisposeBag = DisposeBag()
        var wirelessStereoEntries = [DeviceType]()
        let devicesToProcess = self._currentScanResultIDs
            .map { [weak self] id in self?.internalDevice(for: id)}
            .compactMap { $0 }
        guard devicesToProcess.count > 0 else {
            return Single<Void>.just(())
        }
        return Single.create { [unowned self] event in
            let processActions = devicesToProcess
                .sorted(by: perDeviceSort)
                .map { [unowned self] device -> Observable<DeviceType?> in
                    return self.setupAction(for: device).asObservable()
                }
            Observable.concat(processActions)
                .observeOn(MainScheduler.instance)
                .map { [unowned self] device in BroadcastEntry(devices: self.managedDevices.value, toBeBroadcasted: device) }
                .filter(changedStateDevice)
                .subscribe(
                    onNext: { [weak self] broadcastEntry in
                        guard let device = broadcastEntry.toBeBroadcasted else { return }
                        self?.discoveredInfoObservable.accept(device)
                        guard device.state.outputs.perDeviceTypeFeatures.contains(.hasTrueWireless) else { return }
                        guard case .disconnected = device.state.outputs.trueWirelessStatus().stateObservable().value else {
                                wirelessStereoEntries.append(device)
                                return
                            }
                    },
                    onError: { error in },
                    onCompleted: { [weak self] in
                        guard wirelessStereoEntries.count > 0 else {
                            event(.success(()))
                            setupDisposeBag = DisposeBag()
                            return
                        }
                        verifyPaired(list: wirelessStereoEntries, managedDevices: self?._internalDevices)
                            .subscribe(
                                onSuccess: { _ in
                                    event(.success(()))
                                    setupDisposeBag = DisposeBag()
                                }
                        )
                        .disposed(by: setupDisposeBag)
                    }
                )
                .disposed(by: setupDisposeBag)

                
            
            return Disposables.create()
        }
    }
    func configurationInProgress() -> Bool {
        guard connectModeInfo.contains(where: { $0.value == .checkFirmware }) else {
            return false
        }
        return true
    }
    
    func connectMode(for deviceID: String) -> TymphanyConnectMode {
        guard let mode = connectModeInfo[deviceID] else {
            return .idle
        }
        return mode
    }
    func prepareForDisconnect(id: String) {
        guard let device = internalDevice(for: id) else { return }
        device.internalState.watchForDisconnect(true)
    }
    
    private var _globalConnectMode: TymphanyConnectMode? = .idle
    private let playControlAdapter: TymphanyPlayControlServiceAdapter
    private let dspControlAdapter: TymphanyDigitalSignalProcessingServiceAdapter
    private let otaAdapter: TymphanyFirmwareOverTheAirUpdateServiceAdapterType
    private var connectModeInfo: [String : TymphanyConnectMode] = [:]
    
    private var _savedScanResultIDs = Set<String>()
    private var _currentScanResultIDs = Set<String>()

    private var _storage: DefaultStorage
    private var _setupDisposeBag = DisposeBag()
    private var _disconnectIfNeededDisposeBag = DisposeBag()
    private var _verifyDisposeBag = DisposeBag()
    private var _finalizeDisposeBag = DisposeBag()
    private var _discoveryInProgress = false
    private var _processedDevices = Set<String>()
    private var _internalDevices = [TymphanyDevice]()
    private var _273workaround = true
}

private extension TymphanySystemServiceAdapter {
    private func device(for uuid: String) -> DeviceType? {
        return managedDevices.value.first { $0.id == uuid }
    }
    private func internalDevice(for uuid: String) -> TymphanyDevice? {
        return _internalDevices.first(where: { $0.id == uuid })
    }
}

private extension TymphanySystemServiceAdapter {
    func storedEntry(for deviceID: String) -> StoredDeviceInfoEntry? {
        return _storage.storedDevicesInfo?
            .filter { $0.id == deviceID }
            .first
    }
    func setupAction(for device:TymphanyDevice) -> Single<DeviceType?> {
        return Single<DeviceType?>.create(subscribe: { [unowned self] singleEvent in
            var setupDisposeBag = DisposeBag()
            let storedInfoEntry = self.storedEntry(for: device.id)
            device.internalState.watchForDisconnect(false)
            device.initialSetup(with: storedInfoEntry).asObservable().materialize()
                .take(1)
                .observeOn(MainScheduler.instance)
                .subscribe(
                    onNext: { [unowned self, weak device] processedEntry in
                        guard let deviceID = processedEntry.element else {
                            singleEvent(.success((device)))
                            setupDisposeBag = DisposeBag()
                            return
                        }
                        self._currentScanResultIDs.remove(deviceID)
                        singleEvent(.success(device))
                        switch device?.state.outputs.connectivityStatus().stateObservable().value {
                        case .some(.connected), .some(.wsMaster(peerMAC: _)), .some(.wsSlave(peerMAC: _)):
                            device?.internalState.watchForDisconnect(true)
                        default: break
                        }
                        setupDisposeBag = DisposeBag()
                    }
                )
                .disposed(by: setupDisposeBag)
            return Disposables.create()
        })
    }
}
extension TymphanySystemServiceAdapter {
    func requestMACAddress(of system: TAPSSystem) -> Observable<Data> {
        return Observable.create { [unowned self] observer -> Disposable in
            self.service.macAddress(for: system) { result in
                switch result {
                case .success(let macAddress):
                    observer.onNext(macAddress)
                    observer.onCompleted()
                case .error(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    func requestTrueWirelessPairedDeviceMACAddress(of system: TAPSSystem) -> Observable<Data> {
        return Observable.create { [unowned self] observer -> Disposable in
            self.service.trueWirelessPairedDeviceMACAddress(of: system) { result in
                switch result {
                case .success(let trueWirelessPairedDeviceMACAddress):
                    observer.onNext(trueWirelessPairedDeviceMACAddress)
                    observer.onCompleted()
                case .error(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    func requestModelNumber(of system: TAPSSystem) -> Observable<String> {
        return Observable.create { [unowned self] observer -> Disposable in
            self.service.modelNumber(of: system) { result in
                switch result {
                case .success(let value):
                    observer.onNext(value)
                case .error(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    func requestSoftwareVersion(of system: TAPSSystem) -> Observable<String> {
        return Observable.create { [unowned self] observer -> Disposable in
            self.service.softwareVersion(of: system) { result in
                switch result {
                case .success(let value):
                    observer.onNext(value)
                case .error(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    func hardwareEventInfo(modelNumber: Event<String>, softwareVersion: Event<String>) -> Observable<DeviceHardwareInfo?> {
        guard let model = modelNumber.element, let fw = softwareVersion.element else {
            return Observable.just(nil)
        }
        return Observable.just(DeviceHardwareInfo(model: model, firmwareVersion: fw))
    }
    func requestHardwareInfo(for system: TAPSSystem) -> Observable<DeviceHardwareInfo?> {
        return Observable.combineLatest(
            requestModelNumber(of: system).materialize(),
            requestSoftwareVersion(of: system).materialize()
        )
        .flatMap(hardwareEventInfo)
    }
    func batteryInfo(for system: TAPSSystem) -> Single<BatteryStatus> {
        return Single.create { [unowned self] event in
            self.service.batteryLevel(of: system) { result in
                switch result {
                case .success(let batteryString):
                    event(.success(BatteryStatus.init(stringValue: batteryString)))
                case .error:
                    event(.success(.unknown))
                }
            }
            return Disposables.create()
        }
    }
}

// MARK: - LED brightness control
extension TymphanySystemServiceAdapter {
    func requestBrightnessInfo(for system: TAPSSystem) -> Observable<Int> {
        return Observable.create { [unowned self] observer -> Disposable in
            self.service.brightness(of: system) { result in
                switch result {
                case .success(let value):
                    observer.onNext(value)
                    observer.onCompleted()
                case .error(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    func write(ledBrightness: Int, for system: TAPSSystem) {
        if ledBuffers.contains(where: { $0.key == system.tapSystem.uuidString }) {
            ledBuffers[system.tapSystem.uuidString] = ledBrightness
        } else {
            ledBuffers[system.tapSystem.uuidString] = ledBrightness
            service.write(brightness: ledBrightness, of: system) { [weak self] _ in
                guard let currentBufferedLed = self?.ledBuffers[system.tapSystem.uuidString], currentBufferedLed != ledBrightness else {
                    self?.ledBuffers.removeValue(forKey: system.tapSystem.uuidString)
                    return
                }
                self?.ledBuffers.removeValue(forKey: system.tapSystem.uuidString)
                self?.write(ledBrightness: currentBufferedLed, for: system)
            }
        }
    }
}

// MARK: - Rename functionality
extension TymphanySystemServiceAdapter {
    func requestCustomName(of system: TAPSSystem) -> Observable<String> {
        return Observable.create { [unowned self] observer -> Disposable in
            self.service.customName(of: system) { result in
                switch result {
                case .success(let value):
                    observer.onNext(value.replacingOccurrences(of: "\0", with: ""))
                    observer.onCompleted()
                case .error(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    func write(name: String, for system: TAPSSystem) {
        self.service.write(customName: name, of: system, completion: { _ in })
    }
}

extension TymphanySystemServiceAdapter {
    func requestAutoOffTime(for system: TAPSSystem) -> Observable<AutoOffTime> {
        return Observable.create { [unowned self] observer in
            self.service.autoOffTime(of: system) { autoOffTimeResult in
                switch autoOffTimeResult {
                case .success(let autoOffTimeInMinutes):
                    guard let autoOffTime = AutoOffTime(minutes: autoOffTimeInMinutes) else {
                        observer.onError(NSError())
                        return
                    }
                    observer.onNext(autoOffTime)
                    observer.onCompleted()
                case .error(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    func startMonitoringAutoOffTime(of system: TAPSSystem) -> PublishRelay<AutoOffTime> {
        monitoredAutoOffTime = PublishRelay<AutoOffTime>()
        service.startMonitoringAutoOffTime(of: system)
        return monitoredAutoOffTime!
    }
    func stopMonitoringAutoOffTime(of system: TAPSSystem) {
        service.stopMonitoringAutoOffTime(of: system)
        monitoredAutoOffTime = nil
    }
    func write(autoOffTime: AutoOffTime, for system: TAPSSystem) {
        service.write(autoOffTime: autoOffTime.totalMinutes, of: system, completion: { _ in })
    }
}
extension TymphanySystemServiceAdapter {
    func requestPairingMode(for system: TAPSSystem) -> Single<PairingMode> {
        return Single.create { [unowned self] event in
            self.service.pairingMode(of: system) { internalPairingStatus in
                switch internalPairingStatus {
                case .started:
                    event(.success(.enabled))
                case .stopped:
                    event(.success(.disabled))
                case .unknown:
                    event(.error(PairingModeError.unknownState))
                }
            }
            return Disposables.create()
        }
    }
    func startMonitoringPairingMode(of system: TAPSSystem) {
        service.startMonitoringPairingModeStatus(of: system)
    }
    func stopMonitoringPairingMode(of system: TAPSSystem) {
        service.stopMonitoringPairingModeStatus(of: system)
    }
    func write(pairingMode: PairingMode, for system: TAPSSystem) -> Single<Void> {
        return Single.create { [unowned self] event in
            switch pairingMode {
            case .enabled:
                self.service.startPairing(system) { _ in
                    event(.success(()))
                }
            case .disabled:
                self.service.stopPairing(system) { _ in
                    event(.success(()))
                }
            }
            return Disposables.create()
        }
    }
}
extension TymphanySystemServiceAdapter {
    func startMonitoringBatteryStatus(of system: TAPSSystem) {
        service.startMonitoringBatteryStatus(of: system)
    }
    func stopMonitoringBatterStatus(of system: TAPSSystem) {
        service.stopMonitoringBatteryStatus(of: system)
    }
}

extension TymphanySystemServiceAdapter {
    func requestPowerCableStatus(for system: TAPSSystem) -> Single<PowerCableStatus> {
        return Single.create { [unowned self] event in
            let completion: (TAPSSystemServicePowerCableConntectionStatus) -> Void  = { powerCableStatus in
                switch powerCableStatus {
                case .connected:
                    event(.success(.connected))
                case .notConnected:
                    event(.success(.disconnected))
                case .unknown:
                    event(.success(.unknown))
                }
            }
            self.service.powerStatus(of: system, completion: completion)
            return Disposables.create()
        }
    }
    func startMonitoringPowerCableConnectionStatus(of system: TAPSSystem) {
        service.startMonitoringPowerCableConnectionStatus(of: system)
    }
    func stopMonitoringPowerCableConnectionStatus(of system: TAPSSystem) {
        service.stopMonitoringPowerCableConnectionStatus(of: system)
    }
}


extension TymphanySystemServiceAdapter: TAPSSystemServiceDelegate {
    
    func systemService(didUpdate state: TAPSSystemServiceState) {
        print(">> ", "System service state:", state)
        print(">> ", "Connected systems:", service.connectedSystems)
        
        switch state {
        case .ready:
            adapterState.onNext(.ready)
        case .off:
            adapterState.onNext(.btDisabled)
            _internalDevices.forEach {
                $0.state.inputs.disconnect()
                $0.state.inputs.set(connectivityStatus: .disconnected)
            }
        default:
            fatalError("New unsupported state : \(state)")
        }
    }
    
    func systemService(didDiscover system: TAPSSystem, advertisementData: [AnyHashable : Any], rssi: Int?) {
    
        guard let _ = system.type else { return }
    
        /// We want to avoid passing by results of spontaneous scans
        /// (it happens sometimes after BT connection error - restart cycle)
        let connectMode = connectModeInfo[system.tapSystem.uuidString]
        let globalConnectMode = _globalConnectMode
        
        switch(connectMode, globalConnectMode) {
        case (.some(.ota), .some(.idle)),
             (.some(.ota), nil),
             (nil, .some(.ota)),
             (.some(_), .some(.discovery)),
             (nil, .some(.discovery)):
            break
        default:
            return
        }

        print("📻 ", "System type:", system.type!)
        print("📻 ", "System name:", system.name)
        print("📻 ", "System UUID:", system.tapSystem.uuidString)
        print("📻 ", "Discovered: \(advertisementData), rssi: \(String(describing: rssi))")

        let localName = advertisementData[TAPSSystemKey.localName] as? String
        if let manufacturerData = advertisementData["CBAdvertisementDataManufacturerDataKey"] as? Data {
            system.tapSystem.changeDataMode(by: manufacturerData)
        }
        let discovered = TymphanyDevice(hwDevice: system,
                                        systemService: self,
                                        playControlAdapter: playControlAdapter,
                                        dspControlAdapter: dspControlAdapter,
                                        firmwareOTAAdapter: otaAdapter,
                                        connectivityStatus: .notConfigured,
                                        platform: platform)
        // WORKAROUND FOR OZZY-273
        if _273workaround == true && discovered.hardwareType == .headset {
            if skip273(managedDevices: managedDevices, discovered: discovered) == true {
                return
            }
        }
        if let hwMacAddressData = system.tapSystem.macAddress {
            discovered.mac = MAC(data: hwMacAddressData)
        }
        discovered.state.outputs.name().stateObservable().accept(localName ?? system.name)
        
        if let _ = storedEntry(for: discovered.id) { discovered.configured = true }
        setup(with: discovered)
        switch _globalConnectMode {
        case .some(.discovery):
            _currentScanResultIDs.insert(system.tapSystem.uuidString)
        case .some(.ota):
            discoveredInfoObservable.accept(discovered)
        default:
            if let connectMode = connectModeInfo[system.tapSystem.uuidString] {
                switch connectMode {
                case .ota:
                    discoveredInfoObservable.accept(discovered)
                    return
                default: break
                }
            }
            discoveredInfoObservable.accept(discovered)
        }
    }
    func setup(with discovered: TymphanyDevice) {
        guard let existingInternal = _internalDevices.filter({ $0.hw.tapSystem.uuidString == discovered.hw.tapSystem.uuidString }).first else {
            _internalDevices.append(discovered)
            managedDevices.accept(managedDevices.value.appending(discovered))
            return
        }
        // update TAPSSystem pointer
        existingInternal.hw = discovered.hw
        let newName = discovered.state.outputs.name().stateObservable().value
        if existingInternal.state.outputs.name().stateObservable().value != newName {
            existingInternal.state.outputs.name().stateObservable().accept(newName)
        }
    }
    func systemService(didConnect system: TAPSSystem, success: Bool, error: Error?) {
        guard let device = internalDevice(for: system.tapSystem.uuidString) else {
            return
        }
        guard success else {
            return
        }
        device.internalState.silentOutput.status().stateObservable().accept(.connected)
    }
    func systemService(didDisconnect system: TAPSSystem, error: Error?) {
        guard let device = internalDevice(for: system.tapSystem.uuidString) else {
            return
        }
        device.internalState.silentOutput.status().stateObservable().accept(.disconnected)
    }
    func systemService(_ system: TAPSSystem, didUpdate autoOffTime: Int) {
        guard let autoOffTime = AutoOffTime(minutes: autoOffTime) else {
            return
        }
        monitoredAutoOffTime?.accept(autoOffTime)
    }
    func systemService(_ system: TAPSSystem, didUpdate pairingModeStatus: TAPSSystemServicePairingStatus) {
        guard let device = device(for: system.tapSystem.uuidString) else {
            return
        }
        switch pairingModeStatus {
        case .started:
            device.state.outputs.monitoredPairingMode().stateObservable().accept(.enabled)
        case .stopped:
            device.state.outputs.monitoredPairingMode().stateObservable().accept(.disabled)
        default: break
        }
    }
    func systemService(_ system: TAPSSystem, didReceive error: Error?) {
        print("⚠️ systemService(_ system: TAPSSystem, didReceive error: Error?) Error: \(String(describing: error)) - new unimplemented delegate call - take a closer look.")
    }
    
    func systemService(didUpdate system: TAPSSystem) {
        print("⚠️ systemService(didUpdate system:) - new unimplemented delegate call - take a closer look.")
    }
    
    func systemService(_ system: TAPSSystem, didUpdate batteryStatus: String) {
        guard let device = device(for: system.tapSystem.uuidString) else {
            return
        }
        let batteryStatus = BatteryStatus(stringValue: batteryStatus)
        device.state.outputs.monitoredBatteryStatus().stateObservable().accept(batteryStatus)
    }
    
    func systemService(_ system: TAPSSystem, didUpdate powerStatus: TAPSSystemServicePowerStatus) {
        print("⚠️ systemService(_ system: TAPSSystem, didUpdate powerStatus: TAPSSystemServicePowerStatus) - new unimplemented delegate call - take a closer look.")
    }
    
    func systemService(_ system: TAPSSystem, didUpdate speakerLinkConfig: [AnyHashable: Any]) {
        print("⚠️ systemService(_ system: TAPSSystem, didUpdate speakerLinkConfig: [AnyHashable: Any]) - new unimplemented delegate call - take a closer look.")
    }
    
    func systemService(_ system: TAPSSystem, didUpdate powerCableConnectionStatus: TAPSSystemServicePowerCableConntectionStatus) {
        guard let device = device(for: system.tapSystem.uuidString) else {
            return
        }
        switch powerCableConnectionStatus {
        case .connected:
            device.state.outputs.monitoredPowerCableStatus().stateObservable().accept(.connected)
        case .notConnected:
            device.state.outputs.monitoredPowerCableStatus().stateObservable().accept(.disconnected)
        case .unknown:
            device.state.outputs.monitoredPowerCableStatus().stateObservable().accept(.unknown)
        }
    }
}
private func perDeviceSort(lhs: TymphanyDevice, rhs: TymphanyDevice) -> Bool {
    switch (lhs.hardwareType, rhs.hardwareType) {
    case (.headset, .speaker):
        return true
    default: return false
    }
}
private func skip273(managedDevices: BehaviorRelay<[DeviceType]>, discovered: DeviceType) -> Bool {
    guard let existing = managedDevices.value.first(where: { $0.id == discovered.id }) else {
        return false
    }
    return existing.state.outputs.connectivityStatus().stateObservable().value == .connected
}
extension TymphanySystemServiceAdapter: ExternalAccessoryServiceDelegate {
    func externalAccessoryServiceDidCancelAccessoryPicker(service: ExternalAccessoryService, error: EABluetoothAccessoryPickerError?) {
        let device = _internalDevices
            .filter({
                $0.state.outputs.accessoryPairingStatus().stateObservable().value == .waitingForConfirmation
            })
            .first
        guard let error = error, let waitingDevice = device else {
            return
        }
        switch error.code {
        case .alreadyConnected:
            break
        case .resultCancelled, .resultNotFound, .resultFailed:
            waitingDevice.state.outputs.accessoryPairingStatus().stateObservable().accept(.cancelled)
        }
    }
    func externalAccessory(service: ExternalAccessoryService, didChangeAccessoryState accessoryInfo: AccessoryInfo) {
        guard let device = accessoryDevice(mac: accessoryInfo.mac, devices: _internalDevices) else {
            return
        }
        guard accessoryInfo.connected == true else {
            device.state.outputs.accessoryPairingStatus().stateObservable().accept(.disconnected)
            return
        }
        guard device.state.outputs.accessoryPairingStatus().stateObservable().value == .waitingForConfirmation else {
            return
        }
        device.state.outputs.accessoryPairingStatus().stateObservable().accept(.connected)
    }
}
private func accessoryDevice(mac: String, devices: [TymphanyDevice]) -> TymphanyDevice? {
    return devices
        .filter({ device in
            guard let internalMAC = device.mac else { return false }
            if internalMAC.hex == mac { return true }
            return false
        }).first
}
private func verifyPaired(list: [DeviceType], managedDevices: [TymphanyDevice]?) -> Single<Void> {
    return Single.create { event in
        var verifyDisposeBag = DisposeBag()
        var pairs = [WirelessStereoPairEntry]()
        list.forEach { device in
            guard var existing = pairs.filter({ $0.pairExists(for: device) }).first else {
                var pair = WirelessStereoPairEntry()
                pair.add(device: device)
                pairs.append(pair)
                return
            }
            pairs.removeAll(where: {
                $0.pairExists(for: device)
            })
            existing.add(device: device)
            pairs.append(existing)
        }
        let brokenPairs = pairs.filter { $0.verified() == false }
        let brokenActions = brokenPairs.map { $0.concludeAction(managedDevices: managedDevices).asObservable().materialize() }
        Observable.concat(brokenActions)
            .subscribe(
                onCompleted: {
                    event(.success(()))
                    verifyDisposeBag = DisposeBag()
                }
            )
            .disposed(by: verifyDisposeBag)
        return Disposables.create()
    }
}
extension WirelessStereoPairEntry {
    func concludeAction(managedDevices: [TymphanyDevice]?) -> Single<Void> {
        guard let device = master ?? slave else {
            return Single.just(())
        }
        guard let mac = device.mac else {
            return device.bleConnect()
        }
        var peerMAC = MAC(data: Data())
        var updatedStatus: ConnectivityStatus = .disconnected
        switch device.state.outputs.trueWirelessStatus().stateObservable().value {
        case let .connectedAsMaster(_, oppositeMACData):
            peerMAC = MAC(data: oppositeMACData)
            updatedStatus = .wsSlave(peerMAC: mac)
        case let .connectedAsSlave(_, oppositeMACData):
            peerMAC = MAC(data: oppositeMACData)
            updatedStatus = .wsMaster(peerMAC: mac)
        default: break
        }
        guard let managedDevices = managedDevices else {
            return device.bleConnect()
        }
        guard let paired = managedDevices.filter({ $0.mac?.hex == peerMAC.hex }).first else {
            return device.bleConnect()
        }
        return paired.internalState
            .wait(for: .disconnected)
            .flatMap { [weak paired] _ -> Single<Void> in
                guard let paired = paired else { return Single.just(()) }
                return self.finalize(peerDevice: paired, finalStatus: updatedStatus) }
    }
    private func finalize(peerDevice: DeviceType,  finalStatus: ConnectivityStatus) -> Single<Void> {
        peerDevice.state.inputs.set(connectivityStatus: finalStatus)
        return Single.just(())
    }
}
private func changedStateDevice(entry: BroadcastEntry) -> Bool {
    guard let device = entry.toBeBroadcasted else { return false }
    if device.state.outputs.connectivityStatus().stateObservable().value == .readyToConnect {return false }
    return true
}

