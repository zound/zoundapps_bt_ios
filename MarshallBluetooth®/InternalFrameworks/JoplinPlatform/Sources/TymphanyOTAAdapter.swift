//
//  TymphanyOTAAdapter.swift
//  JoplinPlatform
//
//  Created by Grzegorz Kiel on 07/06/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import RxSwift
import TAPlatformSwift
import GUMA
import RCER
import Reachability

enum OTAErrorPriority: Int {
    case High
    case Medium
    case Low
}

enum TymphanyOTAAdapterState {
    case connected
    case checkedFirmwareUpdateAvailable(TymphanyOTAInfo?)
    case uploadingToDevice(FirmwareCache)
    case uploadCompleted
    case paused
    
    case rebooting
    case done
}

private struct Config {
    static let checkOTAStatusTimeout = 20.0
    static let otaUploadTickTimeout = 10.0
    static let waitForFirstRebootTimeout = 20.0
    static let flashingTickTimeout = 10.0
    static let checkingRemoteFirmwareTimeout = 10.0
    static let checkingLocalFirmwareTimeout = 5.0
    static let forcedVersion = Version(version: "5.0.2")
}

extension UnitType {
    func cachedDirectoryComponent() -> String {
        switch self {
        case .actonII: return "small"
        case .stanmoreII: return "medium"
        case .woburnII: return "large"
        case .actonIILite: return "smallLite"
        case .stanmoreIILite: return "mediumLite"
        case .woburnIILite: return "largeLite"
        case .ozzy: return "ozzy"
        case .ozzyAnc: return "ozzyAnc"
        }
    }
}

struct TymphanyFirmwareVersion {
    let value: Int
    let stringValue: String
    let version: Version
    
    init?(string: String) {
        guard let versionWithoutDate = string
            .split(separator: "(").first else {
                return nil
        }
        self.version = Version(version: String(versionWithoutDate))
        self.stringValue = String(versionWithoutDate)
        let withoutDots = String(versionWithoutDate.split(separator: ".").joined())
        guard let intValue = Int(withoutDots) else {
            return nil
        }
        self.value = intValue
    }
}

struct TymphanyFirmwareInfo {
    var model: UnitType
    var currentVersion: TymphanyFirmwareVersion?
    
    init(model: UnitType, version: String) {
        self.model = model
        self.currentVersion = TymphanyFirmwareVersion(string: version)
    }
}

struct RemoteTymphanyFirmwareInfo {
    static let unzippedMD5FileName = "upgrade.md5"
    static let unzippedFwBin = "upgrade.bin"
    
    var model: UnitType
    var latestVersion: TymphanyFirmwareVersion?
    var downloadURL: String
    
    func cachedBinFilename() -> String? {
        guard let cachedDirectory = cachedDirectory() else {
            return nil
        }
        return cachedDirectory.appending("/" + type(of: self).unzippedFwBin)
    }
    
    func cachedMD5Filename() -> String? {
        guard let cachedDirectory = cachedDirectory() else {
            return nil
        }
        return cachedDirectory.appending("/" + type(of: self).unzippedMD5FileName)
    }
    
    func cachedDirectory() -> String? {
        guard let latestVersion = latestVersion?.value else {
            return nil
        }
        return model.cachedDirectoryComponent() + String(latestVersion)
    }
}

struct TymphanyLatestFirmwareRequestInfo {
    let model: UnitType
    let localFirmwareInfo: TymphanyFirmwareInfo
    let request: URLRequest
}

struct TymphanyOTAInfo {
    var localInfo: TymphanyFirmwareInfo
    var remoteInfo: RemoteTymphanyFirmwareInfo
}

public final class TymphanyOTAAdapter: OTAAdapterTypeInput, OTAAdapterTypeOutput, OTAAdapterType {
    public var adapterError: BehaviorRelay<AnyError<OTAError>?>
    static var baseUpdateOTAURL = "https://usk4y0tsk2.execute-api.us-east-1.amazonaws.com/prod/firmware/zound/"
    static var keyHeaderLabel = "x-api-key"
    static var keyHeaderValue = "iygP2NbSY25sHVzmcf9VA1lN7X5i6Dr016itQgba"
    static var encodingHeaderLabel = "accept"
    static var encodingHeaderValue = "application/json"
    
    public var inputs: OTAAdapterTypeInput { return self }
    public var outputs: OTAAdapterTypeOutput { return self }
    public var deviceID: String
    
    init(systemServiceAdapter: TymphanySystemServiceAdapter,
         hwDevice device: TAPSSystem,
         otaAdapter adapter: TymphanyFirmwareOverTheAirUpdateServiceAdapterType) {
        
        guard let reachability = Reachability() else { fatalError("Could not initialize Reachability") }
        
        self._systemServiceAdapter = systemServiceAdapter
        self._underlyingHw = device
        self.deviceID = _underlyingHw.uuid
        self._firmwareOTAAdapter = adapter
        self.adapterError = BehaviorRelay<AnyError<OTAError>?>.init(value: nil)
        self.errorReporter = ErrorReporter<OTAError>(onlineTracker: reachability)
        
    }
    
    public func checkUpdateAvailable(deviceID: String) -> Single<UpdateAvailableInfo?> {
        // TODO: At this point we're not able to update Ozzys firmware
        guard let type = _underlyingHw.type else {
            return Single.just(nil)
        }
        switch type {
        case .ozzy, .ozzyAnc:
            return Single.just(nil)
        default: break
        }
        return checkUpdateAvailable(deviceID, cachedFirmware: nil)
            .flatMap { [weak self] otaAdapterState -> Single<UpdateAvailableInfo?> in
                switch otaAdapterState {
                case .checkedFirmwareUpdateAvailable(let otaInfo):
                    guard let otaInfo = otaInfo else {
                        return Single.just(nil)
                    }
                    guard let device = self?.device(for: deviceID), device.state.outputs.perDeviceTypeFeatures.contains(.hasForcedOTA) else {
                        return Single.just(UpdateAvailableInfo(deviceID: deviceID, forced: false))
                    }
                    guard let currentVersion = otaInfo.localInfo.currentVersion?.version, currentVersion <= Config.forcedVersion else {
                        return Single.just(UpdateAvailableInfo(deviceID: deviceID, forced: false))
                    }
                    return Single.just(UpdateAvailableInfo(deviceID: deviceID, forced: true ))
                default:
                    return Single.just(nil)
                }
            }
    }
    
    public func checkForcedOTA(deviceID: String) -> Single<UpdateAvailableInfo?> {
        return Single.create(subscribe: { [weak self] singleEvent in
            guard let strongSelf = self, let device = strongSelf.device(for: deviceID) else {
                singleEvent(.error(OTAError.otherError))
                return Disposables.create()
            }
            guard device.state.outputs.perDeviceTypeFeatures.contains(.hasForcedOTA) else {
                singleEvent(.success(UpdateAvailableInfo(deviceID: deviceID, forced: false)))
                return Disposables.create()
            }
            var checkForcedOTADisposeBag = DisposeBag()
            strongSelf.connectDevice(deviceID)
                .observeOn(MainScheduler.instance)
                .flatMap { [unowned strongSelf] _ in
                    return strongSelf.localFirmware()
                }
                .subscribe(
                    onSuccess: { [weak self] localVersion in
                        guard let local = localVersion else {
                            singleEvent(.success(nil))
                            checkForcedOTADisposeBag = DisposeBag()
                            return
                        }                        
                        guard let device = self?.device(for: deviceID), device.state.outputs.perDeviceTypeFeatures.contains(.hasForcedOTA) else {
                            singleEvent(.success(UpdateAvailableInfo(deviceID: deviceID, forced: false)))
                            checkForcedOTADisposeBag = DisposeBag()
                            return
                        }
                        guard local.version <= Config.forcedVersion else {
                            singleEvent(.success(UpdateAvailableInfo(deviceID: deviceID, forced: false)))
                            checkForcedOTADisposeBag = DisposeBag()
                            return
                        }
                        singleEvent(.success(UpdateAvailableInfo(deviceID: deviceID, forced: true)))
                        checkForcedOTADisposeBag = DisposeBag()
                    },
                    onError: { error in
                        singleEvent(.error(error))
                        checkForcedOTADisposeBag = DisposeBag()
                    }
                ).disposed(by: checkForcedOTADisposeBag)
            return Disposables.create()
        })
    }

    public func startUpdate(cachedFirmware: FirmwareCache? = nil,
                            interfaceError: BehaviorRelay<DeviceAdapterError?> = BehaviorRelay<DeviceAdapterError?>.init(value: nil),
                            processMode: PublishRelay<ApplicationProcessMode>) {
        errorReporter.reset()
        stopReporting(.bluetoothOff)
        stopReporting(.internetNotAvailable)
        stopReporting(.deviceDisconnected)
        
        updateDisposeBag = DisposeBag()
        currentStatus = .connecting
        self.interfaceError = interfaceError
        self.processMode = processMode
        
        guard let systemServiceAdapter = _systemServiceAdapter, let otaAdapter = _firmwareOTAAdapter else {
            fatalError("No system service adapter or ota Adapter found")
        }
        systemServiceAdapter.globalMode(.ota)
        systemServiceAdapter.service.stopScanningForSystems()
        checkUpdateAvailable(deviceID, cachedFirmware: cachedFirmware)
            .flatMap { [unowned self] state -> Single<TymphanyOTAAdapterState> in
                switch state {
                case .checkedFirmwareUpdateAvailable(let otaInfo):
                    return self.downloadFirmware(otaInfo)
                case .uploadingToDevice(let cache):
                    self.stopReporting(.internetNotAvailable)
                    return self.checkOTAStatus(firmware: cache)
                case .rebooting:
                    return Single.just(state)
                case .done:
                    return Single.just(state)
                default:
                    fatalError("Unsupported state: \(state)")
                }
            }
            .flatMap { [unowned self] state -> Single<TymphanyOTAAdapterState> in
                switch state {
                case .uploadingToDevice(let cache):
                    return self.checkOTAStatus(firmware: cache)
                default:
                    return Single.just(state)
                }
            }
            .flatMap {
                return Single.just($0)
            }
            .observeOn(MainScheduler.instance)
            .subscribe(
                onSuccess: { [unowned systemServiceAdapter, unowned otaAdapter, weak self] adapterStatus in
                    guard let strongSelf = self else { return }
                    switch adapterStatus {
                    case .rebooting:
                        strongSelf.stopReporting(.deviceDisconnected)
                        strongSelf.stopReporting(.bluetoothOff)
                        systemServiceAdapter.service.scanForSystems()
                    case .done:
                        otaAdapter.inputs.stopMonitoringOTA(system:strongSelf._underlyingHw)
                        systemServiceAdapter.service.stopScanningForSystems()
                        strongSelf.updateProgress.onNext(OTAUpdateInfo(deviceID: strongSelf._underlyingHw.uuid, status: .completed, error: nil))
                    default:
                        fatalError("Unknown adapter status \(adapterStatus)")
                    }
                },
                onError: { [weak self, unowned otaAdapter] error in
                    guard let strongSelf = self else { return }
                    strongSelf.propagateError(deviceID: strongSelf.deviceID, status: strongSelf.currentStatus, error: error)
                    otaAdapter.inputs.pauseUpdateOTA(system: strongSelf._underlyingHw)
                    otaAdapter.inputs.stopMonitoringOTA(system:strongSelf._underlyingHw)
            })
            .disposed(by: updateDisposeBag)
    }
    
    public func completeUpdate(deviceID: String) {
        guard let systemServiceAdapter = _systemServiceAdapter else {
            fatalError("No system service adapter or ota Adapter found")
        }
        systemServiceAdapter.globalMode(.idle)
    }
    
    public func deviceDisconnected() {
        guard let systemServiceAdapter = _systemServiceAdapter, let otaAdapter = _firmwareOTAAdapter else {
            fatalError("No system service adapter or ota Adapter found")
        }
        systemServiceAdapter.service.stopScanningForSystems()
        otaAdapter.inputs.pauseUpdateOTA(system: _underlyingHw)
        otaAdapter.inputs.stopMonitoringOTA(system: _underlyingHw)
        device(for: deviceID)?.state.inputs.disconnect()
        systemServiceAdapter.service.scanForSystems()
    
        updateDisposeBag = DisposeBag()
    }
    
    public func cancel() {
        guard let systemServiceAdapter = _systemServiceAdapter, let otaAdapter = _firmwareOTAAdapter else {
            fatalError("No system service adapter or ota Adapter found")
        }
        systemServiceAdapter.service.stopScanningForSystems()
        otaAdapter.inputs.pauseUpdateOTA(system: _underlyingHw)
        otaAdapter.inputs.stopMonitoringOTA(system: _underlyingHw)
        device(for: deviceID)?.state.inputs.disconnect()
        systemServiceAdapter.service.scanForSystems()
        updateDisposeBag = DisposeBag()
    }
    
    deinit {
        print("💕 \(self) deinited successfuly")
    }
    
    public var updateAvailable = PublishSubject<Bool>()
    public var updateProgress = PublishSubject<OTAUpdateInfo>()
    var updateEnded = PublishSubject<Bool>()
    
    private var disposeBag = DisposeBag()
    
    private weak var _systemServiceAdapter: TymphanySystemServiceAdapter?
    private weak var _firmwareOTAAdapter: TymphanyFirmwareOverTheAirUpdateServiceAdapterType?
    private var _underlyingHw: TAPSSystem
    
    private var _waitingForReconnect = false
    private var updateDisposeBag = DisposeBag()
    private var currentStatus: OTAStatus = .connecting
    private var errorReporter: ErrorReporter<OTAError>
    private var interfaceError = BehaviorRelay<DeviceAdapterError?>.init(value: nil)
    private var errorTriggersDisposables = [TriggerDisposableEntry]()
    private var currentError: OTAError? = nil
    private var processMode = PublishRelay<ApplicationProcessMode>()
    private var currentProcessMode: ApplicationProcessMode = .foreground
}

private extension TymphanyOTAAdapter {
    
    private func connectDevice(_ id: String) -> Single<ConnectivityStatus> {
        guard let device = device(for: id) else {
            return Single.error(OTAError.otherError)
        }
        guard device.state.outputs.connectivityStatus().stateObservable().value != .connected else {
            _systemServiceAdapter?.prepareForDisconnect(id: id)
            return Single.just(.connected)
        }
        startReporting(.bluetoothOff)
        startReporting(.deviceDisconnected)
        return device.bleConnect()
            .map { return ConnectivityStatus.connected }
            .do(onSuccess: { [weak self] _ in
                self?._systemServiceAdapter?.prepareForDisconnect(id: id)
            })
    }

    private func checkUpdateAvailable(_ id: String, cachedFirmware: FirmwareCache?) -> Single<TymphanyOTAAdapterState> {
        return Single.create(subscribe: { [weak self] singleEvent in
            
            guard let strongSelf = self else { return Disposables.create() }
            var updateAvailableCheckDisposeBag = DisposeBag()
            strongSelf.connectDevice(id)
                .observeOn(MainScheduler.instance)
                .flatMap { [unowned strongSelf] connectivityStatus -> Single<TymphanyOTAAdapterState> in
                    guard cachedFirmware == nil else {
                        return strongSelf.checkOTAStatus(firmware: cachedFirmware!)
                    }
                    return strongSelf.checkRemoteFirmware(connectivity: connectivityStatus)
                }.subscribe(
                    onSuccess: { otaState in
                        singleEvent(.success(otaState))
                        updateAvailableCheckDisposeBag = DisposeBag()
                    },
                    onError: { error in
                        singleEvent(.error(error))
                        updateAvailableCheckDisposeBag = DisposeBag()
                    }
                )
                .disposed(by: updateAvailableCheckDisposeBag)

            return Disposables.create()
        })
    }
    
    func waitForConnectedDevice(id: String) -> Single<ConnectivityStatus> {
        
        var connectionDisposeBag = DisposeBag()
        
        return Single<ConnectivityStatus>.create(subscribe: { [weak self] singleEvent in
            guard let strongSelf = self else { return Disposables.create() }
            
            guard let deviceToConnect = strongSelf.device(for: id) else {
                singleEvent(.error(AnyError<OTAError>.init(id: .deviceInOTANotFoundInManagedDevicesList)))
                connectionDisposeBag = DisposeBag()
                return Disposables.create()
            }
            
            strongSelf.errorReporter.currentError.asObservable()
                .distinctUntilChanged()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { error in
                    guard error != nil else {
                        return
                    }
                    singleEvent(.error(error!))
                    connectionDisposeBag = DisposeBag()
                }).disposed(by: connectionDisposeBag)

            deviceToConnect.state.inputs.set(connectivityStatus: .connecting)
            
            deviceToConnect.state.outputs.connectivityStatus().stateObservable().asObservable()
                .skipWhile { !$0.connected && $0 != .readyToConnect }
                .take(1)
                .subscribe(
                    onNext: { connectivityStatus in
                        guard case .connected = connectivityStatus else {
                            singleEvent(.error(AnyError<OTAError>(id: .deviceDisconnected)))
                            connectionDisposeBag = DisposeBag()
                            return
                        }
                        connectionDisposeBag = DisposeBag()
                        return singleEvent(.success(connectivityStatus))
                    },
                    onError: { error in
                        connectionDisposeBag = DisposeBag()
                        return singleEvent(.error(error))
                }
                )
                .disposed(by: connectionDisposeBag)

            return Disposables.create()
        })
    }
    
    func checkRemoteFirmware(connectivity status: ConnectivityStatus) -> Single<TymphanyOTAAdapterState> {
       
        return Single.create(subscribe: { [weak self] singleEvent in
            
            guard let strongSelf = self else { return Disposables.create() }
            
            if case .connected = status {
                strongSelf.updateProgress.onNext(OTAUpdateInfo(deviceID: strongSelf._underlyingHw.uuid, status: .checkingFirmware, error: nil))
                strongSelf.currentStatus = .checkingFirmware
            }

            /// Some arbitraray timeout value picked without any scientific background
            let timeout = Config.checkingRemoteFirmwareTimeout
            var checkRemoteFirmwareDisposeBag = DisposeBag()
        
            strongSelf.startReporting(.internetNotAvailable)
            
            strongSelf.errorReporter.currentError.asObservable()
                .distinctUntilChanged()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { error in
                    guard error != nil else {
                        return
                    }
                    singleEvent(.error(error!))
                    checkRemoteFirmwareDisposeBag = DisposeBag()
                }).disposed(by: checkRemoteFirmwareDisposeBag)
            
            Observable.combineLatest(
                strongSelf.checkUnitType(),
                strongSelf.checkSoftwareVersion())
            .flatMap { (model, version) -> Single<TymphanyFirmwareInfo> in
                return Single.just(TymphanyFirmwareInfo(model: model, version: version))
            }
            .map { [unowned strongSelf] firmwareInfo in
                strongSelf.createFirmwareVersionURLRequest(localInfo: firmwareInfo)
            }
            .flatMap { [unowned strongSelf] latestFirmwareRequestInfo in
                return strongSelf.getLatestFirmwareURL(info: latestFirmwareRequestInfo)
            }
            .timeout(TimeInterval.seconds(timeout), scheduler: MainScheduler.instance)
            .map { [weak self] otaInfo -> TymphanyOTAInfo? in
                self?.checkIfUpdateNeeded(info: otaInfo)
            }
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext: { (otaInfo) in
                    singleEvent(.success(.checkedFirmwareUpdateAvailable(otaInfo)))
                    checkRemoteFirmwareDisposeBag = DisposeBag()
                },
                onError: { error in
                    singleEvent(.error(error))
                    checkRemoteFirmwareDisposeBag = DisposeBag()
                }
            )
            .disposed(by: checkRemoteFirmwareDisposeBag)
            
            return Disposables.create()
        })
    }
    
    private func checkUnitType() -> Observable<UnitType> {
        guard let speakerType = _underlyingHw.type else {
            return Observable.error(OTAError.unknownDeviceType)
        }
        return Observable.just(speakerType)
    }
    
    private func checkSoftwareVersion() -> Observable<String> {
        guard let systemServiceAdapter = _systemServiceAdapter else {
            fatalError("No system service adapter found")
        }
        return systemServiceAdapter.requestSoftwareVersion(of: _underlyingHw)
    }
    
    private func createFirmwareVersionURLRequest(localInfo: TymphanyFirmwareInfo) ->  TymphanyLatestFirmwareRequestInfo {
        
        var endpoint = type(of: self).baseUpdateOTAURL
        
        switch localInfo.model {
        case .actonII: endpoint.append("joplin_s/latest")
        case .stanmoreII: endpoint.append("joplin_m/latest")
        case .woburnII: endpoint.append("joplin_l/latest")
        case .actonIILite: endpoint.append("joplin_s_lite/latest")
        case .stanmoreIILite: endpoint.append("joplin_m_lite/latest")
        case .woburnIILite: endpoint.append("joplin_l_lite/latest")
        case .ozzy: endpoint.append("ozzy/latest")
        case .ozzyAnc: endpoint.append("ozzy_anc/latest")
        }
        
        guard let url = URL(string: endpoint) else {
            fatalError("❌ Could not create URL for OTA")
        }
        
        var request = URLRequest(url: url)
        
        request.addValue(type(of: self).keyHeaderValue, forHTTPHeaderField: type(of: self).keyHeaderLabel)
        request.addValue(type(of: self).encodingHeaderValue, forHTTPHeaderField: type(of: self).encodingHeaderLabel)
        
        return TymphanyLatestFirmwareRequestInfo(model: localInfo.model, localFirmwareInfo: localInfo, request: request)
    }
    
    private func getLatestFirmwareURL(info: TymphanyLatestFirmwareRequestInfo) -> Observable<TymphanyOTAInfo> {
        return URLSession.shared.rx.json(request: info.request)
            .flatMap({ jsonResponse -> Observable<TymphanyOTAInfo> in
                guard let dict = jsonResponse as? [String: Any] else {
                    return Observable.error(OTAError.errorParsingRemoteURL)
                }
                guard let version = dict["version"] as? String else {
                    return Observable.error(OTAError.unknownRemoteFwVersion)
                }
                guard let downloadURL = dict["url"] as? String else {
                    return Observable.error(OTAError.unknownRemoteDownloadURL)
                }
                
                let otaInfo = TymphanyOTAInfo(localInfo: info.localFirmwareInfo,
                                              remoteInfo: RemoteTymphanyFirmwareInfo(model: info.model, latestVersion: TymphanyFirmwareVersion(string: version),
                                                                                     downloadURL: downloadURL))
                guard let _ = otaInfo.remoteInfo.latestVersion?.value, let _ = otaInfo.localInfo.currentVersion?.value else {
                    return Observable.error(OTAError.emptyLocalOrRemoteFirmwareVersion)
                }
                return Observable.just(otaInfo)
            })
    }
    
    private func checkIfUpdateNeeded(info: TymphanyOTAInfo) -> TymphanyOTAInfo? {
        guard let remoteValue = info.remoteInfo.latestVersion?.version, let localValue = info.localInfo.currentVersion?.version else {
            return nil
        }
        return remoteValue > localValue ? info : nil
    }
    
    private func downloadFirmware(_ info: TymphanyOTAInfo?) -> Single<TymphanyOTAAdapterState> {
        
        self.currentStatus = .downloadingFirmware(0.0, nil)
        
        guard let info = info else {
            return Single.just(.done)
        }
        
        return Single.create(subscribe: { [weak self] singleEvent in
            
            var downloadDisposeBag = DisposeBag()
            guard let strongSelf = self else { return Disposables.create() }
            
            let downloader: TymphanyFirmwareDownloaderType = TymphanyFirmwareDownloader(info: info)
            
            strongSelf.errorReporter.currentError.asObservable()
                .distinctUntilChanged()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { error in
                    guard error != nil else {
                        return
                    }
                    singleEvent(.error(error!))
                    downloadDisposeBag = DisposeBag()
                }).disposed(by: downloadDisposeBag)

            downloader.outputs.status()
                .subscribeOn(MainScheduler.instance)
                .subscribe(
                    onNext: { [unowned strongSelf] status in
                        switch status {
                        case .downloading(let progress):
                            strongSelf.updateProgress
                                .onNext(OTAUpdateInfo(deviceID: strongSelf.deviceID, status: .downloadingFirmware(progress, nil), error: nil))
                        case .ready(let firmwareBinaryData, let version):
                            let cache = FirmwareCache(blob: firmwareBinaryData, version: version.stringValue)
                            strongSelf.updateProgress
                                .onNext(OTAUpdateInfo(deviceID: strongSelf.deviceID, status: .downloadingFirmware(1.0, cache), error: nil))
                            singleEvent(.success(.uploadingToDevice(cache)))
                            downloadDisposeBag = DisposeBag()
                        case .error(let error):
                            singleEvent(.error(error))
                            downloadDisposeBag = DisposeBag()
                        default:
                            break
                        }
                    },
                    onError: { error in
                        singleEvent(.error(error))
                        downloadDisposeBag = DisposeBag()
                    }
                )
                .disposed(by: downloadDisposeBag)
            
            downloader.inputs.startDownload()
            return Disposables.create()
        })
    }
    
    func checkOTAStatus(firmware: FirmwareCache) -> Single<TymphanyOTAAdapterState> {
        
        self.currentStatus = .uploadingToDevice(0.0)
        
        return Single<TymphanyOTAAdapterState>.create(subscribe: { [weak self] singleEvent -> Disposable in
            
            var checkOTAStatusDisposeBag = DisposeBag()
            
            /// Some arbitrary OTA ready timeout
            let checkOTAStatusTimeout = Config.checkOTAStatusTimeout
            
            guard let strongSelf = self else { return Disposables.create() }
            guard let otaAdapter = strongSelf._firmwareOTAAdapter else {
                fatalError("No system service adapter or ota Adapter found")
            }

            otaAdapter.outputs.updateStatus
                .skipWhile { otaInfo -> Bool in
                    switch otaInfo.status {
                    case .ready, .downloading, .btUpgradeFinished, .mcuUpgrading, .mcuUpgradeFinished:
                        return false
                    default: return true
                    }
                }
                .take(1)
                .timeout(TimeInterval.seconds(checkOTAStatusTimeout), scheduler: MainScheduler.instance)
                .observeOn(MainScheduler.instance)
                .flatMap { [weak self] status -> Single<TymphanyOTAAdapterState> in
                    guard let strongSelf = self else {
                        return Single.create { _ in Disposables.create() }
                    }
                    switch status.status {
                    case .ready, .downloading:
                        strongSelf.updateProgress
                            .onNext(OTAUpdateInfo(deviceID: strongSelf.deviceID, status: .uploadingToDevice(0.0), error: nil))
                        return strongSelf.waitForUploadCompletion()
                    case .btUpgradeFinished, .mcuUpgrading:
                        strongSelf.updateProgress
                            .onNext(OTAUpdateInfo(deviceID: strongSelf.deviceID, status: .uploadCompleted(0.0), error: nil))
                        return strongSelf.waitForFlashingCompletion()
                    case .mcuUpgradeFinished:
                        return Single.just(.done)
                    default:
                        fatalError("Unhandled status : \(status.status)")
                    }
                }
                .subscribe(onNext: { otaState in
                    singleEvent(.success(otaState))
                    checkOTAStatusDisposeBag = DisposeBag()
                }, onError: { error in
                    singleEvent(.error(error))
                    checkOTAStatusDisposeBag = DisposeBag()
                }).disposed(by: checkOTAStatusDisposeBag)
            
            otaAdapter.inputs.startMonitoringOTA(system: strongSelf._underlyingHw)
            otaAdapter.inputs.uploadFirmwareToDevice(system: strongSelf._underlyingHw, version: firmware.version, binary: firmware.blob)
            
            return Disposables.create()
        })
    }
    
    private func waitForUploadCompletion() -> Single<TymphanyOTAAdapterState> {
        return Single<TymphanyOTAAdapterState>.create(subscribe: { [weak self] singleEvent in
            
            guard let strongSelf = self else { return Disposables.create() }
            var uploadDisposeBag = DisposeBag()
            
            /// Some arbitrary OTA upload tick timeout
            let otaUploadTickTimeout = Config.otaUploadTickTimeout
            
            guard let otaAdapter = strongSelf._firmwareOTAAdapter else {
                fatalError("No system service adapter or ota Adapter found")
            }
            
            let tickOrTimeout = otaAdapter.outputs.updateStatus
                .map { status -> Event<(TAPSFirmwareOTAStatus, Float)> in
                    if case .downloading(let progress) = status.status { return Event.next((status.status, progress)) }
                    return Event.next((status.status, 0.0))
                }
                .timeout(TimeInterval.seconds(otaUploadTickTimeout), scheduler: MainScheduler.instance)
            
            strongSelf.errorReporter.currentError.asObservable()
                .distinctUntilChanged()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { error in
                    guard error != nil else {
                        return
                    }
                    singleEvent(.error(error!))
                    uploadDisposeBag = DisposeBag()
                }).disposed(by: uploadDisposeBag)
            
            Observable.combineLatest(
                    tickOrTimeout,
                    strongSelf.processMode.startWith(.foreground)
                )
                .flatMap { (tickEvent, appProcessMode) -> Single<Event<(TAPSFirmwareOTAStatus, Float)>> in
                    return Single.just(tickEvent)
                }
                .subscribe(
                    onNext: { [weak self] progress in
                        guard let strongSelf = self else { return }
                        guard let status = progress.element?.0 else { return }

                        switch status {
                        case .downloading(let progress):
                            strongSelf.updateProgress
                                .onNext(OTAUpdateInfo(deviceID: strongSelf.deviceID, status: .uploadingToDevice(progress / 100), error: nil))
                        case .downloadingFinished, .validated:
                            break
                        case .activating:
                            strongSelf._firmwareOTAAdapter?.inputs.stopMonitoringOTA(system: strongSelf._underlyingHw)
                            singleEvent(.success(.rebooting))
                            uploadDisposeBag = DisposeBag()
                        default:
                            strongSelf._firmwareOTAAdapter?.inputs.pauseUpdateOTA(system: strongSelf._underlyingHw)
                            strongSelf._firmwareOTAAdapter?.inputs.stopMonitoringOTA(system: strongSelf._underlyingHw)
                            singleEvent(.error(OTAError.otherError))
                            uploadDisposeBag = DisposeBag()
                        }
                    },
                    onError: { error in
                        singleEvent(.error(error))
                        uploadDisposeBag = DisposeBag()
                    }
                )
                .disposed(by: uploadDisposeBag)
            
            return Disposables.create()
        })
    }
    
    private func waitForFlashingCompletion() -> Single<TymphanyOTAAdapterState> {
        
        self.currentStatus = .uploadCompleted(0.0)
        startReporting(.bluetoothOff)
        
        return Single<TymphanyOTAAdapterState>.create(subscribe: { [weak self] singleEvent in
            
            guard let strongSelf = self else { return Disposables.create() }
            var flashingDisposeBag = DisposeBag()
            
            var lastProgress = 0
            
            /// Some arbitrary OTA upload tick timeout
            let flashingTickTimeout = Config.flashingTickTimeout
            
            guard let otaAdapter = strongSelf._firmwareOTAAdapter else {
                fatalError("No system service adapter or ota Adapter found")
            }
            
            let tickOrTimeout = otaAdapter.outputs.updateStatus
                .map { status -> Event<(TAPSFirmwareOTAStatus)> in
                    return Event.next(status.status)
                }
                .timeout(TimeInterval.seconds(flashingTickTimeout), scheduler: MainScheduler.instance)
                .asObservable()
            
            guard let connectionObservable = strongSelf.device(for: strongSelf.deviceID)?.state.outputs.connectivityStatus().stateObservable() else {
                return Disposables.create()
            }
            
            strongSelf.errorReporter.currentError.asObservable()
                .distinctUntilChanged()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { error in
                    guard error != nil else {
                        return
                    }
                    singleEvent(.error(error!))
                    flashingDisposeBag = DisposeBag()
                }).disposed(by: flashingDisposeBag)

            Observable.combineLatest(
                connectionObservable,
                tickOrTimeout
                )
                .flatMap { (connectvitiyStatus, flashingEvent) -> Single<(ConnectivityStatus,Event<TAPSFirmwareOTAStatus>)> in
                    return Single.just((connectvitiyStatus, flashingEvent))
                }
                .observeOn(MainScheduler.instance)
                .subscribe(
                    onNext: { [weak self] (connectivityStatus, flashingEvent) in
                        guard let strongSelf = self else { return }
                        guard let status = flashingEvent.element else { return }
                        
                        switch (connectivityStatus, status) {
                        case (.connecting, .btUpgradeFinished),
                             (.connected, .btUpgradeFinished),
                             (.connecting, .mcuUpgrading):
                            break
                        case (.connected, .mcuUpgrading(let progress)):
                            lastProgress = Int(floor(progress))
                            strongSelf.updateProgress
                                .onNext(OTAUpdateInfo(deviceID: strongSelf.deviceID, status: .uploadCompleted(progress / 100), error: nil))
                            if lastProgress == 100 {
                                singleEvent(.success(.rebooting))
                                flashingDisposeBag = DisposeBag()
                            }
                        case (.disconnected, .mcuUpgrading):
                            guard lastProgress == 100 else {
                                singleEvent(.error(AnyError<OTAError>.init(id: .deviceDisconnected)))
                                flashingDisposeBag = DisposeBag()
                                return
                            }
                            strongSelf._firmwareOTAAdapter?.inputs.stopMonitoringOTA(system: strongSelf._underlyingHw)
                        case (.connected, .mcuUpgradeFinished):
                            strongSelf._firmwareOTAAdapter?.inputs.stopMonitoringOTA(system: strongSelf._underlyingHw)
                            singleEvent(.success(.done))
                            flashingDisposeBag = DisposeBag()
                        default:
                            fatalError("Unhandled case: \(connectivityStatus) , \(status)")
                        }
                    },
                    onError: { error in
                        singleEvent(.error(error))
                        flashingDisposeBag = DisposeBag()
                    }
                ).disposed(by: flashingDisposeBag)
            
            return Disposables.create()
        })
    }
    
    private func propagateError(deviceID: String, status: OTAStatus, error: Error) {
        updateProgress.onError(error)
        updateProgress = PublishSubject<OTAUpdateInfo>()
    }
    
    private func device(for id: String) -> DeviceType? {
        guard let device = _systemServiceAdapter?.managedDevices.value
            .first(where: { $0.id == id }) else {
                return nil
        }
        return device
    }
    
    private func localFirmware() -> Single<TymphanyFirmwareVersion?> {
        return Single.create(subscribe: { [weak self] singleEvent in
            guard let strongSelf = self else { return Disposables.create() }
            
            var checkLocalFirmwareDisposeBag = DisposeBag()
            
            /// Some arbitraray timeout value picked without any scientific background
            let timeout = Config.checkingLocalFirmwareTimeout
            
            Observable.combineLatest(
                strongSelf.checkUnitType(),
                strongSelf.checkSoftwareVersion())
                .flatMap { (model, version) -> Single<TymphanyFirmwareVersion?> in
                    return Single.just(TymphanyFirmwareVersion(string: version))
                }
                .timeout(TimeInterval.seconds(timeout), scheduler: MainScheduler.instance)
                .subscribe(
                    onNext: { localVersion in
                        singleEvent(.success(localVersion))
                        checkLocalFirmwareDisposeBag = DisposeBag()
                },
                    onError: { error in
                        singleEvent(.error(error))
                        checkLocalFirmwareDisposeBag = DisposeBag()
                })
                .disposed(by: checkLocalFirmwareDisposeBag)
            return Disposables.create()
        })
    }
}

extension TymphanyOTAAdapter {

    struct TriggerDisposableEntry {
        let error: OTAError
        var disposeBag: DisposeBag
    }

    private func stopReporting(_ otaError: OTAError) {
        errorReporter.remove(sourceForType: otaError)
        errorTriggersDisposables = errorTriggersDisposables.filter { $0.error != otaError }
    }

    private func startReporting(_ otaError: OTAError) {
        switch otaError {
        case .bluetoothOff:
            errorReporter.append(errorSource: bluetoothErrorSource())
        case .internetNotAvailable:
            errorReporter.append(errorSource: internetErrorSource())
        case .deviceDisconnected:
            guard let deviceDisconnectedErrorSource = deviceDisconnectedErrorSource() else { return }
            errorReporter.append(errorSource: deviceDisconnectedErrorSource)
        default:
            break
        }
    }

    private func disposableEntry(for errorType: OTAError) -> TriggerDisposableEntry {
        if let existingDisposableEntryIndex = errorTriggersDisposables.index(where: { $0.error == errorType }) {
            errorTriggersDisposables.remove(at: existingDisposableEntryIndex)
        }
        let triggerDisposable = TriggerDisposableEntry(error: errorType, disposeBag: DisposeBag())
        errorTriggersDisposables.append(triggerDisposable)
        return triggerDisposable
    }
    
    private func bluetoothErrorSource() -> ErrorSource<OTAError> {
        
        let triggerDisposable = disposableEntry(for: .bluetoothOff)
        let trigger = BehaviorRelay<AnyError<OTAError>?>.init(value: nil)
        
        self.interfaceError
            .flatMap { adapterError -> Observable<AnyError<OTAError>?> in
                guard let adapterError = adapterError else {
                    return Observable.just(nil)
                }
                guard adapterError == .btInterfaceDisabled else {
                    return Observable.just(nil)
                }
                return Observable.just(AnyError<OTAError>.init(id: .bluetoothOff))
            }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { error in trigger.accept(error) })
            .disposed(by: triggerDisposable.disposeBag)
        
        return ErrorSource<OTAError>.init(type: .bluetoothOff, trigger: trigger, priority: OTAErrorPriority.High.rawValue)
    }

    private func internetErrorSource() -> ErrorSource<OTAError> {
        
        let triggerDisposable = disposableEntry(for: .internetNotAvailable)
        let trigger = BehaviorRelay<AnyError<OTAError>?>.init(value: nil)
        
        self.errorReporter.tracker.online()
            .flatMap { reachable -> Observable<AnyError<OTAError>?> in
                guard reachable == true else {
                    return Observable.just(AnyError<OTAError>.init(id: .internetNotAvailable))
                }
                return Observable.just(nil)
            }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { error in trigger.accept(error) })
            .disposed(by: triggerDisposable.disposeBag)
        
        return ErrorSource<OTAError>.init(type: .internetNotAvailable, trigger: trigger, priority: OTAErrorPriority.Low.rawValue)
    }
    
    private func deviceDisconnectedErrorSource() -> ErrorSource<OTAError>? {
        
        let triggerDisposable = disposableEntry(for: .deviceDisconnected)
        let trigger = BehaviorRelay<AnyError<OTAError>?>.init(value: nil)
        
        guard let connectivityState = device(for: deviceID)?.state.outputs.connectivityStatus().stateObservable() else {
            return nil
        }
        
        connectivityState
            .flatMap { connectivityStatus -> Observable<AnyError<OTAError>?> in
                switch connectivityStatus {
                case .disconnected:
                    return Observable.just(AnyError<OTAError>.init(id: .deviceDisconnected))
                default:
                    return Observable.just(nil)
                }
            }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { error in trigger.accept(error) })
            .disposed(by: triggerDisposable.disposeBag)

        return ErrorSource<OTAError>.init(type: .deviceDisconnected, trigger: trigger, priority: OTAErrorPriority.Medium.rawValue)
    }
}

