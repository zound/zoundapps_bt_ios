//
//  MacColorMapper.swift
//  JoplinPlatform
//
//  Created by Dolewski Bartosz A (Ext) on 13.09.2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import os

struct MacColorMapper {
    static var whiteActons: [String : String]? {
        return loadJson(name: "whiteActons")
    }
    
    static var whiteStanmores: [String : String]? {
        return loadJson(name: "whiteStanmores")
    }
    
    static var whiteWoburns: [String : String]? {
        return loadJson(name: "whiteWoburns")
    }
    
    static func isWhiteActon(mac: String) -> Bool {
        guard let lookup = whiteActons else { return false }
        return lookup[mac] != nil
    }
    
    static func isWhiteStanmore(mac: String) -> Bool {
        guard let lookup = whiteStanmores else { return false }
        return lookup[mac] != nil
    }
    
    static func isWhiteWoburn(mac: String) -> Bool {
        guard let lookup = whiteWoburns else { return false }
        return lookup[mac] != nil
    }
    
    static func loadJson(name: String) -> [String: String]? {
        if let path = Bundle.framework.path(forResource: name, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                return jsonResult as? Dictionary<String,String>
            } catch {
                os_log("Could not parse JSON file: %@.json", log: .default, type: .error, name)
                return nil
            }
        } else {
            os_log("Could not load JSON file: %@.json", log: .default, type: .fault, name)
            return nil
        }
    }
}

