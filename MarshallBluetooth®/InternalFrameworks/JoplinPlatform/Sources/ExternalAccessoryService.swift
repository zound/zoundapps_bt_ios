//
//  ExternalAccessoryService.swift
//  ConnectionService
//
//  Created by Wudarski Lukasz on 01/07/2019.
//  Copyright © 2019 ZoundIndustries. All rights reserved.
//

import Foundation
import ExternalAccessory

private struct Const {
    static let macAddressKey = "macAddress"
    static let macAddressBufferSize = 6
}
protocol ExternalAccessoryServiceDelegate: class {
    func externalAccessoryServiceDidCancelAccessoryPicker(service: ExternalAccessoryService, error: EABluetoothAccessoryPickerError?)
    func externalAccessory(service: ExternalAccessoryService, didChangeAccessoryState accessoryInfo: AccessoryInfo)
}
protocol ExternalAccessoryServiceType {
    func showAccessoryPicker()
    func isConnected(accessoryWith macAddress: String) -> Bool
    func setAccessoriesMonitoring(enabled: Bool)
    func startTestEASession(withAccessoryThatUses macAddress: String, for protocolString: String) // TODO: Remove
    func stopTestEASession() // TODO: Remove
}
class ExternalAccessoryService: NSObject, ExternalAccessoryServiceType {
    weak var delegate: ExternalAccessoryServiceDelegate?
    private let accessoryManager = EAAccessoryManager.shared()
    private var session: EASession?
    private var readMacAddressCompletion: (() -> Void)?
    private var testSession: EASession? // TODO: Remove
    private var testDataSendViaEASession = false // TODO: Remove
}

extension ExternalAccessoryService {
    func showAccessoryPicker() {
        accessoryManager.showBluetoothAccessoryPicker(withNameFilter: nil) { error in
            guard let error = error as? EABluetoothAccessoryPickerError else {
                self.delegate?.externalAccessoryServiceDidCancelAccessoryPicker(service: self, error: nil)
                return
            }
            self.delegate?.externalAccessoryServiceDidCancelAccessoryPicker(service: self, error: error)
        }
    }
    func isConnected(accessoryWith macAddress: String) -> Bool {
        return accessoryManager.connectedAccessories.contains {
            ($0.value(forKey: Const.macAddressKey) as! String) == macAddress
        }
    }
    func setAccessoriesMonitoring(enabled: Bool) {
        if enabled {
            NotificationCenter.default.addObserver(self, selector: #selector(handleAccessoryConnection(_:)), name: .EAAccessoryDidConnect, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(handleAccessoryConnection(_:)), name: .EAAccessoryDidDisconnect, object: nil)
            accessoryManager.registerForLocalNotifications()
        } else {
            accessoryManager.unregisterForLocalNotifications()
            NotificationCenter.default.removeObserver(self, name: .EAAccessoryDidConnect, object: nil)
            NotificationCenter.default.removeObserver(self, name: .EAAccessoryDidDisconnect, object: nil)
        }
    }
    func startTestEASession(withAccessoryThatUses macAddress: String, for protocolString: String) {
        guard let accessory = accessoryManager.connectedAccessories.first(where: { ($0.value(forKey: Const.macAddressKey) as! String) == macAddress }),
              accessory.protocolStrings.contains(protocolString) else {
            return
        }
        if let _ = testSession {
            NSLog("SESSOIN ALREADY STARTED")
            return
        }
        testDataSendViaEASession = false
        testSession = EASession(accessory: accessory, forProtocol: protocolString)
        testSession?.outputStream?.delegate = self
        testSession?.inputStream?.delegate = self
        testSession?.outputStream?.schedule(in: RunLoop.main, forMode: .defaultRunLoopMode)
        testSession?.inputStream?.schedule(in: RunLoop.main, forMode: .defaultRunLoopMode)
        testSession?.outputStream?.open()
        testSession?.inputStream?.open()
        NSLog("SESSOIN START \(String(describing: testSession))")
    }
    func stopTestEASession() {
        if let testSession = testSession {
            testSession.outputStream?.close()
            testSession.inputStream?.close()
            testSession.outputStream?.remove(from: .main, forMode: .defaultRunLoopMode)
            testSession.inputStream?.remove(from: .main, forMode: .defaultRunLoopMode)
            testSession.outputStream?.delegate = nil
            testSession.inputStream?.delegate = nil
            NSLog("SESSOIN STOP \(String(describing: self.testSession))")
            self.testSession = nil
        } else {
            NSLog("NO SESSION TO STOP")
        }
    }
}
private extension ExternalAccessoryService {
    @objc func handleAccessoryConnection(_ notification: NSNotification) {
        if let accessory = notification.userInfo?[EAAccessoryKey] as? EAAccessory,
           let macAddress = accessory.value(forKey: Const.macAddressKey) as? String {
            self.delegate?.externalAccessory(service: self, didChangeAccessoryState: AccessoryInfo(mac: macAddress, connected: accessory.isConnected))
        }
    }
}
extension ExternalAccessoryService: StreamDelegate {
    func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
        if let _ = testSession { // TODO: Remove
            NSLog("SESSION STREAM EVENT \(String(describing: eventCode))")
            if eventCode == .hasSpaceAvailable, !testDataSendViaEASession {
                var data: [UInt8] = [0x00, 0x01, 0x02]
                NSLog("SESSION WRITING DATA \(data.hexStringWith8BitSeparator)")
                testSession?.outputStream?.write(&data, maxLength: data.count)
                testDataSendViaEASession.toggle()
            }
        } else {
            if eventCode == .hasBytesAvailable {
                readMacAddressCompletion?()
                readMacAddressCompletion = nil
            }
        }
    }
}
