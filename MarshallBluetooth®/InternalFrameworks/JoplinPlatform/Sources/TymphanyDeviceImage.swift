//
//  TymphanyDeviceImage.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 24/07/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import GUMA

extension Bundle {
    public static var framework: Bundle {
        return Bundle(for: TymphanyDevice.self)
    }
}

struct TymphanyDeviceImage {
    
    private var speakerType: UnitType?
    private var mac: String
    
    init(speakerType: UnitType?, mac: String) {
        self.speakerType = speakerType
        self.mac = mac.uppercased()
    }
}

extension TymphanyDeviceImage: DeviceImageProviding {
    var smallImage: UIImage {
        switch speakerType {
        case .some(.actonII(.some(.black))): return SmallImages.blackActon
        case .some(.actonII(.some(.white))): return SmallImages.whiteActon
        case .some(.actonII(.some(.brown))): return SmallImages.blackActon // fallback to back devices since we have no assets for brown speakers
        case .some(.actonII(.some(.notSupported))): return MacColorMapper.isWhiteActon(mac: self.mac) ? SmallImages.whiteActon : SmallImages.blackActon
        case .some(.stanmoreII(.some(.black))): return SmallImages.blackStanmore
        case .some(.stanmoreII(.some(.white))): return SmallImages.whiteStanmore
        case .some(.stanmoreII(.some(.brown))): return SmallImages.blackStanmore // fallback to back devices since we have no assets for brown speakers
        case .some(.stanmoreII(.some(.notSupported))): return MacColorMapper.isWhiteStanmore(mac: self.mac) ? SmallImages.whiteStanmore : SmallImages.blackStanmore
        case .some(.woburnII(.some(.black))): return SmallImages.blackWoburn
        case .some(.woburnII(.some(.white))): return SmallImages.whiteWoburn
        case .some(.woburnII(.some(.brown))): return SmallImages.blackWoburn // fallback to back devices since we have no assets for brown speakers
        case .some(.woburnII(.some(.notSupported))): return MacColorMapper.isWhiteWoburn(mac: self.mac) ? SmallImages.whiteWoburn : SmallImages.blackWoburn

        case .some(.actonIILite(.black)): return SmallImages.blackActon
        case .some(.actonIILite(.white)): return SmallImages.whiteActon
        case .some(.actonIILite(.brown)): return SmallImages.blackActon // fallback to back devices since we have no assets for brown speakers
        case .some(.stanmoreIILite(.black)): return SmallImages.blackStanmore
        case .some(.stanmoreIILite(.white)): return SmallImages.whiteStanmore
        case .some(.stanmoreIILite(.brown)): return SmallImages.blackStanmore // fallback to back devices since we have no assets for brown speakers
        case .some(.woburnIILite(.black)): return SmallImages.blackWoburn
        case .some(.woburnIILite(.white)): return SmallImages.whiteWoburn
        case .some(.woburnIILite(.brown)): return SmallImages.blackWoburn // fallback to back devices since we have no assets for brown speakers

        case .some(.ozzy(.black)): return SmallImages.blackOzzy
        case .some(.ozzy(.white)): return UIImage() // TODO: ###---
        case .some(.ozzy(.brown)): return UIImage() // TODO: ###---
        case .some(.ozzyAnc(.black)): return SmallImages.blackOzzy
        case .some(.ozzyAnc(.white)): return UIImage() // TODO: ###---
        case .some(.ozzyAnc(.brown)): return UIImage() // TODO: ###---

        default: return UIImage()
        }
    }
    
    var mediumImage: UIImage {
        switch speakerType {
        case .some(.actonII(.some(.black))): return MediumImages.blackActon
        case .some(.actonII(.some(.white))): return MediumImages.whiteActon
        case .some(.actonII(.some(.brown))): return MediumImages.blackActon // fallback to back devices since we have no assets for brown speakers
        case .some(.actonII(.some(.notSupported))): return MacColorMapper.isWhiteActon(mac: self.mac) ? MediumImages.whiteActon : MediumImages.blackActon
        case .some(.stanmoreII(.some(.black))): return MediumImages.blackStanmore
        case .some(.stanmoreII(.some(.white))): return MediumImages.whiteStanmore
        case .some(.stanmoreII(.some(.brown))): return MediumImages.blackStanmore // fallback to back devices since we have no assets for brown speakers
        case .some(.stanmoreII(.some(.notSupported))): return MacColorMapper.isWhiteStanmore(mac: self.mac) ? MediumImages.whiteStanmore : MediumImages.blackStanmore
        case .some(.woburnII(.some(.black))): return MediumImages.blackWoburn
        case .some(.woburnII(.some(.white))): return MediumImages.whiteWoburn
        case .some(.woburnII(.some(.brown))): return MediumImages.blackWoburn // // fallback to back devices since we have no assets for brown speakers
        case .some(.woburnII(.some(.notSupported))): return MacColorMapper.isWhiteWoburn(mac: self.mac) ? MediumImages.whiteWoburn : MediumImages.blackWoburn

        case .some(.actonIILite(.black)): return MediumImages.blackActon
        case .some(.actonIILite(.white)): return MediumImages.whiteActon
        case .some(.actonIILite(.brown)): return MediumImages.blackActon // fallback to back devices since we have no assets for brown speakers
        case .some(.stanmoreIILite(.black)): return MediumImages.blackStanmore
        case .some(.stanmoreIILite(.white)): return MediumImages.whiteStanmore
        case .some(.stanmoreIILite(.brown)): return MediumImages.blackStanmore // fallback to back devices since we have no assets for brown speakers
        case .some(.woburnIILite(.black)): return MediumImages.blackWoburn
        case .some(.woburnIILite(.white)): return MediumImages.whiteWoburn
        case .some(.woburnIILite(.brown)): return MediumImages.blackWoburn // fallback to back devices since we have no assets for brown speakers

        case .some(.ozzy(.black)): return MediumImages.blackOzzy
        case .some(.ozzy(.white)): return UIImage() // TODO: ###---
        case .some(.ozzy(.brown)): return UIImage() // TODO: ###---
        case .some(.ozzyAnc(.black)): return MediumImages.blackOzzy
        case .some(.ozzyAnc(.white)): return UIImage() // TODO: ###---
        case .some(.ozzyAnc(.brown)): return UIImage() // TODO: ###---

        default: return UIImage()
        }
    }
    
    var largeImage: UIImage {
        switch speakerType {
        case .some(.actonII(.some(.black))): return LargeImages.blackActon
        case .some(.actonII(.some(.white))): return LargeImages.whiteActon
        case .some(.actonII(.some(.brown))): return LargeImages.blackActon // fallback to back devices since we have no assets for brown speakers
        case .some(.actonII(.some(.notSupported))): return MacColorMapper.isWhiteActon(mac: self.mac) ? LargeImages.whiteActon : LargeImages.blackActon
        case .some(.stanmoreII(.some(.black))): return LargeImages.blackStanmore
        case .some(.stanmoreII(.some(.white))): return LargeImages.whiteStanmore
        case .some(.stanmoreII(.some(.brown))): return LargeImages.blackStanmore // fallback to back devices since we have no assets for brown speakers
        case .some(.stanmoreII(.some(.notSupported))): return MacColorMapper.isWhiteStanmore(mac: self.mac) ? LargeImages.whiteStanmore : LargeImages.blackStanmore
        case .some(.woburnII(.some(.black))): return LargeImages.blackWoburn
        case .some(.woburnII(.some(.white))): return LargeImages.whiteWoburn
        case .some(.woburnII(.some(.brown))): return LargeImages.blackWoburn // fallback to back devices since we have no assets for brown speakers
        case .some(.woburnII(.some(.notSupported))): return MacColorMapper.isWhiteWoburn(mac: self.mac) ? LargeImages.whiteWoburn : MediumImages.blackWoburn

        case .some(.actonIILite(.black)): return LargeImages.blackActon
        case .some(.actonIILite(.white)): return LargeImages.whiteActon
        case .some(.actonIILite(.brown)): return LargeImages.blackActon // fallback to back devices since we have no assets for brown speakers
        case .some(.stanmoreIILite(.black)): return LargeImages.blackStanmore
        case .some(.stanmoreIILite(.white)): return LargeImages.whiteStanmore
        case .some(.stanmoreIILite(.brown)): return LargeImages.blackStanmore // fallback to back devices since we have no assets for brown speakers
        case .some(.woburnIILite(.black)): return LargeImages.blackWoburn
        case .some(.woburnIILite(.white)): return LargeImages.whiteWoburn
        case .some(.woburnIILite(.brown)): return LargeImages.blackWoburn // fallback to back devices since we have no assets for brown speakers

        case .some(.ozzy(.black)): return LargeImages.blackOzzy
        case .some(.ozzy(.white)): return UIImage() // TODO: ###---
        case .some(.ozzy(.brown)): return UIImage() // TODO: ###---
        case .some(.ozzyAnc(.black)): return LargeImages.blackOzzy
        case .some(.ozzyAnc(.white)): return UIImage() // TODO: ###---
        case .some(.ozzyAnc(.brown)): return UIImage() // TODO: ###---

        default: return UIImage()
        }
    }
    
    var pairingSetupImage: UIImage {
        switch speakerType {
        case .some(.ozzy(.black)): return PairingImages.blackOzzy
        case .some(.ozzy(.white)): return UIImage() // TODO: ###---
        case .some(.ozzy(.brown)): return UIImage() // TODO: ###---
        case .some(.ozzyAnc(.black)): return PairingImages.blackOzzy
        case .some(.ozzyAnc(.white)): return UIImage() // TODO: ###---
        case .some(.ozzyAnc(.brown)): return UIImage() // TODO: ###---
        default: return UIImage()
        }
    }
    
}

struct SmallImages {
    static let blackActon = UIImage(named: "joplin_actonII_black_s", in: .framework, compatibleWith: nil)!
    static let whiteActon = UIImage(named: "joplin_actonII_white_s", in: .framework, compatibleWith: nil)!
    static let brownActon = UIImage(named: "joplin_actonII_brown_s", in: .framework, compatibleWith: nil)!
    
    static let blackStanmore = UIImage(named: "joplin_stanmoreII_black_s", in: .framework, compatibleWith: nil)!
    static let whiteStanmore = UIImage(named: "joplin_stanmoreII_white_s", in: .framework, compatibleWith: nil)!
    static let brownStanmore = UIImage(named: "joplin_stanmoreII_brown_s", in: .framework, compatibleWith: nil)!
    
    static let blackWoburn = UIImage(named: "joplin_woburnII_black_s", in: .framework, compatibleWith: nil)!
    static let whiteWoburn = UIImage(named: "joplin_woburnII_white_s", in: .framework, compatibleWith: nil)!
    static let brownWoburn = UIImage(named: "joplin_woburnII_brown_s", in: .framework, compatibleWith: nil)!

    static let blackOzzy = UIImage(named: "ozzy_black_s", in: .framework, compatibleWith: nil)!
}

struct MediumImages {
    static let blackActon = UIImage(named: "joplin_actonII_black_m", in: .framework, compatibleWith: nil)!
    static let whiteActon = UIImage(named: "joplin_actonII_white_m", in: .framework, compatibleWith: nil)!
    static let brownActon = UIImage(named: "joplin_actonII_brown_m", in: .framework, compatibleWith: nil)!
    
    static let blackStanmore = UIImage(named: "joplin_stanmoreII_black_m", in: .framework, compatibleWith: nil)!
    static let whiteStanmore = UIImage(named: "joplin_stanmoreII_white_m", in: .framework, compatibleWith: nil)!
    static let brownStanmore = UIImage(named: "joplin_stanmoreII_brown_m", in: .framework, compatibleWith: nil)!
    
    static let blackWoburn = UIImage(named: "joplin_woburnII_black_m", in: .framework, compatibleWith: nil)!
    static let whiteWoburn = UIImage(named: "joplin_woburnII_white_m", in: .framework, compatibleWith: nil)!
    static let brownWoburn = UIImage(named: "joplin_woburnII_brown_m", in: .framework, compatibleWith: nil)!

    static let blackOzzy = UIImage(named: "ozzy_black_m", in: .framework, compatibleWith: nil)!
}

struct LargeImages {
    static let blackActon = UIImage(named: "joplin_actonII_black_l", in: .framework, compatibleWith: nil)!
    static let whiteActon = UIImage(named: "joplin_actonII_white_l", in: .framework, compatibleWith: nil)!
    static let brownActon = UIImage(named: "joplin_actonII_brown_l", in: .framework, compatibleWith: nil)!
    
    static let blackStanmore = UIImage(named: "joplin_stanmoreII_black_l", in: .framework, compatibleWith: nil)!
    static let whiteStanmore = UIImage(named: "joplin_stanmoreII_white_l", in: .framework, compatibleWith: nil)!
    static let brownStanmore = UIImage(named: "joplin_stanmoreII_brown_l", in: .framework, compatibleWith: nil)!
    
    static let blackWoburn = UIImage(named: "joplin_woburnII_black_l", in: .framework, compatibleWith: nil)!
    static let whiteWoburn = UIImage(named: "joplin_woburnII_white_l", in: .framework, compatibleWith: nil)!
    static let brownWoburn = UIImage(named: "joplin_woburnII_brown_l", in: .framework, compatibleWith: nil)!

    static let blackOzzy = UIImage(named: "ozzy_black_l", in: .framework, compatibleWith: nil)!
}

struct PairingImages {
    static let blackOzzy = UIImage(named: "ozzy_black_pairing", in: .framework, compatibleWith: nil)!
}
