//
//  TymphanyDevice.swift
//  JoplinPlatform
//
//  Created by Grzegorz Kiel on 07/05/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import TAPlatformSwift
import RxSwift
import GUMA

private extension UnitType {
    var modelName: String {
        switch self {
        case .actonII:
            return "ACTON II"
        case .stanmoreII:
            return "STANMORE II"
        case .woburnII:
            return "WOBURN II"
        case .actonIILite:
            return "ACTON II LITE"
        case .stanmoreIILite:
            return "STANMORE II LITE"
        case .woburnIILite:
            return "WOBURN II LITE"
        case .ozzy, .ozzyAnc:
            return "MONITOR II ANC"
        }
    }
}
private struct Config {
    static let connectionTimeout = TimeInterval.seconds(15.0)
    static let disconnectionTimeout = TimeInterval.seconds(15.0)
    static let macRequestTimeout = TimeInterval.seconds(5.0)
    static let trueWirelessRequestTimeout = TimeInterval.seconds(5.0)
}
private enum SetupError: Int, Error  {
    case connectTimeout
    case macRequestTimeout
}
public final class TymphanyDevice: DeviceType {
    public var state: DeviceStateType {
        return internalState
    }
    public var configured: Bool = false
    public func initialSetup(with storedEntry: StoredDeviceInfoEntry?) -> Single<String> {
        /// Check if device already configured
        guard let entry = storedEntry else {
            /// Application started on new device or
            /// device not previously configured
            return firstTimeSetup()
        }
        return setup(with: entry)
    }
    public func decouple() -> Single<Void> {
        return prepareForConnect(device: self)
            .flatMap { device -> Single<Void> in
                return device.internalState
                    .wait(for: .connected)
                    .flatMap { [weak self] _ in
                        guard let strongSelf = self else { return Single.just(()) }
                        return strongSelf.decoupleMaster()
                    }
                    .map { return device }
                    .flatMap(concludeConnect)
                    .flatMap { _ in return Single.just(()) }
            }
    }
    public func resetSlave() -> Single<Void> {
        return prepareForConnect(device: self)
            .flatMap { device -> Single<Void> in
                return device.internalState.wait(for: .connected)
                    .map { _ in return device }
                    .flatMap(concludeConnect)
                    .flatMap { _ in return Single.just(()) }
            }
    }
    public func bleConnect() -> Single<Void> {
        return prepareForConnect(device: self)
            .flatMap { device -> Single<Void> in
                return device.internalState
                    .wait(for: .connected)
                    .map { _ in return device } // CLASSIC <-> BLE SETUP HERE
                    .flatMap(concludeConnect)
                    .flatMap { _ in return Single.just(()) }
            }
    }
    public var modelName: String {
        return _underlyingHw.type?.modelName ?? String()
    }
    public var connectionTimeout: TimeInterval {
        return  Config.connectionTimeout
    }
    public var id: String {
        return _underlyingHw.tapSystem.uuidString
    }
    public var hardwareType: HardwareType {
        guard let type = _underlyingHw.type else { return .speaker }
        switch type {
        case .ozzy, .ozzyAnc:
            return .headset
        default: return .speaker
        }
    }
    public var image: DeviceImageProviding = TymphanyDeviceImage(speakerType: .actonII(.notSupported), mac: String())
    var internalState: TymphanyDeviceState
    
    public var mac: MAC?
    
    public var otaAdapter: OTAAdapterType?
    
    public let platform: Platform
    
    var hw: TAPSSystem {
        get {
            return _underlyingHw
        }
        set {
            _underlyingHw = newValue
            internalState.hw = newValue
        }
    }
    init(hwDevice device: TAPSSystem,
         systemService: TymphanySystemServiceAdapter,
         playControlAdapter: TymphanyPlayControlServiceAdapter,
         dspControlAdapter: TymphanyDigitalSignalProcessingServiceAdapter,
         firmwareOTAAdapter: TymphanyFirmwareOverTheAirUpdateServiceAdapterType,
         connectivityStatus: ConnectivityStatus,
         platform: Platform
        ) {
        self._underlyingHw = device
        self._systemService = systemService
        self._playControlAdapter = playControlAdapter
        self.otaAdapter = TymphanyOTAAdapter(systemServiceAdapter: systemService, hwDevice: device, otaAdapter: firmwareOTAAdapter)
        self.platform = platform
        self.internalState = TymphanyDeviceState(underlyingHardware: device,
                                                  systemService: systemService,
                                                  playControlService: playControlAdapter,
                                                  dspControlService: dspControlAdapter,
                                                  connectivityStatus: connectivityStatus)
    }
    
    deinit {
        print("💕 \(self.state.outputs.name().stateObservable().value) deinited")
    }
    
    private weak var _playControlAdapter: TymphanyPlayControlServiceAdapter?
    private weak var _systemService: TymphanySystemServiceAdapter?
    private var _underlyingHw: TAPSSystem
}

private extension TymphanyDevice {
    func requestMAC() -> Single<MAC> {
        var macRequestDisposeBag = DisposeBag()
        return Single<MAC>.create(subscribe: { [weak self] singleEvent in
            guard let system = self?._systemService, let hardware = self?._underlyingHw else {
                return Disposables.create()
            }
            system.requestMACAddress(of: hardware)
                .take(1)
                .timeout(TimeInterval.seconds(Config.macRequestTimeout), scheduler: MainScheduler.instance)
                .observeOn(MainScheduler.instance)
                .subscribe(
                    onNext: { macData in
                        singleEvent(.success(MAC(data: macData)))
                        macRequestDisposeBag = DisposeBag()
                    },
                    onError: { error in
                        singleEvent(.error(error))
                        macRequestDisposeBag = DisposeBag()
                    }
                )
                .disposed(by: macRequestDisposeBag)
            return Disposables.create()
        })
    }
    func trueWirelessStatus(mac: MAC) -> Single<(MAC, WirelessStereoStatus)> {
        var trueWirelessRequestDisposeBag = DisposeBag()
        return Single<(MAC, WirelessStereoStatus)>.create(subscribe: { [weak self] singleEvent in
            guard let playControlAdapter = self?._playControlAdapter, let hardware = self?._underlyingHw else {
                return Disposables.create()
            }
            playControlAdapter.requestTrueWirelessStatus(of: hardware)
                .take(1)
                .timeout(TimeInterval.seconds(Config.trueWirelessRequestTimeout), scheduler: MainScheduler.instance)
                .observeOn(MainScheduler.instance)
                .subscribe(
                    onNext: { wirelessStatus in
                        singleEvent(.success((mac, wirelessStatus)))
                        trueWirelessRequestDisposeBag = DisposeBag()
                },
                    onError: { error in
                        singleEvent(.error(error))
                        trueWirelessRequestDisposeBag = DisposeBag()
                }
                )
                .disposed(by: trueWirelessRequestDisposeBag)
            return Disposables.create()
        })
    }
    func fullDeviceInfo() -> Single<(MAC, WirelessStereoStatus)> {
        var fullDeviceInfoDisposeBag = DisposeBag()
        return Single<(MAC, WirelessStereoStatus)>.create(subscribe: { [weak self] singleEvent in
            guard let strongSelf = self else { return Disposables.create() }
            strongSelf.requestMAC().flatMap { mac -> Single<(MAC, WirelessStereoStatus)> in
                return strongSelf.trueWirelessStatus(mac: mac)
            }
            .observeOn(MainScheduler.instance)
            .subscribe(
                onSuccess: { (mac, wirelessStatus) in
                    singleEvent(.success((mac, wirelessStatus)))
                    fullDeviceInfoDisposeBag = DisposeBag()
                },
                onError: { error in
                    strongSelf.state.inputs.set(connectivityStatus: .notConfigured)
                    singleEvent(.error(error))
                    fullDeviceInfoDisposeBag = DisposeBag()
                }
            )
            .disposed(by: fullDeviceInfoDisposeBag)
            return Disposables.create()
        })
    }
    /// No storage entries found for this device - issue "silent connect" , then disconnect device
    func firstTimeSetup() -> Single<String> {
        return Single<String>.create(subscribe: { [weak self] singleEvent  in
            var fullSetupDisposeBag = DisposeBag()
            guard let strongSelf = self else { return Disposables.create() }
            let checkBatteryStatusIfNeeded = batteryStatus(deviceId: strongSelf.id, state: strongSelf.state)
            guard let systemService = strongSelf._systemService else { return Disposables.create() }
            if let type = self?._underlyingHw.type {
                strongSelf.image = TymphanyDeviceImage(speakerType: type, mac: self?.mac?.hex ?? " ")
            }
            var fullSetup = Single.just(String())
            if strongSelf.state.outputs.perDeviceTypeFeatures.contains(.hasTrueWireless) {
                fullSetup = prepareForConnect(device: strongSelf)
                    .flatMap(trueWirelessSetup)
                    .map { return ($0, false) }
                    .flatMap(trueWirelessDisconnectIfNeeded)
                    .flatMap(deviceId)
            } else if strongSelf.state.outputs.perDeviceTypeFeatures.contains(.hasPairing) {
                fullSetup = prepareForConnect(device: strongSelf)
                    .map { [unowned systemService] device in return (systemService.externalAccessoryService, device) }
                    .flatMap(pairingSetup)
                    .map { ($0.state.outputs.connectivityStatus().stateObservable().value )}
                    .flatMap(checkBatteryStatusIfNeeded)
            }
            fullSetup
                .subscribeOn(MainScheduler.instance)
                .subscribe(
                    onSuccess: { deviceId in
                        singleEvent(.success(deviceId))
                        fullSetupDisposeBag = DisposeBag()
                    },
                    onError: { error in
                        singleEvent(.error(error))
                        fullSetupDisposeBag = DisposeBag()
                    }
                )
                .disposed(by: fullSetupDisposeBag)
            return Disposables.create()
        })
    }
    func updateNew(with mac: MAC, _ wirelessStatus: WirelessStereoStatus) {
        self.mac = mac
        /// Update speaker color based on MAC
        self.image = TymphanyDeviceImage(speakerType: _underlyingHw.type, mac: mac.hex)
        /// Check wireless status
        switch wirelessStatus {
        case let .connectedAsMaster(channel, oppositeDeviceMACAddress):
            state.outputs.trueWirelessStatus().stateObservable().accept(.connectedAsMaster(channel, oppositeDeviceMACAddress: oppositeDeviceMACAddress))
        case let .connectedAsSlave(channel, oppositeDeviceMACAddress):
            state.outputs.trueWirelessStatus().stateObservable().accept(.connectedAsSlave(channel, oppositeDeviceMACAddress: oppositeDeviceMACAddress))
        default:
            break
        }
    }
    func setup(with entry: StoredDeviceInfoEntry) -> Single<String> {
        if let type = self._underlyingHw.type {
            self.image = TymphanyDeviceImage(speakerType: type, mac: self.mac?.hex ?? " ")
        }
        guard let systemService = _systemService else {
            return deviceId(device: self)
        }
        var setup = Single.just(String())
        if self.state.outputs.perDeviceTypeFeatures.contains(.hasPairing) {
            let checkBatteryStatusIfNeeded = batteryStatus(deviceId: entry.id, state: state)
            setup = prepareForConnect(device: self)
                .map { [unowned systemService] device in return (systemService.externalAccessoryService, device) }
                .flatMap(pairingSetup)
                .map { ($0.state.outputs.connectivityStatus().stateObservable().value )}
                .flatMap(checkBatteryStatusIfNeeded)
            return setup
        }
        else if self.state.outputs.perDeviceTypeFeatures.contains(.hasTrueWireless) {
            setup = prepareForConnect(device: self)
                .flatMap(trueWirelessSetup)
                .map { return ($0, true) }
                .flatMap(trueWirelessDisconnectIfNeeded)
                .flatMap(deviceId)
            return setup
        }
        return Single.just(entry.id)
    }
    func decoupleMaster() -> Single<Void> {
        return Single<Void>.create(subscribe: { [weak self] singleEvent  in
            guard let strongSelf = self,
                let trueWirelessService = strongSelf._playControlAdapter?.service  else {
                singleEvent(.success(()))
                return Disposables.create()
            }
            trueWirelessService.disconnectTrueWireless(of: strongSelf._underlyingHw, completion: { _ in
                singleEvent(.success(()))
            })
            return Disposables.create()
        })
    }
    func decoupleSlave() -> Single<Void> {
        return Single<Void>.create(subscribe: { singleEvent  in
            singleEvent(.success(()))
            return Disposables.create()
        })
    }
}
private func batteryStatus(deviceId: String, state: DeviceStateType) -> (ConnectivityStatus) -> Single<String> {
    return { status in
        guard state.outputs.perDeviceTypeFeatures.contains(.hasBatteryStatus) else {
            return Single.just(deviceId)
        }
        guard state.outputs.connectivityStatus().stateObservable().value == .connected else {
            return Single.just(deviceId)
        }
        return Single.create { event in
            var disposeBag = DisposeBag()
            state.outputs.batteryStatus().stateObservable()
                .observeOn(MainScheduler.instance)
                .skip(1)
                .subscribe(
                    onNext: { status in
                        event(.success(deviceId))
                        disposeBag = DisposeBag()
                    },
                    onError: { error in
                        event(.error(error))
                        disposeBag = DisposeBag()
                    }
                )
                .disposed(by: disposeBag)
            state.inputs.requestBatteryStatus()
            return Disposables.create()
        }
    }
}
private func trueWirelessSetup(device: TymphanyDevice) -> Single<TymphanyDevice> {
    return device.internalState.silentInput.connect()
        .map { status in
            return (device, status)
        }
        .flatMap(trueWirelessStatus)
}
private func trueWirelessDisconnectIfNeeded(device: TymphanyDevice, previouslyConfigured: Bool) -> Single<DeviceType> {
    let trueWirelessStatus = device.state.outputs.trueWirelessStatus().stateObservable().value
    switch (previouslyConfigured, trueWirelessStatus) {
    case (true, .disconnected):
        return Single.just(device)
            .flatMap(concludeConnect)
            .map { $0 }
    default:
        return disconnect(device: device)
    }
}
private func trueWirelessStatus(device: TymphanyDevice, status: ConnectivityStatus) -> Single<TymphanyDevice> {
    return device.fullDeviceInfo()
        .map { mac, trueWirelessStatus in
            return (device, mac, trueWirelessStatus)
        }
        .flatMap { device, mac, trueWirelessStatus in
            device.updateNew(with: mac, trueWirelessStatus)
            if case .disconnected = trueWirelessStatus {
                if device.state.outputs.connectivityStatus().stateObservable().value == .notConfigured {
                    device.state.inputs.set(connectivityStatus: .readyToConnect)
                    return Single.just(device)
                }
                device.state.inputs.set(connectivityStatus: .notConfigured)
            }
            return Single.just(device)
        }
}
private func pairingSetup(accessoryService: ExternalAccessoryServiceType, device: TymphanyDevice) -> Single<DeviceType> {
    guard let mac = device.mac,
        accessoryService.isConnected(accessoryWith: mac.hex) else {
            if device.state.outputs.connectivityStatus().stateObservable().value == .notConfigured {
                device.state.inputs.set(connectivityStatus: .readyToConnect)
                return Single.just(device)
            } else {
                device.state.inputs.set(connectivityStatus: .notConfigured)
                
            }
            return Single.just(device)
        }
    return device.internalState.silentInput.connect()
        .map { status in
            return (device, status)
        }
        .flatMap { device, _ in
            device.state.inputs.set(connectivityStatus: .connected)
            return Single.just(device)
        }
}
private func disconnect(device: TymphanyDevice) -> Single<DeviceType> {
    return device
        .internalState
        .wait(for: .disconnected)
        .map { _ in return device }
}
private func prepareForConnect(device: TymphanyDevice) -> Single<TymphanyDevice> {
    device.state.inputs.set(connectivityStatus: .connecting)
    return Single.just(device)
}
private func concludeConnect(device: TymphanyDevice) -> Single<TymphanyDevice> {
    device.state.inputs.set(connectivityStatus: .connected)
    return Single.just(device)
}
private func deviceId(device: DeviceType) -> Single<String> {
    return Single.just(device.id)
}
