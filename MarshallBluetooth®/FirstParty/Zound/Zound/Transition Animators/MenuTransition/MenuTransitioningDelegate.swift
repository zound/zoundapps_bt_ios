//
//  MenuTransitioningDelegate.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 28/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public class MenuTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {
    
    public var disableInteractivePlayerTransitioning: Bool = false

    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let animator = MenuViewAnimator()
        animator.transitionType = .dismiss
        return animator
    }
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let animator = MenuViewAnimator()
        animator.transitionType = .present
        return animator
    }

}
