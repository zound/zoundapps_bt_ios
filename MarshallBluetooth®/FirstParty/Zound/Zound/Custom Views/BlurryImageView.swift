//
//  BlurryImageView.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit

public class BlurryImageView: UIImageView {

    @IBInspectable public var blurImage:UIImage?{
        didSet{
            updateImage(blurImage!)
        }
    }
    
    @IBInspectable public var blurRadius:Int = 5 {
        didSet{
            updateImage(blurImage!)
        }
    }
    
    
    public func updateImage(_ imageBlur:UIImage){
        let imageToBlur:CIImage = CIImage(image: imageBlur)!
        let blurFilter:CIFilter = CIFilter(name: "CIGaussianBlur")!
        blurFilter.setValue(imageToBlur, forKey: "inputImage")
        
        blurFilter.setValue(NSNumber(value: blurRadius as Int), forKey: "inputRadius")
        let resultImage:CIImage = blurFilter.value(forKey: "outputImage")! as! CIImage
        var rect = resultImage.extent
        rect.origin.x += (rect.size.width - imageBlur.size.width)/2
        rect.origin.y += (rect.size.height - imageBlur.size.height)/2
        rect.size      = imageBlur.size;
        let context = CIContext(options: nil)
        let cgimg  = context.createCGImage(resultImage, from: rect)
        let blurredImage = UIImage(cgImage: cgimg!)
        
        self.image = blurredImage
    }
}
