//
//  UIViewController+StatusBarStyle.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 20/02/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

public class UENavigationController: UINavigationController {
    
    
    override public var childViewControllerForStatusBarHidden: UIViewController? {
        
        
        return self.topViewController
    }
    
    override public var childViewControllerForStatusBarStyle: UIViewController? {
        
        return self.topViewController
    }
}
