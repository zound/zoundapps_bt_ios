//
//  UrbanEars-Bridging-Header.h
//  UrbanEars
//
//  Created by Raul Andrisan on 14/07/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

#ifndef UrbanEars_Bridging_Header_h
#define UrbanEars_Bridging_Header_h

#import "UIImage+Trim.h"
#import "FadingScrollView.h"
#endif /* UrbanEars_Bridging_Header_h */
