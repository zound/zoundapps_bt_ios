//
//  FirebaseAnalyticsParametersBuilder.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 09/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation
import MarshallBluetoothLibrary

class FirebaseAnalyticsParametersBuilder {

    private enum FirebaseEventParameterName: String {
        case deviceModel
    }

    private var internalParameters: [String: FirebaseLoggable] = [:]

    var parameters: [String : Any]? {
        return internalParameters.isEmpty ? nil : internalParameters.mapValues { $0.objectValue }
    }

    @discardableResult
    func add(_ parameters: [String: FirebaseLoggable]?) -> FirebaseAnalyticsParametersBuilder {
        guard let nonOptionalParameters = parameters else { return self }
        internalParameters = internalParameters.merging(nonOptionalParameters) { (_, new) in new }
        return self
    }

    @discardableResult
    func add(_ deviceModelParameter: DeviceModel?) -> FirebaseAnalyticsParametersBuilder {
        guard let nonOptionalDeviceModelParameterName = deviceModelParameter?.name else { return self }
        internalParameters[FirebaseEventParameterName.deviceModel.rawValue] = nonOptionalDeviceModelParameterName
        return self
    }

}
