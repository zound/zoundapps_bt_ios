//
//  FirebaseAnalyticsService.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 01/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import FirebaseCore
import FirebaseAnalytics
import MarshallBluetoothLibrary

final class FirebaseAnalyticsService: AnalyticsServiceType, AnalyticsServiceInputs {

    public var inputs: AnalyticsServiceInputs { return self }

    public func log(_ event: AnalyticsEvent, associatedWith deviceModel: DeviceModel?) {
        let parameters = FirebaseAnalyticsParametersBuilder()
            .add(deviceModel)
            .add(event.parameters)
            .parameters
        Analytics.logEvent(event.name, parameters: parameters)
    }

    public func setAnalyticsCollection(enabled: Bool) {
        AnalyticsConfiguration.shared().setAnalyticsCollectionEnabled(enabled)
    }


    public init() {
        FirebaseConfiguration.shared.setLoggerLevel(.min)
        FirebaseApp.configure()
    }
}
