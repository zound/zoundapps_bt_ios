//
//  FirebaseLoggable.swift
//  MarshallBluetooth®
//
//  Created by Wudarski Lukasz on 01/08/2018.
//  Copyright © 2018 ZoundIndustries. All rights reserved.
//

import Foundation

protocol FirebaseLoggable {
var objectValue: NSObject { get }
}

extension String: FirebaseLoggable {
    var objectValue: NSObject {
        return self as NSString
    }
}

extension Int: FirebaseLoggable {
    var objectValue: NSObject {
        return NSNumber(value: self)
    }
}

extension UInt: FirebaseLoggable {
    var objectValue: NSObject {
        return NSNumber(value: self)
    }
}
