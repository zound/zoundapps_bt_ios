import UIKit
@testable import MarshallBluetoothLibrary
import PlaygroundSupport
import RxSwift
import GUMA

loadFonts()

let controller = UIViewController.instantiate(DeviceSettingsViewController.self)

let jsonHW = JsonMockDeviceHw(name: "Mocked", id: "aa:bb:cc:dd:ee:ff", playing: false, connected: true, hasUpdate: false)
let jsonDevice = JSONMockDevice(hwDevice: jsonHW, hasUpdate: false, platform: Platform(id: "joplinBT", traits: PlatformTraits()))

let model = DeviceSettingsViewModel(device: jsonDevice)
controller.viewModel = model

var forceUpdate = true
jsonDevice.otaAdapter?.outputs.updateAvailable.onNext(forceUpdate)

let (parent, _) = playgroundControllers(device: .phone5_5inch, orientation: .portrait, child: controller)
let frame = parent.view.frame

//// Present the view controller in the Live View window
PlaygroundPage.current.liveView = parent
parent.view.frame = frame

PlaygroundPage.current.needsIndefiniteExecution = true

let timer = Timer.scheduledTimer(withTimeInterval: TimeInterval.seconds(3), repeats: true) { _ in
    
    forceUpdate = !forceUpdate
    jsonDevice.otaAdapter?.outputs.updateAvailable.onNext(forceUpdate)
}
