import UIKit
@testable import MarshallBluetoothLibrary
import PlaygroundSupport
import RxSwift
import GUMA

loadFonts()

let controller = UIViewController.instantiate(BluetoothPlayerSourceViewController.self)

let jsonHW = JsonMockDeviceHw(name: "Mocked", id: "aa:bb:cc:dd:ee:ff", playing: false, connected: true, hasUpdate: false)
let jsonDevice = JSONMockDevice(hwDevice: jsonHW, hasUpdate: false, platform: Platform(id: "joplinBT", traits: PlatformTraits()))

let audioSourceModel = AudioSourceViewModel(device: jsonDevice, audioSource: .bluetooth)
let bluetoothSourceModel = BluetoothPlayerViewModel(device: jsonDevice)

controller.audioSourceViewModel = audioSourceModel
controller.bluetoothPlayerViewModel = bluetoothSourceModel

let (parent, _) = playgroundControllers(device: .phone5_8inch, orientation: .portrait, child: controller)
let frame = parent.view.frame

//// Present the view controller in the Live View window
PlaygroundPage.current.liveView = parent
parent.view.frame = frame

PlaygroundPage.current.needsIndefiniteExecution = true
