import UIKit
@testable import MarshallBluetoothLibrary
import PlaygroundSupport
import GUMA
import RxSwift

var applicationMode = BehaviorRelay<AppMode>.init(value: .foreground)

loadFonts()

AppEnvironment.replaceCurrentEnvironment(Environment(language: .de))

let progressObservable = Observable<SetupProgressInfo>.just(SetupProgressInfo(otaStatus: OTAUpdateInfo(deviceID: "aa:bb:cc:dd:ee:ff", status: .uploadingToDevice(0.7), error: nil)))

let advertisementController = UIViewController.instantiate(AdvertisementViewController.self)
let platform = Platform(id: "joplinBT", traits: PlatformTraits())
advertisementController.configure(withAdvertisementControllers: AdvertisementPage.viewControllers(for: platform))
let advertisementViewModel = AdvertisementViewModel(progress: progressObservable)
advertisementController.viewModel = advertisementViewModel


let (parent, _) = playgroundControllers(device: .phone5_8inch, orientation: .portrait, child: advertisementController)
let frame = parent.view.frame

// Present the view controller in the Live View window
PlaygroundPage.current.liveView = parent
parent.view.frame = frame

PlaygroundPage.current.needsIndefiniteExecution = true
