import UIKit
@testable import MarshallBluetoothLibrary
import PlaygroundSupport
import RxSwift

loadFonts()

AppEnvironment.replaceCurrentEnvironment(Environment(language: .en))

let controller = UIViewController.instantiate(CoupleSpeakersAdvertViewController.self)

let (parent, _) = playgroundControllers(device: .phone5_5inch, orientation: .portrait, child: controller)
let frame = parent.view.frame

// Present the view controller in the Live View window
PlaygroundPage.current.liveView = parent
parent.view.frame = frame
