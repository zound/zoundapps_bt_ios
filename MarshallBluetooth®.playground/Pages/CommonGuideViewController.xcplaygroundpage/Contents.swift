//: [Previous](@previous)

import UIKit
@testable import MarshallBluetoothLibrary
import PlaygroundSupport
import RxSwift

loadFonts()

AppEnvironment.replaceCurrentEnvironment(Environment(language: .en))

let controller = UIViewController.instantiate(CommonGuideViewController.self)
controller.viewModel = CommonGuideViewModel()
let (parent, _) = playgroundControllers(device: .phone4_7inch, orientation: .portrait, child: controller)
let frame = parent.view.frame

// Present the view controller in the Live View window
PlaygroundPage.current.liveView = parent
parent.view.frame = frame
