import UIKit
@testable import MarshallBluetoothLibrary
import PlaygroundSupport
import RxSwift
import GUMA

loadFonts()

let controller = UIViewController.instantiate(AnalogAudioSourceViewController.self)

let jsonHW = JsonMockDeviceHw(name: "Mocked", id: "aa:bb:cc:dd:ee:ff", playing: false, connected: true, hasUpdate: false)
let jsonDevice = JSONMockDevice(hwDevice: jsonHW, hasUpdate: false, platform: Platform(id: "joplinBT", traits: PlatformTraits()))

let viewModel = AudioSourceViewModel(device: jsonDevice, audioSource: .bluetooth)
controller.viewModel = viewModel

let (parent, _) = playgroundControllers(device: .phone5_8inch, orientation: .portrait, child: controller)
let frame = parent.view.frame

//// Present the view controller in the Live View window
PlaygroundPage.current.liveView = parent
parent.view.frame = frame

PlaygroundPage.current.needsIndefiniteExecution = true
