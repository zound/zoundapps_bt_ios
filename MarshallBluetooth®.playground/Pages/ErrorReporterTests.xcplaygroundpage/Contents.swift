import RCER
import RxSwift
import RxCocoa
import RxTest
import XCTest

public final class DummyInternetConnectionStatusTracker: InternetConnectionStatusTracker {
    
    let onlineStatus = PublishRelay<Bool>()
    
    public func start() { }
    public func stop() { }
    
    public func set(offline: Bool) {
        onlineStatus.accept(offline != true)
    }
    
    public func online() -> Observable<Bool> {
        return onlineStatus.startWith(true)
    }

}

enum ErrorSourcePriority: Int {
    case High
    case Medium
    case Low

    func label() -> String {
        switch self {
        case .High: return "HighPriority"
        case .Medium: return "MediumPriority"
        case .Low: return "LowPriority"
        }
    }
}

class ErrorReporterTest: XCTestCase {

    var testReporter: ErrorReporter<String>!
    var disposeBag: DisposeBag!
    var testScheduler: TestScheduler!
    var testCurrentError: TestableObserver<AnyError<String>?>!
    var internetConnectionStatusTracker: DummyInternetConnectionStatusTracker!
    let dummyNoInternetError = "NoInternetError"
    
    let highPriorityErrorEventsGenerator = PublishRelay<Bool>()
    let mediumPriorityErrorEventsGenerator = PublishRelay<Bool>()
    let lowPriorityErrorEventsGenerator = PublishRelay<Bool>()
    
    override func setUp() {
        super.setUp()
        
        internetConnectionStatusTracker = DummyInternetConnectionStatusTracker()
        
        self.testReporter = ErrorReporter(onlineTracker: internetConnectionStatusTracker)
        self.disposeBag = DisposeBag()
        self.testScheduler = TestScheduler(initialClock: 0)
        self.testCurrentError = testScheduler.createObserver(AnyError<String>?.self)
    }
    
    func testTriggerError() {
        
        let testErrorSource = dummyInternetErrorSource()
        testReporter.append(errorSource: testErrorSource)
        
        testReporter.currentError.asDriver().drive(testCurrentError)
        XCTAssertEqual([.next(0, nil)], testCurrentError.events)
        internetConnectionStatusTracker.set(offline: true)
        XCTAssertEqual([.next(0, nil), .next(0, AnyError<String>(id: dummyNoInternetError))], testCurrentError.events)
        internetConnectionStatusTracker.set(offline: false)
        XCTAssertEqual([.next(0, nil), .next(0, AnyError<String>(id: dummyNoInternetError)), .next(0, nil)], testCurrentError.events)
    }
    
    func testPrioritizedError() {
        let highPriorityErrorSource = prioritizedErrorSource(priority: .High)
        let mediumPriorityErrorSource = prioritizedErrorSource(priority: .Medium)
        let lowPriorityErrorSource = prioritizedErrorSource(priority: .Low)
    
        testReporter.append(errorSource: lowPriorityErrorSource)
        testReporter.append(errorSource: mediumPriorityErrorSource)
        testReporter.append(errorSource: highPriorityErrorSource)
    
        testReporter.currentError.asDriver().drive(testCurrentError)
        XCTAssertEqual([.next(0, nil)], testCurrentError.events)
        lowPriorityErrorEventsGenerator.accept(true)
        XCTAssertEqual([.next(0, nil),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.Low.label()))], testCurrentError.events)
        highPriorityErrorEventsGenerator.accept(true)
        XCTAssertEqual([.next(0, nil),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.Low.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.High.label()))], testCurrentError.events)
        mediumPriorityErrorEventsGenerator.accept(true)
        XCTAssertEqual([.next(0, nil),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.Low.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.High.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.High.label()))], testCurrentError.events)
        highPriorityErrorEventsGenerator.accept(false)
        XCTAssertEqual([.next(0, nil),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.Low.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.High.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.High.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.Medium.label()))], testCurrentError.events)
        mediumPriorityErrorEventsGenerator.accept(false)
        XCTAssertEqual([.next(0, nil),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.Low.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.High.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.High.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.Medium.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.Low.label())),], testCurrentError.events)
        lowPriorityErrorEventsGenerator.accept(false)
        XCTAssertEqual([.next(0, nil),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.Low.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.High.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.High.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.Medium.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.Low.label())),
                        .next(0, nil)], testCurrentError.events)
    }
    
    func testRemoveErrorSource() {
        let highPriorityErrorSource = prioritizedErrorSource(priority: .High)
        let lowPriorityErrorSource = prioritizedErrorSource(priority: .Low)
        
        testReporter.append(errorSource: highPriorityErrorSource)
        testReporter.append(errorSource: lowPriorityErrorSource)
        
        testReporter.currentError.asDriver().drive(testCurrentError)
        
        XCTAssertEqual([.next(0, nil)], testCurrentError.events)
        highPriorityErrorEventsGenerator.accept(true)
        XCTAssertEqual([.next(0, nil),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.High.label()))], testCurrentError.events)
        lowPriorityErrorEventsGenerator.accept(true)
        XCTAssertEqual([.next(0, nil),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.High.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.High.label()))], testCurrentError.events)
        testReporter.remove(sourceForType: ErrorSourcePriority.High.label())
        XCTAssertEqual([.next(0, nil),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.High.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.High.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.Low.label()))], testCurrentError.events)
        testReporter.remove(sourceForType: ErrorSourcePriority.Low.label())
        XCTAssertEqual([.next(0, nil),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.High.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.High.label())),
                        .next(0, AnyError<String>(id: ErrorSourcePriority.Low.label())),
                        .next(0, nil)], testCurrentError.events)
    }

    private func dummyInternetErrorSource() -> ErrorSource<String> {
        
        let trigger = BehaviorRelay<AnyError<String>?>.init(value: nil)
        
        self.testReporter.tracker.online()
            .flatMap { [unowned self] reachable -> Observable<AnyError<String>?> in
                guard reachable == true else {
                    return Observable.just(AnyError<String>.init(id: self.dummyNoInternetError))
                }
                return Observable.just(nil)
            }
            .subscribe(onNext: { error in trigger.accept(error) })
            .disposed(by: disposeBag)
        
        return ErrorSource<String>.init(type: dummyNoInternetError, trigger: trigger, priority: 3)
    }

    private func prioritizedErrorSource(priority: ErrorSourcePriority) -> ErrorSource<String> {
        let errorTrigger = trigger(priority: priority)
        return ErrorSource<String>.init(type: priority.label(), trigger: errorTrigger, priority: priority.rawValue)
    }

    private func trigger(priority: ErrorSourcePriority) -> BehaviorRelay<AnyError<String>?> {
        let trigger = BehaviorRelay<AnyError<String>?>.init(value: nil)
        
        var eventsGenerator: PublishRelay<Bool>? = nil
        switch priority {
        case .High:
            eventsGenerator = highPriorityErrorEventsGenerator
        case .Medium:
            eventsGenerator = mediumPriorityErrorEventsGenerator
        case .Low:
            eventsGenerator = lowPriorityErrorEventsGenerator
        }
        
        eventsGenerator?.startWith(false)
            .flatMap { triggered -> Observable<AnyError<String>?> in
                guard triggered == false else {
                    return Observable.just(AnyError<String>(id: priority.label()))
                }
                return Observable.just(nil)
            }
            .subscribe(onNext: { trigger.accept($0) })
            .disposed(by: disposeBag)
        
        return trigger
    }
}

ErrorReporterTest.defaultTestSuite.run()



