//: [Previous](@previous)

import UIKit
@testable import MarshallBluetoothLibrary
import PlaygroundSupport
import RxSwift

loadFonts()

AppEnvironment.replaceCurrentEnvironment(Environment(language: .de))

let controller = UIViewController.instantiate(OTAFinishedViewController.self)
let image = UIImage(named: "tymphany_mock", in: .framework, compatibleWith: nil)!
let viewModel = OTAFinishedViewModel(image: image)

controller.viewModel = viewModel

let (parent, _) = playgroundControllers(device: .phone4_7inch, orientation: .portrait, child: controller)
let frame = parent.view.frame

// Present the view controller in the Live View window
PlaygroundPage.current.liveView = parent
parent.view.frame = frame

