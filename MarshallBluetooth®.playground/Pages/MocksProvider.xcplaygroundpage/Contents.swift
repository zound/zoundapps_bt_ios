import Foundation
import PlaygroundSupport
import MarshallBluetoothLibrary

let endpoint = "https://artifactorypro.shared.pub.tds.tieto.com/artifactory/zoundindustries/test/mock_config.json"

var mocksProvider: MocksProvider? = nil

let jsonDecoder = JSONDecoder()

let session = URLSession.shared
var request = URLRequest(url: URL(string: endpoint)!)

request.httpMethod = "GET"

let authString = "srv4cntub:qwerty123#".data(using: String.Encoding.utf8)!

request.addValue("application/json", forHTTPHeaderField: "content-type")
request.addValue("application/json", forHTTPHeaderField: "Accept")
request.addValue("Basic ".appending(authString.base64EncodedString()), forHTTPHeaderField: "Authorization");

let task = session.dataTask(with: request) { data, response, error in
    do {
        mocksProvider = try jsonDecoder.decode(MocksProvider.self, from: data!)
    } catch {
        dump(error)
        fatalError()
    }
    dump(mocksProvider)
}.resume()

PlaygroundPage.current.needsIndefiniteExecution = true


