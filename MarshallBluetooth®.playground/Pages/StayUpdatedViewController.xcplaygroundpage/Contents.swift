//: [Previous](@previous)

import UIKit
@testable import MarshallBluetoothLibrary
import PlaygroundSupport
import RxSwift
import Reachability
import GUMA

loadFonts()
AppEnvironment.replaceCurrentEnvironment(Environment(language: .en))

guard let reachability = Reachability() else { fatalError("Could not initialize Reachability") }

struct MockedAnalyticsService: AnalyticsServiceType, AnalyticsServiceInputs {
    var inputs: AnalyticsServiceInputs { return self }
    func log(_ event: AnalyticsEvent, associatedWith deviceModel: DeviceModel?) {}
    func setAnalyticsCollection(enabled: Bool) {}
}
struct MockedAnalyticsServiceDependency: AnalyticsServiceDependency {
    var analyticsService: AnalyticsServiceType = MockedAnalyticsService()
}

struct MockedAppModeDependency: AppModeDependency {
    var appMode = BehaviorRelay<AppMode>(value: .foreground)
}
var applicationMode = BehaviorRelay<AppMode>.init(value: .foreground)

let deviceAdapterService: DeviceAdapterServiceType = DeviceAdapterService.shared
let jsonAdapter = JSONMockDeviceAdapter()

let deviceService = DeviceService(deviceInfoSource: DeviceListChanges(
    new: deviceAdapterService.outputs.addedDevice,
    updated: deviceAdapterService.outputs.updatedDevice,
    removed: deviceAdapterService.outputs.removedDevice))

deviceAdapterService.inputs.append(adapter: jsonAdapter, mocked: true)
deviceAdapterService.inputs.start()

jsonAdapter.adapterState.accept(.ready)
let jsonAppMode = PublishRelay<ApplicationProcessMode>()

let jsonOTAService = OTAService(devicesList: deviceService.outputs.devices, deviceService: deviceService, processMode: jsonAppMode)

let appDependencies = AppDependency(deviceService: deviceService,
                               otaService: jsonOTAService,
                               deviceAdapterService: deviceAdapterService,
                               appMode: BehaviorRelay<AppMode>.init(value: .foreground),
                               analyticsService: MockedAnalyticsService(),
                               newsletterService: NewsletterService())

let controller = UIViewController.instantiate(StayUpdatedViewController.self)
let viewModel = StayUpdatedViewModel(setup: true, dependencies: appDependencies)
controller.viewModel = viewModel

let (parent, _) = playgroundControllers(device: .phone5_5inch, orientation: .portrait, child: controller)
let frame = parent.view.frame
parent.preferredContentSize = UIScreen.main.bounds.size

// Present the view controller in the Live View window
PlaygroundPage.current.liveView = parent
