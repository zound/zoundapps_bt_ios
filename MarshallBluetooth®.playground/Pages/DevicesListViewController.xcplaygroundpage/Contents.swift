import UIKit
@testable import MarshallBluetoothLibrary
import GUMA
import PlaygroundSupport
import RxSwift
import Reachability

struct MockedAnalyticsService: AnalyticsServiceType, AnalyticsServiceInputs {
    var inputs: AnalyticsServiceInputs { return self }
    func log(_ event: AnalyticsEvent, associatedWith deviceModel: DeviceModel?) {}
    func setAnalyticsCollection(enabled: Bool) {}
}

struct MockedAnalyticsServiceDependency: AnalyticsServiceDependency {
    var analyticsService: AnalyticsServiceType = MockedAnalyticsService()
}

struct MockedAppModeDependency: AppModeDependency {
    var appMode = BehaviorRelay<AppMode>(value: .foreground)
}

var applicationMode = BehaviorRelay<AppMode>.init(value: .foreground)

loadFonts()

guard let reachability = Reachability() else { fatalError("Could not initialize Reachability") }

var forceUpdate = false

let deviceAdapterService: DeviceAdapterServiceType = DeviceAdapterService.shared
let jsonAdapter = JSONMockDeviceAdapter()

let deviceService = DeviceService(deviceInfoSource: DeviceListChanges(
    new: deviceAdapterService.outputs.addedDevice,
    updated: deviceAdapterService.outputs.updatedDevice,
    removed: deviceAdapterService.outputs.removedDevice))

deviceAdapterService.inputs.append(adapter: jsonAdapter, mocked: true)
deviceAdapterService.inputs.start()

jsonAdapter.adapterState.accept(.ready)
let jsonAppMode = PublishRelay<ApplicationProcessMode>()

let jsonOTAService = OTAService(devicesList: deviceService.outputs.devices, deviceService: deviceService, processMode: jsonAppMode)

let appDependencies = AppDependency(deviceService: deviceService,
                               otaService: jsonOTAService,
                               deviceAdapterService: deviceAdapterService,
                               appMode: BehaviorRelay<AppMode>.init(value: .foreground),
                               analyticsService: MockedAnalyticsService(),
                               newsletterService: NewsletterService())

let controller = UIViewController.instantiate(DevicesListViewController.self)
let devicesListViewModel = DevicesListViewModel(dependencies: appDependencies)

deviceAdapterService.inputs.start()
deviceAdapterService.inputs.refresh()


controller.viewModel = devicesListViewModel

let (parent, _) = playgroundControllers(device: .phone4inch, orientation: .portrait, child: controller)
let frame = parent.view.frame

//// Present the view controller in the Live View window
PlaygroundPage.current.liveView = parent
parent.view.frame = frame

PlaygroundPage.current.needsIndefiniteExecution = true

//let timer = Timer.scheduledTimer(withTimeInterval: TimeInterval.seconds(3), repeats: true) { _ in
//
//    forceUpdate = !forceUpdate
//    deviceAdapterService.outputs.managedDevices.value.first?.otaAdapter?.outputs.updateAvailable.onNext(forceUpdate)
//}
