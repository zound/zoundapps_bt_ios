import UIKit
@testable import MarshallBluetoothLibrary
import PlaygroundSupport
import RxSwift
import GUMA

loadFonts()

struct MockedAnalyticsService: AnalyticsServiceType, AnalyticsServiceInputs {
    var inputs: AnalyticsServiceInputs { return self }
    func log(_ event: AnalyticsEvent, associatedWith deviceModel: AnalyticsDeviceModel?) {}
    func setAnalyticsCollection(enabled: Bool) {}
}

struct MockedAnalyticsServiceDependency: AnalyticsServiceDependency {
    var analyticsService: AnalyticsServiceType = MockedAnalyticsService()
}

let controller = UIViewController.instantiate(RenameViewController.self)

let jsonHW = JsonMockDeviceHw(name: "Mocked", id: "aa:bb:cc:dd:ee:ff", playing: false, connected: true, hasUpdate: false)
let jsonDevice = JSONMockDevice(hwDevice: jsonHW, hasUpdate: false, platform: Platform(id: "joplinBT", traits: PlatformTraits()))

let model = RenameViewModel(device: jsonDevice, dependencies: MockedAnalyticsServiceDependency())

controller.viewModel = model

let (parent, _) = playgroundControllers(device: .phone5_5inch, orientation: .portrait, child: controller)
let frame = parent.view.frame

//// Present the view controller in the Live View window
PlaygroundPage.current.liveView = parent
parent.view.frame = frame

PlaygroundPage.current.needsIndefiniteExecution = true
