import UIKit
@testable import MarshallBluetoothLibrary
import PlaygroundSupport

loadFonts()

AppEnvironment.replaceCurrentEnvironment(Environment(language: .en))

let controller = UIViewController.instantiate(PairingViewController.self)
let viewModel = PairingViewModel()
controller.viewModel = viewModel


let (parent, _) = playgroundControllers(device: .phone5_5inch, orientation: .portrait, child: controller)
let frame = parent.view.frame

// Present the view controller in the Live View window
PlaygroundPage.current.liveView = parent
parent.view.frame = frame
