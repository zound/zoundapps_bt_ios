import UIKit
@testable import MarshallBluetoothLibrary
import PlaygroundSupport
import RxSwift

loadFonts()

AppEnvironment.replaceCurrentEnvironment(Environment(language: .sv))

let controller = UIViewController.instantiate(ErrorViewController.self)
let viewModel = ErrorViewModel(errorInfo: OTAErrorInfo())

controller.viewModel = viewModel

let (parent, _) = playgroundControllers(device: .phone5_5inch, orientation: .portrait, child: controller)
let frame = parent.view.frame

// Present the view controller in the Live View window
PlaygroundPage.current.liveView = parent
parent.view.frame = frame
