//: [Previous](@previous)

import UIKit
@testable import MarshallBluetoothLibrary
import PlaygroundSupport
import RxSwift

loadFonts()

AppEnvironment.replaceCurrentEnvironment(Environment(language: .en))

let controller = UIViewController.instantiate(OTAUpdateAvailableViewController.self)
let viewModel = OTAUpdateAvailableViewModel()
let disposeBag = DisposeBag()

controller.viewModel = viewModel

let (parent, _) = playgroundControllers(device: .phone5_5inch, orientation: .portrait, child: controller)
let frame = parent.view.frame

// Present the view controller in the Live View window
PlaygroundPage.current.liveView = parent
parent.view.frame = frame

viewModel.outputs.continued
    .subscribe(onNext: { _ in
        print("OTAUpdateAvailableViewModel.outputs.continued event")
    }).disposed(by: disposeBag)
