// Temporary (probably ;) ) logic for generating translations JSON from existing Marshall resources

import MarshallBluetoothLibrary
import PlaygroundSupport

// Keys are labels from existing Localizable.string Marshall files
func prepareJSONForKeys(keys: [String], usingLanguages languages: [Language] = Language.allLanguages) -> String {
    var output: String = "{\n\t\"all\": [\n"
    var eof =            "\n]}\n"
    var insideContent = [String]()
    keys.forEach { key in
        var translationBlock: String = ""
        
        translationBlock.append("\t\t{ \"label\": \"\(key)\",\n")
        translationBlock.append("\t\t  \"translations\": [\n")
        
        var entryLines = [String]()
        languages.forEach { language in
            let switchedBase = language == .en ? "Base" : language.augmentedName()
            guard let path = Bundle.main.url(forResource: "Localizable", withExtension: "strings", subdirectory: "\(switchedBase).lproj") else {
                fatalError("Missing path for language \(language.rawValue)")
            }
            guard let dict = NSDictionary(contentsOf: path) else {
                fatalError("Could not create dictionary for label : \(key), language: \(language.rawValue)")
            }
            guard let translatedString = dict[key] as? String else {
                fatalError("Translation not found for label : \(key), language: \(language.rawValue)")
            }
            entryLines.append("\t\t\t { \"lang\": \"\(language.rawValue)\", \"translation\": \"\(escaped(translatedString))\" }")
        }
        translationBlock.append(entryLines.joined(separator: ",\n"))
        
        translationBlock.append("\n\t\t  ]\n")
        translationBlock.append("\t\t}")
        insideContent.append(translationBlock)
    }
    output.append(insideContent.joined(separator: ",\n"))
    output.append(eof)
    
    return output
}

// Copy-paste generated content to Translations.json file, watching for unescaped characters like `\n` that needs to be manually escaped.
print(prepareJSONForKeys(keys: ["welcome.buttons.accept",
                                "welcome.subtitle",
                                "welcome.title",
                                "loading.loading_speakers"]))

